package com.ampere.bike.ampere;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.FragmentActivity;

import com.ampere.bike.ampere.shared.LocalSession;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest extends FragmentActivity {
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.ampere.bike.ampere", appContext.getPackageName());
    }
    @Test
    public void useLoginFucntion(){
        assertEquals(LocalSession.getUserInfo(this,KEY_ID),"2");
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
                assertNotEquals(true,false);
    }
}
