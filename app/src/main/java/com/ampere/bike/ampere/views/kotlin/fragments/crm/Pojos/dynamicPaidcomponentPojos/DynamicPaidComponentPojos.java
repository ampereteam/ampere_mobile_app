package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.dynamicPaidcomponentPojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DynamicPaidComponentPojos {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("component")
    @Expose
    private String component;
    @SerializedName("warranty_period")
    @Expose
    private String warrantyPeriod;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getWarrantyPeriod() {
        return warrantyPeriod;
    }

    public void setWarrantyPeriod(String warrantyPeriod) {
        this.warrantyPeriod = warrantyPeriod;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
