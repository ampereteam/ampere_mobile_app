package com.ampere.bike.ampere.views.kotlin.item

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListAdapter
import com.ampere.bike.ampere.model.calendar.ModelDateSelection
import com.ampere.bike.ampere.model.task.TaskUpdateModel
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.DateUtils.DD_MM
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.views.fragment.calendar.ViewEvent.retrofitService
import com.ampere.bike.ampere.views.fragment.enquiry.Enquiry
import com.ampere.bike.ampere.views.kotlin.fragments.ViewEventDetailed
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity
import com.ampere.vehicles.R
import customviews.CustomTextView
import kotlinx.android.synthetic.main.dialog_drop_down.*
import kotlinx.android.synthetic.main.item_crm.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ItemsViewEvent(val context: FragmentActivity, val modelDate: ArrayList<ModelDateSelection>) : RecyclerView.Adapter<ItemsViewEvent.ViewEventHolder>() {
    var mobile: String = "";
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewEventHolder {
        return ViewEventHolder(View.inflate(context.getApplicationContext(), R.layout.item_crm, null));
    }

    override fun getItemCount(): Int {
        return modelDate.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.READ_CALL_LOG)) {
            //Toast.makeText(context, "Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show()
        }
        context.requestPermissions(arrayOf(Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE), Enquiry.LOCATION_PERMISSION_CODE)
    }

    fun isReadLocationAllowed(): Boolean {
        val result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG)
        val result1 = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS)
        val CALL_PHONE = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE)
        return if (result1 == PackageManager.PERMISSION_GRANTED && result == PackageManager.PERMISSION_GRANTED && CALL_PHONE == PackageManager.PERMISSION_GRANTED) {
            true
        } else false
    }

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("MissingPermission")
    override fun onBindViewHolder(holder: ViewEventHolder, position: Int) {


        CustomTextView.marquee(holder.itemTask)
        CustomTextView.marquee(holder.itemName)
        CustomTextView.marquee(holder.itemStatus)
        CustomTextView.marquee(holder.itemModel)


        holder.itemName.visibility = View.VISIBLE
        holder.itemName.text = modelDate[position].customerName
        holder.itemDate.text = DateUtils.convertDates(modelDate[position].assignedDate, DateUtils.YYYY_MM_DD, DD_MM)
        holder.itemDate.visibility = View.VISIBLE

        holder.itemTask.text = modelDate[position].task
        holder.itemStatus.text = modelDate[position].taskStatus


        val isCheck = modelDate[position].isChecked
        if (isCheck) {
            holder.itemStatus.text = modelDate[position].taskStatus
        } else {
            holder.itemStatus.text = modelDate[position].taskStatus
        }

        holder.itemEmail.setOnClickListener {
            val email = Intent(Intent.ACTION_SEND)
            email.putExtra(Intent.EXTRA_EMAIL, modelDate[position].email)
            email.putExtra(Intent.EXTRA_SUBJECT, "Add Default Subject")
            email.putExtra(Intent.EXTRA_TEXT, "Add Default Message")
            email.setType("message/rfc822")
            context.startActivity(Intent.createChooser(email, "Testing Email Sent"))
        }

        holder.itemCall.setOnClickListener {
            mobile = modelDate[position].mobileNo
            if (isReadLocationAllowed()) {
                context.startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mobile)))
            } else {
                requestLocationPermission()
                MessageUtils.showToastMessageLong(context, "Please enable permission in App Settings.")
            }

        }

        holder.snack_view.setOnClickListener {

            val fragment: Fragment = ViewEventDetailed()
            val bundle: Bundle = Bundle()
            bundle.putString("id", modelDate[position].id)
            /*bundle.putString("event_date", modelDate[position].assignedDate)
            bundle.putString("custome_name", modelDate[position].customerName)*/

            bundle.putString("id", "" + modelDate[position].id)
            bundle.putString("name", "" + modelDate[position].customerName)
            bundle.putString("city", "" + modelDate[position].city)
            bundle.putString("mobile_no", "" + modelDate[position].mobileNo)
            bundle.putString("email", "" + modelDate[position].email)
            bundle.putString("lead_source", "" + modelDate[position].leadsource)
            bundle.putString("assign_date", "" + modelDate[position].assignedDate)
            bundle.putString("assigned_to", "" + modelDate[position].assignedTo)
            bundle.putString("enquiry_for", "" + modelDate[position].enquiryFor)
            bundle.putString("enquiry_date", "" + modelDate[position].enquiryDate)
            bundle.putString("enquiry_time", "" + modelDate[position].enquiryTime)
            bundle.putString("assigned_person", "" + modelDate[position].assignedPerson)
            bundle.putString("message", "" + modelDate[position].message)
            bundle.putString("status", "" + modelDate[position].taskStatus)
            bundle.putString("task", "" + modelDate[position].task)
            bundle.putString("vehicle_model", "" + modelDate[position].vehicleModel)

            fragment.arguments = bundle
            //Utils.passFragmentBackStack(context, fragment)


            fragment.arguments = bundle

            val intent = Intent(context, CRMDetailsActivity::class.java)
            intent.putExtra("id", "" + modelDate[position].id);
            intent.putExtra("user_name", "" + modelDate[position].customerName);
            intent.putExtra("task_name", "" + modelDate[position].enquiryFor);
            ContextCompat.startActivity(context, intent, bundle)

        }

        holder.itemStatus.setOnClickListener(View.OnClickListener {

            var statusValues = arrayOf(context.getString(R.string.proposals), context.getString(R.string.negotiation), context.getString(R.string.closure))
            var statusCurrent = modelDate[position].taskStatus
            if (statusCurrent.equals(statusValues[0])) {
                statusValues = arrayOf(context.getString(R.string.negotiation), context.getString(R.string.closure))
            } else if (statusCurrent.equals(statusValues[1])) {
                statusValues = arrayOf(context.getString(R.string.closure))
            }
            if (!statusCurrent.equals("Closure")) {
                var dialog = Dialog(context)
                dialog.setContentView(R.layout.dialog_drop_down)
                var dialogTitle = dialog.status
                dialogTitle.text = "Task Status"
                var dialogList = dialog.dialog_list
                dialog.getWindow().attributes.windowAnimations = R.style.grow

                dialogList.adapter = ArrayAdapter<String>(context, R.layout.spinnertext, R.id.text1, statusValues) as ListAdapter?

                dialogList.onItemClickListener = AdapterView.OnItemClickListener { parent, _view, poi, id ->
                    var statusNew = statusValues[poi]

                    MessageUtils.dismissDialog(dialog);
                    notifyDataSetChanged()
                    var updateTaskDetails = HashMap<String, String>()
                    updateTaskDetails.put("user_name", LocalSession.getUserInfo(context, LocalSession.KEY_NAME))
                    updateTaskDetails.put("mobile_no", LocalSession.getUserInfo(context, LocalSession.KEY_MOBILE))
                    updateTaskDetails.put("id", "" + modelDate[position].id)
                    updateTaskDetails.put("task_status", "" + statusNew)


                    var model = retrofitService.onTaskStatusChange(LocalSession.getUserInfo(context, KEY_TOKEN), updateTaskDetails)
                    dialog = MessageUtils.showDialog(context)
                    dialog.setOnKeyListener { dialog_, _keyCode, event ->
                        MessageUtils.dismissDialog(dialog);
                        false
                    }

                    model.enqueue(object : Callback<TaskUpdateModel> {
                        override fun onResponse(call: Call<TaskUpdateModel>, response: Response<TaskUpdateModel>) {
                            MessageUtils.dismissDialog(dialog);
                            if (response.isSuccessful) {
                                var taskModel = response.body()
                                if (taskModel != null) {
                                    Log.d("aslkalsk", "" + taskModel.isStatus)
                                    if (taskModel.isStatus) {
                                        holder.itemStatus.text = statusNew

                                        val modelDateSelection = ModelDateSelection("" + modelDate[position].id, modelDate[position].customerName, modelDate[position].mobileNo, modelDate[position].city,
                                                modelDate[position].email, modelDate[position].enquiryDate, modelDate[position].enquiryTime, modelDate[position].leadsource, modelDate[position].enquiryFor, modelDate[position].vehicleModel,
                                                modelDate[position].chassisNo, modelDate[position].message, modelDate[position].assignedTo, modelDate[position].assignedPerson, modelDate[position].assignedDate, modelDate[position].task, statusNew,
                                                modelDate[position].createdBy, modelDate[position].updatedAt, true)
                                        modelDate.set(position, modelDateSelection)

                                        notifyDataSetChanged()

                                        MessageUtils.showToastMessage(context, taskModel.message)
                                    } else {
                                        // MessageUtils.showSnackBar(context, holder.snack_view, taskModel.message)
                                        MessageUtils.showToastMessage(context, taskModel.message)
                                    }
                                } else {
                                    // MessageUtils.showSnackBar(context, holder.snack_view, "Not found ")
                                    MessageUtils.showToastMessage(context, "Not Found")
                                }
                            } else {
                                MessageUtils.showToastMessage(context, MessageUtils.showErrorCodeToast(response.code()))
                                //MessageUtils.setErrorMessage(context, holder.snack_view, response.code())
                            }
                        }

                        override fun onFailure(call: Call<TaskUpdateModel>, t: Throwable) {
                            MessageUtils.dismissDialog(dialog);
                            MessageUtils.showToastMessage(context, MessageUtils.showFailureToast(t.message))
                            //MessageUtils.setFailureMessage(context, holder.snack_view, t.message)
                        }
                    })
                }
                dialog.show()
            } else {
                MessageUtils.showToastMessage(context, "Already Status Closed")
            }
        })


    }


    inner class ViewEventHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemName = view.item_crm_name
        val itemDate = view.item_crm_date
        val itemTask = view.item_crm_task
        val itemAction = view.item_crm_action
        val itemStatus = view.item_crm_status
        val itemLayout = view.item_crm
        val itemEmail = view.item_crm_action_email
        val itemCall = view.item_crm_action_call
        val snack_view = view.snack_view
        val itemModel = view.crm_item_model
    }


}