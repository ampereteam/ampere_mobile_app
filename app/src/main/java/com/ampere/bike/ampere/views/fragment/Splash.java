package com.ampere.bike.ampere.views.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.utils.NetworkTester;
import com.ampere.vehicles.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import customviews.CustomButton;
import customviews.CustomTextView;

import static com.ampere.bike.ampere.MyApplication.isVISIABLE;


public class Splash extends Fragment {

    private Handler handler = new Handler();
    private Runnable runnable;
    private Unbinder unbinder;

    @BindView(R.id.splash_layout)
    FrameLayout mSplashLayout;

    @BindView(R.id.update_layout)
    RelativeLayout mUplateLyout;

    @BindView(R.id.update_txt)
    CustomTextView mUpdateTxt;

    @BindView(R.id.update_btn)
    CustomButton mUpdateBtn;

    @BindView(R.id.update_skip_layout)
    RelativeLayout mUpdateSkipLayout;

    @BindView(R.id.update_skip_txt)
    CustomTextView mSkipText;


    @BindView(R.id.skip_btn)
    CustomButton mSkipBtn;

    @BindView(R.id.update_skip_btn)
    CustomButton mUpdateBtn1;
    //LocalSession localSession;

    @BindView(R.id.splash_snack_view)
    ConstraintLayout mSnackView;

    @BindView(R.id.error_msg)
    CustomTextView mErrorMsg;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) getActivity()).disableToolbar();
        return inflater.inflate(R.layout.frag_splash, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        mErrorMsg.setVisibility(View.GONE);

        // Visiable Splash Layout and Gone Update Layout
        //  checkUpdate(View.GONE, View.VISIBLE);

        mUplateLyout.setVisibility(View.GONE);
        mSplashLayout.setVisibility(View.VISIBLE);
    }

    private void checkUpdate(int isfalse, int isTrue) {
        mUplateLyout.setVisibility(isfalse);
        mSplashLayout.setVisibility(isTrue);
    }


    @OnClick(R.id.update_btn)
    public void onUpdate() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.ampere.vehicles"));
        startActivity(intent);
    }


    @OnClick(R.id.update_skip_btn)
    public void onUpdate1() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.ampere.vehicles"));
        startActivity(intent);
    }

    @OnClick(R.id.skip_btn)
    public void onSkip() {

        login();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("Status", "" + isVISIABLE);

        isVISIABLE = true;
        if (NetworkTester.isNetworkAvailable(getActivity())) {

            /*MyUtils.getInstance().onCheckUpdate().enqueue(new Callback<CheckForceUpdate>() {
                @Override
                public void onResponse(@NonNull Call<CheckForceUpdate> call, SpareInvoicePojos<CheckForceUpdate> response) {

                    if (response.isSuccessful()) {
                        try {
                            CheckForceUpdate map = response.body();
                            if (map.mSuccess) {
                                if (map.mData != null) {
                                    String versionCode = map.mData.mVersionCode;
                                    String updateMessage = map.mData.mMessage;

                                    boolean isForce = map.mData.mForceUpdate;
                                    try {
                                        PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
                                        String version = pInfo.versionName;
                                        Log.d("version_checking", isVISIABLE + "  " + version + "  " + Build.DEVICE + "  " + Build.BRAND + "  " + Build.ID + "  " + Build.SERIAL + "  " + BuildConfig.VERSION_NAME + "  " + BuildConfig.VERSION_CODE);

                                        if (version.equals(versionCode)) {
                                            //If Version is same goto home
                                            if (isVISIABLE) {
                                                login();
                                            }

                                        } else {
                                            // isForce = false;

                                            if (isForce) {
                                                mUpdateTxt.setText(updateMessage);
                                                // Visiable Update  Layout and Gone Splash Layout
                                                checkUpdate(View.VISIBLE, View.GONE);
                                            } else {
                                                mSkipText.setText(updateMessage);
                                                // Visiable Splash  Layout and Gone Update Layout
                                                checkUpdate(View.GONE, View.GONE);
                                                // Show Skip Button
                                                mUpdateSkipLayout.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    } catch (PackageManager.NameNotFoundException e) {
                                        e.printStackTrace();
                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    mErrorMsg.setVisibility(View.VISIBLE);
                                    mErrorMsg.setText("Something went wrong");
                                    MessageUtils.showToastMessage(getActivity(), "Something went wrong");

                                }
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Log.d("Later", "Show Laters ");
                        mErrorMsg.setVisibility(View.VISIBLE);
                        mErrorMsg.setText(MessageUtils.showErrorCodeToast(response.code()));
                        MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CheckForceUpdate> call, @NonNull Throwable t) {
                    String msg = MessageUtils.showFailureToast(t.getMessage());
                    if (msg.equals("Failed to connect to Server")) {
                        mErrorMsg.setVisibility(View.VISIBLE);
                        mErrorMsg.setText(msg);
                        MessageUtils.showToastMessage(getActivity(), msg);
                    } else {
                        mErrorMsg.setVisibility(View.GONE);
                        MessageUtils.setFailureMessage(getActivity(), mSnackView, msg);
                    }
                }
            });*/




            login();



          /*  runnable = new Runnable() {
                @Override
                public void run() {

                    //Checking Login if true got home page else Login page
                    if (LocalSession.isLogin(getActivity())) {
                        MyUtils.passFragmentWithoutBackStack(getActivity(), new Home());
                    } else {
                        MyUtils.passFragmentWithoutBackStack(getActivity(), new Login());
                    }

                    //Gone Splash Layout and  Visiable  Update Layout
                    //  checkUpdate(View.VISIBLE, View.GONE);

                }
            };
            handler.postDelayed(runnable, 2000);*/
        } else {
            MessageUtils.showNetworkDialog(getActivity());
        }
    }


    private void login() {
        try {
            if (LocalSession.isLogin(getActivity())) {
                //home
                MyUtils.passFragmentWithoutBackStack(getActivity(), new Home());
            } else {

                //login
                MyUtils.passFragmentWithoutBackStack(getActivity(), new Login());
            }
        } catch (Exception e) {
            e.printStackTrace();
            MyUtils.passFragmentWithoutBackStack(getActivity(), new Home());

        }
    }

    @Override
    public void onPause() {
        super.onPause();
        isVISIABLE = false;
        // handler.removeCallbacks(runnable);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        isVISIABLE = false;
    }
}
