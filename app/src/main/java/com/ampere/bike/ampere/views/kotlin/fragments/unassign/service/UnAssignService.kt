package com.ampere.bike.ampere.views.kotlin.fragments.unassign.service

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.utils.MyUtils.SERIVCE_UN_ASSIGN
import com.ampere.bike.ampere.views.kotlin.fragments.unassign.adapter.ItemUnAssinged
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.frag_list_view.*
import kotlinx.android.synthetic.main.header_un_assign.*
import kotlinx.android.synthetic.main.item_crm_header.*

class UnAssignService : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.frag_list_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        crm_header.visibility = View.GONE


        if (MyUtils.SERIVCE_UN_ASSIGN.size == 0) {
            no_text.visibility = View.VISIBLE
            no_text.text = "No Service"
            list_proposal.visibility = View.GONE
        } else {

            list_proposal.visibility = View.VISIBLE
            list_proposal.layoutManager = GridLayoutManager(activity, 1)
            list_proposal.setHasFixedSize(false)
            list_proposal.adapter = ItemUnAssinged(activity!!, SERIVCE_UN_ASSIGN)
            no_text.visibility = View.GONE
            header_un_ass_layout.visibility = View.VISIBLE
        }
    }
}