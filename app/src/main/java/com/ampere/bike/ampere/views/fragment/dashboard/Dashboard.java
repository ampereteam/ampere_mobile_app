package com.ampere.bike.ampere.views.fragment.dashboard;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.NetworkManager;
import com.ampere.bike.ampere.model.warranty.InvoiceWarranty;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.dashboard.ServerREs.DashboardRes;
import com.ampere.vehicles.R;
import com.google.gson.Gson;

import java.util.HashMap;

import butterknife.ButterKnife;
import customviews.CustomTextView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ampere.bike.ampere.CustomNavigationDuo.snackVIew;
import static com.ampere.bike.ampere.shared.LocalSession.BEARER;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_USER_NAME;

public class Dashboard extends Fragment {


    Common  common ;
    Dialog dialog;
    DashboardRes dashboardRes;
    CustomTextView salesSS1,salesSS2,salesSS3,salesSS4,serviceS1,serviceS2,serviceS3,sparesSS1,sparesSS2,sparesSS3,sparesSS4;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) getActivity()).disbleNavigationView(getActivity().getString(R.string.dash_board));
        return inflater.inflate(R.layout.frag_dashboard, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        common = new Common(getActivity());
        salesSS1 = view.findViewById(R.id.edt_sales_ss1);
        salesSS2 = view.findViewById(R.id.edt_sales_ss2);
        salesSS3 = view.findViewById(R.id.edt_sales_ss3);
        salesSS4 = view.findViewById(R.id.edt_sales_ss4);
        serviceS1 = view.findViewById(R.id.edt_service_s1);
        serviceS2 = view.findViewById(R.id.edt_service_s2);
        serviceS3 = view.findViewById(R.id.edt_service_s3);
        sparesSS1 =view.findViewById(R.id.edt_spares_ss1);
        sparesSS2 =view.findViewById(R.id.edt_spares_ss2);
        sparesSS3 =view.findViewById(R.id.edt_spares_ss3);
        sparesSS4 =view.findViewById(R.id.edt_spares_ss4);

       /* Call<InvoiceWarranty> invoiceWarrantyCall = MyUtils.getRetroService().onInvoiceWarranty(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), LocalSession.getUserInfo(getActivity(), KEY_ID));

        invoiceWarrantyCall.enqueue(new Callback<InvoiceWarranty>() {
            @Override
            public void onResponse(Call<InvoiceWarranty> call, Response<InvoiceWarranty> response) {
                InvoiceWarranty invoiceWarranty = response.body();
                //Log.d("voicesdsd", "" + invoiceWarranty.getData().getWarrantyinvoice().size());
                //Log.d("voiceuds", "" + invoiceWarranty.getData().getWarrantyinvoice().get(0).getVehicleModel());
            }

            @Override
            public void onFailure(Call<InvoiceWarranty> call, Throwable t) {

            }
        });*/
       getDashboardRes();
    }

    private void getDashboardRes() {
        try{
            Log.d("INFOR","\n");
            HashMap<String,Object> hashMap = new HashMap<>();
            hashMap.put("userid",LocalSession.getUserInfo(getActivity(),KEY_ID));
            if(common.isNetworkConnected()){
                dialog =MessageUtils.showDialog(getActivity());
                NetworkManager networkManager = common.connectRetro("POST","dashboard");
                Call<ResponseBody> bodyCall = networkManager.call_post("dashboard","Bearer "+LocalSession.getUserInfo(getActivity(),KEY_TOKEN),hashMap);
                bodyCall.enqueue(new Callback<ResponseBody>() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        MessageUtils.dismissDialog(dialog);
                        if(response.isSuccessful()){
                            try{
                                dashboardRes = new Gson().fromJson(response.body().string(),DashboardRes.class);
                                Log.d("INFOR","\n"+dashboardRes.toString());
                                if(dashboardRes!=null){
                                    if(dashboardRes.getSuccess()){
                                        if(dashboardRes.getData().getSalesSs1Enquiry() != null){
                                            Log.d("INFOR",""+dashboardRes.getData().getSalesSs1Enquiry());
                                            salesSS1 .setText(dashboardRes.getData().getSalesSs1Enquiry().toString());
                                        }
                                        if(dashboardRes.getData().getSalesSs2Enquiry()!=null){
                                            Log.d("INFOR","\n"+dashboardRes.getData().getSalesSs2Enquiry());
                                            salesSS2 .setText(dashboardRes.getData().getSalesSs2Enquiry().toString());
                                        }if(dashboardRes.getData().getSalesSs3Enquiry()!=null){
                                            Log.d("INFOR","\n"+dashboardRes.getData().getSalesSs3Enquiry());
                                            salesSS3 .setText(dashboardRes.getData().getSalesSs3Enquiry().toString());
                                        }if(dashboardRes.getData().getSalesSs4Enquiry()!= null){
                                            Log.d("INFOR","\n"+dashboardRes.getData().getSalesSs4Enquiry());
                                            salesSS4 .setText(dashboardRes.getData().getSalesSs4Enquiry().toString());
                                        }if(dashboardRes.getData().getServiceS1Enquiry()!=null){
                                            serviceS1 .setText(dashboardRes.getData().getServiceS1Enquiry().toString());
                                            Log.d("INFOR","\n"+dashboardRes.getData().getServiceS1Enquiry());
                                        }if(dashboardRes.getData().getServiceS2Enquiry() != null){
                                            serviceS2 .setText(dashboardRes.getData().getServiceS2Enquiry().toString());
                                            Log.d("INFOR","\n"+dashboardRes.getData().getServiceS2Enquiry());
                                        } if(dashboardRes.getData().getServiceS3Enquiry()!=null){
                                            serviceS3.setText(dashboardRes.getData().getServiceS3Enquiry().toString());
                                            Log.d("INFOR","\n"+dashboardRes.getData().getServiceS3Enquiry());
                                        }if(dashboardRes.getData().getSparesSs1Enquiry()!=null){
                                            Log.d("INFOR","\n"+dashboardRes.getData().getSparesSs1Enquiry());
                                            sparesSS1.setText(dashboardRes.getData().getSparesSs1Enquiry().toString());
                                        }if(dashboardRes.getData().getSparesSs2Enquiry()!=null){
                                            Log.d("INFOR","\n"+dashboardRes.getData().getSparesSs2Enquiry());
                                            sparesSS2.setText(dashboardRes.getData().getSparesSs2Enquiry().toString());
                                        }if(dashboardRes.getData().getSparesSs3Enquiry()!=null){
                                            Log.d("INFOR","\n"+dashboardRes.getData().getSparesSs3Enquiry());
                                            sparesSS3.setText(dashboardRes.getData().getSparesSs3Enquiry().toString());
                                        }if(dashboardRes.getData().getSparesSs4Enquiry()!= null) {
                                            Log.d("INFOR","\n"+dashboardRes.getData().getSparesSs4Enquiry());
                                            sparesSS4.setText(dashboardRes.getData().getSparesSs4Enquiry().toString());
                                        }
                                    }else{
                                        MessageUtils.showSnackBar(getActivity(),snackVIew,dashboardRes.getMessage());
                                    }
                                }
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }
                        }else{
                            MessageUtils.showSnackBar(getActivity(),snackVIew,response.message());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    MessageUtils.dismissDialog(dialog);
                    MessageUtils.showSnackBar(getActivity(),snackVIew,t.getLocalizedMessage());
                    }
                });

            }else{
                MessageUtils.showSnackBarAction(getActivity(),snackVIew,getString(R.string.check_internet));
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
