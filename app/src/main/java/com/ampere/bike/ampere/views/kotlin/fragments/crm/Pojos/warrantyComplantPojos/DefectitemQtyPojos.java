package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.warrantyComplantPojos;

import com.google.gson.annotations.SerializedName;

public class DefectitemQtyPojos {
    @SerializedName("defect_item")
    private
    String defectitem;
    @SerializedName("defect_qty")
    private
    String qty;

    public DefectitemQtyPojos(String defectitem, String qty) {
        this.defectitem = defectitem;
        this.qty = qty;
    }

    public String getDefectitem() {
        return defectitem;
    }

    public void setDefectitem(String defectitem) {
        this.defectitem = defectitem;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }
}
