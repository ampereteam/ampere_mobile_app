package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.warrantyComplantPojos.DefectitemQtyPojos
import com.ampere.vehicles.R
import customviews.CustomTextEditView
import java.lang.Exception

class SpareItemQtyAddmoreAdapter(val context: Context, val spareItemQtyList:ArrayList<DefectitemQtyPojos>, val configClickEvent: ConfigClickEvent)
: RecyclerView.Adapter<SpareItemQtyAddmoreAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpareItemQtyAddmoreAdapter.ViewHolder {
        val view: View = LayoutInflater.from(this.context).inflate(R.layout.adapter_crm_service_frr_spareitem_addmore, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.spareItemQtyList.size
    }

    override fun onBindViewHolder(holder: SpareItemQtyAddmoreAdapter.ViewHolder, position: Int) {
        try {
            holder.defectitem.setText(this.spareItemQtyList.get(position).defectitem)
            holder.itemQty.setText(this.spareItemQtyList.get(position).qty)
            holder.imgBtn.setOnClickListener {
                try {
                    configClickEvent.connectposition(position, 100)
                    this.spareItemQtyList.removeAt(position)
                    notifyDataSetChanged()
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val defectitem: CustomTextEditView = view.findViewById(R.id.ffr_edt_defect_item_adptr)
        val itemQty: CustomTextEditView = view.findViewById(R.id.ffr_edt_defect_qty_adptr)
        val imgBtn: ImageButton = view.findViewById(R.id.ffr_imgbtn_remove_adptr)
    }
}