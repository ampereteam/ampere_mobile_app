package com.ampere.bike.ampere.model.calendar;

import com.google.gson.annotations.SerializedName;

//@Generated("com.robohorse.robopojogenerator")
public class EnquirydetailsItem{

	@SerializedName("created_time")
	public String createdTime;

	@SerializedName("task_status")
	public String taskStatus;

	@SerializedName("assigned_date")
	public String assignedDate;

	@SerializedName("city")
	public String city;

	@SerializedName("mobile_no")
	public String mobileNo;

	@SerializedName("vehicle_model")
	public String vehicleModel;

	@SerializedName("created_at")
	public String createdAt;

	@SerializedName("message")
	public String message;

	@SerializedName("created_by")
	public String createdBy;

	@SerializedName("enquiry_time")
	public String enquiryTime;

	@SerializedName("task")
	public String task;

	@SerializedName("updated_at")
	public String updatedAt;

	@SerializedName("chassis_no")
	public String chassisNo;

	@SerializedName("enquiry_date")
	public String enquiryDate;

	@SerializedName("enquiry_for")
	public String enquiryFor;

	@SerializedName("leadsource")
	public String leadsource;

	@SerializedName("id")
	public int id;

	@SerializedName("customer_name")
	public String customerName;

	@SerializedName("email")
	public String email;

	@SerializedName("assigned_to")
	public String assignedTo;

	@SerializedName("assigned_person")
	public String assignedPerson;
/*
	@Override
 	public String toString(){
		return 
			"EnquirydetailsItem{" + 
			"created_time = '" + createdTime + '\'' + 
			",task_status = '" + taskStatus + '\'' + 
			",assigned_date = '" + assignedDate + '\'' + 
			",city = '" + city + '\'' + 
			",mobile_no = '" + mobileNo + '\'' + 
			",sendThrough = '" + vehicleModel + '\'' +
			",created_at = '" + createdAt + '\'' + 
			",message = '" + message + '\'' + 
			",created_by = '" + createdBy + '\'' + 
			",enquiry_time = '" + enquiryTime + '\'' + 
			",task = '" + task + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",chassis_no = '" + chassisNo + '\'' + 
			",enquiry_date = '" + enquiryDate + '\'' + 
			",enquiry_for = '" + enquiryFor + '\'' + 
			",leadsource = '" + leadsource + '\'' + 
			",id = '" + id + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			",email = '" + email + '\'' + 
			",assigned_to = '" + assignedTo + '\'' + 
			",assigned_person = '" + assignedPerson + '\'' + 
			"}";
		}*/
}