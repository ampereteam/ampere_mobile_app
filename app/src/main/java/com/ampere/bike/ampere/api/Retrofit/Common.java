package com.ampere.bike.ampere.api.Retrofit;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.widget.Toast;

import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.vehicles.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Common {
    boolean success;
    String message;
    Dialog dialog;
    FragmentActivity activity;
    ResponsebackToClass responsebackToClass;
    ResponseObjectsBackToClass responseObjectsBackToClass;
//SessionManager sessionManager ;

    public Common(FragmentActivity activity, ResponsebackToClass responsebackToClass, ResponseObjectsBackToClass responseObjectsBackToClass) {
        this.activity = activity;
        this.responsebackToClass = responsebackToClass;
        this.responseObjectsBackToClass = responseObjectsBackToClass;
    }

    public Common(FragmentActivity activity, ResponseObjectsBackToClass responseObjectsBackToClass) {
        this.activity = activity;
        this.responseObjectsBackToClass = responseObjectsBackToClass;
    }

    public Common(FragmentActivity activity, ResponsebackToClass responsebackToClass) {
        this.activity = activity;
        this.responsebackToClass = responsebackToClass;
    }

    public Common(FragmentActivity activity) {
        this.activity = activity;
    }

    public NetworkManager connectRetro(String annonationType, String method) {
        String appUrl = null;
        System.out.println("CHECK_DATA ANNONTAION TYPE :" + annonationType + " & method :" + method);
        if (annonationType.equals("POST")) {
            System.out.println("CHECK_DATA INSIDE APP URL :" + method);

            appUrl = activity.getResources().getString(R.string.appURL);
        }
        final OkHttpClient objOkHttpClient = new OkHttpClient.Builder().readTimeout(1, TimeUnit.MINUTES).connectTimeout(1, TimeUnit.MINUTES).addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
        Retrofit objRetrofit = new Retrofit.Builder()
                .client(objOkHttpClient)
                .baseUrl(appUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return objRetrofit.create(NetworkManager.class);
    }


    public void callApiRequest(final String apiName1, final String callType, Map<String, String> map, final int position) { // Receive call from class and return values from API
        Call<ResponseBody> objCallApi = null;
        //sessionManager = new SessionManager(activity);
        if (LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN) != null) {
            if (LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN).length() > 12) {
                if (callType.equals("POST")) {
                    if (map == null) {
                        objCallApi = connectRetro(callType, apiName1).call_post(apiName1);
                    } else {
                        objCallApi = connectRetro(callType, apiName1).call_post(apiName1, "Bearer " + LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN), map);
                    }
                }
            } else {
                if (callType.equals("POST")) {
                    if (map == null) {
                        objCallApi = connectRetro(callType, apiName1).call_post(apiName1);
                    } else {
                        objCallApi = connectRetro(callType, apiName1).call_post(apiName1, map);
                    }
                }
            }

        } else {
            if (callType.equals("POST")) {
                if (map == null) {
                    objCallApi = connectRetro(callType, apiName1).call_post(apiName1);
                } else {
                    System.out.println("Token TYpe is Missing");
                    objCallApi = connectRetro(callType, apiName1).call_post(apiName1, map);
                }
            }
        }
         final Dialog dialogProgress = MessageUtils.showDialog(activity);
         final Call<ResponseBody> finalObjCallApi = objCallApi;
        dialogProgress.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                dialogInterface.dismiss();
                finalObjCallApi.cancel();
                return false;
            }
        });


        objCallApi.enqueue(new Callback<ResponseBody>() {
            JSONObject data;

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println("CHECK_DATA response :" + response.isSuccessful());
                MessageUtils.dismissDialog(dialogProgress);
                if (response.isSuccessful()) {
                    try {
                        JSONObject objJsonObject = new JSONObject(response.body().string());
                        System.out.println("WarrantyInvoice:" + apiName1 + "---" + objJsonObject.toString());
                        success = objJsonObject.getBoolean("success");
                        data = objJsonObject.getJSONObject("data");
                        message = objJsonObject.getString("message");
                        System.out.println("response   message for success   :" + data);
                        if (success) {
                            /*if (apiName1.equals("login")) {
                                JSONObject sonElements = data.getJSONObject("userdetail");
                                sessionManager.save("username", sonElements.getString("username"));
                                sessionManager.save("token", data.getString("token"));
                                sessionManager.save("id", sonElements.getString("id"));
                                sessionManager.save("name", sonElements.getString("name"));
                                sessionManager.save("email", sonElements.getString("email"));
                                sessionManager.save("mobile", sonElements.getString("mobile"));
                                sessionManager.save("usertype", sonElements.getString("user_type"));
                                //activity.startActivity(new Intent(activity, HomePage.class));
                            } else */
                            if (apiName1.equals("warrantycomplaints")) {
                                JSONArray object = data.getJSONArray("warrantycomplaints");
                                // System.out.println("KFLgh     Data" +object);
                                responsebackToClass.apiCallBackOverRideMethod(response, object, apiName1, position, message, success);
                            } else if (apiName1.equals("warrantyinvoice")) {
                                JSONArray object = data.getJSONArray("warrantyinvoice");
                                responsebackToClass.apiCallBackOverRideMethod(response, object, apiName1, position, message, success);
                            } else if (apiName1.equals("defectreturn")) {
                                JSONArray object = data.getJSONArray("defectreturnlist");
                                responsebackToClass.apiCallBackOverRideMethod(response, object, apiName1, position, message, success);
                            } else if (apiName1.equals("servicereturn")) {
                                JSONArray object = data.getJSONArray("defectreturnlist");
                                responsebackToClass.apiCallBackOverRideMethod(response, object, apiName1, position, message, success);
                            } else if (apiName1.equals("sendforservice")) {
                                responsebackToClass.apiCallBackOverRideMethod(response, null, apiName1, position, message, success);
                            } else if (apiName1.equals("vehicledelivered")) {
                                JSONArray jsonArray = data.getJSONArray("vehicles");
                                System.out.println("Json Array" + jsonArray.toString());
                                responsebackToClass.apiCallBackOverRideMethod(response, jsonArray, apiName1, position, message, success);
                            } else if (apiName1.equals("sparesdispatched")) {
                                JSONArray jsonArray = data.getJSONArray("spares");
                                System.out.println("Json Array" + jsonArray.toString());
                                responsebackToClass.apiCallBackOverRideMethod(response, jsonArray, apiName1, position, message, success);
                            } else if (apiName1.equals("sparestock")) {
                                JSONArray jsonArray = data.getJSONArray("spares");
                                System.out.println("Json Array" + jsonArray.toString());
                                responsebackToClass.apiCallBackOverRideMethod(response, jsonArray, apiName1, position, message, success);
                            } else if (apiName1.equals("vehiclestock")) {
                                JSONArray jsonArray = data.getJSONArray("vehicles");
                                System.out.println("Json Array" + jsonArray.toString());
                                responsebackToClass.apiCallBackOverRideMethod(response, jsonArray, apiName1, position, message, success);
                            } else if (apiName1.equals("dealerindent")) {
                                JSONArray jsonArray = data.getJSONArray("indents");
                                System.out.println("Json Array" + jsonArray.toString());
                                responsebackToClass.apiCallBackOverRideMethod(response, jsonArray, apiName1, position, message, success);
                            } else if (apiName1.equals("indentdetail")) {
                                JSONArray jsonArray = data.getJSONArray("indentdetails");
                                responsebackToClass.apiCallBackOverRideMethod(response, jsonArray, apiName1, position, message, success);
                            } else if (apiName1.equals("dealersales")) {
                                JSONArray jsonArray = data.getJSONArray("sales");
                                responsebackToClass.apiCallBackOverRideMethod(response, jsonArray, apiName1, position, message, success);
                            }
                        } else {
                            if (apiName1.equals("warrantycomplaintlists")) {
                                JSONArray object = data.getJSONArray("warrantycomplaints");
                                responsebackToClass.apiCallBackOverRideMethod(response, object, apiName1, position, message, success);
                            } else if (apiName1.equals("warrantyinvoice")) {
                                JSONArray object = data.getJSONArray("warrantyinvoice");
                                responsebackToClass.apiCallBackOverRideMethod(response, object, apiName1, position, message, success);
                            } else if (apiName1.equals("defectreturn")) {
                                JSONArray object = data.getJSONArray("defectreturnlist");
                                responsebackToClass.apiCallBackOverRideMethod(response, object, apiName1, position, message, success);
                            } else if (apiName1.equals("servicereturn")) {
                                JSONArray object = data.getJSONArray("defectreturnlist");
                                responsebackToClass.apiCallBackOverRideMethod(response, object, apiName1, position, message, success);
                            } else if (apiName1.equals("sendforservice")) {
                                responsebackToClass.apiCallBackOverRideMethod(response, null, apiName1, position, message, success);
                            } else if (apiName1.equals("sparesdispatched")) {
                                responsebackToClass.apiCallBackOverRideMethod(response, null, apiName1, position, message, success);
                            } else {
                                responsebackToClass.apiCallBackOverRideMethod(response, null, apiName1, position, message, success);
                            }
                        }
                    } catch (JSONException e) {
                        dialog.dismiss();
                        Toast.makeText(activity, "Server Not Found", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    } catch (IOException e) {
                        Toast.makeText(activity, "Server Not Found", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        e.printStackTrace();
                    }
                } else {
                    try {
                        responsebackToClass.apiCallBackOverRideMethod(null, null, null, position, message, success);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    MessageUtils.dismissDialog(dialogProgress);
                    Toast.makeText(activity, "Server NOT Found", Toast.LENGTH_LONG).show();
                    hideLoad();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("WarrantyInvoice RITRO Failure:" + apiName1 + "---" + t.toString());
                try {
                    MessageUtils.dismissDialog(dialogProgress);
                    try {
                        responsebackToClass.apiCallBackOverRideMethod(null, null, apiName1, position, message, success);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public void callApiReques(final String apiName1, final String callType, HashMap<String, Object> map, final int position) {
        Call<ResponseBody> objCallApi = null;
        //sessionManager = new SessionManager(activity);
        if (LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN) != null) {
            if (LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN).length() > 12) {
                if (callType.equals("POST")) {
                    if (map == null) {
                        objCallApi = connectRetro(callType, apiName1).call_post(apiName1);
                    } else {
                        objCallApi = connectRetro(callType, apiName1).call_post(apiName1, "Bearer " + LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN), map);
                    }
                }
            }
        }
        final Dialog dialogProgress = MessageUtils.showDialog(activity);
        final Call<ResponseBody> finalObjCallApi = objCallApi;
        dialogProgress.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                dialogInterface.dismiss();
                finalObjCallApi.cancel();
                return false;
            }
        });


        objCallApi.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.isSuccessful()) {
                        JSONObject object = new JSONObject(response.body().string());
                        message = object.getString("message");
                        success = object.getBoolean("success");
                        JSONObject jsonObject = object.getJSONObject("data");
                        if (success) {
                            responsebackToClass.apiCallBackOverRideMethod(response, null, apiName1, position, message, success);
                        } else {
                            responsebackToClass.apiCallBackOverRideMethod(response, null, apiName1, position, message, success);
                        }


                    } else {
                        try {
                            responsebackToClass.apiCallBackOverRideMethod(response, null, apiName1, position, message, success);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        MessageUtils.dismissDialog(dialogProgress);
                        hideLoad();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideLoad();
            }
        });
    }


    public void callApiRequestObjects(final String apiName1, final String callType, HashMap<String, Object> map, final int position) {
        Call<ResponseBody> objCallApi = null;
        //sessionManager = new SessionManager(activity);
        if (LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN) != null) {
            if (LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN).length() > 12) {
                if (callType.equals("POST")) {
                    if (map == null) {
                        objCallApi = connectRetro(callType, apiName1).call_post(apiName1);
                    } else {
                        objCallApi = connectRetro(callType, apiName1).call_post(apiName1, "Bearer " + LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN), map);
                    }
                }
            }
        }
        final Dialog dialogProgress = MessageUtils.showDialog(activity);
        final Call<ResponseBody> finalObjCallApi = objCallApi;
        dialogProgress.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                dialogInterface.dismiss();
                finalObjCallApi.cancel();
                return false;
            }
        });


        objCallApi.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        MessageUtils.dismissDialog(dialogProgress);
                        hideLoad();
                        JSONObject object = new JSONObject(response.body().string());
                        message = object.getString("message");
                        success = object.getBoolean("success");
                        JSONObject jsonObject = object.getJSONObject("data");
                        if (success) {
                            responseObjectsBackToClass.responseObjects(jsonObject, apiName1, position, message, success);
                        } else {
                            responseObjectsBackToClass.responseObjects(jsonObject, apiName1, position, message, success);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } else {
                    try {
                        responseObjectsBackToClass.responseObjects(null, null, 0, null, false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    MessageUtils.dismissDialog(dialogProgress);
                    hideLoad();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                MessageUtils.dismissDialog(dialogProgress);
                hideLoad();
            }
        });
    }

    public void showLoad(Boolean cancelable) {
        dialog = MessageUtils.showDialog(activity);
       /* dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (cancelable) {
            dialog.setCancelable(true);
        } else {
            dialog.setCancelable(false);
        }
        dialog.setContentView(R.layout.loading);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        dialog.show();*/
    }

    public void hideLoad() {
        /*if (dialog != null && !activity.isFinishing()) {
            dialog.dismiss();
        }*/
        MessageUtils.dismissDialog(dialog);
    }
}
