package com.ampere.bike.ampere.views.fragment.parthi.Inventory.SpareStock.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ampere.bike.ampere.views.fragment.parthi.Inventory.SpareStock.Pojos.SpareStockPojos;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class SpareStockAdapter extends RecyclerView.Adapter<SpareStockAdapter.ViewHolder> {
    ArrayList<SpareStockPojos> spareStockPojosArrayList;
    Context context;

    public SpareStockAdapter(ArrayList<SpareStockPojos> spareStockPojosArrayList, Context context) {
        this.spareStockPojosArrayList = spareStockPojosArrayList;
        this.context = context;
    }

    public void setList(ArrayList<SpareStockPojos> spareStockPojosArrayList) {
        this.spareStockPojosArrayList = spareStockPojosArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_inventory_spare_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.srlNo.setText("" + (position + 1));
        if(spareStockPojosArrayList.get(position).getId() == null ){
            holder.spareSerialNumber.setText("-");
        }else{
            holder.spareSerialNumber.setText(String.valueOf(spareStockPojosArrayList.get(position).getSerialNo()));
            holder.spareSerialNumber.setSingleLine();
            holder.spareSerialNumber.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.spareSerialNumber.setSelected(true);
        }

        if(spareStockPojosArrayList.get(position).getVehicleModel() == null ||
                spareStockPojosArrayList.get(position).getVehicleModel().isEmpty()){
            holder.spareVehicleModel.setText("-");
        }else{
            holder.spareVehicleModel.setText(spareStockPojosArrayList.get(position).getVehicleModel());
            holder.spareVehicleModel.setSingleLine();
            holder.spareVehicleModel.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.spareVehicleModel.setSelected(true);
        }

        if(spareStockPojosArrayList.get(position).getSpareName() == null || spareStockPojosArrayList.get(position).getSpareName().isEmpty()){
            holder.spareName.setText("-");
        }else{
            holder.spareName.setText(spareStockPojosArrayList.get(position).getSpareName());
            holder.spareName.setSingleLine();
            holder.spareName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.spareName.setSelected(true);
        }

        if(spareStockPojosArrayList.get(position).getIndentInvoiceDate() == null || spareStockPojosArrayList.get(position).getIndentInvoiceDate().isEmpty()){
            holder.spareInvoiceDate.setText("-");
        }else{
            holder.spareInvoiceDate.setText(spareStockPojosArrayList.get(position).getIndentInvoiceDate());
            holder.spareInvoiceDate.setSingleLine();
            holder.spareInvoiceDate.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.spareInvoiceDate.setSelected(true);
        }

        if(spareStockPojosArrayList.get(position).getStatus() == null || spareStockPojosArrayList.get(position).getStatus().isEmpty()){
            holder.spareStatus.setText("-");
        }else{
            holder.spareStatus.setText(spareStockPojosArrayList.get(position).getStatus());
            holder.spareStatus.setSingleLine();
            holder.spareStatus.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.spareStatus.setSelected(true);
        }


    }

    @Override
    public int getItemCount() {
        return spareStockPojosArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView srlNo, spareSerialNumber, spareName, spareInvoiceDate, spareStatus, spareVehicleModel;

        public ViewHolder(View itemView) {
            super(itemView);
            srlNo = itemView.findViewById(R.id.srl_NUM);
            spareSerialNumber = itemView.findViewById(R.id.spare_id);
            spareName = itemView.findViewById(R.id.spare_name);
            spareInvoiceDate = itemView.findViewById(R.id.spare_invoice_date);
            spareStatus = itemView.findViewById(R.id.spare_status);
            spareVehicleModel = itemView.findViewById(R.id.spare_variant);
        }
    }
}
