package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoicePojos

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class SpareinventoryItem(

	@field:SerializedName("delivered_by")
	val deliveredBy: Int? = null,

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("vehicle_model")
	val vehicleModel: String? = null,

	@field:SerializedName("delivery_indent_id")
	val deliveryIndentId: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("indent_id")
	val indentId: String? = null,

	@field:SerializedName("acceptance")
	val acceptance: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("manufacture_date")
	val manufactureDate: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("qty")
	val qty: String? = null,

	@field:SerializedName("invoice_type")
	val invoiceType: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("spare_name")
	val spareName: String? = null,

	@field:SerializedName("serial_no")
	val serialNo: String? = null,

	@field:SerializedName("indent_invoice_date")
	val indentInvoiceDate: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)