package com.ampere.bike.ampere.views.kotlin.model

data class MyEnquiryModel(
        val success: Boolean,
        val data: List<Data>,
        val message: String
)