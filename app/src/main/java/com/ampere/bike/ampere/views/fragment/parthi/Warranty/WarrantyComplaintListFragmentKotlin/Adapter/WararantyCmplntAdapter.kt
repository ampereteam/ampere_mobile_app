package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaintListFragmentKotlin.Adapter



import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.api.api_new.ConfigPosition
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.DateUtils.*
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaintListFragmentKotlin.Pojos.WarrantycomplaintsItem
import com.ampere.vehicles.R
import java.lang.Exception

class WararantyCmplntAdapter (val context: FragmentActivity, val warntyCmplntList: ArrayList<WarrantycomplaintsItem?>?,val configClickEvent: ConfigPosition)
    :RecyclerView.Adapter<WararantyCmplntAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):  WararantyCmplntAdapter.ViewHolder {
        val view :View = LayoutInflater.from(this.context).inflate(R.layout.adapter_warranty_complaints_list_kotlin,parent,false)
        return  ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return  this.warntyCmplntList?.size!!
    }

    override fun onBindViewHolder(holder: WararantyCmplntAdapter.ViewHolder, position: Int) {
        try{
            holder.chassisNo.setText(warntyCmplntList?.get(position)?.chassisNo)
            holder.chassisNo.setSingleLine()
            holder.chassisNo.ellipsize=TextUtils.TruncateAt.MARQUEE
            holder.chassisNo.isSelected =true
            holder.sentDate.setText(DateUtils.convertDates(warntyCmplntList?.get(position)?.sentDate,YYYY_MM_DD,DD_MM))
            holder.status.setText(warntyCmplntList?.get(position)?.status)
            holder.chassisNo.setSingleLine()
            holder.chassisNo.ellipsize=TextUtils.TruncateAt.MARQUEE
            holder.chassisNo.isSelected =true
            holder.complaints.setText(warntyCmplntList?.get(position)?.complaint)
            holder.complaints.setSingleLine()
            holder.complaints.isSelected =true
            holder.complaints.ellipsize = TextUtils.TruncateAt.MARQUEE
            holder.srNo.setText((position+1).toString())
            holder.rlayout.setOnClickListener {
                Log.d("OnclickFunction",""+this.warntyCmplntList?.size)
                this.configClickEvent.callinterfacePosition(position)
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    class ViewHolder(view :View ): RecyclerView.ViewHolder(view) {
        val srNo:TextView = view.findViewById(R.id.txv_srl_no)
        val chassisNo:TextView = view.findViewById(R.id.txv_chassis_no)
        val complaints:TextView = view.findViewById(R.id.txv_complaints)
        val sentDate :TextView = view.findViewById(R.id.txv_sent_date)
        val status :TextView = view.findViewById(R.id.txv_Warranty_sts)
        val rlayout: LinearLayout = view.findViewById(R.id.constrn_main)

    }
}