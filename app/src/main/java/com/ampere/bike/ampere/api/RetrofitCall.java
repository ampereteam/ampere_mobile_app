package com.ampere.bike.ampere.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by AND I5 on 10/27/17.
 */


public class RetrofitCall {
    private static Retrofit retrofit = null;
    //http://13.126.73.147/api/indent
    //static String BASE_URL = "http://13.126.73.147/";
    //static String BASE_URL = "http://192.168.2.54:3000/";
//    public static String BASE_URL = "http://192.168.2.54:8000/api/dealer/";
//      public static String BASE_URL = "http://192.168.2.9:9000/api/dealer/";
//    public static String BASE_URL = "http://18.221.70.29/api/dealer/";
    public static String BASE_URL = "http://18.221.70.29/api/dealer/";


    private static OkHttpClient buildClient() {
        return new OkHttpClient
                .Builder()
                .retryOnConnectionFailure(true)
                .connectTimeout(100, TimeUnit.SECONDS)
                .writeTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }


    public static Retrofit getClient() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(buildClient())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

}
