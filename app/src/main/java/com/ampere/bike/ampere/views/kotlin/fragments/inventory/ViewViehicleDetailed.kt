package com.ampere.bike.ampere.views.kotlin.fragments.inventory

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.frag_view_inventory_info.*

class ViewViehicleDetailed : Fragment() {
    companion object {
        var mBottomLay: LinearLayout? = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        (activity as CustomNavigationDuo).disbleNavigationView("Vehicle Info")
        return inflater.inflate(R.layout.frag_view_inventory_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBottomLay = view.findViewById(R.id.linearLayout4)

        inventory_info_no_data.text = "Vehicle Info Not Found"

        var ids = arguments!!.getString("id");
        var id_values = arguments!!.getString("id_values")
        Log.d("idssdsds", "" + ids + "  id_values  " + id_values)
        //Vehicle_despatched
        if (id_values.equals("Vehicle_despatched")) {
            Log.d("id_for", "vehicle_despatched")
            linearLayout4.visibility = View.VISIBLE
        } else {
            linearLayout4.visibility = View.GONE
            Log.d("id_for", "Vehicle_delivery")
        }

        view_invent_cancel.setOnClickListener {
            activity!!.onBackPressed()
        }
        view_invent_accept.setOnClickListener {
            Log.d("api_called", "Is_loading")
        }
    }
}

