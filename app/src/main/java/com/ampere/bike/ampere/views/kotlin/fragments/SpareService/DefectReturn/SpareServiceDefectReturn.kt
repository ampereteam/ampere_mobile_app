package com.ampere.bike.ampere.views.kotlin.fragments.SpareService.DefectReturn


import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.os.StrictMode
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.DefectReturn.Adapter.DefectRetunAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.DefectReturn.Pojos.DefectItem
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.DefectReturn.Pojos.DefectRESList

import com.ampere.vehicles.R
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SpareServiceDefectReturn : Fragment(),ConfigClickEvent {
    var recydefectlist: RecyclerView?= null
    var acceptlinear: LinearLayout?= null
    var btnAcept: Button?= null
    var emptyList: TextView?= null
    var checkbox: CheckBox?= null
    var callApi: UtilsCallApi?=null
    var dialog: Dialog?=null
    var defectresList: DefectRESList?=null
    var defectListAdapter: DefectRetunAdapter?=null

    /**
     * Send  TO Plant Popup Finctionality
     */
    var dialogSend : AlertDialog?= null
    var dialogSendBuilder : AlertDialog.Builder?= null
    var complaintId:String =""
    var spSendThrough :Spinner ?= null
    var edtDocketNo :EditText ?= null
    var edtVide :EditText ?= null
    var edtDriverName:EditText ?= null
    var edtMobileNo :EditText ? =null
    var btnclose :Button ?= null
    var btnsend:Button ?=null
    var liCourier:LinearLayout ?=null
    var dialogsendToPlantview: View?=null
    /** SPinner String ArrayList and Array Adapter***/
    var sendThroughSTRList:ArrayList<String>  = ArrayList()
    var sendThroughSTRAdapter:ArrayAdapter<String> ?=null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =inflater.inflate(R.layout.fragment_spare_service_defect_return, container, false)
        var newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callApi = UtilsCallApi(activity)
        dialog =Dialog(activity)
        dialogSendBuilder =AlertDialog.Builder(activity)
        recydefectlist = view.findViewById(R.id.recycle_spsdr_defectreturn_list)
        emptyList = view.findViewById(R.id.spsdr_emptyList)
        checkbox = view.findViewById(R.id.checkbox_spsdr)
        acceptlinear = view.findViewById(R.id.spsdr_accept)
        btnAcept = view.findViewById(R.id.spsdr_acpt)
        getDataFrmSrvr()
        checkbox?.setOnCheckedChangeListener { buttonView, isChecked ->
            try{
                if(isChecked){
                    if(defectresList?.data?.defectreturnlist?.size!!>0){
                        for(i :Int in 0..defectresList?.data?.defectreturnlist?.size!!-1){
                            defectresList?.data?.defectreturnlist?.get(i)?.check =true
                        }
                        acceptlinear?.visibility =View.VISIBLE
                        btnAcept?.text="Accept("+defectresList?.data?.defectreturnlist?.size!!+")"
                    }
                }else{
                    if(defectresList?.data?.defectreturnlist?.size!!>0) {
                        for (i: Int in 0..defectresList?.data?.defectreturnlist?.size!! - 1) {
                            defectresList?.data?.defectreturnlist?.get(i)?.check = false
                        }
                        acceptlinear?.visibility =View.GONE
                    }
                }
                defectListAdapter?.notifyDataSetChanged()
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }
        btnAcept?.setOnClickListener {
            try{
                val acceptList:ArrayList<DefectItem> = ArrayList()
                for(i:Int in 0 .. defectresList?.data?.defectreturnlist?.size!!-1){
                    val acceptItem = DefectItem(defectresList?.data?.defectreturnlist?.get(i)?.id.toString())
                    acceptList.add(acceptItem)
                }
                if(acceptList.size<=0){
                    Log.d("defectList",""+acceptList.size)
                }else{
                    Log.d("defectList",""+acceptList.size)
                    popupSendToPlant(acceptList)
                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }
    }

    private fun popupSendToPlant(acceptList: ArrayList<DefectItem>) {
        Log.d("defectList",""+acceptList.size)

            try{
                try {
                    val inflater: LayoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                  dialogsendToPlantview = inflater.inflate(R.layout.dialog_spare_service_complaint, null)
                    dialogSendBuilder?.setView(dialogsendToPlantview)
                    dialogSend = dialogSendBuilder?.create()
                    dialogSend?.show()
                    spSendThrough = dialogsendToPlantview?.findViewById<Spinner>(R.id.spsc_sendthrough)
                    edtDocketNo = dialogsendToPlantview?.findViewById(R.id.edt_spsc_docket)
                    liCourier = dialogsendToPlantview?.findViewById(R.id.li_spsc_courier)
                    edtDriverName = dialogsendToPlantview?.findViewById(R.id.edt_spsc_dr_name)
                    edtMobileNo = dialogsendToPlantview?.findViewById(R.id.edt_spsc_mob_no)
                    edtVide = dialogsendToPlantview?.findViewById(R.id.edt_spsc_vide)
                    btnclose = dialogsendToPlantview?.findViewById(R.id.btn_spsc_close)
                    btnsend = dialogsendToPlantview?.findViewById(R.id.btn_spsc_snd_plnt)
                    sendThroughSTRList.clear()
                    sendThroughSTRList.add(getString(R.string.send_through))
                    sendThroughSTRList.add(getString(R.string.courier))
                    sendThroughSTRList.add(getString(R.string.direct))
                    sendThroughSTRList.add(getString(R.string.transport))
                    sendThroughSTRAdapter = ArrayAdapter(activity, R.layout.spinnertext, R.id.text1, sendThroughSTRList)
                    spSendThrough?.adapter = sendThroughSTRAdapter
                    btnclose?.setOnClickListener {
                        if (dialogSend?.isShowing!!) {
                            dialogSend?.dismiss()
                        } else {
                            try {
                                dialogSend?.dismiss()
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        }
                    }
                    btnsend?.setOnClickListener {
                        try {
                            val sendthough = spSendThrough?.selectedItem.toString()
                            val docketno = edtDocketNo?.text.toString()
                            val drivername = edtDriverName?.text.toString()
                            val mobileno = edtMobileNo?.text.toString()
                            val vide = edtVide?.text.toString()
                            val map: HashMap<String, Any> = HashMap()
                            if (sendthough.equals(getString(R.string.direct))) {
                                liCourier?.visibility=View.GONE
                                map.put("sent_type",sendthough)
                                map.put("docket", "")
                                map.put("vide", "")
                                map.put("driver_phone_no", "")
                                map.put("driver_name", "")
                                senTOsrvr(map,acceptList)
                            } else {
                                liCourier?.visibility=View.VISIBLE
                                if(sendthough.equals(getString(R.string.transport))){
                                    if (validatetrsn(sendthough, drivername, mobileno)) {
                                        map.put("sent_type", sendthough)
                                        map.put("docket", docketno)
                                        map.put("vide", vide)
                                        map.put("driver_phone_no",mobileno )
                                        map.put("driver_name", drivername)
                                        senTOsrvr(map, acceptList)
                                    } else {
                                        Log.d("currentState", "" + isStateSaved)
                                    }
                                }else if (sendthough.equals(getString(R.string.courier))) {
                                    if (validate(sendthough, docketno, vide)) {
                                        map.put("sent_type", sendthough)
                                        map.put("docket", docketno)
                                        map.put("vide", vide)
                                        map.put("driver_phone_no", "")
                                        map.put("driver_name", "")
                                        senTOsrvr(map, acceptList)
                                    } else {
                                        Log.d("currentState", "" + isStateSaved)
                                    }
                                }
                            }

                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
                spSendThrough?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        Log.d("NothingSelected",""+parent?.selectedItem.toString())
                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        Log.d("Selected",""+spSendThrough?.selectedItem.toString())
                        if(spSendThrough?.selectedItem.toString().equals(getString(R.string.direct))){
                            liCourier?.visibility = View.GONE

                        }else{
                            liCourier?.visibility = View.VISIBLE
                                if(spSendThrough?.selectedItem.toString().equals(getString(R.string.transport))){
                                    edtDocketNo?.visibility =View.GONE
                                    edtVide?.visibility =View.GONE
                                    edtDriverName?.visibility =View.VISIBLE
                                    edtMobileNo?.visibility =View.VISIBLE
                                }else if(spSendThrough?.selectedItem.toString().equals(getString(R.string.courier))){
                                    edtDocketNo?.visibility =View.VISIBLE
                                    edtVide?.visibility =View.VISIBLE
                                    edtDriverName?.visibility =View.GONE
                                    edtMobileNo?.visibility =View.GONE
                                }

                        }
                    }

                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }
    }

    private fun senTOsrvr(map:HashMap<String,Any>,acceptList: ArrayList<DefectItem>) {
        try{
            if(callApi?.isNetworkConnected!!){
                dialog=MessageUtils.showDialog(activity)
                val hashmap :HashMap<String,Any> = HashMap()
                hashmap.put("defect",acceptList)
                map.put("defects",hashmap)
                map["userid"]=LocalSession.getUserInfo(activity, LocalSession.KEY_ID)
                val calinterface  =callApi?.connectRetro("POST","sendsparesdefectreturn")
                val callback =calinterface?.call_post("sendsparesdefectreturn","Bearer "+LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),map)
                callback?.enqueue(object :Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.message)
                    }
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                val jsonObject = JSONObject(response.body()?.string())
                                val success =jsonObject.getBoolean("success")!!
                                val message = jsonObject.getString("message")!!
                                if(success){
//                                    activity?.finish()
                                    if (dialogSend?.isShowing!!) {
                                        dialogSend?.dismiss()
                                    } else {
                                        try {
                                            dialogSend?.dismiss()
                                        } catch (ex: Exception) {
                                            ex.printStackTrace()
                                        }
                                    }
                                    acceptlinear?.visibility =View.GONE
                                    getDataFrmSrvr()
                                }else{
                                    MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,message)
                                }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }

                        }else{
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response?.message())
                        }
                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity!!, CustomNavigationDuo.snackVIew,getString(R.string.check_internet))
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun getDataFrmSrvr() {
        try{
            if(callApi?.isNetworkConnected!!){
                dialog= MessageUtils.showDialog(activity)
                val map:HashMap<String,Any> = HashMap()
                map.put("userid", LocalSession.getUserInfo(activity, LocalSession.KEY_ID))
                var calinterface = callApi?.connectRetro("POST","sparesdefectreturn")
                var callback = calinterface?.call_post("sparesdefectreturn","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),map)
                callback?.enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.message)
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                defectresList= Gson().fromJson(response.body()?.string(), DefectRESList::class.java)
                                if(defectresList?.data?.defectreturnlist?.size!!<=0 ){
                                    Log.d("ArrayListSIze",""+isStateSaved)
                                    emptyList?.text = defectresList?.message
                                    emptyList?.visibility =View.VISIBLE
                                    recydefectlist?.visibility =View.GONE
                                }else{
                                    recydefectlist?.visibility =View.VISIBLE
                                    emptyList?.visibility =View.GONE
                                    setToAdpter()
                                }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }


                        }else{
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response.message())
                        }

                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity, CustomNavigationDuo.snackVIew,getString(R.string.check_internet))
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
    private fun setToAdpter() {
        try{
            recydefectlist?.layoutManager =LinearLayoutManager(activity,LinearLayout.VERTICAL,false)
            defectListAdapter= DefectRetunAdapter(activity!!,defectresList?.data?.defectreturnlist,this)
            recydefectlist?.adapter =defectListAdapter
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }
    override fun connectposition(position: Int, listSize: Int) {
        try{
            Log.d("Called interface Function",""+position+listSize)

            if(listSize <=0){
                acceptlinear?.visibility =View.GONE
                checkbox?.isChecked=false
                btnAcept?.setText("Accept(0)")
            }else{
                acceptlinear?.visibility =View.VISIBLE
                btnAcept?.setText("Accept("+listSize+")")
            }
            if(defectresList?.data?.defectreturnlist?.get(position)?.check!!){
                Log.d("SpareName",""+defectresList?.data?.defectreturnlist?.get(position)?.oldSerialNo)
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
    private fun validate(sendthough: String, docketno: String, vide: String): Boolean {
        if(sendthough.isEmpty() || sendthough.equals(getString(R.string.send_through))){
//                Snackbar.make(dialogsendToPlantview!!, "Select Valid send Through", Snackbar.LENGTH_LONG).show()
            MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Select Valid send Through")
            return false
        }else if(docketno.isEmpty()){
            MessageUtils.showSnackBar(activity,dialogsendToPlantview,"docketno Can't Be Empty")
            return false
        }else if(vide.isEmpty()){
            MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Vide Can't Be Empty")
            return false
        }
        return true
    }
    private fun validatetrsn(sendthough: String, mobileno: String, drivername: String): Boolean {
        if(sendthough.isEmpty() || sendthough.equals(getString(R.string.send_through))){
//                Snackbar.make(dialogsendToPlantview!!, "Select Valid send Through", Snackbar.LENGTH_LONG).show()
            MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Select Valid send Through")
            return false
        }else if(mobileno.isEmpty()){
            MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Mobile Number is Can't Be Empty")
            return false
        }else if(drivername.isEmpty()){
            MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Drive Name can't Be Empty")
            return false
        }
        return true
    }
}
