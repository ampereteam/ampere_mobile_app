package com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignService


import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome.Companion.btnAssignList
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome.Companion.serviceserverEnquiryList
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome.Companion.spareserverEnquiryList
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSales.AssignSales
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignService.Adapter.AssignServiceAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo.AssignEnqIDPojo
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo.EnquiryListRes

import com.ampere.vehicles.R
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("SetTextI18n,StaticFieldLeak")
class AssignService : Fragment(),ConfigClickEvent {

    var recyAssignService : RecyclerView?=null
    var dialog : Dialog?=null
    var utilsCallApi: UtilsCallApi?=null
    var cbxAssingService: CheckBox?= null
    var  emptyList : TextView?=null
    var assignServiceAdapter: AssignServiceAdapter? =null
    companion object {
        var btnAssignservice: TextView?= null
        var serviceEnqryRES: EnquiryListRes?= null
        var servicecount=0
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        (activity as CustomNavigationDuo).disbleNavigationView(getString(R.string.assigned_enquiry))
        return inflater.inflate(R.layout.fragment_unassign_service, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        utilsCallApi = UtilsCallApi(activity)
        recyAssignService=view.findViewById(R.id.recyl_assign_sales)
        cbxAssingService =view.findViewById(R.id.cbx_assgin_sales)
        btnAssignservice =view.findViewById(R.id.btn_assignsales)
        emptyList =view.findViewById(R.id.empty_list)
        cbxAssingService?.isChecked =false
        if(utilsCallApi?.isNetworkConnected!!){
            getServiceEnquiryList()
        }else{
            MessageUtils.showSnackBarAction(activity, CustomNavigationDuo.snackVIew,getString(R.string.check_internet))
        }

        cbxAssingService?.setOnCheckedChangeListener { buttonView, isChecked ->

            if(serviceEnqryRES?.data?.enquiries?.size!!<=0){
                Log.d("LISTSIze",""+serviceEnqryRES?.data?.enquiries?.size)

            }else{
                if(isChecked){
                    for(i:Int in 0.. serviceEnqryRES?.data?.enquiries?.size!!-1){
                        serviceEnqryRES?.data?.enquiries?.get(i)?.check =true
                        serviceserverEnquiryList.get(i)?.check =true
                    }
                    btnAssignservice?.setText(getString(R.string.assign)+"("+serviceEnqryRES?.data?.enquiries?.size+")")
                    btnAssignList?.setText(getString(R.string.assign)+"("+serviceEnqryRES?.data?.enquiries?.size+")")
                    btnAssignservice?.visibility =View.VISIBLE
                    btnAssignList?.visibility =View.VISIBLE
                    servicecount= serviceEnqryRES?.data?.enquiries?.size!!
                }else{
                    for(i:Int in 0.. serviceEnqryRES?.data?.enquiries?.size!!-1){
                        serviceEnqryRES?.data?.enquiries?.get(i)?.check =false
                        serviceserverEnquiryList.get(i)?.check =false
                    }
                    btnAssignservice?.visibility =View.GONE
                    btnAssignList?.visibility =View.GONE
                    servicecount=0
                }
                assignServiceAdapter?.notifyDataSetChanged()
            }


        }
        btnAssignservice?.setOnClickListener {
            try{
                if(AssignListHome.spAssignPerson?.selectedItem !=null){
                    assignServiceEnquiries()
                }else{
                    MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,"Select Assign To and Person")
                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }
   /*     if(AssignListHome.btnAssignList!=null){
            AssignListHome.btnAssignList?.setOnClickListener {
                try{
                    if(AssignListHome.spAssignPerson?.selectedItem !=null){
                        assignServiceEnquiries()
                    }else{
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,"Select Assign To and Person")
                    }
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        }*/
    }

    private fun assignServiceEnquiries() {
        try{
            val map:HashMap<String,Any>  = HashMap()
            map["userid"] = LocalSession.getUserInfo(activity, LocalSession.KEY_ID)
            map["assign_to"] = AssignListHome.spAssignTo?.selectedItem.toString()
            var assignpersonID :String?=null
            for(user in AssignListHome.userListRes?.data?.users!!){
                if( user?.name.equals(AssignListHome.spAssignPerson?.selectedItem.toString())){
                    assignpersonID = user?.id.toString()
                }
            }
            if(assignpersonID!=null){
                map["assign_person"] =assignpersonID.toInt()
            }
            val hashMap:HashMap<String,Any>  =HashMap()
            val enquiryList :ArrayList<AssignEnqIDPojo> = ArrayList()
            for (i:Int in 0 .. serviceserverEnquiryList.size-1){
                if( serviceserverEnquiryList[i]?.check!!) {
                    val assignEnqIDPojo = AssignEnqIDPojo( serviceserverEnquiryList[i]?.id.toString())
                    enquiryList.add(assignEnqIDPojo)
                }
            }
            hashMap["enquiry"] =enquiryList
            map["enquiries"] =hashMap
            Log.d("AssignEnquirySales",""+map.toString())
            if(utilsCallApi?.isNetworkConnected!!){
                dialog = MessageUtils.showDialog(activity)
                val calinterface = utilsCallApi?.connectRetro("POST","assignenquiry")
                val callback = calinterface?.call_post("assignenquiry","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),map)
                callback?.enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        try{
                            MessageUtils.dismissDialog(dialog)
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.localizedMessage)
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        try{
                            if(response.isSuccessful){
                                try{
                                    val jsonObject = JSONObject(response.body()?.string())
                                    val message = jsonObject.getString("message")
                                    val success =jsonObject.getBoolean("success")
                                    if(success){
                                        btnAssignservice?.visibility =View.GONE
                                        btnAssignList?.visibility =View.GONE
                                        Toast.makeText(activity,message, Toast.LENGTH_LONG).show()
                                        getServiceEnquiryList()
                                    }else{
                                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,message)
                                    }
                                }catch (ex:Exception){
                                    ex.printStackTrace()

                                }
                            }else{
                                MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response.message())
                            }
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity, CustomNavigationDuo.snackVIew,getString(R.string.check_internet))
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun getServiceEnquiryList() {
        try{
            val hashMap :HashMap<String,Any> = HashMap()
            hashMap["userid"] = LocalSession.getUserInfo(activity, LocalSession.KEY_ID)
            hashMap["enquiry_type"] = getString(R.string.service)
            dialog = MessageUtils.showDialog(activity)
            val callInterface  = utilsCallApi?.connectRetro("POST","EnquiryListRes")
            val callBack = callInterface?.call_post("unassignedenquiry","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),hashMap)
            callBack?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    try{
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.localizedMessage)

                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    try{
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                serviceEnqryRES = Gson().fromJson(response.body()?.string(),EnquiryListRes::class.java)
                                if(serviceEnqryRES?.success!!){
                                    if(serviceEnqryRES?.data?.enquiries?.size!! <=0){
                                        emptyList?.setText(serviceEnqryRES?.message)
                                        emptyList?.visibility = View.VISIBLE
                                        recyAssignService?.visibility =View.GONE
                                    }else{
                                        if(btnAssignservice?.visibility == View.VISIBLE  ){
                                            btnAssignservice?.visibility = View.GONE
                                        }
                                        if(cbxAssingService?.isChecked!!){
                                            cbxAssingService?.isChecked =false
                                        }
                                        emptyList?.visibility = View.GONE
                                        recyAssignService?.visibility =View.VISIBLE
                                        serviceserverEnquiryList  = serviceEnqryRES?.data?.enquiries!!
                                        servicecount =0
                                        setTOAdapter()
                                    }

                                }else{
                                    MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,serviceEnqryRES?.message)
                                }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }
                        }else{
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response.message())
                        }
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }
            })
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setTOAdapter() {
        try{
            recyAssignService?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL,false)
            assignServiceAdapter = AssignServiceAdapter(activity!!, serviceserverEnquiryList,this)
            recyAssignService?.adapter= assignServiceAdapter

        }catch (eX:Exception){
            eX.printStackTrace()
        }
    }

    override fun connectposition(position: Int, listSize: Int) {
        try{
            Log.d("CallPostion",""+position+"\tlistsize"+listSize)
            if(listSize<=0){
                btnAssignservice?.visibility =View.GONE
                btnAssignList?.visibility =View.GONE
                servicecount =0
            }else{
                btnAssignservice?.setText(getString(R.string.assign)+"("+listSize+")")
                btnAssignList?.setText(getString(R.string.assign)+"("+listSize+")")
                btnAssignservice?.visibility =View.VISIBLE
                servicecount=listSize
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

}
