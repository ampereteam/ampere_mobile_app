package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.ServiceEnquiryServerPojo

import android.content.ComponentName
import com.google.gson.annotations.SerializedName

class PaidReplacementServerPojo(
        @field:SerializedName("component_name")
        var componentName: String ?=null,
        @field:SerializedName("old_serial_no")
        var oldSerialNumber: String ?=null,
        @field:SerializedName("new_serial_no")
        var newSerialNumber: String ?=null,
        @field:SerializedName("id")
        var id :Int ?=null
)