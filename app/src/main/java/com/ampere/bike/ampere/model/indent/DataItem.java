package com.ampere.bike.ampere.model.indent;

import com.google.gson.annotations.SerializedName;


public class DataItem{

	@SerializedName("bom")
	public String bom;

	@SerializedName("updated_at")
	public String updatedAt;

	@SerializedName("spares_serial_no")
	public String sparesSerialNo;

	@SerializedName("vehicle_model")
	public String vehicleModel;

	@SerializedName("manufacturing_date")
	public String manufacturingDate;

	@SerializedName("created_at")
	public String createdAt;

	@SerializedName("id")
	public int id;

	@SerializedName("frame_number")
	public String frameNumber;

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"bom = '" + bom + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",spares_serial_no = '" + sparesSerialNo + '\'' + 
			",vehicle_model = '" + vehicleModel + '\'' +
			",manufacturing_date = '" + manufacturingDate + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",frame_number = '" + frameNumber + '\'' + 
			"}";
		}
}