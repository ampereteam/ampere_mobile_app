package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.api.Retrofit.ResponseObjectsBackToClass;
import com.ampere.bike.ampere.api.Retrofit.ResponsebackToClass;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.views.fragment.indent.ColorListPojos.ColorListPojos;
import com.ampere.bike.ampere.views.fragment.indent.VariantListPojos.SparePojos;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Adapter.IndentDetailsAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.EditIndentPojos.IndentModel;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.IndentDetailsPojos;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.ModelVariantPojos.ModelVariantPojos;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.SpareListPojos.SparesListPojos;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.vehicleListPojos.DataList;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.vehicleListPojos.VehiclemodelItem;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import customviews.CustomButton;
import customviews.CustomRadioButton;
import customviews.CustomTextEditView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;
import static com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentHome.msnackview;

public class IndentDetailsActivity extends Fragment implements ResponsebackToClass, ConfigClickEvent, ResponseObjectsBackToClass {
    String ID = null;
    String IndentType;
    String spare, qty, variant, color, remark;
    int  indent_id ;
    Common common;
    RecyclerView recyclerView;
    TextView textView;
    ArrayList<IndentDetailsPojos> indentDetailsPojosArrayList;
    ArrayList<ColorListPojos> colorListPojosArrayList;
    ArrayList<SparePojos> sparePojosArrayList;
    AlertDialog.Builder alertDialogBuilder;
    IndentDetailsAdapter indentDetailsAdapter;
    AlertDialog dialog;
    Spinner spVehicle, spSpares, spVariant, spColor;
    RadioGroup radioGroup;
    CustomTextEditView edtVehicle, edtSpares, edtVariant, edtQty, edtRemark;
    CustomTextEditView edtColor;
    CustomRadioButton rdbVehicle, rdbSpares;
    RelativeLayout variantLay;
    RelativeLayout spareLay;
    RelativeLayout colorLay;
    CustomButton btnIndentCancel, btnIndentSubmit;
    LinearLayout linearLayout;
    int listPosition;
    int indentPostion = 0;
    String  status;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/*
        setContentView(R.layout.activity_indent_details);
        alertDialogBuilder = new AlertDialog.Builder(this);
        ID = getIntent().getStringExtra("ID");
        if (ID != null) {
            System.out.println("get Id    " + ID);
            common = new Common(this, this, this);

            textView = findViewById(R.id.emptyList);
            recyclerView = findViewById(R.id.indent_details_list);
            //recyclerView.setLayoutManager(new GridLayoutManager(this,1));
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            CallApi();
        }*/
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ID = getArguments().getString("ID");
        status = getArguments().getString("status");
        Log.d("GetIndentDetails",""+ID+"  status\t"+status);
        ((CustomNavigationDuo) getActivity()).disbleNavigationView("Indent ID :" + ID);
        return inflater.inflate(R.layout.activity_indent_details, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        alertDialogBuilder = new AlertDialog.Builder(getActivity());


        if (ID != null) {
            System.out.println("get Id    " + ID);
            common = new Common(getActivity(), this, this);
            linearLayout = view.findViewById(R.id.main);
            textView = view.findViewById(R.id.emptyList);
            recyclerView = view.findViewById(R.id.indent_details_list);
            //recyclerView.setLayoutManager(new GridLayoutManager(this,1));
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            CallApi();
        }
    }

    public void CallApi() {
        common.showLoad(true);
        if (common.isNetworkConnected()) {
            HashMap<String, String> map = new HashMap<>();
            map.put("id", ID);
            common.callApiRequest("indentdetail", "POST", map, 0);
        } else {
            common.hideLoad();
            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * get response Json Objects and set to the ArrayList..
     *
     * @param response
     * @param objResponse
     * @param method
     * @param position
     * @param message
     * @param sucs
     * @throws JSONException
     * @throws IOException
     */
    @Override
    public void apiCallBackOverRideMethod(Response<ResponseBody> response, JSONArray objResponse, String method, int position, String message, boolean sucs) throws JSONException, IOException {
        common.hideLoad();
        if (objResponse != null) {
            System.out.println("response LIst" + objResponse.toString());
            indentDetailsPojosArrayList = new Gson().fromJson(objResponse.toString(), new TypeToken<ArrayList<IndentDetailsPojos>>() {
            }.getType());
            if (indentDetailsPojosArrayList.size() > 0) {
                linearLayout.setVisibility(View.VISIBLE);
                indentDetailsAdapter = new IndentDetailsAdapter(getActivity(), indentDetailsPojosArrayList, this);
                recyclerView.setAdapter(indentDetailsAdapter);
            } else {
                textView.setVisibility(View.VISIBLE);
                textView.setText(message);
                linearLayout.setVisibility(View.GONE);
            }

        }
    }

    /**
     * get particular postion from adapter for  edit or change
     *
     * @param position
     * @param listSize
     */
    @Override
    public void connectposition(int position, int listSize) {
        indentPostion = position;
        indent_id  =indentDetailsPojosArrayList.get(position).getId();
//        Log.d("",""+indentDetailsPojosArrayList.get(position).get)
        try{
            if(status!=null && status.equals("Pending")){
                popupMenu();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void popupMenu() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.dialog_indent_details_edit, null, false);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(true);
        dialog = alertDialogBuilder.create();
        dialog.show();
        radioGroup = view.findViewById(R.id.rg_indent_type);

        colorLay = view.findViewById(R.id.ind_colour);
        variantLay = view.findViewById(R.id.model_vehicle);
        spareLay = view.findViewById(R.id.l_spares);


        btnIndentSubmit = view.findViewById(R.id.ind_edt_submit);
        btnIndentCancel = view.findViewById(R.id.ind_cnl);


        rdbSpares = view.findViewById(R.id.rb_spares);
        rdbVehicle = view.findViewById(R.id.rb_vehicle);

        spSpares = view.findViewById(R.id.ind_sp_spares);
        spVehicle = view.findViewById(R.id.ind_sp_vehicle);
        spVariant = view.findViewById(R.id.indent_varient_spinner);
        spColor = view.findViewById(R.id.sp_indent_color);


        edtVehicle = view.findViewById(R.id.in_edt_vehicle);
        edtColor = view.findViewById(R.id.edt_indent_color);
        edtQty = view.findViewById(R.id.in_edt_qty);
        edtRemark = view.findViewById(R.id.indent_remarks);
        edtSpares = view.findViewById(R.id.in_edt_spares);
        edtVariant = view.findViewById(R.id.indent_model_varient);

        /**
         * set the Radio button  here whether its vehicle or spares
         */

        if (indentDetailsPojosArrayList.get(indentPostion).getIndentType().equals("Vehicle")) {
            rdbVehicle.setChecked(true);
            rdbSpares.setChecked(false);
            spSpares.setVisibility(View.GONE);
            edtSpares.setVisibility(View.GONE);
            edtColor.setVisibility(View.VISIBLE);
            colorLay.setVisibility(View.VISIBLE);
        } else if (indentDetailsPojosArrayList.get(indentPostion).getIndentType().equals("spare")) {
            rdbSpares.setChecked(true);
            rdbVehicle.setChecked(false);
            edtColor.setVisibility(View.GONE);
            colorLay.setVisibility(View.GONE);
            spSpares.setVisibility(View.VISIBLE);
            edtSpares.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(getActivity(), "Check with indent Type", Toast.LENGTH_SHORT).show();
            rdbSpares.setChecked(false);
            rdbVehicle.setChecked(false);
        }


        if (common.isNetworkConnected()) {
            getVehiclefromserver();

        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.data), Toast.LENGTH_SHORT).show();
        }
        IndentType = indentDetailsPojosArrayList.get(indentPostion).getIndentType();
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioSexButton = view.findViewById(checkedId);
                IndentType = radioSexButton.getText().toString();
//                Toast.makeText(getActivity(), IndentType, Toast.LENGTH_SHORT).show();
                System.out.println("indent Type" + IndentType);
                if (IndentType.equals("Spares")) {
                    colorLay.setVisibility(View.VISIBLE);
                    edtColor.setVisibility(View.VISIBLE);
                    spColor.setVisibility(View.VISIBLE);
                    edtColor.setText("");
                    variantLay.setVisibility(View.GONE);
                    edtVariant.setVisibility(View.GONE);
                    edtVariant.setText("");
                    spareLay.setVisibility(View.GONE);
                    spSpares.setVisibility(View.GONE);
                } else {
                    /*
                     * new requirement remove variant
                     *
                     variantLay.setVisibility(View.GONE);
                    edtVariant.setVisibility(View.GONE);*/
                    colorLay.setVisibility(View.VISIBLE);
                    edtColor.setVisibility(View.VISIBLE);
                    spColor.setVisibility(View.VISIBLE);
                    spSpares.setVisibility(View.GONE);
                    spareLay.setVisibility(View.GONE);
                    edtSpares.setVisibility(View.GONE);
                }
                getVehiclefromserver();
            }
        });

        /**
         * three spinner selecteddlinstener here
         */
        spVehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String vehicleType = spVehicle.getSelectedItem().toString();
                if (vehicleType != null && !vehicleType.isEmpty()) {
                    vehicleType = vehicleType.toString();
                    if (vehicleType.equals("select Vehicle")) {
                        edtVehicle.setText(vehicleType);

                    } else {
                        edtVehicle.setText(vehicleType);
//                        getVariantFromServer();
                        getColorListFromServer();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spSpares.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String spareName = spSpares.getSelectedItem().toString();
                spare = spSpares.getSelectedItem().toString();
                if (spareName.equals("select Spare")) {
                    edtSpares.setText(spareName);
                } else {
                    edtSpares.setText(spareName);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedColor = spColor.getSelectedItem().toString();
                color = spColor.getSelectedItem().toString();
                if (selectedColor.equals(getString(R.string.select_model_color)) || selectedColor.equals("Colors Not Available")) {
                    edtColor.setText(selectedColor);
                    edtSpares.setText(" ");
                    spareLay.setVisibility(View.GONE);
                    spSpares.setVisibility(View.GONE);
                    edtSpares.setVisibility(View.GONE);
                } else {
                    edtColor.setText(selectedColor);
//                    getColorandSpareFrmSRVR();
                    getColorForSPARE();


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
/*
        spVariant.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedVariant = spVariant.getSelectedItem().toString();
                variant = spVariant.getSelectedItem().toString();
                if (selectedVariant.equals("Select Variant")) {
                    edtVariant.setText(selectedVariant);
                } else {
                    edtVariant.setText(selectedVariant);
//                    getColorandSpareFrmSRVR();

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


        edtQty.setText(indentDetailsPojosArrayList.get(indentPostion).getQty().trim());
        edtQty.setSelection(edtQty.getText().length());
        edtRemark.setText(indentDetailsPojosArrayList.get(indentPostion).getRemarks().trim());
        edtRemark.setSelection(edtRemark.getText().length());
        btnIndentSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToserverUpdateIndentfn();
            }
        });
        btnIndentCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null || dialog.isShowing()) {
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                }

            }
        });

    }

    private void sendToserverUpdateIndentfn() {
        /**
         * create  indent  model objects creating for sending  To Server
         */

        try {
            qty = edtQty.getText().toString();
            color = edtColor.getText().toString();
            remark = edtRemark.getText().toString();
            variant = edtVariant.getText().toString();
            if (spare == null || spare.length() == 0 || spare.isEmpty()) {
                spare = "";
            } else {
                spare = spare;
            }
            if (color == null || color.length() == 0) {
                color = "";
            } else {
                color = color;
            }
            if (variant == null || variant.length() == 0 || variant.isEmpty()) {
                variant = "";
            } else {
                variant = variant;
            }
            if (IndentType.equals("Spares")) {
                IndentType = "Spares";
            } else if (IndentType.equals("spare")) {
                IndentType = "Spares";
            }else{
                spare="";
            }

            IndentModel indentModel = new IndentModel(
                    IndentType,
                    spVehicle.getSelectedItem().toString().trim(),
                    spare,
                    qty,
                    variant,
                    color,
                    remark,
                    indent_id

            );

            ArrayList<IndentModel> indentModelSERVERArrayList = new ArrayList<>();
            indentModelSERVERArrayList.add(indentModel);

            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("spare", indentModelSERVERArrayList);
            HashMap<String, Object> map = new HashMap<>();
            map.put("sparelists", hashMap);
            //sessionManager.getvalue(SessionManager.ID)
            map.put("user_id", LocalSession.getUserInfo(getActivity(), KEY_ID));
            map.put("indent_id", indentDetailsPojosArrayList.get(indentPostion).getIndentId().toString().trim());

            /**
             * Calling  api class common
             */


            if (qty == null || qty.trim().length() == 0 || qty.isEmpty()) {
                edtQty.setError("Required");
            } else {
                if (remark.isEmpty() || remark == null || remark.length() == 0) {
                    edtRemark.setError("Required");
                } else {
                    if (color.equals(getString(R.string.select_model_color)) || color.equals("Colors Not Available")) {
                        edtColor.setError(" Select Model serviceType ");
                        Toast.makeText(getActivity(), "select Model Color", Toast.LENGTH_SHORT).show();
                    } else {
                        if (spare.equals("Select SpareName")) {
                            edtSpares.setError("Select SpareName");
                            Toast.makeText(getActivity(), "Select SpareName", Toast.LENGTH_SHORT).show();
                        } else {
                            if (common.isNetworkConnected()) {

                                common.callApiRequestObjects("editindent", "POST", map, 0);
                            } else {
                                MessageUtils.showSnackBarAction(getActivity(),msnackview,getString(R.string.check_internet));
//                                Toast.makeText(getActivity(), getResources().getString(R.string.data), Toast.LENGTH_SHORT).show();
                            }

                        }
                    }

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void getColorandSpareFrmSRVR() {

        HashMap<String, Object> objectHashMap = new HashMap<>();
        objectHashMap.put("vehicle_model", spVehicle.getSelectedItem().toString().trim());
        objectHashMap.put("model_varient", spVariant.getSelectedItem().toString().trim());
        if (common.isNetworkConnected()) {
            common.callApiRequestObjects("color_spares", "POST", objectHashMap, 0);
        } else {
            MessageUtils.showSnackBarAction(getActivity(),msnackview,getString(R.string.check_internet));
//            Toast.makeText(getActivity(), getResources().getString(R.string.data), Toast.LENGTH_SHORT).show();
        }
    }

    private void getColorForSPARE() {
        HashMap<String, Object> objectHashMap = new HashMap<>();
        objectHashMap.put("vehicleModel", spVehicle.getSelectedItem().toString().trim());
        objectHashMap.put("color", spColor.getSelectedItem().toString().trim());
        if(common.isNetworkConnected()) {
            common.callApiRequestObjects("spareslist", "POST", objectHashMap, 0);
        } else {
            MessageUtils.showSnackBarAction(getActivity(),msnackview,getString(R.string.check_internet));
//            Toast.makeText(getActivity(), getResources().getString(R.string.data), Toast.LENGTH_SHORT).show();
        }
    }

    private void getVariantFromServer() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("vehicle_model", spVehicle.getSelectedItem().toString().trim());
        if (common.isNetworkConnected()) {
            common.callApiRequestObjects("model_varient", "POST", map, 0);
        } else {
            MessageUtils.showSnackBarAction(getActivity(),msnackview,getString(R.string.check_internet));
//            Toast.makeText(getActivity(), getResources().getString(R.string.data), Toast.LENGTH_SHORT).show();
        }

    }

    private void getColorListFromServer() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("vehicleModel", spVehicle.getSelectedItem().toString().trim());
        if (common.isNetworkConnected()) {
            common.callApiRequestObjects("vehiclecolorlist", "POST", map, 0);
        } else {
            MessageUtils.showSnackBarAction(getActivity(),msnackview,getString(R.string.check_internet));
//            Toast.makeText(getActivity(), getResources().getString(R.string.data), Toast.LENGTH_SHORT).show();
        }

    }

    private void getVehiclefromserver() {
        HashMap<String, Object> objectHashMap = new HashMap<>();
        objectHashMap.put("user_id", LocalSession.getUserInfo(getActivity(), KEY_ID));
        Call<DataList> listCall = common.connectRetro("POST", "datalist").call_post("Bearer " + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), objectHashMap);

        listCall.enqueue(new Callback<DataList>() {
            @Override
            public void onResponse(Call<DataList> call, Response<DataList> response) {
                try {
                    if (response.isSuccessful()) {
                        DataList dataList = response.body();

                        if (dataList.isSuccess()) {
                            ArrayList<String> vehicleList = new ArrayList<>();
//                        vehicleList.add("select Vehicle");
                            for (VehiclemodelItem vehiclemodelItem : dataList.getData().getVehiclemodel()) {
                                vehicleList.add(vehiclemodelItem.getVehicleModel());
                            }
                            spVehicle.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, vehicleList));
                            int setPostion = 0;
                            for (int i = 0; i < vehicleList.size(); i++) {
                                if (vehicleList.get(i).equals(indentDetailsPojosArrayList.get(indentPostion).getVehicleModel())) {
                                    setPostion = i;
                                }
                            }

                            spVehicle.setSelection(setPostion);
                            System.out.println(" spinner selcted postion value" + spVehicle.getSelectedItem().toString());
                            common.hideLoad();
                        } else {

                        }


                    } else {
                        Log.v("info", response.body().toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DataList> call, Throwable t) {

            }
        });

    }


    @Override
    public void responseObjects(JSONObject objResponse, String method, int position, String message, boolean success) throws JSONException, IOException {
        if (method.equals("model_varient")) {
            if (success) {
                String checkTest = objResponse.getString("varient");
                if (checkTest.equals("Available")) {
                    variantLay.setVisibility(View.VISIBLE);
                    edtVariant.setVisibility(View.VISIBLE);
                    spVariant.setVisibility(View.VISIBLE);
                    ArrayList<ModelVariantPojos> modelVariantPojosArrayList = new Gson().fromJson(objResponse.getJSONArray("model_varient").toString(), new TypeToken<ArrayList<ModelVariantPojos>>() {
                    }.getType());

                    /**
                     * get the whole variant from server set to spinner
                     */
                    if (modelVariantPojosArrayList.size() > 0) {
                        ArrayList<String> modelVariantList = new ArrayList<>();
                        for (ModelVariantPojos variantPojos : modelVariantPojosArrayList) {
                            modelVariantList.add(variantPojos.getModelVarient());
                        }
                        spVariant.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, modelVariantList));

                        /**
                         * get postion to set the variant
                         */
                        int positon = 0;
                        for (int i = 0; i < modelVariantList.size(); i++) {
                            if (indentDetailsPojosArrayList.get(indentPostion).getModelVarient().equals(modelVariantList.get(i))) {
                                positon = i;
                            }
                        }
                        spVariant.setSelection(positon);
                    } else {
                        Toast.makeText(getActivity(), "Data not Found", Toast.LENGTH_SHORT).show();
                    }
                    ArrayList<String> list = new ArrayList<>();
                    spSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, list));
                    spSpares.setVisibility(View.GONE);
                    edtSpares.setVisibility(View.GONE);
                    edtSpares.setText("");
                    spareLay.setVisibility(View.GONE);
                } else {
                    /**
                     * set disable to variant
                     */
                    variantLay.setVisibility(View.GONE);
                    edtVariant.setText("");
                    edtVariant.setVisibility(View.GONE);
                    ArrayList<String> emptylist = new ArrayList<>();
                    spVariant.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, emptylist));
                    spVariant.setVisibility(View.GONE);
                    /**
                     * get serviceType without variant and set Spares
                     *
                     */
//                    Toast.makeText(this, "Spare with out variant", Toast.LENGTH_SHORT).show();
                    ArrayList<String> colorList = new Gson().fromJson(objResponse.getJSONArray("colors").toString(), new TypeToken<ArrayList<String>>() {
                    }.getType());

                    spColor.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, colorList));

//  edtColor.setText(indentDetailsPojosArrayList.get(indentPostion).getServiceType());


                    /**
                     * getting  Spares from server and put to arrayList
                     *
                     */

                    ArrayList<SparesListPojos> sparesListPojosArrayList = new Gson().fromJson(objResponse.getJSONArray("spares").toString(), new TypeToken<ArrayList<SparesListPojos>>() {
                    }.getType());
                    ArrayList<String> list = new ArrayList<>();
                    int i = 0;
//                    System.out.println("spare Name"+indentDetailsPojosArrayList.get(indentPostion).getSpareName());
                    for (SparesListPojos sparesListPojos : sparesListPojosArrayList) {
                        try {
                            list.add(sparesListPojos.getSpares());
                            if (indentDetailsPojosArrayList.get(indentPostion).getSpareName().equals(sparesListPojos.getSpares())) {
                                System.out.println("spare Name" + sparesListPojos.getSpares());
                                ++i;
                            }
                        } catch (ArrayIndexOutOfBoundsException v) {
                            v.printStackTrace();
                        }
                    }

                    /**
                     * set the spares for particular vehicle not for variant
                     * and  set to corresponding vehicle position
                     */
//                    Toast.makeText(this, "sjjg \t"+IndentType, Toast.LENGTH_SHORT).show();
                    if (IndentType.equals("Spares") || IndentType.equals("spare")) {
                        spSpares.setVisibility(View.VISIBLE);
                        spareLay.setVisibility(View.VISIBLE);
                        edtSpares.setVisibility(View.VISIBLE);
                        spSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, list));

                        /**
                         * set the  position  to spinner corresponding selecting item
                         */
                        int selectedPosition = 0;
                        for (int j = 0; i < list.size(); i++) {
                            if (indentDetailsPojosArrayList.get(indentPostion).getSpareName().equals(list.get(i))) {
                                selectedPosition = j;
                            }
                        }
                        spSpares.setSelection(selectedPosition);

                        edtColor.setVisibility(View.GONE);
                        colorLay.setVisibility(View.GONE);

                    } else {
                        spSpares.setVisibility(View.GONE);
                        edtColor.setVisibility(View.VISIBLE);
                        edtColor.setSelection(edtColor.getText().length());
                        colorLay.setVisibility(View.VISIBLE);
                    }

                }

            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
        /**
         * get Color Spares for corresponding model Variant
         */
        else if (method.equals("color_spares")) {
            if (success) {
                if (objResponse != null) {
                    System.out.println("serviceType  spare list" + objResponse.toString());
                    ArrayList<String> colorList = new Gson().fromJson(objResponse.getJSONArray("colors").toString(), new TypeToken<ArrayList<String>>() {
                    }.getType());
                    spColor.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, colorList));
                    edtColor.setText(indentDetailsPojosArrayList.get(indentPostion).getColor());
                    edtColor.setSelection(edtColor.getText().length());
                    ArrayList<SparesListPojos> sparePojoslist = new Gson().fromJson(objResponse.getJSONArray("spares").toString(), new TypeToken<ArrayList<SparesListPojos>>() {
                    }.getType());
                    ArrayList<String> sparelist = new ArrayList<>();
                    int pos = 0;
                    for (SparesListPojos sparesListPojos : sparePojoslist) {
                        sparelist.add(sparesListPojos.getSpares());
                        if (indentDetailsPojosArrayList.get(indentPostion).getSpareName().equals(sparelist.get(pos))) {
                            pos++;
                        }
                    }
                    spSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, sparelist));
                    spSpares.setSelection(pos);
                    /**
                     * all colors and spares set to adapter
                     * then here check with indent type and show it serviceType and spare List
                     *
                     */
                    System.out.println("  indent Type using" + IndentType);
//                    Toast.makeText(this, "  indent Type using"+IndentType  , Toast.LENGTH_SHORT).show();
                    if (IndentType.equals("Spares") || IndentType.equals("spare")) {
                        spareLay.setVisibility(View.VISIBLE);
                        spSpares.setVisibility(View.VISIBLE);
                        edtSpares.setVisibility(View.VISIBLE);
                        colorLay.setVisibility(View.GONE);
                        edtColor.setVisibility(View.GONE);
                        edtColor.setText("");
                    } else {
                        edtColor.setVisibility(View.VISIBLE);
                        edtColor.setSelection(edtColor.getText().length());
                        colorLay.setVisibility(View.VISIBLE);
                        edtSpares.setVisibility(View.GONE);
                        edtSpares.setText("");
                        spSpares.setVisibility(View.GONE);
                        spareLay.setVisibility(View.GONE);
                    }

                } else {
                    Toast.makeText(getActivity(), "Data not Available", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }

        } else if (method.equals("editindent")) {
            if (success) {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                if (dialog != null || dialog.isShowing()) {
                    dialog.dismiss();
                    CallApi();
                }
                /*startActivity(new Intent(this, IndentHome.class));*/
//                getActivity().finish();
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }

        }
        /**
         *
         * new requirement  changes in indent
         *
         */
        else if (method.equals("vehiclecolorlist")) {
            if (objResponse != null) {
                colorListPojosArrayList = new Gson().fromJson(objResponse.getJSONArray("colors").toString(), new TypeToken<ArrayList<ColorListPojos>>() {
                }.getType());
                ArrayList<String> colorList = new ArrayList<>();
                colorList.add(getString(R.string.select_model_color));
                int pos = 0;
                for (ColorListPojos colorListPojos : colorListPojosArrayList) {
                    if (colorListPojos.getColor() == null || colorListPojos.getColor().isEmpty() || colorListPojos.getColor().length() == 0) {
                        Log.d("info for serviceType is empty", "" + colorList.size());
                    } else {
                        if (indentDetailsPojosArrayList.get(indentPostion).getColor().equals(colorListPojos.getColor())) {
                            ++pos;
                        }
                        colorList.add(colorListPojos.getColor());
                    }

                }
//                Toast.makeText(getActivity(), ""+colorList.get(pos)+"\n"+indentDetailsPojosArrayList.get(indentPostion).getServiceType(), Toast.LENGTH_SHORT).show();
                if (colorList.size() == 1) {
                    colorList.clear();
                    colorList.add("Colors Not Available");
//                    Toast.makeText(getActivity(), ""+colorList.size(), Toast.LENGTH_SHORT).show();
                }
                for (int i = 0; i < colorList.size(); i++) {
                    if (indentDetailsPojosArrayList.get(indentPostion).getColor().equals(colorList.get(i))) {
                        pos = i;
                    }
                }
                spColor.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, colorList));
                spColor.setSelection(pos);
                System.out.println("  indent Type" + IndentType);
                if (IndentType.equals("Vehicle") || IndentType.equals("Vehicle")) {
                    edtSpares.setText(" ");
                    spareLay.setVisibility(View.GONE);
                    spSpares.setVisibility(View.GONE);
                    edtSpares.setVisibility(View.GONE);
                } else {
                    colorLay.setVisibility(View.VISIBLE);
                    edtColor.setVisibility(View.VISIBLE);
                    spColor.setVisibility(View.VISIBLE);
                    spareLay.setVisibility(View.VISIBLE);
                    spSpares.setVisibility(View.VISIBLE);
                    edtSpares.setVisibility(View.VISIBLE);
                }

            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        } else if (method.equals("spareslist")) {
            if (success) {
                sparePojosArrayList = new Gson().fromJson(objResponse.getJSONArray("spareslist").toString(), new TypeToken<ArrayList<SparePojos>>() {
                }.getType());
                ArrayList<String> spareList = new ArrayList<>();
                spareList.add("Select SpareName");
                int post = 0;
                for (SparePojos sparePojos : sparePojosArrayList) {
                    if (sparePojos.getSpares().isEmpty() || sparePojos.getSpares() == null || sparePojos.getSpares().length() == 0) {
                        Log.d("", "" + sparePojos.getSpares());
                    } else {
                        if (sparePojos.getSpares().equals(indentDetailsPojosArrayList.get(indentPostion).getSpareName())) {
                            post++;
                        }
                        spareList.add(sparePojos.getSpares());
                    }

                }
                int positiion=0;
                Log.d("indent Positon serialNumber",""+indentDetailsPojosArrayList.get(indentPostion).getSpareName());
                for(int i=0;i<spareList.size() ;i++){
                    if (spareList.get(i).equals(indentDetailsPojosArrayList.get(indentPostion).getSpareName())) {
                        Log.d("indent Positon serialNumber",""+spareList.get(i));
                        positiion=i;
                    }
                }
                spSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, spareList));
                spSpares.setSelection(positiion);
                if (IndentType.equals("Vehicle")) {
                    edtSpares.setText("");
                    spSpares.setVisibility(View.GONE);
                    edtSpares.setVisibility(View.GONE);
                    spareLay.setVisibility(View.GONE);
                } else {
                    spSpares.setVisibility(View.VISIBLE);
                    edtSpares.setVisibility(View.VISIBLE);
                    spareLay.setVisibility(View.VISIBLE);
                }

            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }
/*
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }*/
}
