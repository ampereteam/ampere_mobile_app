package com.ampere.bike.ampere.model.indent.history;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IndentModelViewDetails {

    @SerializedName("message")
    public String message;

    @SerializedName("indentdetails")
    public List<IndentdetailsItem> indentdetails;

    @SerializedName("status")
    public boolean status;
}
