package com.ampere.bike.ampere.views.kotlin.fragments.invoice.invoicecolorpojos

data class Data(
	val colors: ArrayList<ColorsItem?>? = null
)
