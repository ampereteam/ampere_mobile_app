package com.ampere.bike.ampere.views.kotlin.fragments.unassign

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.BEARER
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.model.unassinged.UnAssginedTaskModel
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.activity_crm.*
import kotlinx.android.synthetic.main.frag_list_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UnAssignedTaskFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        (activity as CustomNavigationDuo).disbleNavigationViewButShowToolbarCustom("Un Assigned Task")
        return inflater.inflate(R.layout.activity_crm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity!!.actionBar?.hide()

        tool_bar_title.text = getString(R.string.un_assigned_task)
        searchView2.visibility = View.GONE

        txt_no_data.visibility = View.GONE
        container_child.visibility = View.GONE

        onTabCreatetion()
        onLoadUnAssignedTask()

        ic_back.setOnClickListener {
            activity!!.onBackPressed()
        }

    }


    private fun onTabCreatetion() {

        var tabValues = arrayListOf<String>("Sales", "Service", "Spares");
        viewpager.adapter = UnAssignedTaskActivityFromCRM.TabsItem(activity!!.supportFragmentManager, tabValues)

        tabs.setupWithViewPager(viewpager)

        for (i: Int in 0..tabs.getTabCount() - 1) {
            //noinspection Constant Conditions
            var tv = TextView(activity!!)
            tv.setTypeface(MyUtils.getTypeface(activity!!, "fonts/Lato-Bold.ttf"));
            if (i == 0) {
                tv.setText("" + tabValues[i].toUpperCase())
            } else if (i == 1) {
                tv.setText("" + tabValues[i].toUpperCase())
            } else {
                tv.setText("" + tabValues[i].toUpperCase())
            }

            tv.setTextColor(this.resources.getColor(R.color.white))
            tv.gravity = Gravity.CENTER
            tabs.getTabAt(i)!!.setCustomView(tv);

        }
    }

    private fun onLoadUnAssignedTask() {

        var map = HashMap<String, String>()
        map.put("user_name", LocalSession.getUserInfo(activity, LocalSession.KEY_USER_NAME))
        var unAssignedTaskModel: Call<UnAssginedTaskModel> = MyUtils.getRetroService().onUnAssignedTask(BEARER+LocalSession.getUserInfo(activity, KEY_TOKEN),map)

        var dialog = MessageUtils.showDialog(activity)
        dialog.setOnKeyListener { dialogInterface, i, keyEvent ->
            MessageUtils.dismissDialog(dialog);
            unAssignedTaskModel.cancel()
            false
        }

        unAssignedTaskModel.enqueue(object : Callback<UnAssginedTaskModel> {

            override fun onResponse(call: Call<UnAssginedTaskModel>?, response: Response<UnAssginedTaskModel>?) {
                MessageUtils.dismissDialog(dialog);
                if (response?.isSuccessful!!) {
                    var models = response.body()
                    if (models!!.data != null) {
                        MyUtils.SALES_UN_ASSIGN.clear()
                        MyUtils.SERIVCE_UN_ASSIGN.clear()
                        MyUtils.SPARES_UN_ASSIGN.clear()
                        Log.d("filterValue_before", "" + models!!.data.enquiry.size);
                        for (enquiryDetails in models!!.data.enquiry) {
                            var filterValue = enquiryDetails.enquiry_for
                            if (filterValue.equals(getString(R.string.sales))) {
                                MyUtils.SALES_UN_ASSIGN.add(enquiryDetails)
                            } else if (filterValue.equals(getString(R.string.service))) {
                                MyUtils.SERIVCE_UN_ASSIGN.add(enquiryDetails)
                            } else {
                                MyUtils.SPARES_UN_ASSIGN.add(enquiryDetails)
                            }
                        }

                        Log.d("filterValue", "SALES_UN_ASSIGN " + MyUtils.SALES_UN_ASSIGN.size + " SERIVCE_UN_ASSIGN " + MyUtils.SERIVCE_UN_ASSIGN.size + " SPARES_UN_ASSIGN " + MyUtils.SPARES_UN_ASSIGN.size)

                        onTabCreatetion()
                    } else {
                        MessageUtils.showSnackBar(activity, main_content, models.message)

                    }

                } else {
                    var msg = MessageUtils.showErrorCodeToast(response.code())
                    MessageUtils.showSnackBar(activity, main_content, msg)
                }
            }

            override fun onFailure(call: Call<UnAssginedTaskModel>?, t: Throwable?) {
                MessageUtils.dismissDialog(dialog);

                var msg = MessageUtils.showFailureToast(t?.message)
                MessageUtils.showSnackBar(activity, snack_View, msg)
            }

        })

    }


}