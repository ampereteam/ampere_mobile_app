package com.ampere.bike.ampere.model.invoiceGenerate.storedInvoice;

import com.google.gson.annotations.SerializedName;

public class ModelInvoiceStored {

    @SerializedName("success")
    public boolean success;

    @SerializedName("message")
    public String message;


}

