package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.google.gson.annotations.SerializedName;

public class EnquiryDetails {

    @SerializedName("id")
    public String id;

    @SerializedName("task")
    public String task;

    @SerializedName("enquiry_id")
    public String enquiry_id;

    @SerializedName("activity_date")
    public String activity_date;

    @SerializedName("remarks")
    public String remarks;

    @SerializedName("updated_at")
    public String updatedAt;

    @SerializedName("created_at")
    public String created_at;

    @SerializedName("completed_at")
    public String completed_at;

    @SerializedName("completed_by")
    public String completed_by;

    @SerializedName("status")
    public String status;

    @SerializedName("enquiry_status")
    public String enquiry_status;


}
