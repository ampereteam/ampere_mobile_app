package com.ampere.bike.ampere.model.enquiry.spares;

public class SparesGetter {


    public int id;
    public int vehicleModeId;
    public String sparesName, spareCode, vehicleVarientId, sparesId;

    public SparesGetter() {

    }

    public SparesGetter(int id, String spares, String spare_id, String spare_code, int vehicle_model_id, String model_varient_id) {
        this.id = id;
        this.sparesName = spares;
        this.sparesId = spare_id;
        this.spareCode = spare_code;
        this.vehicleModeId = vehicle_model_id;
        this.vehicleVarientId = model_varient_id;

    }
}
