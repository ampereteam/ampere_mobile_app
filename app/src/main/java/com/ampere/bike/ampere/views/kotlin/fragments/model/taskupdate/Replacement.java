package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareStockPojos.SpareStockPojos;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Replacement {
    @SerializedName("component")
    @Expose
    private String component;
    @SerializedName("component_id")
    @Expose
    private Integer componentId;
    @SerializedName("oldserial")
    @Expose
    private String oldserial;
    @SerializedName("complaint_id")
    @Expose
    private int  complaint_id;
    /***
     * this SPareStockPojos class in fragment/Crm/Pojos/SPareStockPojos
     * complaint_id
     */
    @SerializedName("sparesstock")
    @Expose
    private ArrayList<SpareStockPojos> sparesstock = null;

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public String getOldserial() {
        return oldserial;
    }

    public void setOldserial(String oldserial) {
        this.oldserial = oldserial;
    }

    public ArrayList<SpareStockPojos> getSparesstock() {
        return sparesstock;
    }

    public void setSparesstock(ArrayList<SpareStockPojos> sparesstock) {
        this.sparesstock = sparesstock;
    }


    public  boolean isValid(){
        return  sparesstock!=null && component !=null && oldserial !=null && componentId!=null;
    }

    public int getComplaint_id() {
        return complaint_id;
    }

    public void setComplaint_id(int complaint_id) {
        this.complaint_id = complaint_id;
    }
}
