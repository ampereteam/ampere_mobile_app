package com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Complaints


import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ampere.bike.ampere.CustomNavigationDuo.snackVIew
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_ID
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Complaints.Adapter.SpareServiceComplaintAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Complaints.ComplaintListPojo.ComplaintLISTRES
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.SpareServiceHomeKT

import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity

import com.ampere.vehicles.R
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SpareServiceComplaint : Fragment() , ConfigClickEvent {


    var recylCompliantList :RecyclerView ?=null
    var spsc_emptyList:TextView ?=null
    var callApi:UtilsCallApi?=null
    var dialog :Dialog?= null
    var dialogSendToplant :Dialog?= null

    var complaintresList:ComplaintLISTRES?= null
    var spareServiceComplaintAdapter: SpareServiceComplaintAdapter?= null
    /**
     * Send  TO Plant Popup Finctionality
     */
    var dialogSend :AlertDialog?= null
    var dialogSendBuilder :AlertDialog.Builder?= null
     var complaintId:String =""
     var spSendThrough :Spinner ?= null
     var edtDocketNo :EditText ?= null
     var edtVide :EditText ?= null
     var edtDriverName:EditText ?= null
     var edtMobileNo :EditText ? =null
     var btnclose :Button ?= null
     var btnsend:Button ?=null
     var liCourier:LinearLayout ?=null
     var dialogsendToPlantview: View?=null
    /** SPinner String ArrayList and Array Adapter***/
     var sendThroughSTRList:ArrayList<String>  = ArrayList()
     var sendThroughSTRAdapter:ArrayAdapter<String> ?=null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view  =inflater.inflate(R.layout.fragment_spare_service_complaint, container, false)
        var newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        return  view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            callApi = UtilsCallApi(activity)
            dialog = Dialog(this!!.activity)
            dialogSendToplant = Dialog(activity)
            dialogSendBuilder =AlertDialog.Builder(activity)
            recylCompliantList =view.findViewById(R.id.recycle_spsc_complaint_list)
            spsc_emptyList =view.findViewById(R.id.spsc_emptyList)
            recylCompliantList?.layoutManager =LinearLayoutManager(activity,LinearLayout.VERTICAL,false)
            getDataFrmSrvr()

    }

    private fun getDataFrmSrvr() {
        try{
            if(callApi?.isNetworkConnected!!){
                dialog = MessageUtils.showDialog(activity)
                val map:HashMap<String,Any> = HashMap()
                map["userid"] = LocalSession.getUserInfo(activity,KEY_ID)
                val calinterface =callApi?.connectRetro("POST","sparescomplaintlist")
                val callback  = calinterface?.call("sparescomplaintlist","Bearer "
                        +LocalSession.getUserInfo(activity, KEY_TOKEN),map)
                callback?.enqueue(object : Callback<ComplaintLISTRES> {
                    override fun onFailure(call: Call<ComplaintLISTRES>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        Log.d("FailureResponse",""+t.localizedMessage)
                        Log.d("FailureResponse",""+t.message)
                        MessageUtils.showSnackBar(activity, snackVIew,t.message)
                    }

                    override fun onResponse(call: Call<ComplaintLISTRES>, response: Response<ComplaintLISTRES>) {
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            Log.d("Response",""+response.body())
                            try{
                               complaintresList =response.body()
                                Log.d("Message",""+complaintresList?.message)
                                if(complaintresList?.data?.sparecomplaints?.size!! <=0){
                                    spsc_emptyList?.text=complaintresList?.message
                                    spsc_emptyList?.visibility =View.VISIBLE
                                    Log.d("ArrayListSize is Empty vIew",""+complaintresList?.message)
                                }else{
                                    spsc_emptyList?.visibility =View.GONE
                                    try{
                                        setTOAdpater()
                                    }catch (ex:Exception){
                                        ex.printStackTrace()
                                    }
                                }

                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }

                        }else{

                            MessageUtils.showSnackBar(activity, snackVIew,getString(R.string.server_not_found))
                        }
                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity,snackVIew,getString(R.string.check_internet))
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setTOAdpater() {
        try{
            spareServiceComplaintAdapter = SpareServiceComplaintAdapter(activity!!,complaintresList?.data?.sparecomplaints,this)
            recylCompliantList?.adapter = spareServiceComplaintAdapter
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    override fun connectposition(position: Int, listSize: Int) {
     Log.d("CAlled","Function"+position+"\n"+listSize)
        complaintId =complaintresList?.data?.sparecomplaints?.get(position)?.id.toString()
        try{
            Log.d("PositionEnquiryId",""+complaintresList?.data?.sparecomplaints?.get(position)?.enquiryId)
            Log.d("PositionEnquiryId",""+complaintresList?.data?.sparecomplaints?.get(position)?.complaintStatus)
            if(complaintresList?.data?.sparecomplaints?.get(position)?.complaintStatus?.equals("Send to Plant")!!){
                popupSendtoPlant()
            }else if(complaintresList?.data?.sparecomplaints?.get(position)?.complaintStatus?.equals("View")!!){
                val intent = Intent(activity, CRMDetailsActivity::class.java)
                intent.putExtra("id", "" +complaintresList?.data?.sparecomplaints?.get(position)?.enquiryId)
                intent.putExtra("user_name", "" + complaintresList?.data?.sparecomplaints?.get(position)?.enquiryId)
                intent.putExtra("task_name", "SparesService")
                startActivity(intent)
            }
//            popupSendtoPlant()
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun popupSendtoPlant() {
        try {
            val inflater: LayoutInflater = activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            /**  val view = inflater.inflate(R.layout.dialog_service_warranty_complints, null)**/
            dialogsendToPlantview = inflater.inflate(R.layout.dialog_spare_service_complaint, null)
            /*dialogsendToPlantview = inflater.inflate(R.layout.dialog_spare_service_complaint, null)
            dialogSendToplant?.setContentView(dialogsendToPlantview)
            dialogSendToplant?.setCancelable(true)
            dialogSendToplant?.show()*/
            dialogSendBuilder?.setView(dialogsendToPlantview)
            dialogSend = dialogSendBuilder?.create()
            dialogSend?.show()
            spSendThrough = dialogsendToPlantview?.findViewById<Spinner>(R.id.spsc_sendthrough)
            edtDocketNo = dialogsendToPlantview?.findViewById(R.id.edt_spsc_docket)
            liCourier = dialogsendToPlantview?.findViewById(R.id.li_spsc_courier)
            edtDriverName = dialogsendToPlantview?.findViewById(R.id.edt_spsc_dr_name)
            edtMobileNo = dialogsendToPlantview?.findViewById(R.id.edt_spsc_mob_no)
            edtVide = dialogsendToPlantview?.findViewById(R.id.edt_spsc_vide)
            btnclose = dialogsendToPlantview?.findViewById(R.id.btn_spsc_close)
            btnsend = dialogsendToPlantview?.findViewById(R.id.btn_spsc_snd_plnt)
            sendThroughSTRList.clear()
            sendThroughSTRList.add(getString(R.string.send_through))
            sendThroughSTRList.add(getString(R.string.courier))
            sendThroughSTRList.add(getString(R.string.direct))
            sendThroughSTRList.add("Transport")
            sendThroughSTRAdapter = ArrayAdapter(activity, R.layout.spinnertext, R.id.text1, sendThroughSTRList)
            spSendThrough?.adapter = sendThroughSTRAdapter
            btnclose?.setOnClickListener {
                if (dialogSend?.isShowing!!) {
                    dialogSend?.dismiss()
                } else {
                    try {
                        dialogSend?.dismiss()
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                }
            }
            btnsend?.setOnClickListener {
                try {
                    val sendthough = spSendThrough?.selectedItem.toString()
                    val docketno = edtDocketNo?.text.toString()
                    val drivername = edtDriverName?.text.toString()
                    val mobileno = edtMobileNo?.text.toString()
                    val vide = edtVide?.text.toString()
                    val map: HashMap<String, Any> = HashMap()
                    if (sendthough.equals(getString(R.string.direct))) {

                        map.put("send_through",sendthough)
                        map.put("docket", "")
                        map.put("vide", "")
                        map.put("driver_phone_no", "")
                        map.put("driver_name", "")
                        senToServer(map)
                    }else if(sendthough.equals("Transport")){
                        if(validatetran(mobileno,drivername)){
                        edtDocketNo?.visibility = View.GONE
                        edtDriverName?.visibility =View.VISIBLE
                        edtMobileNo?.visibility =View.VISIBLE
                        edtVide?.visibility =View.GONE
                        map.put("send_through",sendthough)
                        map.put("docket", "")
                        map.put("vide", "")
                        map.put("driver_phone_no", mobileno)
                        map.put("driver_name", drivername)
                        senToServer(map)
                        }
                    }
                    else if(sendthough.equals(getString(R.string.courier)))  {
                        if (validatecou( docketno, vide)) {
                            map.put("send_through", sendthough)
                            map.put("docket", docketno)
                            map.put("vide", vide)
                            map.put("driver_phone_no", "")
                            map.put("driver_name", "")
                            senToServer(map)
                        } else {
                            Log.d("currentState", "" + isStateSaved)
                        }
                    }else{
                        MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Select The Send Through")
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
       spSendThrough?.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
           override fun onNothingSelected(parent: AdapterView<*>?) {
               Log.d("NothingSelected",""+parent?.selectedItem.toString())
           }

           override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
               Log.d("Selected",""+spSendThrough?.selectedItem.toString())
               if(spSendThrough?.selectedItem.toString().equals(getString(R.string.courier))){
                   edtDocketNo?.visibility = View.VISIBLE
                   edtDriverName?.visibility =View.GONE
                   edtMobileNo?.visibility =View.GONE
                   edtVide?.visibility =View.VISIBLE
               }
               else if(spSendThrough?.selectedItem.toString().equals("Transport")){
                   edtDocketNo?.visibility = View.GONE
                   edtDriverName?.visibility =View.VISIBLE
                   edtMobileNo?.visibility =View.VISIBLE
                   edtVide?.visibility =View.GONE
               }
               else{
                   edtDocketNo?.visibility = View.GONE
                   edtDriverName?.visibility =View.GONE
                   edtMobileNo?.visibility =View.GONE
                   edtVide?.visibility =View.GONE

               }
           }

       }
    }

    private fun validatetran(mobileno: String, drivername: String): Boolean {
        try{
            if(mobileno.isEmpty()  || mobileno.length !=10){
                MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Mobile Number is Can't Be Empty")
                return false
            }else if(drivername.isEmpty()){
                MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Drive Name can't Be Empty")
                return false
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
        return true

    }

    private fun validatecou(docketno: String, vide: String): Boolean {
        try{
            if(docketno.isEmpty()){
                MessageUtils.showSnackBar(activity,dialogsendToPlantview,"docketno Can't Be Empty")
                return false
            }else if(vide.isEmpty()) {
                MessageUtils.showSnackBar(activity, dialogsendToPlantview, "Vide Can't Be Empty")
                return false

            }
            }catch (ex:Exception){
            ex.printStackTrace()
        }
        return true
    }

    private fun senToServer( map :HashMap<String,Any> ) {
        try {
            map.put("userid",LocalSession.getUserInfo(activity, KEY_ID))
            if(!complaintId.isEmpty()){
                map.put("complaint_id",Integer.valueOf(complaintId))
            }
            dialog =MessageUtils.showDialog(activity)
            val calinterface = callApi?.connectRetro("POST","sendsparesforservice")
            val callback = calinterface?.call_post("sendsparesforservice","Bearer "+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
            callback?.enqueue(object:Callback<ResponseBody>{
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    MessageUtils.showSnackBar(activity,dialogsendToPlantview,t.message)

                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    if(response?.isSuccessful){

                        try{
                            val jsonObject = JSONObject(response.body()?.string()!!)
                            val sucsess = jsonObject.getBoolean("success")
                            val message = jsonObject.getString("message")
                            Log.d("REsponse",""+jsonObject.toString())
                            if(sucsess){
                                MessageUtils.showToastMessageLong(activity,message)
                                if(dialogSend?.isShowing!!){
                                    dialogSend?.dismiss()
                                }
                                getDataFrmSrvr()
                            }else{
                                MessageUtils.showSnackBar(activity,dialogsendToPlantview,message)
                            }
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                    }else{
                        MessageUtils.showSnackBar(activity,dialogsendToPlantview,response.body().toString())
                    }
                }
            })
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun validate(sendthough: String, docketno: String, vide: String, mobileno: String, drivername: String): Boolean {
            if(sendthough.isEmpty() || sendthough.equals(getString(R.string.send_through))){
//                Snackbar.make(dialogsendToPlantview!!, "Select Valid send Through", Snackbar.LENGTH_LONG).show()
                MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Select Valid send Through")
                return false
            }else if(docketno.isEmpty()){
                MessageUtils.showSnackBar(activity,dialogsendToPlantview,"docketno Can't Be Empty")
                return false
            }else if(vide.isEmpty()){
                MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Vide Can't Be Empty")
                return false
            }else if(mobileno.isEmpty()){
                MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Mobile Number is Can't Be Empty")
                return false
            }else if(drivername.isEmpty()){
                MessageUtils.showSnackBar(activity,dialogsendToPlantview,"Drive Name can't Be Empty")
                return false
            }
        return true
    }

}
