package com.ampere.bike.ampere.views.fragment.parthi.Inventory.VehicleStock.Adapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ampere.bike.ampere.views.fragment.parthi.Inventory.VehicleStock.Pojos.VehicleStockPojo;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class VehicleStockAdapter extends RecyclerView.Adapter<VehicleStockAdapter.ViewHolder> {
    ArrayList<VehicleStockPojo> vehicleStockPojosArrayList;
    Context context;

    public VehicleStockAdapter(ArrayList<VehicleStockPojo> vehicleStockPojosArrayList, Context context) {
        this.vehicleStockPojosArrayList = vehicleStockPojosArrayList;
        this.context = context;
    }

    public  void setList(  ArrayList<VehicleStockPojo> vehicleStockPojosArrayList){
        this.vehicleStockPojosArrayList = vehicleStockPojosArrayList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_inventory_vehicle_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try{

        holder.srlNo.setText(String.valueOf(position+1));
       if(vehicleStockPojosArrayList.get(position).getId() == null){
            holder.vehicleColor.setText("-");
        }else{
            holder.vehicleColor.setText(String.valueOf(vehicleStockPojosArrayList.get(position).getColor()));
           holder.vehicleColor.setSingleLine();
           holder.vehicleColor.setEllipsize(TextUtils.TruncateAt.MARQUEE);
           holder.vehicleColor.setSelected(true);
        }
        if(vehicleStockPojosArrayList.get(position).getVehicleModel() == null ||
                vehicleStockPojosArrayList.get(position).getVehicleModel().isEmpty()){

            holder.vehicleModel.setText("-");
        }else{
            holder.vehicleModel.setText(vehicleStockPojosArrayList.get(position).getVehicleModel());
            holder.vehicleModel.setSingleLine();
            holder.vehicleModel.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.vehicleModel.setSelected(true);
        }

        if(vehicleStockPojosArrayList.get(position).getBatterySerialNo() == null
                || vehicleStockPojosArrayList.get(position).getBatterySerialNo().isEmpty()){
            holder.vehicleChassisNo.setText("-");
        }else{
            holder.vehicleChassisNo.setText(vehicleStockPojosArrayList.get(position).getChassisNo() );
            holder.vehicleChassisNo.setSingleLine();
            holder.vehicleChassisNo.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.vehicleChassisNo.setSelected(true);
        }


        if(vehicleStockPojosArrayList.get(position).getIndentInvoiceDate() == null
                || vehicleStockPojosArrayList.get(position).getIndentInvoiceDate().isEmpty()){
            holder.vehicleInvoiceDate.setText("-");
        }else{
            holder.vehicleInvoiceDate.setText(vehicleStockPojosArrayList.get(position).getIndentInvoiceDate());
            holder.vehicleInvoiceDate.setSingleLine();
            holder.vehicleInvoiceDate.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.vehicleInvoiceDate.setSelected(true);
        }


        if(vehicleStockPojosArrayList.get(position).getStatus() == null
                || vehicleStockPojosArrayList.get(position).getStatus().isEmpty()){
            holder.vehicleStatus.setText("-");
        }else{
            holder.vehicleStatus.setText(vehicleStockPojosArrayList.get(position).getStatus());
            holder.vehicleStatus.setSingleLine();
            holder.vehicleStatus.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.vehicleStatus.setSelected(true);
        }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return vehicleStockPojosArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView srlNo, vehicleColor, vehicleChassisNo, vehicleInvoiceDate, vehicleStatus, vehicleModel;
        public ViewHolder(View itemView) {
            super(itemView);
            srlNo =itemView.findViewById(R.id.sERNUM);
            vehicleColor =itemView.findViewById(R.id.vehicle_color);
            vehicleChassisNo =itemView.findViewById(R.id.vehicle_serl_num);
            vehicleInvoiceDate = itemView.findViewById(R.id.vehicle_invoice_date);
            vehicleStatus =itemView.findViewById(R.id.vehicle_status);
            vehicleModel =itemView.findViewById(R.id.vehicle_model);
        }
    }
}

