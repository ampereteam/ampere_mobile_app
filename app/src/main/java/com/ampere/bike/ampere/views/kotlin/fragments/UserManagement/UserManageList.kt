package com.ampere.bike.ampere.views.kotlin.fragments.UserManagement


import android.annotation.SuppressLint
import android.app.Dialog
import android.app.FragmentManager
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.CustomNavigationDuo.snackVIew
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.fragments.UserManagement.Adapter.UserListAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.UserManagement.Pojo.UserList.UserListRes
import com.ampere.bike.ampere.views.kotlin.fragments.inventory.StocksAcceptFragment.Companion.fragment

import com.ampere.vehicles.R
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserManageList : Fragment() ,ConfigClickEvent{


    companion object {
        var userListRes :UserListRes ?=null
        var userCRNTId :String =""
        var userCRNTPositon :String =""
    }
    var  utilsCallApi :UtilsCallApi ?= null
    var recyUserList:RecyclerView ?= null
    var tvEmptyList:TextView ?= null
    var fbtnAdduser:FloatingActionButton ?= null
    var dialog : Dialog ?= null
    var userlistaAdpter : UserListAdapter ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        (activity as CustomNavigationDuo).disbleNavigationView(getString(R.string.user_mgnt))
        return inflater.inflate(R.layout.fragment_user_manage_list, container, false)
    }

    @SuppressLint("CommitTransaction")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        utilsCallApi = UtilsCallApi(activity)
        dialog = Dialog(activity)
        recyUserList =view.findViewById(R.id.recy_usr_list)
        fbtnAdduser =view.findViewById(R.id.fbtn_add_user)
        tvEmptyList =view.findViewById(R.id.emptyList)
        recyUserList?.layoutManager = LinearLayoutManager(activity,LinearLayout.VERTICAL,false)
        if(utilsCallApi?.isNetworkConnected!!){
        /**get UserType And Code **/
        getUserTypeandCode()
        }else{
            MessageUtils.showSnackBarAction(activity,snackVIew,getString(R.string.check_internet))
        }

        recyUserList?.addOnScrollListener(object :RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if(dy >0 ){
                    fbtnAdduser?.visibility = View.GONE
                }else{
                    fbtnAdduser?.visibility =View.VISIBLE
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })

        fbtnAdduser?.setOnClickListener {
            try{
                val fragment :Fragment = AddUsermangement()
                activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.container,fragment)?.addToBackStack(null)?.commit()
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }


    }
    private fun getUserTypeandCode() {
        try{
            val hashMap :HashMap<String,Any> = HashMap()
            hashMap["userid"] = LocalSession.getUserInfo(activity, LocalSession.KEY_ID)
            dialog  = MessageUtils.showDialog(activity)
            val callInterface = utilsCallApi?.connectRetro("POST","usermanagement")
            val callback  = callInterface?.call_post("usermanagement","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),hashMap)
            callback?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    try{
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.localizedMessage)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }

                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    try{
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                userListRes = Gson().fromJson(response.body()?.string(),UserListRes::class.java)
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }
                            if(userListRes?.success!!){
                                        if(userListRes?.data?.users?.size!! <=0){
                                            tvEmptyList?.setText(userListRes?.message)
                                            tvEmptyList?.visibility =View.VISIBLE
                                            recyUserList?.visibility = View.GONE
                                        }else{
                                            Log.d("ArrayListValueForUSer",""+userListRes?.data?.users?.get(0)?.name)
                                            tvEmptyList?.visibility =View.GONE
                                            recyUserList?.visibility = View.VISIBLE
                                            setToAdapter()
                                        }

                            }else{
                                MessageUtils.showSnackBar(activity, snackVIew, userListRes?.message)
                            }
                        }else{
                            MessageUtils.showSnackBar(activity, snackVIew,response.message())
                        }
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

            })
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setToAdapter() {
        try{
            userlistaAdpter  = UserListAdapter(activity!!, userListRes?.data?.users,this)
            recyUserList?.adapter = userlistaAdpter
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
    override fun connectposition(position: Int, listSize: Int) {
        try{
            Log.d("PositionCalled",""+ userListRes?.data?.users?.get(position)?.mobile)
            userCRNTId = userListRes?.data?.users?.get(position)?.id.toString()
            userCRNTPositon = position.toString()
            val fragment :Fragment = EditUserManagement()
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.container,fragment)?.addToBackStack(null)?.commit()
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
}
