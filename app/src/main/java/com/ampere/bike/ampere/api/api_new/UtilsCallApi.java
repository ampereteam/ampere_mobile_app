package com.ampere.bike.ampere.api.api_new;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.ampere.bike.ampere.api.RetrofitCall;
import com.ampere.bike.ampere.api.RetrofitService;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ampere.bike.ampere.shared.LocalSession.BEARER;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;

public class UtilsCallApi {


    public FragmentActivity activity;
    public UtilsUICallBack callBackResponse;


    public UtilsCallApi(FragmentActivity activity, UtilsUICallBack responsebackToClass) {
        this.activity = activity;
        this.callBackResponse = responsebackToClass;
    }

    public UtilsCallApi(FragmentActivity activity) {
        this.activity = activity;
    }

    public RetrofitService connectRetro(String annonationType, String method) {

        System.out.println("CHECK_DATA ANNONTAION TYPE :" + annonationType + " & method :" + method);
        if (annonationType.equals("POST")) {
            System.out.println("CHECK_DATA INSIDE APP URL :" + method);

        }
        final OkHttpClient objOkHttpClient = new OkHttpClient.Builder().
                readTimeout(1, TimeUnit.MINUTES).
                connectTimeout(100, TimeUnit.SECONDS).
                addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).
                build();
        Retrofit objRetrofit = new Retrofit.Builder().client(objOkHttpClient).
                baseUrl(RetrofitCall.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        return objRetrofit.create(RetrofitService.class);
    }


    public void callApi(final String apiName1, final String callType, HashMap<String, Object> map, final int position) { // Receive call from class and return values from API
        //callBackResponse = responsebackToClass;
        Call<ResponseBody> objCallApi = null;
        if (callType.equals("POST")) {
            if (map == null) {
                Log.v("message","");
            } else {
                objCallApi = connectRetro(callType, apiName1).call_post(apiName1,BEARER+LocalSession.getUserInfo(activity,KEY_TOKEN), map);
            }
        }
        objCallApi.enqueue(new Callback<ResponseBody>() {
            //JsonObject data;
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                System.out.println("CHECK_DATA response :" + response.isSuccessful());
                System.out.println("CHECK_DATA response :" + response.isSuccessful());
                if (response.isSuccessful()) {
                    Log.d("sdssd", "" + response.body());

                    try {
                        JSONObject objJsonObject = new JSONObject(response.body().string());
                        System.out.println("SpareInvoicePojos:" + apiName1 + "---" + objJsonObject.toString());
                        boolean success = objJsonObject.getBoolean("success");
                        String message = objJsonObject.getString("message");
                        System.out.println("response   message for success   :" + success);
                        JSONObject data = objJsonObject.getJSONObject("data");
                        if(callBackResponse!=null){
                            callBackResponse.apiCallBackOverRideMethod(data, position,success,message,apiName1);
                        }


                        return;
                    } catch (JSONException e) {
                        e.printStackTrace();

                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("SpareInvoicePojos RITRO Failure:" + apiName1 + "---" + t.toString());

                //callBackResponse.apiCallBackOverRideMethod(null, null, apiName1, position, t.getMessage());



            }
        });
    }


    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

}
