package com.ampere.bike.ampere.model.enquiry;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ModelLeadsInfo {

    @SerializedName("success")
    public boolean isStatus;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public ResultObj data;

    public class ResultObj {

        @SerializedName("leadsource")
        public ArrayList<LeadData> leadeSources;


        @SerializedName("vehiclemodel")
        public ArrayList<VehicleData> vehiclemodel;


        @SerializedName("tasks")
        public ArrayList<Tasks> tasks;


        @SerializedName("states")
        public ArrayList<States> states;

        @SerializedName("serviceissues")
        public ArrayList<ServiceIssue> serviceIssues;

/*
        @SerializedName("cities")
        public ArrayList<Cities> cities;*/


        public class LeadData {

            @SerializedName("id")
            public String id;

            @SerializedName("leadsource")
            public String leadsource;
        }


        public class VehicleData {


            @SerializedName("id")
            public String id;

            @SerializedName("product_type")
            public String product_type;

            @SerializedName("vehicle_model")
            public String vehicle_model;

            @SerializedName("varient1")
            public String warient1;

            @SerializedName("varient2")
            public String warient2;
        }


    }


    public class Tasks {
        @SerializedName("id")
        public String id;

        @SerializedName("task_type")
        public String task_type;

        @SerializedName("task")
        public String task;


    }

    public class Cities {
        @SerializedName("id")
        public String id;

        @SerializedName("city")
        public String city;

    }

    public static class States {
        @SerializedName("id")
        public String id;

        @SerializedName("state_name")
        public String state_name;

        @SerializedName("tin_number")
        public String tin_number;

        @SerializedName("state_code")
        public String state_code;

    }


    public class ServiceIssue {

        @SerializedName("id")
        public String id;

        @SerializedName("service_issue")
        public String service_issue;

    }
}
