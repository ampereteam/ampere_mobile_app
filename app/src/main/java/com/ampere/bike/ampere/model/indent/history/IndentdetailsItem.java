package com.ampere.bike.ampere.model.indent.history;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class IndentdetailsItem implements Serializable{

	@SerializedName("indent_id")
	public String indentId;

	@SerializedName("indent_type")
	public String indentType;

	@SerializedName("updated_at")
	public String updatedAt;

	@SerializedName("qty")
	public String qty;

	@SerializedName("vehicle_model")
	public String vehicleModel;

	@SerializedName("created_at")
	public String createdAt;

	@SerializedName("id")
	public int id;

	@SerializedName("spare_name")
	public String spareName;

	@Override
 	public String toString(){
		return 
			"IndentdetailsItem{" + 
			"indent_id = '" + indentId + '\'' + 
			",indent_type = '" + indentType + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",qty = '" + qty + '\'' + 
			",vehicle_model = '" + vehicleModel + '\'' +
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",spare_name = '" + spareName + '\'' + 
			"}";
		}
}