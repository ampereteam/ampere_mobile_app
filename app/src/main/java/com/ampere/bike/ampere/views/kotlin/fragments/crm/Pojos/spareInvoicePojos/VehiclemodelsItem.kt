package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoicePojos

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class VehiclemodelsItem(

	@field:SerializedName("has_varient")
	val hasVarient: String? = null,

	@field:SerializedName("vehicle_model")
	val vehicleModel: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("item_name")
	val itemName: String? = null,

	@field:SerializedName("product_type_id")
	val productTypeId: Int? = null,

	@field:SerializedName("colors")
	val colors: String? = null,

	@field:SerializedName("uom")
	val uom: Any? = null,

	@field:SerializedName("model_code")
	val modelCode: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("subsidy_amount")
	val subsidyAmount: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("item_description")
	val itemDescription: Any? = null,

	@field:SerializedName("code_number")
	val codeNumber: Int? = null
)