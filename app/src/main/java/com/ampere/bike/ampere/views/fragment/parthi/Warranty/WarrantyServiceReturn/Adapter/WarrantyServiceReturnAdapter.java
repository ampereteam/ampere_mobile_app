package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyServiceReturn.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;


import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyServiceReturn.Pojos.ServiceReturnPojo;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class WarrantyServiceReturnAdapter extends RecyclerView.Adapter<WarrantyServiceReturnAdapter.ViewHolder> {
    Context context;
    ArrayList<ServiceReturnPojo> serviceReturnPojoArrayList;
    ConfigClickEvent configposition;
    int i;

    public WarrantyServiceReturnAdapter(Context context, ArrayList<ServiceReturnPojo> serviceReturnPojoArrayList, ConfigClickEvent configposition) {
        this.context = context;
        this.serviceReturnPojoArrayList = serviceReturnPojoArrayList;
        this.configposition = configposition;

    }
    public  void  setList(ArrayList<ServiceReturnPojo> serviceReturnPojoArrayList){
        this.serviceReturnPojoArrayList = serviceReturnPojoArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_warranty_service_return,parent,false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
       /* for(int j=0;j<serviceReturnPojoArrayList.size();j++){
            if(serviceReturnPojoArrayList.get(j).isCheck()){
                holder.checkBox.setChecked(true);
            }else{
                holder.checkBox.setChecked(false);
            }
        }*/
       try{
        if(serviceReturnPojoArrayList.get(position).isCheck()){
            holder.checkBox.setChecked(true);
            i=serviceReturnPojoArrayList.size();
        }else{
            holder.checkBox.setChecked(false);
            i=0;
        }



        holder.sno.setText(String.valueOf(position+1));

        if(serviceReturnPojoArrayList.get(position).getDocket() == null
                || serviceReturnPojoArrayList.get(position).getDocket() .isEmpty()){
            holder.docketNo.setText("-");
        }else{
            holder.docketNo.setText(serviceReturnPojoArrayList.get(position).getDocket());
            holder.docketNo.setSingleLine();
            holder.docketNo.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.docketNo.setSelected(true);
        }

        if(serviceReturnPojoArrayList.get(position).getDefectServiceType() == null
                || serviceReturnPojoArrayList.get(position).getDefectServiceType().isEmpty()){
            holder.serviceType.setText("-");
        }else{
            holder.serviceType.setText(serviceReturnPojoArrayList.get(position).getDefectServiceType());
            holder.serviceType.setSingleLine();
            holder.serviceType.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.serviceType.setSelected(true);

        }





        if(serviceReturnPojoArrayList.get(position).getDefectSerialNo() == null
                || serviceReturnPojoArrayList.get(position).getDefectSerialNo().isEmpty()){
            holder.serialno.setText("-");
        }else{
            holder.serialno.setText(serviceReturnPojoArrayList.get(position).getDefectSerialNo());
        }



        if(serviceReturnPojoArrayList.get(position).getChassisNo() == null
                || serviceReturnPojoArrayList.get(position).getChassisNo().isEmpty()){
            holder.chsno.setText("-");
        }else{
            holder.chsno.setText(serviceReturnPojoArrayList.get(position).getChassisNo());
            holder.chsno.setSingleLine();
            holder.chsno.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.chsno.setSelected(true);

        }






        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            serviceReturnPojoArrayList.get(position).setCheck(isChecked);
                            if(serviceReturnPojoArrayList.get(position).isCheck()){

                                if(i==serviceReturnPojoArrayList.size()){
                                    configposition.connectposition(position,i);
                                }else{
                                    configposition.connectposition(position,++i);
                                }

                            }else{
                                if(i==0){
                                    configposition.connectposition(position,0);
                                }else{
                                    configposition.connectposition(position,--i);
                                }
                            }
            }
        });

    }catch (Exception e){
        e.printStackTrace();
    }

    }

    @Override
    public int getItemCount() {
        return serviceReturnPojoArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView sno,chsno,serialno,serviceType,docketNo;
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            sno=itemView.findViewById(R.id.s_no);
            chsno=itemView.findViewById(R.id.chas_no);
            serialno=itemView.findViewById(R.id.seriallNo);
            serviceType=itemView.findViewById(R.id.srvetType);
            docketNo=itemView.findViewById(R.id.docketno);
            checkBox=itemView.findViewById(R.id.markkk);
        }

    }
}
