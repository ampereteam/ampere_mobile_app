package com.ampere.bike.ampere.views.kotlin.fragments.SpareService.DefectReturn.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.DefectReturn.Pojos.DefectreturnlistItem
import com.ampere.vehicles.R

class DefectRetunAdapter(val context: Context, val defectList: ArrayList<DefectreturnlistItem?>?, val configClickEvent: ConfigClickEvent)
    : RecyclerView.Adapter<DefectRetunAdapter.ViewHolder>(){
    var i:Int =0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DefectRetunAdapter.ViewHolder {
        val view = LayoutInflater.from(this.context).inflate(R.layout.adapter_spareservice_defectreturn,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return  defectList?.size!!
    }

    override fun onBindViewHolder(holder: DefectRetunAdapter.ViewHolder, position: Int) {
        try{
            i=defectList?.size!!
            for(i:Int in 0..defectList?.size!!-1){
                holder.checkbox.isChecked=defectList[position]?.check!!
            }
            holder.sno.text = (position+1).toString()
//            holder.chassi.text =defectList!![position]?.chassisNo
            holder.component.text = defectList[position]?.component
            holder.oldSno.text = defectList[position]?.oldSerialNo
            holder.newSno.text =defectList[position]?.newSerialNo
            /*holder.linearLayout.setOnClickListener {
                this.configClickEvent.connectposition(position,invoiceList.size)
            }*/
            if (defectList.get(position)?.check!!) {
                holder.checkbox.setChecked(true)
                i = defectList.size
            } else {
                holder.checkbox.setChecked(false)
                i = 0
            }
            holder.checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                defectList.get(position)!!.check=isChecked
                if (defectList.get(position)!!.check!!) {
                    if (defectList.size == i) {
                        configClickEvent.connectposition(position, i)
                    } else {
                        configClickEvent.connectposition(position, ++i)
                    }

                } else {
                    if (i != 0) {
                        configClickEvent.connectposition(position, --i)
                    } else {
                        configClickEvent.connectposition(position, 0)
                    }
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }
    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val sno : TextView = itemView?.findViewById(R.id.spsdr_s_no)!!
//        val chassi : TextView = itemView?.findViewById(R.id.spsdr_chassis)!!
        val component : TextView = itemView?.findViewById(R.id.spsdr_component)!!
        val oldSno : TextView = itemView?.findViewById(R.id.spsdr_old_sno)!!
        val newSno : TextView = itemView?.findViewById(R.id.spsdr_new_sno)!!
        val linearLayout : LinearLayout = itemView?.findViewById(R.id.spsdr_linear_adp)!!
        val checkbox : CheckBox = itemView?.findViewById(R.id.spsdr_checkbox)!!
    }
}