package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.ServiceEnquiryAdapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.ServiceEnquiryServerPojo.NewReplacementServerPojo
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.Replacement
import com.ampere.vehicles.R

class NewReplacementAdapter (val context:Context,val newREplacementArrayList: ArrayList<Replacement?>?,val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<NewReplacementAdapter.Viewholder>(){
        var newSerialSTRList :ArrayList<String> = ArrayList()
        var newSerialSTRAdapter : ArrayAdapter<String> ?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewReplacementAdapter.Viewholder {
        val view  =LayoutInflater.from(this.context).inflate(R.layout.adapter_service_enquiry_new_replacement,parent,false)
        return Viewholder(view)
    }

    override fun getItemCount(): Int { return this.newREplacementArrayList?.size!!
    }

    override fun onBindViewHolder(holder: NewReplacementAdapter.Viewholder, position: Int) {
        try{
            var currentPostion = position
            Log.d("newReplacment",""+ newREplacementArrayList!![position]?.component)
            holder.conmponetName.text = newREplacementArrayList!![position]?.component!!
            holder.old_serial_number.text = this.newREplacementArrayList[position]?.oldserial
            for(newSerialNo  in newREplacementArrayList[position]?.sparesstock!!){
                newSerialSTRList.add(newSerialNo?.serialNo!!)
            }
            newSerialSTRAdapter = ArrayAdapter(this.context,R.layout.spinnertext,R.id.text1,newSerialSTRList)
            holder.spNewSerialNumber .adapter = newSerialSTRAdapter
            holder.spNewSerialNumber.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    try{
                        Log.d("NothingSelected",""+parent?.selectedItem)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    try{
                        val selectedItem = holder.spNewSerialNumber.selectedItem.toString()
                        holder.edtNewSerialNumber.setText(selectedItem)
                        /*** update the Selected Item in The Server ArrayList**/
                        val newReplacement = NewReplacementServerPojo(newREplacementArrayList[currentPostion]?.component,
                                newREplacementArrayList[currentPostion]?.oldserial, selectedItem,  newREplacementArrayList[currentPostion]?.complaint_id
                        )
                        CRMDetailsActivity.newReplacementServerLISt[currentPostion] = newReplacement
                        Log.d("ChsngedITEm",""+CRMDetailsActivity.newReplacementServerLISt[currentPostion].newSerialNo)
                        Log.d("ChsngedITEm",""+CRMDetailsActivity.newReplacementServerLISt[currentPostion].componentName)
                        Log.d("ChsngedITEm",""+CRMDetailsActivity.newReplacementServerLISt[currentPostion].oldserial)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }
            }

            for(newReplacementObject in newREplacementArrayList){
                Log.e("NewReplacement",""+newReplacementObject?.component)
                val newReplacement = NewReplacementServerPojo(newReplacementObject?.component.toString(),
                        newReplacementObject?.oldserial, newReplacementObject?.sparesstock!![0]?.serialNo,
                        newReplacementObject.complaint_id
                )
                Log.e("NewReplacement",""+newReplacement?.newSerialNo)
               CRMDetailsActivity.newReplacementServerLISt.add(newReplacement)
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
    class Viewholder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val conmponetName :TextView = itemView?.findViewById(R.id.bs_edt_component_name)!!
        val old_serial_number :TextView = itemView?.findViewById(R.id.bs_edt_old_serial_number)!!
        val spNewSerialNumber :Spinner = itemView?.findViewById(R.id.bs_sp_new_serial_number)!!
        val edtNewSerialNumber :TextView = itemView?.findViewById(R.id.bs_edt_new_serial_number)!!
        val linear :LinearLayout = itemView?.findViewById(R.id.new_component_replacement)!!
    }
}