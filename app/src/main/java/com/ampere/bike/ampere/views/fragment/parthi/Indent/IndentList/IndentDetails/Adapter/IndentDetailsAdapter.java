package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.IndentDetailsPojos;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class IndentDetailsAdapter extends RecyclerView.Adapter<IndentDetailsAdapter.MyHolder> {
    Context context;
    ArrayList<IndentDetailsPojos> indentDetailsPojosArrayList;
    ConfigClickEvent configposition;

    public IndentDetailsAdapter(Context context, ArrayList<IndentDetailsPojos> indentDetailsPojosArrayList, ConfigClickEvent configposition) {
        this.context = context;
        this.indentDetailsPojosArrayList = indentDetailsPojosArrayList;
        this.configposition = configposition;
    }

    public IndentDetailsAdapter(Context context, ArrayList<IndentDetailsPojos> indentDetailsPojosArrayList) {
        this.context = context;
        this.indentDetailsPojosArrayList = indentDetailsPojosArrayList;
    }

    public void setList(ArrayList<IndentDetailsPojos> indentDetailsPojosArrayList) {
        this.indentDetailsPojosArrayList = indentDetailsPojosArrayList;
    }
    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_indent_details, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, final int position) {
        try{

        if(String.valueOf(indentDetailsPojosArrayList.get(position).getPendingQty()).isEmpty()||String.valueOf(indentDetailsPojosArrayList.get(position).getPendingQty())==null
                || String.valueOf(indentDetailsPojosArrayList.get(position).getPendingQty()).length()==0){
            holder.idntID.setText("-");
        }else{
            holder.idntID.setText(String.valueOf(indentDetailsPojosArrayList.get(position).getPendingQty()));
        }
        if(indentDetailsPojosArrayList.get(position).getVehicleModel()==null || indentDetailsPojosArrayList.get(position).getVehicleModel().isEmpty()
                ||indentDetailsPojosArrayList.get(position).getVehicleModel().length()==0){
            holder.vehicleModel.setText("-");
        }else{
            holder.vehicleModel.setText(indentDetailsPojosArrayList.get(position).getVehicleModel());
        }

        if(indentDetailsPojosArrayList.get(position).getSpareName()==null || indentDetailsPojosArrayList.get(position).getSpareName().isEmpty()
                ||indentDetailsPojosArrayList.get(position).getSpareName().length()==0){
            holder.spareName.setText("-");
        }else{
            holder.spareName.setText(indentDetailsPojosArrayList.get(position).getSpareName());
        }
        if(indentDetailsPojosArrayList.get(position).getQty()==null || indentDetailsPojosArrayList.get(position).getQty().isEmpty()
                ||indentDetailsPojosArrayList.get(position).getQty().length()==0){
            holder.totalQty.setText("-");
        }else{
            holder.totalQty.setText(indentDetailsPojosArrayList.get(position).getQty());
        }





        holder.sNo.setText(String.valueOf(position + 1));
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configposition.connectposition(position, indentDetailsPojosArrayList.size());
            }
        });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return indentDetailsPojosArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView sNo, idntID, vehicleModel, spareName, totalQty;
        RelativeLayout relativeLayout;

        public MyHolder(View itemView) {
            super(itemView);
            sNo = itemView.findViewById(R.id.serial_Num);
            idntID = itemView.findViewById(R.id.indent_id);
            vehicleModel = itemView.findViewById(R.id.model_vari);
            spareName = itemView.findViewById(R.id.spare_name);
            totalQty = itemView.findViewById(R.id.total_qty);
            relativeLayout = itemView.findViewById(R.id.indent_details_brf);
        }
    }
}
