package com.ampere.bike.ampere.api.api_new;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public interface UtilsUICallBack {

    /**
     * Valid SpareInvoicePojos and set UI
     *
     * @param response
     * @param position
     * @throws JSONException
     * @throws IOException
     */
    void apiCallBackOverRideMethod(JSONObject response, int position,boolean success,String message,String  apiCallPath ) throws JSONException, IOException;
}
