package com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints


import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.CustomNavigationDuo.snackVIew
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_ID
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FreeReplacementHome.Companion.msnakview
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.Adapter.FreeRepalcementsComplaintsAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.FrrComplaintDetails.FreeReplacementComplaintDetailsKT
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.Pojos.ComplaintResponsePojos.FrrComplaintResponse

import com.ampere.vehicles.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.dialog_freereplacement_required.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FreeRepalcementsComplaints : Fragment() ,ConfigClickEvent{


    /**
     * Declaration For the Free Repalcement  Complaint
     */
    var frrComplaintResponse :FrrComplaintResponse ?=null
    var rcylViewComplaints:RecyclerView ?=null
    var emptyList:TextView ?=null
    var callApi:UtilsCallApi?= null
    var dialog:Dialog? =null
    var frrComplaintAdapter : FreeRepalcementsComplaintsAdapter?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
            val view :View = inflater.inflate(R.layout.fragment_free_repalcements_complaints, container, false)
        val newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        callApi = UtilsCallApi(activity)
        rcylViewComplaints  = view.findViewById(R.id.frr_complaint_list)
        emptyList  = view.findViewById(R.id.frrc_emptyList)
        dialog = Dialog(activity)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rcylViewComplaints?.layoutManager = LinearLayoutManager(activity,LinearLayout.VERTICAL,false)
        try{
            getComplaintfrmSvr()
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun getComplaintfrmSvr() {
        try {
                val map:HashMap<String,Any> = HashMap()
            map["userid"] = LocalSession.getUserInfo(activity,KEY_ID)
            if(callApi?.isNetworkConnected!!){
                dialog =MessageUtils.showDialog(activity)
                val calinterface = callApi?.connectRetro("POST","freereplacementcomplaintlist")
                val callback  = calinterface?.call_post("freereplacementcomplaintlist",
                        "Bearer "+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
                callback?.enqueue(object: Callback<ResponseBody>{
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        Log.d("Response Failure",""+t.message)
                        Log.d("Response Failure",""+t.localizedMessage)
                        MessageUtils.showSnackBar(activity,snackVIew,getString(R.string.server_not_found))
                    }
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                             frrComplaintResponse = Gson().fromJson(response.body()?.string(),FrrComplaintResponse::class.java)
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }
                            if(frrComplaintResponse?.data?.freecomplaints?.size!! <=0){
                                try{
                                    emptyList?.setText(frrComplaintResponse?.message)
                                }catch (ex:Exception){
                                ex.printStackTrace()
                                }
                                emptyList?.visibility =View.VISIBLE
                            }else{
                                emptyList?.visibility =View.GONE
                            }
                            setToAdapter()
                        }else{
                            MessageUtils.showSnackBar(activity,msnakview,getString(R.string.server_not_found))
                            Log.d("ResponseFailure",""+response.body().toString())
                        }
                    }
                })
            }else{
                MessageUtils.showSnackBarAction(activity,snackVIew,getString(R.string.check_internet))
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setToAdapter() {
        try{
            frrComplaintAdapter = FreeRepalcementsComplaintsAdapter(activity!!,frrComplaintResponse?.data?.freecomplaints,this)
            rcylViewComplaints?.adapter = frrComplaintAdapter
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    override fun connectposition(position: Int, listSize: Int) {
        try{
            Log.d("CallaedInterface",""+position+ "\t"+listSize)
            Log.d("CallaedInterface",""+frrComplaintResponse?.data?.freecomplaints?.get(position)?.id+ "\t"+listSize)
            startActivity(Intent(activity,FreeReplacementComplaintDetailsKT::class.java).
                    putExtra("complaintid",frrComplaintResponse?.data?.freecomplaints?.get(position)?.id.toString()))
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }
}
