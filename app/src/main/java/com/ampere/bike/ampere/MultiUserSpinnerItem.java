package com.ampere.bike.ampere;

public class MultiUserSpinnerItem {

    private String id;
    private String name;

    /*public MultiItemSpinner(@Nullable String id, @Nullable String name) {

    }*/

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultiUserSpinnerItem(String name, String id) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
