package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyInvoice.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;


import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyInvoice.Pojos.WarrantyinvoiceItem;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class WarrantyInvoiceAdapter extends RecyclerView.Adapter<WarrantyInvoiceAdapter.ViewHolder> {
    Context context;
    //ArrayList<InvoicePojos> invoicePojosArrayList ;
    ConfigClickEvent configposition;
    ArrayList<WarrantyinvoiceItem> invoicePojosArrayList;
    int listSize;

    public WarrantyInvoiceAdapter(Context context, ArrayList<WarrantyinvoiceItem> invoicePojosArrayList) {
        this.context = context;
        this.invoicePojosArrayList = invoicePojosArrayList;
    }

    public WarrantyInvoiceAdapter(Context context, ArrayList<WarrantyinvoiceItem> invoicePojosArrayList, ConfigClickEvent configposition) {
        this.context = context;
        this.invoicePojosArrayList = invoicePojosArrayList;
        this.configposition = configposition;
    }

    public void setListItem(ArrayList<WarrantyinvoiceItem> invoicePojosArrayList){
        this.invoicePojosArrayList = invoicePojosArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_warranty_invoice,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try{
        listSize=invoicePojosArrayList.size();
        int i = position+1;
        String date = invoicePojosArrayList.get(position).getIndentInvoiceDate();
        String spareName =invoicePojosArrayList.get(position).getSpareName().trim();
        holder.sno.setText(String.valueOf(i));
        System.out.println("ejoyrb99[vh            ArrayList"+invoicePojosArrayList.get(position).getIndentInvoiceDate());
        if(invoicePojosArrayList.get(position).getChassisNo() == null
                || invoicePojosArrayList.get(position).getChassisNo().isEmpty()){
            holder.chassno.setText("-");
        }else{
            holder.chassno.setText(invoicePojosArrayList.get(position).getChassisNo());
            holder.chassno.setSingleLine(true);
            holder.chassno.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.chassno.setSelected(true);
        }

        if(spareName == null
                || spareName.isEmpty()){
            holder.sparename.setText("-");
        }else{
            holder.sparename.setText(spareName.substring(0,spareName.length()-3)+"\n"+spareName.substring(spareName.length()-3,spareName.length()));
            holder.sparename.setSingleLine(true);
            holder.sparename.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.sparename.setSelected(true);
        }
        if(date == null
                || date.isEmpty()){
            holder.indate.setText("-");
        }else{
            holder.indate.setText(" "+date.substring(date.length()-2,date.length())+"-"
                    +date.substring(date.length()-5,date.length()-3)+"-"+
                    date.substring(2,4));
            holder.indate.setSingleLine(true);
            holder.indate.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.indate.setSelected(true);
        }
        if(invoicePojosArrayList.get(position).getVehicleModel() == null
                || invoicePojosArrayList.get(position).getVehicleModel().isEmpty()){
            holder.vmodel.setText("-");
        }else{
            holder.vmodel.setText(invoicePojosArrayList.get(position).getVehicleModel());
            holder.vmodel.setSingleLine(true);
            holder.vmodel.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.vmodel.setSelected(true);
        }


        holder.checkBox.setOnCheckedChangeListener(null);
        if(invoicePojosArrayList.get(position).isCheck()){
            holder.checkBox.setChecked(true);
            listSize=invoicePojosArrayList.size();
        }else{
            holder.checkBox.setChecked(false);
            listSize=0;
        }
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                invoicePojosArrayList.get(position).setCheck(isChecked);
                if(invoicePojosArrayList.get(position).isCheck()){
                    configposition.connectposition(position,++listSize);
                }else{
                    configposition.connectposition(position,--listSize);
                }
            }
        });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return invoicePojosArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView sno,chassno,vmodel,sparename,indate;
        CheckBox checkBox;
        public ViewHolder(View itemView) {
            super(itemView);
            sno = itemView.findViewById(R.id.sERNo);
            chassno = itemView.findViewById(R.id.cHAsNO);
            vmodel = itemView.findViewById(R.id.vModEL);
            sparename = itemView.findViewById(R.id.SPARname);
            indate = itemView.findViewById(R.id.inDate);
            checkBox = itemView.findViewById(R.id.mARK);
        }
    }
}
