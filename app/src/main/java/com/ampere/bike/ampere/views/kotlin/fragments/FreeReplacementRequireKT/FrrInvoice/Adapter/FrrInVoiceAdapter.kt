package com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrInvoice.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrInvoice.Pojos.FreereplacementinvoiceItem

import com.ampere.vehicles.R


class FrrInVoiceAdapter (val context: Context, private val invoiceList: ArrayList<FreereplacementinvoiceItem?>?, private val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<FrrInVoiceAdapter.ViewHolder>(){

    var i :Int = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FrrInVoiceAdapter.ViewHolder {
        val view :View =LayoutInflater.from(this.context).inflate(R.layout.adapter_frrc_invoice,parent,false)
        return  ViewHolder(view)
    }

    override fun getItemCount(): Int {
         return this.invoiceList!!.size
    }

    override fun onBindViewHolder(holder: FrrInVoiceAdapter.ViewHolder, position: Int) {
    try{

        /**set the checkbox checked or Unchecked**/
        for(i:Int in 0 until invoiceList?.size!!-1){
            holder.checkk.isChecked = invoiceList.get(i)?.check!!
        }
                holder.Sno.text = (position+1).toString()
                holder.color.text = this.invoiceList!![position]?.color
                holder.invoiceType.text = this.invoiceList[position]?.invoiceType
                holder.spareName.text = this.invoiceList[position]?.spareName
                holder.status.text = this.invoiceList[position]?.status
                /*holder.linear.setOnClickListener {
                    this.configClickEvent.connectposition(position,invoiceList?.size)
                }*/
        if (invoiceList.get(position)?.check!!) {
            holder.checkk.setChecked(true)
            i = invoiceList.size
        } else {
            holder.checkk.setChecked(false)
            i = 0
        }
        holder.checkk.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
            invoiceList.get(position)!!.check=isChecked

            if (invoiceList.get(position)!!.check!!) {
                if (invoiceList.size == i) {
                    configClickEvent.connectposition(position, i)
                } else {
                    configClickEvent.connectposition(position, ++i)
                }

            } else {
                if (i != 0) {
                    configClickEvent.connectposition(position, --i)
                } else {
                    configClickEvent.connectposition(position, 0)
                }
            }
        })
    }catch (ex:Exception){
        ex.printStackTrace()
    }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val Sno:TextView =itemView?.findViewById(R.id.frrin_s_no_adtr)!!
        val color:TextView =itemView?.findViewById(R.id.frrin_color_adtr)!!
        val invoiceType:TextView =itemView?.findViewById(R.id.frrin_type_adtr)!!
        val spareName:TextView =itemView?.findViewById(R.id.frrin_sparename_adtr)!!
        val status:TextView =itemView?.findViewById(R.id.frrin_status_adtr)!!
        val linear:LinearLayout =itemView?.findViewById(R.id.frrin_linear_adptr)!!
        val checkk: CheckBox =itemView?.findViewById(R.id.frrin_checkbox_adpter)!!
    }
}