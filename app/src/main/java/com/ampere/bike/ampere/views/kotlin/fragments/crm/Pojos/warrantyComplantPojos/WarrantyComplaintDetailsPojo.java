package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.warrantyComplantPojos;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class WarrantyComplaintDetailsPojo{

	@SerializedName("vehicle_color")
	private String vehicleColor;

	@SerializedName("assigned_date")
	private String assignedDate;

	@SerializedName("enquiry_time")
	private String enquiryTime;

	@SerializedName("mtype")
	private Object mtype;

	@SerializedName("vehcile_image")
	private Object vehcileImage;

	@SerializedName("enquiry_no")
	private String enquiryNo;

	@SerializedName("id")
	private int id;

	@SerializedName("state")
	private String state;

	@SerializedName("pincode")
	private String pincode;

	@SerializedName("created_time")
	private String createdTime;

	@SerializedName("locality")
	private String locality;

	@SerializedName("modified_date")
	private String modifiedDate;

	@SerializedName("created_by")
	private String createdBy;

	@SerializedName("complaint_status")
	private String complaintStatus;

	@SerializedName("task")
	private String task;

	@SerializedName("leadsource")
	private String leadsource;

	@SerializedName("enquiry_id")
	private String enquiryId;

	@SerializedName("assigned_to_sub")
	private Object assignedToSub;

	@SerializedName("assignedperson")
	private Assignedperson assignedperson;

	@SerializedName("vehicle_qty")
	private String vehicleQty;

	@SerializedName("assigned_person")
	private int assignedPerson;

	@SerializedName("customer_type")
	private String customerType;

	@SerializedName("closing_date")
	private String closingDate;

	@SerializedName("city")
	private String city;

	@SerializedName("invoice_value")
	private String invoiceValue;

	@SerializedName("mobile_no")
	private String mobileNo;

	@SerializedName("vehicle_model")
	private String vehicleModel;

	@SerializedName("read_status")
	private int readStatus;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("chassis_no")
	private String chassisNo;

	@SerializedName("email")
	private String email;

	@SerializedName("assigned_to")
	private String assignedTo;

	@SerializedName("root_cause")
	private String rootCause;

	@SerializedName("task_status")
	private String taskStatus;

	@SerializedName("address2")
	private String address2;

	@SerializedName("address1")
	private String address1;

	@SerializedName("service_issues")
	private String serviceIssues;

	@SerializedName("enquiry_type")
	private Object enquiryType;

	@SerializedName("message")
	private String message;

	@SerializedName("warranty_component")
	private String warrantyComponent;

	@SerializedName("service_type")
	private String serviceType;

	@SerializedName("spares_id")
	private Object sparesId;

	@SerializedName("enquiry_date")
	private String enquiryDate;

	@SerializedName("modified_by")
	private String modifiedBy;

	@SerializedName("enquiry_for")
	private String enquiryFor;

	@SerializedName("assigned_to_self")
	private String assignedToSelf;

	@SerializedName("customer_name")
	private String customerName;

	@SerializedName("spares_name")
	private Object sparesName;

	@SerializedName("place_of_service")
	private Object placeOfService;

	public void setVehicleColor(String vehicleColor){
		this.vehicleColor = vehicleColor;
	}

	public String getVehicleColor(){
		return vehicleColor;
	}

	public void setAssignedDate(String assignedDate){
		this.assignedDate = assignedDate;
	}

	public String getAssignedDate(){
		return assignedDate;
	}

	public void setEnquiryTime(String enquiryTime){
		this.enquiryTime = enquiryTime;
	}

	public String getEnquiryTime(){
		return enquiryTime;
	}

	public void setMtype(Object mtype){
		this.mtype = mtype;
	}

	public Object getMtype(){
		return mtype;
	}

	public void setVehcileImage(Object vehcileImage){
		this.vehcileImage = vehcileImage;
	}

	public Object getVehcileImage(){
		return vehcileImage;
	}

	public void setEnquiryNo(String enquiryNo){
		this.enquiryNo = enquiryNo;
	}

	public String getEnquiryNo(){
		return enquiryNo;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setPincode(String pincode){
		this.pincode = pincode;
	}

	public String getPincode(){
		return pincode;
	}

	public void setCreatedTime(String createdTime){
		this.createdTime = createdTime;
	}

	public String getCreatedTime(){
		return createdTime;
	}

	public void setLocality(String locality){
		this.locality = locality;
	}

	public String getLocality(){
		return locality;
	}

	public void setModifiedDate(String modifiedDate){
		this.modifiedDate = modifiedDate;
	}

	public String getModifiedDate(){
		return modifiedDate;
	}

	public void setCreatedBy(String createdBy){
		this.createdBy = createdBy;
	}

	public String getCreatedBy(){
		return createdBy;
	}

	public void setComplaintStatus(String complaintStatus){
		this.complaintStatus = complaintStatus;
	}

	public String getComplaintStatus(){
		return complaintStatus;
	}

	public void setTask(String task){
		this.task = task;
	}

	public String getTask(){
		return task;
	}

	public void setLeadsource(String leadsource){
		this.leadsource = leadsource;
	}

	public String getLeadsource(){
		return leadsource;
	}

	public void setEnquiryId(String enquiryId){
		this.enquiryId = enquiryId;
	}

	public String getEnquiryId(){
		return enquiryId;
	}

	public void setAssignedToSub(Object assignedToSub){
		this.assignedToSub = assignedToSub;
	}

	public Object getAssignedToSub(){
		return assignedToSub;
	}

	public void setAssignedperson(Assignedperson assignedperson){
		this.assignedperson = assignedperson;
	}

	public Assignedperson getAssignedperson(){
		return assignedperson;
	}

	public void setVehicleQty(String vehicleQty){
		this.vehicleQty = vehicleQty;
	}

	public String getVehicleQty(){
		return vehicleQty;
	}

	public void setAssignedPerson(int assignedPerson){
		this.assignedPerson = assignedPerson;
	}

	public int getAssignedPerson(){
		return assignedPerson;
	}

	public void setCustomerType(String customerType){
		this.customerType = customerType;
	}

	public String getCustomerType(){
		return customerType;
	}

	public void setClosingDate(String closingDate){
		this.closingDate = closingDate;
	}

	public String getClosingDate(){
		return closingDate;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setInvoiceValue(String invoiceValue){
		this.invoiceValue = invoiceValue;
	}

	public String getInvoiceValue(){
		return invoiceValue;
	}

	public void setMobileNo(String mobileNo){
		this.mobileNo = mobileNo;
	}

	public String getMobileNo(){
		return mobileNo;
	}

	public void setVehicleModel(String vehicleModel){
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleModel(){
		return vehicleModel;
	}

	public void setReadStatus(int readStatus){
		this.readStatus = readStatus;
	}

	public int getReadStatus(){
		return readStatus;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setChassisNo(String chassisNo){
		this.chassisNo = chassisNo;
	}

	public String getChassisNo(){
		return chassisNo;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setAssignedTo(String assignedTo){
		this.assignedTo = assignedTo;
	}

	public String getAssignedTo(){
		return assignedTo;
	}

	public void setRootCause(String rootCause){
		this.rootCause = rootCause;
	}

	public String getRootCause(){
		return rootCause;
	}

	public void setTaskStatus(String taskStatus){
		this.taskStatus = taskStatus;
	}

	public String getTaskStatus(){
		return taskStatus;
	}

	public void setAddress2(String address2){
		this.address2 = address2;
	}

	public String getAddress2(){
		return address2;
	}

	public void setAddress1(String address1){
		this.address1 = address1;
	}

	public String getAddress1(){
		return address1;
	}

	public void setServiceIssues(String serviceIssues){
		this.serviceIssues = serviceIssues;
	}

	public String getServiceIssues(){
		return serviceIssues;
	}

	public void setEnquiryType(Object enquiryType){
		this.enquiryType = enquiryType;
	}

	public Object getEnquiryType(){
		return enquiryType;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setWarrantyComponent(String warrantyComponent){
		this.warrantyComponent = warrantyComponent;
	}

	public String getWarrantyComponent(){
		return warrantyComponent;
	}

	public void setServiceType(String serviceType){
		this.serviceType = serviceType;
	}

	public String getServiceType(){
		return serviceType;
	}

	public void setSparesId(Object sparesId){
		this.sparesId = sparesId;
	}

	public Object getSparesId(){
		return sparesId;
	}

	public void setEnquiryDate(String enquiryDate){
		this.enquiryDate = enquiryDate;
	}

	public String getEnquiryDate(){
		return enquiryDate;
	}

	public void setModifiedBy(String modifiedBy){
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedBy(){
		return modifiedBy;
	}

	public void setEnquiryFor(String enquiryFor){
		this.enquiryFor = enquiryFor;
	}

	public String getEnquiryFor(){
		return enquiryFor;
	}

	public void setAssignedToSelf(String assignedToSelf){
		this.assignedToSelf = assignedToSelf;
	}

	public String getAssignedToSelf(){
		return assignedToSelf;
	}

	public void setCustomerName(String customerName){
		this.customerName = customerName;
	}

	public String getCustomerName(){
		return customerName;
	}

	public void setSparesName(Object sparesName){
		this.sparesName = sparesName;
	}

	public Object getSparesName(){
		return sparesName;
	}

	public void setPlaceOfService(Object placeOfService){
		this.placeOfService = placeOfService;
	}

	public Object getPlaceOfService(){
		return placeOfService;
	}

	@Override
 	public String toString(){
		return 
			"WarrantyComplaintDetailsPojo{" + 
			"vehicle_color = '" + vehicleColor + '\'' + 
			",assigned_date = '" + assignedDate + '\'' + 
			",enquiry_time = '" + enquiryTime + '\'' + 
			",mtype = '" + mtype + '\'' + 
			",vehcile_image = '" + vehcileImage + '\'' + 
			",enquiry_no = '" + enquiryNo + '\'' + 
			",id = '" + id + '\'' + 
			",state = '" + state + '\'' + 
			",pincode = '" + pincode + '\'' + 
			",created_time = '" + createdTime + '\'' + 
			",locality = '" + locality + '\'' + 
			",modified_date = '" + modifiedDate + '\'' + 
			",created_by = '" + createdBy + '\'' + 
			",complaint_status = '" + complaintStatus + '\'' + 
			",task = '" + task + '\'' + 
			",leadsource = '" + leadsource + '\'' + 
			",enquiry_id = '" + enquiryId + '\'' + 
			",assigned_to_sub = '" + assignedToSub + '\'' + 
			",assignedperson = '" + assignedperson + '\'' + 
			",vehicle_qty = '" + vehicleQty + '\'' + 
			",assigned_person = '" + assignedPerson + '\'' + 
			",customer_type = '" + customerType + '\'' + 
			",closing_date = '" + closingDate + '\'' + 
			",city = '" + city + '\'' + 
			",invoice_value = '" + invoiceValue + '\'' + 
			",mobile_no = '" + mobileNo + '\'' + 
			",vehicle_model = '" + vehicleModel + '\'' +
			",read_status = '" + readStatus + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",chassis_no = '" + chassisNo + '\'' + 
			",email = '" + email + '\'' + 
			",assigned_to = '" + assignedTo + '\'' + 
			",root_cause = '" + rootCause + '\'' + 
			",task_status = '" + taskStatus + '\'' + 
			",address2 = '" + address2 + '\'' + 
			",address1 = '" + address1 + '\'' + 
			",service_issues = '" + serviceIssues + '\'' + 
			",enquiry_type = '" + enquiryType + '\'' + 
			",message = '" + message + '\'' + 
			",warranty_component = '" + warrantyComponent + '\'' + 
			",service_type = '" + serviceType + '\'' + 
			",spares_id = '" + sparesId + '\'' + 
			",enquiry_date = '" + enquiryDate + '\'' + 
			",modified_by = '" + modifiedBy + '\'' + 
			",enquiry_for = '" + enquiryFor + '\'' + 
			",assigned_to_self = '" + assignedToSelf + '\'' + 
			",customer_name = '" + customerName + '\'' + 
			",spares_name = '" + sparesName + '\'' + 
			",place_of_service = '" + placeOfService + '\'' + 
			"}";
		}
}