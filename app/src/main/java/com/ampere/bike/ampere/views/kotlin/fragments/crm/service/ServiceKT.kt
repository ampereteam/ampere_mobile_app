package com.ampere.bike.ampere.views.kotlin.fragments.crm.service

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.ampere.bike.ampere.interfaces.RefreshEnquiryTabs
import com.ampere.bike.ampere.model.task.EnquiryModel
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.DateUtils.DD_MM
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.fragment.enquiry.Enquiry
import com.ampere.bike.ampere.views.kotlin.fragments.ViewEventDetailed
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMActivity
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity
import com.ampere.vehicles.R
import customviews.CustomTextView
import kotlinx.android.synthetic.main.frag_list_view.*
import kotlinx.android.synthetic.main.item_crm.view.*
import kotlinx.android.synthetic.main.item_crm_header.*
import kotlinx.android.synthetic.main.item_crm_header.view.*
import java.util.*

class ServiceKT : Fragment(), RefreshEnquiryTabs {
    var fragment: ServiceKT? = null
    var mSalesEnquiryValues: ArrayList<EnquiryModel>? = ArrayList();
    override fun refreshView(enquiryModels: MutableList<EnquiryModel>?) {
        mSalesEnquiryValues = enquiryModels as ArrayList<EnquiryModel>?;
        Log.d("enquiryModels_service", "" + mSalesEnquiryValues!!.size)
        MyUtils.SERVICE = mSalesEnquiryValues
        listViewRefresh()
    }

    fun newInstance(): Fragment? {
        Log.d("fragment_service", "" + fragment);
        if (fragment == null) {
            fragment = ServiceKT();
        }
        return fragment;
    }

    lateinit var mHeaderModel: LinearLayout
    var dialog: Dialog? = null

    companion object {
        var itemService: ItemCRM? = null
        //var noText: CustomTextView? = null

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        Log.d("tab_one_call", "Service 1")
        return inflater.inflate(R.layout.frag_list_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("tab_one_call", "Service 2")
        //noText = view.findViewById(R.id.no_text)

        mHeaderModel = view.crm_header_lay_model


/*
        if (LocalSession.getUserInfo(activity, LocalSession.KEY_USER_TYPE).equals(LocalSession.T_DEALER)) {
            crm_header_name.visibility = View.GONE
            crm_header_action.visibility = View.GONE
        } else {
            crm_header_name.visibility = View.VISIBLE*/
        crm_header_action.visibility = View.GONE
        /*   crm_header_date.visibility = View.VISIBLE
       }*/


        Log.d("MyUtilsserive", "" + MyUtils.SERVICE.size);

        listViewRefresh();

        CRMActivity.refreshServiceView(this)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    fun listViewRefresh() {
        if (mSalesEnquiryValues != null) {
            if (mSalesEnquiryValues!!.size == 0) {
                no_text.text = "Service Not found"
                no_text.visibility = View.VISIBLE
                list_proposal.visibility = View.GONE
                crm_header.visibility = View.GONE
            } else {
                if (isAdded) {
                    crm_header.visibility = View.VISIBLE
                    no_text.visibility = View.GONE
                    list_proposal.visibility = View.VISIBLE
                    list_proposal.layoutManager = LinearLayoutManager(activity)
                    //list_proposal.adapter = activity?.let { ItemCRM(MyUtils.SERVICE, it) }

                    try {
                        Collections.sort(mSalesEnquiryValues, Comparator<EnquiryModel> { crmFilterService, t1 ->
                            var date1 = t1.assignedDate
                            var date2 = crmFilterService.assignedDate
                            //Log.d("date2sdsd_service", "" + date2 + " date1 " + date1)
                            if (date1 != null && date2 != null) {
                                date1.compareTo(date2)
                            } else {
                                -1
                            }
                        })
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    itemService = ItemCRM(mSalesEnquiryValues!!, activity!!)
                    list_proposal.adapter = itemService
                }
            }

        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("tab_one_call", "Service 3")
    }

    class ItemCRM(val item: List<EnquiryModel>, val context: FragmentActivity) : RecyclerView.Adapter<HolderSales>() {
        var items = item


        fun filterList(filterdNames: List<EnquiryModel>) {
            items = filterdNames
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderSales {
            return HolderSales(LayoutInflater.from(context).inflate(R.layout.item_crm, parent, false))
        }

        override fun getItemCount(): Int {
            return items.size
        }


        @SuppressLint("MissingPermission")
        @RequiresApi(Build.VERSION_CODES.M)
        override fun onBindViewHolder(holder: HolderSales, position: Int) {

            CustomTextView.marquee(holder.itemTask)
            CustomTextView.marquee(holder.itemDate)
            CustomTextView.marquee(holder.itemStatus)
            CustomTextView.marquee(holder.itemName)

            CustomTextView.marquee(holder.itemModel)

            /*   if (LocalSession.getUserInfo(context, LocalSession.KEY_USER_TYPE).equals(LocalSession.T_DEALER)) {
                   holder.itemName.visibility = View.GONE
               } else {*/
            // Log.d("namess_service", "" + items[position].customerName);
            holder.itemName.text = items[position].customerName
            holder.itemName.visibility = View.VISIBLE
            //}

            holder?.itemTask?.text = items[position].task
            if (items[position].assignedDate != null) {
                holder?.itemDate?.text = DateUtils.convertDates(items[position].assignedDate, DateUtils.YYYY_MM_DD, DD_MM)
            } else {
                holder?.itemDate?.text = ""
            }


            holder?.itemStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            holder.itemStatus.isClickable = false
            holder.itemStatus.isLongClickable = false
            holder.itemModel.text = items[position].vehicleModel

            holder.itemAction.visibility = View.GONE
            holder?.itemStatus.visibility = View.VISIBLE


            val isCheck = items[position].isChecked
            if (isCheck) {
                holder?.itemStatus.text = "" + items[position].taskStatus
            } else {
                holder?.itemStatus.text = items[position].taskStatus
            }

            //}
            val param = holder.itemLayout.layoutParams as LinearLayout.LayoutParams
            param.setMargins(8, 8, 8, 8);
            holder.itemLayout.layoutParams = param


            holder.snack_view.setOnClickListener {


                if( items[position].id == null ||  items[position].id <=0 ){
                    MyUtils.isEmpty(context, item[position].id.toString(),"Enquiry id  is Null")
                }else{
                    var bundle = Bundle()
                    var fragment = ViewEventDetailed()
                    fragment.arguments = bundle
                    val intent = Intent(context, CRMDetailsActivity::class.java)
                    intent.putExtra("id", /*"12"*/ ""+ items[position].id.toString())
                    Log.d("enquiry Id.",""+item[position].id)
                    intent.putExtra("user_name", "" + items[position].customerName)
                    intent.putExtra("task_name", "" + context.getString(R.string.service))
                    ContextCompat.startActivity(context, intent, bundle)
                }


            }

            holder.itemCall.setOnClickListener(View.OnClickListener {
                if (isReadLocationAllowed()) {
                    context.startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:" + items[position].mobileNo.toString())));
                } else {

                    requestLocationPermission()
                    MessageUtils.showToastMessageLong(context, "Please enable permission in App Settings.")

                }

            })


            holder.itemEmail.setOnClickListener {
                var email = Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, items[position].email);
                email.putExtra(Intent.EXTRA_SUBJECT, "Add Default Subject");
                email.putExtra(Intent.EXTRA_TEXT, "Add Default Message");
                email.setType("message/rfc822");
                context.startActivity(Intent.createChooser(email, "Testing Email Sent"));
            }
/*
            holder.itemStatus.setOnClickListener(View.OnClickListener {
                var statusValues = arrayOf(context.getString(R.string.proposals), context.getString(R.string.negotiation), context.getString(R.string.closure))
                var statusCurrent = items[position].taskStatus
                if (statusCurrent.equals(statusValues[0])) {
                    statusValues = arrayOf(context.getString(R.string.negotiation), context.getString(R.string.closure))
                } else if (statusCurrent.equals(statusValues[1])) {
                    statusValues = arrayOf(context.getString(R.string.closure))
                }
                if (!statusCurrent.equals("Closure")) {
                    var dialog = Dialog(context)
                    dialog.setContentView(R.layout.dialog_drop_down)
                    var dialogTitle = dialog.status
                    dialogTitle.text = "Task Status"
                    var dialogList = dialog.dialog_list

                    dialogList.adapter = ArrayAdapter<String>(context, R.layout.spinnertext, R.id.text1, statusValues) as ListAdapter?

                    dialogList.onItemClickListener = AdapterView.OnItemClickListener { parent, _view, poi, id ->
                        var statusNew = statusValues[poi]

                        dialog.dismiss()
                        notifyDataSetChanged()
                        var updateTaskDetails = HashMap<String, String>()
                        updateTaskDetails.put("user_name", LocalSession.getUserInfo(context, LocalSession.KEY_NAME))
                        updateTaskDetails.put("mobile_no", LocalSession.getUserInfo(context, LocalSession.KEY_MOBILE))
                        updateTaskDetails.put("id", "" + items[position].id)
                        updateTaskDetails.put("task_status", "" + statusNew)


                        var model = ViewEvent.retrofitService.onTaskStatusChange(updateTaskDetails)
                        dialog = MessageUtils.showDialog(context)
                        dialog.setOnKeyListener { dialog, _keyCode, event ->
                            dialog.dismiss()
                            false
                        }

                        model.enqueue(object : Callback<TaskUpdateModel> {
                            override fun onResponse(call: Call<TaskUpdateModel>, response: SpareInvoicePojos<TaskUpdateModel>) {
                                dialog.dismiss()
                                if (response.isSuccessful) {
                                    var taskModel = response.body()
                                    if (taskModel != null) {
                                        Log.d("aslkalsk", "" + taskModel.isStatus)
                                        if (taskModel.isStatus) {
                                            holder.itemStatus.text = statusNew

                                            var enq = CRMFilterService("" + items[position].id, items[position].customerName, items[position].city, "" + items[position].mobileNo, items[position].email, items[position].leadsource, items[position].assignedDate,
                                                    items[position].assignedTo, items[position].enquiryDate, items[position].enquiryFor, items[position].enquiryTime, items[position].assignedPerson, items[position].message, statusNew, items[position].task,
                                                    items[position].vehicleModel, items[position].chassisNo, true)
                                            items.set(position, enq)
                                            //Re_adding values as same

                                            //position = position - 1
                                            /*enq.assigned_date = items[position].assigned_date
                                            enq.assigned_person = items[position].assigned_person
                                            enq.assigned_to = items[position].assigned_to
                                            enq.city = items[position].city
                                            enq.created_at = items[position].created_at
                                            enq.created_time = items[position].created_time
                                            enq.created_by = items[position].created_by
                                            enq.customer_name = items[position].customer_name
                                            enq.email = items[position].email
                                            enq.enquiry_date = items[position].enquiry_date
                                            enq.enquiry_for = items[position].enquiry_for
                                            enq.enquiry_time = items[position].enquiry_time
                                            enq.leadsource = items[position].leadsource
                                            enq.sendThrough = items[position].sendThrough
                                            enq.mobile_no = items[position].mobile_no
                                            enq.id = items[position].id
                                            enq.task = items[position].task

                                            //Update Values
                                            enq.status = statusNew
                                            enq.isChange = true;
                                            items.set(position, enq)*/

                                            notifyDataSetChanged()

                                            MessageUtils.showToastMessage(context, taskModel.message)
                                        } else {
                                            // MessageUtils.showSnackBar(context, holder.snack_view, taskModel.message)
                                            MessageUtils.showToastMessage(context, taskModel.message)
                                        }
                                    } else {
                                        // MessageUtils.showSnackBar(context, holder.snack_view, "Not found ")
                                        MessageUtils.showToastMessage(context, "Not Found")
                                    }
                                } else {
                                    MessageUtils.showToastMessage(context, MessageUtils.showErrorCodeToast(response.code()))
                                    //MessageUtils.setErrorMessage(context, holder.snack_view, response.code())
                                }
                            }

                            override fun onFailure(call: Call<TaskUpdateModel>, t: Throwable) {
                                dialog.dismiss()
                                MessageUtils.showToastMessage(context, MessageUtils.showFailureToast(t.message))
                                //MessageUtils.setFailureMessage(context, holder.snack_view, t.message)
                            }
                        })
                    }
                    dialog.show()
                } else {
                    MessageUtils.showToastMessage(context, "Already Status Closed")
                }
            })*/

        }

        @RequiresApi(Build.VERSION_CODES.M)
        public fun requestLocationPermission() {
            if (ActivityCompat.shouldShowRequestPermissionRationale(context!!, Manifest.permission.READ_CALL_LOG)) {
                //    Toast.makeText(context, "Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show()
                Log.d("l,ick", "44");
            }
            Log.d("l,ick", "55");
            context.requestPermissions(arrayOf(Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE), Enquiry.LOCATION_PERMISSION_CODE)
        }

        public fun isReadLocationAllowed(): Boolean {
            Log.d("l,ick", "666");
            val result = ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_CALL_LOG)
            val result1 = ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_CONTACTS)
            val CALL_PHONE = ContextCompat.checkSelfPermission(context!!, Manifest.permission.CALL_PHONE)
            return if (result1 == PackageManager.PERMISSION_GRANTED && result == PackageManager.PERMISSION_GRANTED && CALL_PHONE == PackageManager.PERMISSION_GRANTED) {
                Log.d("l,ick", "777");
                true
            } else false
        }
    }

    class HolderSales(view: View) : RecyclerView.ViewHolder(view) {
        val itemName = view.item_crm_name
        val itemDate = view.item_crm_date
        val itemTask = view.item_crm_task
        val itemAction = view.item_crm_action
        val itemStatus = view.item_crm_status
        val itemLayout = view.item_crm
        val itemEmail = view.item_crm_action_email
        val itemCall = view.item_crm_action_call
        val snack_view = view.snack_view
        val itemModel = view.crm_item_model
        val crm_item_lay_model = view.crm_item_lay_model
    }
}