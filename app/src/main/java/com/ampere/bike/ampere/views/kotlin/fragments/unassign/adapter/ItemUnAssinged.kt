package com.ampere.bike.ampere.views.kotlin.fragments.unassign.adapter

import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.fragments.unassign.AssignedTaskfromCRM
import com.ampere.bike.ampere.views.kotlin.model.unassinged.UnAssignEnquiryModel
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.item_un_assign.view.*

class ItemUnAssinged(var activity: FragmentActivity, val enquiry: List<UnAssignEnquiryModel>) : RecyclerView.Adapter<ItemUnAssinged.UnAssignedViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UnAssignedViewHolder {
        return UnAssignedViewHolder(View.inflate(activity, R.layout.item_un_assign, null))
    }

    override fun getItemCount(): Int {
        return enquiry.size
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: UnAssignedViewHolder, position: Int) {
        try {
            holder.itemDate.text = if (enquiry[position].enquiry_date != null) DateUtils.convertDates(enquiry[position].enquiry_date, DateUtils.YYYY_MM_DD, DateUtils.DD_MM) else ""
        } catch (nullPoint: NullPointerException) {
            nullPoint.printStackTrace()
        }
        holder.itemCustomerName.text = "" + enquiry[position].customer_name
        holder.itemVehicleModel.text = "" + enquiry[position].vehicle_model
        holder.itemValue.text = "" + enquiry[position].invoice_value

        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        lp.setMargins(8, 8, 8, 8)

        holder.childView.setLayoutParams(lp)
        holder.childView.elevation = 8F
        holder.childView.setOnClickListener {


            var bundle = Bundle()
            var frgament = AssignedTaskfromCRM()

            bundle.putSerializable("itemValues", enquiry[position])

            frgament.arguments = bundle

            MyUtils.passFragmentBackStack(activity, frgament)

            //activity.supportFragmentManager.beginTransaction().replace(R.id.container_child, frgament).addToBackStack("").commit()

        }
    }

    class UnAssignedViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemDate = view.un_ass_date
        val itemCustomerName = view.un_ass_customer_name
        val itemVehicleModel = view.un_ass_vehicle_model
        val itemValue = view.un_ass_value
        val childView = view.un_ass_child_view
    }
}