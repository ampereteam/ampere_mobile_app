package com.ampere.bike.ampere.views.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.calendar.CustomeCalendarView;
import com.ampere.bike.ampere.views.fragment.dashboard.Dashboard;
import com.ampere.bike.ampere.views.fragment.enquiry.Enquiry;
import com.ampere.bike.ampere.views.fragment.indent.Indent;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentHome;
import com.ampere.bike.ampere.views.fragment.parthi.Inventory.InventoryHome;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyHome;
import com.ampere.bike.ampere.views.items.SliderAdapter;
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMActivity;
import com.ampere.bike.ampere.views.kotlin.fragments.crm.sparesInvoice.SpareInvoice;
import com.ampere.bike.ampere.views.kotlin.fragments.enquiry.MyEnquiry;
import com.ampere.bike.ampere.views.kotlin.fragments.invoice.InvoiceActivity;
import com.ampere.vehicles.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import customviews.CustomTextView;

import static com.ampere.bike.ampere.shared.LocalSession.KEY_USER_NAME;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_USER_TYPE;
import static com.ampere.bike.ampere.shared.LocalSession.T_SPECIAL_OFFICER;


public class Home extends Fragment {

    private Unbinder unbinder;

//    private static final int LOCATION_PERMISSION_CODE = 23;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.indicator)
    TabLayout indicator;

    @BindViews({R.id.lay_enquiry, R.id.lay_crm, R.id.lay_indent, R.id.lay_calendar, R.id.lay_dashboard,R.id.lay_inven,R.id.lay_warran})
    List<LinearLayout> mHomeLays;

    @BindViews({R.id.txt_enquiry_count, R.id.txt_proposal_count, R.id.txt_negotiation_count, R.id.txt_closure_count,R.id.tv_inven,R.id.tv_warran})
    List<CustomTextView> mEnquiryCountStatus;

    @BindView(R.id.txt_crm)
    CustomTextView mTxtCrm;
    @BindView(R.id.enquiry_txt)
    CustomTextView mTxtEnquiry;

    @BindView(R.id.mImgCheck)
    ImageView mImgCheck;

    List<Integer> color;
    List<String> colorName;

    Timer timer = new Timer();
    SliderTimer sliderTimer;

/*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("FragmentLife ", "onAttach");
    }*/

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) Objects.requireNonNull(getActivity())).enableNavigationView("AMPERE DMS", 0);
        return inflater.inflate(R.layout.frag_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Log.d("FragmentLife ", "onViewCreated");
        unbinder = ButterKnife.bind(this, view);
        //((Animatable) mImgCheck.getDrawable()).start();

       /* if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CRM)) {
            mHomeLays.get(2).setVisibility(View.GONE);
        }*/

        if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_SPECIAL_OFFICER)) {
            mTxtCrm.setText(getString(R.string.my_enquiry));
            mTxtEnquiry.setText(getString(R.string.enquiry_new));
            mHomeLays.get(2).setVisibility(View.GONE);
        }

/*
        try {
            Log.d("dateConvert", "" + DateUtils.convertDates("02:59 PM", "hh:mm a", "HH:mm:ss") + " AAAMMMM  " + DateUtils.convertDates("02:59 AM", "hh:mm a", "HH:mm:ss"));
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        //addCalendar();

        color = new ArrayList<>();
        color.add(Color.RED);
        color.add(Color.GREEN);
        color.add(Color.BLUE);

        colorName = new ArrayList<>();
        colorName.add("RED");
        colorName.add("GREEN");
        colorName.add("BLUE");

        mEnquiryCountStatus.get(0).setTypeface(MyUtils.getTypeface(Objects.requireNonNull(getActivity()), "permanent_marker.ttf"));
        mEnquiryCountStatus.get(1).setTypeface(MyUtils.getTypeface(getActivity(), "permanent_marker.ttf"));
        mEnquiryCountStatus.get(2).setTypeface(MyUtils.getTypeface(getActivity(), "permanent_marker.ttf"));
        mEnquiryCountStatus.get(3).setTypeface(MyUtils.getTypeface(getActivity(), "permanent_marker.ttf"));


/*
        if (isReadLocationAllowed()) {
            Log.d("persmission", "Yess");
            //addToCalendar(getActivity(), "Subbu Test", System.currentTimeMillis(), System.currentTimeMillis());
            //   addCal(getActivity());

            addEvent();
        } else {
            Log.d("persmission", "Noo");
            requestLocationPermission();
        }
*/

        // adCal();

        /*  view.findViewById(R.id.lay_enquiry).*/


        mHomeLays.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyUtils.passFragmentBackStack(getActivity(), new Enquiry());
            }
        });

        mHomeLays.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyUtils.passFragmentBackStack(getActivity(), new CRM());

                if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_SPECIAL_OFFICER)) {
                    MyUtils.passFragmentBackStack(getActivity(), new MyEnquiry());
                } else {
                    startActivity(new Intent(getActivity(), CRMActivity.class));
                    getActivity().overridePendingTransition(0, 0);
                }

            }
        });


        mHomeLays.get(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyUtils.passFragmentBackStack(getActivity(), new Indent());
                //startActivity(new Intent(getActivity(), Indent.class));
            }
        });

        mHomeLays.get(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyUtils.passFragmentBackStack(getActivity(), new CustomeCalendarView());
            }
        });

        mHomeLays.get(4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyUtils.passFragmentBackStack(getActivity(), new Dashboard());
            /*   Intent intent = new Intent(getActivity(), InvoiceActivity.class);
                intent.putExtra("id", "" + "142");
                intent.putExtra("user_name", "" + LocalSession.getUserInfo(getActivity(), KEY_USER_NAME));
                intent.putExtra("task_name", "" + "Spare");
                getActivity().overridePendingTransition(0, 0);
                getActivity().startActivity(intent);*/
            }
        });
        mHomeLays.get(5).setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                                  MyUtils.passFragmentBackStack(getActivity(), new InventoryHome());
            }
        });

        mHomeLays.get(6).setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                                  MyUtils.passFragmentBackStack(getActivity(), new WarrantyHome());
              }
          });

    }


  /*  private void startSlider() {
        // sliderTimer.run();

        sliderTimer = new SliderTimer();
        timer.scheduleAtFixedRate(sliderTimer, 2000, 4000);
    }*/

    @Override
    public void onResume() {
        super.onResume();
        //Log.d("FragmentLife ", "onResume");
        //startSlider();

        viewPager.setAdapter(new SliderAdapter(getActivity(), color, colorName, timer, sliderTimer));
        indicator.setupWithViewPager(viewPager, true);
    }

/*
    @OnClick(R.id.txt_enquiry)
    public void onEnquiry() {
        //MyUtils.passFragmentBackStack(getActivity(), new EnquiryModel());
    }

    @OnClick(R.id.lay_proposal)
    public void onProposal() {
        // MyUtils.passFragmentBackStack(getActivity(), new EnquiryModel());
    }

    @OnClick(R.id.lay_negotiation)
    public void onNegotiation() {
        // MyUtils.passFragmentBackStack(getActivity(), new EnquiryModel());
    }

    @OnClick(R.id.lay_closure)
    public void onClosure() {
        //MyUtils.passFragmentBackStack(getActivity(), new EnquiryModel());
    }
*/

    public class SliderTimer extends TimerTask {

        @Override
        public void run() {
            Objects.requireNonNull(getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() < color.size() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        /*timer.cancel();
        sliderTimer.cancel();*/

        //Log.d("FragmentLife ", "onPause");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
