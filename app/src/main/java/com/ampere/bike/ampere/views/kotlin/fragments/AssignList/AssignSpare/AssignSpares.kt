package com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSpare

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome.Companion.btnAssignList
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome.Companion.spareserverEnquiryList
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSpare.Adapter.AssignSparesAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo.AssignEnqIDPojo
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo.EnquiryListRes
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.dialog

import com.ampere.vehicles.R
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("SetTextI18n,StaticFieldLeak")
class AssignSpares : Fragment(),ConfigClickEvent {
    companion object {
        var recyAssignSpares : RecyclerView?=null
        var dialog : Dialog?=null
        var utilsCallApi: UtilsCallApi?=null
        var cbxAssingSpares: CheckBox?= null
        var  emptyList : TextView?=null
        var  assignSparesAdapter : AssignSparesAdapter?=null
        var btnAssignspares: TextView?= null
        var spareEnqryRES: EnquiryListRes?= null
        var sparecount=0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("Aftersuccess",""+savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        Log.d("Aftersuccess",""+savedInstanceState)
        (activity as CustomNavigationDuo).disbleNavigationView(getString(R.string.assigned_enquiry))
        return inflater.inflate(R.layout.fragment_unassign_spares, container, false)

    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("Aftersuccess",""+savedInstanceState)
        dialog = Dialog(activity)
        utilsCallApi = UtilsCallApi(activity)
        recyAssignSpares=view.findViewById(R.id.recyl_assign_sales)
        cbxAssingSpares =view.findViewById(R.id.cbx_assgin_sales)
        btnAssignspares =view.findViewById(R.id.btn_assignsales)
        emptyList =view.findViewById(R.id.empty_list)
        cbxAssingSpares?.isChecked =false
        if(utilsCallApi?.isNetworkConnected!!){
            getSparesEnquiryList()
        }else{
            MessageUtils.showSnackBarAction(activity, CustomNavigationDuo.snackVIew,getString(R.string.check_internet))
        }

        cbxAssingSpares?.setOnCheckedChangeListener { buttonView, isChecked ->

            if(spareEnqryRES?.data?.enquiries?.size!!<=0){
                Log.d("LISTSIze",""+spareEnqryRES?.data?.enquiries?.size)
                sparecount=0
            }else{
                if(isChecked){
                    for(i:Int in 0.. spareEnqryRES?.data?.enquiries?.size!!-1){
                        spareEnqryRES?.data?.enquiries?.get(i)?.check =true
                        spareserverEnquiryList[i]?.check =true
                    }
                    btnAssignspares?.setText(getString(R.string.assign)+"("+spareEnqryRES?.data?.enquiries?.size+")")
                    btnAssignList?.setText(getString(R.string.assign)+"("+spareEnqryRES?.data?.enquiries?.size+")")
                    btnAssignspares?.visibility =View.VISIBLE
                    btnAssignList?.visibility =View.VISIBLE
                    sparecount = spareEnqryRES?.data?.enquiries?.size!!
                }else{
                    for(i:Int in 0.. spareEnqryRES?.data?.enquiries?.size!!-1){
                        spareEnqryRES?.data?.enquiries?.get(i)?.check =false
                        spareserverEnquiryList[i]?.check =false
                    }
                    btnAssignspares?.visibility =View.GONE
                    btnAssignList?.visibility =View.GONE
                    sparecount =0
                }
                assignSparesAdapter?.notifyDataSetChanged()
            }


        }
        btnAssignspares?.setOnClickListener {
            try{
                if(AssignListHome.spAssignPerson?.selectedItem !=null){
                    assignSparesEnquiries()
                }else{
                    MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,"Select Assign To and Person")
                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }

    }

    private fun assignSparesEnquiries() {
        try{
            Log.d("AssignSpareEnquiries","Try")
            val map:HashMap<String,Any>  = HashMap()
            map["userid"] = LocalSession.getUserInfo(activity, LocalSession.KEY_ID)
            map["assign_to"] = AssignListHome.spAssignTo?.selectedItem.toString()
            var assignpersonID :String?=null
            for(user in AssignListHome.userListRes?.data?.users!!){
                if( user?.name.equals(AssignListHome.spAssignPerson?.selectedItem.toString())){
                    assignpersonID = user?.id.toString()
                }
            }
            if(assignpersonID!=null){
                map["assign_person"] =assignpersonID.toInt()
            }
            val hashMap:HashMap<String,Any>  =HashMap()
            val enquiryList :ArrayList<AssignEnqIDPojo> = ArrayList()
            for (i:Int in 0 .. spareserverEnquiryList.size-1){
                if(spareserverEnquiryList[i]?.check!!) {
                    val assignEnqIDPojo = AssignEnqIDPojo(spareserverEnquiryList[i]?.id.toString())
                    enquiryList.add(assignEnqIDPojo)
                }
                Log.d("serverList",""+spareserverEnquiryList[i]?.city)
            }
            hashMap["enquiry"] =enquiryList
            map["enquiries"] =hashMap
            Log.d("AssignEnquirySales",""+map.toString())
            if(utilsCallApi?.isNetworkConnected!!){
                dialog = MessageUtils.showDialog(activity)
                val calinterface = utilsCallApi?.connectRetro("POST","assignenquiry")
                val callback = calinterface?.call_post("assignenquiry","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),map)
                callback?.enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        try{
                            MessageUtils.dismissDialog(dialog)
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.localizedMessage)
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        try{
                            if(response.isSuccessful){
                                try{
                                    val jsonObject = JSONObject(response.body()?.string())
                                    val message = jsonObject.getString("message")
                                    val success =jsonObject.getBoolean("success")
                                    if(success){
                                        btnAssignspares?.visibility =View.GONE
                                        btnAssignList?.visibility =View.GONE
                                        Toast.makeText(activity,message, Toast.LENGTH_LONG).show()
                                        getSparesEnquiryList()
                                    }else{
                                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,message)
                                    }
                                }catch (ex:Exception){
                                    ex.printStackTrace()

                                }
                            }else{
                                MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response.message())
                            }
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity, CustomNavigationDuo.snackVIew,getString(R.string.check_internet))
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

      public fun getSparesEnquiryList() {
        try{
            val hashMap :HashMap<String,Any> = HashMap()
            hashMap["userid"] = LocalSession.getUserInfo(activity, LocalSession.KEY_ID)
            hashMap["enquiry_type"] = getString(R.string.spares)
            dialog = MessageUtils.showDialog(activity)
            val callInterface  = utilsCallApi?.connectRetro("POST","EnquiryListRes")
            val callBack = callInterface?.call_post("unassignedenquiry","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),hashMap)
            callBack?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    try{
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.localizedMessage)

                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try{
                        if(response.isSuccessful){
                            try{
                                spareEnqryRES = Gson().fromJson(response.body()?.string(),EnquiryListRes::class.java)
                                if(spareEnqryRES?.success!!){
                                    if(spareEnqryRES?.data?.enquiries?.size!! <=0){
                                        emptyList?.setText(spareEnqryRES?.message)
                                        emptyList?.visibility = View.VISIBLE
                                        recyAssignSpares?.visibility =View.GONE
                                    }else{
                                        if(btnAssignspares?.visibility == View.VISIBLE  ){
                                            btnAssignspares?.visibility = View.GONE
                                        }
                                        if(cbxAssingSpares?.isChecked!!){
                                            cbxAssingSpares?.isChecked =false
                                        }
                                        emptyList?.visibility = View.GONE
                                        recyAssignSpares?.visibility =View.VISIBLE
                                        spareserverEnquiryList = spareEnqryRES?.data?.enquiries!!
                                        setTOAdapter()
                                        sparecount=0
                                    }
                                }else{
                                    MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,spareEnqryRES?.message)
                                }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }
                        }else{
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response.message())
                        }
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }
            })
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setTOAdapter() {
        try{
            recyAssignSpares?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL,false)
            assignSparesAdapter = AssignSparesAdapter(activity!!,spareserverEnquiryList,this)
            recyAssignSpares?.adapter = assignSparesAdapter
        }catch (eX:Exception){
            eX.printStackTrace()
        }
    }

    override fun connectposition(position: Int, listSize: Int) {
        try{
            Log.d("CallPostion",""+position+"\tlistsize"+listSize)
            if(listSize<=0){
                btnAssignspares?.visibility =View.GONE
                btnAssignList?.visibility =View.GONE
                sparecount=0
            }else{
                btnAssignspares?.setText(getString(R.string.assign)+"("+listSize+")")
                btnAssignList?.setText(getString(R.string.assign)+"("+listSize+")")
                btnAssignspares?.visibility =View.VISIBLE
                btnAssignList?.visibility =View.VISIBLE
                sparecount =listSize
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("Aftersuccess","start")
    }

    override fun onResume() {
        super.onResume()
        Log.d("Aftersuccess","RESUME")
    }
}
