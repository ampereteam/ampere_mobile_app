package com.ampere.bike.ampere.model.invoiceGenerate.initial

data class Statelists(
        val id: Int,
        val state_name: String,
        val tin_number: Int,
        val state_code: String,
        val created_at: Any,
        val updated_at: Any

)