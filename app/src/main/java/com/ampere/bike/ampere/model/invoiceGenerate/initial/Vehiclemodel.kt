package com.ampere.bike.ampere.model.invoiceGenerate.initial

data class Vehiclemodel(
        val id: Int,
        val model_code: String,
        val code_number: Int,
        val product_type_id: Int,
        val vehicle_model: String,
        val has_varient: String,
        val colors: String,
        val subsidy_amount: String,
        val created_at: String,
        val updated_at: String
)