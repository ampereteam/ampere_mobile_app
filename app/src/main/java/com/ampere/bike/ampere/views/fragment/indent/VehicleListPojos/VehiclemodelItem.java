package com.ampere.bike.ampere.views.fragment.indent.VehicleListPojos;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class VehiclemodelItem{

	@SerializedName("model_code")
	private String modelCode;

	@SerializedName("has_varient")
	private String hasVarient;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("vehicle_model")
	private String vehicleModel;

	@SerializedName("subsidy_amount")
	private String subsidyAmount;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("product_type_id")
	private int productTypeId;

	@SerializedName("colors")
	private String colors;

	@SerializedName("code_number")
	private int codeNumber;

	public void setModelCode(String modelCode){
		this.modelCode = modelCode;
	}

	public String getModelCode(){
		return modelCode;
	}

	public void setHasVarient(String hasVarient){
		this.hasVarient = hasVarient;
	}

	public String getHasVarient(){
		return hasVarient;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setVehicleModel(String vehicleModel){
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleModel(){
		return vehicleModel;
	}

	public void setSubsidyAmount(String subsidyAmount){
		this.subsidyAmount = subsidyAmount;
	}

	public String getSubsidyAmount(){
		return subsidyAmount;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProductTypeId(int productTypeId){
		this.productTypeId = productTypeId;
	}

	public int getProductTypeId(){
		return productTypeId;
	}

	public void setColors(String colors){
		this.colors = colors;
	}

	public String getColors(){
		return colors;
	}

	public void setCodeNumber(int codeNumber){
		this.codeNumber = codeNumber;
	}

	public int getCodeNumber(){
		return codeNumber;
	}

	@Override
 	public String toString(){
		return 
			"VehiclemodelItem{" + 
			"model_code = '" + modelCode + '\'' + 
			",has_varient = '" + hasVarient + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",vehicle_model = '" + vehicleModel + '\'' +
			",subsidy_amount = '" + subsidyAmount + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",product_type_id = '" + productTypeId + '\'' + 
			",colors = '" + colors + '\'' + 
			",code_number = '" + codeNumber + '\'' + 
			"}";
		}
}