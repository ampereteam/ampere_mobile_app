package com.ampere.bike.ampere.views.kotlin.fragments.inventory

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.BEARER
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.model.inventory.AcceptedStockModel
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare.Sparedelivered
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare.Sparedespatched
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.frag_inventory.*
import kotlinx.android.synthetic.main.frag_view_inventory_info.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ViewSpareDetailed : Fragment() {
    var itemValues: Sparedespatched? = null
    var itemValueSpareDelivery: Sparedelivered? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        (activity as CustomNavigationDuo).disbleNavigationView("Spare Info")
        return inflater.inflate(R.layout.frag_view_inventory_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        inventory_info_no_data.text = "Spare Info Not Found"
        var ids = arguments!!.getString("id");
        var id_values = arguments!!.getString("id_values")

        Log.d("idssdsds_spares", "" + ids + " id_values " + id_values)
        acceptStockDetails(ids)
        if (id_values.equals("Spare_delivery")) {
            linearLayout4.visibility = View.GONE
            Log.d("id_for_spare", "Spare_delivery")

            itemValueSpareDelivery = arguments!!.getSerializable("item_values") as Sparedelivered?

        } else {
            itemValues = arguments!!.getSerializable("item_values") as Sparedespatched?
            linearLayout4.visibility = View.VISIBLE
            Log.d("id_for_spare", "Spare_despatched")
        }

        view_invent_cancel.setOnClickListener {
            activity!!.onBackPressed()
        }
        view_invent_accept.setOnClickListener {
            Log.d("api_called", "Is_loading")
        }
    }


    private fun acceptStockDetails(ids: String) {

        var map = HashMap<String, String>()

        map.put("inventory_id", "" + ids)
        map.put("stock_type", MyUtils.VIEW_ITEM)
        map.put("username", LocalSession.getUserInfo(activity!!, LocalSession.KEY_ID))
        var acceptModels: Call<AcceptedStockModel> = MyUtils.getRetroService().onAcceptStocks(BEARER+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
        var dialog: Dialog = MessageUtils.showDialog(activity)

        dialog.setOnKeyListener { dialogInterface, i, keyEvent ->
            MessageUtils.dismissDialog(dialog);
            acceptModels.cancel()
            false
        }

        acceptModels.enqueue(object : Callback<AcceptedStockModel> {

            override fun onResponse(call: Call<AcceptedStockModel>?, response: Response<AcceptedStockModel>?) {
                MessageUtils.dismissDialog(dialog);

                if (response != null) {
                    if (response.isSuccessful) {

                    } else {
                        var msg = MessageUtils.showErrorCodeToast(response.code())
                        MessageUtils.showSnackBar(activity, accpt_stock_not_found, msg)

                    }
                } else {
                    MessageUtils.showSnackBar(activity!!.applicationContext, accpt_stock_not_found, getString(R.string.api_response_not_found))
                }
            }


            override fun onFailure(call: Call<AcceptedStockModel>?, t: Throwable?) {
                MessageUtils.dismissDialog(dialog);
                var msg = MessageUtils.showFailureToast(t!!.message)
                MessageUtils.showSnackBar(activity!!.applicationContext, accpt_stock_not_found, msg)
            }

        })

    }
}