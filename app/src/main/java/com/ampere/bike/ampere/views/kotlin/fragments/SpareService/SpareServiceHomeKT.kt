package com.ampere.bike.ampere.views.kotlin.fragments.SpareService


import android.annotation.SuppressLint
import android.os.Bundle
import android.os.StrictMode
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.Adapter.FreeReplacementTapAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FreeReplacementHome
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.FreeRepalcementsComplaints
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrInvoice.FreeReplacementInvoice
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Complaints.SpareServiceComplaint
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.DefectReturn.SpareServiceDefectReturn
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Invoice.SpareServiceInvoice
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.ServiceReturn.SpareServiceReturn
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.TapAdapter.SpareServiceTapAdpter


import com.ampere.vehicles.R
import java.util.ArrayList

class SpareServiceHomeKT : Fragment() {
    companion object {
        @SuppressLint("StaticFieldLeak")
        var msnakview: View ?=null
    }
    var adapter: SpareServiceTapAdpter? = null
    private var sPSctabLayout: TabLayout? = null
    private var sPScviewPager: ViewPager? = null
    internal var sPScfragmnetList: ArrayList<String> = ArrayList()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view :View = inflater.inflate(R.layout.fragment_spare_service_home_kt, container, false)
        (activity as CustomNavigationDuo).disbleNavigationView(getString(R.string.spare_service))
        var newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        FreeReplacementHome.msnakview = view.findViewById(R.id.parent_view)
        sPScviewPager = view.findViewById<ViewPager>(R.id.spsc_viewPager)
        sPSctabLayout = view.findViewById<TabLayout>(R.id.spsc_tabLayout)
        sPScfragmnetList = ArrayList<String>()
        sPScfragmnetList.add(getString(R.string.complaints))
        sPScfragmnetList.add(getString(R.string.invoice))
        sPScfragmnetList.add(getString(R.string.service_return))
        sPScfragmnetList.add(getString(R.string.defect_return))
        setupViewPager(sPScviewPager!!)

    }
    private fun setupViewPager(viewPager: ViewPager) {
        adapter = SpareServiceTapAdpter(childFragmentManager)
        adapter!!.addFragment(SpareServiceComplaint(), sPScfragmnetList.get(0))
        adapter!!.addFragment(SpareServiceInvoice(), sPScfragmnetList.get(1))
        adapter!!.addFragment(SpareServiceReturn(), sPScfragmnetList.get(2))
        adapter!!.addFragment(SpareServiceDefectReturn(), sPScfragmnetList.get(3))
        initTabs()
    }

    @SuppressLint("ResourceType")
    private fun initTabs() {
        sPScviewPager?.setAdapter(adapter)
        sPSctabLayout?.setupWithViewPager(sPScviewPager)

        for (i in 0 until adapter?.getCount()!!) {
            val textView = TextView(activity)
            textView.typeface = MyUtils.getTypeface(activity!!, "fonts/Lato-Bold.ttf")
            textView.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            textView.gravity = Gravity.CENTER
            textView.setText(sPScfragmnetList.get(i))
            val tab = sPSctabLayout?.getTabAt(i)
            if (tab != null) {
                tab.setCustomView(textView)
                tab.setText(adapter!!.getPageTitle(i))
            }
        }
        sPSctabLayout?.setTabMode(TabLayout.MODE_FIXED)
    }

}
