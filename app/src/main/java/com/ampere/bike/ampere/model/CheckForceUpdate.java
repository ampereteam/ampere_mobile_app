
package com.ampere.bike.ampere.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CheckForceUpdate {

    @SerializedName("data")
    public Data mData;
    @SerializedName("message")
    public String mMessage;
    @SerializedName("success")
    public Boolean mSuccess;

    public class Data {

        @SerializedName("created_at")
        public Object mCreatedAt;
        @SerializedName("force_update")
        public boolean mForceUpdate;
        @SerializedName("id")
        public Long mId;
        @SerializedName("message")
        public String mMessage;
        @SerializedName("updated_at")
        public Object mUpdatedAt;
        @SerializedName("version_code")
        public String mVersionCode;


    }

}
