package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoicePojos

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Gst(

	@field:SerializedName("sgst")
	val sgst: String? = null,

	@field:SerializedName("spares_igst")
	val sparesIgst: String? = null,

	@field:SerializedName("subsidy_percentage")
	val subsidyPercentage: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("spares_cgst")
	val sparesCgst: String? = null,

	@field:SerializedName("terms_conditions")
	val termsConditions: String? = null,

	@field:SerializedName("created_at")
	val createdAt: Any? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("cgst")
	val cgst: String? = null,

	@field:SerializedName("spares_sgst")
	val sparesSgst: String? = null,

	@field:SerializedName("igst")
	val igst: String? = null
)