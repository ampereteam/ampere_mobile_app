package com.ampere.bike.ampere.views.kotlin.fragments.invoice.invoicecolorpojos

data class ColorsItem(
	val color: String? = null
)
