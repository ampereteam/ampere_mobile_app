package com.ampere.bike.ampere.views.kotlin.fragments.AssignList


import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.os.StrictMode
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.CustomNavigationDuo.enquiryType
import com.ampere.bike.ampere.CustomNavigationDuo.snackVIew
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Adapter.AssignTapAdapte
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSpare.AssignSpares
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSales.AssignSales
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSales.AssignSales.Companion.btnAssignsales
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSales.AssignSales.Companion.salescount
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignService.AssignService
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignService.AssignService.Companion.btnAssignservice
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignService.AssignService.Companion.servicecount
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSpare.AssignSpares.Companion.btnAssignspares
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSpare.AssignSpares.Companion.sparecount
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSpareService.AssignSpareService
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSpareService.AssignSpareService.Companion.btnAssignsparesService
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSpareService.AssignSpareService.Companion.spareservicecount
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo.AssignEnqIDPojo
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo.EnquiriesItem
import com.ampere.bike.ampere.views.kotlin.fragments.UserManagement.Pojo.UserList.UserListRes
import com.ampere.vehicles.R
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("StaticFieldLeak")
class AssignListHome : Fragment() {
    companion object {
        var userListRes : UserListRes?=null
        var spAssignPerson:Spinner ?=null
        var btnAssignList:TextView ?=null
        var spAssignTo:Spinner ?=null
        var assigntoSTRAdapter :ArrayAdapter<String>?= null
        var assignPersonSTRAdapter :ArrayAdapter<String>?= null
        var spareserverEnquiryList =ArrayList<EnquiriesItem?>()
        var serviceserverEnquiryList =ArrayList<EnquiriesItem?>()
        var salesserverEnquiryList =ArrayList<EnquiriesItem?>()
        var spareserviceserverEnquiryList =ArrayList<EnquiriesItem?>()
    }
    var dialog:Dialog ?=null
    var utilsCallApi :UtilsCallApi ?= null
    var assignViewpager:ViewPager ?=null
    var assignTaplayout:TabLayout ?=null
    var edtAssignTo:EditText ?=null
    var edtAssignPerson:EditText ?=null

    var assigntapAdpter: AssignTapAdapte?=null
    var asssignLIst:ArrayList<String> = ArrayList()
    var assigntoSTRList :ArrayList<String> = ArrayList()
    var count=0
    var serverList =ArrayList<EnquiriesItem?>()




    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment.
        (activity as CustomNavigationDuo).disbleNavigationView(getString(R.string.assigned_enquiry))
        val newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        return inflater.inflate(R.layout.fragment_unassigned_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        utilsCallApi = UtilsCallApi(activity)
        assignViewpager = view.findViewById(R.id.uns_viewPager)
        assignTaplayout = view.findViewById(R.id.unas_tabLayout)
        spAssignPerson = view.findViewById(R.id.sp_assign_person)
        spAssignTo = view.findViewById(R.id.sp_assign_to)
        edtAssignTo = view.findViewById(R.id.edt_assign_to)
        btnAssignList = view.findViewById(R.id.btn_assignlist)
        edtAssignPerson = view.findViewById(R.id.edt_assign_person)
        asssignLIst.add("Sales".toUpperCase())
        asssignLIst.add("Service".toUpperCase())
        asssignLIst.add("Spares".toUpperCase())
        asssignLIst.add("SpareSErvice".toUpperCase())
        if(utilsCallApi?.isNetworkConnected!!){
            getUserTypeandCode()
        }else{
            MessageUtils.showSnackBarAction(activity,snackVIew,getString(R.string.check_internet))
        }


        spAssignTo?.onItemSelectedListener = object  :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                try{
                    Log.d("NOthingSelected",""+parent?.selectedItem)
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                try{
                    val selectedITem = spAssignTo?.selectedItem.toString()
                    edtAssignTo?.setText(selectedITem)
                    Log.d("SelectedItem",""+selectedITem)
                    if(selectedITem.equals("Select User Type")){
                        Log.d("SelectedItem",""+selectedITem)
                    }else{
                        val assignPersonList :ArrayList<String> = ArrayList()
                        for(assignPerson in userListRes?.data?.users!!){
                            if(assignPerson?.usertype?.userType!!.equals(selectedITem)){
                                assignPersonList.add(assignPerson.name!!)
                            }
                        }
                        assignPersonSTRAdapter = ArrayAdapter(activity,R.layout.text_spinner,R.id.text1,assignPersonList)
                        spAssignPerson?.adapter = assignPersonSTRAdapter
                    }
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

        }
        spAssignPerson?.onItemSelectedListener = object  :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                try{
                    Log.d("nothingSelecteditem",""+parent?.selectedItem)
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                try{
                    val selectdItem = spAssignPerson?.selectedItem.toString()
                    edtAssignPerson?.setText(selectdItem)


                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

        }
        assignViewpager?.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(assignTaplayout))
        assignTaplayout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            @SuppressLint("SetTextI18n")
            override fun onTabSelected(tab: TabLayout.Tab?) {
                enquiryType=tab?.text.toString()
                Log.d("enquiryType",""+enquiryType)
                if(tab?.position==0){
                        count  =AssignSales.salescount
                }else if(tab?.position ==1) {
                       count = AssignService.servicecount
                }else if(tab?.position ==2){
                       count =AssignSpares.sparecount
                    Log.d("Count",""+AssignSpares.sparecount)
                 /*   Log.d("SpareArraySize",""+AssignSpares.spareserverEnquiryList.size)
                    for (spare in AssignSpares.spareserverEnquiryList){
                        if(spare?.check!!){
                            spareserverEnquiryList.add(spare)
                        }
                    }*/
                    Log.d("SpareArraySize",""+ spareserverEnquiryList.size)

                }else if(tab?.position ==3){
                    count=AssignSpareService.spareservicecount
                    Log.d("Count",""+AssignSpareService.spareservicecount)



                }
                if(count!=0){
                    btnAssignList?.text = "Assign("+count.toString()+")".toUpperCase()
                    btnAssignList?.visibility =View.VISIBLE
                }else{
                    btnAssignList?.visibility =View.GONE
                }
            }
        })
        setupViewPager()
        btnAssignList?.setOnClickListener {
            try {

                if(AssignListHome.spAssignPerson?.selectedItem !=null){
                    if(enquiryType.equals("SALES")){
                        serverList = salesserverEnquiryList
                    }else if(enquiryType.equals("SERVICE")){
                        serverList = serviceserverEnquiryList
                    }else if(enquiryType.equals("SPARES")){
                        serverList = spareserverEnquiryList
                    }else if(enquiryType.equals("SPARESERVICE")){
                        serverList = spareserviceserverEnquiryList
                    }
                assignToServer()
                }else{
                    MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,"Select Assign To and Person")
                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }
    }

    private fun assignToServer() {
        try {
            val map:HashMap<String,Any>  = HashMap()
            map["userid"] = LocalSession.getUserInfo(activity, LocalSession.KEY_ID)
            map["assign_to"] = AssignListHome.spAssignTo?.selectedItem.toString()
            var assignpersonID :String?=null
            for(user in AssignListHome.userListRes?.data?.users!!){
                if( user?.name.equals(AssignListHome.spAssignPerson?.selectedItem.toString())){
                    assignpersonID = user?.id.toString()
                }
            }
            if(assignpersonID!=null){
                map["assign_person"] =assignpersonID.toInt()
            }
            val hashMap:HashMap<String,Any>  =HashMap()
            val enquiryList :ArrayList<AssignEnqIDPojo> = ArrayList()
            for (i:Int in 0 .. serverList.size-1){
                if(serverList[i]?.check!!) {
                    val assignEnqIDPojo = AssignEnqIDPojo(serverList[i]?.id.toString())
                    enquiryList.add(assignEnqIDPojo)
                }
                Log.d("serverList",""+serverList[i]?.city)
            }
            hashMap["enquiry"] =enquiryList
            map["enquiries"] =hashMap
            Log.d("AssignEnquirySales",""+map.toString())
            if(utilsCallApi?.isNetworkConnected!!){
                dialog = MessageUtils.showDialog(activity)
                val calinterface = utilsCallApi?.connectRetro("POST","assignenquiry")
                val callback = calinterface?.call_post("assignenquiry","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),map)
                callback?.enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        try{
                            MessageUtils.dismissDialog(dialog)
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.localizedMessage)
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        try{
                            if(response.isSuccessful){
                                try{
                                    val jsonObject = JSONObject(response.body()?.string())
                                    val message = jsonObject.getString("message")
                                    val success =jsonObject.getBoolean("success")
                                    if(success){
                                        Log.d("CurrentTab",""+assignTaplayout?.selectedTabPosition+" view pager "+assignViewpager?.currentItem.toString())
                                        sparecount=0
                                        servicecount=0
                                        spareservicecount=0
                                        salescount=0
                                        count=0
                                        btnAssignList?.visibility=View.GONE
                                        Toast.makeText(activity,message, Toast.LENGTH_LONG).show()
                                        if(enquiryType.equals("SALES")){
                                            btnAssignsales?.visibility =View.GONE
                                            assignTaplayout?.getTabAt(0)?.select()
                                        }else if(enquiryType.equals("SERVICE")){
                                            btnAssignservice?.visibility =View.GONE
                                            assignTaplayout?.getTabAt(1)?.select()
                                        }else if(enquiryType.equals("SPARES")){
                                            btnAssignspares?.visibility =View.GONE
                                            assignTaplayout?.getTabAt(2)?.select()
                                        }else if(enquiryType.equals("SPARESERVICE")){
                                            btnAssignsparesService?.visibility =View.GONE
                                            assignTaplayout?.getTabAt(3)?.select()
                                        }
                                        MyUtils.passFragmentBackStack(activity,AssignListHome())

                                    }else{
                                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,message)
                                    }
                                }catch (ex:Exception){
                                    ex.printStackTrace()

                                }
                            }else{
                                MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response.message())
                            }
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity, CustomNavigationDuo.snackVIew,getString(R.string.check_internet))
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun getUserTypeandCode() {
        try{
            val hashMap :HashMap<String,Any> = HashMap()
            hashMap["userid"] = LocalSession.getUserInfo(activity, LocalSession.KEY_ID)
            dialog  = MessageUtils.showDialog(activity)
            val callInterface = utilsCallApi?.connectRetro("POST","usermanagement")
            val callback  = callInterface?.call_post("usermanagement","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),hashMap)
            callback?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    try{
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.localizedMessage)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }

                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    try{
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                userListRes = Gson().fromJson(response.body()?.string(),UserListRes::class.java)
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }
                            if(userListRes?.success!!){
                                if(userListRes?.data?.users?.size!! <=0){
                                    Log.d("ArrayListValueForUSer",""+ userListRes?.data?.users?.get(0)?.name)
                                }else{
                                    for(userType in userListRes?.data?.usertype!!){
                                        assigntoSTRList.add(userType?.userType!!)
                                    }
                                    assigntoSTRList.add(0,"Select User Type")
                                    assigntoSTRAdapter =ArrayAdapter(activity,R.layout.text_spinner,R.id.text1,assigntoSTRList)
                                    spAssignTo?.adapter = assigntoSTRAdapter
                                }

                            }else{
                                MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew, userListRes?.message)
                            }
                        }else{
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response.message())
                        }
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

            })
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }



    private fun setupViewPager() {
        assigntapAdpter = AssignTapAdapte(childFragmentManager)
        assigntapAdpter!!.addFragment(AssignSales(), asssignLIst.get(0))
        assigntapAdpter!!.addFragment(AssignService(), asssignLIst.get(1))
        assigntapAdpter!!.addFragment(AssignSpares(), asssignLIst.get(2))
        assigntapAdpter!!.addFragment(AssignSpareService(), asssignLIst.get(3))
        initTabs()
    }

    @SuppressLint("ResourceType")
    private fun initTabs() {
        assignViewpager?.setAdapter(assigntapAdpter)
        assignTaplayout?.setupWithViewPager(assignViewpager)

        for (i in 0 until assigntapAdpter?.getCount()!!) {
            val textView = TextView(activity)
            textView.typeface = MyUtils.getTypeface(activity!!, "fonts/Lato-Bold.ttf")
            textView.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            textView.gravity = Gravity.CENTER
            textView.setText(asssignLIst.get(i))
            val tab = assignTaplayout?.getTabAt(i)
            if (tab != null) {
                tab.setCustomView(textView)
                tab.setText(assigntapAdpter!!.getPageTitle(i))
            }
        }
        assignTaplayout?.setTabMode(TabLayout.MODE_FIXED)
    }

}
