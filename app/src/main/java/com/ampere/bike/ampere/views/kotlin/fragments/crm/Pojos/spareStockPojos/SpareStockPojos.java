package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareStockPojos;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class SpareStockPojos{

	@SerializedName("delivered_by")
	private int deliveredBy;

	@SerializedName("color")
	private String color;

	@SerializedName("vehicle_model")
	private String vehicleModel;

	@SerializedName("delivery_indent_id")
	private int deliveryIndentId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("indent_id")
	private String indentId;

	@SerializedName("acceptance")
	private String acceptance;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("manufacture_date")
	private String manufactureDate;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("qty")
	private String qty;

	@SerializedName("invoice_type")
	private String invoiceType;

	@SerializedName("id")
	private int id;

	@SerializedName("spare_name")
	private String spareName;

	@SerializedName("serial_no")
	private String serialNo;

	@SerializedName("indent_invoice_date")
	private String indentInvoiceDate;

	@SerializedName("status")
	private String status;

	public void setDeliveredBy(int deliveredBy){
		this.deliveredBy = deliveredBy;
	}

	public int getDeliveredBy(){
		return deliveredBy;
	}

	public void setColor(String color){
		this.color = color;
	}

	public String getColor(){
		return color;
	}

	public void setVehicleModel(String vehicleModel){
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleModel(){
		return vehicleModel;
	}

	public void setDeliveryIndentId(int deliveryIndentId){
		this.deliveryIndentId = deliveryIndentId;
	}

	public int getDeliveryIndentId(){
		return deliveryIndentId;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setIndentId(String indentId){
		this.indentId = indentId;
	}

	public String getIndentId(){
		return indentId;
	}

	public void setAcceptance(String acceptance){
		this.acceptance = acceptance;
	}

	public String getAcceptance(){
		return acceptance;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setManufactureDate(String manufactureDate){
		this.manufactureDate = manufactureDate;
	}

	public String getManufactureDate(){
		return manufactureDate;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setInvoiceType(String invoiceType){
		this.invoiceType = invoiceType;
	}

	public String getInvoiceType(){
		return invoiceType;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setSpareName(String spareName){
		this.spareName = spareName;
	}

	public String getSpareName(){
		return spareName;
	}

	public void setSerialNo(String serialNo){
		this.serialNo = serialNo;
	}

	public String getSerialNo(){
		return serialNo;
	}

	public void setIndentInvoiceDate(String indentInvoiceDate){
		this.indentInvoiceDate = indentInvoiceDate;
	}

	public String getIndentInvoiceDate(){
		return indentInvoiceDate;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"SpareStockPojos{" + 
			"delivered_by = '" + deliveredBy + '\'' + 
			",serviceType = '" + color + '\'' +
			",vehicle_model = '" + vehicleModel + '\'' +
			",delivery_indent_id = '" + deliveryIndentId + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",indent_id = '" + indentId + '\'' + 
			",acceptance = '" + acceptance + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",manufacture_date = '" + manufactureDate + '\'' + 
			",user_id = '" + userId + '\'' + 
			",qty = '" + qty + '\'' + 
			",invoice_type = '" + invoiceType + '\'' + 
			",id = '" + id + '\'' + 
			",spare_name = '" + spareName + '\'' + 
			",serial_no = '" + serialNo + '\'' + 
			",indent_invoice_date = '" + indentInvoiceDate + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}