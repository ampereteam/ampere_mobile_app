package com.ampere.bike.ampere.views.fragment.parthi.Inventory.SpareStock;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ResponsebackToClass;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.parthi.Inventory.SpareStock.Adapter.SpareStockAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Inventory.SpareStock.Pojos.SpareStockPojos;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyHome;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.ampere.bike.ampere.CustomNavigationDuo.snackVIew;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.views.fragment.parthi.Inventory.InventoryHome.msnakview;


public class SpareStock extends Fragment implements ResponsebackToClass {
    RecyclerView spareStockList;
    Common common;
    TextView textView;
        LinearLayout linearLayout ;
    ArrayList<SpareStockPojos> spareStockPojosArrayList  =new ArrayList<>();
//    ArrayList<SpareStockPojos> spareServerArrayList  =new ArrayList<>();

    SpareStockAdapter spareStockAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         View view = inflater.inflate(R.layout.fragment_inventory_spare_stock,container,false);
        common = new Common(getActivity(),this);
        spareStockList = view.findViewById(R.id.spare_Stock_List);
        linearLayout = view.findViewById(R.id.main);
        textView =view.findViewById(R.id.emptyList);
        spareStockList.setLayoutManager(new LinearLayoutManager(getActivity()));
        callApi();
       /* view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(getActivity(), CustomNavigationDuo.class);
//                    // i.putExtra("Comingback", cameback);
                    startActivity(i);


                    return true;
                } else {
                    return false;
                }
            }
        });*/
        return view;
    }
    public void callApi(){
        common.showLoad(true);
        if(common.isNetworkConnected()){
            HashMap<String,String> map = new HashMap<>();
            map.put("userid", LocalSession.getUserInfo(getActivity(),KEY_ID));
            common.callApiRequest("sparestock","POST",map,0);
        }else{
            common.hideLoad();
            MessageUtils.showSnackBarAction(Objects.requireNonNull(getActivity()),snackVIew,getString(R.string.check_internet));
//            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void apiCallBackOverRideMethod(Response<ResponseBody> response, JSONArray objResponse, String method, int position, String message , boolean success) {
        common.hideLoad();
        if(objResponse!=null){
            if(method.equals("sparestock")){
                try{
                spareStockPojosArrayList= new Gson().fromJson(objResponse.toString(),new TypeToken<ArrayList<SpareStockPojos>>(){}.getType());
                }catch (Exception e){
                    e.printStackTrace();
                }
                if(spareStockPojosArrayList.size()>0){
                    linearLayout.setVisibility(View.VISIBLE);
                    spareStockAdapter =new SpareStockAdapter(spareStockPojosArrayList,getActivity());
                    spareStockList.setAdapter(spareStockAdapter);
                }else{
                    textView.setVisibility(View.VISIBLE);
                    textView.setText(message);
                }

            }else{
                Log.v("Method",""+method+"\n"+message);
            }
        }else{
            if( method != null && method.equals("acceptservicereturn")){
                //startActivity(new Intent(getActivity(), WarrantyHome.class));
                MyUtils.passFragmentBackStack(Objects.requireNonNull(getActivity()), new WarrantyHome());
//                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }else{
                linearLayout.setVisibility(View.GONE);
                MessageUtils.showSnackBar(Objects.requireNonNull(getActivity()),snackVIew,"Server Not Found");
            }
        }

    }

}
