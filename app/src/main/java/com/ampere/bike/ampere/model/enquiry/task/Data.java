
package com.ampere.bike.ampere.model.enquiry.task;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Data {

    @SerializedName("tasklist")
    private List<Tasklist> mTasklist;

    public List<Tasklist> getTasklist() {
        return mTasklist;
    }

    public void setTasklist(List<Tasklist> tasklist) {
        mTasklist = tasklist;
    }

}
