package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.vehicleListPojos;

import com.google.gson.annotations.SerializedName;

public class ServiceissuesItem{

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("service_issue")
	private String serviceIssue;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setServiceIssue(String serviceIssue){
		this.serviceIssue = serviceIssue;
	}

	public String getServiceIssue(){
		return serviceIssue;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ServiceissuesItem{" + 
			"updated_at = '" + updatedAt + '\'' + 
			",service_issue = '" + serviceIssue + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}