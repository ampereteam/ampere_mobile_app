package com.ampere.bike.ampere.model.invoiceGenerate.chassisNumber

data class ModelChassisNumber(
        val success: Boolean,
        val data: Data,
        val message: String
)