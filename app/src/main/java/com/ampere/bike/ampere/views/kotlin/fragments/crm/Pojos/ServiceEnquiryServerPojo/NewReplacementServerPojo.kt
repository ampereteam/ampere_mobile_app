package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.ServiceEnquiryServerPojo

import com.google.gson.annotations.SerializedName

  class NewReplacementServerPojo(
        @field:SerializedName("component_name")
        var componentName:String ?=null,
        @field:SerializedName("old_serial_no")
        var oldserial:String ?=null,
        @field:SerializedName("new_serial_no")
        var newSerialNo:String ?=null,
        @field:SerializedName("id")
        var id :Int ?=null
)