package com.ampere.bike.ampere.views.kotlin.model.unassinged.assigned_details

data class AssignedModel(
        val success: Boolean,
        val data: String,
        val message: String
)