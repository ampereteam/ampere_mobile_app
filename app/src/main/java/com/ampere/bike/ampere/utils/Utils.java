package com.ampere.bike.ampere.utils;

public class Utils {
    public static String LOGIN = "login";
    public static String WARRANTY_INVOICE = "warrantyinvoice";


    public static String convertDoubleTwoDigits(String amount) {
        try {
            double numberOfAmount = 0.00;
            try {
                numberOfAmount = Double.parseDouble(amount);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return String.format("%.2f", numberOfAmount);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0.00";
    }

}
