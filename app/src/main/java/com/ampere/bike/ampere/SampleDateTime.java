package com.ampere.bike.ampere;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ampere.bike.ampere.utils.DateUtils;
import com.ampere.vehicles.R;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import customviews.CustomTextView;

public class SampleDateTime extends Fragment {

    @BindView(R.id.txt_dp)
    CustomTextView txtDP;
    @BindView(R.id.txt_tp)
    CustomTextView txtTP;
    @BindView(R.id.dp)
    DatePicker DP;
    @BindView(R.id.tp)
    TimePicker TP;
    @BindView(R.id.day)
    EditText day;
    @BindView(R.id.month)
    EditText month;
    @BindView(R.id.year)
    EditText year;

    String FILE_NAME = "Invoice.pdf";
    String DIR_NAME = "/Ampere_PDF";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.sample_date_time, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

   /*     StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build();
        StrictMode.setThreadPolicy(policy);*/

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        StringBuffer newFile = new StringBuffer();
        newFile.append("SUBBU ANDROID\n");


        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("Invoice\n");
        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("AppName : " + getString(R.string.app_name) + "\n");
        /*newFile.append("Counting Number : " + i + "\n");*/
        newFile.append(
                "*\n" +
                        "*\n" +
                        "*\n");


        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("Invoice\n");
        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("AppName : " + getString(R.string.app_name) + "\n");
        /*newFile.append("Counting Number : " + i + "\n");*/
        newFile.append(
                "*\n" +
                        "*\n" +
                        "*\n");


        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("Invoice\n");
        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("AppName : " + getString(R.string.app_name) + "\n");
        /*newFile.append("Counting Number : " + i + "\n");*/
        newFile.append(
                "*\n" +
                        "*\n" +
                        "*\n");


        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("Invoice\n");
        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("AppName : " + getString(R.string.app_name) + "\n");
        /*newFile.append("Counting Number : " + i + "\n");*/
        newFile.append(
                "*\n" +
                        "*\n" +
                        "*\n");


        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("Invoice\n");
        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("AppName : " + getString(R.string.app_name) + "\n");
        /*newFile.append("Counting Number : " + i + "\n");*/
        newFile.append(
                "*\n" +
                        "*\n" +
                        "*\n");


        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("Invoice\n");
        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("AppName : " + getString(R.string.app_name) + "\n");
        /*newFile.append("Counting Number : " + i + "\n");*/
        newFile.append(
                "*\n" +
                        "*\n" +
                        "*\n");


        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("Invoice\n");
        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("AppName : " + getString(R.string.app_name) + "\n");
        /*newFile.append("Counting Number : " + i + "\n");*/
        newFile.append(
                "*\n" +
                        "*\n" +
                        "*\n");

        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("Invoice\n");
        newFile.append("---------------------------------------------------------------------\n");
        newFile.append("AppName : " + getString(R.string.app_name) + "\n");
        /*newFile.append("Counting Number : " + i + "\n");*/
        newFile.append(
                "*\n" +
                        "*\n" +
                        "*\n");


        createandDisplayPdf(newFile.toString());
        //txtDP.setText(i + "-" + i1 + "-" + i2);
        //txtTP.setText(i + ":" + i1);


        day.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                if (charSequence.toString().length() == 2) {
                    int date = Integer.parseInt(charSequence.toString());
                    if (date <= 31) {
                        month.requestFocus();
                    } else {
                        // day.setText("");
                        day.setError("Enter Valid Day");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.toString().length() == 2) {
                    int mo = Integer.parseInt(charSequence.toString());
                    if (mo <= 12) {
                        year.requestFocus();
                    } else {
                        //month.setText("");
                        month.setError("Enter Valid month");

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence editable, int i, int i1, int i2) {
                String input = editable.toString();

                if (input.length() == 4) {
                    int yr = Integer.parseInt(input);
                    int yrs = Integer.parseInt(DateUtils.getCurrentDate("yyyy"));
                    Log.d("yrssdsd", "" + yrs);
                    yrs = yrs - 18;
                    if (yr <= yrs) {
                    } else {
                        // year.setText("");
                        year.setError("18+");
                    }
                }

/*
                if (input.length() == 2) {
                    year.setText(input + "/");
                } else if (input.length() == 5) {
                    year.setText(input + "/");
                } else if (input.length() == 6) {

                }
                year.setSelection(year.getText().toString().length());*/
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

        year.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_

                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (year.getText().toString().length() == 0) {
                        month.requestFocus();
                    }                    //this is for backspace

                }
                return false;
            }
        });


        month.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_

                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    if (month.getText().toString().length() == 0) {
                        day.requestFocus();
                    }                    //this is for backspace

                }
                return false;
            }
        });

        TP.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int i, int i1) {
                Log.d("sdsdsd", "sdsd" + i + ":" + i1 + timePicker.getMinute());

            }
        });


        txtDP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtDP.setText(getCurrentDate());
            }
        });


        if (DP != null) {
            Calendar today = Calendar.getInstance();
            DP.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {

                @Override
                public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    String msg = "Selected Date is " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                    if (txtDP != null) {
                        txtDP.setText(msg);
                    }
                }
            });
        }

        DP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("sdsds", "DP Onclick");
                txtDP.setText(getCurrentDate());

                txtDP.setText("DD" + DP.getDayOfMonth() + "Y" + DP.getYear() + "MM " + DP.getMonth());
            }
        });
    }


    public void createandDisplayPdf(String text) {
        Document doc = new Document();
        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + DIR_NAME;

            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(dir, FILE_NAME);
            FileOutputStream fOut = new FileOutputStream(file);

            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();

            Paragraph p1 = new Paragraph(text);
            //Font paraFont= new Font(Font.ITALIC);
            p1.setAlignment(Paragraph.ALIGN_CENTER);
            //p1.setFont(paraFont);

            //add paragraph to document
            doc.add(p1);
/*
            int totalpages = 0;

            try {

                ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
                PdfRenderer pdfRenderer = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    pdfRenderer = new PdfRenderer(parcelFileDescriptor);
                    totalpages = pdfRenderer.getPageCount();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            //doc.setPageCount(totalpages);
            Log.d("totalpagessd", "" + totalpages);*/
            Log.e("Create_pdf_success", "**********  invoice.pdf");

            viewPdf(FILE_NAME, DIR_NAME);

            //getActivity().finish();
        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        } finally {
            doc.close();
        }

        //viewPdf("newFile.pdf", "Dir");
    }

    private void viewPdf(String fileStr, String directory) {


        File file = new File(Environment.getExternalStorageDirectory(), directory + "/" + fileStr);
        Uri path = Uri.fromFile(file);
        Intent pdfOpenintent = new Intent(Intent.ACTION_VIEW);
        pdfOpenintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pdfOpenintent.setDataAndType(path/*Uri.fromFile(new File("/sdcard/" + directory + "/" + fileStr))*/, "application/pdf");
        try {
            startActivity(pdfOpenintent);
        } catch (ActivityNotFoundException e) {

        }

       /* File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + directory + "/" + file);
        Log.e("pdfFile*******", "" + pdfFile);
        Uri path = Uri.fromFile(pdfFile);
        Log.d("pathsdsd", "" + path);

        // Setting the intent for pdf reader
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        Uri apkURI = FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".provider", pdfFile);
        Log.d("sdapkURI", "" + apkURI);
        pdfIntent.setDataAndType(apkURI, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            getActivity().startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(), "Can't read pdf file", Toast.LENGTH_SHORT).show();
        }*/
    }


    public String getCurrentDate() {
        StringBuilder builder = new StringBuilder();
        builder.append((DP.getMonth() + 1) + "/");//month is 0 based
        builder.append(DP.getDayOfMonth() + "/");
        builder.append(DP.getYear());
        return builder.toString();
    }

    private DatePicker.OnDateChangedListener dateChangedListener =
            new DatePicker.OnDateChangedListener() {
                @Override
                public void onDateChanged(DatePicker datePicker, int i, int i1, int i2) {
                    txtDP.setText(i + "-" + i1 + "-" + i2);
                }
            };

}
