package com.ampere.bike.ampere.views.kotlin.fragments.inventory.adapter

import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.DateUtils.DD_MM
import com.ampere.bike.ampere.utils.DateUtils.YYYY_MM_DD
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare.Sparedelivered

import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.item_stocks_spare.view.*

class ItemStocksInSpare(var activity: FragmentActivity, val stockDeails: ArrayList<Sparedelivered>) : RecyclerView.Adapter<ItemStocksInSpare.StocksInSpareHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StocksInSpareHolder {

        return StocksInSpareHolder(View.inflate(activity, R.layout.item_stocks_spare, null))
    }

    override fun getItemCount(): Int {
        return stockDeails.size
    }

    override fun onBindViewHolder(holder: StocksInSpareHolder, position: Int) {
        holder.stock_name.text = stockDeails[position].spare_name
        holder.stock_check.visibility = View.VISIBLE
        holder.stock_qty.text = stockDeails[position].qty
        holder.stock_serial_number.text = stockDeails[position].serial_no
        holder.stock_invoice_date.text = DateUtils.convertDates(stockDeails[position].indent_invoice_date, YYYY_MM_DD, DD_MM)
        //if (stockDeails[position].isCheck) {
        holder.stock_check.isChecked = stockDeails[position].isCheck
        //}

        holder.stock_check.setOnCheckedChangeListener { compoundButton, isCheck ->

            Log.d("isCheckWhatThereem", "" + isCheck)
            if (isCheck) {

                stockDeails.add(position, Sparedelivered(stockDeails[position].id, stockDeails[position].spare_name, stockDeails[position].serial_no, stockDeails[position].qty,
                        stockDeails[position].indent_invoice_date, stockDeails[position].manufacture_date, stockDeails[position].status, stockDeails[position].acceptance, stockDeails[position].user_id,
                        stockDeails[position].delivered_by, stockDeails[position].created_at, stockDeails[position].updated_at, true))
            } else {

            }
            notifyDataSetChanged()

        }
    }

    class StocksInSpareHolder(view: View) : RecyclerView.ViewHolder(view) {
        val stock_name = view.stock_name
        val stock_qty = view.stock_qty
        val stock_serial_number = view.stock_serial_number
        val stock_invoice_date = view.stock_invoice_date
        val stock_check = view.stock_check
        val inventory_lay = view.inventory_lay
    }
}