package com.ampere.bike.ampere.model.indent.history;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class ModelIndentDetials implements Serializable {


    @SerializedName("message")
    public String message;

    @SerializedName("indentdetails")
    public List<IndentdetailsItem> indentdetails;

    @SerializedName("status")
    public boolean status;

    @SerializedName("indents")
    public List<IndentsItem> indents;

    @Override
    public String toString() {
        return
                "ModelIndentDetials{" +
                        "indents = '" + indents + '\'' +
                        ",message = '" + message + '\'' +
                        ",indentdetails = '" + indentdetails + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }
}