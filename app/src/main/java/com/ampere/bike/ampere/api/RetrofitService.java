package com.ampere.bike.ampere.api;


import com.ampere.bike.ampere.model.CheckForceUpdate;
import com.ampere.bike.ampere.model.GetData;
import com.ampere.bike.ampere.model.calendar.ModelCalendar;
import com.ampere.bike.ampere.model.enquiry.ModelLeadsInfo;
import com.ampere.bike.ampere.model.enquiry.ModelSalesInsert;
import com.ampere.bike.ampere.model.enquiry.cityBasedOnState.ModelCityBasedOnState;
import com.ampere.bike.ampere.model.enquiry.edit.ModelServiceComponets;
import com.ampere.bike.ampere.model.enquiry.spares.ModelVehicleModelBaseSpares;
import com.ampere.bike.ampere.model.enquiry.task.ModelTaskList;
import com.ampere.bike.ampere.model.forgotpassword.ModelForgotPassword;
import com.ampere.bike.ampere.model.indent.IndentModelSuccessResponse;
import com.ampere.bike.ampere.model.indent.IndentVehicleRequestModel;
import com.ampere.bike.ampere.model.indent.history.IndentModelViewDetails;
import com.ampere.bike.ampere.model.indent.history.ModelIndentDetials;
import com.ampere.bike.ampere.model.invoiceGenerate.chassisNumber.ModelChassisNumber;
import com.ampere.bike.ampere.model.invoiceGenerate.initial.ModelInvoice;
import com.ampere.bike.ampere.model.invoiceGenerate.storedInvoice.ModelInvoiceStored;
import com.ampere.bike.ampere.model.invoiceGenerate.variantColor.ModelVariantColor;
import com.ampere.bike.ampere.model.leads.ModelAssign;
import com.ampere.bike.ampere.model.login.ModelLogin;
import com.ampere.bike.ampere.model.profile.ModelProfile;
import com.ampere.bike.ampere.model.profile.ModelUpdateProfile;
import com.ampere.bike.ampere.model.task.TaskUpdateModel;
import com.ampere.bike.ampere.model.task.ViewTaskModel;
import com.ampere.bike.ampere.model.warranty.InvoiceWarranty;
import com.ampere.bike.ampere.views.fragment.indent.VehicleListPojos.DataList;
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Complaints.ComplaintListPojo.ComplaintLISTRES;
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoicePojos.SpareInvoicesPojo;
import com.ampere.bike.ampere.views.kotlin.fragments.crm.sparesInvoice.Pojos.GetspareinvoiceResList;
import com.ampere.bike.ampere.views.kotlin.fragments.invoice.invoicecolorpojos.ColorResponse;
import com.ampere.bike.ampere.views.kotlin.fragments.model.model_crm.StatusModel;
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.TaskDetailsModel;
import com.ampere.bike.ampere.views.kotlin.model.MyEnquiryModel;
import com.ampere.bike.ampere.views.kotlin.model.customersales.StockDetailsModel;
import com.ampere.bike.ampere.views.kotlin.model.inventory.AcceptedStockModel;
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare.InventorySpareModel;
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_vehicle.InventoryModel;
import com.ampere.bike.ampere.views.kotlin.model.invoice.AddInvoice;
import com.ampere.bike.ampere.views.kotlin.model.unassinged.UnAssginedTaskModel;
import com.ampere.bike.ampere.views.kotlin.model.unassinged.assign_details.AssingFromCRM;
import com.ampere.bike.ampere.views.kotlin.model.unassinged.assigned_details.AssignedModel;

import java.util.HashMap;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

@SuppressWarnings("ALL")
public interface RetrofitService {

    /**
     * Check App is available for update or not
     *
     * @return
     */
    @POST("forceupdate")
    Call<CheckForceUpdate> onCheckUpdate(@Header("Authorization") String token);

    /**
     * Get Datas for testing
     */
    @POST("appupdate")
    Call<GetData> onStoreInslattedDevice(@Header("Authorization") String token, @Body HashMap<String, String> map);

    /**
     * Register UserIntentModel Deatils for Sign In
     *
     * @param name
     * @param mobileNo
     * @param email
     * @param pwd
     * @return
     */
    @FormUrlEncoded
    @POST("register")
    Call<ModelLogin> onRegister(@Field("user_name") String name, @Field("mobile_no") String mobileNo, @Field("email") String email, @Field("password") String pwd);


    /**
     * Login UserIntentModel
     *
     * @return
     */

    @POST("login")
    Call<ModelLogin> onLogin(@Body HashMap<String, String> map);

    /**
     * Read Leads Initial Information
     *
     * @param cust_name
     * @return+
     */
    @Headers("Content-Type: application/json")
    @FormUrlEncoded
    @POST("datalist")
    Call<ModelLeadsInfo> onLoadLeadsDetails(@Field("user_name") String cust_name, @Field("user_type") String user_type, @Header("Authorization") String token);

    /**
     * Get Citye from Selected State
     *
     * @param state
     * @return
     */
    @POST("citylist")
    Call<ModelCityBasedOnState> onLoadCity(@Header("Authorization") String token, @Body HashMap<String, String> state);

    @POST("tasklist")
    Call<ModelTaskList> onLoadTask(@Header("Authorization") String token, @Body HashMap<String, String> state);


    @POST("spareslist")
    Call<ModelVehicleModelBaseSpares>   onLoadSparesInEnquiry(@Header("Authorization") String token, @Body HashMap<String, String> state);

    /**
     * Assign Task with Sales
     *
     * @param salesInsert
     * @return
     */
    @Headers("Content-Type: application/json")
    @POST("addenquiry")
    Call<ModelSalesInsert> onSales(@Header("Authorization") String token, @Body HashMap<String, Object> salesInsert);


    @POST("assign_by")
    @Headers("Content-Type: application/json")
    @FormUrlEncoded
    Call<ModelAssign> onVehicleModel(@Header("Authorization") String token, @Field("user_name") String user_name, @Field("assign_by") String assign_by, @Field("city") String city, @Field("mobile_no") String mobile, @Field("Authorization") String userInfo);

    /**
     * Getting only Entered City Details
     *
     * @param assigned
     * @param city
     * @param user_name
     * @param token
     * @return
     */
    @Headers("Content-Type: application/json")
    @FormUrlEncoded
    @POST("assign_by")
    Call<ModelAssign> onLoadAssigned(@Field("assign_by") String assigned, @Field("city") String city, @Field("enquiry_for") String enquiry_for, @Field("user_name") String user_name, @Header("Authorization") String token);


    /**
     * Reset Password
     *
     * @param email
     * @return
     */
    @FormUrlEncoded
    @POST("forgot_password")
    Call<ModelForgotPassword> onForgotPassword(@Header("Authorization") String token, @Field("email") String email);


    /**
     * Load Profile Inforation
     *
     * @param userName
     * @return
     */
    @FormUrlEncoded
    @POST("profile")
    Call<ModelProfile> onLoadProfile(@Header("Authorization") String token, @Field("user_name") String userName);

    /**
     * Update Changed Profile Information
     *
     * @param crName
     * @param crAddress
     * @param crMobile
     * @param crEmail
     * @return
     */
    @FormUrlEncoded
    @POST("update_profile")
    Call<ModelUpdateProfile> onUpdateProfile(@Header("Authorization") String token, @Field("user_name") String crName, @Field("address") String crAddress, @Field("mobile_no") String crMobile, @Field("email") String crEmail);


    /**
     * Read Task and show in Calendar
     *
     * @param taskInMonth
     * @return
     */


    @POST("taskcalender")
    Call<ModelCalendar> onCalendarManager(@Header("Authorization") String token, @Body HashMap<String, String> taskInMonth);

    /**
     * Assign Task
     *
     * @param stringStringHashMap
     * @return
     */
    @Headers("Content-Type: application/json")
    @POST("assign_by")
    Call<ModelAssign> onAssign(@Header("Authorization") String token, @Body HashMap<String, String> stringStringHashMap);

    //Call<ModelSalesInsert> onSales(String name, String email, String mobileNo, String date, String time, String cityAuto, String leadSource, String enquiry, String vehicleMode, String message, String assigneto, String assigned, String dateTwo, String task);


    /**
     * View Task
     *
     * @param stringStringHashMap
     * @return
     */
    @Headers("Content-Type: application/json")
    @POST("enquirylist")
    Call<ViewTaskModel> onTaskView(@Header("Authorization") String token, @Body HashMap<String, String> stringStringHashMap);
    //dealer/enquirylist

    /**
     * Task Update
     *
     * @param stringStringHashMap
     * @return
     */
    @Headers("Content-Type: application/json")
    @POST("updatetaskstatus")
    Call<TaskUpdateModel> onTaskStatusChange(@Header("Authorization") String token, @Body HashMap<String, String> stringStringHashMap);

    /**
     * Get Indetn Initial Details
     *
     * @param map
     * @return
     */

    @POST("request_indent")
    Call<IndentVehicleRequestModel> onIndentRequest(@Header("Authorization") String token, @Body HashMap<String, String> map);

    /**
     * Passed all Indetn requesting  values
     *
     * @param indentModels
     * @return
     */

    @POST("indent")
    Call<IndentModelSuccessResponse> onIndentRaise(@Header("Authorization") String token, @Body HashMap<String, Object> indentModels);


    @POST("spares")
    Call<ModelVehicleModelBaseSpares> onGetSpares(@Header("Authorization") String token, @Body HashMap<String, String> map);

    /**
     * Get Details of Indent
     *
     * @param map
     * @return
     */

    @POST("viewindent")
    Call<ModelIndentDetials> onIndentDetails(@Header("Authorization") String token, @Body HashMap<String, String> map);

    @POST("viewindentdetail")
    Call<IndentModelViewDetails> onIndentViewDetails(@Header("Authorization") String token, @Body HashMap<String, String> map);


    //@POST("viewenquirydetails")
    @POST("viewenquiry")
    Call<TaskDetailsModel> onLoadTaskDetails(@Header("Authorization") String token, @Body HashMap<String, String> miniTask);

    /*
    @POST("addnewtask")
    Call<TaskDetailsModel> onAddNextTask(@Part MultipartBody.Part part, @Body HashMap<String, String> map);*/

    @Multipart
    @POST("addnewtask")
    Call<TaskDetailsModel> onAddNextTask(@Header("Authorization") String token, @Part MultipartBody.Part image, @Part("next_activity_date") RequestBody next_activity_date, @Part("next_activity_time") RequestBody next_activity_time,
                                         @Part("enquiry_status") RequestBody enquiry_status, @Part("remarks") RequestBody remarks, @Part("user_name") RequestBody user_name,
                                         @Part("enquiry_id") RequestBody enquiry_id, @Part("enquirytaskid") RequestBody enquirytaskid, @Part("nexttask") RequestBody nexttask, @Part("imagename") RequestBody imagename);


    @POST("taskbystatus")
    Call<StatusModel> onGetStatus(@Header("Authorization") String token, @Body HashMap<String, String> map);

    @POST("viewenquirybyspecialuser")
    Call<MyEnquiryModel> onMyEnquiryDetails(@Header("Authorization") String token, @Body HashMap<String, String> map);


    @POST("addinvoice")
    Call<AddInvoice> onAddInvoice(@Header("Authorization") String token, @Body HashMap<String, String> map);

    @POST("dealersales")
    Call<StockDetailsModel> onStockDetails(@Header("Authorization") String token, @Body HashMap<String, String> map);

    @POST("vehicleinventory")
    Call<InventoryModel> onInventoryDetailsForVehicle(@Header("Authorization") String token, @Body HashMap<String, String> map);

    @POST("spareinventory")
    Call<InventorySpareModel> onInventoryDetailsForSpare(@Header("Authorization") String token, @Body HashMap<String, String> map);

    /**
     * Read UnAssigned Task for
     */
    @POST("viewunassignedenquiry")
    Call<UnAssginedTaskModel> onUnAssignedTask(@Header("Authorization") String token, @Body HashMap<String, String> map);

    /**
     * Getting information fo Assign task from CRM Login
     */
    @POST("enquiryassign")
    Call<AssingFromCRM> onAssignFromCRM(@Header("Authorization") String token, @Body HashMap<String, String> map);

    /**
     * Assign Success will maintain values
     *
     * @param map
     * @return
     */
    @POST("assigned_task_from_crm")
    Call<AssignedModel> onAssignedInCRM(@Header("Authorization") String token, @Body HashMap<String, String> map);

    @POST("acceptbulkinventory")
    Call<AcceptedStockModel> onAcceptBulkStocks(@Header("Authorization") String token, @Body HashMap<String, Object> map);

    @POST("acceptinventory")
    Call<AcceptedStockModel> onAcceptStocks(@Header("Authorization") String token, @Body HashMap<String, String> map);

    @POST("warrantyinvoice")
    @FormUrlEncoded
    Call<InvoiceWarranty> onInvoiceWarranty(@Header("Authorization") String token, @Field("userid") String userId);

    /**
     * Service Componet based on Service Type
     *
     * @param token
     * @param map
     * @return
     */

    @POST("service_component")
    Call<ModelServiceComponets> onGetServiceComponents(@Header("Authorization") String token, @Body HashMap<String, String> map);


    @POST("updatetask")
    Call<TaskDetailsModel> onUpdateEnquiry(@Header("Authorization") String token, @Body HashMap<String, Object> map);


    /**
     * Get the indent vehicle list.
     * get states
     * lead sources
     *
     * @param token
     * @return
     */
    @POST("datalist")
    Call<DataList> getDataList(@Header("Authorization") String token);


    @POST("{path}")
    Call<ResponseBody> call_post(@Path("path") String method, @Header("Authorization") String auth, @Body HashMap<String, Object> map);

    @POST("{path}")
    Call<ComplaintLISTRES> call(@Path("path") String method, @Header("Authorization") String auth, @Body HashMap<String, Object> map);


    /**
     * Pass Enquiry Id and get Invoice Details for genetate of Invoice
     *
     * @param token
     * @param map
     * @return
     */
    @POST("salesinvoice")
    Call<ModelInvoice> callInvoiceGenerateDetails(@Header("Authorization") String token, @Body HashMap<String, String> map);

    /**
     * Get Model Variant from vehicle model selection
     *
     * @param token
     * @param map
     * @return
     */
    @POST("vehiclecolorlist")
    Call<ColorResponse> callForModelVariantFromVehicleModel(@Header("Authorization") String token, @Body HashMap<String, String> map);

    /**
     * Save Invoice as PDF in server and Download in Local
     *
     * @param
     * @param map
     * @return
     */
    @POST("saveinvoice")
    Call<ModelInvoiceStored> calledToStoreInvoice(@Header("Authorization") String token, @Body HashMap<String, String> map);


    @POST("color_spares")
    Call<ModelVariantColor> onLoadColors(@Header("Authorization") String header, @Body HashMap<String, String> map);


    @POST("chassisdetails")
    Call<ModelChassisNumber> calledChassisNumber(@Header("Authorization") String header, @Body HashMap<String, String> map);

    /**
     * Call api to add  service warranty complaints
     * @param method
     * @param auth
     * @param chassisno
     * @param complaints
     * @param enquiry_id
     * @param userid
     * @param complaintImage
     * @return
     */
    @Multipart
    @POST("{path}")
    Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>
//    Call<ResponseBody>
    call_post(@Path("path") String method,
                    @Header("Authorization") String auth,
                    @Part("chassis_no") RequestBody chassisno,
//                    @Part("defect_item") RequestBody defectItem,
//                    @Part("defect_qty") RequestBody defectQty,
                    @PartMap HashMap<String,Object> map ,
                    @Part("complaint") RequestBody complaints,
                    @Part("enquiry_id") RequestBody enquiry_id,
                    @Part("userid") RequestBody userid,
                    @Part MultipartBody.Part complaintImage
              );
    @Multipart
    @POST("{path}")
    Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>
//    Call<ResponseBody>
    call_po(@Path("path") String method,
              @Header("Authorization") String auth,
              @Part("serial_no") RequestBody serial_no,
              @Part("complaint") RequestBody complaints,
              @Part("enquiry_id") RequestBody enquiry_id,
              @Part("userid") RequestBody userid,
              @Part MultipartBody.Part complaintImage
    );
    @POST("getspareinvoice")
    Call<SpareInvoicesPojo> call_post(@Header("Authorization") String auth, @Body HashMap<String, Object> jsonObject);


    /**
     *
     * @param path
     * @param auth
     * @param map
     * @return
     */
    @POST("{path}")
    @FormUrlEncoded
    Call<GetspareinvoiceResList>   call_post_get(@Path("path") String path  , @Header("Authorization") String auth , @FieldMap HashMap<String,Object> map);

}

