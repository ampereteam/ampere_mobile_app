package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareDetailsPojos

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class InvoicesItem(

	@field:SerializedName("email_id")
	val emailId: String? = null,

	@field:SerializedName("sgst")
	val sgst: String? = null,

	@field:SerializedName("occupation")
	val occupation: String? = null,

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("gender")
	val gender: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("vehicle_model")
	val vehicleModel: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("statecode")
	val statecode: String? = null,

	@field:SerializedName("invoice_amount")
	val invoiceAmount: String? = null,

	@field:SerializedName("igst")
	val igst: String? = null,

	@field:SerializedName("invoice_date")
	val invoiceDate: String? = null,

	@field:SerializedName("igst_amount")
	val igstAmount: Any? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("aadhar_no")
	val aadharNo: String? = null,

	@field:SerializedName("invoice_id")
	val invoiceId: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("sgst_amount")
	val sgstAmount: String? = null,

	@field:SerializedName("address_proof")
	val addressProof: String? = null,

	@field:SerializedName("invoice_by")
	val invoiceBy: Int? = null,

	@field:SerializedName("delivery_at")
	val deliveryAt: String? = null,

	@field:SerializedName("pincode")
	val pincode: String? = null,

	@field:SerializedName("id_proof")
	val idProof: String? = null,

	@field:SerializedName("invoice_no")
	val invoiceNo: String? = null,

	@field:SerializedName("locality")
	val locality: String? = null,

	@field:SerializedName("mobile")
	val mobile: String? = null,

	@field:SerializedName("cgst")
	val cgst: String? = null,

	@field:SerializedName("customer_address1")
	val customerAddress1: String? = null,

	@field:SerializedName("customer_address2")
	val customerAddress2: String? = null,

	@field:SerializedName("total_amount")
	val totalAmount: String? = null,

	@field:SerializedName("dob")
	val dob: String? = null,

	@field:SerializedName("customer_name")
	val customerName: String? = null,

	@field:SerializedName("cgst_amount")
	val cgstAmount: String? = null,

	@field:SerializedName("enquiry_id")
	val enquiryId: Int? = null
)