package com.ampere.bike.ampere.views.fragment.enquiry.calllog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ampere.bike.ampere.utils.CallLogHelper;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.vehicles.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.ampere.bike.ampere.utils.CallLogDate.UNKNOWN;

/**
 * Created by AND I5 on 04-10-2017.
 */
public class CallLogActivity extends AppCompatActivity {
    private RecyclerView mCallLog;
    private TextView mNoCallLog;
    ArrayList<HashMap<String, String>> callLogLis = new ArrayList<>();
    private Dialog dialog;
    private LinearLayout llLoader;
    LoadContact loadContact;
    ImageView icbackCrm;

   /* private ArrayList<String> conNames;
    private ArrayList<String> conNumbers;
    private ArrayList<String> conTime;
    private ArrayList<String> conDate;
    private ArrayList<String> conType;*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.call_log);
        getSupportActionBar().hide();
        mCallLog = findViewById(R.id.call_log_list_view);
        mNoCallLog = findViewById(R.id.call_log_no_data);
        icbackCrm = findViewById(R.id.ic_back_crm);
        llLoader = findViewById(R.id.llLoader);
        llLoader.setVisibility(View.GONE);
        mCallLog.setHasFixedSize(false);
        mCallLog.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        //callLogLis = getCallDetails();
        dialog = new Dialog(CallLogActivity.this);
        dialog.setContentView(R.layout.circular_progress_bar);
        dialog.setCancelable(true);
        dialog.show();
        llLoader.setVisibility(View.GONE);

       /* conNames = new ArrayList<String>();
        conNumbers = new ArrayList<String>();
        conTime = new ArrayList<String>();
        conDate = new ArrayList<String>();
        conType = new ArrayList<String>();
*/
        Cursor curLog = CallLogHelper.getAllCallLogs(getContentResolver());
        setCallLogs(curLog);
        // getContacts();

        /*loadContact = new LoadContact();
        loadContact.execute();*/
        //Log.d("stringBuffer", "" + callLogLis.size());

        icbackCrm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setCallLogs(Cursor curLog) {
        while (curLog.moveToNext()) {

            String callDate = curLog.getString(curLog.getColumnIndex(android.provider.CallLog.Calls.DATE));
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
            String dateString = formatter.format(new Date(Long.parseLong(callDate)));

            String callDates = curLog.getString(curLog.getColumnIndex(android.provider.CallLog.Calls.DATE));
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -2);
            String dates = dateFormat.format(cal.getTime()); //your formatted date here
            String strDate = dateFormat.format(new Date(Long.parseLong(callDates)));

            //Log.d("sakjndsnfgkjdsa", "dates " + dates + " strdate " + strDate);
            HashMap<String, String> stringStringHashMap = new HashMap<>();
            String cusName = "";
            //if (dates.compareTo(strDate) <= 0) {
            String callNumber = curLog.getString(curLog
                    .getColumnIndex(android.provider.CallLog.Calls.NUMBER));
            //conNumbers.add(callNumber);

            String callName = curLog
                    .getString(curLog
                            .getColumnIndex(android.provider.CallLog.Calls.CACHED_NAME));
            if (callName == null) {
                cusName = UNKNOWN;
                //conNames.add("Unknown");
            } else {
                cusName = callName;
                //conNames.add(callName);
            }
            String dir = "";
            String callType = curLog.getString(curLog
                    .getColumnIndex(android.provider.CallLog.Calls.TYPE));
            if (callType.equals("1")) {
                // conType.add("Incoming");
                dir = "INCOMING";
            } else if (callType.equals("2")) {
                //conType.add("Outgoing");
                dir = "OUTGOING";
            } else if (callType.equals("3")) {
                // conType.add("Missed");
                dir = "MISSED";
            } else if (callType.equals("5")) {
                dir = "REJECTED";
            }


            String duration = curLog.getString(curLog
                    .getColumnIndex(android.provider.CallLog.Calls.DURATION));
            // conTime.add(duration);

            // conDate.add(dateString);


            stringStringHashMap.put("phNumber", callNumber);
            stringStringHashMap.put("dir", dir);
            stringStringHashMap.put("callDayTime", "" + dateString);
            stringStringHashMap.put("callDuration", duration);
            stringStringHashMap.put("contactName", cusName);
            callLogLis.add(stringStringHashMap);
            //}

            MessageUtils.dismissDialog(dialog);
            if (callLogLis.size() >= 1) {
                mCallLog.setAdapter(new ItemCallLog(CallLogActivity.this, callLogLis));
                mNoCallLog.setVisibility(View.GONE);
                mCallLog.setVisibility(View.VISIBLE);
            } else {
                mNoCallLog.setVisibility(View.VISIBLE);
                mCallLog.setVisibility(View.GONE);

            }
        }
    }

    private void getContacts() {
        Cursor managedCursor = managedQuery(Uri.parse("content://call_log/calls"), null, null, null, CallLog.Calls.DATE + " DESC");
        Log.d("managedCursor", "" + managedCursor.getCount());
        /*Cursor c = getApplicationContext().getContentResolver().query(CallLog.Calls.CONTENT_URI, projection, null,
                null, CallLog.Calls.DATE + " DESC");*/
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            // String phName = getContactname(phNumber, getApplicationContext());

            String dir = null;
            int dircode = Integer.parseInt(callType);
            //Log.d("dircode", "" + dircode);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;
                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;
                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
                case CallLog.Calls.REJECTED_TYPE:
                    dir = "REJECTED";
                    break;


            }

            HashMap<String, String> stringStringHashMap = new HashMap<>();
            stringStringHashMap.put("phNumber", phNumber);
            stringStringHashMap.put("dir", dir);
            stringStringHashMap.put("callDayTime", "" + callDayTime);
            stringStringHashMap.put("callDuration", callDuration);
            //stringStringHashMap.put("contactName", phName);
            callLogLis.add(stringStringHashMap);
        }

        MessageUtils.dismissDialog(dialog);
        if (callLogLis.size() >= 1) {
            mCallLog.setAdapter(new ItemCallLog(CallLogActivity.this, callLogLis));
            mNoCallLog.setVisibility(View.GONE);
            mCallLog.setVisibility(View.VISIBLE);
        } else {
            mNoCallLog.setVisibility(View.VISIBLE);
            mCallLog.setVisibility(View.GONE);

        }
        Log.d("callLogLis", "" + callLogLis.size());
    }

    @Override
    protected void onResume() {
        super.onResume();
        //  new LoadContact().execute();
    }


    class LoadContact extends AsyncTask<ArrayList<HashMap<String, String>>, Void, ArrayList<HashMap<String, String>>> {
        private ProgressDialog progressBar;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(CallLogActivity.this);
            dialog.setContentView(R.layout.circular_progress_bar);
            dialog.setCancelable(true);
            dialog.show();
            llLoader.setVisibility(View.GONE);
        }

        @Override
        protected ArrayList<HashMap<String, String>> doInBackground(ArrayList<HashMap<String, String>>... params) {
            Log.d("dsdsd", "managedCursor");
            StringBuffer sb = new StringBuffer();
            Cursor managedCursor = managedQuery(Uri.parse("content://call_log/calls"), null, null, null, CallLog.Calls.DATE + " DESC");
            Log.d("managedCursor", "" + managedCursor.getCount());
        /*Cursor c = getApplicationContext().getContentResolver().query(CallLog.Calls.CONTENT_URI, projection, null,
                null, CallLog.Calls.DATE + " DESC");*/
            int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
            int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
            int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
            int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);

            while (managedCursor.moveToNext()) {
                String phNumber = managedCursor.getString(number);
                String callType = managedCursor.getString(type);
                String callDate = managedCursor.getString(date);
                Date callDayTime = new Date(Long.valueOf(callDate));
                String callDuration = managedCursor.getString(duration);
                // String phName = getContactname(phNumber, getApplicationContext());

                String dir = null;
                int dircode = Integer.parseInt(callType);
                //Log.d("dircode", "" + dircode);
                switch (dircode) {
                    case CallLog.Calls.OUTGOING_TYPE:
                        dir = "OUTGOING";
                        break;
                    case CallLog.Calls.INCOMING_TYPE:
                        dir = "INCOMING";
                        break;
                    case CallLog.Calls.MISSED_TYPE:
                        dir = "MISSED";
                        break;
                    case CallLog.Calls.REJECTED_TYPE:
                        dir = "REJECTED";
                        break;

                }

                HashMap<String, String> stringStringHashMap = new HashMap<>();
                stringStringHashMap.put("phNumber", phNumber);
                stringStringHashMap.put("dir", dir);
                stringStringHashMap.put("callDayTime", "" + callDayTime);
                stringStringHashMap.put("callDuration", callDuration);
                //stringStringHashMap.put("contactName", phName);
                callLogLis.add(stringStringHashMap);
           /* sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");
            Log.d("callLogLis", "" + callLogLis.size() + "  " + callLogLis);
            Log.d("sbCallLog", "" + sb);*/

            } //managedCursor.close(); textView.setText(sb); }

            return callLogLis;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            MessageUtils.dismissDialog(dialog);
            loadContact.cancel(true);
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String, String>> callLogLis) {
            super.onPostExecute(callLogLis);
            MessageUtils.dismissDialog(dialog);
            //llLoader.setVisibility(View.GONE);
            if (callLogLis.size() >= 1) {
                mCallLog.setAdapter(new ItemCallLog(CallLogActivity.this, callLogLis));
                mNoCallLog.setVisibility(View.GONE);
                mCallLog.setVisibility(View.VISIBLE);
            } else {
                mNoCallLog.setVisibility(View.VISIBLE);
                mCallLog.setVisibility(View.GONE);

            }
        }
    }

    private ArrayList<HashMap<String, String>> getCallDetails() {
        StringBuffer sb = new StringBuffer();
        Cursor managedCursor = managedQuery(Uri.parse("content://call_log/calls"), null, null, null, CallLog.Calls.DATE + " DESC");
        /*Cursor c = getApplicationContext().getContentResolver().query(CallLog.Calls.CONTENT_URI, projection, null,
                null, CallLog.Calls.DATE + " DESC");*/
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int displya_name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        Log.d("displya_name", "" + displya_name);

        sb.append("Call Log :");
        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String phName = getContactname(phNumber, getApplicationContext());
            Log.d("phName", "" + phName);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            //Log.d("dircode", "" + dircode);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;
                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;
                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
                case CallLog.Calls.REJECTED_TYPE:
                    dir = "REJECTED";
                    break;


            }

            HashMap<String, String> stringStringHashMap = new HashMap<>();
            stringStringHashMap.put("phNumber", phNumber);
            stringStringHashMap.put("dir", dir);
            stringStringHashMap.put("callDayTime", "" + callDayTime);
            stringStringHashMap.put("callDuration", callDuration);

            callLogLis.add(stringStringHashMap);
           /* sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");
            Log.d("callLogLis", "" + callLogLis.size() + "  " + callLogLis);
            Log.d("sbCallLog", "" + sb);*/
            if (callLogLis.size() >= 1) {
                mCallLog.setAdapter(new ItemCallLog(this, callLogLis));
                mNoCallLog.setVisibility(View.GONE);
                mCallLog.setVisibility(View.VISIBLE);
            } else {
                mNoCallLog.setVisibility(View.VISIBLE);
                mCallLog.setVisibility(View.GONE);

            }
        } //managedCursor.close(); textView.setText(sb); }

        return callLogLis;
    }


    private String getContactname(String phoneNumber2, Context context) {
        // TODO Auto-generated method stub
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber2));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null) {
            return null;
        }
        String contactName = null;
        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }

        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }

        return contactName;
    }
}
