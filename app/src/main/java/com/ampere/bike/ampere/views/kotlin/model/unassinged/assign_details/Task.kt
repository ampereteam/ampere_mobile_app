package com.ampere.bike.ampere.views.kotlin.model.unassinged.assign_details

data class Task(
        val id: Int,
        val task_type: String,
        val status: String,
        val task: String,
        val created_at: String,
        val updated_at: String
)