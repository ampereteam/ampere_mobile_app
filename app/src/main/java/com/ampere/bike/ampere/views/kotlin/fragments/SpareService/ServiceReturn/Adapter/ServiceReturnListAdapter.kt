package com.ampere.bike.ampere.views.kotlin.fragments.SpareService.ServiceReturn.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.ServiceReturn.Pojo.ServicereturnsItem
import com.ampere.vehicles.R

class ServiceReturnListAdapter(val context: Context,val serviceReturnList :ArrayList<ServicereturnsItem?>?,val configClickEvent:ConfigClickEvent)
    :RecyclerView.Adapter<ServiceReturnListAdapter.ViewHolder>(){

var i:Int =0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceReturnListAdapter.ViewHolder {
        val view = LayoutInflater.from(this.context).inflate(R.layout.adapter_spare_service_return_list,parent,false )
        return  ViewHolder(view)
    }

    override fun getItemCount(): Int { return  this.serviceReturnList?.size!! }

    override fun onBindViewHolder(holder: ServiceReturnListAdapter.ViewHolder, position: Int) {
       try{
           i=serviceReturnList?.size!!
           for(i:Int in 0..serviceReturnList?.size!!-1){
               holder.checkbox.isChecked=serviceReturnList[position]?.check!!
           }
           holder.sno.text = (position+1).toString()
           holder.serviceType.text =serviceReturnList!![position]?.defectServiceType
           holder.serialNumber.text = serviceReturnList[position]?.defectSerialNo
           holder.sentDate.text = serviceReturnList[position]?.sentDate
           holder.sendThrough.text =serviceReturnList[position]?.sendThrough
           /*holder.linearLayout.setOnClickListener {
               this.configClickEvent.connectposition(position,invoiceList.size)
           }*/
           if (serviceReturnList.get(position)?.check!!) {
               holder.checkbox.setChecked(true)
               i = serviceReturnList.size
           } else {
               holder.checkbox.setChecked(false)
               i = 0
           }
           holder.checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
               serviceReturnList.get(position)!!.check=isChecked
               if (serviceReturnList.get(position)!!.check!!) {
                   if (serviceReturnList.size == i) {
                       configClickEvent.connectposition(position, i)
                   } else {
                       configClickEvent.connectposition(position, ++i)
                   }

               } else {
                   if (i != 0) {
                       configClickEvent.connectposition(position, --i)
                   } else {
                       configClickEvent.connectposition(position, 0)
                   }
               }

           }

       }catch (ex:Exception){
           ex.printStackTrace()
       }
    }
    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val sno : TextView = itemView?.findViewById(R.id.spssr_s_no)!!
        val sendThrough : TextView = itemView?.findViewById(R.id.spssr_send_through)!!
        val serviceType : TextView = itemView?.findViewById(R.id.spssr_service_type)!!
        val serialNumber : TextView = itemView?.findViewById(R.id.spssr_serial_number)!!
        val sentDate : TextView = itemView?.findViewById(R.id.spssr_sent_date)!!
        val linearLayout : LinearLayout = itemView?.findViewById(R.id.spssr_linear_adp)!!
        val checkbox : CheckBox = itemView?.findViewById(R.id.spssr_checkbox)!!
    }
}