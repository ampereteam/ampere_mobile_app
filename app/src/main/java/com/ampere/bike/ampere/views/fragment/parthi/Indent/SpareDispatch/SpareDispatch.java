package com.ampere.bike.ampere.views.fragment.parthi.Indent.SpareDispatch;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.api.Retrofit.NetworkManager;
import com.ampere.bike.ampere.api.Retrofit.ResponsebackToClass;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentHome;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.SpareDispatch.Adapter.SpareDispatchAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.SpareDispatch.Pojos.SpareDispatchPojos;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ampere.bike.ampere.CustomNavigationDuo.snackVIew;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;
import static com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentHome.msnackview;


public class SpareDispatch extends Fragment implements ResponsebackToClass, ConfigClickEvent {
    Common common;
    // SessionManager sessionManager;
    RecyclerView recyclerView;
    CheckBox checkBox;
    ArrayList<SpareDispatchPojos> spareDispatchPojosArrayList = new ArrayList<>();
    ArrayList<SpareDispatchPojos> spareServerList = new ArrayList<>();
    SpareDispatchAdapter spareDispatchAdapter;
    Button button;
    TextView textView;
    LinearLayout linearLayout;
    LinearLayout  linspareListtitle;
    Dialog dialog ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_indent_spare_dispatch, container, false);
        common = new Common(getActivity(), this);
        dialog =new Dialog(Objects.requireNonNull(getActivity()));
        //sessionManager = new SessionManager(getActivity());
        recyclerView = view.findViewById(R.id.vehile_dispatch_List);
        checkBox = view.findViewById(R.id.checkbox);
        button = view.findViewById(R.id.acpt);
        textView = view.findViewById(R.id.emptyList);
        linearLayout = view.findViewById(R.id.accept);
        linspareListtitle = view.findViewById(R.id.main);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        CallApi();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (spareDispatchPojosArrayList.size() > 0) {
                    if (isChecked) {
                        int i;
                        for (i = 0; spareDispatchPojosArrayList.size() > i; i++) {
                            spareDispatchPojosArrayList.get(i).setCheck(true);
                        }
//                        spareDispatchAdapter.setList(spareDispatchPojosArrayList);
                        spareDispatchAdapter.notifyDataSetChanged();
                        linearLayout.setVisibility(View.VISIBLE);
                    } else {

                        for (int i = 0; spareDispatchPojosArrayList.size() > i; i++) {
                            spareDispatchPojosArrayList.get(i).setCheck(false);
                        }

//                        spareDispatchAdapter.setList(spareDispatchPojosArrayList);
                        spareDispatchAdapter.notifyDataSetChanged();
                        linearLayout.setVisibility(View.GONE);
                    }
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToserver();
            }
        });
     /*   view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(getActivity(), CustomNavigationDuo.class);
                    i.putExtra("SELECTEDVALUE", 0);
//                    // i.putExtra("Comingback", cameback);
                    startActivity(i);


                    return true;
                } else {
                    return false;
                }
            }
        });*/
        return view;

    }

    public void CallApi() {
        try{
            if (common.isNetworkConnected()) {
                dialog =MessageUtils.showDialog(getActivity());
                HashMap<String, String> map = new HashMap<>();
                map.put("userid", LocalSession.getUserInfo(getActivity(), KEY_ID));
                common.callApiRequest("sparesdispatched", "POST", map, 0);
            } else {

                MessageUtils.showSnackBarAction(getActivity(),msnackview,getString(R.string.check_internet));
//            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void apiCallBackOverRideMethod(Response<ResponseBody> response, JSONArray objResponse, String method, int position, String message, boolean sucs) throws JSONException, IOException {
       try{
           if(dialog.isShowing()){
               MessageUtils.dismissDialog(dialog);
               dialog.dismiss();
           }
           if (method.equals("sparesdispatched")) {
               if (objResponse != null || objResponse.length() > 0) {
                   spareDispatchPojosArrayList = new Gson().fromJson(objResponse.toString(), new TypeToken<ArrayList<SpareDispatchPojos>>() {
                   }.getType());
               /* SpareDispatchPojos spareDispatchPojos = new SpareDispatchPojos();
                spareDispatchPojos.setChassisNo("56445564564");
                spareDispatchPojos.setCheck(false);
                spareDispatchPojos.setDispatchType("Test");
                spareDispatchPojos.setDriverName("Murugan");
                spareDispatchPojos.setId(2);
                spareDispatchPojos.setQty("8");
                spareDispatchPojos.setSerialNo("987564132");
                spareDispatchPojos.setVehicleModel("Audi");
                spareDispatchPojosArrayList.add(spareDispatchPojos);
                spareDispatchPojosArrayList.addAll(spareDispatchPojosArrayList);
                spareDispatchPojosArrayList.addAll(spareDispatchPojosArrayList);
                spareDispatchPojosArrayList.addAll(spareDispatchPojosArrayList);
                spareDispatchPojosArrayList.addAll(spareDispatchPojosArrayList);*/
                   if (spareDispatchPojosArrayList.size() > 0) {
                       linspareListtitle.setVisibility(View.VISIBLE);
                       spareDispatchAdapter = new SpareDispatchAdapter(getActivity(), spareDispatchPojosArrayList, this);
                       recyclerView.setAdapter(spareDispatchAdapter);
                   } else {
                       textView.setVisibility(View.VISIBLE);
                       linearLayout.setVisibility(View.GONE);
                       linspareListtitle.setVisibility(View.GONE);
                       textView.setText(message);
                   }

               }
           }
           else {
               if (method != null && method.equals("acceptsparesdispatched")) {
                   //startActivity(new Intent(getActivity(), IndentHome.class));
                   Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
               }else{
                   textView.setVisibility(View.VISIBLE);
                   linearLayout.setVisibility(View.GONE);
                   linspareListtitle.setVisibility(View.GONE);
                   textView.setText(message);
               }
           }
       }catch (Exception ex){
           ex.printStackTrace();
       }
    }

    @Override
    public void connectposition(int position, int listSize) {
      try{
          Log.d("CaledInterfaceFunction",""+position+"\t"+listSize);
          if (listSize == 0) {
              linearLayout.setVisibility(View.GONE);
          } else {
              linearLayout.setVisibility(View.VISIBLE);
              button.setText("Accept(" + listSize + ")");
          }
      }catch (Exception ex){
          ex.printStackTrace();
      }

    }

    private void sendToserver() {
        try{
            HashMap<String, Object> objectHashMap = new HashMap<>();
            HashMap<String, Object> object = new HashMap<>();
            spareServerList.clear();
            for (int i = 0; i < spareDispatchPojosArrayList.size(); i++) {
                if (spareDispatchPojosArrayList.get(i).isCheck()) {
                    spareServerList.add(spareDispatchPojosArrayList.get(i));
                }
            }
            object.put("spare", spareServerList);
            objectHashMap.put("spares", object);
            if (common.isNetworkConnected()) {
                dialog =MessageUtils.showDialog(getActivity());
                NetworkManager utilsRetrofitSerivce = common.connectRetro("POST","acceptsparesdispatched");
                Call<ResponseBody> objCallApi = utilsRetrofitSerivce.call_post("acceptsparesdispatched","Bearer "+LocalSession.getUserInfo(getActivity(),KEY_TOKEN),objectHashMap);
                objCallApi.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        MessageUtils.dismissDialog(dialog);
                        if(response.isSuccessful()){
                            try{
                                JSONObject jsonObject   = new JSONObject(response.body().string());
                                String  message = jsonObject.getString("message");
                                Boolean success = jsonObject.getBoolean("success");
                                if(success){
                                    MessageUtils.showToastMessage(getActivity(),message);
                                    MyUtils.passFragmentBackStack(getActivity(),new IndentHome());
                                }else {
                                    MessageUtils.showSnackBar(getActivity(),snackVIew,message);

                                }
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }

                        }else{
                            MessageUtils.showSnackBar(getActivity(),snackVIew,response.message());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        MessageUtils.dismissDialog(dialog);
                        MessageUtils.showSnackBar(getActivity(),snackVIew,t.getLocalizedMessage());
                    }
                });
            } else {
                MessageUtils.showSnackBarAction(getActivity(),msnackview,getString(R.string.check_internet));
//            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
