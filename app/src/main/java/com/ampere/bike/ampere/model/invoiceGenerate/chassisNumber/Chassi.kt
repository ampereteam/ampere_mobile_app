package com.ampere.bike.ampere.model.invoiceGenerate.chassisNumber

data class Chassi(
        val id: Int,
        val indent_id: String,
        val vehicle_model: String,
        val model_varient: String,
        val color: String,
        val chassis_no: String,
        val motor_serial_no: String,
        val battery_serial_no: String,
        val charger_serial_no: String,
        val qty: String,
        val indent_invoice_date: Any,
        val manufacture_date: Any,
        val status: String,
        val acceptance: String,
        val battery_charged: String,
        val battery_charged_date: Any,
        val user_id: Int,
        val delivered_by: Int,
        val delivery_indent_id: Int,
        val created_at: String,
        val updated_at: String
)