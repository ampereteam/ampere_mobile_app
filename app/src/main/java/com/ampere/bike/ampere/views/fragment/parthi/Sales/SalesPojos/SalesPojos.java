package com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalesPojos {
      @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("invoice_id")
    @Expose
    private String invoiceId;
    @SerializedName("invoice_no")
    @Expose
    private String invoiceNo;
    @SerializedName("invoice_date")
    @Expose
    private String invoiceDate;
    @SerializedName("enquiry_id")
    @Expose
    private Integer enquiryId;
    @SerializedName("invoice_amount")
    @Expose
    private String invoiceAmount;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("model_varient")
    @Expose
    private String modelVarient;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("chassis_no")
    @Expose
    private String chassisNo;
    @SerializedName("motor_serial_no")
    @Expose
    private String motorSerialNo;
    @SerializedName("battery_serial_no")
    @Expose
    private String batterySerialNo;
    @SerializedName("charger_serial_no")
    @Expose
    private String chargerSerialNo;
    @SerializedName("indent_invoice_date")
    @Expose
    private String indentInvoiceDate;
    @SerializedName("manufacture_date")
    @Expose
    private String manufactureDate;
    @SerializedName("battery_charged")
    @Expose
    private String batteryCharged;
    @SerializedName("battery_charged_date")
    @Expose
    private Object batteryChargedDate;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_address1")
    @Expose
    private String customerAddress1;
    @SerializedName("customer_address2")
    @Expose
    private String customerAddress2;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("locality")
    @Expose
    private String locality;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("statecode")
    @Expose
    private String statecode;
    @SerializedName("id_proof")
    @Expose
    private String idProof;
    @SerializedName("address_proof")
    @Expose
    private String addressProof;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("aadhar_no")
    @Expose
    private String aadharNo;
    @SerializedName("dob")
    @Expose
    private Object dob;
    @SerializedName("occupation")
    @Expose
    private String occupation;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("no_of_freeservice")
    @Expose
    private String noOfFreeservice;
    @SerializedName("service_available")
    @Expose
    private String serviceAvailable;
    @SerializedName("delivery_at")
    @Expose
    private String deliveryAt;
    @SerializedName("sgst")
    @Expose
    private String sgst;
    @SerializedName("cgst")
    @Expose
    private String cgst;
    @SerializedName("igst")
    @Expose
    private String igst;
    @SerializedName("sgst_amount")
    @Expose
    private String sgstAmount;
    @SerializedName("cgst_amount")
    @Expose
    private String cgstAmount;
    @SerializedName("igst_amount")
    @Expose
    private String igstAmount;
    @SerializedName("subsidy_amount")
    @Expose
    private String subsidyAmount;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("invoice_by")
    @Expose
    private Integer invoiceBy;
    @SerializedName("subsidy_form")
    @Expose
    private Object subsidyForm;
    @SerializedName("invoice_form")
    @Expose
    private String invoiceForm;
    @SerializedName("aadhar_card")
    @Expose
    private String aadharCard;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Integer getEnquiryId() {
        return enquiryId;
    }

    public void setEnquiryId(Integer enquiryId) {
        this.enquiryId = enquiryId;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getModelVarient() {
        return modelVarient;
    }

    public void setModelVarient(String modelVarient) {
        this.modelVarient = modelVarient;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getMotorSerialNo() {
        return motorSerialNo;
    }

    public void setMotorSerialNo(String motorSerialNo) {
        this.motorSerialNo = motorSerialNo;
    }

    public String getBatterySerialNo() {
        return batterySerialNo;
    }

    public void setBatterySerialNo(String batterySerialNo) {
        this.batterySerialNo = batterySerialNo;
    }

    public String getChargerSerialNo() {
        return chargerSerialNo;
    }

    public void setChargerSerialNo(String chargerSerialNo) {
        this.chargerSerialNo = chargerSerialNo;
    }

    public String getIndentInvoiceDate() {
        return indentInvoiceDate;
    }

    public void setIndentInvoiceDate(String indentInvoiceDate) {
        this.indentInvoiceDate = indentInvoiceDate;
    }

    public String getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public String getBatteryCharged() {
        return batteryCharged;
    }

    public void setBatteryCharged(String batteryCharged) {
        this.batteryCharged = batteryCharged;
    }

    public Object getBatteryChargedDate() {
        return batteryChargedDate;
    }

    public void setBatteryChargedDate(Object batteryChargedDate) {
        this.batteryChargedDate = batteryChargedDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress1() {
        return customerAddress1;
    }

    public void setCustomerAddress1(String customerAddress1) {
        this.customerAddress1 = customerAddress1;
    }

    public String getCustomerAddress2() {
        return customerAddress2;
    }

    public void setCustomerAddress2(String customerAddress2) {
        this.customerAddress2 = customerAddress2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStatecode() {
        return statecode;
    }

    public void setStatecode(String statecode) {
        this.statecode = statecode;
    }

    public String getIdProof() {
        return idProof;
    }

    public void setIdProof(String idProof) {
        this.idProof = idProof;
    }

    public String getAddressProof() {
        return addressProof;
    }

    public void setAddressProof(String addressProof) {
        this.addressProof = addressProof;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAadharNo() {
        return aadharNo;
    }

    public void setAadharNo(String aadharNo) {
        this.aadharNo = aadharNo;
    }

    public Object getDob() {
        return dob;
    }

    public void setDob(Object dob) {
        this.dob = dob;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNoOfFreeservice() {
        return noOfFreeservice;
    }

    public void setNoOfFreeservice(String noOfFreeservice) {
        this.noOfFreeservice = noOfFreeservice;
    }

    public String getServiceAvailable() {
        return serviceAvailable;
    }

    public void setServiceAvailable(String serviceAvailable) {
        this.serviceAvailable = serviceAvailable;
    }

    public String getDeliveryAt() {
        return deliveryAt;
    }

    public void setDeliveryAt(String deliveryAt) {
        this.deliveryAt = deliveryAt;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getSgstAmount() {
        return sgstAmount;
    }

    public void setSgstAmount(String sgstAmount) {
        this.sgstAmount = sgstAmount;
    }

    public String getCgstAmount() {
        return cgstAmount;
    }

    public void setCgstAmount(String cgstAmount) {
        this.cgstAmount = cgstAmount;
    }

    public String getIgstAmount() {
        return igstAmount;
    }

    public void setIgstAmount(String igstAmount) {
        this.igstAmount = igstAmount;
    }

    public String getSubsidyAmount() {
        return subsidyAmount;
    }

    public void setSubsidyAmount(String subsidyAmount) {
        this.subsidyAmount = subsidyAmount;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getInvoiceBy() {
        return invoiceBy;
    }

    public void setInvoiceBy(Integer invoiceBy) {
        this.invoiceBy = invoiceBy;
    }

    public Object getSubsidyForm() {
        return subsidyForm;
    }

    public void setSubsidyForm(Object subsidyForm) {
        this.subsidyForm = subsidyForm;
    }

    public String getInvoiceForm() {
        return invoiceForm;
    }

    public void setInvoiceForm(String invoiceForm) {
        this.invoiceForm = invoiceForm;
    }

    public String getAadharCard() {
        return aadharCard;
    }

    public void setAadharCard(String aadharCard) {
        this.aadharCard = aadharCard;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
