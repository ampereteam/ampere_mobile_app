package com.ampere.bike.ampere.views.kotlin.fragments.indent

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import butterknife.ButterKnife
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.`interface`.SampleInterface
import com.ampere.bike.ampere.model.indent.history.ModelIndentDetials
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.BEARER
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.item.ItemsIndentDetails
import com.ampere.vehicles.R
import customviews.CustomTextView
import kotlinx.android.synthetic.main.frag_indent_details.view.*
import kotlinx.android.synthetic.main.item_crm_header.view.*
import kotlinx.android.synthetic.main.item_indent_details.view.*
import kotlinx.android.synthetic.main.item_indent_info.*
import kotlinx.android.synthetic.main.item_indent_info.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IndentDetails : Fragment() {

    var dialog: Dialog? = null
    var mSnackView: LinearLayout? = null
    var mNoData: CustomTextView? = null
    var mLisOfIndent: RecyclerView? = null
    var retrofit = MyUtils.getInstance()
    var response1: Response<ModelIndentDetials>? = null
    var mIndentHeader: LinearLayout? = null

    var sampl: SampleInterface.Sample? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        (activity as CustomNavigationDuo).disbleNavigationView("Indent Details")
        return inflater.inflate(R.layout.frag_indent_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this, view)
        sampl = SampleInterface.Sample()
        mSnackView = view.snack_view
        mNoData = view.no_data
        mLisOfIndent = view.list_of_indent
        var mCRMHeader: LinearLayout = view.crm_header
        mIndentHeader = view.indent_header
        var mIndentHeader1: LinearLayout = view.in_snack_view
        mCRMHeader.visibility = View.GONE
        mIndentHeader1.visibility = View.GONE
        var mHdrName: CustomTextView = view.crm_header_name
        var mHdrDate: CustomTextView = view.crm_header_date
        var mHdrTask: CustomTextView = view.crm_header_task
        var mHdrStatus: CustomTextView = view.crm_header_status
        var mHdrAction: CustomTextView = view.crm_header_action

        if (LocalSession.getUserInfo(activity, LocalSession.KEY_USER_TYPE).equals(LocalSession.T_CRM)) {
            indent_name.visibility = View.VISIBLE
        } else {
            indent_name.visibility = View.GONE
        }

        mHdrName.visibility = View.VISIBLE
        mHdrAction.visibility = View.GONE

        mLisOfIndent?.layoutManager = GridLayoutManager(activity, 1) as RecyclerView.LayoutManager?

        loadData(response1)

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        dialog = MessageUtils.showDialog(activity)
        var map = HashMap<String, String>()
        map.put("user_name", LocalSession.getUserInfo(activity, LocalSession.KEY_USER_NAME))
        var modelIndentDetails: Call<ModelIndentDetials> = retrofit.onIndentDetails(BEARER + LocalSession.getUserInfo(activity, KEY_TOKEN), map);
        dialog?.setOnKeyListener { dialogInterface, i, keyEvent ->
            dialogInterface.dismiss()
            modelIndentDetails.cancel()
            false
        }

        modelIndentDetails.enqueue(object : Callback<ModelIndentDetials> {
            override fun onResponse(call: Call<ModelIndentDetials>?, response: Response<ModelIndentDetials>?) {
                dialog?.dismiss()
                response1 = response
                Log.d("response", "" + response?.body())
                loadData(response1);
            }

            override fun onFailure(call: Call<ModelIndentDetials>?, t: Throwable?) {
                dialog?.dismiss()
                mNoData?.visibility = View.VISIBLE
                mIndentHeader?.visibility = View.GONE
                MessageUtils.setFailureMessage(activity, mSnackView, t?.message)
            }
        })
    }

    private fun loadData(response: Response<ModelIndentDetials>?) {
        Log.d("indetn_status", "Rsponse " + response?.message());
        if (response != null) {
            if (response?.isSuccessful!!) {
                var modelIndentDetails = response.body() as ModelIndentDetials

                if (modelIndentDetails != null) {
                    if (modelIndentDetails.status) {
                        //Log.d("status_view_point", "indent" + modelIndentDetails.indents.size + " indent_details " + modelIndentDetails.indentdetails.size)
                        if (modelIndentDetails.indents.size == 0) {
                            mNoData?.visibility = View.VISIBLE
                            mIndentHeader?.visibility = View.GONE
                            //MessageUtils.showSnackBar(activity, mSnackView, modelIndentDetails.message)
                            mNoData?.text = modelIndentDetails.message
                        } else {
                            mLisOfIndent?.visibility = View.VISIBLE
                            mNoData?.visibility = View.GONE
                            mIndentHeader?.visibility = View.VISIBLE
                            mLisOfIndent?.adapter = ItemsIndentDetails(activity, modelIndentDetails.indents, modelIndentDetails.indentdetails, sampl!!)
                        }
                    } else {
                        mNoData?.visibility = View.VISIBLE
                        mIndentHeader?.visibility = View.GONE
                        MessageUtils.showSnackBar(activity, mSnackView, modelIndentDetails.message)
                        mNoData?.text = modelIndentDetails.message
                    }
                } else {
                    mNoData?.visibility = View.VISIBLE
                    mIndentHeader?.visibility = View.GONE
                    mNoData?.text = activity?.getString(R.string.api_response_not_found)
                    MessageUtils.showSnackBar(activity, mSnackView, activity?.getString(R.string.api_response_not_found))
                }

            } else {
                mNoData?.visibility = View.VISIBLE
                mIndentHeader?.visibility = View.GONE
                mNoData?.text = MessageUtils.showErrorCodeToast(response.code())
                MessageUtils.setErrorMessage(activity, mSnackView, response.code())
            }
        }
    }
}