package com.ampere.bike.ampere.views.fragment.enquiry.adapter


import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.fragment.enquiry.sparesearchpojo.DetailsItem
import com.ampere.vehicles.R
import customviews.CustomTextView

class  SpareEnqSearchAdapter (val context : Context, val spareenqsearchList:ArrayList<DetailsItem?>?, val onclickEvnt: ConfigClickEvent)
    : RecyclerView.Adapter<SpareEnqSearchAdapter.ViewHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SpareEnqSearchAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.adapter_spare_enq_search,p0,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int { return  spareenqsearchList?.size!! }

    override fun onBindViewHolder(p0: SpareEnqSearchAdapter.ViewHolder, p1: Int) {
        try {

            Log.d("AdpterData",""+ spareenqsearchList!![p1]?.serialNo)
            Log.d("AdpterData",""+ spareenqsearchList!![p1]?.vehicleModel)
            Log.d("AdpterData",""+ spareenqsearchList!![p1]?.color)
            p0.serilnumber.text = spareenqsearchList!![p1]?.serialNo
            p0.vehclemodel.text = spareenqsearchList[p1]?.vehicleModel
            p0.color.text = spareenqsearchList[p1]?.color
            p0.linear.setOnClickListener {
                onclickEvnt.connectposition(p1,100)
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var  vehclemodel =itemView.findViewById<CustomTextView>(R.id.search_vehi_adap)
        var  serilnumber =itemView.findViewById<CustomTextView>(R.id.search_chass_adap)
        var  color =itemView.findViewById<CustomTextView>(R.id.search_color_adap)
        var  linear =itemView.findViewById<LinearLayout>(R.id.menus)
    }
}