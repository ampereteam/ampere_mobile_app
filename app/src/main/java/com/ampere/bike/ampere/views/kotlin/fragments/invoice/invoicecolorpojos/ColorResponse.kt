package com.ampere.bike.ampere.views.kotlin.fragments.invoice.invoicecolorpojos

data class ColorResponse(
	val data: Data? = null,
	val success: Boolean? = null,
	val message: String? = null
)
