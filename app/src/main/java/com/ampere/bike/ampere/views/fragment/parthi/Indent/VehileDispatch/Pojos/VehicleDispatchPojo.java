package com.ampere.bike.ampere.views.fragment.parthi.Indent.VehileDispatch.Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleDispatchPojo {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("indent_id")
    @Expose
    private String indentId;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("model_varient")
    @Expose
    private String modelVarient;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("chassis_no")
    @Expose
    private String chassisNo;
    @SerializedName("motor_serial_no")
    @Expose
    private String motorSerialNo;
    @SerializedName("battery_serial_no")
    @Expose
    private String batterySerialNo;
    @SerializedName("charger_serial_no")
    @Expose
    private String chargerSerialNo;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("indent_invoice_date")
    @Expose
    private String indentInvoiceDate;
    @SerializedName("manufacture_date")
    @Expose
    private String manufactureDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("acceptance")
    @Expose
    private String acceptance;
    @SerializedName("battery_charged")
    @Expose
    private String batteryCharged;
    @SerializedName("battery_charged_date")
    @Expose
    private Object batteryChargedDate;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("delivered_by")
    @Expose
    private Integer deliveredBy;
    @SerializedName("delivery_indent_id")
    @Expose
    private Integer deliveryIndentId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("dispatch_type")
    @Expose
    private String dispatchType;
    @SerializedName("dispatch_date")
    @Expose
    private String dispatchDate;
    @SerializedName("docket")
    @Expose
    private String docket;
    @SerializedName("vide")
    @Expose
    private String vide;
    @SerializedName("driver_no")
    @Expose
    private Object driverNo;
    @SerializedName("driver_name")
    @Expose
    private Object driverName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIndentId() {
        return indentId;
    }

    public void setIndentId(String indentId) {
        this.indentId = indentId;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getModelVarient() {
        return modelVarient;
    }

    public void setModelVarient(String modelVarient) {
        this.modelVarient = modelVarient;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getMotorSerialNo() {
        return motorSerialNo;
    }

    public void setMotorSerialNo(String motorSerialNo) {
        this.motorSerialNo = motorSerialNo;
    }

    public String getBatterySerialNo() {
        return batterySerialNo;
    }

    public void setBatterySerialNo(String batterySerialNo) {
        this.batterySerialNo = batterySerialNo;
    }

    public String getChargerSerialNo() {
        return chargerSerialNo;
    }

    public void setChargerSerialNo(String chargerSerialNo) {
        this.chargerSerialNo = chargerSerialNo;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getIndentInvoiceDate() {
        return indentInvoiceDate;
    }

    public void setIndentInvoiceDate(String indentInvoiceDate) {
        this.indentInvoiceDate = indentInvoiceDate;
    }

    public String getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAcceptance() {
        return acceptance;
    }

    public void setAcceptance(String acceptance) {
        this.acceptance = acceptance;
    }

    public String getBatteryCharged() {
        return batteryCharged;
    }

    public void setBatteryCharged(String batteryCharged) {
        this.batteryCharged = batteryCharged;
    }

    public Object getBatteryChargedDate() {
        return batteryChargedDate;
    }

    public void setBatteryChargedDate(Object batteryChargedDate) {
        this.batteryChargedDate = batteryChargedDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDeliveredBy() {
        return deliveredBy;
    }

    public void setDeliveredBy(Integer deliveredBy) {
        this.deliveredBy = deliveredBy;
    }

    public Integer getDeliveryIndentId() {
        return deliveryIndentId;
    }

    public void setDeliveryIndentId(Integer deliveryIndentId) {
        this.deliveryIndentId = deliveryIndentId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDispatchType() {
        return dispatchType;
    }

    public void setDispatchType(String dispatchType) {
        this.dispatchType = dispatchType;
    }

    public String getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(String dispatchDate) {
        this.dispatchDate = dispatchDate;
    }

    public String getDocket() {
        return docket;
    }

    public void setDocket(String docket) {
        this.docket = docket;
    }

    public String getVide() {
        return vide;
    }

    public void setVide(String vide) {
        this.vide = vide;
    }

    public Object getDriverNo() {
        return driverNo;
    }

    public void setDriverNo(Object driverNo) {
        this.driverNo = driverNo;
    }

    public Object getDriverName() {
        return driverName;
    }

    public void setDriverName(Object driverName) {
        this.driverName = driverName;
    }
        boolean check;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
