package com.ampere.bike.ampere.views.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.vehicles.R;
import com.desai.vatsal.mydynamiccalendar.EventModel;
import com.desai.vatsal.mydynamiccalendar.GetEventListListener;
import com.desai.vatsal.mydynamiccalendar.MyDynamicCalendar;

import java.util.ArrayList;


public class AmpereCalendar extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) getActivity()).enableNavigationView(getString(R.string.calendar), 0);
        return inflater.inflate(R.layout.frag_calendar, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MyDynamicCalendar myCalendar = view.findViewById(R.id.myCalendar);//new MyDynamicCalendar(getActivity());


        //myCalendar.setCalendarBackgroundColor("#eeeeee"); //or  myCalendar.setCalendarBackgroundColor(R.serviceType.gray);
        myCalendar.setHeaderBackgroundColor("#454265");
        myCalendar.setHeaderTextColor("#ffffff");
        myCalendar.setNextPreviousIndicatorColor("#245675");
        myCalendar.setWeekDayLayoutBackgroundColor("#965471");
        myCalendar.setWeekDayLayoutTextColor("#246245");
        myCalendar.setExtraDatesOfMonthBackgroundColor("#324568");
        myCalendar.setExtraDatesOfMonthTextColor("#756325");
        myCalendar.setDatesOfMonthBackgroundColor("#145687");
        myCalendar.setDatesOfMonthTextColor("#745632");
        myCalendar.setCurrentDateBackgroundColor(R.color.black);
        myCalendar.setCurrentDateTextColor("#00e600");
        myCalendar.setBelowMonthEventTextColor("#425684");
        myCalendar.setBelowMonthEventDividerColor("#635478");


        // set all saturday off(Holiday) - default value is false
        // isSaturdayOff(true/false, date_background_color, date_text_color);
        myCalendar.isSaturdayOff(true, "#ffffff", "#ff0000");

        // set all sunday off(Holiday) - default value is false
        // isSundayOff(true/false, date_background_color, date_text_color);
        myCalendar.isSundayOff(true, "#ffffff", "#ff0000");


        myCalendar.setEventCellBackgroundColor("#852365");
        myCalendar.setEventCellTextColor("#425684");

        // Add event  -  addEvent(event_date, event_start_time, event_end_time, event_title)
        myCalendar.addEvent("5-10-2016", "8:00", "8:15", "Today Event 1");
        myCalendar.addEvent("05-10-2016", "8:15", "8:30", "Today Event 2");
        myCalendar.addEvent("05-10-2016", "8:30", "8:45", "Today Event 3");

        // Get list of event with detail
        myCalendar.getEventList(new GetEventListListener() {
            @Override
            public void eventList(ArrayList<EventModel> eventList) {

            }
        });

        // updateEvent(position, event_date, event_start_time, event_end_time, event_title)
        myCalendar.updateEvent(0, "5-10-2016", "8:00", "8:15", "Today Event 111111");

        // Delete single event
        myCalendar.deleteEvent(2);

        // Delete all events
        myCalendar.deleteAllEvent();
    }
}
