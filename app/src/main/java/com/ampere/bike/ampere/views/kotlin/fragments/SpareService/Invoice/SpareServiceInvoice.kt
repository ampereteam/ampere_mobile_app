package com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Invoice
import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.os.StrictMode
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.CustomNavigationDuo.snackVIew
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_ID
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Invoice.Adapter.InvoiceListAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Invoice.Pojos.AcceptItem
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Invoice.Pojos.InvoiceListRES

import com.ampere.vehicles.R
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SpareServiceInvoice : Fragment() ,ConfigClickEvent{


    var recyinvoiceList:RecyclerView ?= null
    var acceptlinear:LinearLayout ?= null
    var btnAcept:Button ?= null
    var emptyList:TextView ?= null
    var checkbox:CheckBox ?= null
    var callApi:UtilsCallApi?=null
    var dialog:Dialog?=null
    var invocielistRes: InvoiceListRES?=null
    var  invoiceListAdapter :InvoiceListAdapter?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =inflater.inflate(R.layout.fragment_spare_service_invoice, container, false)
        var newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        return view
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callApi = UtilsCallApi(activity)
        dialog =Dialog(activity)
        recyinvoiceList = view.findViewById(R.id.recycle_spsin_complaint_list)
        emptyList = view.findViewById(R.id.spsin_emptyList)
        checkbox = view.findViewById(R.id.checkbox_spsin)
        acceptlinear = view.findViewById(R.id.spsin_accept)
        btnAcept = view.findViewById(R.id.spsin_acpt)
        recyinvoiceList?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
        getDataFrmSrvr()
        /**checkbox ischeckedListener **/
        checkbox?.setOnCheckedChangeListener { buttonView, isChecked ->
            try{
                if(isChecked){
                    if(invocielistRes?.data?.spareinventory?.size!!>0){
                        for(i :Int in 0..invocielistRes?.data?.spareinventory?.size!!-1){
                            invocielistRes?.data?.spareinventory?.get(i)?.check =true
                        }
                        acceptlinear?.visibility =View.VISIBLE
                        btnAcept?.text="Accept("+invocielistRes?.data?.spareinventory?.size!!+")"
                    }
                }else{
                    if(invocielistRes?.data?.spareinventory?.size!!>0) {
                        for (i: Int in 0..invocielistRes?.data?.spareinventory?.size!! - 1) {
                            invocielistRes?.data?.spareinventory?.get(i)?.check = false
                        }
                        acceptlinear?.visibility =View.GONE
                    }
                }
                invoiceListAdapter?.notifyDataSetChanged()
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }
        btnAcept?.setOnClickListener {
            try{
                val acceptList:ArrayList<AcceptItem> = ArrayList()
                for(i:Int in 0 .. invocielistRes?.data?.spareinventory?.size!!-1){
                    val acceptItem =AcceptItem(invocielistRes?.data?.spareinventory?.get(i)?.id.toString())
                    acceptList.add(acceptItem)
                }
                if(acceptList.size<=0){
                    Log.d("",""+acceptList.size)
                }else{
                    senTOsrvr(acceptList)
                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }
    }

    private fun senTOsrvr(acceptList: ArrayList<AcceptItem>) {
        try{
            if(callApi?.isNetworkConnected!!){
                dialog=MessageUtils.showDialog(activity)
                val hashmap :HashMap<String,Any> = HashMap()
                val map :HashMap<String,Any> = HashMap()
                hashmap.put("invoice",acceptList)
                map.put("invoices",hashmap)
                map["userid"]=LocalSession.getUserInfo(activity, KEY_ID)
                val calinterface  =callApi?.connectRetro("POST","acceptsparesinvoice")
                val callback =calinterface?.call_post("acceptsparesinvoice","Bearer "+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
                callback?.enqueue(object :Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, snackVIew,t.message)
                    }
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                val jsonObject = JSONObject(response.body()?.string())
                                val success =jsonObject.getBoolean("success")!!
                                val message = jsonObject.getString("message")!!
                                if(success){
//                                    activity?.finish()
                                    acceptlinear?.visibility =View.GONE
                                    getDataFrmSrvr()
                                }else{
                                    MessageUtils.showSnackBar(activity, snackVIew,message)
                                }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }

                        }else{
                            MessageUtils.showSnackBar(activity, snackVIew,response?.message())
                        }
                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity!!, snackVIew,getString(R.string.check_internet))
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun getDataFrmSrvr() {
        try{
            if(callApi?.isNetworkConnected!!){
                dialog=MessageUtils.showDialog(activity)
                val map:HashMap<String,Any> = HashMap()
                map.put("userid",LocalSession.getUserInfo(activity, KEY_ID))
                var calinterface = callApi?.connectRetro("POST","sparescomplaintinvoice")
                var callback = calinterface?.call_post("sparescomplaintinvoice","Bearer "+LocalSession.getUserInfo(activity,KEY_TOKEN),map)
                callback?.enqueue(object : Callback<ResponseBody>{
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, snackVIew,t.message)
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                invocielistRes=Gson().fromJson(response.body()?.string(),InvoiceListRES::class.java)
                                    if(invocielistRes?.data?.spareinventory?.size!!<=0 ){
                                        Log.d("ArrayListSIze",""+isStateSaved)
                                        emptyList?.text = invocielistRes?.message
                                        emptyList?.visibility =View.VISIBLE
                                        recyinvoiceList?.visibility =View.GONE
                                    }else{
                                        recyinvoiceList?.visibility =View.VISIBLE
                                        emptyList?.visibility =View.GONE
                                        setToAdpter()
                                    }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }


                        }else{
                            MessageUtils.showSnackBar(activity, snackVIew,response.message())
                        }

                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity, snackVIew,getString(R.string.check_internet))
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setToAdpter() {
        try{
            invoiceListAdapter= InvoiceListAdapter(activity!!,invocielistRes?.data?.spareinventory,this)
            recyinvoiceList?.adapter =invoiceListAdapter
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }
    override fun connectposition(position: Int, listSize: Int) {
        try{
            Log.d("Called interface Function",""+position+listSize)
            if(listSize <=0){
                checkbox?.isChecked =false
                acceptlinear?.visibility =View.GONE
                btnAcept?.setText("Accept(0)")
            }else{
                acceptlinear?.visibility =View.VISIBLE
                btnAcept?.setText("Accept("+listSize+")")
            }
            if(invocielistRes?.data?.spareinventory?.get(position)?.check!!){
                Log.d("SpareName",""+invocielistRes?.data?.spareinventory?.get(position)?.spareName)
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
}
