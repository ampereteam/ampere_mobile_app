package com.ampere.bike.ampere.views.fragment.parthi.Indent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentListFragment;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentTapAdapter.IndentTapAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.SpareDispatch.SpareDispatch;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.VehileDispatch.VehileDispatch;
import com.ampere.vehicles.R;

import java.util.Objects;


public class IndentHome extends Fragment {
    public IndentTapAdapter adapter;
    public TabLayout tabLayout;
    public ViewPager viewPager;
   public static  View msnackview;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((CustomNavigationDuo) Objects.requireNonNull(getActivity())).disbleNavigationView(getString(R.string.indent_details));
        View view =inflater.inflate(R.layout.activity_indent_home, container, false);
        msnackview  = view.findViewById(R.id.parent_view);
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tabLayout);
        adapter = new IndentTapAdapter(getChildFragmentManager());
        adapter.addFragment(new IndentListFragment(), "IndentList");
        adapter.addFragment(new VehileDispatch(), "Vehicle  Dispatched");
        adapter.addFragment(new SpareDispatch(), "Spare Dispatched");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    /*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indent_home);
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        adapter = new IndentTapAdapter(getSupportFragmentManager());
        adapter.addFragment(new IndentListFragment(), "IndentList");
        adapter.addFragment(new VehileDispatch(), "Vehicle  Dispatched");
        adapter.addFragment(new SpareDispatch(), "Spare Dispatched");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.v("Selected Tab", "" + tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.v("unSelected Tab", "" + tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.v("RESelected Tab", "" + tab);
            }
        });

        FloatingActionButton floatingActionButton = findViewById(R.id.add_indent);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //  getSupportFragmentManager().beginTransaction().replace(R.id.call_frgment, new Indent()).commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }*/

}
