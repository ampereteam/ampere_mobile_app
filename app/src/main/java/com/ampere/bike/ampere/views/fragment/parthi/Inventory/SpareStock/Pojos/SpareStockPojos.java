package com.ampere.bike.ampere.views.fragment.parthi.Inventory.SpareStock.Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpareStockPojos {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("model_varient")
    @Expose
    private String modelVarient;
    @SerializedName("spare_name")
    @Expose
    private String spareName;
    @SerializedName("serial_no")
    @Expose
    private String serialNo;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("indent_invoice_date")
    @Expose
    private String indentInvoiceDate;
    @SerializedName("manufacture_date")
    @Expose
    private String manufactureDate;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getModelVarient() {
        return modelVarient;
    }

    public void setModelVarient(String modelVarient) {
        this.modelVarient = modelVarient;
    }

    public String getSpareName() {
        return spareName;
    }

    public void setSpareName(String spareName) {
        this.spareName = spareName;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getIndentInvoiceDate() {
        return indentInvoiceDate;
    }

    public void setIndentInvoiceDate(String indentInvoiceDate) {
        this.indentInvoiceDate = indentInvoiceDate;
    }

    public String getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(String manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
