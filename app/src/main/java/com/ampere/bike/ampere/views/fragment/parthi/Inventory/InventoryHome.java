package com.ampere.bike.ampere.views.fragment.parthi.Inventory;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.parthi.Inventory.SpareStock.SpareStock;
import com.ampere.bike.ampere.views.fragment.parthi.Inventory.VehicleStock.VehicleStock;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class InventoryHome extends Fragment {
    private InventoryTapAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static  View  msnakview;
        ArrayList<String>  fragmnetList ;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * inside of fragment  using tap layout and  viewpager  you can use this
         * lines
         * setRetainInstance(true);
         */
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((CustomNavigationDuo) getActivity()).disbleNavigationView(getString(R.string.inventory));
        View view = inflater.inflate(R.layout.activity_inventory_home, container, false);
        msnakview =view.findViewById(R.id.parent_view);
        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tabLayout);
        fragmnetList = new ArrayList<>();
        fragmnetList.add("Vehicle Stock");
        fragmnetList.add("Spare Stock");
        setupViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.v("Selected Tab", "" + tab.getText());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.v("unSelected Tab", "" + tab.getText());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.v("RESelected Tab", "" + tab.getText());
            }
        });
        /*view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(getActivity(), CustomNavigationDuo.class);
                    i.putExtra("SELECTEDVALUE", 0);
//                    // i.putExtra("Comingback", cameback);
                    startActivity(i);
                    Objects.requireNonNull(getActivity()).finish();

                    return true;
                } else {
                    return false;
                }
            }
        });*/
        return view;
    }

    /// Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        /**
         * get tab adapter working with activity
         */
//        adapter = new InventoryTapAdapter(Objects.requireNonNull(getActivity()).getSupportFragmentManager());
        /**
         * get Inside of Fragment  again using fragment
         */
        adapter = new InventoryTapAdapter(getChildFragmentManager());
        adapter.addFragment(new VehicleStock(), fragmnetList.get(0));
        adapter.addFragment(new SpareStock(), fragmnetList.get(1));
        initTabs();
    }
    @SuppressLint("ResourceType")
    private void initTabs() {
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
      /*View view = getLayoutInflater().inflate(R.layout.tab_layout_custom_font,null,false);
        TextView  textView  = view.findViewById(R.id.tab_text);
        textView.setTypeface(MyUtils.getTypeface(getActivity(), "fonts/Lato-Bold.ttf"));
        textView.setTextColor(ContextCompat.getColorStateList(getActivity(), R.drawable.tab_selector));
        textView.setGravity(Gravity.CENTER);
        textView.setText("hello");*/
//        tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).setCustomView(textView);
        //tabLayout it doesn't respond to style attribute 'tabTextAppearance' so we have to use customview
        for (int i = 0; i < adapter.getCount(); i++) {
            TextView textView  = new TextView(getActivity());
            textView.setTypeface(MyUtils.getTypeface(getActivity(), "fonts/Lato-Bold.ttf"));
            textView.setTextColor(ContextCompat.getColorStateList(getActivity(), R.drawable.tab_selector));
            textView.setGravity(Gravity.CENTER);
            textView.setText(fragmnetList.get(i));
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(textView);
                tab.setText(adapter.getPageTitle(i));
            }
        }
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
    }
}
