package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyInvoice;


import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.api.Retrofit.NetworkManager;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyHome;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyInvoice.Adapter.WarrantyInvoiceAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyInvoice.Pojos.Data;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyInvoice.Pojos.WarrantyInvoice;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyInvoice.Pojos.WarrantyinvoiceItem;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;
import static com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyHome.snackView;

//import smartdevelop.ir.eram.showcaseviewlib.GuideView;

/**
 * A simple {@link Fragment} subclass.
 */
public class WarrantyInvoiceFragment extends Fragment implements ConfigClickEvent {
    RecyclerView warrantyInvoiceList;
    LinearLayout linearLayout;
    LinearLayout listLineartitle;
    WarrantyInvoiceAdapter warrantyInvoiceAdapter;
    ArrayList<WarrantyinvoiceItem> invoicePojosArrayList = new ArrayList<>();
    ArrayList<WarrantyinvoiceItem> invoiceSendList = new ArrayList<>();
    CheckBox checkBox;
    TextView textView;
    JSONArray jsonArray;
    Button button;
    Common common;
    ConfigClickEvent configposition;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("Activiy", "Invoice");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_warranty_invoice, container, false);
        try{
        warrantyInvoiceList = view.findViewById(R.id.warranty_invoice_list);
        textView = view.findViewById(R.id.emptyList);
        linearLayout = view.findViewById(R.id.accept);
        listLineartitle = view.findViewById(R.id.main);
        //checkPermission();
        warrantyInvoiceList.setLayoutManager(new LinearLayoutManager(getActivity()));
        common = new Common(getActivity());
        configposition = this;
        //createandDisplayPdf(text);

      //  ViewTarget target = new ViewTarget(warrantyInvoiceList.findViewById(R.id.warranty_invoice_list));
//        ViewTarget target = new ViewTarget(textView.findViewById(R.id.emptyList));
        // ShowIntro("TEstind","TEsting",R.id.s_no,1);
       // SHowcase(target);
        checkBox = view.findViewById(R.id.markALL);
        button = view.findViewById(R.id.acpt);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    sendObjectTOserver();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        getResopnse();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (invoicePojosArrayList.size() > 0) {
                        for (int i = 0; invoicePojosArrayList.size() > i; i++) {
                            invoicePojosArrayList.get(i).setCheck(true);
                        }
                        warrantyInvoiceAdapter.setListItem(invoicePojosArrayList);
                        warrantyInvoiceAdapter.notifyDataSetChanged();
                        linearLayout.setVisibility(View.VISIBLE);
                        button.setText("Accept(" + invoicePojosArrayList.size() + ")");

                    } else {
                        linearLayout.setVisibility(View.GONE);
                        button.setText("Accept(" + invoicePojosArrayList.size() + ")");
                    }

                } else {
                    if (invoicePojosArrayList.size() > 0) {
                        for (int i = 0; i < invoicePojosArrayList.size(); i++) {
                            invoicePojosArrayList.get(i).setCheck(false);
                        }
                        warrantyInvoiceAdapter.setListItem(invoicePojosArrayList);
                        warrantyInvoiceAdapter.notifyDataSetChanged();
                        linearLayout.setVisibility(View.GONE);
                    }
                    linearLayout.setVisibility(View.GONE);
                }
            }
        });
        /*view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(getActivity(), CustomNavigationDuo.class);
                    i.putExtra("SELECTEDVALUE", 0);
//                    // i.putExtra("Comingback", cameback);
                    startActivity(i);

                    getActivity().finish();

                    return true;
                } else {
                    return false;
                }
            }
        });*/

        }catch (Exception e){
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void connectposition(int position, int listSize) {
        try{
            if (listSize > 0) {
                linearLayout.setVisibility(View.VISIBLE);
                button.setText("Accept(" + listSize + ")");
            } else {
                linearLayout.setVisibility(View.GONE);
            }
        }catch (Exception e){
             e.printStackTrace();
        }


    }

    public void sendObjectTOserver() throws JSONException {
        try{


        jsonArray = new JSONArray();

        HashMap<String, Object> object1 = new HashMap<>();
        HashMap<String, Object> obj = new HashMap<>();
        invoiceSendList.clear();
        for (int h = 0; h < invoicePojosArrayList.size(); h++) {
            if (invoicePojosArrayList.get(h).isCheck()) {
                invoiceSendList.add(invoicePojosArrayList.get(h));
            }
        }

        obj.put("invoice", invoiceSendList);

        object1.put("invoices", obj);

        responseApiservice("POST", "acceptwarrantyinvoice", object1);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private NetworkManager connectRetro(String annonationType, String method) {
        String appUrl = null;
        if (annonationType.equals("POST")) {
            appUrl = getActivity().getResources().getString(R.string.appURL);
        }
        final OkHttpClient objOkHttpClient = new OkHttpClient.Builder().readTimeout(1, TimeUnit.MINUTES).connectTimeout(1, TimeUnit.MINUTES).addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
        Retrofit objRetrofit = new Retrofit.Builder()
                .client(objOkHttpClient).baseUrl(appUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return objRetrofit.create(NetworkManager.class);
    }

    public void responseApiservice(String callTYpe, String method, HashMap<String, Object> jsonObject) {
        if(common.isNetworkConnected()){
            common.showLoad(true);
        Call<ResponseBody> responseBodyCall = null;
        HashMap<String, Object> map = new HashMap<>();
        map.put("invoices", jsonObject);
        responseBodyCall = connectRetro(callTYpe, method).call_post(method, "Bearer " + LocalSession.getUserInfo(getActivity(),KEY_TOKEN), jsonObject);
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                common.hideLoad();
                if (response.isSuccessful()) {
                    try {
                        JSONObject object = new JSONObject(response.body().string());
                        boolean sucess = object.getBoolean("success");
                        String s = object.getString("message");
                        if (sucess) {
                            Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
                            //startActivity(new Intent(getActivity(), WarrantyHome.class));

                            MyUtils.passFragmentBackStack(getActivity(), new WarrantyHome());
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    Log.v("info REsponse", "" + response.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                common.hideLoad();
                Log.v("info REsponse", "" + t.toString());
            }
        });
        }else{
            MessageUtils.showSnackBarAction(Objects.requireNonNull(getActivity()),snackView,getString(R.string.check_internet));
        }
    }

    public void getResopnse() {
        HashMap<String, String> map = new HashMap<>();

        if(common.isNetworkConnected()){

        map.put("userid", LocalSession.getUserInfo(getActivity(),KEY_ID));
        Call<ResponseBody> responseCall = null;
        responseCall = connectRetro("POST", "warrantyinvoice").call_post("warrantyinvoice", "Bearer " + LocalSession.getUserInfo(getActivity(),KEY_TOKEN),map);
        responseCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    common.hideLoad();
                    System.out.println("WarrantyInvoice List" + response.toString());
                    try{
                        JSONObject objJsonObject = new JSONObject(response.body().string());
                        Log.d("",""+response.body().toString());
                        JSONObject data = objJsonObject.getJSONObject("data");
                        JSONArray jsonArray = data.getJSONArray("warrantyinvoice");
                        WarrantyInvoice warrantyInvoice = new Gson().fromJson(String.valueOf(objJsonObject), WarrantyInvoice.class);
                        Gson gson = new Gson();
                        Data response2 = gson.fromJson(String.valueOf(data), Data.class);
                        if (jsonArray != null) {
                            listLineartitle.setVisibility(View.VISIBLE);
                            invoicePojosArrayList = response2.getWarrantyinvoice();
                            Log.d("ArraySize",""+invoicePojosArrayList.size());

                            if (invoicePojosArrayList.size() > 0) {
                                textView.setVisibility(View.GONE);
                                warrantyInvoiceAdapter = new WarrantyInvoiceAdapter(getActivity(), invoicePojosArrayList, configposition);
                                warrantyInvoiceList.setAdapter(warrantyInvoiceAdapter);
                                common.hideLoad();
                            } else {

                                common.hideLoad();
                                textView.setVisibility(View.VISIBLE);
                                textView.setText(warrantyInvoice.getMessage());

                            }
                        } else {
                            common.hideLoad();
                            textView.setVisibility(View.VISIBLE);
                            textView.setText(warrantyInvoice.getMessage());
                        }
                    }catch (NullPointerException e){
                        e.printStackTrace();
                        listLineartitle.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "SERVER NOT FOUND", Toast.LENGTH_SHORT).show();
                    }

                } catch (IOException e) {
                    common.hideLoad();
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                common.hideLoad();
                try {
                    Toast.makeText(getActivity(), "" + t.toString(), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        }else{
//            common.hideLoad();
            MessageUtils.showSnackBarAction(Objects.requireNonNull(getActivity()),snackView,getString(R.string.check_internet));
        }
    }
   /* private void ShowIntro(String title, String text, int viewId, final int type) {

        new GuideView.Builder(getActivity())
                .setTitle(title)
                .setContentText(text)
                .setTargetView(textView.findViewById(R.id.emptyList))
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setGravity(GuideView.Gravity.center)
                .setDismissType(GuideView.DismissType.targetView) //optional - default dismissible by TargetView
                *//*.setGuideListener(new GuideView.GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        if (type == 1) {
                            ShowIntro("Editor", "Edit any photo from selected photos than Apply on your video", R.id.accept, 6);
                        } else if (type == 6) {
                            ShowIntro("Duration", "Set duration between photos", R.id.acpt, 2);
                        } else if (type == 2) {
                            ShowIntro("Filter", "Add filter to video ", R.id.chas_no, 4);
                        } else if (type == 4) {
                            ShowIntro("Add Song", "Add your selected song on your video ", R.id.s_no, 3);
                        } else if (type == 3) {
                            ShowIntro("Overlay", "Add your selected overlay effect on your video ", R.id.checKBox, 5);
                        } else if (type == 5) {
                            sessionManager.putBoolean("showcase", false);
                        }
                    }
                })*//*
                .build()
                .show();

    }*/
       /* public  void SHowcase(Target target){
            new ShowcaseView.Builder(getActivity())
                    .setTarget(target)
                    .setContentTitle("ShowcaseView")
                    .setContentText("This is highlighting the Home button")
                    .hideOnTouchOutside()
                    .build();

        }*/

    public void createandDisplayPdf(String text) {

        Document doc = new Document();

        try {
            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Dir/call";

            File dir = new File(path);
            dir.mkdirs();
            System.out.println("Storage  Path"+dir.toString());

            if(!dir.exists());


            File file = new File(dir, "newFile.pdf");
            FileOutputStream fOut = new FileOutputStream(file);
            System.out.println("Storage  Path"+file.toString());
            PdfWriter.getInstance(doc, fOut);

            //open the document
            doc.open();

            Paragraph p1 = new Paragraph(text);
            Font paraFont= new Font(Font.FontFamily.COURIER);
            p1.setAlignment(Paragraph.ALIGN_CENTER);
            p1.setFont(paraFont);

            //add paragraph to document
            doc.add(p1);

        } catch (DocumentException de) {
            Log.e("PDFCreator", "DocumentException:" + de);
        } catch (IOException e) {
            Log.e("PDFCreator", "ioException:" + e);
        }
        finally {
            doc.close();
        }

        //viewPdf("newFile.pdf", "Dir");
    }

    // Method for opening a pdf file
    private void viewPdf(String file, String directory) {

        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/" + directory + "/" + file);
        Uri path = Uri.fromFile(pdfFile);

        // Setting the intent for pdf reader
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(), "Can't read pdf file", Toast.LENGTH_SHORT).show();
        }
    }
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                //             ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAPTURE_AUDIO_OUTPUT)
                !=
                PackageManager.PERMISSION_GRANTED
                ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                //                   || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.CAPTURE_AUDIO_OUTPUT)
                    ) {

                ActivityCompat.requestPermissions(getActivity(), new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                        //                       Manifest.permission.CAPTURE_AUDIO_OUTPUT
                }, 1);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
//                        Manifest.permission.CAPTURE_AUDIO_OUTPUT
                }, 1);
            }
        } else {
            Toast.makeText(getActivity(), "Already Given Permission", Toast.LENGTH_SHORT).show();
           /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                    getActivity().startForegroundService(new Intent(getActivity(),CallService.class));

            } else {

                    getActivity().startService(new Intent(getActivity(),CallService.class));
                }*/
            createandDisplayPdf("Section 8 also authorizes a variety of \"project-based\" rental assistance programs, under which the owner reserves some or all of the units in a building for low-income tenants, in return for a federal government guarantee to make up the difference between the tenant's contribution and the rent in the owner's contract with the government. A tenant who leaves a subsidized project will lose access to the project-based subsidy.\n" +
                    "\n" +
                    "The United States Department of Housing and Urban Development (HUD) and the United States Department of Veterans Affairs (VA) have created a program called Veterans Affairs Supportive Housing (VASH), or HUD-VASH, which distributes roughly 10,000 vouchers per year at a cost of roughly $75 million per year to eligible homeless and otherwise vulnerable U.S. armed forces veterans.[4] This program was created to pair HUD-funded vouchers with VA-funded services such as health care, counseling, and case management.[5]");

        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0]
                        + grantResults[1]
                        == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission granted", Toast.LENGTH_SHORT).show();
                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                            getActivity().startForegroundService(new Intent(getActivity(), CallService.class));

                    } else {

                            getActivity().startService(new Intent(getActivity(), CallService.class));
                        }*/
                    createandDisplayPdf("Section 8 also authorizes a variety of \"project-based\" rental assistance programs, under which the owner reserves some or all of the units in a building for low-income tenants, in return for a federal government guarantee to make up the difference between the tenant's contribution and the rent in the owner's contract with the government. A tenant who leaves a subsidized project will lose access to the project-based subsidy.\n" +
                            "\n" +
                            "The United States Department of Housing and Urban Development (HUD) and the United States Department of Veterans Affairs (VA) have created a program called Veterans Affairs Supportive Housing (VASH), or HUD-VASH, which distributes roughly 10,000 vouchers per year at a cost of roughly $75 million per year to eligible homeless and otherwise vulnerable U.S. armed forces veterans.[4] This program was created to pair HUD-funded vouchers with VA-funded services such as health care, counseling, and case management.[5]");
                } else {
                    Toast.makeText(getActivity(), " NO Permission granted", Toast.LENGTH_SHORT).show();
                    /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                        this.startForegroundService(new Intent(getActivity(), CallService.class));

                    } else {

                        this.startService(new Intent(getActivity(), CallService.class));
                    }*/
                }
        }
    }

}
