package com.ampere.bike.ampere.model.indent.Pojos;

public class IndentCorporate {


    public String name, address, createAt, email, mobile, pofilePhoto, username, updateAt;
    public String id, userType;

    public IndentCorporate(String name, String userType, String address, String createdAt, String email, String id, String mobile, String profilePhoto, String username, String updatedAt) {
        this.name = name;
        this.id = id;
        this.userType = userType;
        this.address = address;
        this.createAt = createdAt;
        this.email = email;
        this.mobile = mobile;
        this.pofilePhoto = profilePhoto;
        this.username = username;
        this.updateAt = updatedAt;
    }
}
