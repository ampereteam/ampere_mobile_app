package com.ampere.bike.ampere.views.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.api.RetrofitService;
import com.ampere.bike.ampere.api.api_new.UtilsCallApi;
import com.ampere.bike.ampere.api.api_new.UtilsUICallBack;
import com.ampere.bike.ampere.model.GetData;
import com.ampere.bike.ampere.model.forgotpassword.ModelForgotPassword;
import com.ampere.bike.ampere.model.login.ModelLogin;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.DateUtils;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.vehicles.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import customviews.CustomButton;
import customviews.CustomEditText;
import customviews.CustomTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ampere.bike.ampere.CustomNavigationDuo.setHeaderText;
import static com.ampere.bike.ampere.shared.LocalSession.BEARER;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_NAME;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;
import static com.ampere.bike.ampere.utils.MyUtils.capitalize;
import static com.ampere.bike.ampere.utils.MyUtils.isEmpty;

@SuppressWarnings("ALL")
public class Login extends Fragment implements UtilsUICallBack {

    private Handler handler = new Handler();
    private Runnable runnable;
    public static String pattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @BindView(R.id.snack_view)
    ConstraintLayout mSnackView;

    @BindView(R.id.btn_sign_in)
    CustomButton mSignInBtn;

    @BindView(R.id.edt_user_name)
    CustomEditText mUserEdt;
    @BindView(R.id.edt_password)
    CustomEditText mPasswordEdt;

    @BindView(R.id.txt_sign_up)
    CustomTextView mSignUpTxt;
    @BindView(R.id.txt_forgot_password)
    CustomTextView mForgotPassword;


    @BindView(R.id.login_layout)
    LinearLayout mLoginLayout;

    @BindView(R.id.forgot_password_layout)
    LinearLayout mForgotPasswordLayout;

    @BindView(R.id.register_layout)
    LinearLayout mRegisterLayout;

    @BindView(R.id.txt_login_have)
    CustomTextView mSignInTxt;

    @BindView(R.id.edt_user_email)
    CustomEditText mEmail;

    @BindView(R.id.btn_forgot_password)
    CustomButton mSentMail;

    @BindView(R.id.login_have)
    CustomTextView mBtnHaveLogin;

    @BindView(R.id.btn_register)
    CustomButton mRegister;

    @BindView(R.id.reg_user_name)
    CustomEditText mRegUserName;

    @BindView(R.id.reg_mobile_number)
    CustomEditText mRegMobileNo;

    @BindView(R.id.reg_email)
    CustomEditText mRegEmail;

    @BindView(R.id.reg_pwd)
    CustomEditText mRegPwd;

    @BindView(R.id.reg_cnfm_pwd)
    CustomEditText mRegCnfmPwd;


    private Unbinder unbinder;
    private Dialog dialog;
    private RetrofitService retrofitService;
    UtilsCallApi utilsCallApi;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) getActivity()).disableToolbar();
        return inflater.inflate(R.layout.frag_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        utilsCallApi = new UtilsCallApi(getActivity(), this);

        unbinder = ButterKnife.bind(this, view);
        retrofitService = MyUtils.getInstance();
        mLoginLayout.setVisibility(View.VISIBLE);
        mForgotPasswordLayout.setVisibility(View.GONE);
        mRegisterLayout.setVisibility(View.GONE);


        // mPasswordEdt.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD);
       /* mPasswordEdt.setInputType(InputType.TYPE_CLASS_TEXT |
                InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mPasswordEdt.setSelection(mPasswordEdt.getText().length());*/

        final String forgotPass = "<body><a href=www.ampere.com>Forgot Password?<a></body>";
        mForgotPassword.setText(Html.fromHtml(forgotPass));

        final String signUp = "<body>Don't have an account?<a href=www.ampere.com> Sign Up<a></body>";
        mSignUpTxt.setText(Html.fromHtml(signUp));

        final String signIn = "<body>Have an account?<a href=www.ampere.com> Sign In<a></body>";
        mSignInTxt.setText(Html.fromHtml(signIn));

        final String rememberPassowrd = "<body>Have you remember?<a href=www.ampere.com> Sign In<a></body>";
        mBtnHaveLogin.setText(Html.fromHtml(rememberPassowrd));
    }


    @Override
    public void onResume() {
        super.onResume();
      /*  runnable = new Runnable() {
            @Override
            public void run() {
                //MyUtils.passFragmentWithoutBackStack(getActivity(), new Home());
            }
        };
        handler.postDelayed(runnable, 2000);*/
    }

    @Override
    public void onPause() {
        super.onPause();
        //handler.removeCallbacks(runnable);
    }

    @OnClick(R.id.btn_sign_in)
    public void onSignIn() {

        String userName = "", password = "";
        userName = mUserEdt.getText().toString().trim();
        password = mPasswordEdt.getText().toString().trim();

        //Check entered values
        if (!isEmpty((AppCompatActivity) getActivity(), userName, "Password Can't be Empty")) {
            if (!isEmpty((AppCompatActivity) getActivity(), password, "UserIntentModel name Can't be Empty")) {
                // MessageUtils.showToastMessage(getActivity(), "Waiting for API");
                dialog = MessageUtils.showDialog(getActivity());

                RetrofitService retrofitService = MyUtils.getInstance();
                onLogin(retrofitService, userName, password);

            }
        }
    }

    private void onLogin(RetrofitService retrofitService, final String name, final String password) {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_name", name);
        map.put("password", password);

        // utilsCallApi.callApi(LOGIN, "POST", map, 0);


        loginOld(map);
    }

    private void loginOld(HashMap<String, String> map) {
        final Call<ModelLogin> modelLoginCall = retrofitService.onLogin(map);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialo, int keyCode, KeyEvent event) {
                MessageUtils.dismissDialog(dialog);
                modelLoginCall.cancel();
                return false;
            }
        });


        modelLoginCall.enqueue(new Callback<ModelLogin>() {
            @Override
            public void onResponse(Call<ModelLogin> call, Response<ModelLogin> response) {
                MessageUtils.dismissDialog(dialog);
                if (response.isSuccessful()) {
                    ModelLogin modelLogin = response.body();
                    if (modelLogin != null) {
                        if (modelLogin.isStatus) {
                            if(modelLogin.data.userDetails.userType.equals("2")){
                                //Add Login UserIntentModel Details
                                UpdateInstalledUserInfo(modelLogin.data.userDetails.name);


                                //Create session
                                LocalSession.createSeesion(getActivity(), modelLogin.data.userDetails.name, modelLogin.data.userDetails.username, modelLogin.data.userDetails.mobileNnumber, modelLogin.data.userDetails.userType, modelLogin.data.token,
                                        modelLogin.data.userDetails.photo, modelLogin.data.userDetails.address, modelLogin.data.userDetails.email, modelLogin.data.userDetails.id);

                                setHeaderText(getActivity());


                                /*  Log.d("SessionIsCreated", "" + LocalSession.getUserInfo(getActivity(), KEY_NAME) + " isLogin " + LocalSession.isLogin(getActivity()) + "  " + LocalSession.getUserInfo(getActivity(), KEY_TOKEN));*/

                            }else{
                                MessageUtils.showSnackBar(getActivity(), mSnackView, "User is Not a Dealer");
                            }

                        } else {
                            MessageUtils.showSnackBar(getActivity(), mSnackView, modelLogin.message);
                        }
                    } else {
                        MessageUtils.showSnackBar(getActivity(), mSnackView, getActivity().getString(R.string.api_response_not_found));
                    }

                } else {
                    MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                }

            }

            @Override
            public void onFailure(Call<ModelLogin> call, Throwable t) {
                MessageUtils.dismissDialog(dialog);
                //Log.d("login_failure", "" + t.getMessage());
                MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
            }
        });
    }

    private void UpdateInstalledUserInfo(String username) {
        HashMap<String, String> map = new HashMap<>();
        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            capitalize(model);
        } else {
            capitalize(manufacturer + " " + model);
        }

        map.put("device_id", capitalize(Build.SERIAL) + "  " + username);
        map.put("device_name", capitalize(manufacturer));
        map.put("install_at", DateUtils.getCurrentDate(DateUtils.DATE_DD_MM_YYYY_HH_MM_AP));
        map.put("version", version);


        MyUtils.getInstance().onStoreInslattedDevice(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), map).enqueue(new Callback<GetData>() {
            @Override
            public void onResponse(Call<GetData> call, Response<GetData> response) {
                if (response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(Call<GetData> call, Throwable t) {

            }
        });

    }

    @OnClick(R.id.txt_sign_up)
    public void onSignUp() {

        //MyUtils.passFragmentBackStack(getActivity(), new Register());
        mLoginLayout.setVisibility(View.GONE);
        mForgotPasswordLayout.setVisibility(View.GONE);
        mRegisterLayout.setVisibility(View.VISIBLE);


    }

    @OnClick(R.id.btn_register)
    public void onRegister() {
        String name = mRegUserName.getText().toString().trim();
        String mobileNo = mRegMobileNo.getText().toString().trim();
        String email = mRegEmail.getText().toString().trim();
        String pwd = mRegPwd.getText().toString().trim();
        String cnPwd = mRegCnfmPwd.getText().toString().trim();

        //Validaet Register Fields
        if (isRegister(name, mobileNo, email, pwd, cnPwd)) {


            dialog = MessageUtils.showDialog(getActivity());
            Log.d("Validate", "Success");
            final Call<ModelLogin> modelLoginCall = retrofitService.onRegister(name, mobileNo, email, pwd);
            dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialg, int keyCode, KeyEvent event) {
                    MessageUtils.dismissDialog(dialog);
                    modelLoginCall.cancel();
                    return false;
                }
            });
            modelLoginCall.enqueue(new Callback<ModelLogin>() {
                @Override
                public void onResponse(Call<ModelLogin> call, Response<ModelLogin> response) {
                    MessageUtils.dismissDialog(dialog);
                    if (response.isSuccessful()) {
                        ModelLogin modelLogin = response.body();

                    } else {
                        MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                    }
                }

                @Override
                public void onFailure(Call<ModelLogin> call, Throwable t) {
                    MessageUtils.dismissDialog(dialog);
                    MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
                }
            });
        }
    }

    /**
     * Registration Validation fields
     *
     * @param name
     * @param mobileNo
     * @param email
     * @param pwd
     * @param cnPwd
     * @return
     */
    private boolean isRegister(String name, String mobileNo, String email, String pwd, String cnPwd) {
        if (name.isEmpty()) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "UserIntentModel Name can't be empty");
            return false;
        } else if (mobileNo.isEmpty() || mobileNo.length() < 10) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "Mobile Number can't be empty.Enter 10 Digit Mobile Number.");
            return false;
        } else if (email.isEmpty()) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "Email ID can't be empty");
            return false;
        } else if (!Pattern.matches(pattern, email)) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "Invalid Email ID");
            return false;
        } else if (pwd.isEmpty()) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "Password can't be empty");
            return false;
        } else if (cnPwd.isEmpty()) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "Confirm Password can't be empty");
            return false;
        } else if (!pwd.equals(cnPwd)) {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "Password not matched");
            return false;
        }
        return true;
    }

    @OnClick(R.id.txt_forgot_password)
    public void onForgotPassword() {
        mLoginLayout.setVisibility(View.GONE);
        mForgotPasswordLayout.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.btn_forgot_password)
    public void sendMail() {
        String email = "";
        email = mEmail.getText().toString().trim();

        if (!isEmpty((AppCompatActivity) getActivity(), email, "Email ID Can't be Empty")) {
            if (Pattern.matches(pattern, email)) {
                MessageUtils.showToastMessage(getActivity(), "Waiting for API");
                dialog = MessageUtils.showDialog(getActivity());
                retrofitService.onForgotPassword(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), email).enqueue(new Callback<ModelForgotPassword>() {
                    @Override
                    public void onResponse(Call<ModelForgotPassword> call, Response<ModelForgotPassword> response) {
                        MessageUtils.dismissDialog(dialog);
                        if (response.isSuccessful()) {
                            ModelForgotPassword modelForgotPassword = response.body();
                        } else {
                            MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<ModelForgotPassword> call, Throwable t) {
                        MessageUtils.dismissDialog(dialog);
                        MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
                    }
                });
            } else {
                MessageUtils.showSnackBar(getActivity(), mSnackView, "Email Id not valid");
            }
        }
    }

    @OnClick(R.id.login_have)
    public void onHaveLoginDetails() {
        mLoginLayout.setVisibility(View.VISIBLE);
        mForgotPasswordLayout.setVisibility(View.GONE);
        mRegisterLayout.setVisibility(View.GONE);
    }

    /*@OnClick(R.id.btn_register)
    public void onRegister() {
        Log.d("Reister", "Yes");
        String regName = "", regMobileNo = "", regEmail = "", regPwd = "", regCnfmPwd = "";
        regName = mRegUserName.getText().toString();
        regMobileNo = mRegMobileNo.getText().toString();
        regEmail = mRegEmail.getText().toString();
        regPwd = mRegPwd.getText().toString();
        regCnfmPwd = mRegCnfmPwd.getText().toString();


        if (!MyUtils.isEmpty(regName)) {
            if (!MyUtils.isEmpty(regMobileNo)) {
                if (regMobileNo.length() == 10) {
                    if (!MyUtils.isEmpty(regEmail)) {
                        if (Pattern.matches(pattern, regEmail)) {
                            if (!MyUtils.isEmpty(regPwd)) {
                                if (!MyUtils.isEmpty(regCnfmPwd)) {
                                    if (regPwd.equals(regCnfmPwd)) {
                                        MessageUtils.showSnackBar(getActivity(), mSnackView, "Waiting for API");
                                    } else {
                                        MessageUtils.showSnackBar(getActivity(), mSnackView, "Password missmatch");
                                    }
                                } else {
                                    MessageUtils.showSnackBar(getActivity(), mSnackView, "Confirm Password Can't be Empty");
                                }
                            } else {
                                MessageUtils.showSnackBar(getActivity(), mSnackView, "Password Can't be Empty");
                            }
                        } else {
                            MessageUtils.showSnackBar(getActivity(), mSnackView, "Email ID not valid");
                        }
                    } else {
                        MessageUtils.showSnackBar(getActivity(), mSnackView, "Email ID Can't be Empty");
                    }
                } else {
                    MessageUtils.showSnackBar(getActivity(), mSnackView, "Mobile Number must be 10 digit");
                }
            } else {
                MessageUtils.showSnackBar(getActivity(), mSnackView, "Mobile Number Can't be Empty");
            }
        } else {
            MessageUtils.showSnackBar(getActivity(), mSnackView, "UserIntentModel name Can't be Empty");
        }
    }
*/
    @OnClick(R.id.txt_login_have)
    public void haveLogin() {
        Log.d("Reister", "Have Login");
        mLoginLayout.setVisibility(View.VISIBLE);
        mForgotPasswordLayout.setVisibility(View.GONE);
        mRegisterLayout.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void apiCallBackOverRideMethod(JSONObject response, int position,boolean sucess,String message,String apiCallPath) throws JSONException, IOException {
        Gson gson = new Gson();
        ModelLogin modelLogin = null;

        modelLogin = gson.fromJson(String.valueOf(response), ModelLogin.class);

        MessageUtils.dismissDialog(dialog);
        if (modelLogin != null) {
            if (modelLogin.isStatus) {
                Log.d("modelLoginsdsd", "" + modelLogin.data.userDetails.username);
                //Add Login UserIntentModel Details
                UpdateInstalledUserInfo(modelLogin.data.userDetails.name);
                //Create session
                LocalSession.createSeesion(getActivity(), modelLogin.data.userDetails.name, modelLogin.data.userDetails.username, modelLogin.data.userDetails.mobileNnumber, modelLogin.data.userDetails.userType, modelLogin.data.token,
                        modelLogin.data.userDetails.photo, modelLogin.data.userDetails.address, modelLogin.data.userDetails.email, modelLogin.data.userDetails.id);

                setHeaderText(getActivity());

                Log.d("SessionIsCreated", "" + LocalSession.getUserInfo(getActivity(), KEY_NAME) +
                        " isLogin " + LocalSession.isLogin(getActivity()) + "  " + LocalSession.getUserInfo(getActivity(), KEY_TOKEN));

            } else {
                MessageUtils.showSnackBar(getActivity(), mSnackView, modelLogin.message);
            }
        } else {
            MessageUtils.showSnackBar(getActivity(), mSnackView, getActivity().getString(R.string.api_response_not_found));
        }


    }


}
