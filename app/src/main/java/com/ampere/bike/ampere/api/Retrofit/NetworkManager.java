package com.ampere.bike.ampere.api.Retrofit;

//import com.ampere.bike.ampere.Indent.IndentList.IndentDetails.Pojos.vehicleListPojos.DataList;

import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.vehicleListPojos.DataList;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by USER on 28-09-2018.
 */

public interface NetworkManager {

    @POST("{path}")
    Call<ResponseBody> call_post(@Path("path") String method);

    @FormUrlEncoded
    @POST("{path}")
    Call<ResponseBody> call_post(@Path("path") String method, @FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("{path}")
    Call<ResponseBody> call_post(@Path("path") String method, @Header("Authorization") String auth, @FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("{path}")
    Call<ResponseBody> call_post(@Path("path") String method, @Header("Authorization") String auth, @Field("userid") String id);

    @POST("{path}")
    Call<ResponseBody> call_post(@Path("path") String method, @Header("Authorization") String auth, @Body HashMap<String, Object> jsonObject);

    /**
     * @param method
     * @param auth
     * @param id
     * @param form
     * @return
     */
    @Multipart
    @POST("{path}")
    Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response> call_post(@Path("path") String method, @Header("Authorization") String auth, @Part("invoice_id") RequestBody id,
                                                                                           @Part MultipartBody.Part form, @Part MultipartBody.Part adhar);


    @POST("/api/dealer/datalist")
    Call<DataList> call_post(@Header("Authorization") String auth, @Body HashMap<String, Object> jsonObject);


}
