package com.ampere.bike.ampere.views.kotlin.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.vehicles.R

class ViewEventDetailed : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        (activity as CustomNavigationDuo).disbleNavigationView("View Details")
        return inflater.inflate(R.layout.frag_view_even_details, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        /*
        //var eventDetails: CustomTextView = view.event_details
        var bundle: Bundle = this!!.arguments!!
         var id: Int = Integer.valueOf(bundle.get("id").toString())
         var name = bundle.get("name") as String
         var city = bundle.get("city") as String
         var mobile_no = bundle.get("mobile_no") as String
         var email = bundle.get("email") as String
         var lead_cource = bundle.get("lead_source") as String
         var assign_date = bundle.get("assign_date") as String
         var assign_to = bundle.get("assigned_to") as String
         var enquiry_date = bundle.get("enquiry_date") as String
         var enquiry_for = bundle.get("enquiry_for") as String
         var enquiry_time = bundle.get("enquiry_time") as String
         var assigned_person = bundle.get("assigned_person") as String
         var message = bundle.get("message") as String
         var status = bundle.get("status") as String
         var task = bundle.get("task") as String
         var sendThrough = bundle.get("sendThrough") as String

         eventDetails.text = Html.fromHtml(setText("Client Name", name) + setText("Mobile Number", "<u>" + mobile_no + "</u>") + setText("Email", email) +
                 setText("City", city) + setText("Lead Source", lead_cource) + setText("Assign Date", assign_date) + setText("Assigned To", assign_to) +
                 setText("Assigned Persion", assigned_person) + setText("TaskEnquiryModel For", enquiry_for) + setText("Vehicle Model", sendThrough) +
                 setText("TaskEnquiryModel Date & Time", enquiry_date + " " + enquiry_time) + setText("Message", message) + setText("Task ", task) + setText("Task Status", status))
 */
    }

    private fun setText(title: String, name: String): String? {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return "<h3>" + title + "</h3>" + name + "";
    }

    // Destroy when gotot another page
    override fun onDestroy() {
        super.onDestroy()
    }

    // This view destroy long
    override fun onDestroyView() {
        super.onDestroyView()
    }
}