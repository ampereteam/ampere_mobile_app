package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyInvoice.Pojos;

import com.google.gson.annotations.SerializedName;

public class WarrantyinvoiceItem{

	@SerializedName("vide")
	private String vide;

	@SerializedName("driver_no")
	private String driverNo;

	@SerializedName("vehicle_model")
	private String vehicleModel;

	@SerializedName("docket")
	private String docket;

	@SerializedName("dispatch_type")
	private String dispatchType;

	@SerializedName("driver_name")
	private String driverName;

	@SerializedName("manufacture_date")
	private String manufactureDate;

	@SerializedName("chassis_no")
	private String chassisNo;

	@SerializedName("qty")
	private String qty;

	@SerializedName("model_varient")
	private String modelVarient;

	@SerializedName("id")
	private int invoice_id;

	@SerializedName("spare_name")
	private String spareName;

	@SerializedName("serial_no")
	private String serialNo;

	@SerializedName("indent_invoice_date")
	private String indentInvoiceDate;

	public void setVide(String vide){
		this.vide = vide;
	}

	public String getVide(){
		return vide;
	}

	public void setDriverNo(String driverNo){
		this.driverNo = driverNo;
	}

	public String getDriverNo(){
		return driverNo;
	}

	public void setVehicleModel(String vehicleModel){
		this.vehicleModel = vehicleModel;
	}

	public String getVehicleModel(){
		return vehicleModel;
	}

	public void setDocket(String docket){
		this.docket = docket;
	}

	public String getDocket(){
		return docket;
	}

	public void setDispatchType(String dispatchType){
		this.dispatchType = dispatchType;
	}

	public String getDispatchType(){
		return dispatchType;
	}

	public void setDriverName(String driverName){
		this.driverName = driverName;
	}

	public String getDriverName(){
		return driverName;
	}

	public void setManufactureDate(String manufactureDate){
		this.manufactureDate = manufactureDate;
	}

	public String getManufactureDate(){
		return manufactureDate;
	}

	public void setChassisNo(String chassisNo){
		this.chassisNo = chassisNo;
	}

	public String getChassisNo(){
		return chassisNo;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setModelVarient(String modelVarient){
		this.modelVarient = modelVarient;
	}

	public String getModelVarient(){
		return modelVarient;
	}

	public void setId(int id){
		this.invoice_id = id;
	}

	public int getId(){
		return invoice_id;
	}

	public void setSpareName(String spareName){
		this.spareName = spareName;
	}

	public String getSpareName(){
		return spareName;
	}

	public void setSerialNo(String serialNo){
		this.serialNo = serialNo;
	}

	public String getSerialNo(){
		return serialNo;
	}

	public void setIndentInvoiceDate(String indentInvoiceDate){
		this.indentInvoiceDate = indentInvoiceDate;
	}
		boolean check;

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	public String getIndentInvoiceDate(){
		return indentInvoiceDate;
	}

	@Override
 	public String toString(){
		return 
			"WarrantyinvoiceItem{" + 
			"vide = '" + vide + '\'' + 
			",driver_no = '" + driverNo + '\'' + 
			",vehicle_model = '" + vehicleModel + '\'' +
			",docket = '" + docket + '\'' + 
			",dispatch_type = '" + dispatchType + '\'' + 
			",driver_name = '" + driverName + '\'' + 
			",manufacture_date = '" + manufactureDate + '\'' + 
			",chassis_no = '" + chassisNo + '\'' + 
			",qty = '" + qty + '\'' + 
			",model_varient = '" + modelVarient + '\'' + 
			",id = '" + invoice_id + '\'' +
			",spare_name = '" + spareName + '\'' + 
			",serial_no = '" + serialNo + '\'' + 
			",indent_invoice_date = '" + indentInvoiceDate + '\'' + 
			"}";
		}
}