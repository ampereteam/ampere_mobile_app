package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IndentDetailsPojos {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("indent_id")
    @Expose
    private Integer indentId;
    @SerializedName("indent_type")
    @Expose
    private String indentType;
    @SerializedName("vehicle_model")
    @Expose
    private String vehicleModel;
    @SerializedName("model_varient")
    @Expose
    private String modelVarient;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("spare_name")
    @Expose
    private String spareName;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("delivered_qty")
    @Expose
    private String deliveredQty;
    @SerializedName("pending_qty")
    @Expose
    private String pendingQty;
    @SerializedName("remarks")
    @Expose
    private String remarks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIndentId() {
        return indentId;
    }

    public void setIndentId(Integer indentId) {
        this.indentId = indentId;
    }

    public String getIndentType() {
        return indentType;
    }

    public void setIndentType(String indentType) {
        this.indentType = indentType;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getModelVarient() {
        return modelVarient;
    }

    public void setModelVarient(String modelVarient) {
        this.modelVarient = modelVarient;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSpareName() {
        return spareName;
    }

    public void setSpareName(String spareName) {
        this.spareName = spareName;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getDeliveredQty() {
        return deliveredQty;
    }

    public void setDeliveredQty(String deliveredQty) {
        this.deliveredQty = deliveredQty;
    }

    public String getPendingQty() {
        return pendingQty;
    }

    public void setPendingQty(String pendingQty) {
        this.pendingQty = pendingQty;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}
