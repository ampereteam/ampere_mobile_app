package com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints.Adapter.WarrantyComplaintsAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.Pojos.ComplaintResponsePojos.FreecomplaintsItem
import com.ampere.vehicles.R

class FreeRepalcementsComplaintsAdapter(val context: Context, private val frrComplaintList: ArrayList<FreecomplaintsItem?>?, private val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<FreeRepalcementsComplaintsAdapter.Viewholder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FreeRepalcementsComplaintsAdapter.Viewholder {
        val view :View = LayoutInflater.from(this.context).inflate(R.layout.adapter_free_replacement_complaints,parent,false)
        return Viewholder(view)
    }

    override fun getItemCount(): Int {
        return  this.frrComplaintList?.size!!
    }

    override fun onBindViewHolder(holder: FreeRepalcementsComplaintsAdapter.Viewholder, position: Int) {

        try {
            /**
             * set to LIst data from ArrayList
             */

            holder.ffrcSno.text = (position + 1).toString()
            holder.ffrcChassi.text = this.frrComplaintList!![position]?.chassisNo
            holder.ffrcComplaints.text = this.frrComplaintList!![position]?.complaint
            holder.ffrcSentDate.text = this.frrComplaintList!![position]?.sentDate
            holder.ffrcStatus.text = this.frrComplaintList[position]?.status

            /**
             * onclickInterface Function Called
             */

            /**
             * onclickInterface Function Called
             */
            holder.lnearLayout.setOnClickListener {
                this.configClickEvent.connectposition(position, this.frrComplaintList.size)
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    class Viewholder(itemView: View?): RecyclerView.ViewHolder(itemView) {
        val ffrcSno:TextView = itemView?.findViewById(R.id.ffrc_s_no_adtr)!!
        val ffrcChassi:TextView = itemView?.findViewById(R.id.ffrc_chass_no_adtr)!!
        val ffrcComplaints:TextView = itemView?.findViewById(R.id.ffrc_complaint_adtr)!!
        val ffrcSentDate:TextView = itemView?.findViewById(R.id.ffrc_sent_date_adtr)!!
        val ffrcStatus:TextView = itemView?.findViewById(R.id.ffrc_status_adtr)!!
        val lnearLayout:LinearLayout = itemView?.findViewById(R.id.frrc_linear_adptr)!!
    }
}
