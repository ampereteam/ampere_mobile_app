package com.ampere.bike.ampere.views.fragment.indent.VehicleListPojos;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Data{

	@SerializedName("serviceissues")
	private ArrayList<ServiceissuesItem> serviceissues;

	@SerializedName("leadsource")
	private ArrayList<LeadsourceItem> leadsource;

	@SerializedName("vehiclemodel")
	private ArrayList<VehiclemodelItem> vehiclemodel;

	@SerializedName("states")
	private ArrayList<StatesItem> states;

	public void setServiceissues(ArrayList<ServiceissuesItem> serviceissues){
		this.serviceissues = serviceissues;
	}

	public List<ServiceissuesItem> getServiceissues(){
		return serviceissues;
	}

	public void setLeadsource(ArrayList<LeadsourceItem> leadsource){
		this.leadsource = leadsource;
	}

	public List<LeadsourceItem> getLeadsource(){
		return leadsource;
	}

	public void setVehiclemodel(ArrayList<VehiclemodelItem> vehiclemodel){
		this.vehiclemodel = vehiclemodel;
	}

	public List<VehiclemodelItem> getVehiclemodel(){
		return vehiclemodel;
	}

	public void setStates(ArrayList<StatesItem> states){
		this.states = states;
	}

	public List<StatesItem> getStates(){
		return states;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"serviceissues = '" + serviceissues + '\'' + 
			",leadsource = '" + leadsource + '\'' + 
			",vehiclemodel = '" + vehiclemodel + '\'' + 
			",states = '" + states + '\'' + 
			"}";
		}
}