package com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Invoice.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Invoice.Pojos.SpareinventoryItem
import com.ampere.vehicles.R

class InvoiceListAdapter (val context: Context, val invoiceList: ArrayList<SpareinventoryItem?>?, val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<InvoiceListAdapter.ViewHolder>(){
    var i:Int =0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InvoiceListAdapter.ViewHolder {
        val view = LayoutInflater.from(this.context).inflate(R.layout.adapter_spareservice_invoice,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return  invoiceList?.size!!
    }

    override fun onBindViewHolder(holder: InvoiceListAdapter.ViewHolder, position: Int) {
        try{
            i=invoiceList?.size!!
            for(i:Int in 0..invoiceList?.size!!-1){
                holder.checkbox.isChecked=invoiceList[position]?.check!!
            }
            holder.sno.text = (position+1).toString()
            holder.color.text =invoiceList!![position]?.color
            holder.sparename.text = invoiceList[position]?.spareName
            holder.status.text = invoiceList[position]?.status
            holder.vehicle_model.text =invoiceList[position]?.vehicleModel
            /*holder.linearLayout.setOnClickListener {
                this.configClickEvent.connectposition(position,invoiceList.size)
            }*/
            if (invoiceList.get(position)?.check!!) {
                holder.checkbox.setChecked(true)
                i = invoiceList.size
            } else {
                holder.checkbox.setChecked(false)
                i = 0
            }
            holder.checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                invoiceList.get(position)!!.check=isChecked
                if (invoiceList.get(position)!!.check!!) {
                    if (invoiceList.size == i) {
                        configClickEvent.connectposition(position, i)
                    } else {
                        configClickEvent.connectposition(position, ++i)
                    }

                } else {
                    if (i != 0) {
                        configClickEvent.connectposition(position, --i)
                    } else {
                        configClickEvent.connectposition(position, 0)
                    }
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }
    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val sno :TextView = itemView?.findViewById(R.id.spsin_s_no)!!
        val vehicle_model :TextView = itemView?.findViewById(R.id.spsin_vehicle_model)!!
        val color :TextView = itemView?.findViewById(R.id.spsin_color)!!
        val sparename :TextView = itemView?.findViewById(R.id.spsin_spare_name)!!
        val status :TextView = itemView?.findViewById(R.id.spsin_status)!!
        val linearLayout :LinearLayout = itemView?.findViewById(R.id.spsin_linear_adp)!!
        val checkbox :CheckBox = itemView?.findViewById(R.id.spsin_checkbox)!!
    }
}