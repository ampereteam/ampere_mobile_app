package com.ampere.bike.ampere.views.fragment.parthi.Indent.SpareDispatch.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.SpareDispatch.Pojos.SpareDispatchPojos;
import com.ampere.vehicles.R;

import java.util.ArrayList;

@SuppressWarnings("All")
public class SpareDispatchAdapter  extends RecyclerView.Adapter<SpareDispatchAdapter.MyHolder> {
    private Context context;
    private ArrayList<SpareDispatchPojos> spareDispatchPojosArrayList;
    private ConfigClickEvent configposition;
    int i;
    public SpareDispatchAdapter(Context context, ArrayList<SpareDispatchPojos> spareDispatchPojosArrayList, ConfigClickEvent configposition) {
        this.context = context;
        this.spareDispatchPojosArrayList = spareDispatchPojosArrayList;
        this.configposition = configposition;
    }

    public  void  setList(ArrayList<SpareDispatchPojos> spareDispatchPojosArrayList){
        this.spareDispatchPojosArrayList = spareDispatchPojosArrayList;
    }
    @NonNull
    @Override
    public SpareDispatchAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_indent_spare_dispatch,parent,false);
        return new SpareDispatchAdapter.MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SpareDispatchAdapter.MyHolder holder, final int position) {
        try{

        if(spareDispatchPojosArrayList.get(position).isCheck()){
            holder.mark.setChecked(true);
            i= spareDispatchPojosArrayList.size();
        }else{
            holder.mark.setChecked(false);
            i=0;
        }
        holder.sRL_no.setText(String.valueOf(position+1));
        holder.chassNo.setText(spareDispatchPojosArrayList.get(position).getSerialNo());
        holder.v_MOdel.setText(spareDispatchPojosArrayList.get(position).getVehicleModel());
        holder.docket_no.setText(spareDispatchPojosArrayList.get(position).getDispatchType());
        String id =spareDispatchPojosArrayList.get(position).getId().toString();
            Log.d("indentId",""+id);
        holder.indnt_id.setText(id);
        holder.mark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                spareDispatchPojosArrayList.get(position).setCheck(isChecked);

                if(spareDispatchPojosArrayList.get(position).isCheck()){
                    if(spareDispatchPojosArrayList.size()==i){
                        configposition.connectposition(position,i);
                    }else{
                        configposition.connectposition(position,++i);
                    }

                }else{
                    if(i!=0){
                        configposition.connectposition(position,--i);
                    }
                    else{
                        configposition.connectposition(position,0);
                    }
                }


            }
        });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return spareDispatchPojosArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView sRL_no,indnt_id,docket_no,v_MOdel,chassNo;
        CheckBox mark;

        public MyHolder(View itemView) {
            super(itemView);
            sRL_no= itemView.findViewById(R.id.sNO);
            indnt_id =itemView.findViewById(R.id.inDEnt_id_tv);
            docket_no =itemView.findViewById(R.id.docket_No);
            v_MOdel =itemView.findViewById(R.id.vehicle_Mod);
            chassNo =itemView.findViewById(R.id.chass_No);
            mark =itemView.findViewById(R.id.checking);
        }
    }
}
