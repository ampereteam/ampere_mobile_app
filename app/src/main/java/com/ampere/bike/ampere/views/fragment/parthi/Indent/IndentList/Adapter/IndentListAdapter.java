package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.Pojos.IndentsItemPojos;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class IndentListAdapter extends RecyclerView.Adapter<IndentListAdapter.MyHolder> {
    Context context;
    ArrayList<IndentsItemPojos> indentListPojoArrayList ;
    ConfigClickEvent configposition;

    public IndentListAdapter(Context context, ArrayList<IndentsItemPojos> indentListPojoArrayList, ConfigClickEvent configposition) {
        this.context = context;
        this.indentListPojoArrayList = indentListPojoArrayList;
        this.configposition = configposition;
        //System.out.println("adapterList Value"+indentListPojoArrayList.get(0).getRequestby().getMobile());
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_indent_list,parent,false);
        return  new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, final int position) {
        try{

        holder.sNo.setText(String.valueOf(position+1));
        if(indentListPojoArrayList.get(position).getStatus() == null ||
                indentListPojoArrayList.get(position).getStatus().trim().isEmpty()){
            holder.sTs.setText("-");
        }else{
            holder.sTs.setText(indentListPojoArrayList.get(position).getStatus().trim());
        }

        if(indentListPojoArrayList.get(position).getTotalQty() == null ||
                indentListPojoArrayList.get(position).getTotalQty().isEmpty()){
            holder.resTQnt.setText("-");
        }else{
            holder.resTQnt.setText(indentListPojoArrayList.get(position).getTotalQty());
        }
        if(indentListPojoArrayList.get(position).getRequestby().getName() == null ||
                indentListPojoArrayList.get(position).getRequestby().getName().isEmpty()){
            holder.rQstFor.setText("-");
        }else{
            holder.rQstFor.setText(indentListPojoArrayList.get(position).getRequestDate());
        }
        if(indentListPojoArrayList.get(position).getIndentId() == null ||
                indentListPojoArrayList.get(position).getIndentId().isEmpty()){
            holder.idntID.setText("-");
        }else{
            holder.idntID.setText(indentListPojoArrayList.get(position).getIndentId());
        }

        holder.rQstFor.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.rQstFor.setSelected(true);
        holder.rQstFor.setSingleLine(true);
        holder.sTs.setSingleLine(true);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configposition.connectposition(position,indentListPojoArrayList.size());
            }
        });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
         return  indentListPojoArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView sNo,idntID,rQstFor,resTQnt,sTs;
        RelativeLayout relativeLayout;
        public MyHolder(View itemView) {
            super(itemView);
            sNo =itemView.findViewById(R.id.sNum);
            idntID =itemView.findViewById(R.id.inDEnt_id);
            rQstFor =itemView.findViewById(R.id.reStfr);
            resTQnt =itemView.findViewById(R.id.odr_qnty);
            sTs =itemView.findViewById(R.id.sTus);
            relativeLayout =itemView.findViewById(R.id.indent_details);
        }
    }
}
