package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyDefects.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;


import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.utils.DateUtils;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyDefects.Pojos.DefectsPojo;
import com.ampere.vehicles.R;

import java.util.ArrayList;

import static com.ampere.bike.ampere.utils.DateUtils.DD_MM_YY;
import static com.ampere.bike.ampere.utils.DateUtils.YYYY_MM_DD;

public class WarrantyDefectsAdapter extends RecyclerView.Adapter<WarrantyDefectsAdapter.ViewHolder>{
    Context context;
    ArrayList<DefectsPojo> defectsPojoArrayList;
    ConfigClickEvent configposition;
    int i=0;

    public WarrantyDefectsAdapter(Context context, ArrayList<DefectsPojo> defectsPojoArrayList, ConfigClickEvent configposition) {
        this.context = context;
        this.defectsPojoArrayList = defectsPojoArrayList;
        this.configposition = configposition;

    }

        public  void setList(ArrayList<DefectsPojo> defectsPojoArrayList){
        this.defectsPojoArrayList =defectsPojoArrayList;
        }
    public WarrantyDefectsAdapter(Context context, ArrayList<DefectsPojo> defectsPojoArrayList) {
        this.context = context;
        this.defectsPojoArrayList = defectsPojoArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_warranty_defects,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        try{
        if(defectsPojoArrayList.get(position).isCheck()){
            holder.mark.setChecked(true);
            i=defectsPojoArrayList.size();
        }else{
            holder.mark.setChecked(false);
            i=0;
        }
      holder.sno.setText(String.valueOf(position+1));
        System.out.println(" setting serial number"+position);
        if(defectsPojoArrayList.get(position).getChassisNo() == null || defectsPojoArrayList.get(position).getChassisNo().isEmpty()){
            holder.chassno.setText("-");
        }else{
            holder.chassno.setText(defectsPojoArrayList.get(position).getChassisNo());
            holder.chassno.setSingleLine(true);
            holder.chassno.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.chassno.setSelected(true);
        }

        if(defectsPojoArrayList.get(position).getComponent() == null ||defectsPojoArrayList.get(position).getComponent().isEmpty()){
            holder.compant.setText("-");
        }else{
            holder.compant.setText(defectsPojoArrayList.get(position).getComponent());
            holder.compant.setSingleLine(true);
            holder.compant.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.compant.setSelected(true);
        }


        if(defectsPojoArrayList.get(position).getNewSerialNo() == null || defectsPojoArrayList.get(position).getNewSerialNo().isEmpty()){
            holder.sErNO.setText("-");
        }else{
            holder.sErNO.setText(defectsPojoArrayList.get(position).getNewSerialNo());
            holder.sErNO.setSingleLine(true);
            holder.sErNO.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.sErNO.setSelected(true);
        }

        if(defectsPojoArrayList.get(position).getChangedDate() == null || defectsPojoArrayList.get(position).getChangedDate().isEmpty()){
            holder.repldaTe.setText("-");
        }else{
            holder.repldaTe.setText(DateUtils.convertDates(defectsPojoArrayList.get(position).getChangedDate(),YYYY_MM_DD,DD_MM_YY));
            holder.repldaTe.setSingleLine(true);
            holder.repldaTe.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            holder.repldaTe.setSelected(true);
        }






      holder.mark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                defectsPojoArrayList.get(position).setCheck(isChecked);

                    if(defectsPojoArrayList.get(position).isCheck()){
                        if(defectsPojoArrayList.size()==i){
                            configposition.connectposition(position,i);
                        }else{
                            configposition.connectposition(position,++i);
                        }

                    }else{
                        if(i!=0){
                            configposition.connectposition(position,--i);
                        }
                        else{
                            configposition.connectposition(position,0);
                        }
                    }


          }
      });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return defectsPojoArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView sno,chassno,compant,sErNO,repldaTe;
        CheckBox mark;
        public ViewHolder(View itemView) {
            super(itemView);
            sno=itemView.findViewById(R.id.sNO);
            chassno=itemView.findViewById(R.id.CHAASNO);
            compant=itemView.findViewById(R.id.cOMPNTname);
            sErNO=itemView.findViewById(R.id.sEELNO);
            repldaTe=itemView.findViewById(R.id.rePLDate);
            mark=itemView.findViewById(R.id.markk);

        }
    }
}
