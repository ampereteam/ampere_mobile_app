package com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSpareService.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo.EnquiriesItem
import com.ampere.vehicles.R
import customviews.CustomTextView

class AssignSpareServiceAdapter (val  context: Context, val enquiryArrayList: ArrayList<EnquiriesItem?>?, val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<AssignSpareServiceAdapter.ViewHolder>(){
    var i :Int = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssignSpareServiceAdapter.ViewHolder {
        val view =LayoutInflater.from(this.context).inflate(R.layout.adapter_assign_sales_serivce,parent,false)
        return  ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return  this.enquiryArrayList?.size!!
    }

    override fun onBindViewHolder(holder: AssignSpareServiceAdapter.ViewHolder, position: Int) {
        try{
            for(i:Int in 0 ..enquiryArrayList?.size!!-1 ){
                holder.checkbox.isChecked = enquiryArrayList.get(i)?.check!!
            }
            holder.date.text = this.enquiryArrayList.get(position)?.enquiryDate
            holder.date.setSingleLine()
            CustomTextView.marquee(holder.date)
            holder.name.text = this.enquiryArrayList.get(position)?.customerName
            holder.name.setSingleLine()
            CustomTextView.marquee(holder.name)
            holder.task.text = this.enquiryArrayList.get(position)?.task
            holder.task.setSingleLine()
            CustomTextView.marquee(holder.task)
            holder.model.text = this.enquiryArrayList.get(position)?.vehicleModel
            holder.model.setSingleLine()
            CustomTextView.marquee(holder.date)
            holder.status.text = this.enquiryArrayList?.get(position)?.taskStatus
            holder.status.setSingleLine()
            CustomTextView.marquee(holder.date)
            if(enquiryArrayList.get(position)?.check!!){
                holder.checkbox.isChecked =true
                i=enquiryArrayList.size
            }else{
                holder.checkbox.isChecked= false
                i=0
            }
            holder.checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                enquiryArrayList.get(position)?.check = isChecked
                if(isChecked){
                    if (enquiryArrayList.size == i) {
                        configClickEvent.connectposition(position, i)
                    } else {
                        configClickEvent.connectposition(position, ++i)
                    }
                }else{
                    if (i != 0) {
                        configClickEvent.connectposition(position, --i)
                    } else {
                        configClickEvent.connectposition(position, 0)
                    }
                }
            }



        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val date : CustomTextView = itemView?.findViewById(R.id.tv_enquiry_date)!!
        val name : CustomTextView = itemView?.findViewById(R.id.tv_cus_name)!!
        val task : CustomTextView = itemView?.findViewById(R.id.tv_enquiry_task)!!
        val  model : CustomTextView = itemView?.findViewById(R.id.tv_enquiry_vmodel)!!
        val status : CustomTextView = itemView?.findViewById(R.id.tv_enquiry_status)!!
        val checkbox : CheckBox = itemView?.findViewById(R.id.cbx_enquiry_assign)!!
        val linear : LinearLayout = itemView?.findViewById(R.id.linear_sales)!!
    }
}