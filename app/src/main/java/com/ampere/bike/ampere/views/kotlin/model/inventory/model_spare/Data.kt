package com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare

data class Data(
        val sparedespatched: ArrayList<Sparedespatched>,
        val sparedelivered: ArrayList<Sparedelivered>
)