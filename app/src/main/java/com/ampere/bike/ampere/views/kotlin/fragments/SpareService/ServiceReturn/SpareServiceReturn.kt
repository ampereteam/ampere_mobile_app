package com.ampere.bike.ampere.views.kotlin.fragments.SpareService.ServiceReturn


import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.os.StrictMode
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.DefectReturn.Pojos.DefectItem
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Invoice.Adapter.InvoiceListAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Invoice.Pojos.InvoiceListRES

import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.ServiceReturn.Adapter.ServiceReturnListAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.ServiceReturn.Pojo.ServiceReturnListRES


import com.ampere.vehicles.R
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SpareServiceReturn : Fragment(),ConfigClickEvent {

    var recyservicereturnList: RecyclerView?= null
    var acceptlinear: LinearLayout?= null
    var btnAcept: Button?= null
    var emptyList: TextView?= null
    var checkbox: CheckBox?= null
    var callApi: UtilsCallApi?=null
    var dialog: Dialog?=null
    var serviceReturnlistRes: ServiceReturnListRES?=null
    var  serviceReturnListAdapter : ServiceReturnListAdapter?=null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =inflater.inflate(R.layout.fragment_spare_service_return, container, false)
        var newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        return view
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callApi = UtilsCallApi(activity)
        dialog =Dialog(activity)
        recyservicereturnList = view.findViewById(R.id.recycle_spssr_complaint_list)
        emptyList = view.findViewById(R.id.spssr_emptyList)
        checkbox = view.findViewById(R.id.checkbox_spssr)
        acceptlinear = view.findViewById(R.id.spssr_accept)
        btnAcept = view.findViewById(R.id.spssr_acpt)

        getDataFrmSrvr()
        /**checkbox ischeckedListener **/
        checkbox?.setOnCheckedChangeListener { buttonView, isChecked ->
            try{
                if(isChecked){
                    if(serviceReturnlistRes?.data?.servicereturns?.size!!>0){
                        for(i :Int in 0..serviceReturnlistRes?.data?.servicereturns?.size!!-1){
                            serviceReturnlistRes?.data?.servicereturns?.get(i)?.check =true
                        }
                        acceptlinear?.visibility =View.VISIBLE
                        btnAcept?.text="Accept("+serviceReturnlistRes?.data?.servicereturns?.size!!+")"
                    }
                }else{
                    if(serviceReturnlistRes?.data?.servicereturns?.size!!>0) {
                        for (i: Int in 0..serviceReturnlistRes?.data?.servicereturns?.size!! - 1) {
                            serviceReturnlistRes?.data?.servicereturns?.get(i)?.check = false
                        }
                        acceptlinear?.visibility =View.GONE
                    }
                }
                serviceReturnListAdapter?.notifyDataSetChanged()
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }
        btnAcept?.setOnClickListener {
            try{
                val acceptList:ArrayList<DefectItem> = ArrayList()
                for(i:Int in 0 .. serviceReturnlistRes?.data?.servicereturns?.size!!-1){
                    val acceptItem = DefectItem(serviceReturnlistRes?.data?.servicereturns?.get(i)?.id.toString())
                    if(serviceReturnlistRes?.data?.servicereturns?.get(i)?.check!!){
                        Log.d("DefedctItem",""+acceptItem.defectId)
                        acceptList.add(acceptItem)
                    }
                }
                if(acceptList.size<=0){
                    Log.d("",""+acceptList.size)
                }else{
                    senTOsrvr(acceptList)
                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }
    }

    private fun senTOsrvr(acceptList: ArrayList<DefectItem>) {
        try{
            if(callApi?.isNetworkConnected!!){
                dialog= MessageUtils.showDialog(activity)
                val hashmap :HashMap<String,Any> = HashMap()
                val map :HashMap<String,Any> = HashMap()
                hashmap.put("serviceitem",acceptList)
                map.put("serviceitems",hashmap)
                map["userid"]= LocalSession.getUserInfo(activity, LocalSession.KEY_ID)
                val calinterface  =callApi?.connectRetro("POST","acceptsparesservicereturn")
                val callback =calinterface?.call_post("acceptsparesservicereturn","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),map)
                callback?.enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.message)
                    }
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                val jsonObject = JSONObject(response.body()?.string())
                                val success =jsonObject.getBoolean("success")!!
                                val message = jsonObject.getString("message")!!
                                if(success){
//                                    activity?.finish()
                                    acceptlinear?.visibility =View.GONE
                                    getDataFrmSrvr()
                                }else{
                                    MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,message)
                                }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }

                        }else{
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response?.message())
                        }
                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity!!, CustomNavigationDuo.snackVIew,getString(R.string.check_internet))
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun getDataFrmSrvr() {
        try{
            if(callApi?.isNetworkConnected!!){
                dialog= MessageUtils.showDialog(activity)
                val map:HashMap<String,Any> = HashMap()
                map.put("userid", LocalSession.getUserInfo(activity, LocalSession.KEY_ID))
                var calinterface = callApi?.connectRetro("POST","sparesservicereturn")
                var callback = calinterface?.call_post("sparesservicereturn","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),map)
                callback?.enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.message)
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                serviceReturnlistRes= Gson().fromJson(response.body()?.string(), ServiceReturnListRES::class.java)
          /*                      serviceReturnlistRes?.data?.servicereturns?.addAll(serviceReturnlistRes?.data?.servicereturns!!)
                                serviceReturnlistRes?.data?.servicereturns?.addAll(serviceReturnlistRes?.data?.servicereturns!!)
                                serviceReturnlistRes?.data?.servicereturns?.addAll(serviceReturnlistRes?.data?.servicereturns!!)
                                serviceReturnlistRes?.data?.servicereturns?.addAll(serviceReturnlistRes?.data?.servicereturns!!)*/
                                if(serviceReturnlistRes?.data?.servicereturns?.size!!<=0 ){
                                    Log.d("ArrayListSIze",""+isStateSaved)
                                    emptyList?.text = serviceReturnlistRes?.message
                                    emptyList?.visibility =View.VISIBLE
                                    recyservicereturnList?.visibility =View.GONE
                                }else{
                                    recyservicereturnList?.visibility =View.VISIBLE
                                    emptyList?.visibility =View.GONE
                                    setToAdpter()
                                }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }


                        }else{
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response.message())
                        }

                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity, CustomNavigationDuo.snackVIew,getString(R.string.check_internet))
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setToAdpter() {
        try{
            serviceReturnListAdapter= ServiceReturnListAdapter(activity!!,serviceReturnlistRes?.data?.servicereturns,this)
            recyservicereturnList?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            recyservicereturnList?.adapter =serviceReturnListAdapter
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }
    override fun connectposition(position: Int, listSize: Int) {
        try{
            Log.d("Called interface Function",""+position+listSize)

            if(listSize <=0){
                acceptlinear?.visibility =View.GONE
                btnAcept?.setText("Accept(0)")
                checkbox?.isChecked =false
            }else{
                acceptlinear?.visibility =View.VISIBLE
                btnAcept?.setText("Accept("+listSize+")")
            }
            if(serviceReturnlistRes?.data?.servicereturns?.get(position)?.check!!){
                Log.d("SpareName",""+serviceReturnlistRes?.data?.servicereturns?.get(position)?.check)
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
}
