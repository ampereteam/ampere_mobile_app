package com.ampere.bike.ampere.views.kotlin.fragments.model.model_crm

import com.google.gson.annotations.SerializedName

import javax.annotation.Generated

@Generated("net.hexar.json2pojo")
class Datum {

    @SerializedName("created_at")
    var createdAt: String = ""
    @SerializedName("id")
    var id: Long? = 0
    @SerializedName("status")
    var status: String = ""
    @SerializedName("task")
    var task: String = ""
    @SerializedName("task_type")
    var taskType: String = ""
    @SerializedName("updated_at")
    var updatedAt: String = ""


}
