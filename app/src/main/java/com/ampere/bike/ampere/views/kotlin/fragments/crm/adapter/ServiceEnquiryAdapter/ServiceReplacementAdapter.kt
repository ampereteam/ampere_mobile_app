package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.ServiceEnquiryAdapter

import android.content.Context
import android.support.design.widget.TextInputEditText
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.ServiceEnquiryServerPojo.ServiceReplacementServerPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.serviceReplacementPojos.ServicereplacementPojos
import com.ampere.vehicles.R

@Suppress("UNREACHABLE_CODE")
class ServiceReplacementAdapter (val context: Context, val serviceReplacementArrayList: ArrayList<ServicereplacementPojos?>?, val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<ServiceReplacementAdapter.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceReplacementAdapter.ViewHolder {
       val view = LayoutInflater.from(this.context).inflate(R.layout.adapter_service_enquiry_service_replacement,parent,false)
       return  ViewHolder(view)
    }

    override fun getItemCount(): Int {
        try{
            return this.serviceReplacementArrayList?.size!!
        }catch (ex:Exception){
            return this.serviceReplacementArrayList?.size!!
            ex.printStackTrace()
        }
    }


    override fun onBindViewHolder(holder: ServiceReplacementAdapter.ViewHolder, position: Int) {
        try{
            holder.componentName.setText(serviceReplacementArrayList!![position]?.component!!)
            holder.oldSerialNo.setText(serviceReplacementArrayList[position]?.oldserial!!)
            holder.newSerialNo.setText(serviceReplacementArrayList[position]?.sparesstock)
            var i :Int = 0
                for(service in serviceReplacementArrayList){
                    val serviceServer  =ServiceReplacementServerPojos(service?.component,service?.oldserial,service?.sparesstock,serviceReplacementArrayList[position]?.complaint_id)
                    CRMDetailsActivity.serviceReplacementServerLISt.add(serviceServer)
                    Log.d("ArraYLIST",""+CRMDetailsActivity.serviceReplacementServerLISt[0].componentName)
                    Log.d("ArraYLIST",""+CRMDetailsActivity.serviceReplacementServerLISt[0].oldSerialNo)
                    Log.d("ArraYLIST",""+CRMDetailsActivity.serviceReplacementServerLISt[0].newSerialNo)
                    i++
                }

        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }
    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
            val componentName : TextInputEditText = itemView?.findViewById(R.id.ws_edt_component_name)!!
            val oldSerialNo : TextInputEditText = itemView?.findViewById(R.id.wrs_edt_old_serial_number)!!
            val newSerialNo : TextInputEditText = itemView?.findViewById(R.id.wss_edt_new_serial_number)!!
            val linear : LinearLayout = itemView?.findViewById(R.id.warranty_service_replacement)!!
    }
}