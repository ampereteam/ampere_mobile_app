package com.ampere.bike.ampere.views.fragment.indent;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.api.RetrofitService;
import com.ampere.bike.ampere.api.api_new.UtilsCallApi;
import com.ampere.bike.ampere.api.api_new.UtilsUICallBack;
import com.ampere.bike.ampere.model.enquiry.spares.ModelVehicleModelBaseSpares;
import com.ampere.bike.ampere.model.enquiry.spares.SparesGetter;
import com.ampere.bike.ampere.model.indent.DataItem;
import com.ampere.bike.ampere.model.indent.IndentModel;
import com.ampere.bike.ampere.model.indent.IndentModelSuccessResponse;
import com.ampere.bike.ampere.model.indent.IndentVehicleRequestModel;
import com.ampere.bike.ampere.model.indent.Pojos.IndentCorporate;
import com.ampere.bike.ampere.model.indent.Pojos.IndentDealer;
import com.ampere.bike.ampere.model.indent.UserIntentModel;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.DateUtils;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.indent.ColorListPojos.ColorListPojos;
import com.ampere.bike.ampere.views.fragment.indent.SendToServerPojos.SendToServerPojos;
import com.ampere.bike.ampere.views.fragment.indent.VariantListPojos.SparePojos;
import com.ampere.bike.ampere.views.fragment.indent.VariantListPojos.VariantListPojos;
import com.ampere.bike.ampere.views.fragment.indent.VehicleListPojos.DataList;
import com.ampere.bike.ampere.views.fragment.indent.VehicleListPojos.VehiclemodelItem;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentHome;
import com.ampere.bike.ampere.views.kotlin.fragments.indent.IndentDetails;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import customviews.CustomButton;
import customviews.CustomTextEditView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static com.ampere.bike.ampere.CustomNavigationDuo.snackVIew;
import static com.ampere.bike.ampere.shared.LocalSession.BEARER;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_MOBILE;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_USER_TYPE;
import static com.ampere.bike.ampere.shared.LocalSession.T_CRM;
import static com.ampere.bike.ampere.shared.LocalSession.T_DEALER;
import static com.ampere.bike.ampere.utils.MyUtils.focusable;

public class Indent extends Fragment implements UtilsUICallBack {

    private String vehicle = "", spare = "", assignedName = "", assingedTo = "Dealer", color = "";
    private String assigneId = "";
    private String initial = "";
    private Unbinder unbinder;
    @BindView(R.id.rg_indent_type)
    RadioGroup mRGIndentType;
    @BindView(R.id.ind_sp_vehicle)
    Spinner mSpVehicle;
    @BindView(R.id.in_edt_vehicle)
    CustomTextEditView mEdtVehicle;
    @BindView(R.id.ind_sp_spares)
    Spinner mSpSpares;
    @BindView(R.id.in_edt_spares)
    CustomTextEditView mEdtSpares;
    @BindView(R.id.item_add_reomve)
    ImageButton mAddItem;
    @BindView(R.id.in_edt_qty)
    CustomTextEditView mEdtQty;
    @BindView(R.id.l_add_item)
    LinearLayout mAddContainer;
    @BindView(R.id.snack_View)
    RelativeLayout mSnackView;
    @BindView(R.id.btn_pass_data)
    CustomButton mBtnPass;
    @BindView(R.id.l_type_base)
    LinearLayout mLtyBaseLayout;
    @BindView(R.id.ind_model_layout)
    RelativeLayout lModelVariant;
    @BindView(R.id.l_vehicle)
    RelativeLayout mRVehicleLay;
    @BindView(R.id.l_spares)
    RelativeLayout mRSparesLay;
    @BindView(R.id.model_vehicle)
    LinearLayout mModelVihcle;
    @BindView(R.id.indent_model_varient)
    CustomTextEditView mModelVarient;
    @BindView(R.id.l_color)
    RelativeLayout mModelcolorLayout;
    @BindView(R.id.in_edt_color)
    CustomTextEditView edtmModelColor;
    @BindView(R.id.ind_sp_color)
    Spinner spIndentColor;
    @BindView(R.id.indent_remarks)
    CustomTextEditView mModelRemark;
    @BindView(R.id.indent_varient_spinner)
    Spinner mVarientSp;
    @BindView(R.id.rb_new_order)
    RadioButton mRBNewOrderVeh;
    @BindView(R.id.rb_spares)
    RadioButton mRBSapres;
    @BindView(R.id.indent_for_crm)
    LinearLayout mIndentCrmLay;
    @BindView(R.id.indent_crm)
    RadioGroup mRGdealerORCorporate;
    @BindView(R.id.indent_rb_dealer)
    RadioButton mRBDealer;
    @BindView(R.id.indent_rb_corporate)
    RadioButton mRBCorporate;
    @BindView(R.id.indent_edt_assigned_name)
    CustomTextEditView mAssignedName;
    @BindView(R.id.indent_sp_assigned)
    Spinner mAssignedSP;
    private String type = "Vehicle", varientType = "", noVarient = "No Varient", noVehicle = "No Vehicle", noCD = "";
    private RetrofitService retrofitService;
    ArrayList<String> userDealer = new ArrayList<>();
    ArrayList<String> userCorporate = new ArrayList<>();

    ArrayList<IndentDealer> userDealerPojo = new ArrayList<>();
    ArrayList<IndentCorporate> userCorporatePojo = new ArrayList<>();
    private Dialog dialog;
    ArrayList<IndentModel> indentModels = new ArrayList<>();
    private ArrayList<SparesGetter> sparesGetters = new ArrayList<>();
    ArrayList<String> vehicleNameArraylist = new ArrayList<>();
    ArrayList<String> spareNameArraylist = new ArrayList<>();
    ArrayList<String> modelvarient = new ArrayList<>();
    ArrayList<HashMap<String, SendToServerPojos>> sendToServerPojosArrayList = new ArrayList<HashMap<String, SendToServerPojos>>();
    UtilsCallApi utilsCallApi;
    ArrayList<ColorListPojos> colorListPojosArrayList = new ArrayList<>();
    ArrayList<SparePojos> sparePojosArrayList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) getActivity()).disbleNavigationView(getString(R.string.indent));
        utilsCallApi = new UtilsCallApi(getActivity(), this);
        return inflater.inflate(R.layout.frag_indent, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        unbinder = ButterKnife.bind(this, view);
        mVarientSp.setVisibility(View.VISIBLE);
        edtmModelColor.setTypeface(MyUtils.setType(getActivity(), "fonts/Lato-Medium.ttf"));

        if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CRM)) {
            mIndentCrmLay.setVisibility(View.VISIBLE);
        }
        assigneId = LocalSession.getUserInfo(getActivity(), KEY_ID);

        retrofitService = MyUtils.getRetroService();
        indentModels.clear();
        sparesGetters.clear();
        ArrayList<String> spi = new ArrayList<>();
        vehicleNameArraylist.add(getResources().getString(R.string.select_vehicle));
        mSpVehicle.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, spi));
        spareNameArraylist.add(getString(R.string.select_spare));
        mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, spi));
        mRSparesLay.setVisibility(GONE);

//        getLoadInitialValues();
        CallApiVehicle();

        mRGdealerORCorporate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                RadioButton radioButton = mRGdealerORCorporate.findViewById(i);
                assingedTo = radioButton.getText().toString();
                Log.d("selectAssignedTo", "" + assingedTo);
                //if (mAddContainer.getChildCount() == 0) {
                if (assingedTo.equals(getString(R.string.dealer))) {
                    mAssignedSP.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, userDealer));
                } else {
                    mAssignedSP.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, userCorporate));
                }
               /* } else {
                    MessageUtils.showSnackBar(getActivity(), mSnackView, "Already you added " + mAddContainer.getChildCount() + " Item. So can't change now");
                    if (assingedTo.equals(getString(R.string.dealer))) {
                        mRBDealer.setChecked(true);
                        mRBCorporate.setChecked(false);
                    } else {
                        mRBDealer.setChecked(false);
                        mRBCorporate.setChecked(true);
                    }
                }*/
            }
        });

        mAssignedSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                assignedName = mAssignedSP.getSelectedItem().toString();
                Log.d("assignedNames", "" + assignedName);

                if (assignedName.isEmpty()) {
                    mAssignedName.setText("");
                } else {

                    mAssignedName.setText(assignedName);

                 /*   int poistion = mRGdealerORCorporate.getCheckedRadioButtonId();
                    RadioButton radioButton = mRGdealerORCorporate.findViewById(poistion);
                    assingedTo = radioButton.getText().toString();*/
                    Log.d("assignTo ", "" + assingedTo);

                    if (assingedTo.equals(getString(R.string.dealer))) {
                        if (assignedName.equals("No Dealers")) {
                        } else {
                            assigneId = userDealerPojo.get(i).id;
                        }
                    } else {
                        if (assignedName.equals("No Corporate")) {

                        } else {
                            assigneId = userCorporatePojo.get(i).id;
                        }
                    }

                    Log.d("assigneIds", "" + assigneId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sendToServerPojosArrayList.clear();
        mAddItem.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onClick(View view) {

                String qty = mEdtQty.getText().toString().trim();
                String varient = mModelVarient.getText().toString().trim();
                String color = edtmModelColor.getText().toString().trim();
                String remark = mModelRemark.getText().toString().trim();
                String model = mSpVehicle.getSelectedItem().toString().trim();

                if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CRM)) {
                    Log.d("typesds", "" + type);
                    if (!assignedName.equals(getString(R.string.select_corporate)) && !assignedName.equals(getString(R.string.select_dealer))) {
                        validateValues(0, qty, varient, color, remark);

                    } else {
                        MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Assign To " + assingedTo);
                    }
                } else {
                    validateValues(0, qty, varient, color, remark);
                }


            }
        });

        mBtnPass.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view_btn) {
                //indentModels.clear();
                /*for (int ll = 0; ll < mAddContainer.getChildCount(); ll++) {
                    try {
                        View childView = mAddContainer.getChildAt(ll);
                        RadioGroup typOfIndent = childView.findViewById(R.id.rg_indent_type);
                        int select = typOfIndent.getCheckedRadioButtonId();
                        RadioButton radioButton = view.findViewById(select);
                        final String type = radioButton.getText().toString();
                        CustomTextEditView spareEdt = childView.findViewById(R.id.in_edt_spares);
                        CustomTextEditView vehicleEdt = childView.findViewById(R.id.in_edt_vehicle);
                        CustomTextEditView qtyEdt = childView.findViewById(R.id.in_edt_qty);
                        CustomTextEditView mVehicleVarient = childView.findViewById(R.id.indent_model_varient);
                        CustomTextEditView mVehicleColor = childView.findViewById(R.id.edt_indent_color);
//                    mVehicleColor.setTypeface(MyUtils.setType(getActivity(), "fonts/Lato-Medium.ttf"));
                        CustomTextEditView mRemarks = childView.findViewById(R.id.indent_remarks);
                        IndentModel indentModel = new IndentModel(type, vehicleEdt.getText().toString(), spareEdt.getText().toString(), qtyEdt.getText().toString(), mVehicleVarient.getText().toString().trim(), mVehicleColor.getText().toString().trim(), mRemarks.getText().toString().trim(), assigneId, assignedName, assingedTo);
                        indentModels.add(indentModel);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }*/


                int i = mRGIndentType.getCheckedRadioButtonId();
                if (i != -1) {
                    RadioButton radioButton = view.findViewById(i);
                    final String type = radioButton.getText().toString();
                    final String qty = mEdtQty.getText().toString().trim();

                    final String varient = mModelVarient.getText().toString().trim();
                    final String colors = edtmModelColor.getText().toString().trim();
                    final String remarks = mModelRemark.getText().toString().trim();


                    if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CRM)) {
                        if (!assignedName.equals(getString(R.string.select_corporate)) && !assignedName.equals(getString(R.string.select_dealer))) {
                            //showDialogForComfirmation(type, vehicle, spare, qty, "", "", remarks, assigneId, assignedName, assingedTo);
                            validateValues(1, qty, varient, colors, remarks);
                        } else {
                            MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Assign To " + assingedTo);
                        }
                    } else {
                        validateValues(1, qty, varient, colors, remarks);
                        //showDialogForComfirmation(type, vehicle, spare, qty, "", "", remarks, assigneId, assignedName, assingedTo);
                    }


                   /* if (type.equals(getActivity().getString(R.string.spares))) {

                        if (!vehicle.toLowerCase().equals(getString(R.string.select_vehicle).toLowerCase())) {
                            if (!spare.toLowerCase().equals(getString(R.string.select_spare).toLowerCase())) {
                                if (!qty.isEmpty()) {
                                    if (Integer.parseInt(qty) >= 1) {
                                        if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, remarks, "Remark can't be empty")) {
                                            if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CRM)) {
                                                if (!assignedName.equals(getString(R.string.select_corporate)) && !assignedName.equals(getString(R.string.select_dealer))) {
                                                    showDialogForComfirmation(type, vehicle, spare, qty, "", "", remarks, assigneId, assignedName, assingedTo);
                                                } else {
                                                    MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Assign To " + assingedTo);
                                                }
                                            } else {
                                                showDialogForComfirmation(type, vehicle, spare, qty, "", "", remarks, assigneId, assignedName, assingedTo);
                                            }

                                        }
                                    } else {
                                        MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.enter_valid_qty));
                                    }
                                } else {
                                    MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.no_of_qty));
                                }
                            } else {
                                MessageUtils.showSnackBar(getActivity(), mSnackView, getActivity().getString(R.string.select_spare));
                            }
                        } else {
                            MessageUtils.showSnackBar(getActivity(), mSnackView, getActivity().getString(R.string.select_vehicle));
                        }
                    } else {
                        if (!vehicle.toLowerCase().equals(getString(R.string.select_vehicle).toLowerCase())) {
                            if (!qty.isEmpty()) {
                                if (Integer.parseInt(qty) >= 1) {
                                    if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, varient, "Model Varient can't be empty"))
                                        if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, colors, "Model Color can't be empty"))
                                            if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, remarks, "Remark can't be empty")) {
                                                if (LocalSession.getUserInfo(getActivity(), KEY_USER_TYPE).equals(T_CRM)) {
                                                    if (!assignedName.equals(getString(R.string.select_corporate)) && !assignedName.equals(getString(R.string.select_dealer))) {
                                                        showDialogForComfirmation(type, vehicle, spare, qty, "", "", remarks, assigneId, assignedName, assingedTo);
                                                    } else {
                                                        MessageUtils.showSnackBar(getActivity(), mSnackView, "Select Assign To " + assingedTo);
                                                    }
                                                } else {
                                                    showDialogForComfirmation(type, vehicle, spare, qty, "", "", remarks, assigneId, assignedName, assingedTo);
                                                }
                                            }
                                } else {
                                    MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.enter_valid_qty));
                                }
                            } else {
                                MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.no_of_qty));
                            }
                        } else {
                            MessageUtils.showSnackBar(getActivity(), mSnackView, getActivity().getString(R.string.select_vehicle));
                        }
                    }*/
                } else {

                    MessageUtils.showSnackBar(getActivity(), mSnackView, getActivity().getString(R.string.select_indent));
                }
                //}

            }
        });
        mRGIndentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                RadioButton radioSexButton = view.findViewById(i);
                type = radioSexButton.getText().toString();
                mLtyBaseLayout.setVisibility(View.VISIBLE);
                mRSparesLay.setVisibility(GONE);
                System.out.println("  checking Type vehicle or b \t" + type);
            /*    mEdtQty.setText("");
                edtmModelColor.setText("");
                mModelRemark.setText("");
                mModelVarient.setText("");*/
                if (type.endsWith(getString(R.string.spares))) {
                    mRSparesLay.setVisibility(View.VISIBLE);
                    mRVehicleLay.setVisibility(View.VISIBLE);
                    mModelVihcle.setVisibility(View.VISIBLE);
                    if (vehicle != getString(R.string.select_vehicle)) {
//                        requestSparesFromVehicle();
        /*                ArrayList<String> spi = new ArrayList<>();
                        spi.add("Select Your Vehicle");

                        mSpVehicle.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, vehicleNameArraylist));
                        //   mVarientSp.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, spi));
                        // mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, spi));

                        //CallApiVehicle();*/
                    }

                } else {
                    if (vehicle != getString(R.string.select_vehicle)) {
//
                    }
                    mRSparesLay.setVisibility(GONE);
                    mRVehicleLay.setVisibility(View.VISIBLE);
                    mModelVihcle.setVisibility(View.VISIBLE);

                }
                if (type.equals("Spares")) {
                    mRSparesLay.setVisibility(View.VISIBLE);
                    mSpSpares.setVisibility(View.VISIBLE);
                    mEdtSpares.setVisibility(View.VISIBLE);
                } else {
                    mSpSpares.setVisibility(GONE);
                    mEdtSpares.setVisibility(GONE);
                    mRSparesLay.setVisibility(GONE);
                }
            }


        });

        mSpVehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                vehicle = mSpVehicle.getSelectedItem().toString();
                Log.d("vehiclesds", "" + vehicle);
                /*if (!vehicle.isEmpty()) {
                    mEdtVehicle.setText("" + vehicle);
                    if (vehicle != getString(R.string.select_vehicle)) {
//                        requestSparesFromVehicle();
//                        getModelVariantFromServer();
                    getModelColorFromServer();
                    }
                } else {
                    mEdtVehicle.setText(" ");
                }*/
                if (mSpVehicle.getSelectedItem().toString().equals(getString(R.string.select_vehicle))) {
                    mEdtVehicle.setText(" ");
                    mModelcolorLayout.setVisibility(GONE);
                } else {
                    mModelcolorLayout.setVisibility(View.VISIBLE);
                    mEdtVehicle.setText(mSpVehicle.getSelectedItem().toString());
                    getModelColorFromServer();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

       /* mVarientSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                varientType = mVarientSp.getSelectedItem().toString();
                if (varientType.equals(getResources().getString(R.string.select_variant))) {
                    mModelVarient.setText(varientType);
                } else {
                    *
                     *

                    mModelVarient.setText(varientType);
                    ArrayList<String> list = new ArrayList<>();
                    mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, list));
                    getcolorandSpareFromServer();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        spIndentColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                color = spIndentColor.getSelectedItem().toString();
                edtmModelColor.setText(color);
                if (color.equals(getString(R.string.select_model_color)
                ) || color.equals(getString(R.string.color_nt_avl))) {
                    edtmModelColor.setText("-");
                } else {
                    edtmModelColor.setText(spIndentColor.getSelectedItem().toString());
                    getcolorFromSpareFromServer();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSpSpares.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                spare = mSpSpares.getSelectedItem().toString();
                Log.d("sparesdsd", "" + spare);
                if (!spare.equals("Select Your Spare")) {
                    mEdtSpares.setText(vehicle);
                } else {
                    mEdtSpares.setText(" ");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void validateValues(int views, String qty, String varient, String color, String remark) {
        Log.d("type", "type " + type);
        if (type.equals(getActivity().getString(R.string.spares))) {
            if (!vehicle.equals(getActivity().getString(R.string.select_vehicle))) {
                if (!color.equals("-")) {
                    if (!spare.equals("Select SpareName")) {
                        if (!qty.isEmpty()) {
                            if (Integer.parseInt(qty) >= 1) {
                                if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, remark, "Remark can't be empty")) {
                                    if (views == 0) {
                                        onAddCreateView(type, vehicle, spare, qty, varient, color, remark, assignedName);
                                    } else {
                                        showDialogForComfirmation(type, vehicle, spare, qty, varient, color, remark, assigneId, assignedName, assingedTo);
                                    }
                                }

                            }
                        } else {
                            MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.no_of_qty));
                        }
                    } else {
                        MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.select_spare));
                    }
                } else {
                    MessageUtils.showSnackBar(getActivity(), mSnackView, getActivity().getString(R.string.select_color
                    ));
                }
            } else {
                MessageUtils.showSnackBar(getActivity(), mSnackView, getActivity().getString(R.string.select_vehicle));
            }
        } else {
            if (!vehicle.equals(getActivity().getString(R.string.select_vehicle))) {
                if (!varient.equals(getString(R.string.model_variant_small))) {
                    if (!qty.isEmpty()) {
                        if (Integer.parseInt(qty) >= 1) {
                            if (!color.equals("-")) {// (!MyUtils.isEmptySnackView(getActivity(), mSnackView, serviceType, "Color can't be empty"))
                                if (!MyUtils.isEmptySnackView(getActivity(), mSnackView, remark, "Remark can't be empty")) {
                                    //if (!serviceType.equals("-")) {
                                    if (views == 0) {
                                        onAddCreateView(type, vehicle, "", qty, varient, color, remark, assignedName);
                                    } else {
                                        showDialogForComfirmation(type, vehicle, "", qty, varient, color, remark, assigneId, assignedName, assingedTo);
                                    }
                                    /*} else {
                                        MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.enter_valid_color));
                                    }*/

                                }
                            } else {
                                MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.enter_valid_color));
                            }
                        } else {
                            MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.enter_valid_qty));
                        }
                    } else {
                        MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.no_of_qty));
                    }
                } else {
                    MessageUtils.showSnackBar(getActivity(), mSnackView, getActivity().getString(R.string.model_variant_small));
                }
            } else {
                MessageUtils.showSnackBar(getActivity(), mSnackView, getActivity().getString(R.string.select_vehicle));
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showDialogForComfirmation(String type, String vehicleSelection, String sparesSelection, String qty, String varient, String color, String remark, final String request_to, String assignName, String assignTo) {
        IndentModel indentModel = new IndentModel(type, vehicleSelection, sparesSelection, qty, varient, color, remark, request_to, assignName, assignTo);
        indentModels.add(indentModel);
        final Dialog dialogIndent = new Dialog(getActivity());
        dialogIndent.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogIndent.setContentView(R.layout.dialog_indent_details);
        dialogIndent.setCancelable(false);
        dialogIndent.setTitle(null);
        Objects.requireNonNull(dialogIndent.getWindow()).setWindowAnimations(R.style.grow);

        ListView listView = dialogIndent.findViewById(R.id.indent_list_vew);
        listView.setAdapter(new DialogIndentDetails(getActivity(), indentModels));
        CustomButton btnSubmit = dialogIndent.findViewById(R.id.ind_di_submit);
        CustomButton btnEdit = dialogIndent.findViewById(R.id.ind_di_edit);
        dialogIndent.show();
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogIndent.dismiss();
                indentModels.remove(indentModels.size() - 1);


            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogIndent.dismiss();

                indentModels = new ArrayList<>(indentModels);

                HashMap<String, Object> map = new HashMap<>();

                HashMap<String, Object> mapSpare = new HashMap<>();

                mapSpare.put("spare", indentModels);

                map.put("sparelists", mapSpare);

                map.put("request_to", "" + request_to);
                map.put("mobile", LocalSession.getUserInfo(getActivity(), KEY_MOBILE));
                map.put("date_time", DateUtils.getCurrentDate("yyyy-MM-dd hh:mm"));
                map.put("user_id", LocalSession.getUserInfo(getActivity(), KEY_ID));

//                onRequestSent(map);


                if (utilsCallApi.isNetworkConnected()) {
                    dialog = MessageUtils.showDialog(getActivity());
                    utilsCallApi.callApi("addindent", "POST", map, 0);
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getLoadInitialValues() {
        dialog = MessageUtils.showDialog(getActivity());
        HashMap<String, String> stringHashMap = new HashMap<>();
        final Call<IndentVehicleRequestModel> indentCall = retrofitService.onIndentRequest(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), stringHashMap);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                dialogInterface.dismiss();
                indentCall.cancel();
                return false;
            }
        });

        indentCall.enqueue(new Callback<IndentVehicleRequestModel>() {
            @Override
            public void onResponse(Call<IndentVehicleRequestModel> call, Response<IndentVehicleRequestModel> response) {
                MessageUtils.dismissDialog(dialog);

                if (response.isSuccessful()) {
                    IndentVehicleRequestModel indentVehicleRequestModel = response.body();
                    if (indentVehicleRequestModel != null) {
                        if (indentVehicleRequestModel.success) {
                            ArrayList<String> objects = new ArrayList<>();
                            noVehicle = (indentVehicleRequestModel.data.size() == 0) ? noVehicle : getString(R.string.select_vehicle);
                            objects.add(noVehicle);
                            for (DataItem dataItem : indentVehicleRequestModel.data) {
                                objects.add(dataItem.vehicleModel);
                            }
                            mSpVehicle.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, objects));
                            userCorporate.clear();
                            userDealer.clear();
                            userCorporatePojo.clear();
                            userDealerPojo.clear();
                            for (UserIntentModel userIntentModel : indentVehicleRequestModel.users) {
                                Log.d("UserTtpe", "" + userIntentModel.getUserType() + "   T_DEALER " + T_DEALER);
                                if (String.valueOf(userIntentModel.getUserType()).equals(T_DEALER)) {
                                    userDealer.add(userIntentModel.getName());
                                    IndentDealer indentDealer = new IndentDealer(userIntentModel.getName(), userIntentModel.getUserType(), userIntentModel.getAddress(), userIntentModel.getCreatedAt(),
                                            userIntentModel.getEmail(), userIntentModel.getId(), userIntentModel.getMobile(), userIntentModel.getProfilePhoto(), userIntentModel.getUsername(),
                                            userIntentModel.getUpdatedAt());
                                    userDealerPojo.add(indentDealer);
                                } else {
                                    userCorporate.add(userIntentModel.getName());
                                    IndentCorporate indentCorporate = new IndentCorporate(userIntentModel.getName(), userIntentModel.getUserType(), userIntentModel.getAddress(), userIntentModel.getCreatedAt(),
                                            userIntentModel.getEmail(), userIntentModel.getId(), userIntentModel.getMobile(), userIntentModel.getProfilePhoto(), userIntentModel.getUsername(),
                                            userIntentModel.getUpdatedAt());
                                    userCorporatePojo.add(indentCorporate);
                                }
                            }

                            userDealer.add(0, (userDealer.size() == 0) ? "No Dealers" : "Select Dealer");
                            userCorporate.add(0, (userCorporate.size() == 0) ? "No Corporates" : "Select Corporate");

                            userCorporatePojo.add(0, new IndentCorporate("", initial, "", "", "", initial, "", "", "", ""));
                            userDealerPojo.add(0, new IndentDealer("", initial, "", "", "", initial, "", "", "", ""));

                            int selectId = mRGdealerORCorporate.getCheckedRadioButtonId();
                            Log.d("userDealer", "userDealer " + userDealer.size() + " uscor " + userCorporate.size() + " pjode " + userDealerPojo.size() + " copojo " + userCorporatePojo.size());

                            if (selectId != -1) {

                               /* Log.d("assingedTosdsd", "" + assingedTo);
                                RadioButton radioButton = mRGdealerORCorporate.findViewById(selectId);
                                assingedTo = radioButton.getText().toString();*/
                                Log.d("assingedTosdsd_Afters", "" + assingedTo);
                                if (assingedTo.equals(getString(R.string.dealer))) {
                                    mAssignedSP.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, userDealer));
                                } else {
                                    mAssignedSP.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, userCorporate));
                                }
                            }


                        } else {
                            MessageUtils.showSnackBar(getActivity(), mSnackView, indentVehicleRequestModel.message);
                        }
                    } else {
                        MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.api_response_not_found));
                    }
                } else {
                    MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                }
            }

            @Override
            public void onFailure(Call<IndentVehicleRequestModel> call, Throwable t) {
                MessageUtils.dismissDialog(dialog);
                MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void onAddCreateView(String type, String vehicleSelection, String sparesSelection, String qty, String varient, String color, String remark, String assignedName) {
        Log.d("remarksdsds", "" + remark);

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.add_indent, null);
        addView.setBackground(getActivity().getResources().getDrawable(R.drawable.normal_ripple));

        IndentModel indentModel = new IndentModel(type, vehicleSelection, sparesSelection, qty, varient, color, remark, assigneId, assignedName, assingedTo);
        indentModels.add(indentModel);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(8, 8, 8, 8);
        addView.setLayoutParams(lp);
        addView.setElevation(8);
        final CustomTextEditView mVehicle = addView.findViewById(R.id.in_edt_vehicle);
        final CustomTextEditView mSpares = addView.findViewById(R.id.in_edt_spares);
        final CustomTextEditView mQty = addView.findViewById(R.id.in_edt_qty);

       /* final CustomTextEditView mVehicleVarient = addView.findViewById(R.id.indent_model_varient);
        final RelativeLayout mModelVariantLay = addView.findViewById(R.id.ind_model_layout);*/
        final CustomTextEditView mVehicleColor = addView.findViewById(R.id.in_edt_color);/**/
//         TextInputLayout layVehicleColor = addView.findViewById(R.id.ind_colour);
        Spinner spVehicleColor = addView.findViewById(R.id.ind_sp_color);
        RelativeLayout LayColor = addView.findViewById(R.id.l_color);
        spVehicleColor.setVisibility(GONE);
//        /mVehicleColor.setTypeface(MyUtils.setType(getActivity(), "fonts/Lato-Medium.ttf"));
        final CustomTextEditView mRemarks = addView.findViewById(R.id.indent_remarks);
//        final LinearLayout mModelLayout = addView.findViewById(R.id.model_vehicle);

//        mVehicleVarient.setTextColor(getContext().getResources().getServiceType(R.serviceType.black));

        Spinner spare = addView.findViewById(R.id.ind_sp_spares);
        Spinner vehicle = addView.findViewById(R.id.ind_sp_vehicle);
        vehicle.setVisibility(GONE);
        spare.setVisibility(GONE);

        RelativeLayout mVehicleLay = addView.findViewById(R.id.l_vehicle);
        RelativeLayout mSparesLay = addView.findViewById(R.id.l_spares);
        final RadioGroup rgIndentType = addView.findViewById(R.id.rg_indent_type);
        RadioButton rbSpare = addView.findViewById(R.id.rb_spares);
        RadioButton rbOrder = addView.findViewById(R.id.rb_new_order);

        int i = mRGIndentType.getCheckedRadioButtonId();
        rgIndentType.setClickable(false);

        rbOrder.setClickable(false);
        rbOrder.setFocusable(false);
        rbSpare.setClickable(false);
        rbSpare.setFocusable(false);

        if (i != -1) {
            RadioButton radioSexButton = addView.findViewById(i);
            type = radioSexButton.getText().toString();
        }

        if (type.equals(getActivity().getString(R.string.spares))) {
            mVehicleLay.setVisibility(View.VISIBLE);
            mSparesLay.setVisibility(View.VISIBLE);

            rbSpare.setChecked(true);
            rbOrder.setChecked(false);
        } else {
//            mModelLayout.setVisibility(View.VISIBLE);
            mVehicleLay.setVisibility(View.VISIBLE);
            mSparesLay.setVisibility(View.GONE);
            rbSpare.setChecked(false);
            rbOrder.setChecked(true);
        }




       /* mQty.setFocusableInTouchMode(false);
        mQty.setLongClickable(false);
        mQty.setClickable(false);
        mVehicle.setClickable(false);
        mSpares.setClickable(false);*/

        MyUtils.focusable(mSpares, false);
        MyUtils.focusable(mQty, false);
        MyUtils.focusable(mVehicle, false);

//        MyUtils.focusable(mVehicleVarient, false);
        MyUtils.focusable(mVehicleColor, false);
        MyUtils.focusable(mRemarks, false);

        ImageButton mAdd = addView.findViewById(R.id.item_add_reomve);
        mAdd.setVisibility(GONE);
        ImageButton mRemove = addView.findViewById(R.id.item_reomve);
        mRemove.setVisibility(View.VISIBLE);
        mQty.setText(qty);

        mVehicle.setText(vehicleSelection);
        mSpares.setText(sparesSelection);
        mVehicleColor.setText(color);
        mVehicleColor.setTextColor(Color.parseColor("#000000"));
        /*if (varient != null && !varient.isEmpty()) {
            mModelVariantLay.setVisibility(View.VISIBLE);
            mVehicleVarient.setText(varient);
        } else {
            if(varient.equals("Select Model Variant")){
                mModelVariantLay.setVisibility(View.GONE);
                mVehicleVarient.setVisibility(View.GONE);
            }else{
                mModelVariantLay.setVisibility(View.VISIBLE);
                mVehicleVarient.setText(varient);
            }

        }*/


        mRemarks.setText(remark);


        mVehicle.setTextColor(getActivity().getResources().getColor(R.color.black));
        mSpares.setTextColor(getActivity().getResources().getColor(R.color.black));
        mRemove.setImageResource(R.drawable.ic_remove_circle);

        mEdtQty.setText("");
        mModelRemark.setText("");
        //mModelVarient.setText("");
        if (type.equals(getActivity().getString(R.string.spares))) {

            mRBSapres.setChecked(false);
            mRBNewOrderVeh.setChecked(true);
        } else {
            mRBSapres.setChecked(true);
            mRBNewOrderVeh.setChecked(false);
        }
        mSpVehicle.setSelection(0);

        mLtyBaseLayout.setVisibility(View.VISIBLE);

        mRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               /*
                RadioButton radioButton = addView.findViewById(rgIndentType.getCheckedRadioButtonId());
                String type = radioButton.getText().toString().trim();
                String vehicleSelection = mVehicle.getText().toString().trim();//mSpVehicle.getSelectedItem().toString();
                String sparesSelection = mSpares.getText().toString().trim();
                String qty = mQty.getText().toString().trim();*/

                int position = mAddContainer.indexOfChild(addView);
                //IndentModel indentModel = new IndentModel(type, vehicleSelection, sparesSelection, qty);s
                try {
                    indentModels.remove(position);
                } catch (NullPointerException e) {

                    e.printStackTrace();
                }


                ((LinearLayout) addView.getParent()).removeView(addView);

                //Log.d("indentModels_afters", "" + indentModels.size() + "  " + indentModels);

            }
        });

        mAddContainer.addView(addView);
    }

    private void onRequestSent(HashMap<String, Object> map) {

        dialog = MessageUtils.showDialog(getActivity());
        final Call<IndentModelSuccessResponse> indentModelSuccessResponseCall = retrofitService.onIndentRaise(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), map);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                MessageUtils.dismissDialog(dialog);
                indentModelSuccessResponseCall.cancel();

                return false;
            }
        });
        indentModelSuccessResponseCall.enqueue(new Callback<IndentModelSuccessResponse>() {
            @Override
            public void onResponse(Call<IndentModelSuccessResponse> call, Response<IndentModelSuccessResponse> response) {
                MessageUtils.dismissDialog(dialog);
                if (response.isSuccessful()) {
                    IndentModelSuccessResponse indentModelResponse = response.body();
                    if (indentModelResponse != null && indentModelResponse.success) {
                        /*mSpVehicle.setSelection(0);
                        mSpSpares.setSelection(0);
                        mEdtQty.setText("");
                        edtmModelColor.setText("");
                        mModelRemark.setText("");
                        mModelVarient.setText("");
                        mRBNewOrderVeh.setChecked(true);
                        mRBSapres.setChecked(false);
                        mRBNewOrderVeh.setChecked(true);
*/
                        MessageUtils.showSnackBar(getActivity(), mSnackView, indentModelResponse != null ? "" + indentModelResponse.message : getString(R.string.api_response_not_found));

                        MyUtils.passFragmentWithoutBackStack(getActivity(), new IndentDetails());
                    } else {
                        MessageUtils.showSnackBar(getActivity(), mSnackView, indentModelResponse != null ? "" + indentModelResponse.message : getString(R.string.api_response_not_found));
                    }
                } else {

                    // Log.d("Failure_message", "" + type + "  vehicle " + vehicle + "  spare " + spare + "  qty " + mEdtQty.getText().toString());
                    //IndentModel indentModel = new IndentModel(type, vehicle, spare, mEdtQty.getText().toString());
                    try {
                        indentModels.remove(indentModels.size() - 1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                }
            }

            @Override
            public void onFailure(Call<IndentModelSuccessResponse> call, Throwable t) {
                MessageUtils.dismissDialog(dialog);

                // IndentModel indentModel = new IndentModel(type, vehicle, spare, mEdtQty.getText().toString());
                try {
                    indentModels.remove(indentModels.size() - 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Log.d("indentModelsssss", "" + indentModels.size());
                MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
            }
        });
    }

    private void requestSparesFromVehicle() {
        //if (type.equals(getActivity().getResources().getString(R.string.spares)) && !vehicle.toLowerCase().equals(getString(R.string.select_vehicle).toLowerCase())) {
        dialog = MessageUtils.showDialog(getActivity());
        HashMap<String, String> map = new HashMap<>();
        map.put("vehicle_model", vehicle);
        final Call<ModelVehicleModelBaseSpares> modelVehicleModelBaseSparesCall = retrofitService.onGetSpares(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), map);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                MessageUtils.dismissDialog(dialog);
                modelVehicleModelBaseSparesCall.cancel();
                return false;
            }
        });
        modelVehicleModelBaseSparesCall.enqueue(new Callback<ModelVehicleModelBaseSpares>() {
            @Override
            public void onResponse(Call<ModelVehicleModelBaseSpares> call, Response<ModelVehicleModelBaseSpares> response) {
                MessageUtils.dismissDialog(dialog);
                if (response.isSuccessful()) {
                    ModelVehicleModelBaseSpares modelVehicleModelBaseSpares = response.body();
                    if (modelVehicleModelBaseSpares != null && modelVehicleModelBaseSpares.success) {
                        ArrayList<String> spareList = new ArrayList<>();

                        if (modelVehicleModelBaseSpares.data != null && modelVehicleModelBaseSpares.data.sparesLists.size() >= 1) {
                            spareList.add(getString(R.string.select_spare));
                            for (ModelVehicleModelBaseSpares.SparesList sparesList : modelVehicleModelBaseSpares.data.sparesLists) {
                                spareList.add(sparesList.spares);
                                SparesGetter sparesGetter = new SparesGetter(sparesList.id, sparesList.spares, sparesList.spare_id, sparesList.spare_code, sparesList.vehicle_model_id, sparesList.model_varient_id);
                                sparesGetters.add(sparesGetter);
                            }
                            if (spareList.size() != 0) {
                                SparesGetter sparesGetter = new SparesGetter();
                                spareList.add(0, "Select Spare");
                                sparesGetters.add(0, sparesGetter);
                            }
                            mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, spareList));


                        }


                      /*  noVarient = modelVehicleModelBaseSpares.varients.size() == 0 ? noVarient : getString(R.string.model_varient_small);
                        ArrayList<String> listOfVarient = new ArrayList<>();
                        listOfVarient.add(noVarient);

                        for (ModelVehicleModelBaseSpares.Varient varient : modelVehicleModelBaseSpares.varients) {
                            listOfVarient.add(varient.varient);

                        }

                        Log.d("listOfVarient", "" + listOfVarient.size());
                        mVarientSp.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, listOfVarient));*/

                    } else {
                        MessageUtils.showSnackBar(getActivity(), mSnackView, getString(R.string.api_response_not_found));
                    }
                } else {
                    //Log.d("NewIndenfFail", "" + response.errorBody());
                    MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                }
            }

            @Override
            public void onFailure(Call<ModelVehicleModelBaseSpares> call, Throwable t) {
                MessageUtils.dismissDialog(dialog);
                MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
            }
        });
    }
    // }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if(MessageUtils.snackbar!=null){
            MessageUtils.snackbar.dismiss();
        }
    }

    @Override
    public void apiCallBackOverRideMethod(JSONObject response, int position, boolean success, String message, String apiCallPath) throws JSONException, IOException {
        MessageUtils.dismissDialog(dialog);
        if (apiCallPath.equals("model_varient")) {
            if (success) {
                if (response != null) {
                    System.out.println(" response  models " + response.toString());
                    System.out.println("Variant" + response.getString("varient"));
                    modelvarient.clear();
                    mVarientSp.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, modelvarient));
                    if (response.getString("varient").equals("Available")) {
                        ArrayList<VariantListPojos> variantListPojos = new ArrayList<>();

                        variantListPojos = new Gson().fromJson(response.getJSONArray("model_varient").toString(), new TypeToken<ArrayList<VariantListPojos>>() {
                        }.getType());

                        modelvarient.add(getResources().getString(R.string.select_variant));
                        for (VariantListPojos variantListPojos1 : variantListPojos) {
                            modelvarient.add(variantListPojos1.getModelVarient());
                            int i = 0;
                            System.out.println("Model Varient" + modelvarient.get(i));
                            i++;
                        }
                        mVarientSp.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, modelvarient));
                        mModelVarient.setText("");
                        ArrayList<String> list = new ArrayList<>();
                        mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, list));
                        /**
                         * set visible to  model Variant
                         */
                       /* mVarientSp.setVisibility(View.VISIBLE);
                        lModelVariant.setVisibility(View.VISIBLE);
                        mModelVarient.setVisibility(View.VISIBLE);*/
                        /**
                         * set GOne to  Spare spinner and spare Edit text
                         */
                        mRSparesLay.setVisibility(GONE);
                        mSpSpares.setVisibility(GONE);
                        mEdtSpares.setVisibility(GONE);
//                mSpVehicle.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, vehicleNameArraylist));

                    } else {
                        /**
                         * Spare without  vehicle variant
                         * serviceType also without  variant
                         */
                        ArrayList<SparePojos> sparePojosArrayList;
                        spareNameArraylist.clear();
                        mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, spareNameArraylist));
                        spareNameArraylist.add(getResources().getString(R.string.select_spare));

                        sparePojosArrayList = new Gson().fromJson(response.getJSONArray("spares").toString(), new TypeToken<ArrayList<SparePojos>>() {
                        }.getType());
                        for (SparePojos sparePojos : sparePojosArrayList) {
                            spareNameArraylist.add(sparePojos.getSpares());
                        }

                        if (type.equals("Spares")) {
                            mRSparesLay.setVisibility(View.VISIBLE);
                            mSpSpares.setVisibility(View.VISIBLE);
                            mEdtSpares.setVisibility(View.VISIBLE);
                        } else {
                            mSpSpares.setVisibility(GONE);
                            mEdtSpares.setVisibility(GONE);
                            mRSparesLay.setVisibility(GONE);
                        }
                        mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, spareNameArraylist));

                        /**
                         * get serviceType  from server
                         */

                        ArrayList<String> colorList = null;
//                        colorList.clear();
//                        colorList.add(getResources().getString(R.string.));
                        colorList = new Gson().fromJson(response.getJSONArray("colors").toString(), new TypeToken<ArrayList<String>>() {
                        }.getType());
                        System.out.println("colors:" + colorList.toString());
                        spIndentColor.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, colorList));
                        mVarientSp.setVisibility(GONE);
                        mModelVarient.setVisibility(GONE);
                        lModelVariant.setVisibility(GONE);

                    }
                }

            }
        } else if (apiCallPath.equals("color_spares")) {
            /**
             * get serviceType and Spares from server
             * using interface class utilsCallBack
             */

            if (success) {
                if (response != null) {
                    ArrayList<String> color = new Gson().fromJson(response.getJSONArray("colors").toString(), new TypeToken<ArrayList<String>>() {
                    }.getType());
                    spIndentColor.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, color));
                    ArrayList<SparePojos> spares = new Gson().fromJson(response.getJSONArray("spares").toString(), new TypeToken<ArrayList<SparePojos>>() {
                    }.getType());
                    ArrayList<String> spareArrayList = new ArrayList<>();
                    spareArrayList.add(getResources().getString(R.string.select_spare));
                    for (SparePojos sparePojos : spares) {
                        spareArrayList.add(sparePojos.getSpares());
                    }

                    mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, spareArrayList));
                    //mSpSpares.setVisibility(View.VISIBLE);
                    if (type.equals("Spares")) {
                        mRSparesLay.setVisibility(View.VISIBLE);
                        mSpSpares.setVisibility(View.VISIBLE);
                    } else {
                        mSpSpares.setVisibility(GONE);
                        mRSparesLay.setVisibility(GONE);
                    }
                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        } else if (apiCallPath.equals("addindent")) {
            if (success) {
                MyUtils.passFragmentBackStack(getActivity(), new IndentHome());
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }

        }
        /**
         * new requirement changes
         * get serviceType list from particular  corresponding vehicle model
         */
        else if (apiCallPath.equals("vehiclecolorlist")) {
            if (response != null) {
                colorListPojosArrayList = new Gson().fromJson(response.getJSONArray("colors").toString(), new TypeToken<ArrayList<ColorListPojos>>() {
                }.getType());
                ArrayList<String> colorList = new ArrayList<>();
                colorList.add(getString(R.string.select_model_color));
                for (ColorListPojos colorListPojos : colorListPojosArrayList) {
                    if (colorListPojos.getColor() == null || colorListPojos.getColor().isEmpty() || colorListPojos.getColor().length() == 0) {
                        Log.d("info for serviceType is empty", "" + colorList.size());
                    } else {
                        colorList.add(colorListPojos.getColor());
                    }

                }
//                Toast.makeText(getActivity(), ""+colorList.size(), Toast.LENGTH_SHORT).show();
                if (colorList.size() == 1) {
                    colorList.clear();
                    colorList.add("Colors Not Available");
//                    Toast.makeText(getActivity(), ""+colorList.size(), Toast.LENGTH_SHORT).show();
                }
                if(spIndentColor!=null){
                    spIndentColor.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, colorList));
                }

            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        } else if (apiCallPath.equals("spareslist")) {
            if (success) {
                sparePojosArrayList = new Gson().fromJson(response.getJSONArray("spareslist").toString(), new TypeToken<ArrayList<SparePojos>>() {
                }.getType());
                ArrayList<String> spareList = new ArrayList<>();
                spareList.add("Select SpareName");
                for (SparePojos sparePojos : sparePojosArrayList) {
                    spareList.add(sparePojos.getSpares());
                }
                mSpSpares.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, spareList));
            } else {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        }
    }


    class DialogIndentDetails extends BaseAdapter {
        private final FragmentActivity activity;
        private final ArrayList<IndentModel> indentModels;

        DialogIndentDetails(FragmentActivity activity, ArrayList<IndentModel> indentModels) {
            this.activity = activity;
            this.indentModels = indentModels;
        }

        @Override
        public int getCount() {
            return indentModels.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            if (convertView == null) {

                convertView = LayoutInflater.from(activity).inflate(R.layout.add_indent, viewGroup, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            //convertView.setBackground(getActivity().getResources().getDrawable(R.drawable.normal_ripple));

            viewHolder.mVehicleVarient.setTextColor(Objects.requireNonNull(getContext()).getResources().getColor(R.color.black));

            viewHolder.mVehicle.setText(indentModels.get(i).getVehicleModel());
            if (indentModels.get(i).getSpares() != null && !indentModels.get(i).getSpares().isEmpty()) {
                viewHolder.mSpares.setText(indentModels.get(i).getSpares());
                viewHolder.mSparesLay.setVisibility(View.VISIBLE);
            } else {
                viewHolder.mSparesLay.setVisibility(View.GONE);
            }

            if (indentModels.get(i).getIndentType().equals(activity.getString(R.string.vehicle))) {
                viewHolder.rbSpare.setChecked(false);
                viewHolder.rbOrder.setChecked(true);
                viewHolder.mModelLayout.setVisibility(View.VISIBLE);
            } else {
                viewHolder.rbSpare.setChecked(true);
                viewHolder.rbOrder.setChecked(false);
                viewHolder.mModelLayout.setVisibility(View.GONE);
            }
            viewHolder.mSpcolor.setVisibility(GONE);

            viewHolder.mQty.setText(indentModels.get(i).getQty());
            viewHolder.mVehicleVarient.setText(indentModels.get(i).getVehicleModel());
            System.out.println("     serviceType in adapter" + indentModels.get(i).getColor());
            viewHolder.mVehicleColor.setText(indentModels.get(i).getColor());
            viewHolder.mVehicleColor.setTextColor(Color.parseColor("#000000"));
            viewHolder.mRemarks.setText(indentModels.get(i).getRemarks());

            focusable(viewHolder.mQty, false);
            focusable(viewHolder.mVehicle, false);
            focusable(viewHolder.mSpares, false);


            MyUtils.focusable(viewHolder.mVehicleVarient, false);
            MyUtils.focusable(viewHolder.mVehicleColor, false);
            MyUtils.focusable(viewHolder.mRemarks, false);

            viewHolder.add_indent_lay.setBackground(activity.getResources().getDrawable(R.drawable.normal_ripple));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            lp.setMargins(6, 10, 6, 10);
            viewHolder.add_indent_lay.setLayoutParams(lp);
            viewHolder.add_indent_lay.setPaddingRelative(24, 8, 16, 8);
            viewHolder.add_indent_lay.setElevation(8);
            return convertView;
        }

        class ViewHolder {

            CustomTextEditView mVehicle, mSpares, mQty;
            RelativeLayout mVehicleLay, mSparesLay;
            RadioGroup rgIndentType;
            RadioButton rbSpare, rbOrder;
            ImageButton item_add_reomve;
            CustomTextEditView mVehicleVarient, mRemarks;
            LinearLayout add_indent_lay, mModelLayout;
            CustomTextEditView mVehicleColor;
            Spinner mSpcolor;
            public ViewHolder(View addView) {

                mVehicle = addView.findViewById(R.id.in_edt_vehicle);
                mSpares = addView.findViewById(R.id.in_edt_spares);
                mQty = addView.findViewById(R.id.in_edt_qty);
                item_add_reomve = addView.findViewById(R.id.item_add_reomve);
                item_add_reomve.setVisibility(GONE);
                mVehicleLay = addView.findViewById(R.id.l_vehicle);
                mSparesLay = addView.findViewById(R.id.l_spares);
                mSpcolor = addView.findViewById(R.id.ind_sp_color);
                rgIndentType = addView.findViewById(R.id.rg_indent_type);
                rbSpare = addView.findViewById(R.id.rb_spares);
                rbOrder = addView.findViewById(R.id.rb_new_order);
                add_indent_lay = addView.findViewById(R.id.add_indent_lay);

                mVehicleVarient = addView.findViewById(R.id.indent_model_varient);
                mVehicleColor = addView.findViewById(R.id.in_edt_color);
//                mVehicleColor.setTypeface(MyUtils.setType(getActivity(), "fonts/Lato-Medium.ttf"));
                mRemarks = addView.findViewById(R.id.indent_remarks);
                mModelLayout = addView.findViewById(R.id.model_vehicle);
                int i = mRGIndentType.getCheckedRadioButtonId();
                rgIndentType.setClickable(false);
                rbOrder.setClickable(false);
                rbOrder.setFocusable(false);
                rbSpare.setClickable(false);
                rbSpare.setFocusable(false);

                mVehicle.setTextColor(getContext().getResources().getColor(R.color.black));
                mSpares.setTextColor(getContext().getResources().getColor(R.color.black));

                Spinner spare = addView.findViewById(R.id.ind_sp_spares);
                Spinner vehicle = addView.findViewById(R.id.ind_sp_vehicle);
                vehicle.setVisibility(GONE);
                spare.setVisibility(GONE);
            }
        }
    }

    public void CallApiVehicle() {
        if(utilsCallApi.isNetworkConnected()){
        dialog = MessageUtils.showDialog(getActivity());
        final Call<DataList> dataListCall = retrofitService.getDataList(BEARER + LocalSession.getUserInfo(getActivity(), KEY_TOKEN));
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                dialog.dismiss();
                dataListCall.cancel();
                return false;
            }
        });
        dataListCall.enqueue(new Callback<DataList>() {
            @Override
            public void onResponse(Call<DataList> call, Response<DataList> response) {
                System.out.println(" get Vehicle Models" + response.toString());
                MessageUtils.dismissDialog(dialog);
                try {
                    if (response.isSuccessful()) {
                        DataList dataList = response.body();
                        System.out.println(" DataList Date" + dataList.getData().getVehiclemodel().toString());
                        if (dataList.isSuccess()) {
                            vehicleNameArraylist.clear();
                            vehicleNameArraylist.add(getResources().getString(R.string.select_vehicle));
                            for (VehiclemodelItem object : dataList.getData().getVehiclemodel()) {
                                vehicleNameArraylist.add(object.getVehicleModel());
                            }
                            mSpVehicle.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.spinnertext, R.id.text1, vehicleNameArraylist));
                        } else {
                            Toast.makeText(getActivity(), "SpareInvoicePojos Failure", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Server Not Found", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DataList> call, Throwable t) {
                System.out.println(" NETWORKING FAILURE " + t.toString());
                MessageUtils.showSnackBar(getActivity(), mSnackView, "SERVER NOT FOUND");
                MessageUtils.dismissDialog(dialog);
                Toast.makeText(getActivity(), "Server NOT Found", Toast.LENGTH_SHORT).show();
            }
        });
        }else {
            MessageUtils.showSnackBarAction(getActivity(),snackVIew,getString(R.string.check_internet));
        }
    }

    public void getModelVariantFromServer() {
        String vehicleItem = mSpVehicle.getSelectedItem().toString();

        System.out.println("get Vehicle Model" + vehicleItem);
        HashMap<String, Object> map = new HashMap<>();
        map.put("vehicle_model", vehicleItem);
        if (utilsCallApi.isNetworkConnected()) {
            utilsCallApi.callApi("model_varient", "POST", map, 0);
        } else {

            Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_SHORT).show();
        }
    }

    public void getModelColorFromServer() {
        String vehicleItem = mSpVehicle.getSelectedItem().toString();

        System.out.println("get Vehicle Model" + vehicleItem);
        HashMap<String, Object> map = new HashMap<>();
        map.put("vehicleModel", vehicleItem);
        if (utilsCallApi.isNetworkConnected()) {
            dialog = MessageUtils.showDialog(getActivity());
            utilsCallApi.callApi("vehiclecolorlist", "POST", map, 0);
        } else {
            MessageUtils.showSnackBarAction(getActivity(),snackVIew,getString(R.string.check_internet));
//            Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_SHORT).show();
        }
    }

    public void getcolorFromSpareFromServer() {
        HashMap<String, Object> objectHashMap = new HashMap<>();
        objectHashMap.put("vehicleModel", mSpVehicle.getSelectedItem().toString());
        objectHashMap.put("color", spIndentColor.getSelectedItem().toString().trim());
        if (utilsCallApi.isNetworkConnected()) {
            dialog = MessageUtils.showDialog(getActivity());
            utilsCallApi.callApi("spareslist", "POST", objectHashMap, 0);
        } else {
            MessageUtils.showSnackBarAction(getActivity(),snackVIew,getString(R.string.check_internet));
//            Toast.makeText(getActivity(), getResources().getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
        }


    }


    public void getcolorandSpareFromServer() {
        HashMap<String, Object> objectHashMap = new HashMap<>();
        objectHashMap.put("vehicle_model", mSpVehicle.getSelectedItem().toString());
        objectHashMap.put("model_varient", mVarientSp.getSelectedItem().toString());
        if (utilsCallApi.isNetworkConnected()) {
            utilsCallApi.callApi("color_spares", "POST", objectHashMap, 0);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
        }


    }


}