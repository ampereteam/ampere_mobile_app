package com.ampere.bike.ampere.model.enquiry;

import com.google.gson.annotations.SerializedName;

public class ModelSalesInsert {

    @SerializedName("success")
    public boolean status;

    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public Object enqiryId;

}
