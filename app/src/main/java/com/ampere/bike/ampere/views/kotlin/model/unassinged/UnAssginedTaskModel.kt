package com.ampere.bike.ampere.views.kotlin.model.unassinged

data class UnAssginedTaskModel(
        val success: Boolean,
        val data: UnAssignDataModel,
        val message: String
)