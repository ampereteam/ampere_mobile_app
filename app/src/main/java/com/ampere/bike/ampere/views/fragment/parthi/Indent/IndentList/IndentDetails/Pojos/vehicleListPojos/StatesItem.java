package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.vehicleListPojos;

import com.google.gson.annotations.SerializedName;

public class StatesItem{

	@SerializedName("updated_at")
	private Object updatedAt;

	@SerializedName("state_name")
	private String stateName;

	@SerializedName("created_at")
	private Object createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("state_code")
	private String stateCode;

	@SerializedName("tin_number")
	private int tinNumber;

	public void setUpdatedAt(Object updatedAt){
		this.updatedAt = updatedAt;
	}

	public Object getUpdatedAt(){
		return updatedAt;
	}

	public void setStateName(String stateName){
		this.stateName = stateName;
	}

	public String getStateName(){
		return stateName;
	}

	public void setCreatedAt(Object createdAt){
		this.createdAt = createdAt;
	}

	public Object getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setStateCode(String stateCode){
		this.stateCode = stateCode;
	}

	public String getStateCode(){
		return stateCode;
	}

	public void setTinNumber(int tinNumber){
		this.tinNumber = tinNumber;
	}

	public int getTinNumber(){
		return tinNumber;
	}

	@Override
 	public String toString(){
		return 
			"StatesItem{" + 
			"updated_at = '" + updatedAt + '\'' + 
			",state_name = '" + stateName + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			",state_code = '" + stateCode + '\'' + 
			",tin_number = '" + tinNumber + '\'' + 
			"}";
		}
}