package com.ampere.bike.ampere.views.kotlin.fragments.crm.sales

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.interfaces.RefreshEnquiryTabs
import com.ampere.bike.ampere.model.task.EnquiryModel
import com.ampere.bike.ampere.model.task.ViewTaskModel
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.BEARER
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.fragment.enquiry.Enquiry
import com.ampere.bike.ampere.views.kotlin.fragments.ViewEventDetailed
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMActivity
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity
import com.ampere.bike.ampere.views.kotlin.fragments.model.model_crm.CRMFilterSales
import com.ampere.vehicles.R
import com.ampere.vehicles.R.string.enquiry
import customviews.CustomTextView
import kotlinx.android.synthetic.main.frag_list_view.*
import kotlinx.android.synthetic.main.item_crm.view.*
import kotlinx.android.synthetic.main.item_crm_header.*
import kotlinx.android.synthetic.main.item_crm_header.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class SalesKT : Fragment(), RefreshEnquiryTabs {

    var mSalesEnquiryValues: ArrayList<EnquiryModel>? = ArrayList();
    var fragment: SalesKT? = null
    var dialog :Dialog?=null

    override fun refreshView(enquiryModels: List<EnquiryModel>?) {

        Log.d("enquiryModels", "dsfhjd List" + enquiryModels!!.size)

        /* val List = enquiryModels
        Log.d("ArrayLIstSize",""+List.size)
        showListValues()*/
        if (isAdded) {
            crm_header.visibility = View.VISIBLE
            no_text.visibility = View.GONE
            list_proposal.visibility = View.VISIBLE
            list_proposal.layoutManager = LinearLayoutManager(context)
            try {

                Collections.sort(mSalesEnquiryValues) { crmFilterSales, crmFilterSales1 ->
                    var date1 = crmFilterSales1.assignedDate
                    var date2 = crmFilterSales.assignedDate
                    //Log.d("date2sdsdSales", "" + date2 + " date1 " + date1)
                    if (date1 != null && date2 != null) {
                        date1.compareTo(date2)
                    } else {
                        0
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            mSalesEnquiryValues = enquiryModels as ArrayList<EnquiryModel>?
            MyUtils.SALES = mSalesEnquiryValues
            MessageUtils.dismissDialog(dialog)
            adapter = ItemCRM(enquiryModels as ArrayList<EnquiryModel>, activity!!)
            list_proposal.adapter = adapter

        }

    }

    var saleKt: SalesKT? = null
    lateinit var mHeaderModel: LinearLayout
    val TOTAL_PAGES = 10
    var isLastPage = false
    var isLoading = false


    companion object {
        @SuppressLint("StaticFieldLeak")
        var adapter: ItemCRM? = null
        val PAGE_START = 1
        var currentPage = PAGE_START
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog = Dialog(activity)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        Log.d("tab_one_call", "Sales 1 " + isLoading)
        return inflater.inflate(R.layout.frag_list_view, container, false)
    }

    override fun onPause() {
        super.onPause()
        Log.d("destroyView", "pause")
    }

    override fun onStop() {
        super.onStop()
        Log.d("destroyView", "stop")
    }

    override fun onDestroy() {
        super.onDestroy()
        currentPage = 0;
        Log.d("destroyView", "111")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.d("destroyView", "00")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("tab_one_call", "Sales 2")


        saleKt = SalesKT()
        mHeaderModel = view.crm_header_lay_model
        mHeaderModel.visibility = View.VISIBLE

        crm_header_action.visibility = View.GONE

        //itemCRM = ItemCRM(SALES, activity!!, saleKt!!)
        // itemCRM = ItemCRM(activity!!)



        CRMActivity.refreshView(this)

        /*  Log.d("currentPage", "" + currentPage + "  " + TOTAL_PAGES)
          if (currentPage <= TOTAL_PAGES)
              adapter?.addLoadingFooter()
          else
              isLastPage = true

          list_proposal.addOnScrollListener(object : PaginationScrollListener(linearLayoutManager) {

              override fun loadMoreItems() {
                  Log.d("sdasds", "Whats_do_here")
                  this@SalesKT.isLoading = true;
                  currentPage += 1;
                  loadNextPage()
              }

              override fun getTotalPageCount(): Int {
                  return TOTAL_PAGES;
              }

              override fun isLastPage(): Boolean {
                  return this@SalesKT.isLastPage;
              }

              override fun isLoading(): Boolean {
                  return this@SalesKT.isLoading;
              }

          })*/
    }

    fun showListValues() {
        dialog =MessageUtils.showDialog(activity)
        //Log.d("SALESssdsd", "" + SALES.size)
        if (mSalesEnquiryValues != null) {
            //adapter?.addAll(mSalesEnquiryValues)

            Log.d("MyUtils", "" + mSalesEnquiryValues!!.size)
            var linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            if (mSalesEnquiryValues!!.size == 0) {
                no_text.text = "Sales Not found"
                no_text.visibility = View.VISIBLE
                list_proposal.visibility = View.GONE
                crm_header.visibility = View.GONE
                MessageUtils.dismissDialog(dialog)
                adapter = ItemCRM(mSalesEnquiryValues!!, activity!!)
                list_proposal.adapter = adapter
            } else {

                if (isAdded) {
                    crm_header.visibility = View.VISIBLE
                    no_text.visibility = View.GONE
                    list_proposal.visibility = View.VISIBLE
                    list_proposal.layoutManager = linearLayoutManager
                    try {
                        Collections.sort(mSalesEnquiryValues) { crmFilterSales, crmFilterSales1 ->
                            var date1 = crmFilterSales1.assignedDate
                            var date2 = crmFilterSales.assignedDate
                            //Log.d("date2sdsdSales", "" + date2 + " date1 " + date1)
                            if (date1 != null && date2 != null) {
                                date1.compareTo(date2)
                            } else {
                                0
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    MessageUtils.dismissDialog(dialog)
                    adapter = ItemCRM(mSalesEnquiryValues!!, activity!!)
                    list_proposal.adapter = adapter
                }
            }
        }


    }

    override fun onResume() {
        super.onResume()
        showListValues()
        Log.d("tab_one_call", "Sales 3")

    }

    private fun loadNextPage() {

        Log.d("isLoadingsds", "" + isLoading);
        var crmSale: ArrayList<CRMFilterSales>? = ArrayList();
        var taskHash = HashMap<String, String>()
        taskHash.put("user_name", LocalSession.getUserInfo(activity, LocalSession.KEY_USER_NAME));
        var taskModel = MyUtils.getInstance().onTaskView(BEARER + LocalSession.getUserInfo(activity, KEY_TOKEN), taskHash)

        taskModel.enqueue(object : Callback<ViewTaskModel> {
            override fun onResponse(call: Call<ViewTaskModel>, response: Response<ViewTaskModel>) {
                //dialog?.dismiss()
                var taskModel = response.body()
                if (taskModel != null) {
                    if (taskModel.success) {
                        if (taskModel.data != null) {

                            adapter?.removeLoadingFooter()
                            isLoading = false
                            for (items: EnquiryModel in taskModel.data.enquiry) {
                                var filterValue = items.enquiryFor
                                if (filterValue.equals(getString(R.string.sales))) {
                                    var models = CRMFilterSales("" + items.id, items.customerName, items.city, "" + items.mobileNo, items.email, items.leadsource, items.assignedDate,
                                            items.assignedTo, items.enquiryDate, items.enquiryFor, items.enquiryTime, items.assignedPerson, items.message, items.taskStatus, items.task,
                                            items.vehicleModel, "", false)
                                    //  Log.d("filterValue ", filterValue)
                                    crmSale?.add(models)
                                    //MyUtils.SALES.add(models)

                                }
                            }
                            isLoading = false;
                            try {
                                adapter?.addAll(taskModel.data.enquiry!!);
                            } catch (e: NullPointerException) {
                                e.printStackTrace()
                            }

                            if (currentPage <= TOTAL_PAGES) {
                                adapter?.addLoadingFooter()
                                isLastPage = false
                            } else {
                                isLastPage = true
                            }
                            /*val fragmentAdapter = CRM.MyPagerAdapter(supportFragmentManager)
                            viewpager.adapter = fragmentAdapter*/


                        } else {
                            MessageUtils.showSnackBar(activity, snack_View, taskModel.message.toString())
                        }
                    } else {
                        MessageUtils.showSnackBar(activity, snack_View, taskModel.message)
                    }
                } else {
                    MessageUtils.showSnackBar(activity, snack_View, "No data found ")
                }

            }

            override fun onFailure(call: Call<ViewTaskModel>, t: Throwable) {
                // dialog?.dismiss()

                MessageUtils.setFailureMessage(activity, snack_View, t.message)
            }
        })


    }

    fun newInstance(): Fragment? {
        Log.d("fragment_sale", "" + fragment);
        if (fragment == null) {
            fragment = SalesKT();
        }
        return fragment;
    }
}


class ItemCRM(var items: ArrayList<EnquiryModel>, val context: FragmentActivity)/*(val item: FragmentActivity?, val context: FragmentActivity, val saleKt: SalesKT) */ : RecyclerView.Adapter<HolderSales>() {

    private var isLoadingAdded = false
    private var retryPageLoad = false
    // View Types
    private val ITEM = 0
    private val LOADING = 1
    private val HERO = 2

    //var saleResult = java.util.ArrayList<CRMFilterSales>()
    //var items: ArrayList<EnquiryModel> = ArrayList()

    //  var listener: SampleInterface? = null
    fun filterList(filterdNames: ArrayList<EnquiryModel>) {
        items = filterdNames
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderSales {
        return HolderSales(LayoutInflater.from(context).inflate(R.layout.item_crm, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("MissingPermission")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: HolderSales, position: Int) {

        when (getItemViewType(position)) {

            HERO -> {

                holder.loader.visibility = View.GONE
                holder.itemLayout.visibility = View.VISIBLE

                CustomTextView.marquee(holder.itemTask)
                CustomTextView.marquee(holder.itemName)
                CustomTextView.marquee(holder.itemStatus)
                CustomTextView.marquee(holder.itemDate)


                /* if (LocalSession.getUserInfo(context, LocalSession.KEY_USER_TYPE).equals(T_DEALER)) {
              holder.itemName.visibility = View.GONE
          } else {*/
                holder.itemName.visibility = View.VISIBLE
                holder.itemName.text = items[position].customerName
                //}
                holder?.itemTask?.text = items[position].task
                Log.d("vehicelModel",""+items[position].vehicleModel)
                holder.itemModel.text = items[position].vehicleModel
                var dateNull = items[position].assignedDate
                if (dateNull != null) {
                    holder.itemDate?.text = DateUtils.convertDates(items[position].assignedDate, DateUtils.YYYY_MM_DD, DateUtils.DD_MM)
                } else {
                    holder.itemDate?.text = ""
                }
                // Log.d("namess_sales", "" + items[position].customerName);
                CustomTextView.marquee(holder.itemTask)
                CustomTextView.marquee(holder.itemName)
                CustomTextView.marquee(holder.itemStatus)
                CustomTextView.marquee(holder.itemModel)


                holder.itemStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                holder.itemStatus.isClickable = false
                holder.itemStatus.isLongClickable = false

                holder.itemAction.visibility = View.GONE
                holder.itemStatus.visibility = View.VISIBLE

                val isCheck = items[position].isChecked
                if (isCheck) {
                    holder.itemStatus.text = "" + items[position].taskStatus
                } else {
                    holder.itemStatus.text = items[position].taskStatus
                }

                val param = holder.itemLayout.layoutParams as LinearLayout.LayoutParams
                param.setMargins(8, 8, 8, 8);
                holder.itemLayout.layoutParams = param

                holder.snack_view.setOnClickListener {

                    Log.d("sdsdsdsdsd", "" + items[position].taskStatus + " task " + items[position].task+"  "+items[position].isStock_exist)
                    if (items[position].task.toLowerCase().equals("Won".toLowerCase()) ) {
                        if(items[position].isStock_exist){
                            val intent = Intent(context, CRMDetailsActivity::class.java)
                            intent.putExtra("id", "" + items[position].id)
                            intent.putExtra("user_name", "salesStock")
                            intent.putExtra("task_name", "" + context.getString(R.string.sales));
                            context.overridePendingTransition(0, 0)
//                        startActivity(context, intent, bundle)
                            context.startActivity(intent)
                        }else{
                            val intent = Intent(context, CRMDetailsActivity::class.java)
                            intent.putExtra("id", "" + items[position].id)
                            intent.putExtra("user_name", "sales")
                            intent.putExtra("task_name", "" + context.getString(R.string.sales));
                            context.overridePendingTransition(0, 0)
//                        startActivity(context, intent, bundle)
                            context.startActivity(intent)
                        }
                        //MessageUtils.showToastMessage(context, "Waiting for Checking Stock Details API")


                    } else {
                        /*  var bundle = Bundle();
                          var fragment = ViewEventDetailed();
                          fragment.arguments = bundle*/
                        val intent = Intent(context, CRMDetailsActivity::class.java)
                        intent.putExtra("id", "" + items[position].id)
                        intent.putExtra("user_name", "Stock")
                        intent.putExtra("task_name", "" + context.getString(R.string.sales));
                        context.overridePendingTransition(0, 0)
//                        startActivity(context, intent, bundle)
                        context.startActivity(intent)
                    }
                }

                holder.itemCall.setOnClickListener(View.OnClickListener {
                    if (isReadLocationAllowed()) {
                        context.startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:" + items[position].mobileNo.toString())))
                    } else {
                        requestLocationPermission()
                        MessageUtils.showToastMessageLong(context, "Please enable permission in App Settings.")
                    }
                })


                holder.itemEmail.setOnClickListener {
                    var email = Intent(Intent.ACTION_SEND)
                    email.putExtra(Intent.EXTRA_EMAIL, items[position].email)
                    email.putExtra(Intent.EXTRA_SUBJECT, "Add Default Subject")
                    email.putExtra(Intent.EXTRA_TEXT, "Add Default Message")
                    email.setType("message/rfc822")
                    context.startActivity(Intent.createChooser(email, "Testing Email Sent"))
                }
            }
            /* ITEM -> {
                 holder.loader.visibility = View.VISIBLE
                 holder.itemLayout.visibility = View.VISIBLE
             }*/
            LOADING -> {
                holder.loader.visibility = View.VISIBLE
                holder.itemLayout.visibility = View.GONE

            }
        }


    }


    @RequiresApi(Build.VERSION_CODES.M)
    public fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(context!!, Manifest.permission.READ_CALL_LOG)) {
            //    Toast.makeText(context, "Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show()

        }

        context.requestPermissions(arrayOf(Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE), Enquiry.LOCATION_PERMISSION_CODE)
    }

    public fun isReadLocationAllowed(): Boolean {

        val result = ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_CALL_LOG)
        val result1 = ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_CONTACTS)
        val CALL_PHONE = ContextCompat.checkSelfPermission(context!!, Manifest.permission.CALL_PHONE)
        return if (result1 == PackageManager.PERMISSION_GRANTED && result == PackageManager.PERMISSION_GRANTED && CALL_PHONE == PackageManager.PERMISSION_GRANTED) {
            return true
        } else
            false
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == items.size - 1 && isLoadingAdded) LOADING else HERO/* if (position == 0) {
            HERO
        } else {
            if (position == items.size - 1 && isLoadingAdded) LOADING else HERO
        }*/
    }

    fun addAll(sales: List<EnquiryModel>?) {
        for (sale in sales!!) {
            //items.add(sale)
            add(sale)
        }

    }

    fun removeLoadingFooter() {
        isLoadingAdded = false

        val position = items.size - 1
        val result = getItem(position)

        if (result != null) {
            items.removeAt(position)
            notifyItemRemoved(position)
        }
        //notifyDataSetChanged()
    }

    fun getItem(position: Int): EnquiryModel {
        return items.get(position)
    }

    fun add(r: EnquiryModel) {
        items.add(r)
        notifyItemInserted(items.size - 1)
        //notifyDataSetChanged()
    }


    fun addLoadingFooter() {
        isLoadingAdded = true
        //notifyItemInserted(items.size - 1)
        /*if (currentPage == 1) {
        } else {*/

        add(EnquiryModel())
        // }
    }

}

class HolderSales(view: View) : RecyclerView.ViewHolder(view) {
    val itemName = view.item_crm_name
    val itemDate = view.item_crm_date
    val itemTask = view.item_crm_task
    val itemAction = view.item_crm_action
    val itemStatus = view.item_crm_status
    val itemLayout = view.item_crm
    val itemEmail = view.item_crm_action_email
    val itemCall = view.item_crm_action_call
    val snack_view = view.snack_view
    val crm_item_lay_model = view.crm_item_lay_model
    val itemModel = view.crm_item_model
    val loader = view.loader
}


