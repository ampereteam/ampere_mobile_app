package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareDetailsPojos.InvoicesItem
import com.ampere.bike.ampere.views.kotlin.fragments.crm.SpareDetailsFgmt
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.adapter_spare_details.view.*

class SpareDetailsAdapter  (val context: SpareDetailsFgmt, val spareDeatilsPojoList: ArrayList<InvoicesItem?>?,val configClickEvent: ConfigClickEvent) : RecyclerView.Adapter<SpareDetailsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpareDetailsAdapter.ViewHolder {
        val view : View  = LayoutInflater.from(parent.context).inflate(R.layout.adapter_spare_details,parent,false)
        return  ViewHolder(view)
    }

    override fun getItemCount(): Int {
      return spareDeatilsPojoList?.size!!
    }

    override fun onBindViewHolder(holder: SpareDetailsAdapter.ViewHolder, position: Int) {
        try{

        holder.serialNo.text= (position+1).toString()
        holder.mobile.text = spareDeatilsPojoList?.get(position)?.mobile
        holder.customerName.text = spareDeatilsPojoList?.get(position)?.customerName
        holder.invoiceId.text = spareDeatilsPojoList?.get(position)?.invoiceId
        holder.vehicleModel.text = spareDeatilsPojoList?.get(position)?.vehicleModel
        holder.spareDetails.setOnClickListener {
            configClickEvent.connectposition(position,spareDeatilsPojoList?.size!!)
        }
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    class ViewHolder(view : View): RecyclerView.ViewHolder(view) {
        val serialNo = view.spr_sNum
        val mobile = view.spr_mob
        val customerName =view .spr_cust_name
        val vehicleModel  =view .spr_vehicle_model
        val invoiceId = view .spr_invoice_id
        val spareDetails = view.spare_details
    }
}
