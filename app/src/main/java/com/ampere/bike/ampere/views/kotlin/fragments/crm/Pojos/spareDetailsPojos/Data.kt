package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareDetailsPojos

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("invoices")
	val invoices: ArrayList<InvoicesItem?>? = null
)