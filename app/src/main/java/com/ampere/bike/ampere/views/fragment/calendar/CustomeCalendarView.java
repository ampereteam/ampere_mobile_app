/*
 * Copyright 2011 Lauri Nevala.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ampere.bike.ampere.views.fragment.calendar;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.GridLayoutAnimationController;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.api.RetrofitCall;
import com.ampere.bike.ampere.api.RetrofitService;
import com.ampere.bike.ampere.model.calendar.ModelCalendar;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.DateUtils;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.vehicles.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import customviews.CustomTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ampere.bike.ampere.shared.LocalSession.BEARER;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_USER_NAME;
import static com.ampere.bike.ampere.utils.MyUtils.LEAVE_CAL_DATE;
import static com.ampere.bike.ampere.utils.MyUtils.LEAVE_CAL_MONTH;
import static com.ampere.bike.ampere.utils.MyUtils.LEAVE_CAL_YEAR;


public class CustomeCalendarView extends Fragment {

    public Calendar month;
    private CoordinatorLayout mSnackView;
    public CalendarAdapter adapter;
    public Handler handler;
    public ArrayList<String> items; // container to store some random calendar items
    public ArrayList<HashMap<String, String>> mEventAdds;
    private FloatingActionButton mCurrentDate;
    private TextView title, previous, next;
    private GridView gridview;
    private RelativeLayout mHeader;
    private RetrofitService retrofitService;
    private Dialog dialog;
    private String viewMonth, viewDate, viewYear;
    private boolean isVisible;
    public static ModelCalendar modelCalendar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        ((CustomNavigationDuo) getActivity()).disbleNavigationView("Leave Calendar");
       /* ((CustomNavigationDuo) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((CustomNavigationDuo) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
        View view = inflater.inflate(R.layout.calendar, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        retrofitService = RetrofitCall.getClient().create(RetrofitService.class);
        month = Calendar.getInstance();



       /* SampleModel modelCalendara = new SampleModel("Name12", 450);

        hashMap.add(modelCalendara);


        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("names", "name 1");
        stringObjectHashMap.put("mark", 500);
        hashMap.add(stringObjectHashMap);


        for (int i = 0; i < hashMap.size(); i++) {
            if (i == 0) {
                SampleModel sampleModel = (SampleModel) hashMap.get(i);
                Log.d("hashMap", "" + sampleModel.name);
            }
            if (i == 1) {
                HashMap<String, Object> stringObjectHashMap1 = (HashMap<String, Object>) hashMap.get(i);
                Log.d("sdsd", "" + stringObjectHashMap1.get("names"));
            }
        }
        stringObjectHashMap.clear();
        stringObjectHashMap.put("all", hashMap);

        hashMaps.add(stringObjectHashMap);
        for (int i = 0; i < hashMaps.size(); i++) {
            Log.d("hashMaps", "" + hashMaps.get(i).get("all"));
        }
*/
        onNewIntent(getActivity().getIntent());

        items = new ArrayList<String>();
        mEventAdds = new ArrayList<>();
        adapter = new CalendarAdapter(getActivity(), month);
        gridview = (GridView) view.findViewById(R.id.gridview);
        mSnackView = (CoordinatorLayout) view.findViewById(R.id.mSnackView);
        mCurrentDate = (FloatingActionButton) view.findViewById(R.id.lev_event_current_date);
        title = (TextView) view.findViewById(R.id.title);
        mHeader = (RelativeLayout) view.findViewById(R.id.header);
        previous = (TextView) view.findViewById(R.id.previous);
        next = (TextView) view.findViewById(R.id.next);
       /* if (checkVersion() == 1) {
            mHeader.setBackground(getActivity().getResources().getDrawable(R.drawable.draw_ripple_effect_top));
            next.setBackground(getActivity().getResources().getDrawable(R.drawable.draw_ripple_effect));
            previous.setBackground(getActivity().getResources().getDrawable(R.drawable.draw_ripple_effect));
        } else {
            mHeader.setBackground(getActivity().getResources().getDrawable(R.drawable.shadow_below_21));
            next.setBackground(getActivity().getResources().getDrawable(R.drawable.shadow_below_21));
            previous.setBackground(getActivity().getResources().getDrawable(R.drawable.shadow_below_21));
        }*/
        viewMonth = "" + android.text.format.DateFormat.format("MM", month);
        viewDate = "" + android.text.format.DateFormat.format("DD", month);
        viewYear = "" + android.text.format.DateFormat.format("yyyy", month);

        //loadPlanLeaves(viewDate, viewMonth, viewYear);

        mCurrentDate.setVisibility(View.GONE);
        gridview.setAdapter(adapter);
        handler = new Handler();
        // setAnimation(R.anim.fade);

        if (!LEAVE_CAL_DATE.isEmpty() && !LEAVE_CAL_MONTH.isEmpty() && !LEAVE_CAL_YEAR.isEmpty()) {
            month.set((Integer.parseInt(LEAVE_CAL_YEAR)), (Integer.parseInt(LEAVE_CAL_MONTH) - 1), 1);
            refreshCalendarFirst();
            title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
            LEAVE_CAL_YEAR = "";
            LEAVE_CAL_MONTH = "";
            LEAVE_CAL_YEAR = "";
        } else {
            title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
        }


        handler.post(calendarUpdater);

        mCurrentDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                modelCalendar = null;
                Toast.makeText(getActivity(), "This Month", Toast.LENGTH_SHORT).show();
                String crDate = DateUtils.getCurrentDate("dd");
                String crMonth = DateUtils.getCurrentDate("MM");
                String crYear = DateUtils.getCurrentDate("yyyy");
                month.set((Integer.parseInt(crYear)), (Integer.parseInt(crMonth) - 1), 1);
                refreshCalendar();
                title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
                //  setAnimation(R.anim.fade);
            }
        });


        previous.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("WrongConstant")
            @Override
            public void onClick(View v) {
                modelCalendar = null;
                if (month.get(Calendar.MONTH) == month.getActualMinimum(Calendar.MONTH)) {
                    month.set((month.get(Calendar.YEAR) - 1), month.getActualMaximum(Calendar.MONTH), 1);
                } else {
                    month.set(Calendar.MONTH, month.get(Calendar.MONTH) - 1);
                }
                refreshCalendar();
                //  setAnimation(R.anim.slide_left_out);
            }
        });


        next.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("WrongConstant")
            @Override
            public void onClick(View v) {
                modelCalendar = null;
                buttonEnabled(false);
                if (month.get(Calendar.MONTH) == month.getActualMaximum(Calendar.MONTH)) {
                    month.set((month.get(Calendar.YEAR) + 1), month.getActualMinimum(Calendar.MONTH), 1);
                } else {
                    month.set(Calendar.MONTH, month.get(Calendar.MONTH) + 1);
                }
                refreshCalendar();
                //  setAnimation(R.anim.slide_left_out);

            }
        });

        gridview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                CustomTextView date = (CustomTextView) v.findViewById(R.id.date);
                CustomTextView mEventName = (CustomTextView) v.findViewById(R.id.mEventName);


                if (date instanceof TextView && !date.getText().equals("")) {
                    String day = date.getText().toString();
                    String name = mEventName.getText().toString();
                    if (day.length() == 1) {
                        day = "0" + day;
                    }
                    LEAVE_CAL_DATE = day;
                    LEAVE_CAL_MONTH = "" + android.text.format.DateFormat.format("MM", month);
                    LEAVE_CAL_YEAR = "" + android.text.format.DateFormat.format("yyyy", month);
                    Bundle bundle = new Bundle();
                    bundle.putString("date", android.text.format.DateFormat.format("yyyy-MM", month) + "-" + day);
                    bundle.putString("eventName", "" + name);
                    Log.d("dsdlkl", "call");


                    ViewEvent addEvent = new ViewEvent();
                    addEvent.setArguments(bundle);

                    MyUtils.passFragmentBackStack(getActivity(), addEvent);
                    //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.co, addEvent).addToBackStack(null).commit();

                }
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        } else {
            return false;
        }
    }

    private void buttonEnabled(boolean isEnable) {
        previous.setEnabled(isEnable);
        next.setEnabled(isEnable);
    }

    private void refreshCalendarFirst() {
        adapter.refreshDays();
        adapter.notifyDataSetChanged();
        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

        String currentMonth = String.valueOf(android.text.format.DateFormat.format("MM-yyyy", month));
        String currenMonth = DateUtils.getCurrentDate("MM-yyyy");
        if (currenMonth.equals(currentMonth)) {
            mCurrentDate.setVisibility(View.GONE);
        } else {
            mCurrentDate.setVisibility(View.VISIBLE);
        }
    }

    private void setAnimation(int anim) {
        Animation animation = AnimationUtils.loadAnimation(getContext(), anim);
        GridLayoutAnimationController controller = new GridLayoutAnimationController(animation, .2f, .2f);
        gridview.setLayoutAnimation(controller);
    }

    public void refreshCalendar() {
        adapter.refreshDays();
        adapter.notifyDataSetChanged();
        handler.post(calendarUpdater); // generate some random calendar items

        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

        String currentMonth = String.valueOf(android.text.format.DateFormat.format("MM-yyyy", month));
        String currenMonth = DateUtils.getCurrentDate("MM-yyyy");
        if (currenMonth.equals(currentMonth)) {
            mCurrentDate.setVisibility(View.GONE);
        } else {
            mCurrentDate.setVisibility(View.VISIBLE);
        }

    }

    public void onNewIntent(Intent intent) {
        String date = intent.getStringExtra("date");
        if (date != null) {
            String[] dateArr = date.split("-"); // date format is yyyy-mm-dd
            month.set(Integer.parseInt(dateArr[0]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[2]));
        }
    }

    public Runnable calendarUpdater = new Runnable() {

        @Override
        public void run() {
            items.clear();
            // format random values. You can implement a dedicated class to provide real values
            viewDate = "" + android.text.format.DateFormat.format("DD", month);
            viewMonth = "" + android.text.format.DateFormat.format("MM", month);
            viewYear = "" + android.text.format.DateFormat.format("yyyy", month);


            if (modelCalendar != null) {
                mEventAdds.clear();
                Log.d("whenIsCalled", "000");

                for (int i = 0; i < modelCalendar.data.enquirycount.size(); i++) {
                    HashMap<String, String> stringStringHashMap = new HashMap<>();
                    stringStringHashMap.put("date", "" + modelCalendar.data.enquirycount.get(i).date);
                    stringStringHashMap.put("event", "" + modelCalendar.data.enquirycount.get(i).enquirycount);
                    mEventAdds.add(stringStringHashMap);

                }

                //}

                adapter.setItems(mEventAdds);
                adapter.notifyDataSetChanged();
            } else {
                Log.d("whenIsCalled", "111");
                loadPlanLeaves(viewDate, viewMonth, viewYear);
            }
        }
    };


    private void loadPlanLeaves(String viewDate, String viewMonth, String viewYear) {


        buttonEnabled(true);
        dialog = MessageUtils.showDialog(getActivity());

        HashMap<String, String> taskInMonth = new HashMap<>();
        taskInMonth.put("month", "" + viewMonth);
        taskInMonth.put("year", "" + viewYear);
        taskInMonth.put("user_name", "" + LocalSession.getUserInfo(getActivity(), KEY_USER_NAME));
        Log.d("taskInMonthPassing", "" + taskInMonth);
        final Call<ModelCalendar> modelCalendarCall = retrofitService.onCalendarManager(BEARER+LocalSession.getUserInfo(getActivity(),KEY_TOKEN),taskInMonth);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialo, int keyCode, KeyEvent event) {
                MessageUtils.dismissDialog(dialog);
                modelCalendarCall.cancel();
                return false;
            }
        });

        modelCalendarCall.enqueue(new Callback<ModelCalendar>() {
            @Override
            public void onResponse(Call<ModelCalendar> call, Response<ModelCalendar> response) {
                MessageUtils.dismissDialog(dialog);
                if (response.isSuccessful()) {
                    modelCalendar = response.body();
                    if (modelCalendar != null) {
                        //Log.d("checkLog", "1 " + modelCalendar.success);
                        if (modelCalendar.success) {
                            //Log.d("checkLog", "22  " + modelCalendar.data);
                            if (modelCalendar.data != null) {
                                //Log.d("checkLog", "22582 " + modelCalendar.data.enquirycount.size());
                                //if (modelLeaveCalendar.allMoldeLeaveCalendar.isStatus) {
                                for (int i = 0; i < modelCalendar.data.enquirycount.size(); i++) {
                                    HashMap<String, String> stringStringHashMap = new HashMap<>();
                                    stringStringHashMap.put("date", "" + modelCalendar.data.enquirycount.get(i).date);
                                    stringStringHashMap.put("event", "" + modelCalendar.data.enquirycount.get(i).enquirycount);
                                    mEventAdds.add(stringStringHashMap);
                                }
                                //}

                                Log.d("mEventAdds", "" + mEventAdds.size());
                                adapter.setItems(mEventAdds);
                                adapter.notifyDataSetChanged();
                            } else {
                                MessageUtils.showSnackBar(getActivity(), mSnackView, modelCalendar.message);
                            }
                        } else {
                            MessageUtils.showSnackBar(getActivity(), mSnackView, modelCalendar.message);
                        }
                    } else {
                        MessageUtils.showSnackBar(getActivity(), mSnackView, "SpareInvoicePojos not found");
                    }
                } else {
                    MessageUtils.setErrorMessage(getActivity(), mSnackView, response.code());
                }
            }

            @Override
            public void onFailure(Call<ModelCalendar> call, Throwable t) {
                MessageUtils.dismissDialog(dialog);
                MessageUtils.setFailureMessage(getActivity(), mSnackView, t.getMessage());
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        modelCalendar = null;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


}
