package com.ampere.bike.ampere.interfaces;

import com.ampere.bike.ampere.model.task.EnquiryModel;

import java.util.List;

public interface RefreshEnquiryTabs {

    void refreshView(List<EnquiryModel> enquiryModels);


}
