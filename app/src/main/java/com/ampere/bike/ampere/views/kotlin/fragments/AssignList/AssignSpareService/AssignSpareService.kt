package com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSpareService


import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome.Companion.btnAssignList
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome.Companion.spareserviceserverEnquiryList
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSpareService.Adapter.AssignSpareServiceAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo.AssignEnqIDPojo
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo.EnquiryListRes

import com.ampere.vehicles.R
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
@SuppressLint("SetTextI18n,StaticFieldLeak")
class AssignSpareService : Fragment(),ConfigClickEvent {
    var recyAssignSparesService : RecyclerView?=null
    var dialog : Dialog?=null
    var utilsCallApi: UtilsCallApi?=null
    var cbxAssingSparesService: CheckBox?= null
    var  emptyList : TextView?=null
    var  assignSparesServiceAdapter : AssignSpareServiceAdapter?=null
    companion object {
        var btnAssignsparesService: TextView?= null
        var spareserviceEnqryRES: EnquiryListRes?= null
        var spareservicecount =0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_unassign_spare_service, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        utilsCallApi = UtilsCallApi(activity)
        recyAssignSparesService=view.findViewById(R.id.recyl_assign_sales)
        cbxAssingSparesService =view.findViewById(R.id.cbx_assgin_sales)
        btnAssignsparesService =view.findViewById(R.id.btn_assignsales)
        emptyList =view.findViewById(R.id.empty_list)
        cbxAssingSparesService?.isChecked =false
        if(utilsCallApi?.isNetworkConnected!!){
            getSparesEnquiryList()
        }else{
            MessageUtils.showSnackBarAction(activity, CustomNavigationDuo.snackVIew,getString(R.string.check_internet))
        }
        cbxAssingSparesService?.setOnCheckedChangeListener { buttonView, isChecked ->

            if(spareserviceEnqryRES?.data?.enquiries?.size!!<=0){
                Log.d("LISTSIze",""+spareserviceEnqryRES?.data?.enquiries?.size)
                spareservicecount=0
            }else{
                if(isChecked){
                    for(i:Int in 0.. spareserviceEnqryRES?.data?.enquiries?.size!!-1){
                        spareserviceEnqryRES?.data?.enquiries?.get(i)?.check =true
                        spareserviceserverEnquiryList.get(i)?.check =true

                    }
                    btnAssignsparesService?.setText(getString(R.string.assign)+"("+spareserviceEnqryRES?.data?.enquiries?.size+")")
                    btnAssignList?.setText(getString(R.string.assign)+"("+spareserviceEnqryRES?.data?.enquiries?.size+")")
                    btnAssignsparesService?.visibility =View.VISIBLE
                    btnAssignList?.visibility =View.VISIBLE
                    spareservicecount= spareserviceEnqryRES?.data?.enquiries?.size!!
                }else{
                    for(i:Int in 0.. spareserviceEnqryRES?.data?.enquiries?.size!!-1){
                        spareserviceEnqryRES?.data?.enquiries?.get(i)?.check =false
                        spareserviceserverEnquiryList.get(i)?.check =false
                    }
                    btnAssignsparesService?.visibility =View.GONE
                    btnAssignList?.visibility =View.GONE
                    spareservicecount =0
                }
                assignSparesServiceAdapter?.notifyDataSetChanged()
            }


        }
        btnAssignsparesService?.setOnClickListener {
            try{
                if(AssignListHome.spAssignPerson?.selectedItem !=null){
                    assignSparesServiceEnquiries()
                }else{
                    MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,"Select Assign To and Person")
                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }
   /*     if(AssignListHome.btnAssignList!=null){
            AssignListHome.btnAssignList?.setOnClickListener {
                try{
                    if(AssignListHome.spAssignPerson?.selectedItem !=null){
                        assignSparesServiceEnquiries()
                    }else{
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,"Select Assign To and Person")
                    }
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        }*/
    }

    private fun assignSparesServiceEnquiries() {
        try{
            val map:HashMap<String,Any>  = HashMap()
            map["userid"] = LocalSession.getUserInfo(activity, LocalSession.KEY_ID)
            map["assign_to"] = AssignListHome.spAssignTo?.selectedItem.toString()
            var assignpersonID :String?=null
            for(user in AssignListHome.userListRes?.data?.users!!){
                if( user?.name.equals(AssignListHome.spAssignPerson?.selectedItem.toString())){
                    assignpersonID = user?.id.toString()
                }
            }
            if(assignpersonID!=null){
                map["assign_person"] =assignpersonID.toInt()
            }
            val hashMap:HashMap<String,Any>  =HashMap()
            val enquiryList :ArrayList<AssignEnqIDPojo> = ArrayList()
            for (i:Int in 0 .. spareserviceserverEnquiryList.size-1){
                if(spareserviceserverEnquiryList.get(i)?.check!!) {
                    val assignEnqIDPojo = AssignEnqIDPojo(spareserviceserverEnquiryList.get(i)?.id.toString())
                    enquiryList.add(assignEnqIDPojo)
                }
            }
            hashMap["enquiry"] =enquiryList
            map["enquiries"] =hashMap
            Log.d("AssignEnquirySales",""+map.toString())
            if(utilsCallApi?.isNetworkConnected!!){
                dialog = MessageUtils.showDialog(activity)
                val calinterface = utilsCallApi?.connectRetro("POST","assignenquiry")
                val callback = calinterface?.call_post("assignenquiry","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),map)
                callback?.enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        try{
                            MessageUtils.dismissDialog(dialog)
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.localizedMessage)
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        try{
                            if(response.isSuccessful){
                                try{
                                    val jsonObject = JSONObject(response.body()?.string())
                                    val message = jsonObject.getString("message")
                                    val success =jsonObject.getBoolean("success")
                                    if(success){
                                        btnAssignsparesService?.visibility =View.GONE
                                        btnAssignList?.visibility =View.GONE
                                        Toast.makeText(activity,message, Toast.LENGTH_LONG).show()
                                        getSparesEnquiryList()
                                    }else{
                                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,message)
                                    }
                                }catch (ex:Exception){
                                    ex.printStackTrace()

                                }
                            }else{
                                MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response.message())
                            }
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity, CustomNavigationDuo.snackVIew,getString(R.string.check_internet))
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun getSparesEnquiryList() {
        try{
            val hashMap :HashMap<String,Any> = HashMap()
            hashMap["userid"] = LocalSession.getUserInfo(activity, LocalSession.KEY_ID)
            hashMap["enquiry_type"] = getString(R.string.spare_service)
            dialog = MessageUtils.showDialog(activity)
            val callInterface  = utilsCallApi?.connectRetro("POST","EnquiryListRes")
            val callBack = callInterface?.call_post("unassignedenquiry","Bearer "+ LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN),hashMap)
            callBack?.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    try{
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,t.localizedMessage)

                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    try{
                        if(response.isSuccessful){
                            try{
                                spareserviceEnqryRES = Gson().fromJson(response.body()?.string(), EnquiryListRes::class.java)
                                if(spareserviceEnqryRES?.success!!){
                                    if(spareserviceEnqryRES?.data?.enquiries?.size!! <=0){
                                        emptyList?.setText(spareserviceEnqryRES?.message)
                                        emptyList?.visibility = View.VISIBLE
                                        recyAssignSparesService?.visibility =View.GONE
                                    }else{
                                        if(btnAssignsparesService?.visibility == View.VISIBLE  ){
                                            btnAssignsparesService?.visibility = View.GONE
                                        }
                                        if(cbxAssingSparesService?.isChecked!!){
                                            cbxAssingSparesService?.isChecked =false
                                        }
                                        emptyList?.visibility = View.GONE
                                        recyAssignSparesService?.visibility =View.VISIBLE
                                        spareserviceserverEnquiryList = spareserviceEnqryRES?.data?.enquiries!!
                                        spareservicecount =0
                                        setTOAdapter()
                                    }

                                }else{
                                    MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,spareserviceEnqryRES?.message)
                                }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }
                        }else{
                            MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,response.message())
                        }
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }
            })
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setTOAdapter() {
        try{
            recyAssignSparesService?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL,false)
            assignSparesServiceAdapter = AssignSpareServiceAdapter(activity!!,spareserviceserverEnquiryList,this)
            recyAssignSparesService?.adapter = assignSparesServiceAdapter
        }catch (eX:Exception){
            eX.printStackTrace()
        }
    }

    override fun connectposition(position: Int, listSize: Int) {
        try{
            Log.d("CallPostion",""+position+"\tlistsize"+listSize)
            if(listSize<=0){
                btnAssignsparesService?.visibility =View.GONE
                btnAssignList?.visibility =View.GONE
                spareservicecount =0
            }else{
                btnAssignsparesService?.setText(getString(R.string.assign)+"("+listSize+")")
                btnAssignList?.setText(getString(R.string.assign)+"("+listSize+")")
                btnAssignsparesService?.visibility =View.VISIBLE
                btnAssignList?.visibility =View.VISIBLE
                spareservicecount = spareserviceEnqryRES?.data?.enquiries?.size!!
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
}
