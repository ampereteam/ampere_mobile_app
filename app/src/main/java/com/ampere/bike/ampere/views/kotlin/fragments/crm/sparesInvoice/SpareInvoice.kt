package com.ampere.bike.ampere.views.kotlin.fragments.crm.sparesInvoice

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.api.api_new.ConfigPosition
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.api.api_new.UtilsUICallBack
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_ID
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.DateUtils.showDateDailog
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.utils.Utils
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMActivity
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.dialog
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.enquiryId
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoiceAdapterViewPojos.SpareInvoiceadapterviewPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoicePojos.ColorsItem
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoicePojos.Data
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoicePojos.SpareInvoicesPojo
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoicePojos.SpareinventoryItem
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.SpareinvoiceviewAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.crm.sparesInvoice.Pojos.GetspareinvoiceResList
import com.ampere.vehicles.R
import com.ampere.vehicles.R.id.mSnackView
import com.ampere.vehicles.R.id.spr_add_invoice_snack_view
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import customviews.CustomEditText
import customviews.CustomRadioButton
import kotlinx.android.synthetic.main.activity_spare_invoice.*
import kotlinx.android.synthetic.main.dialog_invoice.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@Suppress("NAME_SHADOWING", "DEPRECATION")
@SuppressWarnings("ALL")
class SpareInvoice : AppCompatActivity(), UtilsUICallBack, ConfigPosition {


    var enquiryId: String = ""
    var user_name: String = ""
    private var task_name: String = ""
    var vehicleModelsList: ArrayList<String> = ArrayList()
    private var idproofsList: ArrayList<String> = ArrayList()
    private var addressproofsList: ArrayList<String> = ArrayList()
    private var sparestringList: ArrayList<String> = ArrayList()
    private var spareinvoicelistaddmoreList: ArrayList<SpareInvoiceadapterviewPojos> = ArrayList()
    private var stateLists: ArrayList<String> = ArrayList()
    var dialog: Dialog? = null
    var callApi: UtilsCallApi? = null
    var colorList: ArrayList<String> = ArrayList()
    var data: Data? = null
    var spareinvoiceresponse: SpareInvoicesPojo? = null

    /***
     * set the All Spinner to Adapter
     */
    var vehicleModelAdapter: ArrayAdapter<String>? = null
    var vehicleColorAdapter: ArrayAdapter<String>? = null
    var statelistAdapter: ArrayAdapter<String>? = null
    var customerProofAdapter: ArrayAdapter<String>? = null
    var customerAddressProofAdapter: ArrayAdapter<String>? = null
    var spareArrayAdapter: ArrayAdapter<String>? = null
    var spareinvoiceviewAdapter: SpareinvoiceviewAdapter? = null
    var map = HashMap<String, Any>()
    var mSnackView:View ? =null
    /** getSpareResPojos**/
    var getspareinvoiceResList : GetspareinvoiceResList ?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        setContentView(R.layout.activity_spare_invoice)
        mSnackView = findViewById(R.id.spr_add_invoice_snack_view)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowTitleEnabled(false)
        getSupportActionBar()?.hide()
        val bundle = intent
        enquiryId = bundle.getStringExtra("id")
        user_name = bundle.getStringExtra("user_name")
        task_name = bundle.getStringExtra("task_name")
        callApi = UtilsCallApi(this, this)

        onFocus()
        getDataForInvoiceFromSVR()
        spr_ic_back_invoice.setOnClickListener {
            onBackPressed()
        }
        spr_invoice_submit.setOnClickListener {
            try {

                val vehicleModel: String = spr_edt_vehicle_model.text.toString()
                val modelVarient: String = ""
                val color: String = spr_edt_vehicle_model_color.text.toString()
                val invoiceAmount: String = spr_edt_invoice_amt.text.toString()
                val customerName: String = spr_customer_name.text.toString()
                val customerAddress1: String = spr_customer_address.text.toString()
                val customerAddress2: String = spr_customer_address_2.text.toString()
                val locality: String = spr_customer_locality.text.toString()
                val city: String = spr_city.text.toString()
                val mobile: String = spr_customer_mobile_no.text.toString()
                val pinCode: String = spr_add_pincode.text.toString()
                val state: String = spr_stat_edt.text.toString()
                val statecode: String = spr_stat_code_edt.text.toString()
                val total_amount: String = spr_customer_total_amonut.text.toString()
                val deliveryAt: String = spr_customer_delivery_at.text.toString()
                val aadharNo: String = spr_customer_aadhar_numer.text.toString()
                val selectedId: Int = spr_radio_group.checkedRadioButtonId;
                var gender: String = ""
                // find the radiobutton by returned id
                if (selectedId != -1) {
                    val radioButton: CustomRadioButton = findViewById(selectedId);
                    gender = radioButton.text.toString()
                }

//enquiry_id, user_id, sendThrough, model_varient, serviceType, chassis_no, invoice_amount, customer_name, customer_address1, customer_address2, locality, city, mobile, pincode, state, statecode, total_amount, delivery_at, aadhar_no, gender,

                val idProof: String = spr_id_proof_sp.selectedItem.toString()
                val addressProof: String = spr_address_proof_sp.selectedItem.toString()
                val email: String = spr_customer_email_id.text.toString()
                val dob: String = spr_customer_dob.text.toString()
                val occupation: String = spr_customer_occupation.text.toString()
                val totalAmount: String = spr_customer_total_amonut.text.toString()

                if (!isValidationOfInvoiceItems(enquiryId, vehicleModel, color, invoiceAmount, customerName, customerAddress1, customerAddress2, locality, city, mobile, pinCode, state, statecode, total_amount, deliveryAt, aadharNo, gender)) {
                    // mandatory
                    map["enquiry_id"] = "" + enquiryId
                    map["userid"] = LocalSession.getUserInfo(this@SpareInvoice, LocalSession.KEY_ID)
                    map["vehicle_model"] = "" + vehicleModel
//                    map.put("model_varient", modelVarient)
                    map["color"] = color
//                    map.put("chassis_no", chassisNo)
                    map["invoice_amount"] = invoiceAmount
                    map["customer_name"] = customerName
                    map["customer_address1"] = customerAddress1
                    map["customer_address2"] = customerAddress2
                    map["locality"] = locality
                    map["city"] = city
                    map["mobile"] = mobile
                    map["pincode"] = pinCode
                    map["state"] = state
                    map["statecode"] = statecode
                    map["total_amount"] = total_amount
                    map["delivery_at"] = deliveryAt
                    map["aadhar_no"] = aadharNo
                    map["gender"] = gender

                    //mon mandatory


                    map["email_id"] = email
                    map["dob"] = dob
                    map["occupation"] = occupation

                    //if (state.toLowerCase().equals(modelDataEnquiry?.state.toString().toLowerCase())) {
                    map["sgst_id"] = "" + spareinvoiceresponse?.data?.gst?.sgst.toString()
                    map["sgst"] = "" + spr_tax_sgst.text.toString()
                    map["cgst_id"] = "" + spareinvoiceresponse?.data?.gst?.cgst.toString()
                    map["cgst"] = "" + spr_tax_cgst.text.toString()
                    //} else {
                    map["igst_id"] = "" + spareinvoiceresponse?.data?.gst?.igst.toString()
                    map["igst"] = spr_tax_igst.text.toString()
                    //}

                    map["total_amount"] = totalAmount

                    if (spr_sp_spare.selectedItem.equals(getString(R.string.no_data_avl)) || spr_sp_spare.selectedItem.equals(getString(R.string.select_spare))) {
                        MessageUtils.showToastMessage(this@SpareInvoice, getString(R.string.enter_valid_spare))
                    } else {
                        if (spr_edt_qty.text.isNullOrEmpty()) {
                            MessageUtils.showToastMessage(this@SpareInvoice, "Invalid Qty")
                        } else {
                            if (spr_edt_prunit.text.isNullOrEmpty()) {

                            } else {
                                if (idProof != MyUtils.SELECT_ID_PROOF && idProof != getString(R.string.select_proof)) {
                                    map["id_proof"] = idProof

                                    if (addressProof != MyUtils.SELECT_ID_PROOF && addressProof != getString(R.string.select_proof)) {
                                        map["address_proof"] = addressProof
                                        spareinvoicelistaddmoreList.add(SpareInvoiceadapterviewPojos(spr_sp_spare.selectedItem.toString(), spr_edt_qty.text.toString(), spr_edt_prunit.text.toString(), spr_edt_total.text.toString()))
                                        val hashmap: HashMap<String, Any> = HashMap()
                                        hashmap["spare"] = spareinvoicelistaddmoreList
                                        map["sparelists"] = hashmap
                                        sendToserver(map)
                                    } else {
                                        MessageUtils.showToastMessage(this@SpareInvoice, "Eneter the Valid Data Addrss Proof")
                                    }
                                } else {

                                    MessageUtils.showToastMessage(this@SpareInvoice, "Eneter the Valid Data ID Proof")
                                }

                            }

                        }
                    }

                }
            } catch (Ex:Exception) {
                Ex.printStackTrace()
            }
        }

        spr_invoice_cancel.setOnClickListener {
            //MessageUtils.showToastMessage(this@SpareInvoice, "Failure " + vehicleModelsList.size)
            onBackPressed()
        }

        spr_customer_dob.setOnClickListener {
            showDateDailog()
        }
        spr_imgbn_addspare.setOnClickListener {

            if (spr_sp_spare.selectedItem == null || spr_sp_spare.selectedItem.toString().isNullOrEmpty()
                    || spr_sp_spare.selectedItem.equals(getString(R.string.select_spare))
                    || spr_sp_spare.selectedItem.equals(getString(R.string.no_data_avl))) {
                Log.d("selected item empty", "$spr_sp_spare.selectedItem")
                MessageUtils.showToastMessage(this, getString(R.string.enter_valid_spare))

            } else {
                if (spr_edt_qty.text == null || spr_edt_qty.text.toString().isNullOrEmpty()
                ) {
                    Log.d("selected item empty", "$spr_sp_spare.selectedItem")
                } else {
                    if (spr_edt_prunit.text == null || spr_edt_prunit.text.toString().isNullOrEmpty()
                    ) {
                        Log.d("selected item empty", "$spr_sp_spare.selectedItem")
                    } else {
                        if (spr_edt_total.text == null || spr_edt_total.text.toString().isNullOrEmpty()
                        ) {
                            Log.d("selected item empty", "$spr_sp_spare.selectedItem")
                        } else {
                            val sparename = spr_sp_spare.selectedItem.toString()
                            val qty = spr_edt_qty.text.toString()
                            val perunit = spr_edt_prunit.text.toString()
                            val total_amount = spr_edt_total.text.toString()
                            spareinvoicelistaddmoreList.add(SpareInvoiceadapterviewPojos(sparename, qty, perunit, total_amount))
                            addmorespare()
                        }
                    }
                }
            }


        }
        /**
         * get and set the spare spinner
         */
        spr_sp_spare.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Log.d("Please Select", "spare item")

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedSpareitem = spr_sp_spare.selectedItem.toString()
                spr_edt_spare.setText(selectedSpareitem)
                if (selectedSpareitem == getString(R.string.select_spare) ) {

                } else {

                }
            }

        }

        /**
         * setOn Text Listener for calculating the total amount
         */
        spr_edt_prunit.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                //Log.d("textAfter Changed", spr_edt_prunit.text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //Log.d("beforeTextChanged ", spr_edt_prunit.text.toString())
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //Log.d("beforeTextChanged ", spr_edt_prunit.text.toString())
                if (spr_edt_qty.text.isNullOrEmpty()) {

                } else {
                    try {
                        val total_amount = spr_edt_qty.text.toString().toDouble() * spr_edt_prunit.text.toString().toDouble()
                        spr_edt_total.setText(total_amount.toString())
                    } catch (ex: java.lang.Exception) {
                        spr_edt_total.setText("0")
                        ex.printStackTrace()
                    }

                }

            }

        })
        /**
         * add Text Changed  for Total Amount
         */
        spr_edt_qty.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
               // Log.d("textAfter Changed", spr_edt_qty.text.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //Log.d("beforeTextChanged ", spr_edt_qty.text.toString())
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
               // Log.d("beforeTextChanged ", spr_edt_qty.text.toString())
                if (spr_edt_prunit.text.isNullOrEmpty()) {

                } else {
                    try {
                        val total_amount = spr_edt_qty.text.toString().toDouble() * spr_edt_prunit.text.toString().toDouble()
                        spr_edt_total.setText(total_amount.toString())
                    } catch (ex: java.lang.Exception) {
                        spr_edt_total.setText("0")
                        ex.printStackTrace()
                    }

                }

            }

        })
    }

    private fun addmorespare() {
        spareinvoiceviewAdapter = SpareinvoiceviewAdapter(this, spareinvoicelistaddmoreList, this)
        spr_rcyle.layoutManager = LinearLayoutManager(this)
        spr_rcyle.adapter = spareinvoiceviewAdapter
        Log.d("spare",""+spareinvoicelistaddmoreList.size)
        Log.d("spare",""+sparestringList.size)

        for(spare in spareinvoicelistaddmoreList){
              if (sparestringList.contains(spare.spareNam)) {
                  sparestringList.remove(spare.spareNam)
              }
        }
        if (sparestringList.size <= 0) {
            sparestringList.add(getString(R.string.no_data_avl))
            spr_imgbn_addspare?.visibility =View.GONE
        }
        spareArrayAdapter?.notifyDataSetChanged()
        println("changed arraylist" + spareinvoicelistaddmoreList.size)
    }

    private fun sendToserver(map: HashMap<String, Any>) {
        if(callApi?.isNetworkConnected!!){
        dialog = MessageUtils.showDialog(this@SpareInvoice)
        val apiinterface = callApi?.connectRetro("POST", "savesparesinvoice")
        val callback = apiinterface?.call_post("savesparesinvoice", "Bearer " + LocalSession.getUserInfo(this@SpareInvoice, KEY_TOKEN), map)
        callback?.enqueue((object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                //Log.d("Server Failure", "" + t.toString())
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                MessageUtils.dismissDialog(dialog)
                if (response.isSuccessful) {
                 //   println("response" + response.body()?.string())
                    spareinvoiceresponse = Gson().fromJson(response.body()?.string(), SpareInvoicesPojo::class.java)
                    if (spareinvoiceresponse?.success!!) {

                        var intent = Intent(this@SpareInvoice, CustomNavigationDuo::class.java)
                        intent.putExtra("redirect", "spareDetails")
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                        finish()

                    } else {
                        MessageUtils.showToastMessage(this@SpareInvoice, spareinvoiceresponse?.message)
                    }
                } else {
                    MessageUtils.showToastMessage(this@SpareInvoice, "Server Error Found")
                }

            }
        }))
        }else{
            MessageUtils.showSnackBarAction(this@SpareInvoice,mSnackView,getString(R.string.check_internet))
        }

    }

    private fun getDataForInvoiceFromSVR() {
        if(callApi?.isNetworkConnected!!){

        dialog = MessageUtils.showDialog(this@SpareInvoice)
        val map = HashMap<String, Any>()
        map["userid"] = "" + LocalSession.getUserInfo(this@SpareInvoice, LocalSession.KEY_ID)
        map["enquiry_id"] = "" + enquiryId
        if (callApi?.isNetworkConnected!!) {
//            callApi?.callApi("getspareinvoice","POST",map,0)
            val apiservice = callApi?.connectRetro("POST", "getspareinvoice")
            val response = apiservice?.call_post("Bearer " + LocalSession.getUserInfo(this, KEY_TOKEN), map)
            response?.enqueue((object : Callback<SpareInvoicesPojo> {
                override fun onResponse(call: Call<SpareInvoicesPojo>, response: Response<SpareInvoicesPojo>) {
                    MessageUtils.dismissDialog(dialog)
                    if (response.isSuccessful) {
                        spareinvoiceresponse = response.body()
                        println("Response for spareinvoice " + spareinvoiceresponse.toString())
                        Log.d("gstsdsd", "" + spareinvoiceresponse?.data?.gst?.sparesIgst + " \n sgst" + spareinvoiceresponse?.data?.gst?.sparesSgst + " inVoiceAmt " )
                        getandSetData()
                    } else {
                        Log.d("Server Not Found", "")
                    }

                }

                override fun onFailure(call: Call<SpareInvoicesPojo>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    MessageUtils.showToastMessage(this@SpareInvoice, "Server Not Found")
                    Log.d("Failure Not Found Server", "" + t.toString())
                }

            }))
        } else {
            MessageUtils.dismissDialog(dialog)
            MessageUtils.showToastMessage(this, "Please Check Internet Connection")
        }

        }else{
            MessageUtils.showSnackBarAction(this@SpareInvoice,mSnackView,getString(R.string.check_internet))
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this@SpareInvoice, CRMActivity::class.java)
        /*intent.putExtra("id", "" + enquiryId)
        intent.putExtra("user_name", "" + LocalSession.getUserInfo(this@SpareInvoice, LocalSession.KEY_USER_NAME))
        intent.putExtra("task_name", "" + task_name)*/
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }

    private fun isValidationOfInvoiceItems(enquiryId: String, vehicle_model: String,  color: String, invoiceAmount: String, customerName: String, customerAddress1: String, customerAddress2: String, locality: String, city: String, mobile: String, pinCode: String, state: String, statecode: String, total_amount: String, deliveryAt: String, aadharNo: String, gender: String): Boolean {

        if (enquiryId.isEmpty()) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Enquiry Id Missing")
            return true
        } else if (vehicle_model.isEmpty() || vehicle_model == " " || vehicle_model.toLowerCase().equals(MyUtils.SELECT_VEHICLE_MODEL.toLowerCase())) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, MyUtils.SELECT_VEHICLE_MODEL)
            return true

        } /*else if (re_model_variant.visibility == View.VISIBLE) {
            if (modelVarient.isEmpty() || modelVarient == " " || modelVarient.toLowerCase().equals(SELECT_VARIANT_TYPE.toLowerCase())) {
                MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, SELECT_VARIANT_TYPE)
                return true
            }
        } */ else if (color.isEmpty() || color == " " || color.toLowerCase().equals(MyUtils.SELECT_COLOR.toLowerCase())) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, MyUtils.SELECT_COLOR)
            return true
        } /*else if (chassisNo.isEmpty() || chassisNo == " " || chassisNo.toLowerCase().equals(MyUtils.SELECT_CHASSIS_NUMBER.toLowerCase())) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, MyUtils.SELECT_CHASSIS_NUMBER)
            return true
        }*/ else if (invoiceAmount.isEmpty() || invoiceAmount == " " || invoiceAmount == "0") {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Invoice Amonut can't be empty")
            return true
        } else if (customerName.isEmpty()) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Customer Name can't be empty")
            return true
        } else if (customerAddress1.isEmpty()) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Address1 can't be empty")
            return true
        } else if (customerAddress2.isEmpty()) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Address2 can't be empty")
            return true
        } else if (locality.isEmpty()) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Locality can't be empty")
            return true
        } else if (city.isEmpty()) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "City can't be empty")
            return true
        } else if (mobile.isEmpty() || mobile.length < 10) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Please enter 10 digit mobile number")
            return true
        } else if (pinCode.isEmpty()) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Pincode can't be empty")
            return true
        } else if (state.isEmpty() || state == " " || state.equals(MyUtils.SELECT_STATE_NAME)) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "State can't be empty")
            return true
        } else if (total_amount.isEmpty()) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Total Amount can't be empty")
            return true
        } else if (deliveryAt.isEmpty()) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Delivert At can't be empty")
            return true
        } else if (aadharNo.isEmpty() || aadharNo.toString().length < 12) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Enter Valid Aadhar Number")
            return true
        } else if (gender.isEmpty()) {
            MessageUtils.showSnackBar(this@SpareInvoice, spr_add_invoice_snack_view, "Please Select Your Gender")
            return true
        }
        return false

    }

    private fun showDateDailog() {
        val c = Calendar.getInstance()
        var year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)
        var day = c.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(this@SpareInvoice, DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDate ->
            year = selectedYear
            month = selectedMonth
            day = selectedDate
            val dateFormatter = SimpleDateFormat(DateUtils.DD_MM_YYYY, Locale.US)
            val newDate = Calendar.getInstance()
            newDate.set(year, month, day)
            spr_customer_dob.setText(dateFormatter.format(newDate.time))
        }, year, month, day)
            c.add(Calendar.YEAR, -18)
        datePickerDialog.datePicker.maxDate = c.timeInMillis
        datePickerDialog.window!!.setWindowAnimations(R.style.grow)
        datePickerDialog.show()
    }

    private fun onFocus() {
        try {
            CustomEditText.focus(spr_edt_invoice_amt)
            CustomEditText.focus(spr_customer_name)
            CustomEditText.focus(spr_customer_address)
            CustomEditText.focus(spr_customer_address_2)
            CustomEditText.focus(spr_customer_locality)
            CustomEditText.focus(spr_add_pincode)
            CustomEditText.focus(spr_customer_email_id)
            CustomEditText.focus(spr_customer_occupation)
            CustomEditText.focus(spr_customer_delivery_at)
            CustomEditText.focus(spr_customer_aadhar_numer)
            CustomEditText.focus(spr_customer_subsidy_amount)
            CustomEditText.focus(spr_customer_total_amonut)
            CustomEditText.focus(spr_city)
            CustomEditText.focus(spr_customer_mobile_no)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun gstCalculationForInvoice(invoice: String, stateCode: String, stateItemTiNumber: String) {
        try {
            val inVoiceAmt = invoice.toDouble()
            println("invoice Amount"+inVoiceAmt+stateCode+"\n"+stateItemTiNumber)
            if (stateCode == stateItemTiNumber) {
                spr_same_state.visibility = View.VISIBLE
                spr_tip_cgst.visibility = View.GONE
                spr_tip_igst.visibility = View.GONE
                Log.d("gstsdsd", "" + spareinvoiceresponse?.data?.gst?.sparesIgst + " \n sgst" + spareinvoiceresponse?.data?.gst?.sparesSgst + " inVoiceAmt " + inVoiceAmt)
                val cgstCalc = inVoiceAmt * spareinvoiceresponse?.data?.gst?.sparesCgst.toString().toDouble() / 100
                val sgstCalc = inVoiceAmt * spareinvoiceresponse?.data?.gst?.sparesSgst.toString().toDouble() / 100

                Log.d("sgstCalcsdsd", "sgstCalc " + sgstCalc + "\n cgstCalc " + cgstCalc)
                spr_tip_cgst.setHint("CGST (" + spareinvoiceresponse?.data?.gst?.sparesCgst.toString() + "% )")
                spr_tax_cgst.setText(cgstCalc.toString())
                spr_tax_sgst.setText(sgstCalc.toString())
                spr_tip_cgst.setHint("SGST (" + spareinvoiceresponse?.data?.gst?.sparesSgst.toString() + "% )")

                var totalCalc: Double = ((cgstCalc + sgstCalc) + inVoiceAmt)


                spr_customer_total_amonut.setText("" + Utils.convertDoubleTwoDigits("" + totalCalc))

            } else {
                spr_same_state.visibility = View.GONE
                spr_tip_igst.visibility = View.VISIBLE
                val igstCalc = inVoiceAmt * spareinvoiceresponse?.data?.gst?.sparesIgst.toString().toDouble() / 100
                Log.d("gstsdsd_others", "" + spareinvoiceresponse?.data?.gst?.sparesIgst + " inVoiceAmt " + inVoiceAmt + " igstCalc " + igstCalc)
                spr_tax_igst.setText(igstCalc.toString())
                spr_tip_igst.setHint("IGST (" + spareinvoiceresponse?.data?.gst?.sparesIgst.toString() + "% )")
//                val subsidy = spr_customer_subsidy_amount.text.toString()
                var totalAmt = igstCalc + inVoiceAmt

                spr_customer_total_amonut.setText("" + Utils.convertDoubleTwoDigits("" + totalAmt))
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    private fun getandSetData() {
        try {
            if (spareinvoiceresponse?.success!!) {
                /**
                 * spinner Listener
                 *vehicle model SpinnerListerner
                 */
                spr_vehicle_model_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        val selectedmodel = spr_vehicle_model_sp.selectedItem.toString()
                        spr_edt_vehicle_model?.setText(selectedmodel)
                        if (selectedmodel == getString(R.string.select_vehicle) || selectedmodel == getString(R.string.no_data_avl)) {
                            Log.d(":Selected Item", "" + selectedmodel)
                        } else {
                            getColorFrmvehicle()
                        }
                    }

                }
                //vehicle serviceType SpinnerListerner
                spr_model_color_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        val selectedcolor = spr_model_color_sp.selectedItem.toString()
                        spr_edt_vehicle_model_color?.setText(selectedcolor)
                        if (selectedcolor==(getString(R.string.select_color)) ||selectedcolor==(getString(R.string.no_data_avl))) {
                            Log.d(":Selected Item", "" + selectedcolor)
                        } else {
                            getSpareFrmcolorandmodels()
                        }
                    }

                }
                //vehicle chassis number  SpinnerListerner
                spr_chassis_number_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {


                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        val selectedchassisno = spr_chassis_number_sp.selectedItem.toString()
                        spr_edt_chassis_number?.setText(selectedchassisno)
                        if (selectedchassisno.isNullOrEmpty()) {
                            Log.d(":Selected Item", "" + selectedchassisno)
                        } else {
//                        getColorFrmvehicle()
                        }
                    }

                }
                //customer State SpinnerListerner
                spr_state_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        MessageUtils.showToastMessage(this@SpareInvoice, "Please Select")
                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        val selectedstate = spr_state_sp.selectedItem.toString()
                        spr_stat_edt?.setText(selectedstate)
                        if (selectedstate.equals(getString(R.string.select_state))) {
                            Log.d(":Selected Item", "" + selectedstate)
                        } else {
                            val stateName = spr_state_sp.selectedItem.toString()
                            val stateCodeArrayList = ArrayList<String>()
                            try {
                                if (stateName != null && !stateName.equals(getString(R.string.select_state))) {
                                    spr_rl_state_code.visibility = View.VISIBLE
                                    spr_stat_edt.setText(stateName)
                                    for (stateItem in spareinvoiceresponse?.data?.statelists!!) {
                                        val names = stateItem?.stateName
                                        if (names.equals(stateName)) {
                                            stateCodeArrayList.add(stateItem?.stateCode!!)
                                            spr_stat_code_edt.setText(stateItem?.tinNumber.toString())
                                            if (spareinvoiceresponse?.data?.enquiry?.assignedperson != null) {
                                                val state = spareinvoiceresponse?.data?.enquiry?.assignedperson?.state.toString()
                                                val stateCode = spareinvoiceresponse?.data?.enquiry?.assignedperson?.statecode.toString()
                                                Log.d("aslkdlsjd", "$state stateCode $stateCode")
                                                val invoice = spr_edt_invoice_amt.text.toString()
                                                if (!invoice.isEmpty()) {
                                                    gstCalculationForInvoice(invoice, stateCode, stateItem.tinNumber.toString())
                                                } else {
                                                    MessageUtils.showToastMessage(this@SpareInvoice, "Enter Invoice Amount")
                                                    spr_edt_invoice_amt.setError("Enter Invoice Amount")
                                                    spr_edt_invoice_amt.requestFocus()
                                                }

                                            }
                                        }
                                    }
                                    //state_code_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, stateCodeArrayList)

                                } else {
                                    spr_stat_edt.setText(" ")
                                    spr_rl_state_code.visibility = View.GONE
                                }
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        }
                    }

                }
                //customer id Proof spinner Listener
                spr_id_proof_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        MessageUtils.showToastMessage(this@SpareInvoice, "Please Select")
                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        val selectedproof = spr_id_proof_sp.selectedItem.toString()
                        spr_customer_proof?.setText(selectedproof)
                        if (selectedproof.equals(getString(R.string.select_proof))) {
                            Log.d(":Selected Item", "" + selectedproof)
                        } else {
//                        getColorFrmvehicle()
                        }
                    }

                }
                //  Customer Address Proof spinner Listener
                spr_address_proof_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        MessageUtils.showToastMessage(this@SpareInvoice, "Please Select");
                    }

                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        val selectedaddressproof = spr_address_proof_sp.selectedItem.toString()
                        spr_customer_address_proof?.setText(selectedaddressproof)
                        if (selectedaddressproof.equals(getString(R.string.select_proof))) {
                            Log.d(":Selected Item", "" + selectedaddressproof)
                        } else {
//                        getColorFrmvehicle()
                        }
                    }

                }

                /***spr_state_sp
                 * get  and set the vehicle List
                 * */
                Log.d("vehicle Array", "" + spareinvoiceresponse?.data?.vehiclemodels)
                if (spareinvoiceresponse?.data?.vehiclemodels != null) {

                    var postion = 0
                    for (vehicle in spareinvoiceresponse?.data?.vehiclemodels!!) {
                        vehicleModelsList.add(vehicle?.vehicleModel!!)
                    }
                    for (i in 1..vehicleModelsList.size - 1) {
                        if (vehicleModelsList.get(i).equals(spareinvoiceresponse?.data?.enquiry?.vehicleModel)) {
                            postion = i
                        }
                    }
                    vehicleModelsList.add(0, getString(R.string.select_vehicle))
                    vehicleModelAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, vehicleModelsList)
                    spr_vehicle_model_sp.adapter = vehicleModelAdapter
                    spr_vehicle_model_sp.setSelection(postion + 1)
                }
                /**
                 * get and set the  serviceType
                 */
                Log.d("colors Array", "" + spareinvoiceresponse?.data?.colors)
                var countColors = 0
                if (spareinvoiceresponse?.data?.colors != null) {
                    countColors = spareinvoiceresponse?.data?.colors?.size!!
                }

                if (countColors != 0) {
                    colorList.clear()
//                    var i = 0
                    var postion = 0
                    for (color in spareinvoiceresponse?.data?.colors!!) {
                        colorList.add(color?.color!!)
                    }
                    for (i in 1..colorList.size - 1) {
                        if (colorList.get(i).equals(spareinvoiceresponse?.data?.enquiry?.vehicleColor)) {
                            postion = i
                        }
                    }

                    colorList.add(0, getString(R.string.select_color))
                    vehicleColorAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, colorList)
                    spr_model_color_sp.adapter = vehicleColorAdapter
                    spr_model_color_sp.setSelection(postion + 1)
                }else{
                    colorList.add(0, getString(R.string.no_data_avl))
                    vehicleColorAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, colorList)
                    spr_model_color_sp.adapter = vehicleColorAdapter

                }
                /**
                 * get and set chassis number
                 */
                /*
                Log.d("chassis number ", "" + spareinvoiceresponse?.data?.enquiry?.chassisNo)
                if (spareinvoiceresponse?.data?.enquiry?.chassisNo.isNullOrEmpty()) {
                    Log.d("chassis number ", "" + spareinvoiceresponse?.data?.enquiry?.chassisNo)
                    chassisNoList.add(getString(R.string.no_data_avl))
                    chassisNumberAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, chassisNoList)
                    spr_chassis_number_sp.adapter = chassisNumberAdapter
                } else {
                    chassisNoList.add(spareinvoiceresponse?.data?.enquiry?.chassisNo!!)

                    chassisNumberAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, chassisNoList)
                    spr_chassis_number_sp.adapter = chassisNumberAdapter
                }
    */
                /**
                 * Set the Customer Basic Details..
                 */
                if (spareinvoiceresponse?.data?.enquiry?.customerName.isNullOrEmpty()) {
                    spr_customer_name.setText(getString(R.string.no_data_avl))
                } else {
                    spr_customer_name.setText(spareinvoiceresponse?.data?.enquiry?.customerName)
                }
                if (spareinvoiceresponse?.data?.enquiry?.address1.isNullOrEmpty()) {
                    spr_customer_address.setText(getString(R.string.no_data_avl))
                } else {
                    spr_customer_address.setText(spareinvoiceresponse?.data?.enquiry?.address1)
                }
                if (spareinvoiceresponse?.data?.enquiry?.address2.isNullOrEmpty()) {
                    spr_customer_address_2.setText(getString(R.string.no_data_avl))
                } else {
                    spr_customer_address_2.setText(spareinvoiceresponse?.data?.enquiry?.address2)
                }
                if (spareinvoiceresponse?.data?.enquiry?.locality.isNullOrEmpty()) {
                    spr_customer_locality.setText(getString(R.string.no_data_avl))
                } else {
                    spr_customer_locality.setText(spareinvoiceresponse?.data?.enquiry?.locality)
                }

                if (spareinvoiceresponse?.data?.enquiry?.city.isNullOrEmpty()) {
                    spr_city.setText(getString(R.string.no_data_avl))
                } else {
                    spr_city.setText(spareinvoiceresponse?.data?.enquiry?.city)
                }
                if (spareinvoiceresponse?.data?.enquiry?.pincode.isNullOrEmpty()) {
                    spr_add_pincode.setText("N/A")
                } else {
                    spr_add_pincode.setText(spareinvoiceresponse?.data?.enquiry?.pincode)
                }
                if (spareinvoiceresponse?.data?.enquiry?.invoiceValue.toString().isEmpty()) {
                    spr_edt_invoice_amt.setText("N/A")
                } else {
                    spr_edt_invoice_amt.setText(spareinvoiceresponse?.data?.enquiry?.invoiceValue.toString())
                }
                /**
                 * get and set SateList
                 */
                Log.d("state Lists", "" + spareinvoiceresponse?.data?.statelists)
                if (spareinvoiceresponse?.data?.statelists != null) {
                    for (state in spareinvoiceresponse?.data?.statelists!!) {
                        stateLists.add(state?.stateName!!)
                    }
                    stateLists.add(0, getString(R.string.select_state))
                    statelistAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, stateLists)
                    spr_state_sp.adapter = statelistAdapter
                    spr_state_sp.setSelection(stateLists.indexOf(spareinvoiceresponse?.data?.enquiry?.state))
                 }
                /**
                 * get and set the Id Proof and Address Proof
                 *
                 */
                Log.d("Proof Lists", "" + spareinvoiceresponse?.data?.proofs)
                if (spareinvoiceresponse?.data?.proofs == null) {
                    spr_stat_edt.setText(getString(R.string.no_data_avl))
                    Log.d("state Lists", "" + spareinvoiceresponse?.data?.proofs)
                } else {
//                    var postion = 0
                    for (proof in spareinvoiceresponse?.data?.proofs!!) {
                        idproofsList.add(proof?.proof!!)
                        addressproofsList.add(proof.proof)//customer address proof List adding
                    }
                    /* for(i in 1.. stateLists.size-1){
                         if(idproofsList.get(i).equals(spareinvoiceresponse?.data?.enquiry?.assignedPerson?.)){
                             postion=i
                         }
                     }*/
                    idproofsList.add(0, getString(R.string.select_proof))
                    addressproofsList.add(0, getString(R.string.select_proof))
                    customerProofAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, idproofsList)
                    spr_id_proof_sp.adapter = customerProofAdapter
//                spr_state_sp.setSelection(postion+1)


                    /**
                     * customer Address proof is also....same proofs
                     */
                    customerAddressProofAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, addressproofsList)
                    spr_address_proof_sp.adapter = customerAddressProofAdapter

                }

                /***
                 **  set Customer Contact Details
                 **/
                if (spareinvoiceresponse?.data?.enquiry?.assignedperson?.email.isNullOrEmpty()) {
                    spr_customer_email_id.setText(getString(R.string.no_data_avl))
                } else {
                    spr_customer_email_id.setText(spareinvoiceresponse?.data?.enquiry?.assignedperson?.email!!)
                }
                if (spareinvoiceresponse?.data?.enquiry?.assignedperson?.mobile.isNullOrEmpty()) {
                    spr_customer_mobile_no.setText(getString(R.string.no_data_avl))
                } else {
                    spr_customer_mobile_no.setText(spareinvoiceresponse?.data?.enquiry?.assignedperson?.mobile!!)
                }
                /***
                 * invoice Amount Calculation for Spares
                 */
                spr_edt_invoice_amt.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {

                    }

                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        try {
                            val inputValues = p0.toString()
                            Log.d("inputValuessdsd", "" + inputValues)
                            if (!inputValues.isEmpty()) {
                                val stateName = spr_state_sp.selectedItem.toString()
                                if (!stateName.equals(MyUtils.SELECT_STATE_NAME)) {
                                    val stateCodeSp = spr_stat_code_edt.text.toString()
                                    if (!stateCodeSp.isEmpty()) {
                                        val stateCode = spareinvoiceresponse?.data?.enquiry?.assignedperson?.statecode.toString()
                                        gstCalculationForInvoice(inputValues, stateCode, stateCodeSp)
                                    }
                                }
                            } else {
                                val stateName = state_sp.selectedItem.toString()
                                if (!stateName.equals(MyUtils.SELECT_STATE_NAME)) {
                                    val stateCodeSp = spr_stat_code_edt.text.toString()
                                    if (!stateCodeSp.isEmpty()) {
                                        val stateCode = spareinvoiceresponse?.data?.enquiry?.assignedperson?.statecode.toString()
                                        gstCalculationForInvoice("0", stateCode, stateCodeSp)
                                    }
                                }
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }

                })
                /**
                 * get and set the  spare ArrayList
                 */
                try {
                    Log.d("sparessArray ", "" + spareinvoiceresponse?.data?.spareinventory?.size)
                    Log.d("sparess", "000")
                    if (spareinvoiceresponse?.data?.spareinventory != null) {
                        Log.d("sparess", "22222")
                        sparestringList.clear()
                        var postion = 0
                        for (spare in spareinvoiceresponse?.data?.spareinventory!!) {
                            sparestringList.add(spare?.spareName!!)
                        }
                        Log.d("sdsd258", "" + sparestringList.size)
                        if (sparestringList.size != 0) {

                            Log.d("sdsd258", "000completed")
                            sparestringList.add(0, getString(R.string.select_spare))
                            spareArrayAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, sparestringList)
                            spr_sp_spare.adapter = spareArrayAdapter
                            spr_sp_spare.setSelection(sparestringList.indexOf(spareinvoiceresponse?.data?.enquiry?.sparesName))
                        } else {
                            Log.d("sdsd258", "111completed")
                            sparestringList.add(0, getString(R.string.no_data_avl))
                            spareArrayAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, sparestringList)
                            spr_sp_spare.adapter = spareArrayAdapter
                        }
                        Log.d("sdsd258", "222completed")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                /**
                 * Set the Default Spare in the SPare List
                 */
                try{
                    if(spareinvoiceresponse?.data?.sparelists != null  && spareinvoiceresponse?.data?.sparelists?.size!! > 0){
                                if(spareinvoiceresponse?.data?.spareinventory!= null && spareinvoiceresponse?.data?.spareinventory?.size!! > 0){
                                    /**
                                     * The set Default Spinner for Enquiry Values
                                     */
                                    for(i:Int in  0 until spareinvoiceresponse?.data?.sparelists?.size!!){
//                                            val spareName = spareinvoiceresponse?.data?.sparelists?.get(i)?.sparesId
                                             var  position =0
                                        for(j:Int in 0 until spareinvoiceresponse?.data?.spareinventory?.size!!){
                                            if(spareinvoiceresponse?.data?.spareinventory?.get(j)?.id  == spareinvoiceresponse?.data?.sparelists?.get(i)?.sparesId){
                                                position =j
                                            }
                                        }
                                        val spareName = spareinvoiceresponse?.data?.spareinventory?.get(position)?.spareName!!
         spareinvoicelistaddmoreList.add(SpareInvoiceadapterviewPojos(spareName, spareinvoiceresponse?.data?.sparelists?.get(i)?.qty.toString(),"",""))
                                    }
                                    addmorespare()
                                }else{
                                    MessageUtils.showSnackBar(this,mSnackView,"Spare Inventory is Empty")
                                }
                    }else{
//                        MessageUtils.showSnackBar(this,mSnackView,"Spare List is Empty")
                    }

                }catch (ex :Exception){
                    ex.printStackTrace();
                }

            } else {
                MessageUtils.showToastMessage(this, spareinvoiceresponse?.message)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun getSpareFrmcolorandmodels() {
        Log.d("spare Color List",""+spr_model_color_sp.selectedItem.toString())
        if(spr_model_color_sp.selectedItem.toString() == getString(R.string.no_data_avl) || spr_model_color_sp.selectedItem.toString() == getString(R.string.select_color)){
            Log.d("spare Color List",""+spr_model_color_sp.selectedItem.toString())
        }else{
            val map: HashMap<String, Any> = HashMap()
            map.put("userid", LocalSession.getUserInfo(this, KEY_ID))
            map.put("vehicle_model", spr_vehicle_model_sp.selectedItem.toString())
            map.put("vehicle_color", spr_model_color_sp.selectedItem.toString())
            if (callApi?.isNetworkConnected!!) {
//                dialog = MessageUtils.showDialog(this@SpareInvoice)
                Log.d("VehicleColorMap",""+map.toString())
                val calInterface = callApi?.connectRetro("POST","getsparestock")
                val callback   = calInterface?.call_post_get("getsparestock","Bearer "+LocalSession.getUserInfo(this, KEY_TOKEN),map)
                callback?.enqueue( object : Callback<GetspareinvoiceResList>{
                    override fun onFailure(call: Call<GetspareinvoiceResList>, t: Throwable) {
//                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(this@SpareInvoice,mSnackView,t.localizedMessage)
                    }

                    override fun onResponse(call: Call<GetspareinvoiceResList>, response: Response<GetspareinvoiceResList>) {
//                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                getspareinvoiceResList =response.body()
                            if(getspareinvoiceResList?.success!!){
                                try{
                                    if (getspareinvoiceResList?.data?.spareinventory != null && getspareinvoiceResList?.data?.spareinventory!!.size >=0) {
                                     sparestringList.clear()
                                        for (spare in getspareinvoiceResList?.data?.spareinventory!! ) {
                                            sparestringList.add(spare?.spareName!!)
                                        }
                                        spareArrayAdapter = ArrayAdapter(this@SpareInvoice, R.layout.spinnertext, R.id.text1, sparestringList)
                                        spr_sp_spare.adapter = spareArrayAdapter
                                    } else {
                                        MessageUtils.showToastMessage(this@SpareInvoice, getspareinvoiceResList?.message)
                                    }
                                }catch (ex:Exception){
                                    ex.printStackTrace()
                                }

                            }else{
                                MessageUtils.showSnackBar(this@SpareInvoice,mSnackView,response.message())
                            }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }
                        }else{
                            MessageUtils.showSnackBar(this@SpareInvoice,mSnackView,response.message())
                        }
                    }

                })
            } else {
                MessageUtils.showSnackBarAction(this@SpareInvoice,mSnackView,getString(R.string.check_internet))
            }
        }


    }

    private fun getColorFrmvehicle() {
        if(spr_vehicle_model_sp.selectedItem.toString().equals(getString(R.string.no_data_avl))  || spr_vehicle_model_sp.selectedItem.toString().equals(getString(R.string.no_data_avl))){
            Log.d("GetColorList",""+spr_vehicle_model_sp.selectedItem.toString())
        }else{
            Log.d("GetColorList",""+spr_vehicle_model_sp.selectedItem.toString())
            val map: HashMap<String, Any> = HashMap()
            map.put("userid", LocalSession.getUserInfo(this, KEY_ID))
            map.put("vehicle_model", spr_vehicle_model_sp.selectedItem.toString())
            if (callApi?.isNetworkConnected!!) {
                dialog = MessageUtils.showDialog(this@SpareInvoice)
                callApi?.callApi("getsparecolor", "POST", map, 0)
            } else {
//            MessageUtils.showToastMessage(this, getString(R.string.check_internet))
                MessageUtils.showSnackBarAction(this@SpareInvoice,mSnackView,getString(R.string.check_internet))
            }
        }

    }

    override fun apiCallBackOverRideMethod(response: JSONObject?, position: Int, success: Boolean, message: String?, apiCallPath: String?) {
        if (apiCallPath.equals("getsparecolor")) {
            MessageUtils.dismissDialog(dialog)
            if (success) {
                if (response != null) {
                    if (!response.getJSONArray("colors").toString().isNullOrEmpty() && response.getJSONArray("colors").length()>0) {
                        var colorobjects = Gson().fromJson<ArrayList<ColorsItem>>(response.getJSONArray("colors").toString(), object : TypeToken<ArrayList<ColorsItem>>() {}.type)
                        colorList.clear()
                        for (color in colorobjects) {
                            colorList.add(color.color!!)
                        }

                    } else {
                        colorList.clear()
                        colorList.add(getString(R.string.no_data_avl))
                    }
                    vehicleColorAdapter = ArrayAdapter(this@SpareInvoice, R.layout.spinnertext, R.id.text1, colorList)
                    spr_model_color_sp.adapter = vehicleColorAdapter
                } else {
                    MessageUtils.showToastMessage(this, message);
                }

            } else {
                val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
                toast.setGravity(Gravity.HORIZONTAL_GRAVITY_MASK, 0, 0)
                toast.show()
            }
        } else if (apiCallPath.equals("getsparestock")) {
            MessageUtils.dismissDialog(dialog)
            Log.d("get SpareStock",""+apiCallPath)
            if (success) {
                if (response != null) {
                    if (response.getJSONArray("spareinventory") != null && response.getJSONArray("spareinventory").length()>=0) {
                        var colorobjects = Gson().fromJson<ArrayList<SpareinventoryItem>>(response.getJSONArray("spareinventory").toString(), object : TypeToken<ArrayList<SpareinventoryItem>>() {}.type)
                        sparestringList.clear()
                        for (spare in colorobjects) {
                            sparestringList.add(spare.spareName!!)
                        }
                        spareArrayAdapter = ArrayAdapter(this@SpareInvoice, R.layout.spinnertext, R.id.text1, sparestringList)
                        spr_sp_spare.adapter = spareArrayAdapter
                    } else {
                        MessageUtils.showToastMessage(this, message)
                    }

                } else {
                    MessageUtils.showToastMessage(this, message)
                }
            } else {
                MessageUtils.showToastMessage(this, message)
            }
        } else {
            MessageUtils.showToastMessage(this, message)

        }
    }

    override fun callinterfacePosition(position: Int) {
        Log.d("postionvaluew",""+position)
/*
        val spare = spareinvoicelistaddmoreList.get(position)
        Log.d("postionspare",""+spare.spareNam)
         if (sparestringList.size <= 1 || sparestringList.get(0).equals(getString(R.string.no_data_avl))) {
           sparestringList.clear()
       }
       sparestringList.add(spare.spareNam)
       spareArrayAdapter?.notifyDataSetChanged()
        Log.d("position",""+spareinvoicelistaddmoreList.size)
        if(spareinvoicelistaddmoreList.size!=1){
            spareinvoicelistaddmoreList.removeAt(position)
        }else{
        spareinvoicelistaddmoreList.clear()
        }*/

    }
}


