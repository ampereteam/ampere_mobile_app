package com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.SalesPojos;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class SalesAdapter extends RecyclerView.Adapter<SalesAdapter.MyHolder> {
    Context context;
    ArrayList<SalesPojos> salesPojosArrayList;
    ConfigClickEvent configposition;

    public SalesAdapter(Context context, ArrayList<SalesPojos> salesPojosArrayList, ConfigClickEvent configposition) {
        this.context = context;
        this.salesPojosArrayList = salesPojosArrayList;
        this.configposition = configposition;
        //System.out.println("adapterList Value"+salesPojosArrayList.get(0).getRequestby().getMobile());
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_sales_list, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, final int position) {
        try{
        holder.sNo.setText(String.valueOf(position + 1));
        holder.invoiceID.setText(salesPojosArrayList.get(position).getInvoiceId().trim());
        holder.chassNo.setText(salesPojosArrayList.get(position).getChassisNo());
        holder.customerName.setText(salesPojosArrayList.get(position).getCustomerName());
        holder.mobileNo.setText(salesPojosArrayList.get(position).getMobile());
        holder.customerName.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.customerName.setSelected(true);
        holder.customerName.setSingleLine(true);
        holder.invoiceID.setSingleLine(true);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configposition.connectposition(position, salesPojosArrayList.size());
            }
        });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return salesPojosArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView sNo, mobileNo, customerName, chassNo, invoiceID;
        RelativeLayout relativeLayout;

        public MyHolder(View itemView) {
            super(itemView);
            sNo = itemView.findViewById(R.id.sNum);
            mobileNo = itemView.findViewById(R.id.inDEnt_id);
            customerName = itemView.findViewById(R.id.reStfr);
            chassNo = itemView.findViewById(R.id.odr_qnty);
            invoiceID = itemView.findViewById(R.id.sTus);
            relativeLayout = itemView.findViewById(R.id.sales_details);
        }
    }
}
