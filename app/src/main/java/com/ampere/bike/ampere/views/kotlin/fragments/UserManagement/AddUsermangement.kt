package com.ampere.bike.ampere.views.kotlin.fragments.UserManagement



import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.CustomNavigationDuo.snackVIew
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_ID
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.fragment.Home
import com.ampere.bike.ampere.views.fragment.indent.VehicleListPojos.DataList
import  com.ampere.bike.ampere.views.kotlin.fragments.UserManagement.UserManageList.Companion.userListRes

import com.ampere.vehicles.R

import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern

class AddUsermangement : Fragment() {

    var  edtUsrMobile :EditText?=null
    var  edtUsraddress :EditText?=null
    var  edtUsrcity :EditText?=null
    var  edtUsrMailId :EditText?=null
    var  edtUsrLoginID :EditText?=null
    var  edtUsrPasswd :EditText?=null
    var  edtGSTNO :EditText?=null
    var  llStateCdeGSt :LinearLayout?=null

    var spUsrtype :Spinner ?=null
    var spState :Spinner ?=null
    var spStatecode :Spinner ?=null
    var imgPassHide :ImageView ?=null
    var imgPassShow :ImageView ?=null
    var edtUsrtype :EditText ?=null
    var edtState :EditText ?=null
    var edtStatecode :EditText ?=null
    var edtUsrName :EditText ?=null
    var btnCancel :Button ?=null
    var btnSubmit:Button ?=null
    var  usrTypeSTRList : ArrayList<String> = ArrayList()
    var  usrTypeIdSTRList : ArrayList<String> = ArrayList()
    var usrTypeSTRAdapter:ArrayAdapter<String> ?= null

    var stateSTRLIST :ArrayList<String> = ArrayList()
    var statecodeSTRLIST :ArrayList<String> = ArrayList()
    var stateSTRAdapter :ArrayAdapter<String> ?= null
    var statecodeSTRAdapter :ArrayAdapter<String> ?= null

    var utilsCallApi :UtilsCallApi ?= null
    var dialog :Dialog?=null
    var stateresList :DataList ?=null
    var patternStr ="[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    var userID:String =""
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        (activity as CustomNavigationDuo).disbleNavigationView(getString(R.string.user_mgnt))
        return inflater.inflate(R.layout.fragment_usermangement, container, false)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try{
            utilsCallApi = UtilsCallApi(activity)
            dialog = Dialog(activity)
            spUsrtype = view.findViewById(R.id.sp_usr_type)
            spState = view.findViewById(R.id.sp_state)
            spStatecode = view.findViewById(R.id.sp_state_cde)
            edtUsrtype = view.findViewById(R.id.edt_usr_type)
            edtState = view.findViewById(R.id.edt_state)
            llStateCdeGSt = view.findViewById(R.id.ll_sta_cde_gst)
            edtGSTNO = view.findViewById(R.id.edt_usr_gst_no)
            edtStatecode = view.findViewById(R.id.edt_state_cde)
            imgPassHide = view.findViewById(R.id.imgn_pass_hide)
            imgPassShow = view.findViewById(R.id.imgn_pass_show)
            edtUsrPasswd = view.findViewById(R.id.edt_usr_passwd)
            edtUsrLoginID = view.findViewById(R.id.edt_usr_login_id)
            edtUsrMailId = view.findViewById(R.id.edt_usr_email_id)
            edtUsrcity = view.findViewById(R.id.edt_usr_city)
            edtUsrMobile= view.findViewById(R.id.edt_usr_mobile)
            edtUsraddress = view.findViewById(R.id.edt_usr_address)
            edtUsrName = view.findViewById(R.id.edt_usr_name)
            btnCancel = view.findViewById(R.id.btn_usr_cancel)
            btnSubmit = view.findViewById(R.id.btn_usr_submit)
            usrTypeSTRList.clear()
            usrTypeSTRList.add(0,"Select The User Type")
            if(UserManageList.userListRes!=null){
                for(userType in UserManageList.userListRes?.data?.usertype!!){
                    usrTypeSTRList.add(userType?.userType!!)
                    usrTypeIdSTRList.add(userType.id.toString())
                }
            }

            usrTypeSTRAdapter = ArrayAdapter(activity,R.layout.spinnertext,R.id.text1,usrTypeSTRList)
            spUsrtype?.adapter =usrTypeSTRAdapter

            /**ONCLick Listener**/
            imgPassHide?.setOnClickListener {
                imgPassShow?.visibility= View.VISIBLE
                imgPassHide?.visibility = View.GONE
                val string = edtUsrPasswd?.text.toString()
                Log.d("TypedText",""+string+"\tINPT TYPE"+edtUsrPasswd?.inputType)
                edtUsrPasswd?.inputType  = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                edtUsrPasswd?.setText(string)
                edtUsrPasswd?.setSelection(string.length)

            }
            imgPassShow?.setOnClickListener {
                imgPassShow?.visibility =View.GONE
                imgPassHide?.visibility =View.VISIBLE
                val string = edtUsrPasswd?.text.toString()
                Log.d("TypedText",""+string + "\t INPUTTYPE"+edtUsrPasswd?.inputType)
                edtUsrPasswd?.inputType  = 129
                edtUsrPasswd?.setText(string)
                edtUsrPasswd?.setSelection(string.length)
            }
            btnCancel?.setOnClickListener {
                try{
                    /**When We want to BAck Press ALwalys Handle with  Only Actiivty**/
                    (activity as CustomNavigationDuo).onBackPressed()
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
            btnSubmit?.setOnClickListener {
                try{
                    val map: HashMap<String, Any> = HashMap()
                    if(spUsrtype?.selectedItem!=null) {
                        if(spUsrtype?.selectedItem.toString().equals("Employee")) {
                            if (validateFormDataempt(edtUsrName?.text.toString(), spUsrtype?.selectedItem.toString(), edtUsrLoginID?.text.toString(), edtUsrPasswd?.text.toString(), edtUsrMailId?.text.toString()
                                            , edtUsrcity?.text.toString(), edtUsrMobile?.text.toString(), edtUsraddress?.text.toString())) {
                                map["user_type"] = userID.toInt()
                                map["name"] = edtUsrName?.text.toString()
                                map["username"] = edtUsrLoginID?.text.toString()
                                map["password"] = edtUsrPasswd?.text.toString()
                                map["email"] = edtUsrMailId?.text.toString()
                                map["mobile"] = edtUsrMobile?.text.toString()
                                map["address"] = edtUsrcity?.text.toString()
                                map["address1"] = edtUsraddress?.text.toString()
                                Log.d("Usertype", "" + spUsrtype?.selectedItem.toString())
                                addUsertoSVR(map)
                            }
                        }

                        else{
                            if(validateFormData(edtUsrName?.text.toString(), spUsrtype?.selectedItem.toString(), edtUsrLoginID?.text.toString(), edtUsrPasswd?.text.toString(), edtUsrMailId?.text.toString()
                                            , edtUsrcity?.text.toString(), edtUsrMobile?.text.toString(), edtUsraddress?.text.toString(), spStatecode?.selectedItem.toString(), spState?.selectedItem.toString()
                                            , edtGSTNO?.text.toString())){
                                map["user_type"] = userID.toInt()
                                map["name"] = edtUsrName?.text.toString()
                                map["username"] = edtUsrLoginID?.text.toString()
                                map["password"] = edtUsrPasswd?.text.toString()
                                map["email"] = edtUsrMailId?.text.toString()
                                map["mobile"] = edtUsrMobile?.text.toString()
                                map["address"] = edtUsrcity?.text.toString()
                                map["address1"] = edtUsraddress?.text.toString()
                                map["state"] = spState?.selectedItem.toString()
                                map["statecode"] = spStatecode?.selectedItem.toString()
                                map["gst_no"] = edtGSTNO?.text.toString()
                                addUsertoSVR(map)
                            }

                            }

                        }
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
                /** Spinner Listener**/
            spStatecode?.isEnabled =false
            spStatecode?.isClickable =false
            spState?.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    try{
                      Log.d("SElected Item",""+parent)

                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    try{
                        val selectedItem  =spState?.selectedItem.toString()
                        edtState?.setText(selectedItem)
                        val stateCodeSTRLIst:ArrayList<String> = ArrayList()
                        stateCodeSTRLIst.clear()
                        if(selectedItem.equals(getString(R.string.select_state))){
                            Log.d("SElected Item",""+selectedItem)
                            stateCodeSTRLIst.add(getString(R.string.no_data_avl))
                            if(spUsrtype?.selectedItem.toString().equals("Select The User Type")) {
                                MessageUtils.showSnackBar(activity, snackVIew,"Select The User Type")
                            }else{
                                MessageUtils.showSnackBar(activity, snackVIew, getString(R.string.select_state))
                            }
                        }else{
                            stateCodeSTRLIst.add(statecodeSTRLIST[position])
                        }
                        statecodeSTRAdapter = ArrayAdapter(activity,R.layout.spinnertext,R.id.text1,stateCodeSTRLIst)
                        spStatecode?.adapter = statecodeSTRAdapter
                    }catch (ex:Exception){

                    }
                }

            }
            spStatecode?.onItemSelectedListener = object  :AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    try{
                        Log.d("Nothing  selected",""+parent?.selectedItem)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    try{
                        edtStatecode?.setText(spStatecode?.selectedItem.toString())
                        Log.d("SelectedItem"," "+spStatecode?.selectedItem.toString())
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

            }
            spUsrtype?.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    try{
                        Log.d("Nothing Is Selected",""+parent?.selectedItem)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    val selectedItem = spUsrtype?.selectedItem.toString()
                    edtUsrtype?.setText( selectedItem)
                    if(selectedItem.equals("Select The User Type")){
                        Log.d("SelectedItem",""+selectedItem)
                    }else{
                        if(selectedItem.equals("Employee")){
                            llStateCdeGSt?.visibility = View.GONE
                        }else{
                            llStateCdeGSt?.visibility =View.VISIBLE
                        }
                        Log.d("SelectedItem",""+selectedItem)
                    }
                    if(position !=0){
                        userID =usrTypeIdSTRList[position-1]
                    }
                    Log.d("SelectedUSERID",""+userID)
                }

            }
            if(utilsCallApi?.isNetworkConnected!!){

                /**Getting State  and State Code **/
                GetStatecodefrmSVR()
            }else {
                MessageUtils.showSnackBar(activity, snackVIew,getString(R.string.check_internet))
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }




    private fun addUsertoSVR(map: HashMap<String, Any>) {
        try{
            //adduser
            map["userid"]= LocalSession.getUserInfo(activity, KEY_ID)
            dialog = MessageUtils.showDialog(activity)
            val callInterface =utilsCallApi?.connectRetro("POST","adduser")
            val callback = callInterface?.call_post("adduser","Bearer "+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
            callback?.enqueue(object  : Callback<ResponseBody>{
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    try{
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(activity, snackVIew,t.localizedMessage)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    try{
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                val jsonobject = JSONObject(response.body()?.string())
                                val success = jsonobject.getBoolean("success")
                                val message = jsonobject.getString("message")
                                if(success){
                                    setEmptyForm()
//                                    val home :Fragment = Home()
//                                    MyUtils.passFragmentWithoutBackStack(activity,home)
                                    (activity as CustomNavigationDuo).onBackPressed()
                                }else{
                                    MessageUtils.showSnackBar(activity, snackVIew,message)
                                }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }

                        }else{
                            MessageUtils.showSnackBar(activity, snackVIew,response.message())
                        }
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

            })
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    private fun setEmptyForm() {
        try{
            spUsrtype?.setSelection(0)
            edtGSTNO?.setText("")
            edtUsraddress?.setText("")
            edtUsrMobile?.setText("")
            edtUsrcity?.setText("")
            edtUsrMailId?.setText("")
            edtUsrPasswd?.setText("")
            edtUsrLoginID?.setText("")
            edtUsrName?.setText("")
            spState?.setSelection(0)
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun validateFormData(userName: String,userType : String, login: String, password: String, mailId: String, city: String, mobile:
    String, address: String, statecode: String, state: String, gstno: String): Boolean {
        try{

            if(userType.isEmpty() || userType.equals("Select The User Type")){
                MessageUtils.showSnackBar(activity, snackVIew,"Select The User Type")
                return false
            }else if (userName.isEmpty() ){
                MessageUtils.showSnackBar(activity, snackVIew,"UserName Can't Be Empty")
                return false
            }else if (login.isEmpty() ||!Pattern.matches(patternStr, login) ){
                MessageUtils.showSnackBar(activity, snackVIew,"Invalid Login ID")
                return false
            }else if (password.isEmpty()){
                MessageUtils.showSnackBar(activity, snackVIew,"Password Can't Be Empty")
                return false
            }else if (mailId.isEmpty() ||!Pattern.matches(patternStr, mailId) ){
                MessageUtils.showSnackBar(activity, snackVIew,"Invalid mail id")
                return false
            }else if (mobile.isEmpty() || mobile.length!=10){
                MessageUtils.showSnackBar(activity, snackVIew,"Mobile Number is Invalid")
                return false
            }else if (city.isEmpty() ){
                MessageUtils.showSnackBar(activity, snackVIew,"City Can't Be Empty")
                return false
            }else if (address.isEmpty() ){
                MessageUtils.showSnackBar(activity, snackVIew,"Address Can't Be Empty")
                return false
            }
            else if (state.isEmpty()  || state.equals(getString(R.string.select_state))){
                MessageUtils.showSnackBar(activity, snackVIew,getString(R.string.select_state))
                return false
            }else if (statecode.isEmpty() || statecode.equals(getString(R.string.no_data_avl)) ){
                MessageUtils.showSnackBar(activity, snackVIew,"Invalid  State Code")
                return false
            }else if (gstno.isEmpty() ){
                MessageUtils.showSnackBar(activity, snackVIew,"GST Can't Be Empty")
                return false
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
        return  true
    }
    private fun validateFormDataempt(userName: String,userType : String, login: String, password: String, mailId: String, city: String, mobile:
    String, address: String): Boolean {
        try{

            if(userType.isEmpty() || userType.equals("Select The User Type")){
                MessageUtils.showSnackBar(activity, snackVIew,"Select The User Type")
                return false
            }else if (userName.isEmpty() ){
                MessageUtils.showSnackBar(activity, snackVIew,"UserName Can't Be Empty")
                return false
            }else if (login.isEmpty() ||!Pattern.matches(patternStr, login) ){
                MessageUtils.showSnackBar(activity, snackVIew,"Invalid Login ID")
                return false
            }else if (password.isEmpty()){
                MessageUtils.showSnackBar(activity, snackVIew,"Password Can't Be Empty")
                return false
            }else if (mailId.isEmpty() ||!Pattern.matches(patternStr, mailId) ){
                MessageUtils.showSnackBar(activity, snackVIew,"Invalid mail id")
                return false
            }else if (mobile.isEmpty() || mobile.length!=10){
                MessageUtils.showSnackBar(activity, snackVIew,"Mobile Number is Invalid")
                return false
            }else if (city.isEmpty() ){
                MessageUtils.showSnackBar(activity, snackVIew,"City Can't Be Empty")
                return false
            }else if (address.isEmpty() ){
                MessageUtils.showSnackBar(activity, snackVIew,"Address Can't Be Empty")
                return false
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
        return  true
    }
    private fun GetStatecodefrmSVR() {
        try{
            val callInterface  =MyUtils.getRetroService()
            dialog =MessageUtils.showDialog(activity)
            val callResponse  = callInterface.getDataList("Bearer "+LocalSession.getUserInfo(activity,KEY_TOKEN))
            callResponse.enqueue(object : Callback<DataList> {
                override fun onFailure(call: Call<DataList>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    MessageUtils.showSnackBar(activity,snackVIew,t.localizedMessage)
                }

                override fun onResponse(call: Call<DataList>, response: Response<DataList>) {
                 MessageUtils.dismissDialog(dialog)
                    if(response.isSuccessful){
                        try{
                            stateresList = response.body()
                            if(stateresList?.isSuccess!!){
                                if(stateresList?.data?.states == null ||stateresList?.data?.states?.size!! <=0 ){
                                    MessageUtils.showSnackBar(activity, snackVIew,"States are  not Available")
                                }else{
                                    setStateToAdapter()
                                }

                            }else{
                                MessageUtils.showSnackBar(activity, snackVIew,stateresList?.message)
                            }
                        }catch (ex:Exception){
                         ex.printStackTrace()
                        }
                    }else{
                        MessageUtils.showSnackBar(activity, snackVIew,response.message())
                    }
                }

            })
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
    /**Set The Sates To Adapter**/
    private fun setStateToAdapter() {
        try{
            stateSTRLIST.clear()
            statecodeSTRLIST.clear()
            for(state in stateresList?.data?.states!!){
                stateSTRLIST.add(state.stateName)
                statecodeSTRLIST.add(state.tinNumber.toString())
            }
            stateSTRLIST.add(0,getString(R.string.select_state))
            statecodeSTRLIST.add(0,getString(R.string.select_state_code))
            stateSTRAdapter = ArrayAdapter(activity,R.layout.spinnertext,R.id.text1,stateSTRLIST)
            spState?.adapter = stateSTRAdapter
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MessageUtils.dismissDialog(dialog)
        if (MessageUtils.snackbar != null) {
            MessageUtils.snackbar.dismiss()
        }
    }

}
