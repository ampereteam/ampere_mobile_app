package com.ampere.bike.ampere.`interface`

import android.content.Context
import android.util.Log

class SampleInterface<g> {

    interface SampleTest<g> {

        fun sampleInterface(context: Context, msg: g)

        fun toastMsg(message: g) {


        }
    }

    class Sample() : SampleTest<HashMap<String, String>> {

        override fun sampleInterface(context: Context, msg: HashMap<String, String>) {

            Log.d("Testing", "" + msg["name"])

        }

    }

}