/*
 * Copyright 2011 Lauri Nevala.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ampere.bike.ampere.views.fragment.calendar;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.ampere.bike.ampere.utils.DateUtils;
import com.ampere.vehicles.R;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import customviews.CustomTextView;


public class CalendarAdapter extends BaseAdapter {
    static final int FIRST_DAY_OF_WEEK = 1; // Sunday = 0, Monday = 1
    private FragmentActivity mContext;
    private Calendar month;
    private Calendar selectedDate;
    private ArrayList<HashMap<String, String>> items;
    private String dateStr, monthStr, yearStr;
    // references to our items
    public String[] days;

    public CalendarAdapter(FragmentActivity c, Calendar monthCalendar) {
        month = monthCalendar;
        selectedDate = (Calendar) monthCalendar.clone();
        mContext = c;
        month.set(Calendar.DAY_OF_MONTH, 1);
        this.items = new ArrayList<HashMap<String, String>>();
        refreshDays();
    }

    public void setItems(ArrayList<HashMap<String, String>> items) {
       /* Log.d("ddssdayesss", "called");
        for (int i = 0; i < items.size(); i++) {
            Log.d("ddssdayesss", "" + items.get(i).get("date"));
        }*/
        Log.d("items", "" + items.size());
        this.items = items;
        //notifyDataSetChanged();
    }


    public int getCount() {
        return days.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new view for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        final CustomTextView dayView, mEventName, mEventCount;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.calendar_item, null);
        }
        dayView = (CustomTextView) v.findViewById(R.id.date);
        mEventName = (CustomTextView) v.findViewById(R.id.mEventName);
        mEventCount = (CustomTextView) v.findViewById(R.id.mEventCount);
        mEventCount.setVisibility(View.GONE);

        // disable empty days from the beginning
        // Log.d("daysss", "" + days[position]);
        if (days[position].equals("")) {
            dayView.setClickable(false);
            dayView.setFocusable(false);
            v.setVisibility(View.GONE);
        } else {
            v.setVisibility(View.VISIBLE);
            // mark current day as focused
            // Log.d("salskalskal", " " + month.get(Calendar.YEAR) + "  == " + selectedDate.get(Calendar.YEAR) + " && " + month.get(Calendar.MONTH) + " == " + selectedDate.get(Calendar.MONTH) + " && " + days[position].equals("" + selectedDate.get(Calendar.DAY_OF_MONTH)));
            if (month.get(Calendar.YEAR) == selectedDate.get(Calendar.YEAR) && month.get(Calendar.MONTH) == selectedDate.get(Calendar.MONTH) && days[position].equals("" + selectedDate.get(Calendar.DAY_OF_MONTH))) {
               /* if (checkVersion() == 1) {
                    v.setBackgroundResource(R.drawable.draw_ripple_effect);
                } else {
                    v.setBackgroundResource(R.drawable.shadow_below_21);
                }
                dayView.setTextColor(mContext.getResources().getServiceType(R.serviceType.orange));*/
            } else {
                mEventCount.setText("");
                dayView.setTextColor(mContext.getResources().getColor(R.color.black));
                //v.setBackgroundResource(R.drawable.shadow_login_layout);
            }
        }
        dayView.setText(days[position]);
        // create date string for comparison
        dateStr = days[position];
        Log.d("dateStrsdsd", "" + dateStr);
        if (dateStr.length() == 1) {
            dateStr = "0" + dateStr;
        }
        yearStr = "" + month.get(Calendar.YEAR);
        /*String date = days[position];

        if (date.length() == 1) {
            date = "0" + date;
        }*/
        monthStr = "" + (month.get(Calendar.MONTH) + 1);
        if (monthStr.length() == 1) {
            monthStr = "0" + monthStr;
        }
        //Log.d("dateStr12454758", "" + dateStr);

        // show icon if date is not empty and it exists in the items array
        ImageView iw = (ImageView) v.findViewById(R.id.date_icon);
        iw.setVisibility(View.GONE);
        mEventName.setVisibility(View.INVISIBLE);
        String txtName = "";

       /* if (position % 2 == 0)
            // mEventCount.setText("25");
            mEventCount.setVisibility(View.VISIBLE);
*/

        // mEventCount.setTextColor(mContext.getResources().getServiceType(R.serviceType.white));
        for (int i = 0; i < items.size(); i++) {
            String date = yearStr + "-" + monthStr + "-" + dateStr;
           /// Log.d("dayesss", "" + items.get(i).get("date") + " joins " + date);
            try {
                if (DateUtils.convertDates(items.get(i).get("date"), DateUtils.DATE_YYYY_MM_DD_HH_MM_SS, DateUtils.YYYY_MM_DD).equals(date)) {
                    //iw.setVisibility(View.VISIBLE);
                    //  dayView.setTextColor(mContext.getResources().getServiceType(R.serviceType.white));
                    mEventName.setVisibility(View.VISIBLE);

                    String name = items.get(i).get("event");
                    // Log.d("name123456789", "" + name);
                    //   if (name != null) {


                        /*String arr[] = name.split(",");

                        for (int ij = 0; ij < arr.length; ij++) {
                            try {
                                if (txtName.equals("")) {
                                    String subStr = arr[ij];
                                    // Log.d("dsjds" + date, ij + "  sdsd  " + subStr);
                                    if (subStr.length() > 5) {
                                        txtName = subStr.substring(0, 5) + "..";
                                    } else {
                                        txtName = subStr;
                                    }
                                } *//*else {
                                    String subStr = arr[ij];
                                   *//**//* Log.d("dsjds" + date, ij + "  " + subStr);
                                    if (subStr.length() > 5) {
                                        txtName = txtName + subStr.substring(0, 5) + ".." + System.getProperty("line.separator");
                                    } else {*//**//*
                                    txtName = txtName + subStr + System.getProperty("line.separator");
                                    // }
                                }*//*

                                // Log.d("txtNametxtName", date + " " + txtName);
                            } catch (StringIndexOutOfBoundsException e) {
                                e.printStackTrace();
                            }
                        }
                        int length = arr.length - 1;*/
                    // Log.d("sddsdlength", "" + name);
                    if (name != null && !name.isEmpty()) {
                        mEventName.setText(txtName);
                        mEventCount.setText(name);
                        dayView.setTextColor(mContext.getResources().getColor(R.color.orange));
                        mEventCount.setTextColor(mContext.getResources().getColor(R.color.orange));
                        //mEventCount.setBackgroundResource(R.drawable.cart);
                        mEventCount.setVisibility(View.VISIBLE);
                        iw.setVisibility(View.GONE);
                    } else {
                        dayView.setTextColor(mContext.getResources().getColor(R.color.black));
                        mEventCount.setTextColor(mContext.getResources().getColor(R.color.black));
                        mEventName.setText(txtName);
                        mEventCount.setVisibility(View.GONE);
                        iw.setVisibility(View.INVISIBLE);
                    }
                    // }

                    // mEventName.setTextColor(mContext.getResources().getServiceType(R.serviceType.orange));
                  /*  if (checkVersion() == 1) {
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.shadow_event_date));
                    } else {
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.reject_below_21));
                    }*/
                }/* else {
                    Log.d("visssi","invis");
                    iw.setVisibility(View.INVISIBLE);
                }*/
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

/*
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("success", "" + dayView.getText().toString());

                int dateOfDay = Integer.parseInt(dayView.getText().toString());
                String dateOfDayStr = MyUtils.checkTwoDigitNumber(dateOfDay);
                String monthYear = CustomeCalendarView.title.getText().toString();
                try {
                    CAL_YEAR = monthYear;
                    monthYear = DateUtils.convertDates(monthYear, "MMMM yyyy", "MM/yyyy");
                    ViewEvent viewEvent = new ViewEvent();
                    Bundle bundle = new Bundle();
                    bundle.putString("event_date", "" + dateOfDayStr + "/" + monthYear);
                    viewEvent.setArguments(bundle);
                  //  MyUtils.passFragmentBackStack(mContext, viewEvent);



                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Log.d("dateOfDayStr", "" + dateOfDayStr + "   " + monthYear);


            }
        });*/
        return v;
    }

    public void refreshDays() {
        // clear items
        //items.clear();
        int lastDay = month.getActualMaximum(Calendar.DAY_OF_MONTH);
        int firstDay = (int) month.get(Calendar.DAY_OF_WEEK);
        // figure size of the array
        if (firstDay == 1) {
            days = new String[lastDay + (FIRST_DAY_OF_WEEK * 6)];
        } else {
            days = new String[lastDay + firstDay - (FIRST_DAY_OF_WEEK + 1)];
        }
        int j = FIRST_DAY_OF_WEEK;
        // populate empty days before first real day
        if (firstDay > 1) {
            for (j = 0; j < firstDay - FIRST_DAY_OF_WEEK; j++) {
                days[j] = "";
            }
        } else {
            for (j = 0; j < FIRST_DAY_OF_WEEK * 6; j++) {
                days[j] = "";
            }
            j = FIRST_DAY_OF_WEEK * 6 + 1; // sunday => 1, monday => 7
        }

        // populate days
        int dayNumber = 1;
        for (int i = j - 1; i < days.length; i++) {
            days[i] = "" + dayNumber;
            // Log.d("dayNumber", "" + dayNumber);
            dayNumber++;
        }
    }

}