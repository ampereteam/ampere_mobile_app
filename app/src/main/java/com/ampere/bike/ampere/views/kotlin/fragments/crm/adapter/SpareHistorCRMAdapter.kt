package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.Data
import com.ampere.vehicles.R

class SpareHistorCRMAdapter(private val  context:Context,private  val  spareHistoryList:ArrayList<Data.SpareHistory>, private val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<SpareHistorCRMAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpareHistorCRMAdapter.ViewHolder {
        val view  = LayoutInflater.from(this.context).inflate(R.layout.adapter_crm_spare_history,parent,false)
        return ViewHolder(view)
    }
    override fun getItemCount(): Int {
        return  this.spareHistoryList.size
    }
    override fun onBindViewHolder(holder: SpareHistorCRMAdapter.ViewHolder, position: Int) {
        try{
            holder.sno.text = (position+1).toString()
            holder.changedDate.text = spareHistoryList[position].changed_date
            holder.componet.text =spareHistoryList[position].component
            holder.serialNo.text =spareHistoryList[position].new_serial_no
            holder.serviceType.text =spareHistoryList[position].warranty
            holder.linear.setOnClickListener {
                this.configClickEvent.connectposition(position,spareHistoryList.size)
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val sno :TextView = itemView?.findViewById(R.id.tv_item_si_no_adptr)!!
        val componet :TextView = itemView?.findViewById(R.id.tv_componet_adptr)!!
        val serialNo :TextView = itemView?.findViewById(R.id.tv_new_serial_no_adptr)!!
        val serviceType :TextView = itemView?.findViewById(R.id.tv_service_type_adptr)!!
        val changedDate :TextView = itemView?.findViewById(R.id.tv_spare_changed_date_adptr)!!
        val linear :LinearLayout = itemView?.findViewById(R.id.linearlayout)!!
    }
}