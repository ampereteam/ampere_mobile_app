package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.ServiceEnquiryAdapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.provider.Settings.Global.getString
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_ID
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.ServiceEnquiryServerPojo.PaidReplacementServerPojo
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.dynamicAddPojo.ServiceReplecementViewPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.dynamicPaidcomponentPojos.DynamicPaidComponentPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareStockPojos.SpareStockPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.ServiceReplecementAdapter
import com.ampere.vehicles.R
import com.ampere.vehicles.R.string.component
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import customviews.CustomTextEditView
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PaidReplacementAdapter(val enquiryId:String,  val chassisno :String, val  context: FragmentActivity,val paidReplacementArrayList: ArrayList<DynamicPaidComponentPojos?>?,val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<PaidReplacementAdapter.ViewHolder>(){
    var utilsCallApi :UtilsCallApi = UtilsCallApi(this.context)
    var  componetSTRList :ArrayList<String> = ArrayList()
    var  oldserialSTRList :ArrayList<String> = ArrayList()
    var  newSerialSTRList :ArrayList<String> = ArrayList()
    var  componetSTRAdapter :ArrayAdapter<String> ?= null
    var  oldserialSTRAdapter :ArrayAdapter<String> ?= null
    var  newSerialSTRAdapter :ArrayAdapter<String> ?= null
    var dialog = Dialog(context)
    var  servicereplacePojoArrayList :ArrayList<ServiceReplecementViewPojos>  = ArrayList()
    var  servicerepalcementAdapter :ServiceReplecementAdapter ?  = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaidReplacementAdapter.ViewHolder {
        val view  = LayoutInflater.from(this.context).inflate(R.layout.adapter_service_enquiry_paid_replacement,parent,false)
        return  ViewHolder(view)
    }

    override fun getItemCount(): Int {    return  1   }

    override fun onBindViewHolder(holder: PaidReplacementAdapter.ViewHolder, position: Int) {
        try{
            var currentPosition = position
            var selectedcomponentId:Int ?=null
            for(component in paidReplacementArrayList!!){
                componetSTRList.add(component?.component!!)
            }
            componetSTRList.add(0,"Select The Component")
            newSerialSTRAdapter = ArrayAdapter(context,R.layout.spinnertext ,R.id.text1,componetSTRList)
            holder.spcomponets.adapter = newSerialSTRAdapter
            holder.spcomponets.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    try{
                        Log.d("Nothingselected",""+parent?.selectedItem)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    try{
                        var selectedItem = holder.spcomponets.selectedItem.toString()
                        holder.edtComponets.setText(selectedItem)
                        if(selectedItem.isEmpty() || selectedItem.equals("Select The Component")){
                            Log.d("SelecteItem",""+selectedItem)
                        }else{
                            val map :HashMap<String,Any> = HashMap()
                            map["userid"] =LocalSession.getUserInfo(context, KEY_ID)
                            map["chassis_no"] =chassisno
                            map["enquiry_id"] =enquiryId
                            map["component"] =selectedItem
                            selectedcomponentId=  paidReplacementArrayList[position]?.id
                            dialog =MessageUtils.showDialog(context)
                            val calInterface =utilsCallApi?.connectRetro("POST","getoldserielnoandstock")
                            val callback = calInterface?.call_post("getoldserielnoandstock","Bearer "+LocalSession.getUserInfo(context,KEY_TOKEN), map)
                            callback?.enqueue(object : Callback<ResponseBody> {
                                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                    try{
                                        MessageUtils.dismissDialog(dialog)
                                        Toast.makeText(context,t.message,Toast.LENGTH_SHORT).show()
                                    }catch (ex:Exception){
                                        ex.printStackTrace()
                                    }
                                }

                                @SuppressLint("ShowToast")
                                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                    MessageUtils.dismissDialog(dialog)
                                   if(response.isSuccessful){
                                       try{
                                           val jsonobject = JSONObject(response.body()?.string())
                                           val message = jsonobject.getString("message")!!
                                           val success = jsonobject.getBoolean("success")
                                           val data = jsonobject.getJSONObject("data")!!
                                           if(success){
                                               newSerialSTRList?.clear()
                                               oldserialSTRList?.clear()
                                               if (data.getString("oldserial") == null || data.getString("oldserial").length == 0 ||
                                                       data.getString("oldserial").isEmpty() ||  data.getString("oldserial").equals(null)) {
                                                   oldserialSTRList?.add("Old Serial Number Not Available")
                                                   Toast.makeText(context, "Old serial Number Not Available", Toast.LENGTH_SHORT).show()
                                               } else {

                                                   oldserialSTRList?.add(data.getString("oldserial"))

                                               }
                                               /**
                                                * GETTING  SpareStock For New Serial Number
                                                *
                                                */
                                               if (data.getJSONArray("sparesstock") == null || data.getJSONArray("sparesstock").toString().isEmpty() || data.getJSONArray("sparesstock").length() == 0) {
                                                   newSerialSTRList.add("Stock Not Available")
                                                   Toast.makeText(context, "Stock Not Available", Toast.LENGTH_SHORT).show()
                                               } else {
                                                   var sparestockList = Gson().fromJson<ArrayList<SpareStockPojos>>(data.getJSONArray("sparesstock").toString(), object : TypeToken<ArrayList<SpareStockPojos>>() {}.type)
                                                   for (spare in sparestockList) {
                                                       newSerialSTRList?.add(spare.serialNo)
                                                   }
                                               }
                                               oldserialSTRAdapter = ArrayAdapter(context,R.layout.spinnertext,R.id.text1,oldserialSTRList)
                                               holder.spOldSerilNO.adapter = oldserialSTRAdapter
                                               newSerialSTRAdapter =ArrayAdapter(context,R.layout.spinnertext,R.id.text1,newSerialSTRList)
                                               holder.spnewSerialNo.adapter=newSerialSTRAdapter
                                               if(newSerialSTRList.size <=1){
                                                   holder.addimgbtn.visibility = View.GONE
                                                   if(holder.spnewSerialNo.selectedItem.toString().equals("Stock Not Available")
                                                           || holder.spOldSerilNO.selectedItem.toString().isEmpty()
                                                           || holder.spnewSerialNo.selectedItem.toString().isEmpty()
                                                   ){
                                                       Log.d("",""+holder.spnewSerialNo.selectedItem.toString())
                                                   }else{
                                                       val paidReplacementServerPojo = PaidReplacementServerPojo(holder.spcomponets.selectedItem.toString(),
                                                               holder.spOldSerilNO.selectedItem.toString(),holder.spnewSerialNo.selectedItem.toString(),
                                                               selectedcomponentId
                                                               )
                                                       CRMDetailsActivity.paidReplacementServerLISt.add(paidReplacementServerPojo)
                                                       Log.d("PaidComponent",""+ CRMDetailsActivity.paidReplacementServerLISt[0].componentName)
                                                       Log.d("PaidComponent",""+ CRMDetailsActivity.paidReplacementServerLISt[0].oldSerialNumber)
                                                       Log.d("PaidComponent",""+ CRMDetailsActivity.paidReplacementServerLISt[0].newSerialNumber)
                                                   }
                                               }else{
                                                   holder.addimgbtn.visibility = View.VISIBLE

                                               }
                                           }else{
                                               Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
                                           }
                                       }catch (ex:Exception){
                                           ex.printStackTrace()
                                       }
                                   }else{
                                       Toast.makeText(context,response.message(),Toast.LENGTH_LONG).show()
                                   }
                                }
                            })

                        }
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }

                }
            }
            holder.spOldSerilNO.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    try{
                        Log.d("NothingSelected",""+parent?.selectedItem)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    try{
                        val selectitem = holder.spOldSerilNO.selectedItem.toString()
                        holder.edtoldSerialNo.setText(selectitem)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }

                }

            }
            holder.spnewSerialNo.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    try{
                        Log.d("NothingSelected",""+parent?.selectedItem)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    try{
                        val selectitem = holder.spnewSerialNo.selectedItem.toString()
                        holder.edtnewSerialNo.setText(selectitem)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }

                }

            }
            holder.addimgbtn.setOnClickListener {
              if(holder.spOldSerilNO.selectedItem !=null &&  holder.spnewSerialNo.selectedItem !=null ){
                  if(holder.spOldSerilNO.selectedItem.toString().isEmpty()
                          || holder.spOldSerilNO.selectedItem.toString().equals("Old Serial Number Not Available")
                          || holder.spnewSerialNo.selectedItem.toString().equals("Stock Not Available")
                          || holder.spnewSerialNo.selectedItem.toString().isEmpty()
                  ){
                      Toast.makeText(context,"Valid Data Required",Toast.LENGTH_SHORT).show()
                  }else{
                      val serviceReplecementViewPojos: ServiceReplecementViewPojos = ServiceReplecementViewPojos(holder.spOldSerilNO.selectedItem.toString()
                              , holder.spcomponets.selectedItem.toString(),holder.spnewSerialNo.selectedItem.toString(), "nonwarranty", "")
                      servicereplacePojoArrayList.add(serviceReplecementViewPojos)
                      servicerepalcementAdapter = ServiceReplecementAdapter(context, servicereplacePojoArrayList)
                      holder.recyList.layoutManager = LinearLayoutManager(context,LinearLayout.VERTICAL,false)
                      holder.recyList.adapter = servicerepalcementAdapter
                          val paidReplacementServerPojo = PaidReplacementServerPojo(holder.spcomponets.selectedItem.toString(),
                          holder.spOldSerilNO.selectedItem.toString(),holder.spnewSerialNo.selectedItem.toString(),selectedcomponentId)
                          CRMDetailsActivity.paidReplacementServerLISt.add(paidReplacementServerPojo)
                          Log.d("PaidComponent",""+ CRMDetailsActivity.paidReplacementServerLISt[0].componentName)
                          Log.d("PaidComponent",""+ CRMDetailsActivity.paidReplacementServerLISt[0].oldSerialNumber)
                          Log.d("PaidComponent",""+ CRMDetailsActivity.paidReplacementServerLISt[0].newSerialNumber)
//                      oldserialSTRList.remove(holder.spOldSerilNO.selectedItem)
////                      oldserialSTRAdapter?.notifyDataSetChanged()
                      newSerialSTRList.remove(holder.spnewSerialNo.selectedItem.toString())
                      newSerialSTRAdapter?.notifyDataSetChanged()
                  }
              }else{
                  Toast.makeText(context,"Valid Data Required",Toast.LENGTH_SHORT).show()
              }
            }
            if(holder.spnewSerialNo.selectedItem !=null && holder.spOldSerilNO.selectedItem !=null){
                if(holder.spnewSerialNo.selectedItem.toString().equals("Stock Not Available")
                        || holder.spOldSerilNO.selectedItem.toString().isEmpty()
                        || holder.spnewSerialNo.selectedItem.toString().isEmpty()
                ){
                    Log.d("",""+holder.spnewSerialNo.selectedItem.toString())
                }else{
                    val paidReplacementServerPojo = PaidReplacementServerPojo(holder.spcomponets.selectedItem.toString(),
                            holder.spOldSerilNO.selectedItem.toString(),holder.spnewSerialNo.selectedItem.toString(),selectedcomponentId)
                    CRMDetailsActivity.paidReplacementServerLISt.add(paidReplacementServerPojo)
                    Log.d("PaidComponent",""+ CRMDetailsActivity.paidReplacementServerLISt[0].componentName)
                    Log.d("PaidComponent",""+ CRMDetailsActivity.paidReplacementServerLISt[0].oldSerialNumber)
                    Log.d("PaidComponent",""+ CRMDetailsActivity.paidReplacementServerLISt[0].newSerialNumber)
                }
            }else{
//                Toast.makeText(context ,"Valid Data Required",Toast.LENGTH_SHORT).show()
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val linearLayout :LinearLayout =itemView?.findViewById(R.id.ll_addmore)!!
        val edtComponets : CustomTextEditView =itemView?.findViewById(R.id.bs_edt_component_name_addmore)!!
        val edtoldSerialNo : CustomTextEditView =itemView?.findViewById(R.id.bs_edt_old_serial_number_addmore)!!
        val edtnewSerialNo: CustomTextEditView =itemView?.findViewById(R.id.bs_edt_new_serial_number_addmore)!!
        val spcomponets : Spinner =itemView?.findViewById(R.id.bs_sp_component_name_addmore)!!
        val spOldSerilNO : Spinner =itemView?.findViewById(R.id.bs_sp_old_serial_number_addmore)!!
        val spnewSerialNo : Spinner =itemView?.findViewById(R.id.bs_sp_new_serial_number_addmore)!!
        val addimgbtn : ImageButton =itemView?.findViewById(R.id.dynamic_add)!!
        val recyList : RecyclerView =itemView?.findViewById(R.id.rclv_service_replace)!!
    }
}