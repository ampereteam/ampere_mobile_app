package com.ampere.bike.ampere.views.kotlin.fragments.crm

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import com.ampere.bike.ampere.interfaces.RefreshEnquiryTabs
import com.ampere.bike.ampere.model.task.EnquiryModel
import com.ampere.bike.ampere.model.task.ViewTaskModel
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.BEARER
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.fragments.crm.SparesService.SparesServiceKT
import com.ampere.bike.ampere.views.kotlin.fragments.crm.sales.SalesKT
import com.ampere.bike.ampere.views.kotlin.fragments.crm.service.ServiceKT
import com.ampere.bike.ampere.views.kotlin.fragments.crm.spares.SparesKT
import com.ampere.vehicles.R
import com.ampere.vehicles.R.string.spares
import customviews.CustomTextView
import kotlinx.android.synthetic.main.activity_crm.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class CRMActivity : AppCompatActivity() {

    var name: TextView? = null
    var dialog: Dialog? = null
    val mobile: String = ""
    var mNoData: CustomTextView? = null
    var mSnackView: CoordinatorLayout? = null
    var response1: Response<ViewTaskModel>? = null
    var tabValues = arrayListOf<String>("Sales", "Service", "Spares","SparesService");


    companion object {
        var refresh: RefreshEnquiryTabs? = null
        var refreshService: RefreshEnquiryTabs? = null
        var refreshSales: RefreshEnquiryTabs? = null
        var refreshSparesService: RefreshEnquiryTabs? = null
        var refreshSpare: RefreshEnquiryTabs? = null

        fun refreshView(refresh: RefreshEnquiryTabs) {
            this.refresh = refresh

        }

        fun refreshServiceView(refresh: RefreshEnquiryTabs) {
            this.refreshService = refresh
        }

        fun refreshSalesView(refresh: RefreshEnquiryTabs) {
            this.refreshSales = refresh
        }
        fun refreshSparesServiceView(refresh: RefreshEnquiryTabs) {
            this.refreshSparesService = refresh
        }

        fun refreshSpareView(refresh: RefreshEnquiryTabs) {
            this.refreshSpare = refresh
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar!!.hide()
        setContentView(R.layout.activity_crm)
        tool_bar_title.text = getString(R.string.assigned_task)
        mSnackView = findViewById(R.id.main_content)
        mNoData = findViewById(R.id.txt_no_data)
        mNoData?.visibility = View.GONE
        //if (response1 != null) {
        //response1?.let { loadData(it) };
        //val fragmentAdapter = CRM.MyPagerAdapter(supportFragmentManager)

        setTabs();

        //}

        onCallEnquiryListApi("Sales", 0)

        ic_back.setOnClickListener {
            if (searchView2.query.isEmpty()) {
                onBackPressed()
            } else {
                searchView2.isIconified = true
                if (!searchView2.isIconified) {
                    searchView2.isIconified = true
                }
            }
        }

       /* tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewpager.currentItem = tabs!!.selectedTabPosition
                //MessageUtils.showToastMessage(this@CRMActivity, "" + tabs.selectedTabPosition)
                //Log.d("isOpenOrClose", "" + searchView2.isIconified)
                if (!searchView2.query.isEmpty()) {
                    searchView2.isIconified = true
                    if (!searchView2.isIconified) {
                        searchView2.isIconified = true
                    }
                } else {
                    if (!searchView2.isIconified) {
                        searchView2.isIconified = true
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })*/

        searchView2.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                //Log.d("asasa_query 121 ", " " + searchView2.query + "  " + DateUtils.getCurrentDate(DateUtils.DATE_DD_MM_YYYY_HH_MM_AP))
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                 Log.d("asasa_query", " " + newText)
                when (tabs!!.selectedTabPosition) {
                    0 -> {
                        Log.d("Selected","Postion"+tabs!!.selectedTabPosition)
                        Log.d("ArrayListSize\t",""+MyUtils.SALES.size)
                        if (MyUtils.SALES.size != 0) {
                            var filters = ArrayList<EnquiryModel>()
//                            Log.d("Test1",""+filters[0].customerName)
                            for (sale in MyUtils.SALES) {
//                                Log.d("Test2Loop",""+filters[0].customerName)
                                if (newText != null) {
                                    Log.d("Testif",""+newText)
                                    if(sale.customerName!!.toLowerCase().contains(newText.toLowerCase())) {
                                        Log.d("Testifif",""+sale)
                                        filters.add(sale)
                                    } else if (sale.taskStatus!!.toLowerCase().contains(newText.toLowerCase())) {
                                        filters.add(sale)
                                    }
                                }
                            }
                            Log.d("filter_sales", "" + filters.size)
                            if (SalesKT.adapter != null) {
                                SalesKT.adapter!!.filterList(filters)
                            }
                        }
                    }
                    1 -> {
                        if (MyUtils.SERVICE.size != 0) {
                            var filters = ArrayList<EnquiryModel>()
                            for (service in MyUtils.SERVICE) {
                                if (newText != null) {
                                    if (service.customerName!!.toLowerCase().contains(newText.toLowerCase())) {
                                        filters.add(service)
                                    } else if (service.taskStatus!!.toLowerCase().contains(newText.toLowerCase())) {
                                        filters.add(service)
                                    }
                                }
                            }
                            if (ServiceKT.itemService != null) {
                                ServiceKT.itemService!!.filterList(filters)
                            }
                        }
                    }
                    2 -> {
                        if (MyUtils.SPARES.size != 0) {
                            var filters = ArrayList<EnquiryModel>()
                            for (spares in MyUtils.SPARES) {
                                if (newText != null) {
                                    if (spares.customerName!!.toLowerCase().contains(newText.toLowerCase())) {
                                        filters.add(spares)
                                    } else if (spares.taskStatus!!.toLowerCase().contains(newText.toLowerCase())) {
                                        filters.add(spares)
                                    }
                                }
                            }
                            Log.d("filter_spares", "" + filters.size)
                            if (SparesKT.itemSpares != null) {
                                SparesKT.itemSpares!!.filterList(filters)
                            }
                        }
                    }
                    3 -> {
                        if (MyUtils.SPARESSERVICE.size != 0) {
                            var filters = ArrayList<EnquiryModel>()
                            for (sparesService in MyUtils.SPARESSERVICE) {
                                if (newText != null) {
                                    if (sparesService.customerName!!.toLowerCase().contains(newText.toLowerCase())) {
                                        filters.add(sparesService)
                                    } else if (sparesService.taskStatus!!.toLowerCase().contains(newText.toLowerCase())) {
                                        filters.add(sparesService)
                                    }
                                }
                            }
                            Log.d("filter_spares", "" + filters.size)
                            if (SparesServiceKT.itemSpares != null) {
                                SparesServiceKT.itemSpares!!.filterList(filters)
                            }
                        }
                    }
                }
                return false
            }
        })
    }


    private fun loadData(response: Response<ViewTaskModel>, tabPosition: Int) {
        Log.d("tabPositionFineal", "" + tabPosition)
        if (response != null) {

            if (response.isSuccessful) {
                var taskModel = response.body()
                if (taskModel != null) {
                    if (taskModel.success) {
                        if (taskModel.data != null) {
                            //mNoData?.visibility = View.GONE

/*

                            for (items: EnquiryModel in taskModel.data.enquiry) {
                                var filterValue = items.enquiryFor
                                if (filterValue.equals(getString(R.string.sales))) {
                                    var models = CRMFilterSales("" + items.id, items.customerName, items.city, "" + items.mobileNo, items.email, items.leadsource, items.assignedDate,
                                            items.assignedTo, items.enquiryDate, items.enquiryFor, items.enquiryTime, items.assignedPerson, items.message, items.taskStatus, items.task,
                                            items.vehicleModel, "", false)
                                    //  Log.d("filterValue ", filterValue)
                                    MyUtils.SALES.add(models)


                                } else if (filterValue.equals(getString(R.string.service))) {
                                    var models = CRMFilterService("" + items.id, items.customerName, items.city, "" + items.mobileNo, items.email, items.leadsource, items.assignedDate,
                                            items.assignedTo, items.enquiryDate, items.enquiryFor, items.enquiryTime, items.assignedPerson, items.message, items.taskStatus, items.task,
                                            items.vehicleModel, "", false)
                                    MyUtils.SERVICE.add(models)

                                } else {
                                    var models = CRMFilterSpares("" + items.id, items.customerName, items.city, "" + items.mobileNo, items.email, items.leadsource, items.assignedDate,
                                            items.assignedTo, items.enquiryDate, items.enquiryFor, items.enquiryTime, items.assignedPerson, items.message, items.taskStatus, items.task,
                                            items.vehicleModel, "", false)
                                    MyUtils.SPARES.add(models)

                                }
                            }

                            Log.d("MyUtilsALL", "" + MyUtils.SALES.size + " sddsd " + MyUtils.SERVICE.size + " sdsdsd " + MyUtils.SPARES.size);

*/


                            if (tabPosition == 0) {
                                Log.d("tabPositionadaprere", "" + tabPosition)

                                /*if (SalesKT.adapter != null) {
                                    SalesKT.adapter!!.notifyDataSetChanged()
                                }*/
/*
                                for (enquiryModel in taskModel.data.enquiry) {
                                    Log.d("enquiryModelsss000", "" + enquiryModel.enquiryFor)
                                }*/
                                if (refresh != null) {
                                    refresh!!.refreshView(taskModel.data.enquiry)
                                }

                            } else if (tabPosition == 1) {
                                Log.d("tabPositionadaprere", "" + tabPosition)
                                /*ServiceKT.addData(MyUtils.SERVICE)
                                if (ServiceKT.itemService != null) {
                                    ServiceKT.itemService!!.notifyDataSetChanged()
                                }*/


                                /* for (enquiryModel in taskModel.data.enquiry) {
                                     Log.d("enquiryModelsss11111", "" + enquiryModel.enquiryFor)
                                 }*/

                                if (refreshService != null) {
                                    refreshService!!.refreshView(taskModel.data.enquiry)
                                }

                            } else if (tabPosition == 2) {
                                Log.d("tabPositionadaprere", "" + tabPosition)
                                /*for (enquiryModel in taskModel.data.enquiry) {
                                    Log.d("enquiryModelsss2222", "" + enquiryModel.enquiryFor)
                                }*/
                                if (refreshSpare != null) {
                                    refreshSpare!!.refreshView(taskModel.data.enquiry)
                                }
                                /*Log.d("adaprere", "" + SparesKT.itemSpares)
                                SparesKT.addData(MyUtils.SPARES)
*/
                                /*if (SparesKT.itemSpares != null) {
                                    SparesKT.itemSpares!!.notifyDataSetChanged()
                                }*/
                            }else if (tabPosition == 3) {
                                Log.d("tabPositionadaprere", "" + tabPosition)
                                /*for (enquiryModel in taskModel.data.enquiry) {
                                    Log.d("enquiryModelsss2222", "" + enquiryModel.enquiryFor)
                                }*/
                                if (refreshSparesService != null) {
                                    refreshSparesService!!.refreshView(taskModel.data.enquiry)
                                }

                            }


                            /*val fragmentAdapter = CRM.MyPagerAdapter(supportFragmentManager)
                            viewpager.adapter = fragmentAdapter*/

                            //setTabs()

                        } else {
                            MessageUtils.showSnackBar(this@CRMActivity, mSnackView, taskModel.message.toString())
                        }
                    } else {
                        MessageUtils.showSnackBar(this@CRMActivity, mSnackView, taskModel.message)
                    }
                } else {
                    MessageUtils.showSnackBar(this@CRMActivity, mSnackView, "No data found ")
                }
            } else {
                MessageUtils.setErrorMessage(this@CRMActivity, mSnackView, response.code())
            }
        }
    }

    private fun setTabs() {
        //Service login

        if (LocalSession.getUserInfo(this, LocalSession.KEY_USER_TYPE).equals(LocalSession.T_CORPORATE_SERVICE)) {
            tabValues.clear()
            tabValues = arrayListOf("Service", "Spares")
            viewpager.adapter = PageAdapService(supportFragmentManager, tabValues)
            //Show only service
        } else if (LocalSession.getUserInfo(this, LocalSession.KEY_USER_TYPE).equals(LocalSession.T_DEALER)) {
            viewpager.adapter = MyCustomPageAdap(supportFragmentManager, tabValues, this@CRMActivity)
        } else if (LocalSession.getUserInfo(this, LocalSession.KEY_USER_TYPE).equals(LocalSession.T_CORPORATE_SALES)) {
            //Two Sales andSpares
            tabValues.clear()
            tabValues = arrayListOf("Sales", "Spares")
            viewpager.adapter = PageAdapSalesSpares(supportFragmentManager, tabValues)
        } else {
            // show Three or others
            viewpager.adapter = MyCustomPageAdap(supportFragmentManager, tabValues, this@CRMActivity)
        }

        tabs.setupWithViewPager(viewpager)



        for (i: Int in 0..tabs.getTabCount() - 1) {
            //noinspection ConstantConditions
            var tv = TextView(this)
            tv.setTypeface(MyUtils.getTypeface(this, "fonts/Lato-Bold.ttf"));
            if (i == 0) {
                tv.setText("" + tabValues[i].toUpperCase())
            } else if (i == 1) {
                tv.setText("" + tabValues[i].toUpperCase())
            } else if (i == 2) {
                tv.setText("" + tabValues[i].toUpperCase())
            } else {
                tv.setText("" + tabValues[i].toUpperCase())
            }

            tv.setTextColor(this.resources.getColor(R.color.white))
            tv.gravity = Gravity.CENTER
            tabs.getTabAt(i)!!.setCustomView(tv);

        }
        //viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs));

        //viewpager.setCurrentItem(0)
        /*viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                Log.d("PaginationScrollLisr", "   " + position)

                viewpager.setCurrentItem(position)
                if (!searchView2.query.isEmpty()) {
                    searchView2.isIconified = true
                    if (!searchView2.isIconified) {
                        searchView2.isIconified = true
                    }
                } else {
                    if (!searchView2.isIconified) {
                        searchView2.isIconified = true
                    }
                }
            }


        });*/

        tabs.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {


            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

                var itemView: Int = tab!!.position
                Log.d("PaginationScrollLisr", "after   " + itemView)

                if (!searchView2.query.isEmpty()) {
                    searchView2.isIconified = true
                    if (!searchView2.isIconified) {
                        searchView2.isIconified = true
                    }
                } else {
                    if (!searchView2.isIconified) {
                        searchView2.isIconified = true
                    }
                }
                viewpager.setCurrentItem(itemView)
                Log.d("after_collection", "" + MyUtils.SALES.size + " service " + MyUtils.SERVICE.size + " spare " + MyUtils.SPARES.size+"\n SparesService"+MyUtils.SPARESSERVICE.size)
                Log.d("itemVIew",""+itemView)
                if (itemView == 0) {
                    onCallEnquiryListApi("Sales", 0)
                   /* var test ="12"
                    if (MyUtils.SALES.size == 0) {
                        test ="121"
                        onCallEnquiryListApi("Sales", 0)
                    } else {

                        if (refresh != null) {
                            test ="122\t"+MyUtils.SALES.size
                            refresh!!.refreshView(MyUtils.SALES)
                        }else{
                            onCallEnquiryListApi("Sales", 0)
                            test ="123"
                        }
                    }*/
//                    Log.d("uyikyuiky",""+test)
                } else if (itemView == 1) {
                    onCallEnquiryListApi("Service", 1)
                    /*if (MyUtils.SERVICE.size == 0) {
                        onCallEnquiryListApi("Service", 1)
                    } else {
                        if (refreshService != null) {
                            refreshService!!.refreshView(MyUtils.SERVICE)
                        }
                    }*/
                } else if (itemView == 2) {
                    onCallEnquiryListApi("Spares", 2)
                    /*if (MyUtils.SPARES.size == 0) {
                        onCallEnquiryListApi("Spares", 2)
                    } else {
                        if (refreshSpare != null) {
                            refreshSpare!!.refreshView(MyUtils.SPARES)
                        }
                    }*/
                }
                else if (itemView == 3) {
                    onCallEnquiryListApi("SparesService", 3)
                   /* if (MyUtils.SPARESSERVICE.size == 0) {
                        onCallEnquiryListApi("SparesService", 3)
                    } else {
                        if (refreshSparesService != null) {
                            refreshSparesService!!.refreshView(MyUtils.SPARESSERVICE)
                        }
                    }*/
                }
                // viewpager.currentItem = tab!!.position
                //MessageUtils.showToastMessage(this@CRMActivity, "" + tabs.selectedTabPosition)
                //Log.d("isOpenOrClose", "" + searchView2.isIconified)
            }

        })

    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        try {
            val view = currentFocus
            if (view != null && (ev.action == MotionEvent.ACTION_UP || ev.action == MotionEvent.ACTION_MOVE) && view is EditText && !view.javaClass.name.startsWith("android.webkit.")) {
                val scrcoords = IntArray(2)
                view.getLocationOnScreen(scrcoords)
                val x = ev.rawX + view.left - scrcoords[0]
                val y = ev.rawY + view.top - scrcoords[1]
                if (x < view.left || x > view.right || y < view.top || y > view.bottom)
                    (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(this.window.decorView.applicationWindowToken, 0)
            }
        } catch (e: RuntimeException) {
            e.printStackTrace()
        }

        return super.dispatchTouchEvent(ev)
    }


    class MyCustomPageAdap(fm: FragmentManager?, val values: ArrayList<String>, val activity: FragmentActivity) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            Log.d("positionsdsdaaa", "" + position)
            var fragment: Fragment? = null
            /*when (position) {
                0 -> {
                    // onCallEnquiryListApi("Sales")
                    fragment = SalesKT()
                }
                1 -> {
                    //onCallEnquiryListApi("Service")
                    fragment = ServiceKT()

                }
                2 -> {

                    // onCallEnquiryListApi("Spare")
                    fragment = SparesKT()
                }
                *//* else -> {
                     return null
                 }*//*
            }*/


            when (position) {
                0 -> fragment = SalesKT()
                1 -> fragment = ServiceKT()
                2 -> fragment = SparesKT()
                3-> fragment = SparesServiceKT()
            }

            return fragment

        }


        override fun getCount(): Int {
            return values.size
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> values[position]
                1 -> values[position]
                2 -> values[position]
                3 -> values[position]
                else -> {
                    return values[0]
                }
            }
        }
    }

    class PageAdapService(fm: FragmentManager?, val values: ArrayList<String>) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            var fragment: Fragment? = null
            when (position) {
                0 -> fragment = ServiceKT()
                1 -> fragment = SparesKT()
                2 -> fragment = SalesKT()

            }
            return fragment
        }

        override fun getCount(): Int {
            return values.size
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> values[position]
                1 -> values[position]
                2 -> values[position]
                else -> {
                    return values[0]
                }
            }
        }
    }

    class PageAdapSalesSpares(fm: FragmentManager?, val values: ArrayList<String>) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            var fragment: Fragment? = null
            when (position) {
                0 ->
                    fragment = SalesKT()

                1 ->
                    fragment = SparesKT()


            }
            return fragment
        }

        override fun getCount(): Int {
            return values.size
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> values[position]
                1 -> values[position]
                else -> {
                    return values[0]
                }
            }
        }
    }


    fun onCallEnquiryListApi(enquiry_type: String, tabPosition: Int) {
        Log.d("tabPositionsdsd", "" + tabPosition)
        var taskHash = HashMap<String, String>()
        taskHash.put("userid", LocalSession.getUserInfo(this, LocalSession.KEY_ID))
        taskHash.put("enquiry_type", enquiry_type);
        //var retrofitService: RetrofitService = MyUtils.getRetroService()
        var taskModel = MyUtils.getInstance().onTaskView(BEARER + LocalSession.getUserInfo(this, KEY_TOKEN), taskHash)

        dialog = MessageUtils.showDialog(this)
        dialog?.setOnKeyListener({ dialo, keyCode, event ->
            MessageUtils.dismissDialog(dialog);
            false
        })

        //MessageUtils.dismissDialog(dialog);

        taskModel.enqueue(object : Callback<ViewTaskModel> {
            override fun onResponse(call: Call<ViewTaskModel>, response: Response<ViewTaskModel>) {
                MessageUtils.dismissDialog(dialog)
                response1 = response
                Log.d("TabPosition",""+tabPosition)
                loadData(response, tabPosition)
            }

            override fun onFailure(call: Call<ViewTaskModel>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                mNoData?.visibility = View.VISIBLE
                MessageUtils.setFailureMessage(this@CRMActivity, mSnackView, t.message)
            }
        })

    }

    override fun onPause() {
        super.onPause()
        MessageUtils.dismissDialog(dialog)
    }

    override fun stopLocalVoiceInteraction() {
        super.stopLocalVoiceInteraction()
        MessageUtils.dismissDialog(dialog)
    }

    override fun onStop() {
        super.onStop()
        MessageUtils.dismissDialog(dialog)
    }

    override fun onStart() {
        super.onStart()
        MessageUtils.dismissDialog(dialog)
    }
}