package com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.ArrayList

class AssignTapAdapte(fm: FragmentManager?) : FragmentPagerAdapter(fm)  {
    private val unAssignList = ArrayList<Fragment>()
    private val unAssignListTitleList = ArrayList<String>()


    fun addFragment(fragment: Fragment, title: String) {
        unAssignList.add(fragment)
        unAssignListTitleList.add(title)
    }

    override fun getItem(position: Int): Fragment {
        return unAssignList[position]
    }


    override fun getCount(): Int {
        return unAssignList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return unAssignListTitleList[position]
    }

}