package com.ampere.bike.ampere.views.kotlin.fragments.unassign

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.MultiUserSpinnerItem
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.*
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.model.unassinged.UnAssignEnquiryModel
import com.ampere.bike.ampere.views.kotlin.model.unassinged.assign_details.AssingFromCRM
import com.ampere.bike.ampere.views.kotlin.model.unassinged.assigned_details.AssignedModel
import com.ampere.vehicles.R
import customviews.CustomTextView
import kotlinx.android.synthetic.main.frag_assigned_task.*
import kotlinx.android.synthetic.main.view_task_expandable.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class AssignedTaskfromCRM : Fragment() {

    private var year: Int = 0
    private var month: Int = 0
    private var day: Int = 0
    var assign: String = ""
    var assignId: String = ""

    internal val dealerList = ArrayList<MultiUserSpinnerItem>()
    internal val taskList = ArrayList<MultiTaskSpinnerItem>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        (activity as CustomNavigationDuo).disbleNavigationView("Assigned Task")
        return inflater.inflate(R.layout.frag_assigned_task, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var models: UnAssignEnquiryModel = arguments?.getSerializable("itemValues") as UnAssignEnquiryModel

        Log.d("sdssdsds", "" + models + "  " + models.enquiry_date + "  " + models.customer_name);
        Log.d("enquiry_for sdsd", " " + models.enquiry_for[0])

        try {
            ev_details_name.text = "  " + models.customer_name
            ev_details_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_user, 0, 0, 0)
            ev_details_city.text = "  " + models.city
            ev_details_vehicle_model.text = "  " + models.vehicle_model
            ev_details_chassis_no.text = "  " + models.chassis_no
            ev_details_mail.text = "  " + models.email
            ev_invoice.text = "  " + getString(R.string.inr) + " " + models.invoice_value
            ev_details_mobile_no.text = "  " + models.mobile_no

            if (!models.spares_name.isEmpty()) {
                ev_details_spares.text = "  " + models.spares_name
            } else {
                ev_details_spares.visibility = View.VISIBLE
            }
            ev_lead_source.text = "  " + models.leadsource
            ev_details_task.text = "  " + models.task
            ev_details_message.text = "  " + models.message
            ev_task_details.text = "  " + models.task_status

            if (!models.task.isEmpty()) {
                CustomTextView.marquee(ev_details_task)
            }

            CustomTextView.marquee(ev_details_mail)

            if (models.enquiry_date != null) {
                ev_details_enquiry_date.text = "  " + DateUtils.convertDates(models.enquiry_date, DateUtils.CURRENT_FORMAT, DateUtils.DD_MM) + "  " + models.enquiry_time
            }
/*            if (models.assigned_date != null) {
                ev_details_enquiry_assigned_date.text = "  " + DateUtils.convertDates(models.assigned_date, DateUtils.CURRENT_FORMAT + " HH:mm:ss", DateUtils.DD_MM + " hh:mm a")
            }*/
            ev_details_enquiry_assigned_date.visibility = View.GONE
            ev_details_enqquiry_for.text = "  " + models.enquiry_for
            ev_details_enqquiry_for.visibility = View.VISIBLE
        } catch (nullPointer: NullPointerException) {
            nullPointer.printStackTrace()
        }


        ass_self_date_edt.setOnClickListener {
            showDateDailog()
        }

        ass_self_time_edt.setOnClickListener {
            var calendar = Calendar.getInstance()
            val d = TimePickerDialog(activity, getTimeFromPicker(ass_self_time_edt), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false)
            d.window.setWindowAnimations(R.style.grow)
            d.show()
        }

        ass_cancel.setOnClickListener {
            activity!!.onBackPressed()
        }


        ic_back.setOnClickListener {
            activity!!.onBackPressed()
        }

        ass_done.setOnClickListener {
            var date = ass_self_date_edt.text.toString().trim()
            var time = ass_self_time_edt.text.toString().trim()
            if (!MyUtils.isEmptySnackView(activity, ass_snack_view, assign, "Select Assign To")) {
                if (!MyUtils.isEmptySnackView(activity, ass_snack_view, if (assignId.equals("-1")) "" else assignId, "Select Assign " + assign)) {
                    if (!MyUtils.isEmptySnackView(activity, ass_snack_view, date, "Select Date")) {
                        if (!MyUtils.isEmptySnackView(activity, ass_snack_view, time, "Select Time")) {
                            var task = ass_assigned_task_sp.selectedItem.toString()
                            if (!task.equals("Select Task")) {
                                var map = HashMap<String, String>()
                                map["assign"] = assign
                                map["assignId"] = assignId
                                map["time"] = time
                                map["date"] = date
                                map["task"] = task

                                map["who_assign"] = LocalSession.getUserInfo(activity, KEY_ID)
                                map["enquiry_id"] = "" + models.id

                                onAssignedInCRM(map)

                            } else {
                                MessageUtils.showSnackBar(activity, ass_snack_view, "Select Task")
                            }
                        }
                    }
                }
            }

        }

        ass_assigned_to_sp.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {

                val swt = adapterView.getItemAtPosition(i) as MultiUserSpinnerItem
                assignId = swt.id

                Log.d("keysdsd", "" + id + "  " + swt.name)

                val taskSelf = ass_assigned_to_sp.getSelectedItem().toString()
                if (taskSelf.isEmpty()) {
                    //taskSelf == "Select " + assign
                    ass_txt_assign_to.setText("")
                } else {
                    ass_txt_assign_to.setText("" + taskSelf)
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        })


        ass_assigned_task_sp.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {


                val taskSelf = ass_assigned_task_sp.getSelectedItem().toString()
                if (taskSelf.isEmpty()) {
                    //taskSelf == "Select Task"
                    ass_txt_assign_task.setText("")
                } else {
                    ass_txt_assign_task.setText("" + taskSelf)
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        })


        rg_ass.setOnCheckedChangeListener { radioGroup, i ->
            assign = if (R.id.rb_ass_dealer == i) getString(R.string.dealer) else getString(R.string.corporate)

            var city = ev_details_city.text.toString().trim()
            var mobileNo = ev_details_mobile_no.text.toString().trim()
            var enquirFor = ev_details_enqquiry_for.text.toString().trim()
            var vehicleModel = ev_details_vehicle_model.text.toString().trim()
            val stringStringHashMap = HashMap<String, String>()

            stringStringHashMap["city"] = city
            stringStringHashMap["enquiry_for"] = enquirFor
            stringStringHashMap["assigned_to"] = assign
            var retoroModels: Call<AssingFromCRM> = MyUtils.getInstance().onAssignFromCRM(BEARER+LocalSession.getUserInfo(activity, KEY_TOKEN),stringStringHashMap);
            var dialog = MessageUtils.showDialog(activity)
            dialog.setOnKeyListener { dialogInterface, i, keyEvent ->

                MessageUtils.dismissDialog(dialog);
                retoroModels.cancel()
                false

            }
            retoroModels.enqueue(object : retrofit2.Callback<AssingFromCRM> {
                override fun onResponse(call: Call<AssingFromCRM>?, response: Response<AssingFromCRM>?) {
                    MessageUtils.dismissDialog(dialog);
                    if (response?.isSuccessful!!) {
                        var pojoModel: AssingFromCRM = response.body()!!
                        if (pojoModel != null) {
                            if (pojoModel!!.success) {
                                if (pojoModel!!.data != null)
                                    assign_details.visibility = View.VISIBLE

                                for (dealer in pojoModel!!.data.users) {
                                    var pass = MultiUserSpinnerItem(dealer.name, dealer.id)
                                    dealerList.add(pass)
                                }

                                dealerList.add(0, if (dealerList.size == 0) MultiUserSpinnerItem("No Found " + assign, "-1") else MultiUserSpinnerItem("Select " + assign, "-1"))
                                var adapter = ArrayAdapter<MultiUserSpinnerItem>(activity, R.layout.spinnertext, R.id.text1, dealerList)
                                adapter.setDropDownViewResource(R.layout.spinnertext);
                                ass_assigned_to_sp.adapter = adapter

                                taskList.clear()
                                for (taskInfo in pojoModel.data.tasks) {
                                    taskList.add(MultiTaskSpinnerItem(taskInfo.task_type, taskInfo.status, taskInfo.task))
                                }
                                taskList.add(0, if (taskList.size == 0) MultiTaskSpinnerItem("-1", "", "No Found Task") else MultiTaskSpinnerItem("-1", "", "Select Task"))

                                var adapter1 = ArrayAdapter<MultiTaskSpinnerItem>(activity, R.layout.spinnertext, R.id.text1, taskList)
                                adapter.setDropDownViewResource(R.layout.spinnertext);
                                ass_assigned_task_sp.adapter = adapter1

                            } else {
                                MessageUtils.showSnackBar(activity, ass_snack_view, pojoModel!!.message)
                            }
                        } else {
                            MessageUtils.showSnackBar(activity, ass_snack_view, pojoModel!!.message)
                        }
                    } else {
                        var msg = MessageUtils.showErrorCodeToast(response.code())
                        MessageUtils.showSnackBar(activity, ass_snack_view, msg)
                    }
                }

                override fun onFailure(call: Call<AssingFromCRM>?, t: Throwable?) {
                    MessageUtils.dismissDialog(dialog);
                    var msg = MessageUtils.showFailureToast(t?.message)
                    MessageUtils.showSnackBar(activity, ass_snack_view, msg)
                }
            })
        }
    }

    private fun onAssignedInCRM(map: HashMap<String, String>) {
        var dialog = MessageUtils.showDialog(activity)

        var assignModel: Call<AssignedModel> = MyUtils.getRetroService().onAssignedInCRM(BEARER+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
        dialog.setOnKeyListener { dialogInterface, i, keyEvent ->
            MessageUtils.dismissDialog(dialog);
            assignModel.cancel()
            false
        }

        assignModel.enqueue(object : Callback<AssignedModel> {
            override fun onFailure(call: Call<AssignedModel>?, t: Throwable?) {
                MessageUtils.dismissDialog(dialog);
                var msg = MessageUtils.showFailureToast(t?.message)
                MessageUtils.showSnackBar(activity, ass_snack_view, msg)
            }

            override fun onResponse(call: Call<AssignedModel>?, response: Response<AssignedModel>?) {
                MessageUtils.dismissDialog(dialog);
                if (response?.isSuccessful!!) {
                    var pojoAssigned = response.body()

                    if (pojoAssigned != null) {
                        if (pojoAssigned.success) {
                            MessageUtils.showToastMessage(activity, pojoAssigned.message)

                            activity!!.onBackPressed()
                        } else {
                            MessageUtils.showSnackBar(activity!!, ass_snack_view, pojoAssigned.message)
                        }
                    } else {
                        MessageUtils.showSnackBar(activity!!, ass_snack_view, getString(R.string.api_response_not_found))
                    }
                } else {
                    var msg = MessageUtils.showErrorCodeToast(response.code())
                    MessageUtils.showSnackBar(activity, ass_snack_view, msg)
                }
            }
        })
    }


    private fun showDateDailog() {

        val datePickerDialog = DatePickerDialog(activity!!, DatePickerDialog.OnDateSetListener { datePicker, selectedYear, selectedMonth, selectedDate ->
            year = selectedYear
            month = selectedMonth
            day = selectedDate
            val dateFormatter = SimpleDateFormat(DateUtils.DD_MM_YYYY, Locale.US)
            val newDate = Calendar.getInstance()
            newDate.set(year, month, day)
            ass_self_date_edt.setText(dateFormatter.format(newDate.time))

        }, year, month, day)
        datePickerDialog.datePicker.minDate = System.currentTimeMillis()
        datePickerDialog.window!!.setWindowAnimations(R.style.grow)
        datePickerDialog.show()
    }


    private fun getTimeFromPicker(mTime: TextView): TimePickerDialog.OnTimeSetListener {

        return TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->

            var hourOfDay = hourOfDay
            Log.d("TimefromTimePicker", "$view $hourOfDay  $minute")

            var tims = DateUtils.convertDates("" + hourOfDay + ":" + minute, "HH:mm", "hh:mm a")

            /*var AM_PM = " AM"
            var mm_precede = ""
            val hourOfDayPlusTwo = hourOfDay + 2
            //if (hourOfDay >= 9 && 22 > hourOfDay) {
            if (hourOfDay >= 12) {
                AM_PM = " PM"
                if (hourOfDay >= 13 && hourOfDay < 24) {
                    hourOfDay -= 12
                } else {
                    hourOfDay = 12
                }
            } else if (hourOfDay == 0) {
                hourOfDay = 12
            }
            if (minute < 10) {
                mm_precede = "0"
            }
*/
            mTime.text = tims
        }
    }

    class Passing(
            var name: String, val id: String) {
        override fun toString(): String {
            return name
        }
    }


    internal class MultiTaskSpinnerItem(var task_type: String, var status: String, var task: String) {

        override fun toString(): String {
            return task
        }
    }

}