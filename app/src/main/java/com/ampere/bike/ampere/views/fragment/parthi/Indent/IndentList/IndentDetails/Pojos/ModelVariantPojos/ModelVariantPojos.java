package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.ModelVariantPojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelVariantPojos {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("vehicle_model_id")
    @Expose
    private Integer vehicleModelId;
    @SerializedName("varient_code")
    @Expose
    private String varientCode;
    @SerializedName("varient_id")
    @Expose
    private Integer varientId;
    @SerializedName("model_varient")
    @Expose
    private String modelVarient;
    @SerializedName("colors")
    @Expose
    private String colors;
    @SerializedName("subsidy_amount")
    @Expose
    private String subsidyAmount;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVehicleModelId() {
        return vehicleModelId;
    }

    public void setVehicleModelId(Integer vehicleModelId) {
        this.vehicleModelId = vehicleModelId;
    }

    public String getVarientCode() {
        return varientCode;
    }

    public void setVarientCode(String varientCode) {
        this.varientCode = varientCode;
    }

    public Integer getVarientId() {
        return varientId;
    }

    public void setVarientId(Integer varientId) {
        this.varientId = varientId;
    }

    public String getModelVarient() {
        return modelVarient;
    }

    public void setModelVarient(String modelVarient) {
        this.modelVarient = modelVarient;
    }

    public String getColors() {
        return colors;
    }

    public void setColors(String colors) {
        this.colors = colors;
    }

    public String getSubsidyAmount() {
        return subsidyAmount;
    }

    public void setSubsidyAmount(String subsidyAmount) {
        this.subsidyAmount = subsidyAmount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}