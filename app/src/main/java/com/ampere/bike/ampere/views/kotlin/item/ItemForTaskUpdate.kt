package com.ampere.bike.ampere.views.kotlin.item

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.ampere.bike.ampere.interfaces.RemarksAndIDUpdate
import com.ampere.bike.ampere.model.task.TaskUpdateModel
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.BEARER
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.EnquiryDetails
import com.ampere.vehicles.R
import customviews.CustomTextView
import kotlinx.android.synthetic.main.dialog_drop_down.*
import kotlinx.android.synthetic.main.item_crm_view_details.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ItemForTaskUpdate(val activity: FragmentActivity, val bottomSheetBehavior: BottomSheetBehavior<*>, val itemValue: MutableList<EnquiryDetails>, val status: String) : RecyclerView.Adapter<ItemForTaskUpdate.HolderTask>() {
    var hasFocush: Boolean = false

    companion object {
        private lateinit var remarksAndIDUpdate: RemarksAndIDUpdate

        fun init(remarksAndIDUpdate: RemarksAndIDUpdate) {
            this.remarksAndIDUpdate = remarksAndIDUpdate
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderTask {
        return HolderTask(View.inflate(activity, R.layout.item_crm_view_details, null))
    }

    override fun getItemCount(): Int {
        return itemValue.size
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: HolderTask, position: Int) {
     try{
         val calendar = Calendar.getInstance()
         calendar.set(Calendar.MONTH, position)
         CustomTextView.marquee(holder.task_task)
         //CustomTextView.marquee(holder.task_remark)
         holder.task_id.text = itemValue[position].id

         var date = DateUtils.getFormattedDateFromTimestamp(calendar.timeInMillis, DateUtils.MMM_DD_YY)
         holder.task_date.text = DateUtils.convertDates(itemValue[position].activity_date, DateUtils.CURRENT_FORMAT + "HH:mm:ss", DateUtils.DD_MM)
         holder.task_task.text = itemValue[position].task
         holder.task_status.text = itemValue[position].status
         /*holder.task_customer_name.text = itemValue[position].completed_by*/

         Log.d("sdsdsdsds", "" + itemValue[position].status + "  " + itemValue[position].enquiry_status + "  remarks " + itemValue[position].remarks)

         if( itemValue[position].remarks!=null){

             var remark: String = itemValue[position].remarks as String
             if (!remark.isEmpty()) {
                 holder.task_remark.setText(remark)
                 holder.task_remark.setSingleLine()
             } else {
                 holder.task_remark.setText("")
             }
         }
         println("status update"+status)
         if (status.toLowerCase().equals("SS4".toLowerCase()) || status.toLowerCase().equals("S4".toLowerCase()) ) {
             holder.task_remark.isFocusableInTouchMode = false
             holder.task_remark.isEnabled = false
             holder.task_remark.isLongClickable = false
         } else {
             holder.task_remark.isFocusableInTouchMode = true
             holder.task_remark.isEnabled = true
             holder.task_remark.isLongClickable = true
         }

         /*if (itemValue.size != 1) {
             holder.task_remark.setText(itemValue[position].remarks)
         } else {*/
         /*if (position == (itemValue.size - 1)) {
             *//* if (!itemValue[position].enquiry_status.equals("Closure") && !itemValue[position].enquiry_status.equals("SS4") && !itemValue[position].enquiry_status.equals("Cold")) {
                 holder.task_remark.text = "click & add"
                 holder.task_remark.setTextColor(activity.resources.getColor(R.color.orange))
             } else {
                 holder.task_remark.text = "Closed"
             }*//*
            var ststusFromAct = CRMDetailsActivity.mStatus?.text.toString()
            Log.d("ststusFromAct", "" + ststusFromAct)
            holder.task_remark.setText(itemValue[position].remarks)
            *//* if (ststusFromAct.toLowerCase().trim().equals("SS4".toLowerCase())) {
                 Log.d("ststusFromAct", "1")
                 holder.task_remark.setText(itemValue[position].remarks)
             } else {
                 Log.d("ststusFromAct", "222")
                 holder.task_remark.setText("")
             }*//*
            //holder.task_remark.currentTextColor = activity.resources.getColor(R.color.red)
        } else {
            var remark: String = itemValue[position].remarks as String
            if (remark != null) {
                holder.task_remark.setText(remark)
            } else {
                holder.task_remark.setText("")
            }
        }*/
         //}


/*
        holder.task_remark.setOnClickListener {
            var ststusFromAct = CRMDetailsActivity.mStatus?.text.toString()
            Log.d("ststusFromActsdsdsds", "" + ststusFromAct)
            if (!ststusFromAct.toLowerCase().trim().equals("SS4".toLowerCase())) {
                //if (!itemValue[position].task.equals("Win") && !itemValue[position].task.equals("Lost") && !itemValue[position].task.equals("Cold")) {
                val remark = holder.task_remark.text
                //if (remark.isEmpty() && remark.equals("click & add")) {
                if (position == (itemValue.size - 1)) {
                    CRMDetailsActivity.childId = itemValue[position].id
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                }
            } else {

                MessageUtils.showToastMessage(activity, "Enquiry has been Colsed")
            }
        }*/




         holder.task_remark.setOnFocusChangeListener { view, hasFocus ->
             Log.d("hasFocussdsd", "" + hasFocus + " position " + position + " size " + itemValue.size);

             hasFocush = hasFocus;
             if (!hasFocus) {
                 Log.d("what_sdisoid", "11");

                 var remarkCahnged: String = view.task_remark.text.toString()
                 Log.d("valuess", "  " + remarkCahnged + "  " + position);
                 itemValue.get(position).remarks = remarkCahnged
                 if (remarksAndIDUpdate != null) {
                     remarksAndIDUpdate.updateIDAndRemarkDetails(itemValue)
                 }
             }

         }
         holder.task_remark.addTextChangedListener(object : TextWatcher {
             override fun afterTextChanged(p0: Editable?) {
                 if (hasFocush) {
                     itemValue.get(position).remarks = p0.toString()
                     if (remarksAndIDUpdate != null) {
                         remarksAndIDUpdate.updateIDAndRemarkDetails(itemValue)
                     }
                 }
             }

             override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

             }

             override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

             }

         })

         holder.item_task_view.setOnClickListener {
             if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                 bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
             }
         }


         /*holder.task_status.setOnClickListener {
             val statusValues = ArrayList<String>()
             //dialog = MessageUtils.showDialog(this)
             val map = HashMap<String, String>()
             map.put("enquiry_id", enquiryId)
             //map.put("status", CRMDetailsActivity.mTaskDetails?.text.toString().trim())
             map.put("status", "New Enquiry")
             var retrofit = MyUtils.getInstance()
             var dialog = MessageUtils.showDialog(activity)
             val statusModel: Call<StatusModel> = retrofit.onGetStatus(map);
             dialog?.dismiss()
             dialog?.setOnKeyListener { dialogInterface, i, keyEvent ->
                 dialogInterface.dismiss()
                 statusModel.cancel()
                 false
             }

             statusModel.enqueue(object : Callback<StatusModel> {
                 override fun onResponse(call: Call<StatusModel>?, response: SpareInvoicePojos<StatusModel>?) {
                     dialog?.dismiss()
                     dialog = null
                     if (response?.isSuccessful!!) {
                         var models = response.body()
                         if (models != null) {
                             if (models.success) {
                                 var newTask = ArrayList<NewTaskStatusUpdate>()
                                 for (statuass in models.data) {
                                     statusValues.add(statuass.task)
                                     var addTaskId = NewTaskStatusUpdate(statuass.task, statuass.id, statuass.status, statuass.taskType);
                                     newTask.add(addTaskId)
                                 }

                                 showStatusDialog(statusValues, newTask)

                             } else {
                                 // MessageUtils.showSnackBar(applicationContext, mSnackView, models.message)
                             }
                         } else {
                             //MessageUtils.showSnackBar(applicationContext, mSnackView, getString(R.string.api_response_not_found))
                         }
                     } else {
                         //MessageUtils.setErrorKotlin(applicationContext, mSnackView, response.code())
                     }
                 }

                 override fun onFailure(call: Call<StatusModel>?, t: Throwable?) {
                     dialog?.dismiss()
                     // MessageUtils.setFailureKotlin(applicationContext, mSnackView, t?.message)
                 }
             })

         }*/
     }catch (ex:Exception){
         ex.printStackTrace()
     }
    }

    @SuppressLint("ResourceType")
    private fun showStatusDialog(statusValues: ArrayList<String>, newTask: ArrayList<NewTaskStatusUpdate>) {


        val dialogLi = Dialog(activity)
        dialogLi.window.setWindowAnimations(R.anim.design_bottom_sheet_slide_out)
        dialogLi.setContentView(R.layout.dialog_drop_down)
        dialogLi.show()
        val dialogTitle = dialogLi.status
        dialogTitle.text = "Task Status"
        val dialogList = dialogLi.dialog_list

        dialogList.adapter = ArrayAdapter<String>(activity, R.layout.spinnertext, R.id.text1, statusValues)

        dialogList.onItemClickListener = AdapterView.OnItemClickListener { parent, _view, poi, id ->

            //var idd = newTask[poi].id

            dialogLi.dismiss()

            var updateTaskDetails = java.util.HashMap<String, String>()
            /* updateTaskDetails.put("user_name", LocalSession.getUserInfo(activity, LocalSession.KEY_NAME))
             updateTaskDetails.put("mobile_no", LocalSession.getUserInfo(activity, LocalSession.KEY_MOBILE))
             updateTaskDetails.put("id", "" + enquiryId)
             updateTaskDetails.put("task_status", "" + statusNew)
 */

            var model = MyUtils.getInstance().onTaskStatusChange(BEARER + LocalSession.getUserInfo(activity, KEY_TOKEN), updateTaskDetails)
            var dialog = MessageUtils.showDialog(activity as FragmentActivity?)
            dialog?.setOnKeyListener { _dialog, _keyCode, event ->
                MessageUtils.dismissDialog(dialog)
                false
            }

            model.enqueue(object : Callback<TaskUpdateModel> {
                override fun onResponse(call: Call<TaskUpdateModel>, response: Response<TaskUpdateModel>) {
                    dialog?.dismiss()
                    if (response.isSuccessful) {
                        var taskModel = response.body()
                        if (taskModel != null) {
                            Log.d("aslkalsk", "" + taskModel.isStatus)
                            if (taskModel.isStatus) {
                                //CRMDetailsActivity.mTaskDetails?.text = statusNew
                                MessageUtils.showToastMessage(activity, taskModel.message)
                            } else {
                                // MessageUtils.showSnackBar(context, holder.snack_view, taskModel.message)
                                MessageUtils.showToastMessage(activity, taskModel.message)
                            }
                        } else {
                            // MessageUtils.showSnackBar(context, holder.snack_view, "Not found ")
                            MessageUtils.showToastMessage(activity, "Not Found")
                        }
                    } else {
                        MessageUtils.showToastMessage(activity, MessageUtils.showErrorCodeToast(response.code()))
                        //MessageUtils.setErrorMessage(context, .snack_view, response.code())
                    }
                }

                override fun onFailure(call: Call<TaskUpdateModel>, t: Throwable) {
                    dialog?.dismiss()
                    MessageUtils.showToastMessage(activity, MessageUtils.showFailureToast(t.message))
                    //MessageUtils.setFailureMessage(context, holder.snack_view, t.message)
                }
            })
        }
    }

    class HolderTask(view: View) : RecyclerView.ViewHolder(view) {

        var task_date = view.task_date
        var task_task = view.task_task
        var task_status = view.task_status
        var task_remark = view.task_remark
        var item_task_view = view.item_task_view
        var task_customer_name = view.task_custmr_name
        var task_id = view.task_id


    }


    class NewTaskStatusUpdate(val task: String, val id: Long, val status: String, val taskType: String)
}