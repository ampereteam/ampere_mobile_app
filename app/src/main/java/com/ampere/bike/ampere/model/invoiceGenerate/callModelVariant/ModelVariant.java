package com.ampere.bike.ampere.model.invoiceGenerate.callModelVariant;

import com.ampere.bike.ampere.views.fragment.indent.VariantListPojos.SparePojos;
import com.ampere.bike.ampere.views.fragment.indent.VariantListPojos.VariantListPojos;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ModelVariant {
    @SerializedName("success")
    public boolean success;

    @SerializedName("data")
    public ModelVariantData modelVariantData;

    public class ModelVariantData {

        @SerializedName("varient")
        public String varient;

        @SerializedName("model_varient")
        public ArrayList<VariantListPojos> variantListPojos;

        @SerializedName("spares")
        public ArrayList<SparePojos> sparePojos;

        @SerializedName("colors")
        public ArrayList<String> colors;
    }

}
