package com.ampere.bike.ampere.views.kotlin.fragments.invoice

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.model.invoiceGenerate.callModelVariant.ModelVariant
import com.ampere.bike.ampere.model.invoiceGenerate.chassisNumber.ModelChassisNumber
import com.ampere.bike.ampere.model.invoiceGenerate.initial.*
import com.ampere.bike.ampere.model.invoiceGenerate.storedInvoice.ModelInvoiceStored
import com.ampere.bike.ampere.model.invoiceGenerate.variantColor.ModelVariantColor
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_ID
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.DateUtils.showDateDailog
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.utils.MyUtils.*
import com.ampere.bike.ampere.utils.Utils.convertDoubleTwoDigits
import com.ampere.bike.ampere.views.fragment.indent.ColorListPojos.ColorListPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMActivity
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.dialog
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.enquiryId
import com.ampere.bike.ampere.views.kotlin.fragments.invoice.invoicecolorpojos.ColorResponse
import com.ampere.bike.ampere.views.kotlin.fragments.invoice.invoicecolorpojos.ColorsItem
import com.ampere.vehicles.R
import com.ampere.vehicles.R.id.*
import com.google.gson.Gson
import customviews.CustomEditText
import customviews.CustomRadioButton
import customviews.CustomTextView
import kotlinx.android.synthetic.main.dialog_invoice.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class InvoiceActivity : AppCompatActivity() {

    private var enquiryId: String = ""
    private var user_name: String = ""
    private var task_name: String = ""
    var vehicleInventoryList: ArrayList<Any> = ArrayList<Any>()
    var vehicleModelsList: ArrayList<Vehiclemodel> = ArrayList<Vehiclemodel>()
    var proofsList: ArrayList<Proof> = ArrayList<Proof>()
    var gst: Gst? = null
    var stateLists: ArrayList<Statelists> = ArrayList<Statelists>()
    var modelDataEnquiry: Enquiry? = null
    var dialog: Dialog? = null
    var colorPojosList:ArrayList<ColorsItem?>? = ArrayList()

/*
    @BindView(R.id.invoice_cancel)
    lateinit var invoice_cancel: CustomButton
    @BindView(R.id.invoice_submit)
    lateinit var invoice_submit: CustomButton

     @BindView(R.id.mSnackView)
     lateinit var mSnackView: NestedScrollView
     @BindView(R.id.sendThrough)
     lateinit var sendThrough: CustomTextEditView
     @BindView(R.id.model_varent)
     lateinit var model_varent: CustomTextEditView
     @BindView(R.id.chassis_number)
     lateinit var chassis_number: CustomTextEditView
     @BindView(R.id.invoice_amt)
     lateinit var invoice_amt: CustomTextEditView
     @BindView(R.id.customer_name)
     lateinit var customer_name: CustomTextEditView
     @BindView(R.id.customer_address)
     lateinit var customer_address: CustomTextEditView
     @BindView(R.id.customer_proof)
     lateinit var customer_proof: CustomTextEditView
     @BindView(R.id.customer_address_proof)
     lateinit var customer_address_proof: CustomTextEditView
     @BindView(R.id.customer_email_id)
     lateinit var customer_email_id: CustomTextEditView
     @BindView(R.id.customer_mobile_no)
     lateinit var customer_mobile_no: CustomTextEditView
     @BindView(R.id.vehicle_model_color)
     lateinit var model_color: CustomTextEditView
 */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar!!.hide()

        var newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())

        setContentView(R.layout.dialog_invoice)

        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        var bundle = intent
         enquiryId = bundle.getStringExtra("id")
         user_name = bundle.getStringExtra("user_name")
         task_name = bundle.getStringExtra("task_name")
//        enquiryId = "34"

        onFocus()

        onLoadInvoiceDetails()

        ic_back_invoice.setOnClickListener {
            onBackPressed()
        }

        invoice_submit.setOnClickListener {
            try {
                val map = HashMap<String, String>()
                val vehicleModel: String = vehicle_model.text.toString()
                val modelVarient: String = model_varent.text.toString()
                val color: String = vehicle_model_color.text.toString()
                val chassisNo: String = chassis_number.text.toString()
                val invoiceAmount: String = invoice_amt.text.toString()
                val customerName: String = customer_name.text.toString()
                val customerAddress1: String = customer_address.text.toString()
                val customerAddress2: String = customer_address_2.text.toString()
                val locality: String = customer_locality.text.toString()
                val city: String = city.text.toString()
                val mobile: String = customer_mobile_no.text.toString()
                val pinCode: String = add_pincode.text.toString()
                val state: String = stat_edt.text.toString()
                val statecode: String = stat_code_edt.text.toString()
                val total_amount: String = customer_total_amonut.text.toString()
                val deliveryAt: String = customer_delivery_at.text.toString()
                val aadharNo: String = customer_aadhar_numer.text.toString()
                val selectedId: Int = radio_group.checkedRadioButtonId;
                var gender: String = ""
                // find the radiobutton by returned id
                if (selectedId != -1) {
                    val radioButton: CustomRadioButton = findViewById(selectedId);
                    gender = radioButton.text.toString()
                }

//enquiry_id, user_id, sendThrough, model_varient, serviceType, chassis_no, invoice_amount, customer_name, customer_address1, customer_address2, locality, city, mobile, pincode, state, statecode, total_amount, delivery_at, aadhar_no, gender,

                val idProof: String = id_proof_sp.selectedItem.toString()
                val addressProof: String = address_proof_sp.selectedItem.toString()
                val email: String = customer_email_id.text.toString()
                val dob: String = customer_dob.text.toString()
                val occupation: String = customer_occupation.text.toString()
                val subsidy_amount: String = customer_subsidy_amount.text.toString()
                val totalAmount: String = customer_total_amonut.text.toString()

                if (!isValidationOfInvoiceItems(enquiryId, vehicleModel, modelVarient, color, chassisNo, invoiceAmount, customerName, customerAddress1, customerAddress2, locality,
                                city, mobile, pinCode, state, statecode, total_amount, deliveryAt, aadharNo, gender,idProof,addressProof,dob,occupation)) {
                    // mandatory
                    map["enquiry_id"] = "" + enquiryId
                    map["user_id"] = "" + LocalSession.getUserInfo(this@InvoiceActivity, KEY_ID)
                    map["vehicle_model"] = "" + vehicleModel
                    map["model_varient"] = modelVarient
                    map["color"] = color
                    map["chassis_no"] = chassisNo
                    map["invoice_amount"] = invoiceAmount
                    map["customer_name"] = customerName
                    map["customer_address1"] = customerAddress1
                    map["customer_address2"] = customerAddress2
                    map["locality"] = locality
                    map["city"] = city
                    map["mobile"] = mobile
                    map["pincode"] = pinCode
                    map["state"] = state
                    map["statecode"] = statecode
                    map["total_amount"] = total_amount
                    map["delivery_at"] = deliveryAt
                    map["aadhar_no"] = aadharNo
                    map["gender"] = gender

                    //mon mandatory
                    map["id_proof"] = if (idProof != SELECT_ID_PROOF) {
                        idProof
                    } else {
                        ""
                    }
                    map["address_proof"] = if (addressProof != SELECT_ID_PROOF) {
                        addressProof
                    } else {
                        ""
                    }
                    map["email_id"] = email
                    map["dob"] = dob
                    map["occupation"] = occupation

                    //if (state.toLowerCase().equals(modelDataEnquiry?.state.toString().toLowerCase())) {
                    map["sgst_id"] = "" + gst?.sgst
                    map["sgst"] = "" + tax_sgst.text.toString()
                    map["cgst_id"] = "" + gst?.cgst
                    map["cgst"] = "" + tax_cgst.text.toString()
                    //} else {
                    map["igst_id"] = "" + gst?.igst
                    map["igst"] = "" + tax_igst.text.toString()
                    //}

                    map["subsidy"] = subsidy_amount
                    map["total_amount"] = totalAmount

                    onStoreInvoice(LocalSession.BEARER + LocalSession.getUserInfo(this@InvoiceActivity, LocalSession.KEY_TOKEN), map)
                }
            } catch (Ex:Exception) {
                Ex.printStackTrace()
            }
        }

        invoice_cancel.setOnClickListener {
            //MessageUtils.showToastMessage(this@InvoiceActivity, "Failure " + vehicleModelsList.size)
            onBackPressed()
        }

        customer_dob.setOnClickListener {
            showDateDailog()
        }

      /*  model_variant_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val variantType = model_variant_sp.selectedItem.toString()
                try {
                    if (variantType != null && !variantType.equals(SELECT_VARIANT_TYPE)) {
                        model_varent.setText(variantType)
                        val map = HashMap<String, String>()
                        map["sendThrough"] = "" + vehicle_model_sp.selectedItem.toString()
                        map["model_varient"] = "" + model_variant_sp.selectedItem.toString()
                        onLoadColors(LocalSession.BEARER + LocalSession.getUserInfo(this@InvoiceActivity, LocalSession.KEY_TOKEN), map)
                    } else {
                        model_varent.setText(" ")
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        }*/
        chassis_number_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val chassisNo = chassis_number_sp.selectedItem.toString()
                try {
                    Log.d("chassisNosdsd", "" + chassisNo)
                    if (chassisNo != null && !chassisNo.equals(SELECT_CHASSIS_NUMBER)) {

                        chassis_number.setText(chassisNo)
                    } else {
                        chassis_number.setText(" ")
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        }
        model_color_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val colors = model_color_sp.selectedItem.toString()
                vehicle_model_color.setText(colors)
                try {
                    if (colors != null && !colors.equals(SELECT_COLOR)) {
                        var map = HashMap<String, String>()
                        map["color"] = colors
                        map["user_id"] = LocalSession.getUserInfo(this@InvoiceActivity, KEY_ID)
                        map["vehicle_model"] = vehicle_model_sp.selectedItem.toString()
                       onLoadChassisNumber(map)

                    } else {
                        vehicle_model_color.setText(" ")
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        }
        state_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {}

            @SuppressLint("SetTextI18n")
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val stateName = state_sp.selectedItem.toString()
            val stateCodeArrayList = ArrayList<String>()
            try {
                if (stateName != null && !stateName.equals(SELECT_STATE_NAME)) {
                    re_state_code.visibility = View.VISIBLE
                    stat_edt.setText(stateName)
                    for (stateItem in stateLists) {
                        val names = stateItem.state_name
                        if (names.equals(stateName)) {
                            stateCodeArrayList.add(stateItem.state_code)
                            stat_code_edt.setText(stateItem.tin_number.toString())
                            if (modelDataEnquiry?.assignedperson != null) {
                                val state = modelDataEnquiry?.assignedperson?.state.toString()
                                val stateCode = modelDataEnquiry?.assignedperson?.statecode.toString()
                                Log.d("aslkdlsjd", "" + state + " stateCode " + stateCode);
                                val invoice = invoice_amt.text.toString()
                                if (!invoice.isEmpty()) {
                                    gstCalculationForInvoice(invoice, stateCode, stateItem.tin_number.toString())
                                } else {
                                    MessageUtils.showToastMessage(this@InvoiceActivity, "Enter Invoice Amount")
                                    invoice_amt.setError("Enter Invoice Amount")
                                    invoice_amt.requestFocus()
                                }

                            }
                        }
                    }
                    //state_code_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, stateCodeArrayList)

                } else {
                    stat_edt.setText(" ")
                    re_state_code.visibility = View.GONE
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        }

        invoice_amt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                try {
                    val inputValues = p0.toString()
                    Log.d("inputValuessdsd", "" + inputValues)
                    if (!inputValues.isEmpty()) {
                        val stateName = state_sp.selectedItem.toString()
                        if (!stateName.equals(SELECT_STATE_NAME)) {
                            val stateCodeSp = stat_code_edt.text.toString()
                            if (!stateCodeSp.isEmpty()) {
                                val stateCode = modelDataEnquiry?.assignedperson?.statecode.toString()
                                gstCalculationForInvoice(inputValues, stateCode, stateCodeSp)
                            }
                        }
                    } else {
                        val stateName = state_sp.selectedItem.toString()
                        if (!stateName.equals(SELECT_STATE_NAME)) {
                            val stateCodeSp = stat_code_edt.text.toString()
                            if (!stateCodeSp.isEmpty()) {
                                val stateCode = modelDataEnquiry?.assignedperson?.statecode.toString()
                                gstCalculationForInvoice("0", stateCode, stateCodeSp)
                            }
                        }
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }

        })
        customer_subsidy_amount.setSelection(customer_subsidy_amount.text.toString().length)
        customer_subsidy_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                try {
                    val inputValues = p0.toString()
                    Log.d("customer_subsidy_amount", "" + inputValues)

                    val stateName = state_sp.selectedItem.toString()
                    if (!stateName.equals(SELECT_STATE_NAME)) {
                        val stateCodeSp = stat_code_edt.text.toString()
                        if (!stateCodeSp.isEmpty()) {
                            val stateCode = modelDataEnquiry?.assignedperson?.statecode.toString()
                            val invoiceAmt = invoice_amt.text.toString().toDouble()
                            var totalAmt: Double = 0.0
                            if (!inputValues.isEmpty()) {
                                val subsidy = inputValues.toDouble()
                                totalAmt = calculationInvoice(stateCodeSp, stateCode, subsidy, invoiceAmt)
                                Log.d("totalAmtsdsd", "" + totalAmt)
                                if (totalAmt >= 1) {
                                    Log.d("totalAmtsdsd", " 111 " + totalAmt)
                                    customer_total_amonut.setText("" + convertDoubleTwoDigits("" + totalAmt))
                                } else {
                                    customer_total_amonut.setText("")
                                    MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Enter valid Amont")
                                    customer_subsidy_amount.setText("0")
                                    customer_subsidy_amount.setSelection(1)
                                    val subsidy = customer_subsidy_amount.text.toString().toDouble()
                                    val invoiceAmt = invoice_amt.text.toString().toDouble()
                                    totalAmt = calculationInvoice(stateCodeSp, stateCode, subsidy, invoiceAmt)
                                    Log.d("totalAmtsdsd", " 222  " + totalAmt)
                                    customer_total_amonut.setText("" + convertDoubleTwoDigits("" + totalAmt))
                                }
                            } else {

                                val subsidy: Double = 0.0
                                val invoiceAmt = invoice_amt.text.toString().toDouble()
                                var totalAmt = calculationInvoice(stateCodeSp, stateCode, subsidy, invoiceAmt)
                                Log.d("totalAmtsdsd", " 222222  " + totalAmt)
                                customer_total_amonut.setText("" + convertDoubleTwoDigits("" + totalAmt))
                            }
                        }
                    }

                } catch (ex: Exception) {
                    ex.printStackTrace()

                    MessageUtils.showToastMessage(this@InvoiceActivity, "" + ex.message)
                }
            }

            private fun calculationInvoice(stateCodeSp: String, stateCode: String, subsidy: Double, invoiceAmt: Double): Double {

                if (stateCode == stateCodeSp) {
                    same_state.visibility = View.VISIBLE
                    tip_igst.visibility = View.GONE

                    val cgstCalc = invoiceAmt * gst?.cgst.toString().toDouble() / 100
                    val sgstCalc = invoiceAmt * gst?.sgst.toString().toDouble() / 100
                    return ((cgstCalc + sgstCalc) + invoiceAmt) - subsidy

                } else {
                    same_state.visibility = View.GONE
                    tip_igst.visibility = View.VISIBLE
                    val igstCalc = invoiceAmt * gst?.igst.toString().toDouble() / 100
                    return (igstCalc + invoiceAmt) - subsidy

                }

            }

        })

        state_code_sp.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {}

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        val stateCode = state_code_sp.selectedItem.toString()
                        try {
                            if (stateCode != null) {
                                stat_code_edt.setText(stateCode)
                            } else {
                                stat_code_edt.setText(" ")
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }
                }

        id_proof_sp.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {}

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        val idProof = id_proof_sp.selectedItem.toString()
                        try {
                            if (idProof != null && idProof.equals(SELECT_ID_PROOF)) {
                                customer_proof.setText(idProof)
                            } else {
                                customer_proof.setText(" ")
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }
                }


        address_proof_sp.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {}

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        try {
                            val addressProof = address_proof_sp.selectedItem.toString()
                            Log.d("addressProofsdsd", "" + addressProof)
                            if (addressProof != null && !addressProof.equals(SELECT_ADDRESS_PROOF)) {
                                customer_address_proof.setText(addressProof)
                            } else {
                                customer_address_proof.setText(" ")
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                    }
                }
        vehicle_model_sp.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                        val vehicleModel: String = vehicle_model_sp.selectedItem.toString()
                        if (!vehicleModel.equals(SELECT_VEHICLE_MODEL)) {
                            try {
                                //if (vehicleModel != null) {
                                vehicle_model.setText(vehicleModel)
                                Log.d("vehicleModelsdsd", "" + vehicleModel)
                                if (dialog == null) {
                                    dialog = MessageUtils.showDialog(this@InvoiceActivity)
                                } else {
                                    dialog?.show()
                                }
                                val map = HashMap<String, String>()
                                map.put("vehicleModel", vehicleModel)
                                val callBackModel = MyUtils.getInstance().callForModelVariantFromVehicleModel(LocalSession.BEARER + LocalSession.getUserInfo(this@InvoiceActivity, LocalSession.KEY_TOKEN), map)

                                dialog?.setOnKeyListener { dialogInterface, i, keyEvent ->
                                    dialogInterface.dismiss()
                                    callBackModel.cancel()
                                    false
                                }

                                callBackModel.clone().enqueue(object : Callback<ColorResponse> {
                                    override fun onFailure(call: Call<ColorResponse>, t: Throwable) {
                                        MessageUtils.dismissDialog(dialog)
                                        val msg = MessageUtils.showFailureToast(t.message)
                                        Log.d("msgsdsd", "" + msg)
                                    }

                                    override fun onResponse(call: Call<ColorResponse>, response: Response<ColorResponse>) {
                                        MessageUtils.dismissDialog(dialog)
                                        if (response.isSuccessful) {
                                            /**old Requirement*/
                                           /* val modelVariant = response.body()
                                            if (modelVariant != null && modelVariant.success) {
                                                if (modelVariant.modelVariantData != null) {
                                                    // if variant comes available variant model is true.
                                                    // not avialable is not come but colors and spares comes
                                                    if (modelVariant.modelVariantData.varient.toLowerCase().equals("Available".toLowerCase())) {
                                                        re_model_variant.visibility = View.VISIBLE
                                                        val variantArrayList = ArrayList<String>()
                                                        variantArrayList.add(0, SELECT_VARIANT_TYPE)
                                                        for (variantListPojos in modelVariant.modelVariantData.variantListPojos) {
                                                            variantArrayList.add(variantListPojos.modelVarient)
                                                        }

                                                        model_variant_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, variantArrayList)

                                                        for (i in 0..variantArrayList.size - 1) {
                                                            if (variantArrayList[i].equals(modelDataEnquiry?.sendThrough))
                                                                model_variant_sp.setSelection(i)
                                                        }
                                                    } else {
                                                        re_model_variant.visibility = View.GONE
                                                        *//*var sparesArrayList = ArrayList<String>()
                                                        for (sparePojo in modelVariant.modelVariantData.sparePojos) {
                                                            sparesArrayList.add(sparePojo.spares)
                                                        }
                                                        spare.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, sparesArrayList)
            *//*
                                                        val colorsArray = modelVariant.modelVariantData.colors
                                                        colorsArray.add(0, SELECT_COLOR)
                                                        model_color_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, colorsArray)
                                                        for (i in 0..colorsArray.size - 1) {
                                                            if (colorsArray.get(i).equals(modelDataEnquiry?.vehicle_color)) {
                                                                model_color_sp.setSelection(i)
                                                            }
                                                        }
                                                    }
                                                }*/
                                            val colorResponse = response.body()
                                            var colorStringList :ArrayList<String> = ArrayList()
                                            colorPojosList = colorResponse?.data?.colors!!
                                            if(colorPojosList!=null && colorPojosList?.size!=0){
                                                colorStringList!!.clear()
                                                for(color in colorPojosList!!){
                                                    colorStringList.add(color?.color!!)
                                                }
                                                try{

//                                                Log.d("ColorList ",""+colorStringList.get(colorStringList.indexOf(modelDataEnquiry?.vehicle_color)))
                                                model_color_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity,R.layout.spinnertext,R.id.text1,colorStringList)
                                                model_color_sp.setSelection(colorStringList.indexOf(modelDataEnquiry?.vehicle_color))

                                                }catch (e:Exception){
                                                    e.printStackTrace()
                                                }
                                            } else {
                                                colorStringList!!.clear()
                                                colorStringList.add(getString(R.string.no_data_avl))
                                                val msg = MessageUtils.showErrorCodeToast(response.code())
                                                Log.d("msgsdsd", "" + msg)
                                                model_color_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity,R.layout.spinnertext,R.id.text1,colorStringList)
                                            }
                                        }
                                    }
                                })
                                /*} else {
                                                    sendThrough.setText(" ")
                                                }*/
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                        } else {
                            vehicle_model.setText(" ")
                        }
                    }

                    override fun onNothingSelected(adapterView: AdapterView<*>) {}
                }
    }

    private fun isValidationOfInvoiceItems(enquiryId: String, vehicle_model: String, modelVarient: String, color: String, chassisNo: String,
                                           invoiceAmount: String, customerName: String, customerAddress1: String, customerAddress2: String,
                                           locality: String, city: String, mobile: String, pinCode: String, state: String, statecode: String,
                                           total_amount: String, deliveryAt: String, aadharNo: String, gender: String,idProof:String,
                                           addressProof:String,dob:String,occupation:String): Boolean {
        Log.d("sdlksldkls", "  " + re_model_variant.visibility + "  VISIBLE " + View.VISIBLE + " GONE " + View.GONE)
        Log.d("enquiryIdsdsd", "" + enquiryId + "  vehicle_model " + vehicle_model + "  modelVarient  " + modelVarient + " color " + color + " chassisNo " + chassisNo)

        if (enquiryId.isEmpty()) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Enquiry Id Missing")
            return true
        } else if (vehicle_model.isEmpty() || vehicle_model == " " || vehicle_model.toLowerCase().equals(SELECT_VEHICLE_MODEL.toLowerCase())) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, SELECT_VEHICLE_MODEL)
            return true

        } /*else if (re_model_variant.visibility == View.VISIBLE) {
            if (modelVarient.isEmpty() || modelVarient == " " || modelVarient.toLowerCase().equals(SELECT_VARIANT_TYPE.toLowerCase())) {
                MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, SELECT_VARIANT_TYPE)
                return true
            }
        } */ else if (color.isEmpty() || color == " " || color.toLowerCase().equals(SELECT_COLOR.toLowerCase())) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, SELECT_COLOR)
            return true
        } else if (chassisNo.isEmpty() || chassisNo == " " || chassisNo.toLowerCase().equals(SELECT_CHASSIS_NUMBER.toLowerCase()) || chassisNo.equals(getString(R.string.no_data_avl))) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, SELECT_CHASSIS_NUMBER)
            return true
        } else if (invoiceAmount.isEmpty() || invoiceAmount == " " || invoiceAmount == "0") {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Invoice Amonut can't be empty")
            return true
        } else if (customerName.isEmpty()) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Customer Name can't be empty")
            return true
        } else if (customerAddress1.isEmpty() || customerAddress1.equals(getString(R.string.no_data_avl))) {
            MessageUtils.showSnackBar(this@InvoiceActivity,add_invoice_snack_view, "Address1 not Valid")
            return true
        } else if (customerAddress2.isEmpty() || customerAddress2.equals(getString(R.string.no_data_avl))) {
            MessageUtils.showSnackBar(this@InvoiceActivity,add_invoice_snack_view, "Address2 not Valid")
            return true
        } else if (locality.isEmpty()) {
            MessageUtils.showSnackBar(this@InvoiceActivity,add_invoice_snack_view, "Locality can't be empty")
            return true
        } else if (city.isEmpty()) {
            MessageUtils.showSnackBar(this@InvoiceActivity,add_invoice_snack_view, "City can't be empty")
            return true
        } else if (mobile.isEmpty() || mobile.length < 10) {
            MessageUtils.showSnackBar(this@InvoiceActivity,add_invoice_snack_view, "Please enter 10 digit mobile number")
            return true
        } else if (pinCode.isEmpty()) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Pincode can't be empty")
            return true
        } else if (state.isEmpty() || state == " " || state.equals(SELECT_STATE_NAME)) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "State can't be empty")
            return true
        } else if (total_amount.isEmpty()) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Total Amount can't be empty")
            return true
        }else if (idProof.isEmpty() || idProof.equals("Select Id Proof")) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Customer ID PROOF can't be empty")
            return true
        } else if (addressProof.isEmpty() || addressProof.equals("Select Address Proof")) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Customer Address PROOF can't be empty")
            return true
        }
        else if (deliveryAt.isEmpty()) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Delivert At can't be empty")
            return true
        } else if (aadharNo.isEmpty() || aadharNo.toString().length < 12) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Enter Valid Aadhar Number")
            return true
        } else if (dob.isEmpty()) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Please Select Your DateOfBirth")
            return true
        }else if (occupation.isEmpty()) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Please Enter Your  Occupation")
            return true
        }else if (gender.isEmpty()) {
            MessageUtils.showSnackBar(this@InvoiceActivity, add_invoice_snack_view, "Please Select Your Gender")
            return true
        }
        return false

    }

    private fun onLoadChassisNumber(map: HashMap<String, String>) {
        if (dialog != null) {
            dialog!!.show()
        } else {
            dialog = MessageUtils.showDialog(this@InvoiceActivity)
        }

        var models = MyUtils.getInstance().calledChassisNumber(LocalSession.BEARER + LocalSession.getUserInfo(this@InvoiceActivity, LocalSession.KEY_TOKEN), map)

        models.enqueue(object : Callback<ModelChassisNumber> {
            override fun onFailure(call: Call<ModelChassisNumber>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                val msg = MessageUtils.showFailureToast(t.message)
                Log.d("msgsds_colors", "" + msg)
            }

            override fun onResponse(call: Call<ModelChassisNumber>, response: Response<ModelChassisNumber>) {
                try {
                    val modelChassisNumberArray = ArrayList<String>()
                    MessageUtils.dismissDialog(dialog)
                    if (response.isSuccessful) {
                        val modelChassisNumber = response.body()
                        if (modelChassisNumber != null) {
                            if (modelChassisNumber.success) {
                                if (modelChassisNumber.data != null) {
                                    if (modelChassisNumber.data.chassis != null)
                                        modelChassisNumberArray.clear()
                                        for (chassi in modelChassisNumber.data.chassis) {
                                            /* if (chassi.sendThrough.equals(map["sendThrough"])) {
                                                 if (re_model_variant.visibility == View.VISIBLE) {
                                                     if (chassi.model_varient.equals(map["model_varient"])) {
                                                         if (chassi.serviceType.equals(map["serviceType"])) {
                                                             modelChassisNumberArray.add(chassi.chassis_no)

                                                         }
                                                     }
                                                 } else {
                                                     if (chassi.serviceType.equals(map["serviceType"])) {
                                                         modelChassisNumberArray.add(chassi.chassis_no)
                                                     }
                                                 }
                                             }*/
                                            modelChassisNumberArray.add(chassi.chassis_no)
                                        }

                                    if (modelChassisNumberArray.size >= 1) {
                                        modelChassisNumberArray.add(0, SELECT_CHASSIS_NUMBER)

                                        chassis_number_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, modelChassisNumberArray)
                                    } else {
                                        modelChassisNumberArray.clear()
                                        modelChassisNumberArray.add(getString(R.string.no_data_avl))
                                        chassis_number_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, modelChassisNumberArray)
                                        val dialog = Dialog(this@InvoiceActivity)
                                        dialog.setContentView(R.layout.dialog_internet)
                                        val title = dialog.findViewById(R.id.cmn_title) as CustomTextView
                                        var msg = dialog.findViewById(R.id.cmn_msg) as CustomTextView
                                        val cancel = dialog.findViewById(R.id.cancel) as CustomTextView
                                        val done = dialog.findViewById(R.id.gotoSt) as CustomTextView
                                        dialog.setCancelable(false)
                                        dialog.show()
                                        title.text = getString(R.string.raise_intent_title)
                                        msg.text = getString(R.string.raise_intent_message)
                                        done.text = getString(R.string.raise_intent)
                                        done.setOnClickListener {
                                            dialog.dismiss()
                                            val indent = Intent(this@InvoiceActivity, CustomNavigationDuo::class.java)
                                            indent.putExtra("redirect", "indent")
                                            startActivity(indent)
                                            finish()
                                            //MyUtils.passFragmentBackStack(this@InvoiceActivity, Indent())
                                        }

                                        cancel.setOnClickListener {
                                            dialog.dismiss()
//                                            onBackPressed()
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }

        })
    }

    private fun gstCalculationForInvoice(invoice: String, stateCode: String, stateItemTiNumber: String) {
        try {
            val inVoiceAmt = invoice.toDouble()
            if (stateCode == stateItemTiNumber) {
                same_state.visibility = View.VISIBLE
                tip_igst.visibility = View.GONE
                Log.d("gstsdsd", "" + gst?.cgst + "  sgst" + gst?.sgst + " inVoiceAmt " + inVoiceAmt)
                val cgstCalc = inVoiceAmt * gst?.cgst.toString().toDouble() / 100
                val sgstCalc = inVoiceAmt * gst?.sgst.toString().toDouble() / 100

                Log.d("sgstCalcsdsd", "sgstCalc " + sgstCalc + " cgstCalc " + cgstCalc)
                tip_cgst.setHint("CGST (" + gst?.cgst.toString() + "% )")

                tax_cgst.setText(cgstCalc.toString())
                tax_sgst.setText(sgstCalc.toString())
                tip_sgst.setHint("SGST (" + gst?.sgst.toString() + "% )")

                var totalCalc: Double = ((cgstCalc + sgstCalc) + inVoiceAmt)
                val subsidy = customer_subsidy_amount.text.toString()

                if (!subsidy.isEmpty()) {
                    val subsidyAmt = subsidy.toDouble()
                    totalCalc = totalCalc - subsidyAmt
                }

                customer_total_amonut.setText("" + convertDoubleTwoDigits("" + totalCalc))

            } else {
                same_state.visibility = View.GONE
                tip_igst.visibility = View.VISIBLE
                val igstCalc = inVoiceAmt * gst?.igst.toString().toDouble() / 100
                Log.d("gstsdsd_others", "" + gst?.igst + " inVoiceAmt " + inVoiceAmt + " igstCalc " + igstCalc)
                tax_igst.setText(igstCalc.toString())
                tip_igst.setHint("IGST (" + gst?.igst.toString() + "% )")
                val subsidy = customer_subsidy_amount.text.toString()
                var totalAmt = igstCalc + inVoiceAmt
                if (!subsidy.isEmpty()) {
                    val subsidyAmt = subsidy.toDouble()
                    totalAmt = totalAmt - subsidyAmt
                }
                customer_total_amonut.setText("" + convertDoubleTwoDigits("" + totalAmt))
            }
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    private fun onLoadColors(header: String, map: HashMap<String, String>) {
        val colors = MyUtils.getInstance().onLoadColors(header, map)
        colors.clone().enqueue(object : Callback<ModelVariantColor> {
            override fun onFailure(call: Call<ModelVariantColor>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                val msg = MessageUtils.showFailureToast(t.message)
                Log.d("msgsds_colors", "" + msg)
            }

            override fun onResponse(call: Call<ModelVariantColor>, response: Response<ModelVariantColor>) {
                try {
                    MessageUtils.dismissDialog(dialog)
                    if (response.isSuccessful) {

                        val modelVariantColor = response.body()
                        if (modelVariantColor != null && modelVariantColor.success) {
                            if (modelVariantColor.data != null) {
                                val colorsArray: ArrayList<String> = modelVariantColor.data.colors
                                colorsArray.add(0, SELECT_COLOR)
                                model_color_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, colorsArray)

                            }
                        } else {
                            Log.d("msgsdsdColors", "" + modelVariantColor?.message)
                        }

                    } else {
                        val msg = MessageUtils.showFailureToast(response.message())
                        Log.d("msgsdsd", "" + msg)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        })
    }

    private fun onStoreInvoice(header: String, map: HashMap<String, String>) {

        val dialog = MessageUtils.showDialog(this@InvoiceActivity)

        val modelInvoiceStore = MyUtils.getInstance().calledToStoreInvoice(header, map)
        /* dialog.setOnKeyListener { dialogInterface, i, keyEvent ->
             dialogInterface.dismiss()
             modelInvoiceStore.cancel()
             false
         }*/
        modelInvoiceStore.clone().enqueue(object : Callback<ModelInvoiceStored> {
            override fun onFailure(call: Call<ModelInvoiceStored>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                val msg = MessageUtils.showFailureToast(t.message)
                Log.d("msgsds", "" + msg)

            }

            override fun onResponse(call: Call<ModelInvoiceStored>, response: Response<ModelInvoiceStored>) {
                try {
                    MessageUtils.dismissDialog(dialog)

                    if (response.isSuccessful) {
                        var modelInvoiceStored = response.body()
                        if (modelInvoiceStore != null) {
                            var modelInvoiceStored = response.body()
                            if (modelInvoiceStored != null) {
                                if (modelInvoiceStored.success) {
                                    MessageUtils.showToastMessage(this@InvoiceActivity, modelInvoiceStored.message)

                                    var intent = Intent(this@InvoiceActivity, CustomNavigationDuo::class.java)
                                    intent.putExtra("redirect", "SalesCustomerDetails")
                                    startActivity(intent)
                                    overridePendingTransition(0, 0)
                                    finish()
                                } else {
                                    MessageUtils.showToastMessage(this@InvoiceActivity, modelInvoiceStored.message)
                                }
                            } else {
                                MessageUtils.showToastMessage(this@InvoiceActivity, "SpareInvoicePojos not found")
                            }
                        } else {
                            MessageUtils.showToastMessage(this@InvoiceActivity, "Failure ")
                        }

                    } else {
                        val msg = MessageUtils.showFailureToast(response.message())
                        Log.d("msgsdsd", "" + msg)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
        })
    }

    private fun onLoadInvoiceDetails() {

        dialog = MessageUtils.showDialog(this@InvoiceActivity)
        val map = HashMap<String, String>()
        map.put("userid", "" + LocalSession.getUserInfo(this@InvoiceActivity, KEY_ID))
        map.put("enquiry_id", "" + enquiryId)

        val modelInvoice = MyUtils.getInstance().callInvoiceGenerateDetails(LocalSession.BEARER + LocalSession.getUserInfo(this@InvoiceActivity, LocalSession.KEY_TOKEN), map)


        dialog?.setOnKeyListener { dialogInterface, i, keyEvent ->
            dialogInterface.dismiss()
            modelInvoice.cancel()
            false

        }

        modelInvoice.clone().enqueue(object : retrofit2.Callback<ModelInvoice> {
            override fun onFailure(call: Call<ModelInvoice>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                val msg = MessageUtils.showFailureToast(t.message)
                Log.d("msgsds", "" + msg)

            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<ModelInvoice>, response: Response<ModelInvoice>) {
                MessageUtils.dismissDialog(dialog)
                try {
                    if (response.isSuccessful) {
                        val modelInvoices = response.body()
                        if (modelInvoices != null && modelInvoices.success) {
                            val modelData = modelInvoices.data
                            if (modelData != null) {
                                stateLists.clear()
                                proofsList.clear()
                                vehicleModelsList.clear()
                                vehicleInventoryList.clear()
                                modelDataEnquiry = modelData.enquiry
                                if (modelDataEnquiry != null) {

                                    /**
                                     * check and set the Data If Availability..
                                     *                                     *
                                     */
                                    /**
                                     * The Check  the Customer Name.
                                     */
                                    if(modelDataEnquiry?.customer_name.toString().isNullOrEmpty()){
                                        customer_name.setText(getString(R.string.no_data_avl))
                                    }else{
                                        customer_name.setText(modelDataEnquiry?.customer_name.toString())
                                    }
                                    /**
                                     * Check Address
                                     */
                                    if(modelDataEnquiry?.address1.toString().isNullOrEmpty()){
                                        customer_address.setText(getString(R.string.no_data_avl))
                                    }else{
                                        customer_address.setText(modelDataEnquiry?.address1.toString())
                                    }
                                    /**
                                     * The check address 2
                                     */
                                    if(modelDataEnquiry?.address2.toString().isNullOrEmpty()){
                                        customer_address_2.setText(getString(R.string.no_data_avl))
                                    }else{
                                        customer_address_2.setText(getString(R.string.no_data_avl))
                                    }
                                    /**
                                     * The check Locality                                     *
                                     */
                                    if(modelDataEnquiry?.locality.toString().isNullOrEmpty()){
                                        customer_locality.setText(getString(R.string.no_data_avl))
                                    }else{
                                        customer_locality.setText(modelDataEnquiry?.locality.toString())

                                    }
                                    /**
                                     * THe Check the Mobile Number
                                     */
                                    if(modelDataEnquiry?.mobile_no.toString().isNullOrEmpty()){
                                        customer_mobile_no.setText(getString(R.string.no_data_avl))
                                    }else{
                                        customer_mobile_no.setText(modelDataEnquiry?.mobile_no.toString())
                                    }
                                    /**
                                     * THe check City
                                     */
                                    if(modelDataEnquiry?.city.toString().isNullOrEmpty()){
                                        city.setText(getString(R.string.no_data_avl))
                                    }else{
                                        city.setText(modelDataEnquiry?.city.toString())
                                    }
                                    /**
                                     * The check pincode
                                     */
                                    if(modelDataEnquiry?.pincode.toString().isNullOrEmpty()){
                                        add_pincode.setText("N/A")


                                    }else{
                                        add_pincode.setText(modelDataEnquiry?.pincode.toString())

                                    }
                                    /**
                                     * The check  Customer Email Id
                                     */
                                    if(modelDataEnquiry?.email.toString().isNullOrEmpty()){
                                        customer_email_id.setText(getString(R.string.no_data_avl))
                                    }else{
                                        customer_email_id.setText(modelDataEnquiry?.email.toString())
                                    }
                                    Log.d("invoicAmount",""+ modelDataEnquiry?.invoice_value)
                                    var invoice = modelDataEnquiry?.invoice_value.toString()
                                    if (invoice != null && !invoice.isEmpty()) {
                                        invoice = invoice.replace(",", "")
                                        invoice_amt.setText("" + invoice)
                                    }else{
                                        invoice_amt.setText("0.00")
                                    }

                                    /*customer_total_amonut.setText(modelDataEnquiry?.tota)
                                    customer_delivery_at.setText(modelDataEnquiry?.delivery.toString())
                                    customer_aadhar_numer.setText(modelDataEnquiry?.city.toString())
                                    customer_dob.setText(modelDataEnquiry?.city.toString())
                                    customer_occupation.setText(modelDataEnquiry?.city.toString())
                                    customer_subsidy_amount.setText(modelDataEnquiry?.city.toString())
                                    customer_total_amonut.setText(modelDataEnquiry?.city.toString())*/
                                }
                                //list of state values
                                stateLists = modelData.statelists
                                if (stateLists != null) {
                                    val stateNameArrayList = ArrayList<String>()
                                    stateNameArrayList.add(0, SELECT_STATE_NAME)

                                    for (statelists in stateLists) {
                                        stateNameArrayList.add(statelists.state_name)
                                    }


                                    state_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, stateNameArrayList)
                                         var state=stateNameArrayList.indexOf(modelInvoices?.data?.enquiry?.state)
                                    state_sp.setSelection(state)
/*
                                    for (its in 0..stateNameArrayList.size - 1) {

                                        if (stateNameArrayList.get(its).equals(modelDataEnquiry?.state)) {
                                            address_proof_sp.setSelection(its)
                                        }
                                    }*/

                                }
                                //gst dataile
                                gst = modelData.gst
                                // list of Proof
                                proofsList = modelData.proofs
                                if (proofsList != null) {
                                    val proofArrayList = ArrayList<String>()
                                    val addressProofArrayList = ArrayList<String>()
                                    addressProofArrayList.add(0, SELECT_ADDRESS_PROOF)
                                    proofArrayList.add(0, SELECT_ID_PROOF)
                                    for (proof in proofsList) {
                                        proofArrayList.add(proof.proof)
                                        addressProofArrayList.add(proof.proof)
                                    }

                                    id_proof_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, proofArrayList)

                                    address_proof_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, addressProofArrayList)


                                }
                                //list of vehicleModels Details
                                vehicleModelsList = modelData.vehiclemodels

                                if (vehicleModelsList != null) {
                                    val vehicleModelArray = ArrayList<String>()
                                    vehicleModelArray.add(0, SELECT_VEHICLE_MODEL)
                                    for (vehicleModel in vehicleModelsList) {
                                        vehicleModelArray.add(vehicleModel.vehicle_model)
                                    }

                                    vehicle_model_sp.adapter = ArrayAdapter<String>(this@InvoiceActivity, R.layout.spinnertext, R.id.text1, vehicleModelArray)


                                    for (its in 0..vehicleModelArray.size - 1) {

                                        if (vehicleModelArray.get(its).equals(modelDataEnquiry?.vehicle_model)) {
                                            vehicle_model_sp.setSelection(its)
                                        }
                                    }
                                }

                                //list of VehicleInventory Info
                                vehicleInventoryList = modelData.vehicleinventory

                                onFocus()

                            }
                        }
                    } else {
                        val msg = MessageUtils.showFailureToast(response.message())
                        Log.d("msgsdsd", "" + msg)
                    }
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
        })
    }

    @SuppressLint("WrongConstant")
    private fun showDateDailog() {
        val c = Calendar.getInstance()
        var year = c.get(Calendar.YEAR)
        var month = c.get(Calendar.MONTH)
        var day = c.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(this@InvoiceActivity,DatePickerDialog.OnDateSetListener { _, selectedYear, selectedMonth, selectedDate ->
            year = selectedYear
            month = selectedMonth
            day = selectedDate
            val dateFormatter = SimpleDateFormat(DateUtils.DD_MM_YYYY, Locale.US)
            val newDate = Calendar.getInstance()
            newDate.set(year, month, day)
            customer_dob.setText(dateFormatter.format(newDate.time))
        }, year, month, day)
       c.add(Calendar.YEAR ,-18)
        datePickerDialog.datePicker.maxDate = c.timeInMillis
        datePickerDialog.window!!.setWindowAnimations(R.style.grow)
        datePickerDialog.show()
    }

    private fun onFocus() {
        try {
            CustomEditText.focus(invoice_amt)
            CustomEditText.focus(customer_name)
            CustomEditText.focus(customer_address)
            CustomEditText.focus(customer_address_2)
            CustomEditText.focus(customer_locality)
            CustomEditText.focus(add_pincode)
            CustomEditText.focus(customer_email_id)
            CustomEditText.focus(customer_occupation)
            CustomEditText.focus(customer_delivery_at)
            CustomEditText.focus(customer_aadhar_numer)
            CustomEditText.focus(customer_subsidy_amount)
//            CustomEditText.focus(customer_total_amonut)
            CustomEditText.focus(city)
            CustomEditText.focus(customer_mobile_no)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this@InvoiceActivity, CRMActivity::class.java)
        /*intent.putExtra("id", "" + enquiryId)
        intent.putExtra("user_name", "" + LocalSession.getUserInfo(this@InvoiceActivity, LocalSession.KEY_USER_NAME))
        intent.putExtra("task_name", "" + task_name)*/
        startActivity(intent)
        overridePendingTransition(0, 0)
        finish()
    }
}