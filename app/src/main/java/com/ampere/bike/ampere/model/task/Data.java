package com.ampere.bike.ampere.model.task;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Awesome Pojo Generator
 */
public class Data {
    @SerializedName("enquiries")
    @Expose
    public List<EnquiryModel> enquiry;
}