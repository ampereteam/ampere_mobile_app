package com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.FrrComplaintDetails

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_ID
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.FrrComplaintDetails.Adapter.ComplintDetailsAdpter
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.FrrComplaintDetails.Pojos.ComplaintDetailsPojos.ComplaintDetailsResponsePj
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity
import com.ampere.vehicles.R
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FreeReplacementComplaintDetailsKT : AppCompatActivity(),ConfigClickEvent {
    /**
     * Global Variable Declaration
     */
    var complaintid :String ?=null
    var msnakview : ConstraintLayout?=null
    var backpress :ImageView?=null
    var emptyList :TextView?=null
    var reclrComplaintDetailsList :RecyclerView?=null
    var callApi:UtilsCallApi ?=null
    var dialog :Dialog ?=null
    var complaintDtilsRes : ComplaintDetailsResponsePj ?=null
    var complintDetailsAdpter : ComplintDetailsAdpter?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try{
            this.supportActionBar?.hide()
        }catch (ex:Exception){
            ex.printStackTrace()
        }
        setContentView(R.layout.activity_free_replacement_complaint_details_kt)
        try {
            complaintid = intent.getStringExtra("complaintid")
        } catch (e: Exception) {
            e.printStackTrace()
        }
        dialog = Dialog(this)
        callApi =UtilsCallApi(this)
        backpress = findViewById(R.id.ic_back_complaintdetails)
        emptyList = findViewById(R.id.ffrc_details_emptyList)
        msnakview = findViewById(R.id.msnakview)
        reclrComplaintDetailsList = findViewById(R.id.ffrc_details_replcnt_complaint_list)
        reclrComplaintDetailsList?.layoutManager =LinearLayoutManager(this,LinearLayout.VERTICAL,false)
        backpress?.setOnClickListener {
            finish()
        }
        /**get Data from Server **/
        getDataFromSrvr()
    }

    private fun getDataFromSrvr() {
        try{
            if(callApi?.isNetworkConnected!!){
                dialog=MessageUtils.showDialog(this)
                val map :HashMap<String,Any> = HashMap()
                map["userid"]=LocalSession.getUserInfo(this, KEY_ID)
                map["complaintid"]=complaintid.toString()
                val calinterafce = callApi?.connectRetro("POST","freereplacementcomplaintitem")
                val callback = calinterafce?.call_post("freereplacementcomplaintitem","Bearer "+LocalSession.getUserInfo(this, KEY_TOKEN),map)
                    callback?.enqueue(object: Callback<ResponseBody>{
                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            MessageUtils.dismissDialog(dialog)
                            MessageUtils.showSnackBar(this@FreeReplacementComplaintDetailsKT,msnakview,getString(R.string.server_not_found))
                            Log.d("FailureREsponae",""+t.message)
                            Log.d("FailureREsponae",""+t.localizedMessage)
                        }

                        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                            MessageUtils.dismissDialog(dialog)
                            if(response.isSuccessful){
                                try{
                                    complaintDtilsRes = Gson().fromJson(response.body()?.string(),ComplaintDetailsResponsePj::class.java)
                                }catch (ex:Exception){
                                    ex.printStackTrace()
                                }
                                if(complaintDtilsRes?.data?.freecomplaints?.size !! <=0){
                                    emptyList?.setText(complaintDtilsRes?.message)
                                    emptyList?.visibility = View.VISIBLE
                                }else{
                                      emptyList?.visibility = View.GONE
                                    setToAdapter()
                                }
                            }else{
                                MessageUtils.showSnackBar(this@FreeReplacementComplaintDetailsKT,msnakview,getString(R.string.server_not_found))
                            Log.d("Response Failure",""+response.body().toString())
                            }
                        }

                    })
           }else{
                MessageUtils.showSnackBarAction(this,msnakview,getString(R.string.check_internet))
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setToAdapter() {
        try{
            complintDetailsAdpter = ComplintDetailsAdpter(this@FreeReplacementComplaintDetailsKT,complaintDtilsRes?.data?.freecomplaints,this )
            reclrComplaintDetailsList?.adapter =complintDetailsAdpter
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    override fun connectposition(position: Int, listSize: Int) {
       try {
           Log.d("CalledCoonection Fucntion",""+position+"\t"+listSize)
           Log.d("CalleCoonection Fucntion",""+complaintDtilsRes?.data?.freecomplaints?.get(position)?.status)
           Log.d("CalldCoonection Fucntion",""+complaintDtilsRes?.data?.freecomplaints?.get(position)?.complaint)
           Log.d("CaledCoonection Fucntion",""+complaintDtilsRes?.data?.freecomplaints?.get(position)?.complaintstatus)
           if(complaintDtilsRes?.data?.freecomplaints?.get(position)?.complaintstatus?.equals("View")!!){
               val intent = Intent(this, CRMDetailsActivity::class.java)
               intent.putExtra("id", "" +complaintDtilsRes?.data?.freecomplaints?.get(position)?.enquiryId)
               intent.putExtra("user_name", "" + complaintDtilsRes?.data?.freecomplaints?.get(position)?.complaint)
               intent.putExtra("task_name", "" + getString(R.string.free_replcment_Req))
               startActivity(intent)
           }else{
               val intent = Intent(this, CRMDetailsActivity::class.java)
               intent.putExtra("id", "" +complaintDtilsRes?.data?.freecomplaints?.get(position)?.enquiryId)
               intent.putExtra("user_name", "" + complaintDtilsRes?.data?.freecomplaints?.get(position)?.complaint)
               intent.putExtra("task_name", "" +getString(R.string.service))
               startActivity(intent)
           }
       }catch (ex:Exception){
           ex.printStackTrace()
       }

    }
}
