package com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Invoice.Pojos

import com.google.gson.annotations.SerializedName

class AcceptItem(
    @field:SerializedName("invoice_id")
    var invoiceID :String?=null
)