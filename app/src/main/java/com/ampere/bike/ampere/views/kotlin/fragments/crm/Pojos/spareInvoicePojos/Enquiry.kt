package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoicePojos

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Enquiry(

	@field:SerializedName("vehicle_color")
	val vehicleColor: String? = null,

	@field:SerializedName("assigned_date")
	val assignedDate: String? = null,

	@field:SerializedName("enquiry_time")
	val enquiryTime: String? = null,

	@field:SerializedName("mtype")
	val mtype: Any? = null,

	@field:SerializedName("vehcile_image")
	val vehcileImage: Any? = null,

	@field:SerializedName("enquiry_no")
	val enquiryNo: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("state")
	val state: String? = null,

	@field:SerializedName("pincode")
	val pincode: String? = null,

	@field:SerializedName("created_time")
	val createdTime: String? = null,

	@field:SerializedName("locality")
	val locality: String? = null,

	@field:SerializedName("modified_date")
	val modifiedDate: String? = null,

	@field:SerializedName("created_by")
	val createdBy: String? = null,

	@field:SerializedName("complaint_status")
	val complaintStatus: Any? = null,

	@field:SerializedName("task")
	val task: String? = null,

	@field:SerializedName("leadsource")
	val leadsource: String? = null,

	@field:SerializedName("enquiry_id")
	val enquiryId: String? = null,

	@field:SerializedName("assigned_to_sub")
	val assignedToSub: Any? = null,

	@field:SerializedName("assignedperson")
	val assignedperson: Assignedperson? = null,

	@field:SerializedName("vehicle_qty")
	val vehicleQty: String? = null,

	@field:SerializedName("assigned_person")
	val assignedPerson: Int? = null,

	@field:SerializedName("customer_type")
	val customerType: String? = null,

	@field:SerializedName("closing_date")
	val closingDate: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("invoice_value")
	val invoiceValue: Any? = null,

	@field:SerializedName("mobile_no")
	val mobileNo: String? = null,

	@field:SerializedName("vehicle_model")
	val vehicleModel: String? = null,

	@field:SerializedName("read_status")
	val readStatus: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("chassis_no")
	val chassisNo: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("assigned_to")
	val assignedTo: String? = null,

	@field:SerializedName("root_cause")
	val rootCause: Any? = null,

	@field:SerializedName("task_status")
	val taskStatus: String? = null,

	@field:SerializedName("address2")
	val address2: String? = null,

	@field:SerializedName("address1")
	val address1: String? = null,

	@field:SerializedName("service_issues")
	val serviceIssues: String? = null,

	@field:SerializedName("enquiry_type")
	val enquiryType: Any? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("warranty_component")
	val warrantyComponent: Any? = null,

	@field:SerializedName("service_type")
	val serviceType: Any? = null,

	@field:SerializedName("spares_id")
	val sparesId: Any? = null,

	@field:SerializedName("enquiry_date")
	val enquiryDate: String? = null,

	@field:SerializedName("modified_by")
	val modifiedBy: String? = null,

	@field:SerializedName("enquiry_for")
	val enquiryFor: String? = null,

	@field:SerializedName("assigned_to_self")
	val assignedToSelf: String? = null,

	@field:SerializedName("customer_name")
	val customerName: String? = null,

	@field:SerializedName("spares_name")
	val sparesName: Any? = null,

	@field:SerializedName("place_of_service")
	val placeOfService: Any? = null
)