package com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.FrrComplaintDetails.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.FrrComplaintDetails.Pojos.ComplaintDetailsPojos.FreecomplaintsItem
import com.ampere.vehicles.R
import java.lang.Exception

class ComplintDetailsAdpter(val context: Context, private val complaintDetailsList: List<FreecomplaintsItem?>?, val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<ComplintDetailsAdpter.Viewholder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComplintDetailsAdpter.Viewholder {
        val view :View =LayoutInflater.from(this.context).inflate(R.layout.adapter_frr_complaint_details,parent,false)
        return  Viewholder(view)
    }

    override fun getItemCount(): Int {
        return  this.complaintDetailsList?.size!!
    }

    override fun onBindViewHolder(holder: ComplintDetailsAdpter.Viewholder, position: Int) {
            try{
                holder.frrsno.text =(position+1).toString()
                holder.frrchassi .text = complaintDetailsList!![position]?.defectItem
                holder.frrcomplaint.text = complaintDetailsList[position]?.defectQty
                holder.frrsendDate.text = complaintDetailsList[position]?.complaint
                holder.frrstatus.text = complaintDetailsList[position]?.complaintstatus
                holder.frrlinear.setOnClickListener {
                    this.configClickEvent.connectposition(position,complaintDetailsList.size)
                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }

    }

    class Viewholder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val frrsno :TextView = itemView?.findViewById(R.id.frrc_txv_srl_no)!!
        val frrchassi :TextView = itemView?.findViewById(R.id.frrc_txv_chassis_no)!!
        val frrcomplaint  :TextView = itemView?.findViewById(R.id.frrc_txv_complaints)!!
        val frrsendDate :TextView = itemView?.findViewById(R.id.frrc_txv_sent_date)!!
        val frrstatus :TextView = itemView?.findViewById(R.id.frrc_txv_Warranty_sts)!!
        val frrlinear :LinearLayout = itemView?.findViewById(R.id.frrc_linear)!!
    }
}