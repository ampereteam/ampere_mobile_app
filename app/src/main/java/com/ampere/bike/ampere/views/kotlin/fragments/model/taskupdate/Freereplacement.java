package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Freereplacement {

    @SerializedName("freecomplaint_id")
    @Expose
    private Integer freecomplaintId;
    @SerializedName("component")
    @Expose
    private String component;
    @SerializedName("oldserial")
    @Expose
    private String oldserial;
    @SerializedName("sparesstock")
    @Expose
    private ArrayList<Sparesstock> sparesstock = null;

    public Integer getFreecomplaintId() {
        return freecomplaintId;
    }

    public void setFreecomplaintId(Integer freecomplaintId) {
        this.freecomplaintId = freecomplaintId;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getOldserial() {
        return oldserial;
    }

    public void setOldserial(String oldserial) {
        this.oldserial = oldserial;
    }

    public ArrayList<Sparesstock> getSparesstock() {
        return sparesstock;
    }

    public void setSparesstock(ArrayList<Sparesstock> sparesstock) {
        this.sparesstock = sparesstock;
    }

    public boolean isValid(){
        return  component!=null && oldserial != null && sparesstock!=null ;
    }

  public class   Sparesstock{

      @SerializedName("id")
      @Expose
      private Integer id;
      @SerializedName("indent_id")
      @Expose
      private String indentId;
      @SerializedName("vehicle_model")
      @Expose
      private String vehicleModel;
      @SerializedName("color")
      @Expose
      private String color;
      @SerializedName("spare_name")
      @Expose
      private String spareName;
      @SerializedName("serial_no")
      @Expose
      private String serialNo;
      @SerializedName("qty")
      @Expose
      private String qty;
      @SerializedName("indent_invoice_date")
      @Expose
      private String indentInvoiceDate;
      @SerializedName("manufacture_date")
      @Expose
      private String manufactureDate;
      @SerializedName("status")
      @Expose
      private String status;
      @SerializedName("acceptance")
      @Expose
      private String acceptance;
      @SerializedName("invoice_type")
      @Expose
      private String invoiceType;
      @SerializedName("user_id")
      @Expose
      private Integer userId;
      @SerializedName("delivered_by")
      @Expose
      private Integer deliveredBy;
      @SerializedName("delivery_indent_id")
      @Expose
      private Integer deliveryIndentId;
      @SerializedName("created_at")
      @Expose
      private String createdAt;
      @SerializedName("updated_at")
      @Expose
      private String updatedAt;

      public Integer getId() {
          return id;
      }

      public void setId(Integer id) {
          this.id = id;
      }

      public String getIndentId() {
          return indentId;
      }

      public void setIndentId(String indentId) {
          this.indentId = indentId;
      }

      public String getVehicleModel() {
          return vehicleModel;
      }

      public void setVehicleModel(String vehicleModel) {
          this.vehicleModel = vehicleModel;
      }

      public String getColor() {
          return color;
      }

      public void setColor(String color) {
          this.color = color;
      }

      public String getSpareName() {
          return spareName;
      }

      public void setSpareName(String spareName) {
          this.spareName = spareName;
      }

      public String getSerialNo() {
          return serialNo;
      }

      public void setSerialNo(String serialNo) {
          this.serialNo = serialNo;
      }

      public String getQty() {
          return qty;
      }

      public void setQty(String qty) {
          this.qty = qty;
      }

      public String getIndentInvoiceDate() {
          return indentInvoiceDate;
      }

      public void setIndentInvoiceDate(String indentInvoiceDate) {
          this.indentInvoiceDate = indentInvoiceDate;
      }

      public String getManufactureDate() {
          return manufactureDate;
      }

      public void setManufactureDate(String manufactureDate) {
          this.manufactureDate = manufactureDate;
      }

      public String getStatus() {
          return status;
      }

      public void setStatus(String status) {
          this.status = status;
      }

      public String getAcceptance() {
          return acceptance;
      }

      public void setAcceptance(String acceptance) {
          this.acceptance = acceptance;
      }

      public String getInvoiceType() {
          return invoiceType;
      }

      public void setInvoiceType(String invoiceType) {
          this.invoiceType = invoiceType;
      }

      public Integer getUserId() {
          return userId;
      }

      public void setUserId(Integer userId) {
          this.userId = userId;
      }

      public Integer getDeliveredBy() {
          return deliveredBy;
      }

      public void setDeliveredBy(Integer deliveredBy) {
          this.deliveredBy = deliveredBy;
      }

      public Integer getDeliveryIndentId() {
          return deliveryIndentId;
      }

      public void setDeliveryIndentId(Integer deliveryIndentId) {
          this.deliveryIndentId = deliveryIndentId;
      }

      public String getCreatedAt() {
          return createdAt;
      }

      public void setCreatedAt(String createdAt) {
          this.createdAt = createdAt;
      }

      public String getUpdatedAt() {
          return updatedAt;
      }

      public void setUpdatedAt(String updatedAt) {
          this.updatedAt = updatedAt;
      }
  }
}
