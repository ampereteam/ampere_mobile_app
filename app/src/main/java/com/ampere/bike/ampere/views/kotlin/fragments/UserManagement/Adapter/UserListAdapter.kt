package com.ampere.bike.ampere.views.kotlin.fragments.UserManagement.Adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.UserManagement.Pojo.UserList.UsersItem
import com.ampere.vehicles.R

class UserListAdapter (val context: Context,val userArrayList: ArrayList<UsersItem?>?,val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<UserListAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListAdapter.ViewHolder {
        val view = LayoutInflater.from(this.context).inflate(R.layout.adapter_user_list_item,parent,false)
        return  ViewHolder(view)
    }

    override fun getItemCount(): Int {   return this.userArrayList?.size!!
    }

    override fun onBindViewHolder(holder: UserListAdapter.ViewHolder, position: Int) {
        try {
            holder.sino.setText((position+1).toString())
            if(this.userArrayList?.get(position)?.usertype?.userType!=null){
                holder.role.ellipsize = TextUtils.TruncateAt.MARQUEE
                holder.role.isSelected =true
                holder.role.text = this.userArrayList.get(position)?.usertype?.userType
            }else{
                holder.role.text ="-"
            }
            if(this.userArrayList?.get(position)?.name!=null){
                holder.name.ellipsize = TextUtils.TruncateAt.MARQUEE
                holder.name.isSelected =true
                holder.name.text = this.userArrayList.get(position)?.name
            }else{
                holder.name.text ="-"
            }
            if(this.userArrayList?.get(position)?.email!=null){
                holder.email.text = this.userArrayList.get(position)?.email
                holder.email.ellipsize = TextUtils.TruncateAt.MARQUEE
                holder.email.isSelected =true
            }else{
                holder.email.text ="-"
            }
            if(this.userArrayList?.get(position)?.mobile!=null){
                holder.mobile.ellipsize = TextUtils.TruncateAt.MARQUEE
                holder.mobile.text = this.userArrayList.get(position)?.mobile
                holder.mobile.isSelected =true

            }else{
                holder.mobile.text ="-"
            }

            holder.clClick.setOnClickListener {
                try{
                    this.configClickEvent.connectposition(position, this.userArrayList?.size!!)
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val sino :TextView = itemView?.findViewById(R.id.tv_usr_si_no)!!
        val role :TextView = itemView?.findViewById(R.id.tv_usr_role)!!
        val name :TextView = itemView?.findViewById(R.id.tv_usr_name)!!
        val email :TextView = itemView?.findViewById(R.id.tv_usr_emailid)!!
        val mobile :TextView = itemView?.findViewById(R.id.tv_usr_mobileno)!!
        val clClick :ConstraintLayout = itemView?.findViewById(R.id.cl_click)!!
    }
}