package com.ampere.bike.ampere.views.kotlin.model.inventory

class AcceptedStockModel(val success: Boolean, val data: String, val message: String)