package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.serviceReplacementPojos;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServicereplacementPojos {

    @SerializedName("component")
    @Expose
    private String component;
    @SerializedName("oldserial")
    @Expose
    private String oldserial;
    @SerializedName("sparesstock")
    @Expose
    private String sparesstock;
    @SerializedName("complaint_id")
    @Expose
    private int  complaint_id;

    public int getComplaint_id() {
        return complaint_id;
    }

    public void setComplaint_id(int complaint_id) {
        this.complaint_id = complaint_id;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getOldserial() {
        return oldserial;
    }

    public void setOldserial(String oldserial) {
        this.oldserial = oldserial;
    }

    public String getSparesstock() {
        return sparesstock;
    }

    public void setSparesstock(String sparesstock) {
        this.sparesstock = sparesstock;
    }


    public boolean isValid(){
        return  component!=null && oldserial != null && sparesstock!=null ;
    }

}