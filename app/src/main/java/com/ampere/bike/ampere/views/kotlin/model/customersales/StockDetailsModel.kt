package com.ampere.bike.ampere.views.kotlin.model.customersales

data class StockDetailsModel(
        val success: Boolean,
        val data: StockData,
        val message: String

)

class StockData(var sales:ArrayList<Data>)

