package com.ampere.bike.ampere.views.fragment.parthi.Indent.VehileDispatch.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.VehileDispatch.Pojos.VehicleDispatchPojo;
import com.ampere.vehicles.R;

import java.util.ArrayList;

public class VehicleDispatchAdapter extends RecyclerView.Adapter<VehicleDispatchAdapter.MyHolder> {
    Context context;
    ArrayList<VehicleDispatchPojo> vehileDispatchArrayList;
    ConfigClickEvent configposition;
    int i;

    public VehicleDispatchAdapter(Context context, ArrayList<VehicleDispatchPojo> vehileDispatchArrayList, ConfigClickEvent configposition) {
        this.context = context;
        this.vehileDispatchArrayList = vehileDispatchArrayList;
        this.configposition = configposition;
    }

    public void setList(ArrayList<VehicleDispatchPojo> vehileDispatchArrayList) {
        this.vehileDispatchArrayList = vehileDispatchArrayList;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_indent_vehicle_dispt, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, final int position) {
        try{
        if (vehileDispatchArrayList.get(position).isCheck()) {
            holder.mark.setChecked(true);
            i = vehileDispatchArrayList.size();
        } else {
            holder.mark.setChecked(false);
            i = 0;
        }
        holder.sRL_no.setText(String.valueOf(position + 1));
        holder.chassNo.setText(vehileDispatchArrayList.get(position).getChassisNo());
        holder.v_MOdel.setText(vehileDispatchArrayList.get(position).getVehicleModel());
        holder.docket_no.setText(vehileDispatchArrayList.get(position).getDocket());
        holder.indnt_id.setText(vehileDispatchArrayList.get(position).getIndentId());
        holder.mark.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                vehileDispatchArrayList.get(position).setCheck(isChecked);

                if (vehileDispatchArrayList.get(position).isCheck()) {
                    if (vehileDispatchArrayList.size() == i) {
                        configposition.connectposition(position, i);
                    } else {
                        configposition.connectposition(position, ++i);
                    }

                } else {
                    if (i != 0) {
                        configposition.connectposition(position, --i);
                    } else {
                        configposition.connectposition(position, 0);
                    }
                }


            }
        });

    }catch (Exception e){
        e.printStackTrace();
    }
    }

    @Override
    public int getItemCount() {
        return vehileDispatchArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView sRL_no, indnt_id, docket_no, v_MOdel, chassNo;
        CheckBox mark;

        public MyHolder(View itemView) {
            super(itemView);
            sRL_no = itemView.findViewById(R.id.sNO);
            indnt_id = itemView.findViewById(R.id.inDEnt_id);
            docket_no = itemView.findViewById(R.id.docket_No);
            v_MOdel = itemView.findViewById(R.id.vehicle_Mod);
            chassNo = itemView.findViewById(R.id.chass_No);
            mark = itemView.findViewById(R.id.checking);
        }
    }
}
