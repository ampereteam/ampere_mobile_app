package com.ampere.bike.ampere.views.kotlin.fragments.crm

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.*
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.api.RetrofitService
import com.ampere.bike.ampere.model.task.EnquiryModel
import com.ampere.bike.ampere.model.task.TaskUpdateModel
import com.ampere.bike.ampere.model.task.ViewTaskModel
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.*
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MessageUtils.showErrorCodeToast
import com.ampere.bike.ampere.utils.MessageUtils.showFailureToast
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.utils.MyUtils.*
import com.ampere.bike.ampere.views.fragment.calendar.ViewEvent.retrofitService
import com.ampere.bike.ampere.views.fragment.enquiry.Enquiry
import com.ampere.bike.ampere.views.fragment.enquiry.Enquiry.LOCATION_PERMISSION_CODE
import com.ampere.bike.ampere.views.kotlin.fragments.ViewEventDetailed
import com.ampere.bike.ampere.views.kotlin.fragments.crm.sales.SalesKT
import com.ampere.bike.ampere.views.kotlin.fragments.crm.service.ServiceKT
import com.ampere.bike.ampere.views.kotlin.fragments.crm.spares.SparesKT
import com.ampere.bike.ampere.views.kotlin.fragments.model.model_crm.CRMFilterSales
import com.ampere.vehicles.R
import customviews.CustomTextView
import kotlinx.android.synthetic.main.dialog_drop_down.*
import kotlinx.android.synthetic.main.frag_crm.*
import kotlinx.android.synthetic.main.item_crm.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class CRM : Fragment() {

    val animals = ArrayList<java.util.HashMap<String, String>>()

    var name: TextView? = null
    var dialog: Dialog? = null
    val mobile: String = ""
    var mNoData: CustomTextView? = null
    var mSnackView: LinearLayout? = null
    var recyclerView: RecyclerView? = null
    var response1: Response<ViewTaskModel>? = null
    var tabValues = arrayListOf<String>("Sales", "Service", "Spares");
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        (activity as CustomNavigationDuo).disbleNavigationView(getString(R.string.crm))

        return inflater.inflate(R.layout.frag_crm, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mSnackView = view.findViewById(R.id.mSnackView)
        mNoData = view.findViewById(R.id.txt_no_data)
        mNoData?.visibility = View.GONE
        Log.d("tagss", "111");

        recyclerView = view.findViewById(R.id.list_crm)
        recyclerView?.layoutManager = LinearLayoutManager(activity)

        if (response1 != null) {
            var listValues = arrayListOf<String>("Sales", "Service", "Spares")
            //response1?.let { loadData(it) };
            val fragmentAdapter = MyPagerAdapter(activity?.supportFragmentManager, listValues)
            viewpager_main.adapter = fragmentAdapter
            tabs_main.setupWithViewPager(viewpager_main)

            /*  viewpager_main.setCurrentItem(1);
              tabs_main.setupWithViewPager(viewpager_main);*/

            tabs_main.getTabAt(2)?.select()
        }

    }

    private fun loadData(response: Response<ViewTaskModel>) {
        if (response != null) {
            SALES.clear()
            SERVICE.clear()
            SPARES.clear()
            if (response.isSuccessful) {
                Log.d("tagss", "222");

                var taskModel = response.body()
                if (taskModel != null) {
                    if (taskModel.success) {
                        if (taskModel.data != null) {
                            mNoData?.visibility = View.GONE
                            /*for (items: EnquiryModel in taskModel.data.enquiry) {
                                var filterValue = items.enquiryFor
                                if (filterValue.equals(activity?.getString(R.string.sales))) {
                                    var models = CRMFilterSales("" + items.id, items.customerName, items.city, "" + items.mobileNo, items.email, items.leadsource, items.assignedDate,
                                            items.assignedTo, items.enquiryDate, items.enquiryFor, items.enquiryTime, items.assignedPerson, items.message, items.taskStatus, items.task,
                                            items.vehicleModel, "", false)

                                    SALES.add(models)
                                } else if (filterValue.equals(activity?.getString(R.string.service))) {
                                    var models = CRMFilterService("" + items.id, items.customerName, items.city, "" + items.mobileNo, items.email, items.leadsource, items.assignedDate,
                                            items.assignedTo, items.enquiryDate, items.enquiryFor, items.enquiryTime, items.assignedPerson, items.message, items.taskStatus, items.task,
                                            items.vehicleModel, "", false)
                                    SERVICE.add(models)
                                } else {
                                    var models = CRMFilterSpares("" + items.id, items.customerName, items.city, "" + items.mobileNo, items.email, items.leadsource, items.assignedDate,
                                            items.assignedTo, items.enquiryDate, items.enquiryFor, items.enquiryTime, items.assignedPerson, items.message, items.taskStatus, items.task,
                                            items.vehicleModel, "", false)
                                    SPARES.add(models)
                                }


                            }*/
                            Log.d("aldskalsk", "" + SALES.size + "  SPARES " + SPARES.size + " SERVICE " + SERVICE.size)
                            // recyclerView.adapter = ItemCRM(SALES as ArrayList<Object>/*sortingZA()*/, activity!!)
                            val fragmentAdapter = MyPagerAdapter(activity?.supportFragmentManager, tabValues)
                            viewpager_main.adapter = fragmentAdapter
                            tabs_main.setupWithViewPager(viewpager_main)
                        } else {
                            MessageUtils.showSnackBar(activity, mSnackView, taskModel.message.toString())
                        }
                    } else {
                        MessageUtils.showSnackBar(activity, mSnackView, taskModel.message)
                    }
                } else {
                    MessageUtils.showSnackBar(activity, mSnackView, "Not found ")
                }
            } else {
                MessageUtils.setErrorMessage(activity, mSnackView, response.code())
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        var taskHash = HashMap<String, String>()
//        taskHash.put("mobile_no", "9600057939")
        taskHash.put("user_name", LocalSession.getUserInfo(activity, KEY_USER_NAME));
        //var retrofitService: RetrofitService = MyUtils.getRetroService()
        var taskModel = retrofitService.onTaskView(BEARER + LocalSession.getUserInfo(activity, KEY_TOKEN), taskHash)
        dialog = MessageUtils.showDialog(activity)
        dialog?.setOnKeyListener({ dialog, keyCode, event ->
            dialog.dismiss()
            false
        })

        taskModel.enqueue(object : Callback<ViewTaskModel> {
            override fun onResponse(call: Call<ViewTaskModel>, response: Response<ViewTaskModel>) {
                dialog?.dismiss()
                response1 = response
                loadData(response);
            }

            override fun onFailure(call: Call<ViewTaskModel>, t: Throwable) {
                dialog?.dismiss()
                mNoData?.visibility = View.VISIBLE
                MessageUtils.setFailureMessage(activity, mSnackView, t.message)
            }

        })
    }

    private class ItemCRM(val item: MutableList<Object>, val context: FragmentActivity) : RecyclerView.Adapter<ViewHolder>() {

        val items: ArrayList<CRMFilterSales> = item as ArrayList<CRMFilterSales>

        var retrofitService: RetrofitService = MyUtils.getRetroService()
        @RequiresApi(Build.VERSION_CODES.M)
        @SuppressLint("MissingPermission")
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {

            Log.d("items", "" + items.size)
            //Log.d("cahckeDateformat", "" + items[position].enquiry_date);
            holder?.itemTask?.text = items[position].task + "  " + position
            holder?.itemDate?.text = DateUtils.convertDates(items[position].assignedDate, DateUtils.YYYY_MM_DD, "dd MMM yy")
            //holder?.itemTask?.text = items[position].task
            val isCheck = items[position].isChecked
            if (isCheck) {
                holder?.itemStatus.text = "" + items[position].taskStatus
            } else {
                holder?.itemStatus.text = items[position].taskStatus
            }


            val param = holder.itemLayout.layoutParams as LinearLayout.LayoutParams
            param.setMargins(8, 8, 8, 8);
            holder.itemLayout.layoutParams = param
            holder.snack_view.setOnClickListener {

                var bundle = Bundle();
                var fragment = ViewEventDetailed();
/*
                bundle.putString("id", "" + items[position].id)
                bundle.putString("name", "" + items[position].customer_name)
                bundle.putString("city", "" + items[position].city)
                bundle.putString("mobile_no", "" + items[position].mobile_no)
                bundle.putString("email", "" + items[position].email)
                bundle.putString("lead_source", "" + items[position].leadsource)
                bundle.putString("assign_date", "" + items[position].assigned_date)
                bundle.putString("assigned_to", "" + items[position].assigned_to)
                bundle.putString("enquiry_date", "" + items[position].enquiry_date)
                bundle.putString("enquiry_for", "" + items[position].enquiry_for)
                bundle.putString("enquiry_time", "" + items[position].enquiry_time)
                bundle.putString("assigned_person", "" + items[position].assigned_person)
                bundle.putString("message", "" + items[position].message)
                bundle.putString("status", "" + items[position].status)
                bundle.putString("task", "" + items[position].task)
                bundle.putString("sendThrough", "" + items[position].sendThrough)*/

                fragment.arguments = bundle
                MyUtils.passFragmentBackStack(context, fragment)

            }

            holder.itemCall.setOnClickListener(View.OnClickListener {

                Log.d("l,ick", "sdsdsd");
                //mobile = items[position].mobile_no.toString()
                if (isReadLocationAllowed()) {
                    Log.d("l,ick", "222");
                    context.startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:" + items[position].mobileNo.toString())));
                } else {
                    Log.d("l,ick", "333");
                    requestLocationPermission()
                    MessageUtils.showToastMessageLong(context, "Please enable permission in App Settings.")

                }

            })


            holder.itemEmail.setOnClickListener {
                var email = Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, items[position].email);
                email.putExtra(Intent.EXTRA_SUBJECT, "Add Default Subject");
                email.putExtra(Intent.EXTRA_TEXT, "Add Default Message");
                email.setType("message/rfc822");
                context.startActivity(Intent.createChooser(email, "Testing Email Sent"));
            }

            holder.itemStatus.setOnClickListener(View.OnClickListener {
                var statusValues = arrayOf(context.getString(R.string.proposals), context.getString(R.string.negotiation), context.getString(R.string.closure))
                var statusCurrent = items[position].taskStatus
                if (statusCurrent.equals(statusValues[0])) {
                    statusValues = arrayOf(context.getString(R.string.negotiation), context.getString(R.string.closure))
                } else if (statusCurrent.equals(statusValues[1])) {
                    statusValues = arrayOf(context.getString(R.string.closure))
                }
                if (!statusCurrent.equals("Closure")) {
                    var dialog = Dialog(context)
                    dialog.setContentView(R.layout.dialog_drop_down)
                    var dialogTitle = dialog.status
                    dialogTitle.text = "Task Status"
                    var dialogList = dialog.dialog_list

                    dialogList.adapter = ArrayAdapter<String>(context, R.layout.spinnertext, R.id.text1, statusValues) as ListAdapter?

                    dialogList.onItemClickListener = AdapterView.OnItemClickListener { parent, _view, poi, id ->
                        var statusNew = statusValues[poi]

                        dialog.dismiss()
                        notifyDataSetChanged()
                        var updateTaskDetails = HashMap<String, String>()
                        updateTaskDetails.put("user_name", LocalSession.getUserInfo(context, KEY_NAME))
                        updateTaskDetails.put("mobile_no", LocalSession.getUserInfo(context, KEY_MOBILE))
                        updateTaskDetails.put("id", "" + items[position].id)
                        updateTaskDetails.put("task_status", "" + statusNew)


                        var model = retrofitService.onTaskStatusChange(BEARER + LocalSession.getUserInfo(context, KEY_TOKEN), updateTaskDetails)
                        dialog = MessageUtils.showDialog(context)
                        dialog.setOnKeyListener { dialog, _keyCode, event ->
                            dialog.dismiss()
                            false
                        }

                        model.enqueue(object : Callback<TaskUpdateModel> {
                            override fun onResponse(call: Call<TaskUpdateModel>, response: Response<TaskUpdateModel>) {
                                dialog.dismiss()
                                if (response.isSuccessful) {
                                    var taskModel = response.body()
                                    if (taskModel != null) {
                                        Log.d("aslkalsk", "" + taskModel.isStatus)
                                        if (taskModel.isStatus) {
                                            holder.itemStatus.text = statusNew

                                            var enq = EnquiryModel()
                                            //Re_adding values as same

                                            //position = position - 1
                                            /*enq.assigned_date = items[position].assigned_date
                                            enq.assigned_person = items[position].assigned_person
                                            enq.assigned_to = items[position].assigned_to
                                            enq.city = items[position].city
                                            enq.created_at = items[position].created_at
                                            enq.created_time = items[position].created_time
                                            enq.created_by = items[position].created_by
                                            enq.customer_name = items[position].customer_name
                                            enq.email = items[position].email
                                            enq.enquiry_date = items[position].enquiry_date
                                            enq.enquiry_for = items[position].enquiry_for
                                            enq.enquiry_time = items[position].enquiry_time
                                            enq.leadsource = items[position].leadsource
                                            enq.sendThrough = items[position].sendThrough
                                            enq.mobile_no = items[position].mobile_no
                                            enq.id = items[position].id
                                            enq.task = items[position].task

                                            //Update Values
                                            enq.status = statusNew
                                            enq.isChange = true;
                                            items.set(position, enq)*/

                                            notifyDataSetChanged()

                                            MessageUtils.showToastMessage(context, taskModel.message)
                                        } else {
                                            // MessageUtils.showSnackBar(context, holder.snack_view, taskModel.message)
                                            MessageUtils.showToastMessage(context, taskModel.message)
                                        }
                                    } else {
                                        // MessageUtils.showSnackBar(context, holder.snack_view, "Not found ")
                                        MessageUtils.showToastMessage(context, "Not Found")
                                    }
                                } else {
                                    MessageUtils.showToastMessage(context, showErrorCodeToast(response.code()))
                                    //MessageUtils.setErrorMessage(context, holder.snack_view, response.code())
                                }
                            }

                            override fun onFailure(call: Call<TaskUpdateModel>, t: Throwable) {
                                dialog.dismiss()
                                MessageUtils.showToastMessage(context, showFailureToast(t.message))
                                //MessageUtils.setFailureMessage(context, holder.snack_view, t.message)
                            }
                        })
                    }
                    dialog.show()
                } else {
                    MessageUtils.showToastMessage(context, "Already Status Closed")
                }
            })

            //Glide.with(context).load("https://wallpaper-house.com/data/out/9/wallpaper2you_320457.jpg").into(holder?.sampl)

        }


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_crm, parent, false))
        }


        override fun getItemCount(): Int {
            return items.size
        }


        @RequiresApi(Build.VERSION_CODES.M)
        public fun requestLocationPermission() {
            if (ActivityCompat.shouldShowRequestPermissionRationale(context!!, Manifest.permission.READ_CALL_LOG)) {
                //    Toast.makeText(context, "Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show()
                Log.d("l,ick", "44");
            }
            Log.d("l,ick", "55");
            context.requestPermissions(arrayOf(Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE), Enquiry.LOCATION_PERMISSION_CODE)
        }

        public fun isReadLocationAllowed(): Boolean {
            Log.d("l,ick", "666");
            val result = ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_CALL_LOG)
            val result1 = ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_CONTACTS)
            val CALL_PHONE = ContextCompat.checkSelfPermission(context!!, Manifest.permission.CALL_PHONE)
            return if (result1 == PackageManager.PERMISSION_GRANTED && result == PackageManager.PERMISSION_GRANTED && CALL_PHONE == PackageManager.PERMISSION_GRANTED) {
                Log.d("l,ick", "777");
                true
            } else false
        }

    }

    internal open class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val itemName = view.item_crm_name
        val itemDate = view.item_crm_date
        val itemTask = view.item_crm_task
        val itemAction = view.item_crm_action
        val itemStatus = view.item_crm_status
        val itemLayout = view.item_crm
        val itemEmail = view.item_crm_action_email
        val itemCall = view.item_crm_action_call
        val snack_view = view.snack_view
    }

    fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.READ_CALL_LOG)) {
            // Toast.makeText(activity, "Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show()
        }
        requestPermissions(arrayOf(Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_CONTACTS), Enquiry.LOCATION_PERMISSION_CODE)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        //Checking the request code of our request

        if (requestCode == LOCATION_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                requestLocationPermission()
                //Displaying another toast if permission is not granted

            }
        }
    }


    class MyPagerAdapter(fm: FragmentManager?, val values: ArrayList<String>) : FragmentPagerAdapter(fm) {
        // var values = arrayListOf<String>("Sales", "Service", "Spares");

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {

                    SalesKT()
                }
                1 -> {
                    ServiceKT()
                }
                2 -> {
                    SparesKT()
                }
                else -> {
                    return SalesKT()
                }
            }
        }

        override fun getCount(): Int {
            return values.size
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> values[position]
                1 -> values[position]
                2 -> values[position]
                else -> {
                    return values[0]
                }
            }
        }
    }
}


