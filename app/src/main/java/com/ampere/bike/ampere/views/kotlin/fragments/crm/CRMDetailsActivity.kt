package com.ampere.bike.ampere.views.kotlin.fragments.crm

import android.Manifest
import android.annotation.SuppressLint
import android.app.*
import android.content.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.*
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.support.design.widget.*
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityCompat.requestPermissions
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v4.widget.NestedScrollView
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.animation.AnimationUtils
import android.widget.*
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.RetrofitCall
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.api.api_new.UtilsUICallBack
import com.ampere.bike.ampere.interfaces.ListSetter
import com.ampere.bike.ampere.interfaces.RefreshViews
import com.ampere.bike.ampere.interfaces.RemarksAndIDUpdate
import com.ampere.bike.ampere.model.enquiry.edit.ModelServiceComponets
import com.ampere.bike.ampere.model.enquiry.enquiryRemarksUpdate.CallAPIEnquiryTaskIDAndRemarks
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.*
import com.ampere.bike.ampere.utils.*
import com.ampere.bike.ampere.utils.DateUtils.showDateDailog
import com.ampere.bike.ampere.utils.MessageUtils.showErrorCodeToast
import com.ampere.bike.ampere.utils.MessageUtils.showFailureToast
import com.ampere.bike.ampere.utils.MyUtils.checkVersion
import com.ampere.bike.ampere.utils.MyUtils.convertStreamToString
import com.ampere.bike.ampere.views.fragment.Login.pattern
import com.ampere.bike.ampere.views.fragment.enquiry.Enquiry.LOCATION_PERMISSION_CODE
import com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesHome.getPath
import com.ampere.bike.ampere.views.kotlin.fragments.ViewEventDetailed
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMActivity.Companion.refresh
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.AddNextTask.Companion.refresh
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.addNextTask
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.childId
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.dialog
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.enquiryId
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.freeReplacementServerLISt
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.mEmailId
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.mEnqquiry_for
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.mMobileNo
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.mName
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.mSnacView
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.mValues
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.mVehicleModel
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.models
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.newReplacementServerLISt
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.paidReplacementServerLISt
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.serviceReplacementServerLISt
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.task_name
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.FreeReplacementPojos.FreeReplacementReqPojoRESPNSE
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.ServiceEnquiryServerPojo.FreeReplacementServerPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.ServiceEnquiryServerPojo.NewReplacementServerPojo
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.ServiceEnquiryServerPojo.PaidReplacementServerPojo
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.ServiceEnquiryServerPojo.ServiceReplacementServerPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.SpareServicePojo.SpareServiceccmplntRes
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.SpareServiceRepalcementServerPojos.SpareServiceserverPojo
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareStockPojos.SpareStockPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.warrantyComplantPojos.WarrantyComplaintDetailsPojo
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.dynamicAddPojo.ServiceReplecementViewPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.warrantyComplantPojos.DefectitemQtyPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.warrantyComplantPojos.RaiseComplaintRES
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.warrantyComplantPojos.WarrantycomponentsItem
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.DefectItemQtyAddmoreAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.ServiceEnquiryAdapter.FreeServiceReplacementAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.ServiceEnquiryAdapter.NewReplacementAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.ServiceEnquiryAdapter.PaidReplacementAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.ServiceEnquiryAdapter.ServiceReplacementAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.crm.sparesInvoice.SpareInvoice
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.ServiceReplecementAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.SpareHistorCRMAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.SpareItemQtyAddmoreAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.SpareServcieReplacementAdapter.SpareServiceReplacementAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.invoice.InvoiceActivity
import com.ampere.bike.ampere.views.kotlin.fragments.model.model_crm.StatusModel
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.*
import com.ampere.bike.ampere.views.kotlin.item.ItemForTaskUpdate
import com.ampere.bike.ampere.views.kotlin.model.invoice.AddInvoice
import com.ampere.vehicles.R
import com.ampere.vehicles.R.id.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import customviews.CustomButton
import customviews.CustomTextEditView
import customviews.CustomTextView
import kotlinx.android.synthetic.main.dialog_invoice.*
import kotlinx.android.synthetic.main.frag_view_even_details.*
import kotlinx.android.synthetic.main.frag_view_even_details.view.*
import kotlinx.android.synthetic.main.item_dialog.view.*
import kotlinx.android.synthetic.main.item_enquiry_status_list.view.*
import kotlinx.android.synthetic.main.item_spare_list.view.*
import kotlinx.android.synthetic.main.view_task_expandable.*
import modifyviews.MyTextView_Lato_Bold
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.mime.HttpMultipartMode
import org.apache.http.entity.mime.MultipartEntity
import org.apache.http.entity.mime.content.FileBody
import org.apache.http.entity.mime.content.StringBody
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.protocol.BasicHttpContext
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class CRMDetailsActivity : AppCompatActivity(), ListSetter<TaskDetailsModel>, RemarksAndIDUpdate, UtilsUICallBack, ConfigClickEvent {



    companion object {
        var childId: String = ""
        var enquiryId: String = " "
        var user_name: String = ""

        var task_name: String = ""
        var addNextTask: AddNextTask? = null
        var dialog: Dialog? = null
        var mEnqquiry_for: CustomTextView? = null
        var mVehicleModel: CustomTextView? = null

        var mEmailId: CustomTextView? = null
        var mMobileNo: CustomTextView? = null

        var mValues: CustomTextView? = null
        var mName: CustomTextView? = null
        var mStatus: CustomTextView? = null
        var mSnacView: CoordinatorLayout? = null
        var models: TaskDetailsModel? = null

        /**ServiceEnquiry Replacement  Server ArrayList**/
        var newReplacementServerLISt :ArrayList<NewReplacementServerPojo> = ArrayList()
        var serviceReplacementServerLISt :ArrayList<ServiceReplacementServerPojos> = ArrayList()
        var paidReplacementServerLISt :ArrayList<PaidReplacementServerPojo> = ArrayList()
        var freeReplacementServerLISt :ArrayList<FreeReplacementServerPojos> = ArrayList()
        /**  spareService Replacement ServerArrayList**/
        var spareServiceServerList :ArrayList<SpareServiceserverPojo> = ArrayList()
    }
    var taskStatusHint: String = ""
    var enquiryDetailsList: MutableList<EnquiryDetails> = ArrayList()
    var mEnquiryStatusView: LinearLayout? = null
    var mEnquiryStatusList: RecyclerView? = null
    var mNoDataEnquiryStatusList: CustomTextView? = null

    var mSpareListView: LinearLayout? = null
    var mSpareList: RecyclerView? = null
    var mNoDataSpareList: CustomTextView? = null

    var isHidden: Boolean = false
    var year: Int = 0
    var month: Int = 0
    var day: Int = 0
    var statusValues = ArrayList<String>()
    var mTaskDetails: CustomTextView? = null
    var TAKE_PHOTO_CODE = 0
    var TAKE_CAMERA_CODE = 8888

    var outputFileUri: Uri? = null
    var spareFileUri: Uri? = null
    val retrofit = MyUtils.getInstance()
    var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    var bottomDheetDialog: BottomSheetDialog? = null
    internal var bottomSheetBehavior: BottomSheetBehavior<*>? = null

    var mRLServiceComponent: RelativeLayout? = null
    var mLLServiceType: LinearLayout? = null
    var mSpSeviceComponent: Spinner? = null
    var mEdtServiceComponent: CustomTextEditView? = null
    var mSpSeviceType: Spinner? = null
    var mEdtServiceType: CustomTextEditView? = null

    var serviceComponents: List<ModelServiceComponets>? = ArrayList()
    var strServiceComponentId: String? = null
    var mIPLRemarks: TextInputLayout? = null
    val enquiryStatusNewTask: HashMap<String, String>? = HashMap()

    /**
     * global variables for setting   Service warranty Complaint Details
     *
     */
    var recycleviewDefectItem : RecyclerView? =null
    var igtakeImage: ImageView? = null
    var tvImageName: CustomTextView? = null
    var spDefectItem: Spinner? = null
    var edtDefectItem: CustomTextEditView? = null
    var rlDefectItem: RelativeLayout? = null
    var edtDefectQty: CustomTextEditView? = null
    var tvChassisNo: CustomTextEditView? = null
    var edtServiceWarrantyComplaints: CustomTextEditView? = null
    var btnCancel: CustomButton? = null
    var btnSubmit: CustomButton? = null
    var imgbtnAddraisecomplaint:ImageButton ? =null
    var warrantyPojoLis: ArrayList<WarrantyComplaintDetailsPojo>? = ArrayList()
    var warrantyPojoList: ArrayList<WarrantycomponentsItem>? = ArrayList()
    var raiseComplaintRES:RaiseComplaintRES ?= null
    var defectitemList: ArrayList<String>? = ArrayList()
    var defectTtemSTRAdpter : ArrayAdapter<String> ?= null
    var warrantydefectitemList : ArrayList<DefectitemQtyPojos>? = ArrayList()
    /***
     * Adapter  for Service Warranty Complaint addmore Defect item   and quantity
     */
    var warrantydefectAddmoreAdapter : DefectItemQtyAddmoreAdapter? =null

    /**
     * temporary ArrayList Source for Spinner
     *
     */
    var oldsrlNoList: ArrayList<String>? = ArrayList()
    var compontList: ArrayList<String>? = ArrayList()
    var newSerialNoList: ArrayList<String>? = ArrayList()
    var newSerialNoAddMoreList: ArrayList<String>? = ArrayList()
    /**
     * temporary  ArrayAdapter  for spinner
     * set Adapter
     */

    var oldsrlListAdapter: ArrayAdapter<String>? = null
    var compontListAdapter: ArrayAdapter<String>? = null
    var newSerialNoListAdapter: ArrayAdapter<String>? = null
    var newSerialNoaddmoreListAdapter: ArrayAdapter<String>? = null
    /**
     * declaration for the dynamic view using RecyclerView ,Adapter and ArrayList
     *
     */
    var servicereplacePojoArrayList: ArrayList<ServiceReplecementViewPojos> = ArrayList()
    var servicersendPojoArrayList: ArrayList<ServiceReplecementViewPojos> = ArrayList()
    var servicerepalcementAdapter: ServiceReplecementAdapter? = null
    var dialogRaiseComplaint: Dialog? = null
    /**
     * Free Repalcement Required  Modules Variable Objects Declarations
     */
    var dialogFreeReplacementRequired: Dialog? = null
    var ffRrecycleviewDefectItem : RecyclerView? =null
    var ffRigtakeImage: ImageView? = null
    var ffRtvImageName: CustomTextView? = null
    var ffRspSpareItem: Spinner? = null
    var ffRedtSpareItem: CustomTextEditView? = null
    var ffRrlSpareItem: RelativeLayout? = null
    var ffRedtSpareQty: CustomTextEditView? = null
    var ffRtvChassisNo: CustomTextEditView? = null
    var ffRedtServiceWarrantyComplaints: CustomTextEditView? = null
    var ffRbtnCancel: CustomButton? = null
    var ffRbtnSubmit: CustomButton? = null
    var ffRimgbtnAddraisecomplaint:ImageButton ? =null
    /**Response CLass And ArrayLIst**/
    var freeReplacementReqPojoRESPNSE : FreeReplacementReqPojoRESPNSE?=null
    var  ffrSpareSTrList :ArrayList<String> =ArrayList()
    var  ffrSpareSTrAdapter :ArrayAdapter<String> ?=null
    var ffrSpareItemnQtyList : ArrayList<DefectitemQtyPojos>? = ArrayList()
    var spareItemQtyAddmoreAdapter:SpareItemQtyAddmoreAdapter ?= null


    /**
     * send server  updateTask api Interface
     * its gobal variant
     * retrofit Methods
     */

    var callApi: UtilsCallApi? = null
    var map = HashMap<String, Any>()

    /**
     * SparesService Raise Complaint  variable declaration
     */
    var dialogSpareServiceRaiseComplaint: Dialog? = null
    var sPSCigtakeImage: ImageView? = null
    var sPSCtvImageName: CustomTextView? = null
    var sPSCbtnCancel: CustomButton? = null
    var sPSCbtnSubmit: CustomButton? = null
    var sPSCtvSerialno: CustomTextEditView? = null
    var sPSCedtspareServiceComplaints: CustomTextEditView? = null
    var spareServiceFileUri: Uri? = null
    var spareServicecomplaintRes: SpareServiceccmplntRes? = null
    /**
     * Spare History. List
     */
    var spareHistoryadpter :SpareHistorCRMAdapter ?= null
    var recySpareHistory :RecyclerView?=null
    var llSpareHistory :LinearLayout?=null
    var currentStateStatus :Long  =0
    /** FreeServiceReplacement    Adapter and ArrayList **/
    var  freereplacementSTRadapter : ArrayAdapter<String> ?= null
    var  freereplacemntSTRList : ArrayList<String> = ArrayList()



    /*** ServiceEnquiry  Types New Replacement , Service Replacement , Paid Component  and  Free Replacement   ALl Adapter***/
    var newReplacementAdapter: NewReplacementAdapter?=null
    var serviceReplacementAdapter: ServiceReplacementAdapter?=null
    var paidReplacementAdapter: PaidReplacementAdapter?=null
    var freeServiceReplacementAdapter: FreeServiceReplacementAdapter?=null
    /*** SpareService  Replacement Adpater**/
    var spareServiceReplacementAdapter: SpareServiceReplacementAdapter?=null

    @SuppressWarnings("All")
    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try{
            supportActionBar!!.hide()
        }catch (ex:Exception){
            ex.printStackTrace()
        }
        val newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        setContentView(R.layout.frag_view_even_details)
        dialogRaiseComplaint = Dialog(this@CRMDetailsActivity)
        dialogFreeReplacementRequired = Dialog(this@CRMDetailsActivity)
        dialogSpareServiceRaiseComplaint = Dialog(this@CRMDetailsActivity)
        /**
         * intialization the retrofit method
         *
         */
        callApi = UtilsCallApi(this@CRMDetailsActivity, this)

        mEnqquiry_for = findViewById(R.id.ev_details_enqquiry_for)
        llSpareHistory = findViewById(R.id.ll_spare_history)
        recySpareHistory = findViewById(R.id.recy_spare_history)
        mVehicleModel = findViewById(R.id.ev_details_vehicle_model)
        mEmailId = findViewById(R.id.ev_details_mail)
        mMobileNo = findViewById(R.id.ev_details_mobile_no)
        mValues = findViewById(R.id.ev_invoice)
        mName = findViewById(R.id.ev_details_name)
        mStatus = findViewById(R.id.ev_task_details)
        mEnquiryStatusView = findViewById(R.id.enquiry_status_layout)
        mEnquiryStatusList = findViewById(R.id.enquiry_status_list)
        mEnquiryStatusList!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        mNoDataEnquiryStatusList = findViewById(R.id.no_enquiry_status)
        mSpareListView = findViewById(R.id.spare_list_layout)
        mSpareList = findViewById(R.id.spare_list)
        mSpareList!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        recySpareHistory!!.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        mNoDataSpareList = findViewById(R.id.no_spare_status)

        mRLServiceComponent = findViewById(R.id.rl_service_component)
        mLLServiceType = findViewById(R.id.ll_service_type)
        mSpSeviceComponent = findViewById(R.id.sp_service_component)
        mEdtServiceComponent = findViewById(R.id.apnt_edt_service_component)
        mSpSeviceType = findViewById(R.id.sp_service_type)
        mEdtServiceType = findViewById(R.id.apnt_edt_service_type)
        mIPLRemarks = findViewById(R.id.bs_impl_remark)
        mIPLRemarks?.visibility = View.GONE





        /**
         * temporary ArrayList Source for Spinner
         *     oldsrlNoList?.add("SelectThe Old Serial No")
        oldsrlNoList?.add("987564132")
        oldsrlNoList?.add("123564987")
        oldsrlNoList?.add("147582936")
        oldsrlNoList?.add("693528741")
         */


        /**
         * temporary Component array
         *    compontList?.add("Select your Component")
        compontList?.add("BatteryBattery")
        compontList?.add("wire")
        compontList?.add("frame")
        compontList?.add("engine")
         */


        /*
        temporary new serial Number

        newSerialNoList?.add("SelectThe New Serial No")
        newSerialNoList?.add("111987564132")
        newSerialNoList?.add("111123564987")
        newSerialNoList?.add("111147582936")
        newSerialNoList?.add("111693528741")
 */

        /**
         * temporary  ArrayAdapter  for spinner
         * set Adapter
         *     oldsrlListAdapter = ArrayAdapter<String>(this@CRMDetailsActivity,R.layout.spinner_dialog,R.id.text2,oldsrlNoList)
         *
         */


        view_hidden_items.visibility = View.GONE
        if (checkVersion() == 1) {
            add_new_task.setBackgroundResource(R.color.orange)
        }

        ic_back_crm.setOnClickListener {
            onBackPressed()
        }

        mTaskDetails = ev_task_details
        mSnacView = snack_view
        add_new_task.visibility = View.VISIBLE

        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        var bundle = intent
        enquiryId = bundle.getStringExtra("id")
        user_name = bundle.getStringExtra("user_name")
        task_name = bundle.getStringExtra("task_name")
//        popupServiceWarrantyComplaint()//Testing purpose
        Log.d("user_nameuser_name", "" + user_name + "  enquiryId " + enquiryId + " task_name " + task_name)

        tool_title.text = task_name + " Details"
        if(user_name.equals("sales")){
            checkStocknotExisit("Sales")
        }else if(user_name.equals("spares")){
            checkStocknotExisit("Spares")
        }else if(user_name.equals("salesStock")){
            checkStocknotExisit("salesStock")
        }else if(user_name.equals("sparesStock")){
            checkStocknotExisit("sparesStock")
        }
        else{
            Log.d("Username",""+ user_name)
        }
        if (task_name.equals(getString(R.string.service)) || task_name.equals(getString(R.string.spare_service))) {
            mLLServiceType!!.visibility = View.VISIBLE
        } else {
            mLLServiceType!!.visibility = View.GONE
        }
        if (task_name.equals(getString(R.string.sales))) {
            ev_details_spares.visibility = View.INVISIBLE
        } else if (task_name.equals(getString(R.string.service))) {
            ev_details_spares.visibility = View.INVISIBLE
        } else if (task_name.equals(getString(R.string.spares))) {
            ev_details_spares.visibility = View.VISIBLE
        }

        val llBottomSheet: LinearLayout = bottom_sheet!!
        llBottomSheet.visibility = View.VISIBLE
        //var viewss: View = llBottomSheet.parent as View
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet)
        bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        bottomSheetBehavior?.setPeekHeight(340)
        bottomSheetBehavior?.setHideable(true)

        ev_lis_view.layoutManager = LinearLayoutManager(this)
        var miniTask = HashMap<String, String>()
        miniTask.put("enquiry_id", "" + enquiryId)
        miniTask.put("user_name", "" + user_name)
        miniTask.put("userid", "" + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_ID))
        println("Call View  Enquiyr Array Map" + miniTask.toString())
        var taskModel: Call<TaskDetailsModel> = retrofit.onLoadTaskDetails(BEARER + LocalSession.getUserInfo(this, KEY_TOKEN), miniTask)

        //set Task details from enquiry_id
        onTaskLoading(taskModel)

        mSwipeRefreshLayout = findViewById(R.id.swiperefresh)
        mSwipeRefreshLayout?.setRefreshing(false)


        val animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.lay_slide_up)
        val animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.lay_slide_down)


        mName?.setOnClickListener {
            if (isHidden) {
                isHidden = false
                view_hidden_items.startAnimation(animFadeIn)
                view_hidden_items.visibility = View.GONE

                mName?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_user, 0, R.drawable.ic_down, 0)

            } else {
                view_hidden_items.startAnimation(animFadeOut)
                view_hidden_items.visibility = View.VISIBLE
                mName?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_user, 0, R.drawable.ic_up, 0)

                isHidden = true
            }
        }

        ev_lis_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                Log.d("scroll", "scrolling")
                Log.d("scroll", "scrolling"+dy)
                if (dy > 0 && add_new_task.visibility === View.VISIBLE) {

                } else if (dy < 0 && add_new_task.visibility !== View.VISIBLE) {
                    if(taskStatusHint.equals("SS4")|| taskStatusHint.equals("S4")){
                        add_new_task.hide()
                    }else{
                        add_new_task.show()
                    }
                }
            }
        })
        mSpSeviceType?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(adapterVie: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val strServiceType = mSpSeviceType?.selectedItem as String
                Log.d("strServiceType", "" + strServiceType)

                if (strServiceType != null) {
                    mEdtServiceType?.setText("" + strServiceType)
                    // onGetServiceComponents(strServiceType)
                    if (strServiceType.equals("Warranty Service")) {
                        if(models?.data?.enquiry?.enquiryFor.equals(getString(R.string.spare_service))){
                            mRLServiceComponent?.visibility = View.GONE
                            mSpSeviceComponent?.visibility = View.GONE
                            mEdtServiceComponent?.visibility =View.GONE
                        }else{
                        mRLServiceComponent!!.visibility = View.VISIBLE
                        mSpSeviceComponent?.visibility = View.VISIBLE
                        mEdtServiceComponent?.visibility =View.VISIBLE
                        }
                    } else {
                        mRLServiceComponent!!.visibility = View.GONE
                        mSpSeviceComponent?.visibility = View.GONE
                        mEdtServiceComponent?.visibility =View.GONE
                    }
                } else {
                    mEdtServiceType!!.setText(" ")
                }
            }

        }
        mSpSeviceComponent?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(adapterVie: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

                p0?.getChildAt(p2)
                val serviceComponents: LinearLayout = p0!!.selectedView as LinearLayout

                val strServiceComponentName = serviceComponents.text11.text.toString()
                Log.d("strServiceType", " " + strServiceComponentName)
                if (strServiceComponentName != null) {
                    mEdtServiceComponent?.setText("" + strServiceComponentName)
                    strServiceComponentId = serviceComponents.id_s.text.toString()
                    //onGetServiceComponents(strServiceType)
                } else {
                    mEdtServiceComponent!!.setText(" ")

                }
            }

        }

        refresh(object : RefreshViews {
            override fun refresh(models: TaskDetailsModel) {
                //Log.d("taskDetailsModel_models", "" + models)
                mName?.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.customerName)
                ev_details_city.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.city)
                ev_details_vehicle_model.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.vehicleModel)
                ev_details_chassis_no.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.chassisNo)
                mEmailId?.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.email)
                mValues?.text = "  " + getString(R.string.inr) + " " + MyUtils.nullPointerValidation(models.data.enquiry.invoiceValue)
                mMobileNo?.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.mobileNo)
                mEnqquiry_for?.text = MyUtils.nullPointerValidation(models.data.enquiry.enquiryFor)
                ev_details_spares.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.sparesName)
                ev_lead_source.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.leadsource)
                ev_details_task.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.task)
                ev_details_message.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.message)
                ev_task_details.text = "  " + MyUtils.nullPointerValidation(models.data.enquiry.taskStatus)
                CustomTextView.marquee(ev_details_task)
                CustomTextView.marquee(mEmailId)
                try {
                    if (models.data.enquiry.enquiryDate != null) {
                        ev_details_enquiry_date.text = "  " + DateUtils.convertDates(models.data.enquiry.enquiryDate, DateUtils.CURRENT_FORMAT, DateUtils.DD_MM) + "  " + models.data.enquiry.enquiryTime
                    }
                    if (models.data.enquiry.assignedDate != null) {
                        ev_details_enquiry_assigned_date.text = "  " + DateUtils.convertDates(models.data.enquiry.assignedDate, DateUtils.CURRENT_FORMAT + " HH:mm:ss", DateUtils.DD_MM + " hh:mm a")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

        mSwipeRefreshLayout?.setOnRefreshListener {
            onTaskLoading(taskModel)
        }

        bs_task?.setOnClickListener {
            bs_task_spinner?.performClick()
        }

        bs_task_spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                var taskInput = bs_task_spinner?.selectedItem as String
                bs_task?.setText(taskInput)
                if (!taskInput.equals("")) {
                    bs_task?.setText(taskInput)
                } else {
                    bs_task?.setText("")
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }


        /**
         *
         * root cause spinner  adapter Listener
         *
         */
        sp_rootcause.onItemSelectedListener =  object  : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedItem =  sp_rootcause.selectedItem.toString()
                edt_rootcause.setText(selectedItem)
                if(selectedItem == (getString(R.string.select_rootcause))){
                    Log.d("infORoot cause",""+selectedItem)
                }else{
                    Log.d("infORoot cause",""+selectedItem)

                }
            }

        }

        /***FreeReplacement Service Spinner Listener for only Service*
        bs_sp_new_serial_number_addmore_free .onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
        try{
        Log.d("NothingSelected",""+parent?.selectedItem.toString())
        }catch (ex:Exception){
        ex.printStackTrace()
        }
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        try{
        val selectedItem = bs_sp_new_serial_number_addmore_free.selectedItem.toString()
        bs_edt_new_serial_number_addmore_free.setText(selectedItem)
        }catch (ex:Exception){
        ex.printStackTrace()
        }

        }

        }
         */
        bottomSheetBehavior?.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                //bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED)
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        //  Log.d("bottom_sheet", "hidd")
                        add_new_task.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        //  Log.d("bottom_sheet", "exx")
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        // Log.d("bottom_sheet", "coll")
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        // Log.d("bottom_sheet", "dragg")
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                        //  Log.d("bottom_sheet", "sett")
                        add_new_task.visibility = View.GONE
                    }
                }

            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })
        close.setOnClickListener {
            //if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
            //}
        }


        bs_add_event_new?.setOnClickListener {
            val remarksStr = bs_remarks?.text.toString()
            val statusStr = bs_status_spinner.selectedItem.toString()
            val dateStr = bs_date?.text.toString()
            val timeStr = bs_time?.text.toString()
            val taskStr = bs_task?.text.toString()
            var chassisNumberStr = bs_edt_chassis_number.text.toString()


            //if (!MyUtils.isEmpty(this, remarksStr, "EnterThe Remarks")) {
            val enquiuryUpeadeRemarks: ArrayList<CallAPIEnquiryTaskIDAndRemarks> = ArrayList<CallAPIEnquiryTaskIDAndRemarks>()
            //var taskCount: Int = ev_lis_view.adapter.itemCount
            val taskCount: Int = enquiryDetailsList.size
            Log.d("taskCountsdsd", "" + taskCount)
            for (i in 0..taskCount - 1) {
                System.out.print("indendksdjs " + i)
                if( enquiryDetailsList.get(i).remarks!=null) {
                    val remarks: String = enquiryDetailsList.get(i).remarks.toString()
                    val task_id: String = enquiryDetailsList.get(i).id.toString()
                    Log.d("remarkssdsd", "" + i + "  " + remarks + "  " + task_id)
                    val callAPIEnquiryTaskIDAndRemarks = CallAPIEnquiryTaskIDAndRemarks(task_id, remarks)
                    enquiuryUpeadeRemarks.add(callAPIEnquiryTaskIDAndRemarks)
                }

            }

            Log.d("enquiuryUpeadeRemark", "" + enquiuryUpeadeRemarks.size)
            var enquirytaskid = HashMap<String, Any>()
            enquirytaskid.put("enquirytaskid", enquiuryUpeadeRemarks)
            map.put("enquirytaskids", enquirytaskid)
            map.put("enquiry_id", enquiryId)
            map.put("enquiry_status", enquiryStatusNewTask?.get(statusStr)!!)
            map.put("username", LocalSession.getUserInfo(this@CRMDetailsActivity, LocalSession.KEY_ID))
            //map.put("enquirytaskid", childId)

            Log.d(" statusHint ","StatusHint "+taskStr+"\t"+taskStatusHint)
            var isLoadAPI: Boolean = false
            if (taskStatusHint.equals("SS4")||taskStatusHint.equals("S4")) {
                if (!taskStr.equals("No Task List Found")) {
                    Log.d("outputFileUri", "" + outputFileUri)
                    if (!MyUtils.isEmpty(this@CRMDetailsActivity, taskStr, "Select Next Task")) {
                        if (!MyUtils.isEmpty(this@CRMDetailsActivity, remarksStr, "Remarks can't be empty")) {
//                            Toast.makeText(this, "Service Type" + mEdtServiceType?.text.toString(), Toast.LENGTH_SHORT).show()
                            Log.d("Service Type ", "" + task_name + "Task Name" + map.toString())
                            map.put("remarks", remarksStr)
                            if (task_name.equals(getString(R.string.sales))) {
                                map.put("nexttask", taskStr)
//                                map.put("remarks", remarksStr)
//                                adddynamcsparestoMap(map)
                                isLoadAPI = true

                            } else if (task_name.equals(getString(R.string.spares))) {
                                map.put("nexttask", taskStr)
//                                map.put("remarks", remarksStr)
//                                adddynamcsparestoMap(map)
                                isLoadAPI = true
                            } else {
                                Log.d("tasknaenm", "" + task_name)
                                if (task_name.equals(getString(R.string.service))) {
                                    if (!MyUtils.isEmpty(this@CRMDetailsActivity, bs_edt_service_value.text.toString(), "Service Can't be empty")) {
                                        if (!MyUtils.isEmpty(this@CRMDetailsActivity, bs_edt_spare_value.text.toString(), "Spare Can't be empty")) {
                                            /* if(!MyUtils.isEmpty(this@CRMDetailsActivity,bs_edt_component_name.text.toString(),"Component  Can't be empty"))
                                        {
                                            if(!MyUtils.isEmpty(this@CRMDetailsActivity,bs_edt_old_serial_number.text.toString(),"Old serial Number Can't  be empty")){
                                                if(!MyUtils.isEmpty(this@CRMDetailsActivity,bs_sp_new_serial_number.selectedItem.toString()," New Serial Number  can't be empty")){
                                                   if(bs_sp_new_serial_number.selectedItem.toString().equals("Select New Serial No")){
                                                    Log.d("spinner Value",""+bs_sp_new_serial_number.selectedItem.toString())
                                                    }else{
                                                    if(bs_edt_component_name.text.toString().isEmpty() ||
                                                            bs_edt_component_name.text == null
                                                            || bs_edt_component_name.text.length ==0){
                                                        var toast = Toast.makeText(this,"Component Can't be Empty",Toast.LENGTH_SHORT)
                                                        toast.setGravity(Gravity.CENTER,0,0)
                                                        toast.show()
                                                    }else{
                                                        if(bs_edt_old_serial_number.text.toString().isEmpty() ||
                                                                bs_edt_old_serial_number.text.length ==0
                                                                || bs_edt_old_serial_number.text == null ){
                                                            var toast = Toast.makeText(this,"Old serial Number Can't be Empty",Toast.LENGTH_SHORT)
                                                            toast.setGravity(Gravity.CENTER,0,0)
                                                            toast.show()

                                                        }else{
                                                            if(bs_sp_new_serial_number.selectedItem.toString() == null ||
                                                                    bs_sp_new_serial_number.selectedItem.toString().isEmpty() ||
                                                                    bs_sp_new_serial_number.selectedItem.toString().length ==0
                                                                    ||  bs_sp_new_serial_number.selectedItem.toString().equals("Select New Serial No")
                                                            ){
                                                                var toast = Toast.makeText(this,"New Serial Number is  Not Valid Data",Toast.LENGTH_SHORT)
                                                                toast.setGravity(Gravity.CENTER,0,0)
                                                                toast.show()
                                                            }else{*/
                                            map.put("nexttask", taskStr)
                                            map["invoice_value"]= bs_edt_service_value.text.toString()
                                            map["spare_value"]= bs_edt_spare_value.text.toString()
//                                        map.put("remarks", remarksStr)
                                            adddynamcsparestoMap(map)
                                            isLoadAPI = true/*
                                                            }
                                                        }

                                                    }

                                                            }
                                                }

                                            }
                                        }*/
                                        }
                                    }
                                }else{
                                    try{
                                        rl_rootcause.visibility =View.VISIBLE
                                        if(sp_rootcause.selectedItem == null ||  sp_rootcause.selectedItem.toString().equals(getString(R.string.select_rootcause))){
                                            Log.d("sp_rootcause",""+sp_rootcause.selectedItem.toString())
                                        }else{
                                            map.put("rootcause",sp_rootcause.selectedItem.toString())
                                        }
                                        map.put("nexttask", taskStr)
                                        addSpareSErvice(map)
                                        isLoadAPI =true
                                    }catch (ex:Exception){
                                        ex.printStackTrace()
                                    }
                                }
                            }
                        }
                    }
                    /*addNextTask = AddNextTask(this@CRMDetailsActivity, dateStr, timeStr, taskStr, remarksStr, statusStr, "" + outputFileUri, chassisNumberStr, oldSerialNo, newSerialNo)
                    addNextTask?.execute()*/
                } else {
                    MessageUtils.showToastMessage(this@CRMDetailsActivity, "Task Is Empty. So Contact your Admin.")
                }
            } else {
                Log.d("TasKNAME",""+taskStatusHint+"\n"+ task_name)
                if (!MyUtils.isEmpty(this@CRMDetailsActivity, taskStr, "Select Next Task") && !taskStr.equals(getString(R.string.select_task))) {
                    if (!MyUtils.isEmpty(this@CRMDetailsActivity, dateStr, "Select Next Date")) {
                        if (!MyUtils.isEmpty(this@CRMDetailsActivity, timeStr, "Select Next Time")) {
                            if (!taskStr.equals("No Task List Found")) {
                                Log.d("outputFileUri", "" + outputFileUri)

                                map.put("nexttask", taskStr)
                                if(task_name.equals(getString(R.string.service))){
                                    if (mEdtServiceType?.getText() != null) {
                                        if (mEdtServiceType?.getText().toString().equals("Warranty Service")) {
                                            map.put("service_type", "Warranty Service")

                                        } else {
                                            map.put("service_type", "Paid Service")
                                        }
                                        if (mEdtServiceComponent?.text == null) {

                                        } else {
                                            map.put("defectitem", mEdtServiceComponent?.text.toString())
                                        }
                                        map.put("enquiry_status", enquiryStatusNewTask?.get(statusStr)!!)
                                    }
                                }else{
                                    map.put("service_type", mSpSeviceType?.selectedItem.toString())
                                }
                                map.put("next_activity_date", dateStr + " " + timeStr)
                                map.put("remarks", remarksStr)
                                Log.d("mapsdsd", "" + map)
                                isLoadAPI = true
                                /*var date = next_activity_date
                                onAddNextTask(date, date, enquiry_status, remarks, userName, enquiry_id, enquirytaskid, nexttask, nexttask)*/
                                /* addNextTask = AddNextTask(this@CRMDetailsActivity, dateStr, timeStr, taskStr, remarksStr, statusStr, "" + outputFileUri, chassisNumberStr, oldSerialNo, newSerialNo)
                                    addNextTask?.execute()
*/
                            } else {
                                MessageUtils.showToastMessage(this@CRMDetailsActivity, "Task Is Empty. So Contact your Admin.")
                            }
                        }
                    }
                }
            }

            Log.d("mapsdsd_after", "" + map.toString())


            if (isLoadAPI) {
                var taskDetailsModel: Call<TaskDetailsModel> = MyUtils.getInstance().onUpdateEnquiry(BEARER + LocalSession.getUserInfo(this, KEY_TOKEN), map)

                var dialog = MessageUtils.showDialog(this@CRMDetailsActivity)

                taskDetailsModel.enqueue(object : Callback<TaskDetailsModel> {
                    override fun onFailure(call: Call<TaskDetailsModel>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.setFailureKotlin(this@CRMDetailsActivity, snack_view, MessageUtils.showFailureToast(t.message))
                    }

                    override fun onResponse(call: Call<TaskDetailsModel>, response: Response<TaskDetailsModel>) {
                        MessageUtils.dismissDialog(dialog)
                        if (response.isSuccessful) {
                            models = response.body()
                            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
                            Log.d("tasak name ", "" + task_name)
                            if (task_name.equals(getString(R.string.sales))) {
                                Log.d("tasak name ", "" + models?.data?.enquiry?.taskStatus)
//                                if (statusStr.equals(getString(R.string.closure))) {
                                if (models?.data?.enquiry?.taskStatus.equals("SS4")) {
                                    if (taskStr.toLowerCase().equals("won".toLowerCase())) {
                                        Log.d("Info", "Submit Function")
                                        //MyUtils.passFragmentBackStack(this@CRMDetailsActivity, InvoiceFragment())
                                        var intent = Intent(this@CRMDetailsActivity, InvoiceActivity::class.java)
                                        intent.putExtra("id", "" + enquiryId)
                                        intent.putExtra("user_name", "" + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_USER_NAME))
                                        intent.putExtra("task_name", "" + statusStr)
                                        startActivity(intent)
                                        overridePendingTransition(0, 0)
                                        finish()

                                    }
                                } else {
                                    Log.d("tasak name ", "" + task_name)
                                    viewRefresh(models)

                                }
                            } else {
                                Log.d("tasak name ", "" + task_name)
//                                Toast.makeText(this@CRMDetailsActivity, "Its toast!"+ task_name, Toast.LENGTH_SHORT).show()
                                if (task_name.equals("Service")) {
                                    /*Toast.makeText(this@CRMDetailsActivity,""+mEdtServiceType?.getText().toString(),Toast.LENGTH_SHORT)
                                            .show()
                                            .setGravity(Gravity.CENTER,0,0)*/
                                    Log.d("info ", "" + mEdtServiceType?.getText().toString())
                                    if (mEdtServiceType?.getText().toString().equals(getString(R.string.warrenty_service)) &&
                                            bs_task?.getText().toString().equals("Raise Complaint")
                                            && bs_task_spinner?.selectedItem.toString().equals("Raise Complaint")
                                    ) {
                                        //popupOnsubmit()
                                        popupServiceWarrantyComplaint()
                                    }
                                    else   if (mEdtServiceType?.getText().toString().equals(getString(R.string.warrenty_service)) &&
                                            bs_task?.getText().toString().equals("Free Replacement Required")
                                            && bs_task_spinner?.selectedItem.toString().equals("Free Replacement Required")
                                    ){

                                        popupFreeReplacementRequired()
                                    }
                                    else {
//                                        popupServiceWarrantyComplaint()//Testing Purpose
                                        viewRefresh(models)
//                                        add_new_task.visibility = View.GONE
                                    }

                                } else {
                                    if (task_name.equals(getString(R.string.spares)) && models?.data?.enquiry?.taskStatus.equals("SS4")) {

                                        var intent = Intent(this@CRMDetailsActivity, SpareInvoice::class.java)
                                        intent.putExtra("id", "" + enquiryId)
                                        intent.putExtra("user_name", "" + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_USER_NAME))
                                        intent.putExtra("task_name", "" + statusStr)
                                        startActivity(intent)
                                        overridePendingTransition(0, 0)
                                        finish()

                                    } else {

                                        /**
                                         * SpareService  Raise Complaint View Design
                                         */
                                        if(task_name.equals(getString(R.string.spare_service)) && models?.data?.enquiry?.taskStatus.equals("S2")){
                                            popupSpareServiceRaiseComplaint()
                                        }else{
                                            viewRefresh(models)
                                        }
                                    }


                                }
                            }

                        } else {
                            MessageUtils.setFailureKotlin(this@CRMDetailsActivity, snack_view, MessageUtils.showErrorCodeToast(response?.code()))
                        }
                    }
                })
            } else {
                MessageUtils.setFailureKotlin(this@CRMDetailsActivity, snack_view, "Something went wrong")
            }
        }


        bs_status_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                try {

                    val localStr: String = bs_status_spinner?.selectedItem as String

                    //val taskInput: String = "58"
                    // Log.d("taskInput", "" + localStr)
                    if (models != null) {
                        for (enquiry_list in models!!.data.enquirystatuslist) {
                            var status = enquiry_list.status
                            //Log.d("statussdsd", "" + status)
                            Log.d("status",""+taskStatusHint)
                            if (localStr.equals(status)) {
                                taskStatusHint = enquiry_list.statusHint
                                Log.d("status Equals",""+taskStatusHint)
                            }
                        }
                        Log.d("status",""+localStr)
                    }

                    //Log.d("taskInput_after", "" + localStr + " taskInput " + taskInput)

                    if (taskStatusHint != null && !taskStatusHint.equals("")) {
                        bs_status?.setText(taskStatusHint)
                        var statusCOde=""
                        for(spare in models?.data?.enquiryStatusList!!){
                            Log.d("Update Status",""+spare.status)
                            if(spare.status.equals(localStr)){
                                statusCOde=spare.status
                                Log.d("status code",""+statusCOde)
                            }
                        }
                        Log.d("Status",""+localStr+"\n"+ taskStatusHint)
                        if (taskStatusHint.equals(getString(R.string.statusHint)) || taskStatusHint.equals("S4") ) {
                            mIPLRemarks?.visibility = View.VISIBLE


                            var enquiry = mEnqquiry_for?.text.toString()
                            Log.d("enquirysds", "" + enquiry)
                            if (enquiry.equals("Service")) {
                                rl_rootcause.visibility  =View.VISIBLE
                                bs_open_camera_lay.visibility = View.VISIBLE
                                bs_ll_spare_service_replacement.visibility = View.GONE
                            }else{
                                if(enquiry.equals("SparesService")){
                                    rl_rootcause.visibility  =View.VISIBLE
                                    bs_open_camera_lay.visibility = View.GONE
                                    bs_ll_spare_service_replacement.visibility = View.VISIBLE
                                }
                            }
                            layout_action_date.visibility = View.GONE
                        } else {
//                            mIPLRemarks?.visibility = View.GONE
                            mIPLRemarks?.visibility = View.VISIBLE
                            rl_rootcause.visibility = View.GONE
                            bs_ll_spare_service_replacement.visibility = View.GONE
                            bs_open_camera_lay.visibility = View.GONE
                            layout_action_date.visibility = View.VISIBLE
                        }
                    } else {
                        bs_open_camera_lay.visibility = View.GONE
                        bs_status?.setText("")
                        rl_rootcause.visibility = View.GONE
                        bs_ll_spare_service_replacement.visibility = View.GONE
                        layout_action_date.visibility = View.VISIBLE
                    }


                    /**
                     * lets changes in closer status for new  requirement
                     *
                     * new changes all in one functions
                     * set the all data to spinners and text as Possible
                     */

                    if (taskStatusHint.equals(getString(R.string.statusHint)) || taskStatusHint.equals("S4") ) {
                        /*    println("info Spinner Status" + localStr)
                            bs_sp_new_serial_number.adapter = newSerialNoListAdapter
    //                        println("info Spinner Status"+bs_sp_new_serial_number.selectedItem.toString())
                            bs_sp_new_serial_number.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                override fun onNothingSelected(parent: AdapterView<*>?) {

                                }
                                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                                    bs_edt_new_serial_number?.setText(bs_sp_new_serial_number.selectedItem.toString())
                                }

                            }*/

                    } else {
                        Log.d("Enquiry Satus", "" + localStr)
                    }


                    // Log.d("when_comes", "sdsdsd no " + taskInput + " hid " + bottomSheetBehavior?.isHideable + "  state " + bottomSheetBehavior?.state)
                    //if (bottomSheetBehavior?.state!!.equals(BottomSheetBehavior.STATE_EXPANDED)) {
                    val statusValues = ArrayList<String>()
                    //dialog = MessageUtils.showDialog(this)
                    val map = HashMap<String, String>()
                    map.put("enquiry_id", enquiryId)
                    //map.put("status", CRMDetailsActivity.mTaskDetails?.text.toString().trim())
                    //map.put("status", "New Enquiry")
                    map.put("status", taskStatusHint)
                    var retrofit = MyUtils.getInstance()
                    var dialog = MessageUtils.showDialog(this@CRMDetailsActivity)

                    val statusModel: Call<StatusModel> = retrofit.onGetStatus(BEARER + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_TOKEN), map)
                    MessageUtils.dismissDialog(dialog)
                    dialog?.setOnKeyListener { dialogInterface, _, keyEvent ->
                        dialogInterface.dismiss()
                        statusModel.cancel()
                        false
                    }

                    statusModel.enqueue(object : Callback<StatusModel> {
                        override fun onResponse(call: Call<StatusModel>?, response: Response<StatusModel>?) {
                            try {
                                MessageUtils.dismissDialog(dialog)
                                if (response?.isSuccessful!!) {
                                    var models: StatusModel = response.body()!!
                                    if (models != null) {
                                        if (models.success) {
                                            //  var newTask = ArrayList<NewTaskStatusUpdate>()
                                            for (statuass in models.data) {

                                                statusValues.add(statuass.task)

                                                /*  var addTaskId = NewTaskStatusUpdate(statuass.task, statuass.id, statuass.status, statuass.taskType)
                                                  newTask.add(addTaskId)*/
                                            }
                                            bs_task_spinner.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, android.R.layout.simple_list_item_1, android.R.id.text1, statusValues)

                                        } else {

                                            statusValues.add("" + models.message)
                                            bs_task_spinner.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, android.R.layout.simple_list_item_1, android.R.id.text1, statusValues)
                                            if (bottomSheetBehavior?.state!!.equals(BottomSheetBehavior.STATE_EXPANDED)) {
                                                MessageUtils.showToastMessage(this@CRMDetailsActivity, models.message)
                                            }
                                        }
                                    } else {
                                        if (bottomSheetBehavior?.state!!.equals(BottomSheetBehavior.STATE_EXPANDED)) {
                                            MessageUtils.showToastMessage(this@CRMDetailsActivity, showFailureToast(getString(R.string.api_response_not_found)))
                                        }
                                    }
                                } else {
                                    if (bottomSheetBehavior?.state!!.equals(BottomSheetBehavior.STATE_EXPANDED)) {
                                        MessageUtils.showToastMessage(this@CRMDetailsActivity, showErrorCodeToast(response.code()))
                                    }
                                }
                                Log.d("taskStatusHint",""+taskStatusHint)
                                if(taskStatusHint.equals("SS4") || taskStatusHint.equals("S4")){
                                    add_new_task.visibility = GONE
                                }else{

                                }

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }

                        override fun onFailure(call: Call<StatusModel>?, t: Throwable?) {
                            MessageUtils.dismissDialog(dialog)
                            if (bottomSheetBehavior?.state!!.equals(BottomSheetBehavior.STATE_EXPANDED)) {
                                MessageUtils.showToastMessage(this@CRMDetailsActivity, t?.message)
                            }
                        }
                    })
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }

        bs_add_event_cancel?.setOnClickListener {
            /*if (bottomDheetDialog != null) {
                bottomDheetDialog?.dismiss()*/
            // add_new_task.visibility = View.VISIBLE
            bs_date?.setText("")
            bs_time?.setText("")
            //bs_task?.setText("")
            bs_remarks?.setText("")
            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)

            //}
        }

        bs_date?.setOnClickListener {
            var datesStr = showDateDailog()
            Log.d("datesStr", "" + datesStr)
            //bs_date?.setText(datesStr)
        }

        bs_time?.setOnClickListener {
            val calendar = Calendar.getInstance()

            val d = TimePickerDialog(this, getTimeFromPicker(this, bs_time!!), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false)
            d.window.attributes.windowAnimations = R.style.grow
            d.show()

        }

        snack_view.setOnClickListener {
            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
        }

        take_image.setOnClickListener {

            if (isReadLocationAllowed()) {

                //MessageUtils.showToastMessage(this@CRMDetailsActivity, "Open Camera")
                var dir =/* "" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +*/ "/sdcard/Ampere_Service/"
                var newdir = File(dir)
                newdir.mkdirs()
                var file = dir + DateUtils.getCurrentDate(DateUtils.DATE_DDMMYYYYHHMMAP) + ".jpg"
                var newfile = File(file)
                try {
                    newfile.createNewFile()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                outputFileUri = Uri.fromFile(newfile)
                Log.d("outputFileUriClicke", "" + outputFileUri)

                var cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
                startActivityForResult(cameraIntent, TAKE_PHOTO_CODE)
            } else {
                requestLocationPermission()
            }
        }

        add_new_task.setOnClickListener {
            //snack_view.setBackgroundColor(resources.getServiceType(R.serviceType.white_un_select))
            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_EXPANDED)
        }
        /**Its no need to For New  Paid Repacement**/
        /*  img_addmore.setOnClickListener {
              img_addmore.visibility = View.GONE
              tv_addmore.visibility = View.GONE
              ll_addmore.visibility = View.VISIBLE
              img_remvemore.visibility = View.VISIBLE
              tv_remvemore.visibility = View.VISIBLE
              dynamicAddingSpareFn()
          }
          img_remvemore.setOnClickListener {
              servicereplacePojoArrayList.clear()
              servicerepalcementAdapter?.notifyDataSetChanged()
              img_addmore.visibility = View.VISIBLE
              tv_addmore.visibility = View.VISIBLE
              ll_addmore.visibility = View.GONE
              img_remvemore.visibility = View.GONE
              tv_remvemore.visibility = View.GONE
          }*/
    }

    private fun addSpareSErvice(map: HashMap<String, Any>) {

        try{
            val spareService :HashMap<String,Any> = HashMap()
            if(models?.data?.spareservicereplaementArrayList?.size!=0){
                if(spareServiceServerList.size !=0){
                    spareService.clear()
                    spareService["spares_service_replacement"] = spareServiceServerList
                }
                map["spares_service_replacement"] = spareService

            }else{
                Log.d("spares_service_replacementArrayList","Both isEmpty")
            }


        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun checkStocknotExisit( typE : String) {
        try{
            Log.d("Tpye",""+typE)
            val dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_stock_check)
            val title:TextView = dialog.findViewById(R.id.cmn_title)
            var msg:TextView = dialog.findViewById(R.id.cmn_msg)
            val cancel:TextView = dialog.findViewById(R.id.cancel)
            val raiseIntent:TextView = dialog.findViewById(R.id.gotoSt)
            val raiseinvoice:TextView = dialog.findViewById(R.id.raiseinvoice)
            dialog.setCancelable(false)
            dialog.show()
            if (typE.equals("salesStock")){
                title.text =this.getString(R.string.raise_invoice)
                msg.text=""
                raiseIntent.visibility = View.GONE
            }else if (typE.equals("sparesStock")){
                title.text =this.getString(R.string.raise_invoice)
                msg.text=""
                raiseIntent.visibility = View.GONE
            }
            else{
                title.text = this.getString(R.string.raise_intent_title)
                msg.text = this.getString(R.string.raise_intent_message)
                raiseIntent.visibility = View.VISIBLE
            }

//            raiseIntent.text = this.getString(R.string.raise_intent)
//            raiseinvoice.text = this.getString(R.string.raise_invoice)
            raiseIntent.setOnClickListener {
                dialog.dismiss()
                var intent = Intent(this@CRMDetailsActivity, CustomNavigationDuo::class.java)
                intent.putExtra("id", "" + enquiryId)
                intent.putExtra("user_name", "" + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_USER_NAME))
                intent.putExtra("redirect", "indent")
                startActivity(intent)
                overridePendingTransition(0, 0)
                finish()
                //MyUtils.passFragmentBackStack(this@InvoiceActivity, Indent())
            }
            raiseinvoice.setOnClickListener {
                dialog.dismiss()
                val intent :Intent
                Log.d("Type Of Sales",""+typE)
                if(typE.equals("Sales") || typE.equals("salesStock")){
                    intent = Intent(this@CRMDetailsActivity, InvoiceActivity::class.java)
                    intent.putExtra("id", "" + enquiryId)
                    intent.putExtra("user_name", "" + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_USER_NAME))
                    intent.putExtra("task_name", "Invoice")
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                    finish()
                }else if(typE.equals("Spares") ||typE.equals("sparesStock")){
                    intent = Intent(this@CRMDetailsActivity, SpareInvoice::class.java)
                    intent.putExtra("id", "" + enquiryId)
                    intent.putExtra("user_name", "" + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_USER_NAME))
                    intent.putExtra("task_name", "Invoice")
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                    finish()
                }

            }
            cancel.setOnClickListener {
                dialog.dismiss()

            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
    private fun checkStocknotExisi() {
        try{
            val dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_internet)
            val title = dialog.findViewById(R.id.cmn_title) as CustomTextView
            var msg = dialog.findViewById(R.id.cmn_msg) as CustomTextView
            val cancel = dialog.findViewById(R.id.cancel) as CustomTextView
            val done = dialog.findViewById(R.id.gotoSt) as CustomTextView
            dialog.setCancelable(false)
            dialog.show()
            title.text = this.getString(R.string.raise_intent_title)
            msg.text = this.getString(R.string.raise_intent_message)
            done.text = this.getString(R.string.raise_intent)
            done.setOnClickListener {
                dialog.dismiss()
                var intent = Intent(this@CRMDetailsActivity, InvoiceActivity::class.java)
                intent.putExtra("id", "" + enquiryId)
                intent.putExtra("user_name", "" + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_USER_NAME))
                intent.putExtra("task_name", "Invoice")
                startActivity(intent)
                overridePendingTransition(0, 0)
                finish()
                //MyUtils.passFragmentBackStack(this@InvoiceActivity, Indent())
            }

            cancel.setOnClickListener {
                dialog.dismiss()

            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }


    private fun popupSpareServiceRaiseComplaint() {
        try{
            val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(R.layout.dialog_spare_service_complaints, null)
            dialogSpareServiceRaiseComplaint?.setContentView(view)
            dialogSpareServiceRaiseComplaint?.setCancelable(true)
            dialogSpareServiceRaiseComplaint?.show()
            /**get the XML Id**/
            sPSCbtnCancel = view?.findViewById(R.id.cb_cancel_spare_service)
            sPSCbtnSubmit = view?.findViewById(R.id.cb_submit_spare_service)

            sPSCtvSerialno = view?.findViewById(R.id.edt_spare_service_complint_chassis)
            sPSCtvImageName = view?.findViewById(R.id.tv_spare_service_imge_name)

            /** NOt Necessary for SpareService. May Be its Usable
            ffRimgbtnAddraisecomplaint = view?.findViewById(R.id.imgbtn_spare_service_complint_addmore)
            ffRspSpareItem = view?.findViewById(R.id.sp_spare_service_complint_spare_item)
            ffRrlSpareItem = view?.findViewById(R.id.rl_spare_service_complint_spare_item)
            ffRedtSpareItem = view?.findViewById(R.id.edt_spare_service_complint_spare_item)
            ffRedtSpareQty = view?.findViewById(R.id.edt_spare_service_complint_spare_qty)
            ffRrecycleviewDefectItem = view?.findViewById(R.id.recyle_spare_service_complint_)
             ***/

            sPSCedtspareServiceComplaints = view?.findViewById(R.id.edt_spare_service_complints)

            sPSCigtakeImage = view?.findViewById(R.id.spare_service_ig_take_image)
            getandsespareService()

            sPSCbtnCancel?.setOnClickListener {
                try{
                    if(dialogSpareServiceRaiseComplaint?.isShowing()!!){
                        dialogSpareServiceRaiseComplaint?.dismiss()
                    }
//                   getandsetFreeRpcmntReq()
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

            sPSCbtnSubmit?.setOnClickListener {

                if (sPSCtvSerialno?.getText().toString().isEmpty() || sPSCtvSerialno?.getText().toString() == null || sPSCtvSerialno?.getText().toString().length == 0) {
//                   ffRtvChassisNo?.setError("require field")
                    MessageUtils.showToastMessageLong(this@CRMDetailsActivity,getString(R.string.stocknoavl))
                } else {
                    /*if (ffRspSpareItem?.selectedItem.toString() == null || ffRspSpareItem?.selectedItem.toString().isEmpty()
                            || ffRspSpareItem?.selectedItem.toString().equals(" Select Defect Item ") || ffRspSpareItem?.selectedItem.toString().length == 0
                    ) {
//                       ffRedtSpareItem?.setError(" require field ")
                        MessageUtils.showToastMessage(this,"Spare QTY Can't be Empty ")
                    } else {
                        if (ffRedtSpareQty?.text.toString().isEmpty() ||
                                ffRedtSpareQty?.text.toString() == null ||
                                ffRedtSpareQty?.text.toString().length == 0 ||
                                Integer.parseInt(ffRedtSpareQty?.text.toString()) < 1
                        ) {
                            MessageUtils.showToastMessage(this,"Spare QTY Can't be Empty ")
                        } else {*/
                    if (sPSCedtspareServiceComplaints?.text.toString().isEmpty() || sPSCedtspareServiceComplaints?.text.toString().length == 0
                            || sPSCedtspareServiceComplaints?.text.toString() == null) {
//                               ffRedtServiceWarrantyComplaints?.setError("Required Field ")
                        MessageUtils.showToastMessage(this,"Complaint  Can't be Empty ")
                    } else {
                        if (spareServiceFileUri== null) {
                            MessageUtils.showToastMessage(this@CRMDetailsActivity, "please select  Image")
                        } else {
//                                dialog = MessageUtils.showDialog(this@CRMDetailsActivity)
                            sendToSpareServiceComplaint(sPSCtvSerialno?.text.toString(),
                                    sPSCedtspareServiceComplaints?.text.toString())
                        }

                    }
                    /*         }
                         }*/
                }


            }

            sPSCigtakeImage?.setOnClickListener {
                if (isReadLocationAllowed()) {

                    //MessageUtils.showToastMessage(this@CRMDetailsActivity, "Open Camera")
                    val dir =/* "" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +*/ "/sdcard/Android/data/com.ampere.bike.ampere/files/Ampere_Service/ServiceComplaints/"
                    val newdir = File(dir)
                    newdir.mkdirs()
                    val file = dir + DateUtils.getCurrentDate(DateUtils.DATE_DDMMYYYYHHMMAP) + ".jpg"
                    val newfile = File(file)
                    try {
                        newfile.createNewFile()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                    spareServiceFileUri = Uri.fromFile(newfile)
                    Log.d("outputFileUriClicke", "" + spareServiceFileUri)

                    /* var cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                     cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
                     startActivityForResult(cameraIntent, TAKE_CAMERA_CODE)*/
                    launchCamera(12177)
                } else {
                    requestLocationPermission()
                }
            }
        }catch (ex:Exception) {
            ex.printStackTrace()
        }
    }

    private fun getandsespareService() {
        try{
            if(callApi?.isNetworkConnected!!){
                dialog =MessageUtils.showDialog(this)
                val map:HashMap<String,Any> = HashMap()
                map["userid"]= LocalSession.getUserInfo(this, KEY_ID)
                map["enquiry_id"]= enquiryId
                val calinterface = callApi?.connectRetro("POST","issuesparescomplaint")
                val callback = calinterface?.call_post("issuesparescomplaint","Bearer "+LocalSession.getUserInfo(this, KEY_TOKEN),map)
                callback?.enqueue(object:Callback<ResponseBody>{
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        MessageUtils.showSnackBar(this@CRMDetailsActivity,snack_view,t.message)
                    }
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                spareServicecomplaintRes = Gson().fromJson(response.body()?.string(),SpareServiceccmplntRes::class.java)
                                if(spareServicecomplaintRes?.success!!){
                                    if(spareServicecomplaintRes?.data?.enquiry?.get(0)?.serialNo.toString().isEmpty()){
                                        MessageUtils.showToastMessageLong(this@CRMDetailsActivity,getString(R.string.invalienq))
                                        if(dialogSpareServiceRaiseComplaint?.isShowing!!){
                                            dialogSpareServiceRaiseComplaint?.dismiss()
                                        }else{
                                            try{
                                                dialogSpareServiceRaiseComplaint?.dismiss()
                                            }catch (ex:Exception){
                                                ex.printStackTrace()
                                            }
                                        }
                                    }else{
                                        sPSCtvSerialno?.setText(spareServicecomplaintRes?.data?.enquiry?.get(0)?.serialNo.toString())
                                    }
                                }else{
                                    if(dialogSpareServiceRaiseComplaint?.isShowing!!){
                                        dialogSpareServiceRaiseComplaint?.dismiss()
                                    }else{
                                        try{
                                            dialogSpareServiceRaiseComplaint?.dismiss()
                                        }catch (ex:Exception){
                                            ex.printStackTrace()
                                        }
                                    }
                                }
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }
                        }else{
                            MessageUtils.showSnackBar(this@CRMDetailsActivity,snack_view,response?.message())
                        }

                    }

                })
            }else{
                MessageUtils.showSnackBarAction(this,snack_view,getString(R.string.check_internet))
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun sendToSpareServiceComplaint(serialno: String, complaints: String) {
        try{
            val serialnoBDY = RequestBody.create(MediaType.parse("multipart/form-data"), serialno)
            val serviceComplaintBDY = RequestBody.create(MediaType.parse("multipart/form-data"), complaints)
            val userId = RequestBody.create(MediaType.parse("multipart/form-data"), LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_ID))
            val enquiryId = RequestBody.create(MediaType.parse("multipart/form-data"), enquiryId)
            /**
             * get the image convert multipart
             *
             **/
            println("testing url path" + getPath(this@CRMDetailsActivity, this.spareServiceFileUri!!))
//            val imagefile =CompressFile.getCompressedImageFile( File(getPath(this@CRMDetailsActivity, this.spareServiceFileUri!!)),this)
//            val imagefile =CompressFile.getCompressedImageFile( File(spareServiceFileUri.toString()),this)
            val imagefile =CompressFile.getCompressedImageFile( File(getPath(this,spareServiceFileUri!!)),this)
            println("imageFilePath" + imagefile?.absolutePath)


            val spareServiceComplaintIMage = RequestBody.create(MediaType.parse("*/*"), imagefile)
            val complaintImagePart = MultipartBody.Part.createFormData("image", imagefile.name, spareServiceComplaintIMage)



            dialog = MessageUtils.showDialog(this@CRMDetailsActivity)
            val callBack: Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response> = MyUtils.getRetroService().call_po(
                    "savesparescomplaint",
                    BEARER + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_TOKEN),
                    serialnoBDY, serviceComplaintBDY, enquiryId, userId,
                    complaintImagePart)


            /**
             * getting SpareInvoicePojos from  call Api interface
             *
             */

            callBack.enqueue(object : Callback<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response> {
                override fun onResponse(call: Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>, response: Response<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>) {

                    MessageUtils.dismissDialog(dialog)
                    if (response.isSuccessful) {
                        val res = response.body()
                        println("SpareInvoicePojos" + res)
                        if (res?.isSuccess!!) {
                            Toast.makeText(this@CRMDetailsActivity, res?.message.toString(), Toast.LENGTH_SHORT).show()
                            dialogSpareServiceRaiseComplaint?.dismiss()
                            finish()
                        } else {
                            Toast.makeText(this@CRMDetailsActivity, res?.message.toString(), Toast.LENGTH_SHORT).show()
                        }
                    } else {

                        MessageUtils.showToastMessage(this@CRMDetailsActivity, "Server Not Found" + response.body())
                    }

                }

                override fun onFailure(call: Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    Toast.makeText(this@CRMDetailsActivity, "Sever NOT Found" + t, Toast.LENGTH_SHORT).show()
                    Log.d("SpareInvoicePojos Failure", "" + t)
                    Log.d("SpareInvoicePojos Failure", "\n" + call)

                }


            })

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }


    private fun adddynamcsparestoMap(map: HashMap<String, Any>) {
        try{
            if(sp_rootcause.selectedItem.toString() == null ||  sp_rootcause.selectedItem.toString().equals(getString(R.string.select_rootcause))){
                Log.d("sp_rootcause",""+sp_rootcause.selectedItem.toString())
            }else{
                map.put("rootcause",sp_rootcause.selectedItem.toString())
            }
            map.put("rootcause",sp_rootcause.selectedItem.toString())
            var warrantycomponent: ServiceReplecementViewPojos? = null

/*
            if (models?.data?.replacementList!!.get(0).isValid) {
                if (mEdtServiceType?.text.toString().equals("Warranty")) {
                    warrantycomponent = ServiceReplecementViewPojos(
                            bs_edt_old_serial_number.text.toString(),
                            bs_edt_component_name.text.toString(),
                            bs_sp_new_serial_number.selectedItem.toString(),
                            "warranty", " New Replacement")
                } else {
                    warrantycomponent = ServiceReplecementViewPojos(
                            bs_edt_old_serial_number.text.toString(),
                            bs_edt_component_name.text.toString(),
                            bs_sp_new_serial_number.selectedItem.toString(),

                            "warranty", " New Replacement")
                }
                servicersendPojoArrayList.add(warrantycomponent)
            } else {
                if (models?.data?.servicereplacement!!.get(0).isValid) {
                    if (mEdtServiceType?.text.toString().equals("Warranty")) {
                        warrantycomponent = ServiceReplecementViewPojos(
                                wrs_edt_old_serial_number.text.toString(),
                                ws_edt_component_name.text.toString(),
                                wss_edt_new_serial_number.text.toString(),

                                "warranty", " Service Replacement")
                    } else {
                        warrantycomponent = ServiceReplecementViewPojos(
                                wrs_edt_old_serial_number.text.toString(),
                                ws_edt_component_name.text.toString(),
                                wss_edt_new_serial_number.text.toString(),

                                "warranty", " Service Replacement")
                    }
                    servicersendPojoArrayList.add(warrantycomponent)
                } else {
                    Log.d("May be use.............. Waits ", "" + models?.data?.servicereplacement.toString())
                }

            }


            val warrantySpareComponent: HashMap<String, Any> = HashMap()
            warrantySpareComponent.put("component_name", servicersendPojoArrayList)
            map.put("component_name", warrantySpareComponent)
            /**
             * dynamic add more spares  for
             *
             */
            if (servicereplacePojoArrayList == null || servicereplacePojoArrayList?.size == 0) {
                Log.d("Service spare Pojo ArrayList", "" + servicersendPojoArrayList?.size)
            } else {
                val dynamicaddpaidSparecomponent: HashMap<String, Any> = HashMap()
                dynamicaddpaidSparecomponent.put("componentname", servicereplacePojoArrayList)
                map.put("componentname", dynamicaddpaidSparecomponent)
            }
*/



            /*** SErvice Four  types are new Repacement Service ,free Replacement,paid componenet**/

            val newreplacementMap :HashMap<String,Any> = HashMap()
            val servicereplacementMap :HashMap<String,Any> = HashMap()
            val paidreplacementMap :HashMap<String,Any> = HashMap()
            val freereplacementMap :HashMap<String,Any> = HashMap()
            if(models?.data?.replacementList?.size!=0){
                if(newReplacementServerLISt.size !=0){
                    newreplacementMap.clear()
                    newreplacementMap["newreplacement"] = newReplacementServerLISt
                }
                map["newreplacements"] = newreplacementMap

            }else{
                Log.d("NewReplacmentArrayList","Both isEmpty")
            }
            if(models?.data?.servicereplacement?.size!=0){
                if(serviceReplacementServerLISt.size!=0){
                    servicereplacementMap.clear()
                    servicereplacementMap["servicereplacement"] = serviceReplacementServerLISt
                }
                map["servicereplacements"] = servicereplacementMap

            }else{
                Log.d("serviceReplacmentArrayList","Both isEmpty")
            }
            if(models?.data?.paidcomponents?.size!=0){
                if(paidReplacementServerLISt.size!=0){
                    paidreplacementMap.clear()
                    paidreplacementMap["paidreplacement"] = paidReplacementServerLISt
                }
                map["paidreplacements"] = paidreplacementMap
            }else{
                Log.d("PaidReplacmentArrayList","Both isEmpty")
            }
            if(models?.data?.freereplacementArrayList?.size!=0){
                if(freeReplacementServerLISt.size!=0){
                    freereplacementMap.clear()
                    freereplacementMap["freereplacement"] = freeReplacementServerLISt
                }
                map["freereplacements"] = freereplacementMap
            }else{
                Log.d("FreeReplacmentArrayList","Both isEmpty")
            }
        }catch (e:Exception){
            e.printStackTrace()
        }

    }

    private fun dynamicAddingSpareFn() {
        /*      Log.d("dynamiccomponentsize",""+compontList?.size)
          if(compontList?.size !! <= 0){
              dynamic_add.visibility = View.GONE
          }else{
              dynamic_add.visibility = View.VISIBLE


            bs_sp_old_serial_number_addmore.adapter = oldsrlListAdapter

            bs_sp_new_serial_number_addmore.adapter = newSerialNoaddmoreListAdapter

          bs_sp_old_serial_number_addmore.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
              override fun onNothingSelected(parent: AdapterView<*>?) {
                  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
              }

              override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                  bs_edt_old_serial_number_addmore.setText(bs_sp_old_serial_number_addmore.selectedItem.toString())
                  var oldSerialNo = bs_edt_old_serial_number_addmore.text.toString()
                  if (oldSerialNo.equals("Select Old serial No")) {
                      Log.d("Info Of old serial Number", "" + oldSerialNo)
                  } else {

                  }
              }
          }
              bs_sp_component_name_addmore.adapter = compontListAdapter
          bs_sp_component_name_addmore.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
              override fun onNothingSelected(parent: AdapterView<*>?) {
                  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
              }

              override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                  bs_edt_component_name_addmore.setText(bs_sp_component_name_addmore.selectedItem.toString())
                  var componet = bs_edt_component_name_addmore.text.toString()
                  if (componet.equals("SelectThe Component ")) {
                      Log.d("Info Component", "" + componet)
                  } else {
                      getOldSerialNumberFromAPI(componet)
                  }

              }
          }

          bs_sp_new_serial_number_addmore.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
              override fun onNothingSelected(parent: AdapterView<*>?) {
                  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
              }

              override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                  bs_edt_new_serial_number_addmore.setText(bs_sp_new_serial_number_addmore.selectedItem.toString())
              }

          }
          }
          dynamic_add.setOnClickListener {

              if (  bs_sp_component_name_addmore.selectedItem == null||bs_sp_component_name_addmore.selectedItem.toString().isEmpty()
                      || bs_sp_component_name_addmore.selectedItem.toString().length == 0 || bs_edt_component_name_addmore.text.toString().equals("Select your Component")
              ) {
  //                Toast.makeText(this@CRMDetailsActivity, "" + bs_sp_old_serial_number_addmore.selectedItem.toString(), Toast.LENGTH_SHORT).show()
                  bs_edt_component_name_addmore.setError("Valid Data Required")
                  MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, "Valid Data")
              } else {
                  bs_edt_component_name_addmore.setError(null)
                  bs_edt_component_name_addmore.clearFocus()
                  if (bs_edt_old_serial_number_addmore.text.toString().isEmpty() || bs_edt_old_serial_number_addmore.text == null
                          || bs_edt_old_serial_number_addmore.text.toString().length == 0
                          || bs_edt_old_serial_number_addmore.text.toString().equals("SelectThe Old Serial No")
                      ||bs_sp_old_serial_number_addmore.selectedItem.toString().equals(getString(R.string.select_old_serial_number))
                  ) {
                      bs_edt_old_serial_number_addmore.setError("Valid Data Required")
                      Toast.makeText(this,"Valid Data is Required",Toast.LENGTH_LONG).show()
                  } else {
                      bs_edt_old_serial_number_addmore.setError(null)
                      if (bs_sp_new_serial_number_addmore.selectedItem == null
                              || bs_sp_new_serial_number_addmore.selectedItem.toString().length == 0 || bs_edt_new_serial_number_addmore.text.toString().equals("SelectThe New Serial No")
                              || bs_edt_new_serial_number_addmore.text.toString().equals("Stock Not Available")
                      ) {
                          bs_edt_new_serial_number_addmore.setError("Valid Data Required")
                          var toast = Toast.makeText(this, "Valid Data Required", Toast.LENGTH_SHORT)
                          toast.setGravity(Gravity.HORIZONTAL_GRAVITY_MASK, 0, 0)
                          toast.show()
                      } else {
                          bs_edt_new_serial_number_addmore.setError(null)
                          bs_edt_new_serial_number_addmore.clearFocus()
                          dynamicAddView(bs_edt_old_serial_number_addmore.text.toString(),
                                  bs_sp_component_name_addmore.selectedItem.toString(),
                                  bs_sp_new_serial_number_addmore.selectedItem.toString())
                          var i = 0
                          var pos = 0
                          val changednewstockList: ArrayList<String> = ArrayList()
                          for (spre in newSerialNoList!!) {
                              if (bs_sp_new_serial_number_addmore?.selectedItem.toString().equals(newSerialNoList?.get(i))) {
                                  Log.d("changed StockList", "" + bs_sp_new_serial_number_addmore.selectedItem.toString())
                                  pos = i
                              } else {
                                  changednewstockList.add(spre)
                              }
                              ++i
                          }
                          *//* newSerialNoaddmoreListAdapter = ArrayAdapter<String>(this@CRMDetailsActivity,R.layout.spinnertext,R.id.text1,changednewstockList)
                         newSerialNoaddmoreListAdapter?.notifyDataSetChanged()
                         bs_sp_new_serial_number_addmore.adapter = newSerialNoaddmoreListAdapter

                         bs_sp_old_serial_number_addmore.setSelection(0)
                         bs_sp_component_name_addmore.setSelection(0)
                         bs_sp_new_serial_number_addmore.setSelection(0)*//*
                    }
                }
            }

        }*/

    }

    private fun getOldSerialNumberFromAPI(componet: String) {
        var hashmap: HashMap<String, Any> = HashMap()
        if (enquiryId != null) {
            hashmap.put("enquiry_id", enquiryId)
        }
        hashmap.put("userid", LocalSession.getUserInfo(this, KEY_ID))
        hashmap.put("chassis_no", map.get("chassis")!!)
        hashmap.put("component", componet)
        if(callApi?.isNetworkConnected!!){
            dialog = MessageUtils.showDialog(this@CRMDetailsActivity)
            callApi?.callApi("getoldserielnoandstock", "POST", hashmap, 0)
        }else{
            MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView,getString(R.string.stocknoavl))
        }
    }

    private fun dynamicAddView(oldSerialNo: String, component: String, newSerialNO: String) {
        val serviceReplecementViewPojos: ServiceReplecementViewPojos = ServiceReplecementViewPojos(oldSerialNo, component, newSerialNO, "nonwarranty", "")
        servicereplacePojoArrayList?.add(serviceReplecementViewPojos)
        servicerepalcementAdapter = ServiceReplecementAdapter(this, servicereplacePojoArrayList)
        /*  rclv_service_replace.layoutManager = LinearLayoutManager(this,LinearLayout.VERTICAL,false)
          rclv_service_replace.adapter = servicerepalcementAdapter*/
    }

    private fun popupServiceWarrantyComplaint() {
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.dialog_service_warranty_complints, null)
//        dialogRaiseComplaint?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogRaiseComplaint?.setCancelable(false)
        dialogRaiseComplaint?.setContentView(view)

//        dialogRaiseComplaint?.setContentView(R.layout.dialog_service_warranty_complints)

        dialogRaiseComplaint?.show()
        /**
         * getting the corresponding id
        button id
         */
        btnCancel = dialogRaiseComplaint?.findViewById(R.id.cb_cancel_service)
        btnSubmit = dialogRaiseComplaint?.findViewById(R.id.cb_submit_service)
        imgbtnAddraisecomplaint = dialogRaiseComplaint?.findViewById(R.id.imgbtn_addmore)


        tvChassisNo = dialogRaiseComplaint?.findViewById(R.id.tv_chassno)
        tvImageName = dialogRaiseComplaint?.findViewById(R.id.tv_image_name)

        spDefectItem = dialogRaiseComplaint?.findViewById(R.id.sp_defect_item)
        rlDefectItem = dialogRaiseComplaint?.findViewById(R.id.rl_defect_item)
        edtDefectItem = dialogRaiseComplaint?.findViewById(R.id.edt_defect_item)

        edtDefectQty = dialogRaiseComplaint?.findViewById(R.id.edt_defect_qty)

        edtServiceWarrantyComplaints = dialogRaiseComplaint?.findViewById(R.id.edt_defect_complaints)

        igtakeImage = dialogRaiseComplaint?.findViewById(R.id.ig_take_image)
        recycleviewDefectItem = dialogRaiseComplaint?.findViewById(R.id.recyle_addmore_complaint)
        getWarrantydetails()

        /***
         * set string array to  spinner for defect item
         * and string array must be change after the api response getting
         */
        imgbtnAddraisecomplaint?.setOnClickListener {
            try{
                AddmoreDefectItem()
                edtDefectQty?.setText("1")
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }

        btnCancel?.setOnClickListener {
            try{
                dialogRaiseComplaint?.dismiss()
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }

        btnSubmit?.setOnClickListener {

            if (tvChassisNo?.getText().toString().isEmpty() || tvChassisNo?.getText().toString() == null || tvChassisNo?.getText().toString().length == 0) {
                tvChassisNo?.setError("require field")
                MessageUtils.showToastMessageLong(this@CRMDetailsActivity,getString(R.string.stocknoavl))
            } else {
                if (spDefectItem?.selectedItem.toString() == null || spDefectItem?.selectedItem.toString().isEmpty()
                        || spDefectItem?.selectedItem.toString().equals(" Select Defect Item ") || spDefectItem?.selectedItem.toString().length == 0
                ) {
                    edtDefectItem?.setError(" require field ")
                    MessageUtils.showToastMessage(this@CRMDetailsActivity,"Defect Item Can't be Empty")
                } else {
                    if (edtDefectQty?.text.toString().isEmpty() ||
                            edtDefectQty?.text.toString() == null ||
                            edtDefectQty?.text.toString().length == 0 ||
                            Integer.parseInt(edtDefectQty?.text.toString()) < 1
                    ) {
                        edtDefectQty?.setError("Require  field ")
                    } else {
                        if (edtServiceWarrantyComplaints?.text.toString().isEmpty() || edtServiceWarrantyComplaints?.text.toString().length == 0
                                || edtServiceWarrantyComplaints?.text.toString() == null) {
                            edtServiceWarrantyComplaints?.setError("Required Field ")

                        } else {
                            if (outputFileUri == null) {
                                MessageUtils.showToastMessage(this@CRMDetailsActivity, "please select  Image")
                                MessageUtils.showSnackBar(this@CRMDetailsActivity, view, "please select Image")
                            } else {
//                                dialog = MessageUtils.showDialog(this@CRMDetailsActivity)
                                sendToServerwrntyComplnt(tvChassisNo?.text.toString(), spDefectItem?.selectedItem.toString(), edtDefectQty?.text.toString(),
                                        edtServiceWarrantyComplaints?.text.toString())
                            }

                        }
                    }
                }
            }


        }
        spDefectItem?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                var taskInput = spDefectItem?.selectedItem as String
                edtDefectItem?.setText(taskInput)
                if (!taskInput.equals("")) {
                    edtDefectItem?.setText(taskInput)
                } else {
                    edtDefectItem?.setText("")
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {

            }
        }
        igtakeImage?.setOnClickListener {
            if (isReadLocationAllowed()) {

                //MessageUtils.showToastMessage(this@CRMDetailsActivity, "Open Camera")
                val dir =/* "" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +*/ "/sdcard/Android/data/com.ampere.bike.ampere/files/Ampere_Service/ServiceComplaints/"
                val newdir = File(dir)
                newdir.mkdirs()
                val file = dir + DateUtils.getCurrentDate(DateUtils.DATE_DDMMYYYYHHMMAP) + ".jpg"
                val newfile = File(file)
                try {
                    newfile.createNewFile()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                outputFileUri = Uri.fromFile(newfile)
                Log.d("outputFileUriClicke", "" + outputFileUri)

                /* var cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                 cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
                 startActivityForResult(cameraIntent, TAKE_CAMERA_CODE)*/
                launchCamera(8888)
            } else {
                requestLocationPermission()
            }
        }
    }
    private fun popupFreeReplacementRequired() {
        try{
            val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(R.layout.dialog_freereplacement_required, null)
            dialogFreeReplacementRequired?.setCancelable(false)
            dialogFreeReplacementRequired?.setContentView(view)
            dialogFreeReplacementRequired?.show()
            /**get the XML Id**/
            ffRbtnCancel = view?.findViewById(R.id.frr_cb_cancel_service)
            ffRbtnSubmit = view?.findViewById(R.id.frr_cb_submit_service)
            ffRimgbtnAddraisecomplaint = view?.findViewById(R.id.frr_imgbtn_addmore)
            ffRtvChassisNo = view?.findViewById(R.id.frr_tv_chassno)
            ffRtvImageName = view?.findViewById(R.id.frr_tv_image_name)
            ffRspSpareItem = view?.findViewById(R.id.frr_sp_spare_item)
            ffRrlSpareItem = view?.findViewById(R.id.frr_rl_spare_item)
            ffRedtSpareItem = view?.findViewById(R.id.frr_edt_spare_item)
            ffRedtSpareQty = view?.findViewById(R.id.frr_edt_defect_qty)
            ffRedtServiceWarrantyComplaints = view?.findViewById(R.id.frr_edt_defect_complaints)

            ffRigtakeImage = view?.findViewById(R.id.frr_ig_take_image)
            ffRrecycleviewDefectItem = view?.findViewById(R.id.frr_recyle_addmore_complaint)
            /**
             * getting  free Replacement Required
             */
            getandsetFreeRpcmntReq()
            ffRimgbtnAddraisecomplaint?.setOnClickListener {
                try{
                    AddmoreSpareItem()

                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

            ffRbtnCancel?.setOnClickListener {
                try{
                    if(dialogFreeReplacementRequired?.isShowing()!!){
                        dialogFreeReplacementRequired?.dismiss()
                    }
//                   getandsetFreeRpcmntReq()
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }

            ffRbtnSubmit?.setOnClickListener {

                if (ffRtvChassisNo?.getText().toString().isEmpty() || ffRtvChassisNo?.getText().toString() == null || ffRtvChassisNo?.getText().toString().length == 0) {
//                   ffRtvChassisNo?.setError("require field")
                    MessageUtils.showToastMessageLong(this@CRMDetailsActivity,getString(R.string.stocknoavl))
                } else {
                    if (ffRspSpareItem?.selectedItem.toString() == null || ffRspSpareItem?.selectedItem.toString().isEmpty()
                            || ffRspSpareItem?.selectedItem.toString().equals(" Select Defect Item ") || ffRspSpareItem?.selectedItem.toString().length == 0
                    ) {
//                       ffRedtSpareItem?.setError(" require field ")
                        MessageUtils.showToastMessage(this,"Spare QTY Can't be Empty ")
                    } else {
                        if (ffRedtSpareQty?.text.toString().isEmpty() ||
                                ffRedtSpareQty?.text.toString() == null ||
                                ffRedtSpareQty?.text.toString().length == 0 ||
                                Integer.parseInt(ffRedtSpareQty?.text.toString()) < 1
                        ) {
                            MessageUtils.showToastMessage(this,"Spare QTY Can't be Empty ")
                        } else {
                            if (ffRedtServiceWarrantyComplaints?.text.toString().isEmpty() || ffRedtServiceWarrantyComplaints?.text.toString().length == 0
                                    || ffRedtServiceWarrantyComplaints?.text.toString() == null) {
//                               ffRedtServiceWarrantyComplaints?.setError("Required Field ")
                                MessageUtils.showToastMessage(this,"Complaint  Can't be Empty ")
                            } else {
                                if (spareFileUri == null) {
                                    MessageUtils.showToastMessage(this@CRMDetailsActivity, "please select  Image")
                                } else {
//                                dialog = MessageUtils.showDialog(this@CRMDetailsActivity)
                                    sendToFreeReplacementRequired(ffRtvChassisNo?.text.toString(), ffRspSpareItem?.selectedItem.toString(), ffRedtSpareQty?.text.toString(),
                                            ffRedtServiceWarrantyComplaints?.text.toString())
                                }

                            }
                        }
                    }
                }
            }
            ffRspSpareItem?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                    var taskInput = ffRspSpareItem?.selectedItem as String
                    ffRedtSpareItem?.setText(taskInput)
                    if (!taskInput.equals("")) {
                        ffRedtSpareItem?.setText(taskInput)
                    } else {
                        ffRedtSpareItem?.setText("")                   }
                }
                override fun onNothingSelected(adapterView: AdapterView<*>) {

                }
            }
            ffRigtakeImage?.setOnClickListener {
                if (isReadLocationAllowed()) {

                    //MessageUtils.showToastMessage(this@CRMDetailsActivity, "Open Camera")
                    var dir =/* "" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +*/ "/sdcard/Android/data/com.ampere.bike.ampere/files/Ampere_Service/ServiceComplaints/"
                    var newdir = File(dir)
                    newdir.mkdirs()
                    var file = dir + DateUtils.getCurrentDate(DateUtils.DATE_DDMMYYYYHHMMAP) + ".jpg"
                    var newfile = File(file)
                    try {
                        newfile.createNewFile()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                    spareFileUri = Uri.fromFile(newfile)
                    Log.d("spareFileUritFileUriClicke", "" + spareFileUri)

                    /* var cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                     cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
                     startActivityForResult(cameraIntent, TAKE_CAMERA_CODE)*/
                    launchCamera(12153)
                } else {
                    requestLocationPermission()
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    private fun getandsetFreeRpcmntReq() {
        try{
            if(callApi?.isNetworkConnected!!){
                val map :HashMap<String,Any> = HashMap()
                map.put("userid",LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_ID))
                map.put("enquiry_id", enquiryId)
                dialog  = MessageUtils.showDialog(this@CRMDetailsActivity)
                val calinterface = callApi?.connectRetro("POST","freereplacementcomplaint")
                val callback = calinterface?.call_post("freereplacementcomplaint","Bearer "+LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_TOKEN),map)
                callback?.enqueue(object :Callback<ResponseBody>{
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                        Log.d("FailureREsponse",""+t.message)
                        Log.d("FailureREsponse",""+t.localizedMessage)
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        if(response.isSuccessful){
                            try{
                                freeReplacementReqPojoRESPNSE = Gson().fromJson(response.body()?.string(),FreeReplacementReqPojoRESPNSE::class.java)
                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }
                            if(freeReplacementReqPojoRESPNSE?.success!!){
                                try{
                                    for(spare in freeReplacementReqPojoRESPNSE?.data?.spares!!){
                                        if(spare?.spares != null){
                                            ffrSpareSTrList.add(spare?.spares)
                                        }
                                    }
                                    ffrSpareSTrAdapter =ArrayAdapter(this@CRMDetailsActivity,R.layout.spinnertext,R.id.text1,ffrSpareSTrList)
                                    ffRspSpareItem?.adapter =ffrSpareSTrAdapter
                                    try{
                                        ffRtvChassisNo?.setText(freeReplacementReqPojoRESPNSE?.data?.enquiry?.get(0)?.chassisNo)
                                    }catch (ex:Exception){
                                        ex.printStackTrace()
                                    }
                                }catch (ex:Exception){
                                    ex.printStackTrace()
                                }
                            }else{
                                Log.d("ResponseSuccess","RepsonseComing  success False")
                            }

                        }else{
                            Log.d("ResponseIFailure",""+response.body()?.string())
                        }
                    }

                })
            }else{
                MessageUtils.showToastMessage(this@CRMDetailsActivity,getString(R.string.check_internet))
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    private fun AddmoreSpareItem() {
        try{
            if(ffRspSpareItem?.selectedItem.toString().isEmpty() || ffRspSpareItem?.selectedItem.toString().equals(null)
                    || ffRspSpareItem?.selectedItem.toString().equals(getString(R.string.no_data_avl))){
                Log.d("defectItem",""+ffRspSpareItem?.selectedItem.toString())
//                MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView,"Defect QTY Can't be Empty ")
                MessageUtils.showToastMessageLong(this,"Spare Item Can't be Empty ")
            }
            else{
                if(ffRedtSpareQty?.text?.toString().isNullOrEmpty()){
                    Log.d("Qtunatity",""+ffRspSpareItem?.selectedItem.toString())
                    ffRedtSpareItem?.setText(ffRspSpareItem?.selectedItem.toString())
//                    MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView,"Defect QTY Can't be Empty ")
                    MessageUtils.showToastMessage(this,"Spare QTY Can't be Empty ")
                }else{

                    val defectItem = DefectitemQtyPojos(ffRspSpareItem?.selectedItem.toString(),ffRedtSpareQty?.text?.toString())
                    ffrSpareItemnQtyList?.add(defectItem)
                    spareItemQtyAddmoreAdapter = SpareItemQtyAddmoreAdapter(this@CRMDetailsActivity,ffrSpareItemnQtyList!!,this)
                    ffRrecycleviewDefectItem?.layoutManager = LinearLayoutManager(this@CRMDetailsActivity,LinearLayout.VERTICAL,false)
                    ffRrecycleviewDefectItem?.adapter = spareItemQtyAddmoreAdapter
                    val position=ffrSpareSTrList.indexOf(ffRspSpareItem?.selectedItem?.toString())
                    ffrSpareSTrList.removeAt(position)
                    if(ffrSpareSTrList.size  <=0){
                        ffrSpareSTrList.add(getString(R.string.no_data_avl))
                        ffrSpareSTrAdapter?.notifyDataSetChanged()
                    }else{
                        if(ffrSpareSTrList.size<=1){
                            ffRimgbtnAddraisecomplaint?.visibility = View.GONE
                            ffrSpareSTrAdapter?.notifyDataSetChanged()
                        }else{
                            ffRimgbtnAddraisecomplaint?.visibility =View.VISIBLE
                            ffrSpareSTrAdapter?.notifyDataSetChanged()
                        }
                    }
                    ffRedtSpareItem?.setText(ffRspSpareItem?.selectedItem.toString())
                }
            }
            ffRedtSpareQty?.setText("")
            ffRedtSpareItem?.setText(ffRspSpareItem?.selectedItem.toString())
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun sendToFreeReplacementRequired(chassisNo: String, spareItem: String, spareQty: String, complaints: String) {
        try{
            /**
             * get to assign values for call api services
             *
             */

            val ffrchassisnoBDY = RequestBody.create(MediaType.parse("multipart/form-data"), chassisNo)
            /*       val defectItemBDY = RequestBody.create(MediaType.parse("multipart/form-data"), defectItem)
                   val defectQtyBDY = RequestBody.create(MediaType.parse("multipart/form-data"), defectQty)*/
            val ffrSpareComplaintBDY = RequestBody.create(MediaType.parse("multipart/form-data"), complaints)
            val ffruserId = RequestBody.create(MediaType.parse("multipart/form-data"), LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_ID))
            val ffrenquiryId = RequestBody.create(MediaType.parse("multipart/form-data"), enquiryId)

            /**
             * Multiple defectItem
             */
            val defectitemQtyPojos = DefectitemQtyPojos(spareItem,spareQty)
            ffrSpareItemnQtyList?.add(defectitemQtyPojos)
            val   map :HashMap<String,Any> =HashMap()
            map.put("defectitem", ffrSpareItemnQtyList!!)
            /**
             * get the image convert multipart
             *
             **/
            println("testing url path" + getPath(this@CRMDetailsActivity, this.spareFileUri!!))
            val imagefile = CompressFile.getCompressedImageFile(File(getPath(this@CRMDetailsActivity, this.spareFileUri!!)),this)
            println("imageFilePath" + getPath(this@CRMDetailsActivity, this.spareFileUri!!))
            println("imageFilePath\n" + imagefile.name)
            println("imageFilePath\n" + imagefile.length())
            println("imageFilePath\n" + imagefile.path)


            val spareImagePart = RequestBody.create(MediaType.parse("*/*"), imagefile)
            val complaintImagePart = MultipartBody.Part.createFormData("image", imagefile.name, spareImagePart)

            dialog =MessageUtils.showDialog(this@CRMDetailsActivity)
            val callBack: Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response> = MyUtils.getRetroService().call_post(
                    "savefreereplacementcomplaint",
                    BEARER + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_TOKEN),
                    ffrchassisnoBDY, map, ffrSpareComplaintBDY, ffrenquiryId, ffruserId,
                    complaintImagePart)


            /**
             * getting SpareInvoicePojos from  call Api interface
             *
             */

            callBack.enqueue(object : Callback<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response> {
                override fun onResponse(call: Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>, response: Response<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>) {
                    MessageUtils.dismissDialog(dialog)
                    if (response.isSuccessful) {
//                    MessageUtils.dismissDialog(dialog)
                        val res = response.body()
                        println("SpareInvoicePojos" + res)
//                    val data = res?.data
//                    Log.d("Data",""+data)
                        if (res?.isSuccess!!) {
                            Toast.makeText(this@CRMDetailsActivity, res.message.toString(), Toast.LENGTH_SHORT).show()
                            if(dialogFreeReplacementRequired?.isShowing()!!){
                                dialogFreeReplacementRequired?.dismiss()
                            }
                            finish()
                        } else {
                            Toast.makeText(this@CRMDetailsActivity, res.message.toString(), Toast.LENGTH_SHORT).show()
                        }
                    } else {
//                    MessageUtils.dismissDialog(dialog)
                        MessageUtils.showToastMessage(this@CRMDetailsActivity, "Server Not Found" + response.body())
                    }

                }

                override fun onFailure(call: Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    Toast.makeText(this@CRMDetailsActivity, "Sever NOT Found" + t, Toast.LENGTH_SHORT).show()
                    Log.d("SpareInvoicePojos Failure", "" + t)
                    Log.d("SpareInvoicePojos Failure", "\n" + call)

                }


            })

        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    private fun AddmoreDefectItem() {
        try{
            Log.d("defectItem",""+spDefectItem?.selectedItem.toString())
            if(spDefectItem?.selectedItem.toString().isEmpty() || spDefectItem?.selectedItem.toString().equals(null)
                    || spDefectItem?.selectedItem.toString().equals(getString(R.string.no_data_avl))){
                Log.d("defectItem",""+spDefectItem?.selectedItem.toString())
//                MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView,"Defect QTY Can't be Empty ")
                MessageUtils.showToastMessageLong(this,"Defect Item Can't be Empty ")
            }
            else{
                if(edtDefectQty?.text?.toString().isNullOrEmpty()){
                    Log.d("Qtunatity",""+spDefectItem?.selectedItem.toString())
//                    MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView,"Defect QTY Can't be Empty ")
                    MessageUtils.showToastMessage(this,"Defect QTY Can't be Empty ")
                }else{

                    val defectItem = DefectitemQtyPojos(spDefectItem?.selectedItem.toString(),edtDefectQty?.text?.toString())
                    warrantydefectitemList?.add(defectItem)
                    warrantydefectAddmoreAdapter = DefectItemQtyAddmoreAdapter(this@CRMDetailsActivity,warrantydefectitemList!!,this)
                    recycleviewDefectItem?.layoutManager = LinearLayoutManager(this@CRMDetailsActivity,LinearLayout.VERTICAL,false)
                    recycleviewDefectItem?.adapter = warrantydefectAddmoreAdapter
                    val position=defectitemList?.indexOf(spDefectItem?.selectedItem?.toString())
                    defectitemList?.removeAt(position!!)
                    if(defectitemList?.size !! <=0){
                        defectitemList?.add(getString(R.string.no_data_avl))
                        defectTtemSTRAdpter?.notifyDataSetChanged()
                    }else{
                        if(defectitemList?.size!!<=1){
                            imgbtnAddraisecomplaint?.visibility = View.GONE
                            defectTtemSTRAdpter?.notifyDataSetChanged()
                        }else{
                            imgbtnAddraisecomplaint?.visibility =View.VISIBLE
                            defectTtemSTRAdpter?.notifyDataSetChanged()
                        }
                    }

                }
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun getWarrantydetails() {

        var map: HashMap<String, Any> = HashMap()
        map.put("userid", LocalSession.KEY_ID)
        map.put("enquiry_id", enquiryId)



        if (callApi?.isNetworkConnected!!) {
            dialog = MessageUtils.showDialog(this@CRMDetailsActivity)
            callApi?.callApi("raisecomplaint", "POST", map, 0)
        } else {

            val toast: Toast = Toast.makeText(this, "Please Check Internet Connection", Toast.LENGTH_LONG)
            toast.setGravity(Gravity.TOP, 0, 0)
            toast.show()
        }
    }

    private fun sendToServerwrntyComplnt(chassisno: String, defectItem: String, defectQty: String, serviceComplaint: String) {
        /**
         * get to assign values for call api services
         *
         */

        val chassisnoBDY = RequestBody.create(MediaType.parse("multipart/form-data"), chassisno)
        /*       val defectItemBDY = RequestBody.create(MediaType.parse("multipart/form-data"), defectItem)
               val defectQtyBDY = RequestBody.create(MediaType.parse("multipart/form-data"), defectQty)*/
        val serviceComplaintBDY = RequestBody.create(MediaType.parse("multipart/form-data"), serviceComplaint)
        val userId = RequestBody.create(MediaType.parse("multipart/form-data"), LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_ID))
        val enquiryId = RequestBody.create(MediaType.parse("multipart/form-data"), enquiryId)

        /**
         * Multiple defectItem
         */
        val defectitemQtyPojos:DefectitemQtyPojos = DefectitemQtyPojos(defectItem,defectQty)
        warrantydefectitemList?.add(defectitemQtyPojos)
        val   hashmap :HashMap<String,Any> =HashMap()
        val   map :HashMap<String,Any> =HashMap()
        map.put("defectitem", warrantydefectitemList!!)
//        hashmap.put("defectitem",map)
        /**
         * get the image convert multipart
         *
         **/
        try {
            println("testing url path" + getPath(this@CRMDetailsActivity, this.outputFileUri!!))
            val imagefile = CompressFile.getCompressedImageFile(File(getPath(this@CRMDetailsActivity, this.outputFileUri!!)), this)
            println("imageFilePath" + getPath(this@CRMDetailsActivity, this.outputFileUri!!))
            println("imageFilePath\n" + imagefile.name)
            println("imageFilePath\n" + imagefile.length())
            println("imageFilePath\n" + imagefile.path)




        val warrantycomplaintIMage = RequestBody.create(MediaType.parse("*/*"), imagefile)
//        var warrantycomplaintIMage = RequestBody.create(MediaType.parse("*/*"), "empty")
        val complaintImagePart = MultipartBody.Part.createFormData("image", imagefile.name, warrantycomplaintIMage)

        /**
         * call api interface service
         * "chassis_no,
        val objOkHttpClient = OkHttpClient.Builder().readTimeout(1, TimeUnit.MINUTES).connectTimeout(1, TimeUnit.MINUTES).addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build()
        val objRetrofit = Retrofit.Builder().client(objOkHttpClient).baseUrl("https://parthifrancis.000webhostapp.com/parthi/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()


        val api=objRetrofit.create(RetrofitService::class.java)

        var callBack : Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.SpareInvoicePojos>
        =api.call_post(
        "uploadImage.php",
        BEARER+LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_TOKEN),
        chassisnoBDY,defectItemBDY,defectQtyBDY,serviceComplaintBDY,enquiryId,userId,
        complaintImagePart
        )

         */

        dialog = MessageUtils.showDialog(this@CRMDetailsActivity)
        var callBack: Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response> = MyUtils.getRetroService().call_post(
                "savecomplaint",
                BEARER + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_TOKEN),
                chassisnoBDY, map, serviceComplaintBDY, enquiryId, userId,
                complaintImagePart)


        /**
         * getting SpareInvoicePojos from  call Api interface
         *
         */

        callBack.enqueue(object : Callback<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response> {
            override fun onResponse(call: Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>, response: Response<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>) {

                MessageUtils.dismissDialog(dialog)
                if (response.isSuccessful) {
                    val res = response.body()
                    println("SpareInvoicePojos" + res)
//                    val data = res?.data
//                    Log.d("Data",""+data)
                    if (res?.isSuccess!!) {
                        Toast.makeText(this@CRMDetailsActivity, res?.message.toString(), Toast.LENGTH_SHORT).show()
                        dialogRaiseComplaint?.dismiss()
                        finish()
                    } else {
                        Toast.makeText(this@CRMDetailsActivity, res?.message.toString(), Toast.LENGTH_SHORT).show()
                    }
                } else {
//                    MessageUtils.dismissDialog(dialog)
                    MessageUtils.showToastMessage(this@CRMDetailsActivity, "Server Not Found" + response.body())
                }

            }

            override fun onFailure(call: Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                Toast.makeText(this@CRMDetailsActivity, "Sever NOT Found" + t, Toast.LENGTH_SHORT).show()
                Log.d("SpareInvoicePojos Failure", "" + t)
                Log.d("SpareInvoicePojos Failure", "\n" + call)
            }

        })

        }catch(ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun popupOnsubmit() {
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.dialog_service_warranty_complints, null)
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(this@CRMDetailsActivity)
        alertDialog.setCancelable(false)
        alertDialog.setView(view)
        val dialog: AlertDialog = alertDialog.create()
        dialog.show()


    }

    /**
     * Get Information of Components
     *
     */
    private fun onGetServiceComponents(strServiceType: String) {
        val map: HashMap<String, String> = HashMap()
        map.put("serviceType", strServiceType)
        val serviceComponents: Call<ModelServiceComponets> = MyUtils.getRetroService().onGetServiceComponents(BEARER + LocalSession.getUserInfo(this@CRMDetailsActivity, KEY_TOKEN), map)

        val dialog = MessageUtils.showDialog(this@CRMDetailsActivity)
        dialog.setOnKeyListener { dialogInterface, _, keyEvent ->
            serviceComponents.cancel()
            MessageUtils.dismissDialog(dialog)
            false
        }

        serviceComponents.enqueue(object : Callback<ModelServiceComponets> {
            override fun onFailure(call: Call<ModelServiceComponets>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                val msg = MessageUtils.showFailureToast(t!!.message)
                MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, msg)

            }

            override fun onResponse(call: Call<ModelServiceComponets>, response: Response<ModelServiceComponets>) {
                MessageUtils.dismissDialog(dialog)
                if (response?.isSuccessful) {


                    var models = response.body()
                } else {
                    val msg = MessageUtils.showErrorCodeToast(response?.code())
                    MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, msg)

                }
            }
        })
    }

    /* private fun onLoadAsynTask(dateStr: String, timeStr: String, taskStr: String, remarksStr: String) {
         var dialog: Dialog? = null
         var result: String = ""

       */
    class AddNextTask(private val crmActivity: CRMDetailsActivity, var dateStr: String, var timeStr: String, var taskStr: String, var remarksStr: String, var enquiry_status: String
                      , var outputFileUri: String, var chassisNumberStr: String, var oldSerialNo: String, var newSerialNo: String) : AsyncTask<String, Void, String>() {

        //var dialog: Dialog? = null
        var result: String = ""


        companion object {
            var refreshViews: RefreshViews? = null

            fun refresh(refreshViews: RefreshViews) {
                this.refreshViews = refreshViews
            }
        }

        override fun onPreExecute() {
            super.onPreExecute()
            if (dialog != null) {
                if (dialog!!.isShowing) {
                    dialog!!.dismiss()
                }
                dialog = MessageUtils.showDialog(crmActivity)
            }

            dialog?.setOnKeyListener { dialogInterface, _, keyEvent ->
                dialogInterface.toString()
                addNextTask?.cancel(true)
                false
            }
        }


        override fun doInBackground(vararg p0: String?): String {
            val url = RetrofitCall.BASE_URL + "updatetask"
            Log.d("vdsfdfd", "" + url)
            val httpclient = DefaultHttpClient()
            val httppost = HttpPost("" + url)
            val multipartEntity = MultipartEntity(HttpMultipartMode.STRICT)
            val localContext = BasicHttpContext()
            try {
                // Log.d("imageFilePath", "" + imageFilePath)

                multipartEntity.addPart("user_name", StringBody(LocalSession.getUserInfo(crmActivity, LocalSession.KEY_USER_NAME)))
                multipartEntity.addPart("next_activity_date", StringBody(dateStr))
                multipartEntity.addPart("next_activity_time", StringBody(timeStr))
                multipartEntity.addPart("enquiry_status", StringBody(enquiry_status))
                multipartEntity.addPart("remarks", StringBody(remarksStr))
                multipartEntity.addPart("enquiry_id", StringBody(enquiryId))
                multipartEntity.addPart("nexttask", StringBody(taskStr))
                multipartEntity.addPart("enquirytaskid", StringBody(childId))

                multipartEntity.addPart("chassis_number", StringBody(chassisNumberStr))
                multipartEntity.addPart("old_serial_number", StringBody(oldSerialNo))
                multipartEntity.addPart("new_serial_number", StringBody(newSerialNo))

                Log.d("enquiry_status", "" + enquiry_status)
                if (enquiry_status.equals("Closure")) {
                    val enquiry = mEnqquiry_for?.text.toString()
                    if (enquiry.equals("Service")) {
                        outputFileUri = outputFileUri.replace("file:///", "/")
                        Log.d("outputFileUri_replace", "" + outputFileUri)
                        multipartEntity.addPart("upload", FileBody(File(outputFileUri)))
                        multipartEntity.addPart("imagename", StringBody("http://13.232.130.235:8000/public/uploads/" + File(outputFileUri).getName()))
                        multipartEntity.addPart("imagename", StringBody("http://13.232.130.235:8000/public/uploads/" + File(outputFileUri).getName()))
                    }
                }
                val totalSize = multipartEntity.getContentLength()
                Log.d("totalSize", "" + totalSize)
                httppost.setEntity(multipartEntity)
                val response = httpclient.execute(httppost, localContext)
                val r_entity = response.getEntity()
                val inputStream = r_entity.getContent()
                result = convertStreamToString(inputStream)
                Log.d("resultsdsd", "" + result)
                return result


            } catch (e: UnsupportedEncodingException) {
                return "fileNotFound"
            } catch (e: FileNotFoundException) {
                // MessageUtils.showToastMessage(crmActivity, e.message)
                e.printStackTrace()
            }

            return result
        }

        @SuppressLint("SetTextI18n")
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            MessageUtils.dismissDialog(dialog)

            try {
                File(outputFileUri).delete()
                val gson = Gson()
                val models = gson.fromJson(result, TaskDetailsModel::class.java)

                if (models.success) {
                    if (models.data != null) {
                        crmActivity.setList(models)
                        if (refreshViews != null) {
                            refreshViews?.refresh(models)
                        }

                        Log.d("task_details ", "enquiry_status " + enquiry_status + "  " + taskStr + "  task_name  " + task_name)

                        if (task_name.equals(crmActivity.getString(R.string.sales))) {
                            if (enquiry_status.equals(crmActivity.getString(R.string.closure))) {
                                if (taskStr.equals("Win")) {

                                    val dialogInvoice = Dialog(crmActivity)
                                    dialogInvoice.setContentView(R.layout.dialog_invoice)
                                    dialogInvoice.show()
                                    dialogInvoice.setCancelable(false)

                                    var vehicle_model: CustomTextEditView = dialogInvoice.findViewById(R.id.vehicle_model)
                                    var model_varent: CustomTextEditView = dialogInvoice.model_varent
                                    val chassis_number: CustomTextEditView = dialogInvoice.chassis_number
                                    val invoice_amt: CustomTextEditView = dialogInvoice.invoice_amt
                                    val customer_name: CustomTextEditView = dialogInvoice.customer_name
                                    val customer_address: CustomTextEditView = dialogInvoice.customer_address
                                    val customer_proof: CustomTextEditView = dialogInvoice.customer_proof
                                    val customer_address_proof: CustomTextEditView = dialogInvoice.customer_address_proof
                                    val customer_email_id: CustomTextEditView = dialogInvoice.customer_email_id
                                    val customer_mobile_no: CustomTextEditView = dialogInvoice.customer_mobile_no
                                    val invoice_cancel: CustomButton = dialogInvoice.invoice_cancel
                                    val invoice_submit: CustomButton = dialogInvoice.invoice_submit
                                    val mSnackView: NestedScrollView = dialogInvoice.mSnackView
                                    val model_color: CustomTextEditView = dialogInvoice.vehicle_model_color


                                    val strVehicle: String = mVehicleModel!!.text.toString().trim()
                                    val strEmail: String = mEmailId?.text.toString()
                                    val strName: String = mName?.text.toString()
                                    val mobileNo: String = mMobileNo?.text.toString().trim()
                                    val modelColor: String = model_color.text.toString().trim()
                                    val strValues: String = mValues?.text.toString().trim().replace("₹ ", "")

                                    Log.d("strVehicle", "strVehicle  " + strVehicle + " strEmail " + strEmail +
                                            "  strName  " + strName + " mobileNo " + mobileNo + " strValues " + strValues)
                                    vehicle_model.setText(strVehicle)
                                    customer_email_id.setText(strEmail)
                                    customer_name.setText(strName)
                                    customer_mobile_no.setText(mobileNo)
                                    invoice_amt.setText(strValues)

                                    invoice_submit.setOnClickListener {
                                        var vehileModel: String = vehicle_model.text.toString().trim()
                                        val modelVarent: String = model_varent.text.toString().trim()
                                        val chassisNumber: String = chassis_number.text.toString().trim()
                                        val invoiceAmt: String = invoice_amt.text.toString().trim()
                                        val custmerName: String = customer_name.text.toString().trim()
                                        val customerMobileNumber: String = customer_mobile_no.text.toString().trim()
                                        val customerEmail: String = customer_email_id.text.toString().trim()
                                        val customerAdd: String = customer_address.text.toString().trim()
                                        val customerProof: String = customer_proof.text.toString().trim()
                                        val customerAddProof: String = customer_address_proof.text.toString().trim()

                                        if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, vehileModel, "Vehicle Model can't be empty")) {
                                            if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, modelVarent, "Model Vareint can't be empty")) {
                                                if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, chassisNumber, "Chassis Number can't be empty")) {
                                                    if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, modelColor, "Model Color can't be empty")) {

                                                        if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, invoiceAmt, "Invoice Amount can't be empty")) {

                                                            if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, custmerName, "Customer Name can't be empty")) {
                                                                if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, customerMobileNumber, "Customer Mmobile Number can't be empty")) {
                                                                    if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, customerEmail, "Customer Email Id can't be empty")) {
                                                                        if (Pattern.matches(pattern, customerEmail)) {
                                                                            if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, customerAdd, "Customer Address can't be empty")) {

                                                                                if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, customerProof, "Customer Proof can't be empty")) {
                                                                                    if (!MyUtils.isEmptySnackView(crmActivity, mSnackView, customerAddProof, "Customer Address Proof can't be empty")) {
                                                                                        val map = HashMap<String, String>()
                                                                                        map.put("vehicle_model", vehileModel)
                                                                                        map.put("model_varient", modelVarent)
                                                                                        map.put("chassis_no", chassisNumber)
                                                                                        map.put("invoice_amount", invoiceAmt)
                                                                                        map.put("customer_name", custmerName)
                                                                                        map.put("mobile", customerMobileNumber)
                                                                                        map.put("email_id", customerEmail)
                                                                                        map.put("customer_address", customerAdd)
                                                                                        map.put("id_proof", customerProof)
                                                                                        map.put("address_proof", customerAddProof)
                                                                                        map.put("enquiry_id", enquiryId)
                                                                                        map.put("modelColor", modelColor)
                                                                                        map.put("username", LocalSession.getUserInfo(crmActivity, LocalSession.KEY_ID))

                                                                                        onAddInVoice(map, dialogInvoice)

                                                                                    }
                                                                                }
                                                                            }
                                                                        } else {
                                                                            MessageUtils.showSnackBar(crmActivity, mSnackView, "Enter valid email id")
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    invoice_cancel.setOnClickListener {

                                        dialogInvoice.dismiss()
                                    }


                                }
                            }
                        }
                    }
                }


            } catch (e: NullPointerException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        private fun onAddInVoice(map: HashMap<String, String>, dialogInvoice: Dialog) {
            val addInvoice: Call<AddInvoice> = MyUtils.getRetroService().onAddInvoice(BEARER + LocalSession.getUserInfo(crmActivity, KEY_TOKEN), map)
            addInvoice.enqueue(object : Callback<AddInvoice> {
                override fun onResponse(call: Call<AddInvoice>?, response: Response<AddInvoice>?) {
                    if (response != null) {
                        dialogInvoice.dismiss()
                        if (response.isSuccessful) {
                            val addInvoic: AddInvoice = response.body() as AddInvoice
                            Log.d("Info", " on submit function")
                            MessageUtils.showToastMessage(crmActivity, addInvoic!!.message)
                            val bundle = Bundle()
                            val fragment = ViewEventDetailed()
                            fragment.arguments = bundle
                            val intent = Intent(crmActivity, CRMActivity::class.java)
                            /*intent.putExtra("id", "" + enquiryId)
                            intent.putExtra("user_name", "" + items[position].customerName)
                            intent.putExtra("task_name", "" + context.getString(R.string.sales))*/
                            ContextCompat.startActivity(crmActivity, intent, bundle)
                            crmActivity.overridePendingTransition(0, 0)
                            crmActivity.finish()
                        } else {
                            val msg: String = MessageUtils.showErrorCodeToast(response.code())
                            MessageUtils.showToastMessage(crmActivity, msg)
                        }
                    }
                }

                override fun onFailure(call: Call<AddInvoice>?, t: Throwable?) {
                    dialogInvoice.dismiss()
                    val msg: String = MessageUtils.showFailureToast(t?.message)
                    MessageUtils.showToastMessage(crmActivity, msg)
                }
            })
        }

    }
    //}


    private fun onAddNextTask(part: MultipartBody.Part, next_activity_date: RequestBody, next_activity_time: RequestBody, enquiry_status: RequestBody, remarks: RequestBody, user_name: RequestBody, enquiry_id: RequestBody, enquirytaskid: RequestBody, nexttask: RequestBody, imageName: RequestBody) {

        val model: Call<TaskDetailsModel> = retrofit.onAddNextTask(BEARER + LocalSession.getUserInfo(this, KEY_TOKEN), part, next_activity_date, next_activity_time, enquiry_status, remarks, user_name, enquiry_id, enquirytaskid, nexttask, imageName)
        dialog = MessageUtils.showDialog(this)
        dialog?.setOnKeyListener { dialogInterface, _, keyEvent ->
            dialogInterface.toString()
            model.cancel()
            false
        }

        model.enqueue(object : Callback<TaskDetailsModel> {
            override fun onResponse(call: Call<TaskDetailsModel>, response: Response<TaskDetailsModel>) {
                MessageUtils.dismissDialog(dialog)
                if (response.isSuccessful) {
                    val valuesOfModel = response.body()
                    if (valuesOfModel != null) {
                        if (valuesOfModel.success) {
                            if (valuesOfModel.data.enquirydetails.size == 0) {
                                task_no_data.visibility = View.VISIBLE
                                ev_lis_view.visibility = View.GONE
                            } else {
                                task_no_data.visibility = View.GONE
                                ev_lis_view.visibility = View.VISIBLE


                                ev_lis_view.adapter = ItemForTaskUpdate(this@CRMDetailsActivity, bottomSheetBehavior as BottomSheetBehavior<*>, valuesOfModel.data.enquirydetails as MutableList<EnquiryDetails>, "")
                                MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, valuesOfModel.message)
                            }
                        } else {
                            MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, valuesOfModel.message)
                        }
                    } else {
                        MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, getString(R.string.api_response_not_found))
                    }

                } else {
                    MessageUtils.setErrorKotlin(this@CRMDetailsActivity, snack_view, response.code())
                }
            }

            override fun onFailure(call: Call<TaskDetailsModel>, t: Throwable) {
                MessageUtils.dismissDialog(dialog)
                MessageUtils.setFailureKotlin(this@CRMDetailsActivity, snack_view, t.message)
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun onTaskLoading(taskModel: Call<TaskDetailsModel>) {
        if (NetworkTester.isNetworkAvailable(this@CRMDetailsActivity)) {
            dialog = MessageUtils.showDialog(this@CRMDetailsActivity)
            dialog?.setOnKeyListener { dialogInterface, _, keyEvent ->
                dialogInterface.dismiss()
                taskModel.cancel()
                finish()
                false
            }

            taskModel.clone().enqueue(object : Callback<TaskDetailsModel> {
                @RequiresApi(Build.VERSION_CODES.M)
                override fun onResponse(call: Call<TaskDetailsModel>, response: Response<TaskDetailsModel>) {
                    MessageUtils.dismissDialog(dialog)
                    if (response.isSuccessful) {
                        models = response.body()
                        Log.d("ResponseArray",""+models.toString())

                        /**
                         * new Requirement like new spare adding functionality
                         * so..we need to set  old serialnumber component and new serial Number component
                         * call set to spare component function
                         **/
                        SetServicesTypeArrays(response.body()?.data)
                        viewRefresh(models)
                        map?.put("chassis", response.body()?.data?.enquiry?.chassisNo.toString())
                    } else {
                        val msg: String = showErrorCodeToast(response.code())
                        txt_no_data_task.text = msg
                        all_view_view_event.visibility = View.GONE
                        add_new_task.visibility = View.GONE
                        appbar_view.visibility = View.GONE
                        txt_no_data_task.visibility = View.VISIBLE
                        MessageUtils.setErrorKotlin(this@CRMDetailsActivity, mSnacView, response.code())

                    }
                }

                override fun onFailure(call: Call<TaskDetailsModel>, t: Throwable) {
                    MessageUtils.dismissDialog(dialog)
                    Log.d(" Failure Error", "" + t.message)
                    val msg: String = showFailureToast(t.message)
                    txt_no_data_task.text = msg
                    all_view_view_event.visibility = View.GONE
                    appbar_view.visibility = View.GONE
                    add_new_task.visibility = View.GONE
                    txt_no_data_task.visibility = View.VISIBLE
                    //MessageUtils.setFailureKotlin(this@CRMDetailsActivity, mSnacView, t.message)
                }
            })
        } else {
            MessageUtils.snackActionSetting(this@CRMDetailsActivity, mSnacView, getString(R.string.check_internet))
        }
    }

    @SuppressLint("SetTextI18n")
    fun SetServicesTypeArrays(data: Data?) {
        try{
            /****New Replacment  Service Enquiry Array List**/

            newReplacementAdapter =NewReplacementAdapter(this,data?.replacementList,this)
            bs_recy_new_replacement.layoutManager = LinearLayoutManager(this,LinearLayout.VERTICAL,false)
            bs_recy_new_replacement.adapter = newReplacementAdapter
            bs_edt_count .setText("Count Of Service : "+data?.replacementList?.size)
            if(data?.replacementList?.size!! <= 0){
                bs_recy_new_replacement.visibility = View.GONE
                bs_rl_new_replacement.visibility = View.GONE
            }else{
                bs_rl_new_replacement.setOnClickListener {
                    if(bs_recy_new_replacement.visibility == View.VISIBLE){
                        bs_recy_new_replacement.visibility = View.GONE
                    }else{
                        bs_recy_new_replacement.visibility = View.VISIBLE
                    }
                }
            }
            Log.d("newReplacement","componentName "+ newReplacementServerLISt.size)
            /*       Log.d("newReplacement","componentName"+ newReplacementServerLISt[0].oldserial)
                   Log.d("newReplacement","componentName"+ newReplacementServerLISt[0].newSerialNo)*/

            /***** Service Warranty Repalcement   ArrayList ****/
            serviceReplacementAdapter =ServiceReplacementAdapter(this,data?.servicereplacement,this)
            bs_recy_service_replacement.layoutManager = LinearLayoutManager(this,LinearLayout.VERTICAL,false)
            bs_recy_service_replacement.adapter = serviceReplacementAdapter
            Log.d("newReplacementSize",""+data?.servicereplacement?.size)
            bs_edt_scount .setText("Count Of Service : "+data?.servicereplacement?.size)
            if(data?.servicereplacement?.size!! <= 0){
                Log.d("ArrrayListisEmpty",""+ data.replacementList?.size)
                bs_recy_service_replacement.visibility = View.GONE
                bs_rl_service_replacement.visibility = View.GONE
            }else{

                bs_rl_service_replacement.setOnClickListener {
                    if(bs_recy_service_replacement.visibility == View.VISIBLE){
                        bs_recy_service_replacement.visibility = View.GONE
                    }else{
                        bs_recy_service_replacement.visibility = View.VISIBLE
                    }
                }
            }
            /***paid Replacement Service***/
            paidReplacementAdapter =PaidReplacementAdapter(enquiryId,data?.enquiry?.chassisNo.toString().toString(),this,data?.paidcomponents,this)
            bs_recy_paid_replacement.layoutManager = LinearLayoutManager(this,LinearLayout.VERTICAL,false)
            bs_recy_paid_replacement.adapter = paidReplacementAdapter
            Log.d("newReplacementSize",""+data?.servicereplacement?.size)
            bs_edt_pcount .setText("Component size : "+data?.paidcomponents?.size)
            if(data?.paidcomponents?.size!! <= 0){
                Log.d("ArrrayListisEmpty",""+ data.paidcomponents?.size)
                bs_recy_paid_replacement.visibility = View.GONE
                bs_rl_paid_replacement.visibility = View.GONE
            }else{

                bs_rl_paid_replacement.setOnClickListener {
                    if(bs_recy_paid_replacement.visibility == View.VISIBLE){
                        bs_recy_paid_replacement.visibility = View.GONE
                    }else{
                        bs_recy_paid_replacement.visibility = View.VISIBLE
                    }
                }
            }
            /***Free  Replacement Service***/
            freeServiceReplacementAdapter =FreeServiceReplacementAdapter(this,data?.freereplacementArrayList,this)
            bs_recy_free_replacement.layoutManager = LinearLayoutManager(this,LinearLayout.VERTICAL,false)
            bs_recy_free_replacement.adapter = freeServiceReplacementAdapter
            Log.d("newReplacementSize",""+data?.freereplacementArrayList?.size)
            bs_edt_fcount .setText("Count Of Service : "+data?.freereplacementArrayList?.size)
            if(data?.freereplacementArrayList?.size!! <= 0){
                Log.d("ArrrayListisEmpty",""+ data.freereplacementArrayList?.size)
                bs_recy_free_replacement.visibility = View.GONE
                bs_rl_free_replacement.visibility = View.GONE
            }else{

                bs_rl_free_replacement.setOnClickListener {
                    if(bs_recy_free_replacement.visibility == View.VISIBLE){
                        bs_recy_free_replacement.visibility = View.GONE
                    }else{
                        bs_recy_free_replacement.visibility = View.VISIBLE
                    }
                }
            }
            /***SpareService Replacement**/
            spareServiceReplacementAdapter =SpareServiceReplacementAdapter(this,data?.spareservicereplaementArrayList,this)
            bs_recy_spare_service_replacement.layoutManager = LinearLayoutManager(this,LinearLayout.VERTICAL,false)
            bs_recy_spare_service_replacement.adapter = spareServiceReplacementAdapter
            Log.d("spareSErvice",""+data?.spareservicereplaementArrayList?.size)
            bs_edt_sscount .setText("Count Of Service : "+data?.spareservicereplaementArrayList?.size)
            if(data?.spareservicereplaementArrayList?.size!! <= 0){
                Log.d("ArrrayListisEmpty",""+ data.spareservicereplaementArrayList?.size)
                bs_recy_spare_service_replacement.visibility = View.GONE
                bs_ll_spare_service_replacement.visibility = View.GONE
            }else{

                bs_ll_spare_service_replacement.visibility = View.VISIBLE
                bs_ll_spare_service_replacement.setOnClickListener {
                    if(bs_recy_spare_service_replacement.visibility == View.VISIBLE){
                        bs_recy_spare_service_replacement.visibility = View.GONE
                    }else{
                        bs_recy_spare_service_replacement.visibility = View.VISIBLE
                    }
                }
            }

            /*  Log.d("dataViewEnqury",""+data?.replacementList?.get(0)?.component)
              if (!data?.replacementList!!.get(0).isValid) {
                  new_component_replacement.visibility = View.GONE
              } else {
                  new_component_replacement.visibility = View.VISIBLE
                  if (data?.replacementList?.get(0)?.component != null) {
                      bs_edt_component_name.setText(data?.replacementList?.get(0)?.component)
                  } else {
                      bs_edt_component_name.setText("Data Not Available")
                  }
                  if (data?.replacementList?.get(0)?.oldserial != null) {
                      bs_edt_old_serial_number.setText(data?.replacementList?.get(0)?.oldserial)
                  } else {
                      bs_edt_old_serial_number.setText("Data Not Available")
                  }
                  val stocklist = data?.replacementList?.get(0)?.sparesstock
                  if (stocklist != null) {
                      for (sparelist in stocklist!!) {
                          if (sparelist?.serialNo.equals(bs_edt_component_name?.text.toString())) {
                              Log.d("Serial Number", "" + sparelist?.serialNo)
                          } else {
                              newSerialNoList?.add(sparelist.serialNo)
                          }

                      }
                  }
              }
              *//**
             * Paid componet  add more fuctionality.
             *
             *//*

              if (data.paidcomponents != null) {
                  Log.d("paidcompnebnt",""+data.paidcomponents?.get(0)?.component)
                  for (componet in data.paidcomponents!!) {
                      compontList?.add(componet?.component!!)
                      Log.d("componentName",""+componet?.component)
                  }
              }
              compontListAdapter = ArrayAdapter<String>(this@CRMDetailsActivity, R.layout.spinner_dialog, R.id.text2, compontList)
              newSerialNoList?.add(0, "Select New Serial No")
              newSerialNoListAdapter = ArrayAdapter<String>(this@CRMDetailsActivity, R.layout.spinner_dialog, R.id.text2, newSerialNoList)
              *//**** Call  dynamicAddingSpareFn() for Paid Service
             * Replacement
             *//*
              dynamicAddingSpareFn()
              *//**
             * set into the OLD Component Replacement...its Help  to Component  Name  serial Number are Same
             * Like A Warranty Service replacement
             *//*
              println(" SpareInvoicePojos the Data from  pojosClass" + data.servicereplacement[0].component)
              if (!data.servicereplacement!!.get(0)?.isValid!!) {
                  warranty_service_replacement.visibility = View.GONE
              } else {
                  warranty_service_replacement.visibility = View.VISIBLE
                  if (data.servicereplacement.get(0)?.component == null || data.servicereplacement.get(0)?.component.toString().isEmpty() || data.servicereplacement.get(0)?.component.toString().length == 0) {
                      ws_edt_component_name.setText("Data Not Available")
                  } else {
                      ws_edt_component_name.setText(data.servicereplacement.get(0)?.component)
                  }
                  if (data.servicereplacement.get(0)?.oldserial == null || data.servicereplacement.get(0)?.oldserial.toString().isEmpty() || data.servicereplacement.get(0)?.oldserial.toString().length == 0) {
                      wrs_edt_old_serial_number.setText("Data Not Available")
                  } else {
                      wrs_edt_old_serial_number.setText(data.servicereplacement.get(0)?.oldserial)
                  }

                  if (data.servicereplacement.get(0)?.sparesstock == null || data.servicereplacement.get(0)?.sparesstock.toString().isEmpty() || data.servicereplacement.get(0)?.sparesstock.toString().isEmpty()) {
                      wss_edt_new_serial_number.setText("Data Not Available")
                  } else {
                      wss_edt_new_serial_number.setText(data.servicereplacement.get(0)?.sparesstock)
                  }
              }

              *//****
             * The  Free  Service   Replacement Data
             *//*
              Log.d("FreeReplacement",""+data?.freereplacementArrayList?.get(0)?.component)
              if (!data.freereplacementArrayList!!.get(0)?.isValid!!) {
                  ll_addmore_free.visibility = View.GONE
              } else {
                  ll_addmore_free.visibility = View.VISIBLE
                  if (data.freereplacementArrayList.get(0)?.component == null || data.freereplacementArrayList.get(0)?.component.toString().isEmpty() || data.freereplacementArrayList.get(0)?.component.toString().length == 0) {
                      bs_edt_component_name_addmore_free.setText("Data Not Available")
                  } else {
                      bs_edt_component_name_addmore_free.setText(data.freereplacementArrayList.get(0)?.component)
                  }
                  if (data.freereplacementArrayList.get(0)?.oldserial == null || data.freereplacementArrayList.get(0)?.oldserial.toString().isEmpty() || data.freereplacementArrayList.get(0)?.oldserial.toString().length == 0) {
                      bs_edt_old_serial_number_addmore_free.setText("Data Not Available")
                  } else {
                      bs_edt_old_serial_number_addmore_free.setText(data.freereplacementArrayList.get(0)?.oldserial)
                  }
//                  bs_edt_old_serial_number_addmore_free
                  if (data.freereplacementArrayList.get(0)?.sparesstock == null ||
                          data.freereplacementArrayList.get(0)?.sparesstock.toString().isEmpty() ||
                          data.freereplacementArrayList.get(0)?.sparesstock.toString().isEmpty()) {
                      freereplacemntSTRList.add(0,getString(R.string.no_data_avl))
                  } else {
                      for (freeReplacementSpareStock  in data.freereplacementArrayList[0].sparesstock!!){
                          freereplacemntSTRList.add(freeReplacementSpareStock?.serialNo!!)
                      }
                  }
                  freereplacementSTRadapter =  ArrayAdapter(this,R.layout.spinnertext,R.id.text1,freereplacemntSTRList)
                  bs_sp_new_serial_number_addmore_free .adapter=  freereplacementSTRadapter
              }*/
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    @SuppressLint("SetTextI18n")
    private fun viewRefresh(models: TaskDetailsModel?) {
        if (models != null) {

            if (models != null) {
                if (models!!.success) {
                    if (models!!.data != null) {
                        if (models!!.data.enquiry != null) {
                            try {
                                all_view_view_event.visibility = View.VISIBLE
                                appbar_view.visibility = View.VISIBLE
                                txt_no_data_task.visibility = View.GONE

                                ItemForTaskUpdate.init(this)

                                if (MyUtils.nullPointerValidation(models.data.enquiry.message).equals("-")) {
                                    ev_details_message.visibility = View.GONE
                                } else {
                                    ev_details_message.visibility = View.VISIBLE
                                }

                                if (MyUtils.nullPointerValidation(models.data.enquiry.sparesName).equals("-")) {
                                    ev_details_spares.visibility = View.GONE
                                } else {
                                    ev_details_spares.visibility = View.VISIBLE
                                }

                                if (MyUtils.nullPointerValidation(models.data.enquiry.task).equals("-")) {
                                    ev_details_task.visibility = View.GONE
                                } else {
                                    ev_details_task.visibility = View.VISIBLE
                                }

                                mName?.text = "  " + models!!.data.enquiry.customerName
                                CustomTextView.marquee(mName)
                                ev_details_city.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.city)
                                ev_details_vehicle_model.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.vehicleModel)
                                ev_details_chassis_no.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.chassisNo)
                                mEmailId?.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.email)
                                mValues?.text = "  " + getString(R.string.inr) + " " + MyUtils.nullPointerValidation(models!!.data.enquiry.invoiceValue)
                                CustomTextView.marquee(ev_details_task)
                                CustomTextView.marquee(mEmailId)
                                CustomTextView.marquee(ev_details_message)

                                mMobileNo?.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.mobileNo)
                                ev_details_spares.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.sparesName)
                                ev_lead_source.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.leadsource)
                                ev_details_task.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.task)
                                ev_details_message.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.message)
                                ev_task_details.text = "  " + MyUtils.nullPointerValidation(models!!.data.enquiry.taskStatus)

                                mEnqquiry_for?.text = models!!.data.enquiry.enquiryFor
                                if (models.data.enquiry.enquiryDate != null) {

                                    try {
                                        ev_details_enquiry_date.text = "  " + DateUtils.convertDates(models!!.data.enquiry.enquiryDate, DateUtils.CURRENT_FORMAT, DateUtils.DD_MM) + "  " + DateUtils.convertDates(models!!.data.enquiry.enquiryTime, "HH:mm:ss", "hh:mm a")
                                    } catch (e: RuntimeException) {
                                        e.printStackTrace()
                                    }
                                }
                                if (models.data.enquiry.assignedDate != null) {

                                    ev_details_enquiry_assigned_date.text = "  " + DateUtils.convertDates(models!!.data.enquiry.assignedDate, DateUtils.CURRENT_FORMAT + " HH:mm:ss", DateUtils.DD_MM + " hh:mm a")
                                }

                                val taskItem: List<EnquiryDetails> = models.data.enquirydetails as List<EnquiryDetails>
                                if (taskItem.size == 0) {
                                    task_no_data.visibility = View.VISIBLE
                                    ev_lis_view.visibility = View.GONE
                                } else {
                                    task_no_data.visibility = View.GONE
                                    ev_lis_view.visibility = View.VISIBLE
                                    ev_lis_view.adapter = ItemForTaskUpdate(this@CRMDetailsActivity, bottomSheetBehavior as BottomSheetBehavior<*>, taskItem as MutableList<EnquiryDetails>, MyUtils.nullPointerValidation(models.data.enquiry.taskStatus))
                                }

                                val taskArray = ArrayList<String>()
                                for (task in models!!.data.tasks) {
                                    taskArray.add(task.task)
                                }

                                bs_task_spinner?.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, android.R.layout.simple_list_item_1, android.R.id.text1, taskArray)
                                //bs_task_spinner?.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, taskArray)
                                statusValues.clear()

                                //bs_status_spinner.adapter = ItemBottomSheetStatusUpdate(this@CRMDetailsActivity, models!!.data.enquirystatuslist)
                                /*** Enquiry Status  Not Showns  to Previous status**/
                                for (enquiry_list in models.data.enquirystatuslist) {
                                    Log.d("DebugEnquiryStatus", "" + enquiry_list.status + "\t" + enquiry_list.id)
                                    val previousStatus = models.data.enquiry.taskStatus!!.equals(enquiry_list.statusHint)
                                    Log.d("DebugEnquiryStatus", "" + models.data.enquiry.taskStatus!! + "\t" + enquiry_list.statusHint)
                                    if (previousStatus) {
                                        Log.d("DebugEnquiryStatus", "" + previousStatus)
                                        currentStateStatus = enquiry_list.id
                                        Log.d("CurrentStateStatus", "\t" + currentStateStatus)
                                    }
                                }
                                for (enquiry_list in models.data.enquirystatuslist) {
                                    if(currentStateStatus <= enquiry_list.id){
                                        statusValues.add(enquiry_list.status)
                                        Log.d("DebugEnquiryStatus",""+enquiry_list.status + "\t"+enquiry_list.id)
                                    }
                                    enquiryStatusNewTask?.put(enquiry_list.status.toString(), enquiry_list.statusHint.toString())
                                }
                                /**USing Lamdaa Functions***/

                                /*        models.data.enquiryStatusList.fold(0,{
                                            acc:Int,i:Int->
                                            val postionStatusPosition :Int
                                            if(models.data.enquiryStatusList[i].status.equals(models.data.enquiry.taskStatus)){
                                                postionStatusPosition =i
                                            }else{

                                            }
                                            i+acc

                                        })*/
                                //This adaper is Next Update Status from Bottomsheet
                                bs_status_spinner.adapter = ArrayAdapter<String>(this@CRMDetailsActivity,
                                        android.R.layout.simple_list_item_1, android.R.id.text1, statusValues)
                                mSwipeRefreshLayout?.setRefreshing(false)

                                Log.d("Enquiry Status viewrefred", "" + models.data.enquiry.taskStatus)
                                if (models.data.enquiry.taskStatus.equals("SS4") || models.data.enquiry.taskStatus.trim().equals("S4")) {
                                    Log.d("Enquiry Status viewrefred", "" + models.data.enquiry.taskStatus)


                                    /*var task = bs_task_spinner.selectedItem.toString()
                                    var status = bs_status_spinner.selectedItem.toString()

                                    bs_status.setText(status)
                                    bs_task.setText(task)
                                    bs_task.visibility = View.VISIBLE
                                    bs_status.visibility = View.VISIBLE

                                    bs_task_spinner.visibility = View.GONE
                                    bs_status_spinner.visibility = View.GONE
                                    layout_action_date.visibility = View.GONE
                                    bs_btn_lay.visibility = View.GONE


                                    bs_status.setTextColor(getServiceType(R.serviceType.black))
                                    bs_task.setTextColor(getServiceType(R.serviceType.black))*/


                                } else {
                                    add_new_task.visibility = View.VISIBLE
                                    layout_action_date.visibility = View.VISIBLE
                                }

                                Log.d("EnquiryFOr",""+models.data.enquiry.enquiryFor)


                                Log.d("modelServiceComponets", "" + models.data?.modelServiceComponets?.size)
                                var componetCount: Int = 0
                                if (models.data?.modelServiceComponets != null) {
                                    Log.d("ServiceComponets",""+models.data.modelServiceComponets.size)
                                    componetCount = models.data?.modelServiceComponets?.size!!
                                    if (componetCount > 0) {
                                        /**
                                         * Service Type Like a Warranty Or Paid
                                         */
                                        val serviceType = arrayOf("Warranty Service","Paid Service")
                                        Log.d("ServiceComponets",""+models.data.modelServiceComponets.size)
                                        serviceComponents = models.data.modelServiceComponets
                                        mSpSeviceComponent?.visibility = View.VISIBLE
                                        mEdtServiceComponent?.visibility =View.VISIBLE
                                        mRLServiceComponent?.visibility = View.VISIBLE
                                        mSpSeviceComponent?.adapter = MyClassAdapter(this@CRMDetailsActivity, serviceComponents)
                                        mSpSeviceType!!.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, R.layout.spinnertext, R.id.text1, serviceType)
                                    } else {
                                        val serviceType = arrayOf("Warranty Service","Paid Service")
                                        mSpSeviceType!!.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, R.layout.spinnertext, R.id.text1, serviceType)
                                        mRLServiceComponent?.visibility = View.GONE
                                        mSpSeviceComponent?.visibility = View.GONE
                                        mEdtServiceComponent?.visibility =View.GONE
                                    }
                                    if( models.data?.enquiry?.taskStatus.equals("S4")){
                                        if(models?.data?.enquiry?.serviceType.equals("Warranty Service")){
                                            val serviceType = arrayOf("Warranty Service")
                                            mSpSeviceType!!.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, R.layout.spinnertext, R.id.text1, serviceType)
                                        }else{
                                            val serviceType = arrayOf("Paid Service")
                                            mSpSeviceType!!.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, R.layout.spinnertext, R.id.text1, serviceType)

                                        }
                                    }

                                }else{
                                    val serviceType = arrayOf("Warranty Service","Paid Service")
                                    mSpSeviceType!!.adapter = ArrayAdapter<String>(this@CRMDetailsActivity, R.layout.spinnertext, R.id.text1, serviceType)
                                    mRLServiceComponent?.visibility = View.GONE
                                    mSpSeviceComponent?.visibility = View.GONE
                                    mEdtServiceComponent?.visibility =View.GONE
                                }
                                /*var adapter = ArrayAdapter<ModelServiceComponets>(this@CRMDetailsActivity, R.layout.spinnertext, R.id.text1, serviceComponents)
                                adapter.setDropDownViewResource(R.layout.spinnertext)
                                mSpSeviceComponent?.adapter = adapter*/

                            } catch (ex: Exception) {
                                ex.printStackTrace()
                                Log.d("exceptionMesg", "" + ex.message)
                            }
                        } else {
                            MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, "Enquiry Details not found")
                        }

                        /**
                         * The Service Enquiry Task  has  root cause requirement only service
                         */
//                    try{
                        try{
                            Log.d("Root cause ",""+models.data.serviceRootCause.toString())
                            if( models.data.serviceRootCause == null || models.data.serviceRootCause.isEmpty() ) {
                                Log.d("Rootcause","The Root Cause model")
                            }else{
                                val rootCauseList:ArrayList<String> =ArrayList()
                                for(rootcause in models.data.serviceRootCause){
                                    rootCauseList.add(rootcause.rootcause)
                                }
                                sp_rootcause.adapter = ArrayAdapter<String>(this@CRMDetailsActivity,R.layout.spinnertext,R.id.text1,rootCauseList)
                            }

                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                        Log.d("what_comes_here", "" + models.data.enquiryStatusList.size + "  sparesList  " + models.data.sparesList.size + "  spareHistory " + models.data.spareHistory.size)
                        if (models.data.sparesList.size != 0) {
                            mSpareListView!!.visibility = View.VISIBLE
                            mSpareList!!.adapter = ItemSpares(this@CRMDetailsActivity, models.data.sparesList)
                        } else {
                            mSpareListView!!.visibility = View.GONE
                        }
                        /**Spare History  List Set To Adapter**/
                        if(models.data.spareHistory.size <=0){
                            Log.d("SpareHistory List",""+models.data.spareHistory.size)
                            llSpareHistory?.visibility = View.GONE
                        }else{
                            if(task_name.equals(getString(R.string.service)) || task_name.equals(getString(R.string.spare_service))){
                                llSpareHistory?.visibility =View.VISIBLE
                                spareHistoryadpter = SpareHistorCRMAdapter(this@CRMDetailsActivity,models.data.spareHistory,this)
                                recySpareHistory?.adapter =spareHistoryadpter
                            }else{
                                llSpareHistory?.visibility =View.GONE
                            }
                        }
                        if (models.data.enquiryStatusList.size != 0) {
                            mEnquiryStatusView!!.visibility = View.VISIBLE
                            Log.d("enquiry_details_before", "" + models.data.enquiryStatusList.size)

                            /*modelTasks.data.enquiryStatusList.addAll(modelTasks.data.enquiryStatusList)
                            Log.d("enquiry_details_mi", "" + modelTasks.data.enquiryStatusList.size)
                            modelTasks.data.enquiryStatusList.addAll(modelTasks.data.enquiryStatusList)
                            Log.d("enquiry_details_after", "" + modelTasks.data.enquiryStatusList.size)*/



                            mEnquiryStatusList!!.adapter = ItemEnquiry(this@CRMDetailsActivity, models.data.enquiryStatusList)
                            /*mSpareListView!!.visibility = View.VISIBLE
                            mSpareList!!.adapter = ItemEnquiry(this@CRMDetailsActivity, modelTasks.data.enquiryStatusList)*/
                        } else {
                            mEnquiryStatusView!!.visibility = View.GONE
                        }


                        try{
                            if(models.data.enquiry.invoiceValue.toString().isEmpty()){
                                bs_edt_service_value.setText("")
                            }else{
                                bs_edt_service_value.setText(models.data.enquiry.invoiceValue)
                            }
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }



                    } else {
                        MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, getString(R.string.api_response_not_found))
                    }
                } else {
                    MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, models!!.message)
                }
            }
        } else {
            MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView, getString(R.string.api_response_not_found))
        }
    }


    private fun showDateDailog(): String {
        var date = ""
        val datePickerDialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { datePicker, selectedYear, selectedMonth, selectedDate ->
            year = selectedYear
            month = selectedMonth
            day = selectedDate
            val dateFormatter = SimpleDateFormat(DateUtils.DD_MM_YYYY, Locale.US)
            val newDate = Calendar.getInstance()
            newDate.set(year, month, day)

            date = dateFormatter.format(newDate.time)
            bs_date?.setText(date)

            bs_time?.setText("12:00 PM")
            Log.d("daasaste", "" + date)


        }, year, month, day)


        datePickerDialog.datePicker.minDate = System.currentTimeMillis()
        datePickerDialog.window.attributes.windowAnimations = R.style.grow
        datePickerDialog.show()
        Log.d("daasaste", "return  " + date)
        return date
    }

    /*

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
            super.onActivityResult(requestCode, resultCode, data)
    //        Log.d("request Code","resquest code\n"+requestCode+"\n result code"+resultCode)
            try{
                */
/*if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
                try {*//*

                    if (requestCode == TAKE_PHOTO_CODE) {

                        if (resultCode == Activity.RESULT_OK) {

                            if (outputFileUri != null) {
                                var files = File(outputFileUri.toString())
                                Log.d("outputFileUri", "" + File("" + outputFileUri).name)
                                bs_image_name.text = files.name
                                //file:/storage/emulated/0/Pictures/Ampere_Service/280620180548PM.jpg
                                // Get content resolver.
                                var contentResolver: ContentResolver = getContentResolver()
                                // Use the content resolver to open camera taken image input stream through image uri.
                                var inputStream: InputStream = contentResolver.openInputStream(outputFileUri)
                                // Decode the image input stream to a bitmap use BitmapFactory.
                                var pictureBitmap: Bitmap = BitmapFactory.decodeStream(inputStream)
                                // Set the camera taken image bitmap in the image view component to display.
                                take_image.setImageBitmap(pictureBitmap)
                            }
                        }
                    } else if (requestCode == TAKE_CAMERA_CODE) {
                        if (resultCode == Activity.RESULT_OK) {

                            if (outputFileUri != null) {
                                var files = File(outputFileUri.toString())
                                Log.d("outputFileUri", "" + File("" + outputFileUri).name)
                                Log.d("outputFileUri", "" + outputFileUri.toString())
                                tvImageName?.text = files.name

                                // Get content resolver.
                                var contentResolver: ContentResolver = getContentResolver()
                                // Use the content resolver to open camera taken image input stream through image uri.
                                var inputStream: InputStream = contentResolver.openInputStream(outputFileUri)
                                var pictureBitmap: Bitmap = BitmapFactory.decodeStream(inputStream)

                                // Decode the image input stream to a bitmap use BitmapFactory.
                                igtakeImage?.setImageBitmap(pictureBitmap)
                            }

                        }

                    }
               */
/* } catch (e: NullPointerException) {
                    e.printStackTrace()
                }
            }else{
//                onBackPressed()
                MessageUtils.showToastMessage(this,"Capture Not Success")
            }*//*


        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }
*/
    override fun onActivityResult(requestCode: Int, resultCode: Int,
                                  data: Intent?) {
        try{
            if (resultCode == Activity.RESULT_OK  &&   requestCode == TAKE_CAMERA_CODE) {

                Log.d("requset code",""+requestCode+"response code\t"+resultCode)

                if (outputFileUri != null) {
                    val files = File(outputFileUri.toString()+".png")
                    Log.d("outputFileUri", "" + File("" + outputFileUri).name)
                    Log.d("outputFileUri", "" + outputFileUri.toString())
                    tvImageName?.text = files.name

                    // Get content resolver.
                    val contentResolver: ContentResolver = getContentResolver()
                    // Use the content resolver to open camera taken image input stream through image uri.
                    val inputStream: InputStream = contentResolver.openInputStream(outputFileUri)
                    val pictureBitmap: Bitmap = BitmapFactory.decodeStream(inputStream)

                    // Decode the image input stream to a bitmap use BitmapFactory.
                    igtakeImage?.setImageBitmap(pictureBitmap)
                }

            } else if(resultCode == Activity.RESULT_OK  &&   requestCode == 12153) {
                if (spareFileUri != null) {
                    val files = File(spareFileUri.toString() + ".jpg")
                    Log.d("outputFileUri", "" + File("" + spareFileUri).name)
                    Log.d("outputFileUri", "" + spareFileUri.toString())
                    ffRtvImageName?.text = files.name

                    // Get content resolver.
                    val contentResolver: ContentResolver = getContentResolver()
                    // Use the content resolver to open camera taken image input stream through image uri.
                    val inputStream: InputStream = contentResolver.openInputStream(spareFileUri)
                    val pictureBitmap: Bitmap = BitmapFactory.decodeStream(inputStream)

                    // Decode the image input stream to a bitmap use BitmapFactory.
                    ffRigtakeImage?.setImageBitmap(pictureBitmap)
                }
            }
            else if(resultCode == Activity.RESULT_OK  &&   requestCode == 12177) {
                if (spareServiceFileUri != null) {
                    val files = File(spareServiceFileUri.toString()+".jpg" )
                    Log.d("spareServiceFileUri", "" + File("" + spareServiceFileUri).name)
                    Log.d("spareServiceFileUri", "" + spareServiceFileUri.toString())
                    sPSCtvImageName?.text = files.name
                    val contentResolver: ContentResolver = getContentResolver()
                    // Use the content resolver to open camera taken image input stream through image uri.
                    val inputStream: InputStream = contentResolver.openInputStream(spareServiceFileUri)
                    val pictureBitmap: Bitmap = BitmapFactory.decodeStream(inputStream)
                    sPSCigtakeImage?.setImageBitmap(pictureBitmap)
//                sPSCigtakeImage?.setImageBitmap(data?.extras?.get("data") as Bitmap)
//                 sPSCigtakeImage?.setImageURI(uri)
                }
            }
            else {
                Log.d("BackPress button CLicked",""+requestCode)
            }
            super.onActivityResult(requestCode, resultCode, data)

        }catch(e:Exception){
            e.printStackTrace()
        }
    }


    private fun getTimeFromPicker(Enquiry: AppCompatActivity, mTime: CustomTextEditView): TimePickerDialog.OnTimeSetListener {
        return TimePickerDialog.OnTimeSetListener { view, hourOfDay_, minute ->
            var hourOfDay = hourOfDay_

            Log.d("TimefromTimePicker", "$view $hourOfDay  $minute")

            var AM_PM = " AM"
            var mm_precede = ""
            val hourOfDayPlusTwo = hourOfDay + 2
            //if (hourOfDay >= 9 && 22 > hourOfDay) {
            if (hourOfDay >= 12) {
                AM_PM = " PM"
                if (hourOfDay >= 13 && hourOfDay < 24) {
                    hourOfDay -= 12
                } else {
                    hourOfDay = 12
                }
            } else if (hourOfDay == 0) {
                hourOfDay = 12
            }
            if (minute < 10) {
                mm_precede = "0"
            }
            mTime.setText("$hourOfDay:$mm_precede$minute$AM_PM")

        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun setList(valuesOfModel: TaskDetailsModel?) {

        if (valuesOfModel != null) {
            if (valuesOfModel.success) {
                if (valuesOfModel.data.enquirydetails.size == 0) {
                    task_no_data.visibility = View.VISIBLE
                    ev_lis_view.visibility = View.GONE
                } else {
                    task_no_data.visibility = View.GONE
                    ev_lis_view.visibility = View.VISIBLE

                    ItemForTaskUpdate.init(this)


                    ev_lis_view.adapter = ItemForTaskUpdate(this@CRMDetailsActivity, bottomSheetBehavior as BottomSheetBehavior<*>, valuesOfModel.data.enquirydetails as MutableList<EnquiryDetails>, "")
                    bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
                    bs_date?.setText("")
                    bs_time?.setText("")
                    //bs_task?.setText("")
                    bs_remarks?.setText("")
                    MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, valuesOfModel.message)
                }
            } else {
                MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, valuesOfModel.message)
            }
        } else {
            MessageUtils.showSnackBar(this@CRMDetailsActivity, snack_view, getString(R.string.api_response_not_found))
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this@CRMDetailsActivity, Manifest.permission.CAMERA)) {
            Toast.makeText(this@CRMDetailsActivity, "Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show()
        }
        requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), LOCATION_PERMISSION_CODE)
    }

    private fun isReadLocationAllowed(): Boolean {
        val result = ContextCompat.checkSelfPermission(this@CRMDetailsActivity, Manifest.permission.CAMERA)
        val result1 = ContextCompat.checkSelfPermission(this@CRMDetailsActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        //val CALL_PHONE = ContextCompat.checkSelfPermission(this@CRMDetailsActivity, Manifest.permission.READ_EXTERNAL_STORAGE)
        return if (result1 == PackageManager.PERMISSION_GRANTED && result == PackageManager.PERMISSION_GRANTED) {
            return true
        } else false
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        //Checking the request code of our request
        if (requestCode == LOCATION_PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                //Displaying another toast if permission is not granted
            }
        }
    }

    class ItemBottomSheetStatusUpdate(val crmDetailsActivity: CRMDetailsActivity, val enquiryStatusList: MutableList<EnquiryStatusList>) : BaseAdapter() {
        val mInflater: LayoutInflater = LayoutInflater.from(crmDetailsActivity)

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {

            val view: View?
            var textViewItemName: TextView? = null
            if (convertView == null) {
                view = mInflater.inflate(R.layout.item_dialog, parent, false)
                textViewItemName = view!!.findViewById(R.id.text11) as TextView

            } else {
                view = convertView

            }
            textViewItemName!!.setText(enquiryStatusList.get(position).status)
            textViewItemName.setOnClickListener {
                Log.d("aslklskds", "" + enquiryStatusList.get(position).statusHint)

            }

            return view
        }


        override fun getItem(p0: Int): Any {
            return enquiryStatusList.size
        }

        override fun getItemId(p0: Int): Long {
            return 0
        }

        override fun getCount(): Int {
            return enquiryStatusList.size
        }
    }

    class ItemEnquiry(val crmDetailsActivity: CRMDetailsActivity, val enquiryStatusList: MutableList<Data.EnquiryStatus>) : RecyclerView.Adapter<HoldeEnquiry>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HoldeEnquiry {
            Log.d("enquiry_details_two", "" + enquiryStatusList.size)
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_enquiry_status_list, parent, false)
            return HoldeEnquiry(v)
        }

        override fun getItemCount(): Int {
            return enquiryStatusList.size
        }

        override fun onBindViewHolder(holder: HoldeEnquiry, position: Int) {
            try {
                Log.d("compleetedBy", "" + enquiryStatusList[position] + "\n" + enquiryStatusList[position].completed_by + "  at" + enquiryStatusList[position].completed_at + "no")
                holder.itemSiNo.text = "" + (position + 1)
                holder.itemCompletedBy.text = MyUtils.nullPointerValidation(enquiryStatusList[position].completed_by)
                println(enquiryStatusList[position].completed_by)
                holder.itemStatus.text = MyUtils.nullPointerValidation(enquiryStatusList[position].status)
                var dateStr: String = MyUtils.nullPointerValidation(enquiryStatusList[position].completed_at)
                if (dateStr != "-") {
                    dateStr = DateUtils.convertDates(dateStr, "yyyy-MM-dd hh:mm:ss", "dd/MM hh:mm:ss")
                }
                holder.itemCompletedDate.text = dateStr

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    class HoldeEnquiry(view: View) : RecyclerView.ViewHolder(view) {
        val itemCompletedBy = view.item_enquiry_eompleted_by
        val itemStatus = view.item_enquiry_name
        val itemSiNo = view.item_enquiry_si_no
        val itemCompletedDate = view.item_enquiry_completed_date

    }


    class ItemSpares(val crmDetailsActivity: CRMDetailsActivity, val enquiryStatusList: MutableList<Data.Spares>) : RecyclerView.Adapter<HolderSpares>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderSpares {
            Log.d("enquiry_details_two", "" + enquiryStatusList.size)
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_spare_list, parent, false)
            return HolderSpares(v)
        }

        override fun getItemCount(): Int {
            return enquiryStatusList.size
        }

        override fun onBindViewHolder(holder: HolderSpares, position: Int) {
            holder.itemSiNo.text = "" + (position + 1)
            holder.itemSpareName.text = MyUtils.nullPointerValidation(enquiryStatusList[position].spare_name)
            holder.itemCount.text = MyUtils.nullPointerValidation(enquiryStatusList[position].qty)


        }

    }

    class HolderSpares(view: View) : RecyclerView.ViewHolder(view) {
        val itemSpareName = view.item_spare_name
        val itemCount = view.item_spare_count
        val itemSiNo = view.item_spare_si_no


    }


    class MyClassAdapter(val context: FragmentActivity, val items: List<ModelServiceComponets>?) : BaseAdapter() {
        override fun getView(p0: Int, v: View?, p2: ViewGroup?): View {
            val dayView: MyTextView_Lato_Bold
            val vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            var v = vi.inflate(R.layout.item_dialog, null)
            dayView = v!!.findViewById(R.id.text11)
            var id: MyTextView_Lato_Bold = v!!.findViewById(R.id.id_s)
            dayView.text = items!![p0].componentname
            id.text = "" + items!![p0].componentid
            return v
        }

        override fun getItem(p0: Int): Any {
            return p0
        }

        override fun getItemId(p0: Int): Long {
            return 0
        }

        override fun getCount(): Int {
            return items!!.size
        }

    }

    override fun updateIDAndRemarkDetails(enquiryDetailsList: MutableList<EnquiryDetails>) {
        Log.d("enquiryDetailsList", "" + enquiryDetailsList.size)
        this.enquiryDetailsList = enquiryDetailsList
        Log.d("enquiryDetailsList_af", "" + enquiryDetailsList.size)
    }

    override fun apiCallBackOverRideMethod(response: JSONObject?, position: Int, success: Boolean, message: String?, apiCallPath: String?) {
        MessageUtils.dismissDialog(dialog)
        if (apiCallPath.equals("raisecomplaint")) {
            if (success) {

                if (response != null) {
                    /**
                     * if Json Syntax Error Exception   Occurred in our project
                     * please check the SpareInvoicePojos {} object
                     *  array  []
                     *  only its this  issue...
                     *  don't forget   response like  { [ {} ] }  this correct format
                     *
                     */
                    val data = response?.getJSONArray("warrantycomponents")
                    val dat = response?.getJSONArray("enquiry")
                    Log.d("ResponseInterFace",""+data)

//                warrantyPojoList = Gson().fromJson(response["enquiry"].toString(),object : TypeToken<ArrayList<WarrantyComplaintDetailsPojo>>(){}.type)
                    try{
                        warrantyPojoList = Gson().fromJson(data.toString(), object : TypeToken<ArrayList<WarrantycomponentsItem>>() {}.type)
                        warrantyPojoLis = Gson().fromJson(dat.toString(), object : TypeToken<ArrayList<WarrantyComplaintDetailsPojo>>() {}.type)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }

//                    println("waranty Pojo List" + warrantyPojoList?.get(0)?.city)

                    /**
                     * create string  array getting source from  Pojo ArrayList
                     */
                    try{
                        defectitemList?.clear()
                        for (item in this.warrantyPojoList!!) {
                            if(item.componentname!=null){
                                defectitemList?.add(item.componentname)
                            }
                        }
                        spDefectItem?.setSelection(defectitemList?.indexOf(warrantyPojoLis?.get(0)?.warrantyComponent)!!)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }

                    try{
                        if(defectitemList?.size!!<=1){
                            imgbtnAddraisecomplaint?.visibility =View.GONE
                        }else{
                            imgbtnAddraisecomplaint?.visibility =View.VISIBLE
                        }
                        defectTtemSTRAdpter = ArrayAdapter(this@CRMDetailsActivity, R.layout.spinnertext, R.id.text1, defectitemList)
                        spDefectItem?.adapter = defectTtemSTRAdpter
//                            tvChassisNo?.setText(warrantyPojoList?.get(0)?.chassisNo.toString())
                        try{
                            tvChassisNo?.setText(warrantyPojoLis?.get(0)?.chassisNo.toString())
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }

                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }

                } else {
                    Toast.makeText(this@CRMDetailsActivity, message, Toast.LENGTH_SHORT).show()
                }

            } else {
                Toast.makeText(this@CRMDetailsActivity, message, Toast.LENGTH_SHORT).show()

            }
        }
        else if (apiCallPath.equals("getoldserielnoandstock")) {
            if (success) {
                println("Resaponse Form Server " + response.toString())
                if (response == null || response.length() == 0 || response.toString().isEmpty()) {
                    var toast = Toast.makeText(this@CRMDetailsActivity, "" + message, Toast.LENGTH_SHORT)
                    toast.setGravity(Gravity.CENTER, 0, 0)
                    toast.show()
                } else {
                    /**
                     * getting  the old Serial Number
                     */

                    newSerialNoAddMoreList?.clear()
                    oldsrlNoList?.clear()
                    if (response.getString("oldserial") == null || response.getString("oldserial").length == 0 ||
                            response.getString("oldserial").isEmpty() ||  response.getString("oldserial").equals(null)) {
                        oldsrlNoList?.add("Old Serial Number Not Available")
                        Toast.makeText(this, "Old serial Number Not Available", Toast.LENGTH_SHORT).show()
                    } else {
                        oldsrlNoList?.add(0,getString(R.string.select_old_serial_number))
//                        bs_edt_old_serial_number_addmore.setText(response.getString("oldserial"))
                        oldsrlNoList?.add(response.getString("oldserial"))

                    }
                    /**
                     * GETTING  SpareStock For New Serial Number
                     *
                     */
                    if (response.getJSONArray("sparesstock") == null || response.getJSONArray("sparesstock").toString().isEmpty() || response.getJSONArray("sparesstock").length() == 0) {
                        newSerialNoAddMoreList?.add("Stock Not Available")
                        Toast.makeText(this, "Stock Not Available", Toast.LENGTH_SHORT).show()
                    } else {
                        var sparestockList = Gson().fromJson<ArrayList<SpareStockPojos>>(response.getJSONArray("sparesstock").toString(), object : TypeToken<ArrayList<SpareStockPojos>>() {}.type)
                        for (spare in sparestockList) {
                            newSerialNoAddMoreList?.add(spare.serialNo)
                        }
                    }
                    /**
                     * setting the Adapter To  Spinner and Array List To Adapter
                     * */
                    newSerialNoaddmoreListAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, newSerialNoAddMoreList)
//                    bs_sp_new_serial_number_addmore.adapter = newSerialNoaddmoreListAdapter
                    oldsrlListAdapter = ArrayAdapter(this, R.layout.spinnertext, R.id.text1, oldsrlNoList)
//                    bs_sp_old_serial_number_addmore.adapter = oldsrlListAdapter

                }

            } else {

                MessageUtils.showSnackBar(this@CRMDetailsActivity, mSnacView,message)
            }

        }

    }


    fun getPath(context: Context?, uri: Uri): String? {

        // DocumentProvider
        if (DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
                // TODO handle non-primary volumes
            } else if (isDownloadsDocument(uri)) {
                val id = DocumentsContract.getDocumentId(uri)
                val contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))
                return getDataColumn(context!!, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]
                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])
                return getDataColumn(context!!, contentUri, selection, selectionArgs)
            }// MediaProvider
            // DownloadsProvider
        } else if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context!!, uri, null, null)
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }// File
        // MediaStore (and general)
        return null
    }

    fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)
        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun launchCamera(requestCode: Int) {
        val values = ContentValues(1)
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
        val fileUri = contentResolver
                .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        values)
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if(intent.resolveActivity(packageManager) != null) {
            if(requestCode== 12153){
                spareFileUri = fileUri
                intent.putExtra(MediaStore.EXTRA_OUTPUT, spareFileUri)
            }else if(requestCode == 12177) {
                spareServiceFileUri = fileUri

                intent.putExtra(MediaStore.EXTRA_OUTPUT, spareServiceFileUri)
            }else {
                outputFileUri = fileUri
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
            }

            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                    or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
            startActivityForResult(intent, requestCode)
        }
    }
//class RecyclerAdapter(private val photos: ArrayList<Photo>) RecyclerView.Adapter<RecyclerAdapter.PhotoHolder>() {

    override fun connectposition(position: Int, listSize: Int) {
        try{
            if(listSize == 100){
                if (ffrSpareSTrList?.contains(getString(R.string.no_data_avl))!!) {
                    ffrSpareSTrList?.removeAt(ffrSpareSTrList?.indexOf(getString(R.string.no_data_avl))!!)
                    ffrSpareSTrList?.add(ffrSpareItemnQtyList?.get(position)?.defectitem!!)
                    defectTtemSTRAdpter?.notifyDataSetChanged()
                } else {
                    ffrSpareSTrList?.add(ffrSpareItemnQtyList?.get(position)?.defectitem!!)
                    ffrSpareSTrAdapter?.notifyDataSetChanged()
                    if (ffrSpareSTrList?.size!! <= 1) {
                        ffRimgbtnAddraisecomplaint?.visibility = View.GONE
                    } else {
                        ffRimgbtnAddraisecomplaint?.visibility = View.VISIBLE
                    }
                    Log.d("DefectItemArrayListSize", "" + defectitemList?.size)
                }
            }else {
                if (defectitemList?.contains(getString(R.string.no_data_avl))!!) {
                    defectitemList?.removeAt(defectitemList?.indexOf(getString(R.string.no_data_avl))!!)
                    defectitemList?.add(warrantydefectitemList?.get(position)?.defectitem!!)
                    defectTtemSTRAdpter?.notifyDataSetChanged()
                } else {
                    defectitemList?.add(warrantydefectitemList?.get(position)?.defectitem!!)
                    defectTtemSTRAdpter?.notifyDataSetChanged()
                    if (defectitemList?.size!! <= 1) {
                        imgbtnAddraisecomplaint?.visibility = View.GONE
                    } else {
                        imgbtnAddraisecomplaint?.visibility = View.VISIBLE
                    }
                    Log.d("DefectItemArrayListSize", "" + defectitemList?.size)
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }
}