package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.warrantyComplantPojos.DefectitemQtyPojos
import com.ampere.vehicles.R
import customviews.CustomTextEditView
import java.lang.Exception

class  DefectItemQtyAddmoreAdapter (val context: Context, val warrantydefectitemList:ArrayList<DefectitemQtyPojos>, val configClickEvent: ConfigClickEvent)
    : RecyclerView.Adapter<DefectItemQtyAddmoreAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DefectItemQtyAddmoreAdapter.ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.adapter_crm_service_raisecomplint_defectitem_addmore, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return  warrantydefectitemList.size
    }

    override fun onBindViewHolder(holder: DefectItemQtyAddmoreAdapter.ViewHolder, position: Int) {
        try{
            holder.defectitem.setText(warrantydefectitemList.get(position).defectitem)
            holder.itemQty.setText(warrantydefectitemList.get(position).qty)
            holder.imgBtn.setOnClickListener {
                try{
                    configClickEvent.connectposition(position,warrantydefectitemList.size)
                    warrantydefectitemList.removeAt(position)
                    notifyDataSetChanged()
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }
    class ViewHolder(view : View): RecyclerView.ViewHolder(view)
    {
            val  defectitem:CustomTextEditView = view.findViewById(R.id.edt_defect_item_adptr)
            val  itemQty  :CustomTextEditView =  view.findViewById(R.id.edt_defect_qty_adptr)
            val  imgBtn : ImageButton = view.findViewById(R.id.imgbtn_remove_adptr)
    }

}