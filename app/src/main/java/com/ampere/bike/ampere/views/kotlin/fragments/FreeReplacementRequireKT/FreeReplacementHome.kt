package com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT


import android.annotation.SuppressLint
import android.os.Bundle
import android.os.StrictMode
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.fragment.parthi.Inventory.InventoryHome.msnakview
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.Adapter.FreeReplacementTapAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrComplaints.FreeRepalcementsComplaints
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrInvoice.FreeReplacementInvoice

import com.ampere.vehicles.R
import java.util.ArrayList


class FreeReplacementHome : Fragment() {
  companion object {
      @SuppressLint("StaticFieldLeak")
      var msnakview: View ?=null
  }
    var adapter: FreeReplacementTapAdapter? = null
    private var fRRtabLayout: TabLayout? = null
    private var fRRviewPager: ViewPager? = null
    internal var fRRfragmnetList: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

            val view :View = inflater.inflate(R.layout.fragment_free_replacement_home, container, false)
        (activity as CustomNavigationDuo).disbleNavigationView(getString(R.string.free_replcment_Req))
        val newbuilder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(newbuilder.build())
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        msnakview = view.findViewById(R.id.parent_view)
        fRRviewPager = view.findViewById<ViewPager>(R.id.frr_viewPager)
        fRRtabLayout = view.findViewById<TabLayout>(R.id.frr_tabLayout)
        fRRfragmnetList = ArrayList<String>()
        fRRfragmnetList.add("Complaints")
        fRRfragmnetList.add("Invoice")
        setupViewPager(fRRviewPager!!)


    }

    private fun setupViewPager(viewPager: ViewPager) {
        adapter = FreeReplacementTapAdapter(childFragmentManager)
        adapter!!.addFragment(FreeRepalcementsComplaints(), fRRfragmnetList.get(0))
        adapter!!.addFragment(FreeReplacementInvoice(), fRRfragmnetList.get(1))
        initTabs()
    }

    @SuppressLint("ResourceType")
    private fun initTabs() {
        fRRviewPager?.setAdapter(adapter)
        fRRtabLayout?.setupWithViewPager(fRRviewPager)

        for (i in 0 until adapter?.getCount()!!) {
            val textView = TextView(activity)
            textView.typeface = MyUtils.getTypeface(activity!!, "fonts/Lato-Bold.ttf")
            textView.setTextColor(ContextCompat.getColorStateList(activity!!, R.drawable.tab_selector))
            textView.gravity = Gravity.CENTER
            textView.setText(fRRfragmnetList.get(i))
            val tab = fRRtabLayout?.getTabAt(i)
            if (tab != null) {
                tab.setCustomView(textView)
                tab.setText(adapter!!.getPageTitle(i))
            }
        }
        fRRtabLayout?.setTabMode(TabLayout.MODE_FIXED)
    }
}
