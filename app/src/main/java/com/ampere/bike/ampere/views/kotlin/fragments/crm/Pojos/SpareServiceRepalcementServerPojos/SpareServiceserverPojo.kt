package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.SpareServiceRepalcementServerPojos

import com.google.gson.annotations.SerializedName

class SpareServiceserverPojo (
        @field:SerializedName("component_name")
        var component :String ?=null,
        @field:SerializedName("old_serial_no")
        var oldserial :String ?=null,
        @field:SerializedName("new_serial_no")
        var newserial :String ?=null,
        @field:SerializedName("id")
        var id :Int ?=null
)