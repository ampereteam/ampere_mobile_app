package com.ampere.bike.ampere.model.enquiry;

import com.google.gson.annotations.SerializedName;


public class DataItem {


        @SerializedName("batch_no")
        public String batchNo;

        @SerializedName("updated_at")
        public String updatedAt;

        @SerializedName("name")
        public String name;

        @SerializedName("supplier_invoice_details")
        public String supplierInvoiceDetails;

        @SerializedName("manufacturing_date")
        public String manufacturingDate;

        @SerializedName("created_at")
        public String createdAt;

        @SerializedName("id")
        public int id;

        @SerializedName("supplier_details")
        public String supplierDetails;

        @SerializedName("serial_no")
        public String serialNo;

        @SerializedName("lot_number")
        public String lotNumber;


}