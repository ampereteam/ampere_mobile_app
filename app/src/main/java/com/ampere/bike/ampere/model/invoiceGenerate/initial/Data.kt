package com.ampere.bike.ampere.model.invoiceGenerate.initial

data class Data(
        val enquiry: Enquiry,
        val statelists: ArrayList<Statelists>,
        val gst: Gst,
        val proofs: ArrayList<Proof>,
        val vehiclemodels: ArrayList<Vehiclemodel>,
        val vehicleinventory: ArrayList<Any>
)