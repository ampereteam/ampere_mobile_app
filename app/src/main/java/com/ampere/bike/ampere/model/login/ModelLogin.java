package com.ampere.bike.ampere.model.login;

import com.google.gson.annotations.SerializedName;

public class ModelLogin {

    @SerializedName("success")
    public boolean isStatus;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public Data data;


    public class Data {

        @SerializedName("token")
        public String token;

        @SerializedName("userdetail")
        public UserDetails userDetails;

        public class UserDetails {
            @SerializedName("id")
            public int id;

            @SerializedName("name")
            public String name;

            @SerializedName("username")
            public String username;

            @SerializedName("email")
            public String email;

            @SerializedName("mobile")
            public String mobileNnumber;

            @SerializedName("profile_photo")
            public String photo;

            @SerializedName("address")
            public String address;


            @SerializedName("user_type")
            public String userType;


        }
    }

}
