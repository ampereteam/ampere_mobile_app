package com.ampere.bike.ampere.model.invoiceGenerate.initial

data class Proof(
        val id: Int,
        val proof: String,
        val created_at: String,
        val updated_at: String
)