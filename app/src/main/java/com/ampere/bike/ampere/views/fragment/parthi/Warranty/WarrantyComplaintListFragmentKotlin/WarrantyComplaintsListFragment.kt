package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaintListFragmentKotlin

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.ampere.bike.ampere.CustomNavigationDuo.snackVIew
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.RetrofitService
import com.ampere.bike.ampere.api.api_new.ConfigPosition
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.*
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaintListFragmentKotlin.Adapter.WararantyCmplntAdapter
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaintListFragmentKotlin.Pojos.WarrantyComplaintListResPojos
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints.WarrantyComplaintsDetails

import com.ampere.vehicles.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_warranty_complaints_list.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.util.*


class WarrantyComplaintsListFragment : Fragment(),ConfigPosition
{



    /**
     * Global Declaration  for  Warranty List Fragment
     */
   private var utilsCallApi: UtilsCallApi?=null
   private var dialog: Dialog?=null
   private var warrantyComplaintListResPojos: WarrantyComplaintListResPojos?=null
   private var wararantyCmplntAdapter: WararantyCmplntAdapter?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view:View = inflater.inflate(R.layout.fragment_warranty_complaints_list, container, false)
        utilsCallApi = UtilsCallApi(activity)
        dialog = Dialog(activity)
        try{
            if(utilsCallApi?.isNetworkConnected!!){
                val map : HashMap<String,Any> = HashMap()
                map.put("userid",LocalSession.getUserInfo(activity, KEY_ID))
                val callInterface = utilsCallApi?.connectRetro("POST","warrantycomplaintlists")
                getandSetList(callInterface,map)
            }else{
                MessageUtils.showSnackBarAction(activity,snackVIew,getString(R.string.check_internet))
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }

        return  view
    }

    private fun getandSetList(retrofitService: RetrofitService?,map :HashMap<String,Any>) {
        try{
            dialog =MessageUtils.showDialog(activity)
            val callback = retrofitService?.call_post("warrantycomplaintlists", "Bearer "+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
                callback?.enqueue(object :retrofit2.Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        MessageUtils.dismissDialog(dialog)
                       Log.d("ReponseFailure",""+t.message)
                        MessageUtils.showSnackBar(activity,snackVIew,"Server Not Found")
                    }
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        if(response?.isSuccessful){
                            MessageUtils.dismissDialog(dialog)
                            try{
                                Log.d("infoResponse",""+response.body().toString())
                                warrantyComplaintListResPojos = Gson().fromJson(response.body()?.string(),WarrantyComplaintListResPojos::class.java)
                                Log.d("infoResponse",""+warrantyComplaintListResPojos.toString())
                                    getandSetREsponse(warrantyComplaintListResPojos)

                            }catch (ex:Exception){
                                ex.printStackTrace()
                            }
                        }else{
                            MessageUtils.showSnackBar(activity,snackVIew,"Server Not Found")
                        }
                    }
                })
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    @SuppressLint("SetTextI18n")
    private fun getandSetREsponse(warrantyComplaintListResPojos: WarrantyComplaintListResPojos?) {
        try{
        recycle_warranty_complaint_list .layoutManager = LinearLayoutManager(activity,LinearLayout.VERTICAL,false)
        wararantyCmplntAdapter = WararantyCmplntAdapter(activity!!,warrantyComplaintListResPojos?.data?.warrantycomplaints,this)
        recycle_warranty_complaint_list.adapter = wararantyCmplntAdapter
            if(warrantyComplaintListResPojos?.data?.warrantycomplaints?.size!! <=0){
                emptyList.setText(""+ warrantyComplaintListResPojos.message!!)
                emptyList.visibility=View.VISIBLE
            }else{
                emptyList.visibility =View.GONE
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
    override fun callinterfacePosition(position: Int) {
     Log.d("CalledFunction", position.toString())
        Log.d("Positoin",""+warrantyComplaintListResPojos?.data?.warrantycomplaints?.get(position)?.id)
        startActivity(Intent(activity,WarrantyComplaintsDetails::class.java).putExtra("complaintid",warrantyComplaintListResPojos?.data?.warrantycomplaints?.get(position)?.id.toString()))
    }


}
