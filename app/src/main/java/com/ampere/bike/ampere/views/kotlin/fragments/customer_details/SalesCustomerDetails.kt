package com.ampere.bike.ampere.views.kotlin.fragments.customer_details

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.BEARER
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.DateUtils.DD_MM
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.model.customersales.Data
import com.ampere.bike.ampere.views.kotlin.model.customersales.StockDetailsModel
import com.ampere.vehicles.R
import customviews.CustomTextView
import kotlinx.android.synthetic.main.frag_customer_details.view.*
import kotlinx.android.synthetic.main.frag_inventory.*
import kotlinx.android.synthetic.main.item_customer_sales.*
import kotlinx.android.synthetic.main.item_customer_sales.view.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class SalesCustomerDetails : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        (activity as CustomNavigationDuo).disbleNavigationView("Sales Details")
        return inflater.inflate(R.layout.frag_customer_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.sales_customer_list_view.visibility = View.GONE
        view.expand.visibility = View.GONE

        loadSalesDetails(view);

    }

    private fun loadSalesDetails(view: View) {
        var map: HashMap<String, String> = HashMap()
        map.put("userid", "" + LocalSession.getUserInfo(activity, LocalSession.KEY_ID))
        var models = MyUtils.getRetroService().onStockDetails(BEARER + LocalSession.getUserInfo(activity, KEY_TOKEN), map)
        var dialog = MessageUtils.showDialog(activity)
        dialog.setOnKeyListener { dialogInterface, i, keyEvent ->
            models.cancel()
            MessageUtils.dismissDialog(dialog);
            false
        }

        models.clone().enqueue(object : retrofit2.Callback<StockDetailsModel> {

            override fun onResponse(call: Call<StockDetailsModel>?, response: Response<StockDetailsModel>?) {
                MessageUtils.dismissDialog(dialog);
                if (response != null) {
                    if (response.isSuccessful) {
                        var stockDetailsModel: StockDetailsModel = response.body()!!
                        if (stockDetailsModel != null) {
                            if (stockDetailsModel.success) {
                                if (stockDetailsModel.data.sales.size == 0) {
                                    item_task_view.visibility = View.GONE
                                    view.sales_no_data.visibility = View.VISIBLE
                                    view.sales_customer_list_view.visibility = View.GONE
                                } else {
                                    item_task_view.visibility = View.VISIBLE
                                    view.sales_customer_list_view.visibility = View.VISIBLE
                                    view.sales_no_data.visibility = View.GONE
                                    view.sales_customer_list_view.layoutManager = LinearLayoutManager(activity)

                                    view.sales_customer_list_view.adapter = ItemCustomerSales(activity!!, stockDetailsModel.data.sales)
                                }
                            }
                        }
                    } else {
                        var msg = MessageUtils.showErrorCodeToast(response.code())
                        //MessageUtils.showSnackBar(activity, invent_snack_view, msg)
                        view.sales_no_data.text = msg
                        view.sales_no_data.visibility = View.VISIBLE
                    }
                }
            }

            override fun onFailure(call: Call<StockDetailsModel>?, t: Throwable?) {
                MessageUtils.dismissDialog(dialog);
                var msg = MessageUtils.showFailureToast(t!!.message)
                MessageUtils.showSnackBar(activity, invent_snack_view, msg)
                invent_no_data.text = msg
                invent_no_data.visibility = View.VISIBLE
            }
        })
    }

    class ItemCustomerSales(val activity: FragmentActivity, val stockItems: ArrayList<Data>) : RecyclerView.Adapter<ItemCustomerSale>() {
        var isExpand: Boolean = false
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemCustomerSale {
            return ItemCustomerSale(View.inflate(activity, R.layout.item_customer_sales, null))
        }

        override fun getItemCount(): Int {
            return stockItems.size
        }

        override fun onBindViewHolder(holder: ItemCustomerSale, position: Int) {

            holder.itemDate.setTypeface(MyUtils.getTypeface(activity, "fonts/Lato-Regular.ttf"))
            holder.itemCustomerName.setTypeface(MyUtils.getTypeface(activity, "fonts/Lato-Regular.ttf"))
            holder.itemInvoiceId.setTypeface(MyUtils.getTypeface(activity, "fonts/Lato-Regular.ttf"))
            holder.itemAmount.setTypeface(MyUtils.getTypeface(activity, "fonts/Lato-Regular.ttf"))

            /*       holder.itemChassisNo.setTextColor(activity.resources.getServiceType(R.serviceType.colorGray))
                   holder.itemModel.setTextColor(activity.resources.getServiceType(R.serviceType.colorGray))
                   holder.itemMobile.setTextColor(activity.resources.getServiceType(R.serviceType.colorGray))
                   holder.itemVarient.setTextColor(activity.resources.getServiceType(R.serviceType.colorGray))

       */

            holder.itemDate.text = if (stockItems[position].invoice_date != null) DateUtils.convertDates(stockItems[position].invoice_date, DateUtils.YYYY_MM_DD, DD_MM) else ""
            holder.itemCustomerName.text = stockItems[position].customer_name
            holder.itemInvoiceId.text = stockItems[position].invoice_id
            holder.itemAmount.text = stockItems[position].invoice_amount

            CustomTextView.marquee(holder.itemCustomerName)
            CustomTextView.marquee(holder.itemInvoiceId)
            CustomTextView.marquee(holder.itemAmount)

            holder.itemChassisNo.text = "    " + stockItems[position].chassis_no
            holder.itemModel.text = "    " + stockItems[position].vehicle_model
            holder.itemMobile.text = "    " + stockItems[position].mobile
            holder.itemVarient.text = "    " + stockItems[position].model_varient

            CustomTextView.marquee(holder.itemChassisNo)
            CustomTextView.marquee(holder.itemModel)
            CustomTextView.marquee(holder.itemMobile)
            CustomTextView.marquee(holder.itemVarient)

            var animFadeIn = AnimationUtils.loadAnimation(activity, R.anim.lay_slide_up);
            var animFadeOut = AnimationUtils.loadAnimation(activity, R.anim.lay_slide_down);

            holder.itemParentView.setOnClickListener {

                Log.d("logingd", "" + stockItems[position].isExpand)
                var addDatas: Data? = null
                /*if (isExpand) {
                    isExpand =false*/


                /*   for (i: Int in 0..(stockItems.size - 1)) {

                       if (!stockItems[i].isExpand) {
                           //if (i == position) {
                           addDatas = setListItems(true, i)
                       } else {
                           addDatas = setListItems(false, i)
                       }

                       stockItems.set(i, addDatas)
                   }

                   for (i: Int in 0..stockItems.size - 1) {
                       Log.d("isXExpandable__", "" + stockItems[i].isExpand)
                   }*/
                try {
                    if (stockItems[position].isExpand) {
                        holder.itemExpand.startAnimation(animFadeIn);
                        addDatas = setListItems(false, position)

                        holder.itemExpand.visibility = View.GONE

                    } else {
                        holder.itemExpand.startAnimation(animFadeOut);
                        addDatas = setListItems(true, position)
                        holder.itemExpand.visibility = View.VISIBLE
                    }

                    stockItems.set(position, addDatas)
                    notifyDataSetChanged()
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
                /*for (dat: Data in stockItems) {
                    if (dat.isExpand) {
                        holder.itemExpand.visibility = View.GONE
                    } else {
                        holder.itemExpand.visibility = View.VISIBLE

                    }
                }*/
            }
        }

        private fun setListItems(isExpand: Boolean, position: Int): Data {

            Log.d("stockItemssdsd", "" + stockItems.size)

            return Data(stockItems[position].id,
                    stockItems[position].invoice_id,
                    stockItems[position].invoice_no,
                    stockItems[position].invoice_date,
                    stockItems[position].enquiry_id,
                    stockItems[position].invoice_amount,
                    stockItems[position].vehicle_model,
                    stockItems[position].model_varient,
                    stockItems[position].color,
                    stockItems[position].chassis_no,
                    stockItems[position].motor_serial_no,
                    stockItems[position].battery_serial_no,
                    stockItems[position].charger_serial_no,
                    stockItems[position].indent_invoice_date,
                    stockItems[position].manufacture_date,
                    stockItems[position].battery_charged,
                    stockItems[position].battery_charged_date,
                    stockItems[position].customer_name,
                    stockItems[position].customer_address1,
                    stockItems[position].customer_address2,
                    stockItems[position].city,
                    stockItems[position].locality,
                    stockItems[position].pincode,
                    stockItems[position].state,
                    stockItems[position].statecode,
                    stockItems[position].id_proof,
                    stockItems[position].address_proof,
                    stockItems[position].email_id,
                    stockItems[position].mobile,
                    stockItems[position].aadhar_no,
                    stockItems[position].dob,
                    stockItems[position].occupation,
                    stockItems[position].gender,
                    stockItems[position].no_of_freeservice,
                    stockItems[position].service_available,
                    stockItems[position].delivery_at,
                    stockItems[position].sgst,
                    stockItems[position].cgst,
                    stockItems[position].igst,
                    stockItems[position].sgst_amount,
                    stockItems[position].cgst_amount,
                    stockItems[position].igst_amount,
                    stockItems[position].subsidy_amount,
                    stockItems[position].total_amount,
                    stockItems[position].invoice_by,
                    stockItems[position].subsidy_form,
                    stockItems[position].invoice_form,
                    stockItems[position].aadhar_card,
                    stockItems[position].created_at,
                    stockItems[position].updated_at,
                    isExpand)

            /*  try {*/
            /* return Data(stockItems[position].id,
                     stockItems[position].invoice_id,
                     stockItems[position].invoice_no,
                     stockItems[position].invoice_date,
                     stockItems[position].enquiry_id,
                     stockItems[position].invoice_amount,
                     stockItems[position].sendThrough,
                     stockItems[position].model_varient,
                     stockItems[position].chassis_no,
                     stockItems[position].customer_name,
                     stockItems[position].customer_address1,
                     stockItems[position].customer_address2,
                     stockItems[position].id_proof,
                     stockItems[position].address_proof,
                     stockItems[position].email_id,
                     stockItems[position].mobile,
                     stockItems[position].invoice_by,
                     stockItems[position].created_at,
                     stockItems[position].updated_at,
                     isExpand)*/


            /* } catch (ex: Exception) {
                 ex.printStackTrace()

             }*/

        }
    }

    class ItemCustomerSale(view: View) : RecyclerView.ViewHolder(view) {
        var itemDate = view.cus_sal_date
        var itemCustomerName = view.cus_sal_custmr_name
        var itemInvoiceId = view.cus_sal_invoice_id
        var itemAmount = view.cus_sal_amt
        var itemExpand = view.expand
        var itemParentView = view.parent_view

        var itemChassisNo = view.cs_vehicle_chassis_no
        var itemModel = view.cs_vehicle_model
        var itemMobile = view.cs_mobile_no
        var itemVarient = view.cs_vareint_no

    }
}