package com.ampere.bike.ampere.model.invoiceGenerate.variantColor

data class Spare(
        val id: Int,
        val spare_code: String,
        val spare_id: String,
        val vehicle_model_id: Int,
        val model_varient_id: String,
        val spares: String,
        val created_at: String,
        val updated_at: String
)