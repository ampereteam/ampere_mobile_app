package com.ampere.bike.ampere.utils;

import android.support.v4.app.FragmentActivity;
import android.view.View;

public class ExceptionUtils {

    public static boolean exceptionHandler(FragmentActivity activity, View snackView) {
        try {
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            MessageUtils.showSnackBar(activity, snackView, e.getMessage());
            return false;
        }
    }
}
