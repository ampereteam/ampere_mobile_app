package com.ampere.bike.ampere.views.kotlin.fragments.inventory

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.*
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.DateUtils.DD_MM
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.utils.MyUtils.*
import com.ampere.bike.ampere.views.kotlin.fragments.inventory.adapter.ItemStocksInSpare
import com.ampere.bike.ampere.views.kotlin.model.inventory.AcceptedStockModel
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare.Sparedespatched
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_vehicle.Vehicledespatched
import com.ampere.vehicles.R
import customviews.BottomSheetSelectionFragment
import customviews.CustomButton
import customviews.CustomTextView
import kotlinx.android.synthetic.main.frag_inventory.*
import kotlinx.android.synthetic.main.frag_inventory.view.*
import kotlinx.android.synthetic.main.item_stocks_spare.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable
import java.util.*


public class StocksAcceptFragment : Fragment() {

    companion object {
        var idSelection: Int? = 0
        var isCheck: Boolean = false

        var listOfSelectionId = ArrayList<Int>()

        @SuppressLint("StaticFieldLeak")
        var mSelectLay: LinearLayout? = null
        var mAcceptCancel: CustomButton? = null
        var mAcceptSelection: CustomButton? = null
        var mAcceptDone: CustomButton? = null

        var sheetBehavior: BottomSheetBehavior<*>? = null
        @SuppressLint("StaticFieldLeak")
        var layoutBottomSheet: LinearLayout? = null
        var mBS_SelectAll: CustomTextView? = null
        var mBS_DeSelectAll: CustomTextView? = null
        var fragment: BottomSheetSelectionFragment? = null
        @SuppressLint("StaticFieldLeak")
        var adapterItemStockVehicle: ItemStockeInVehicleDispatched? = null
        @SuppressLint("StaticFieldLeak")
        var itemStockSparePending: ItemStockPendingSpares? = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        // (activity as CustomNavigationDuo).disbleNavigationView("Stocks")
        return inflater.inflate(R.layout.frag_inventory, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setListView(view)


        BottomSheetSelectionFragment.bindListener {
            listOfSelectionId.clear()
            try {
                if (it.toString().equals(activity!!.getString(R.string.select_all))) {
                    if (MyUtils.VIEW_ITEM.equals(getString(R.string.vehicle))) {
                        listOfSelectionId.clear()
                        //Stock Pending for Vehicle
                        for (i: Int in (0..DISPATCHED_VHICLE.size - 1)) {
                            listOfSelectionId.add(DISPATCHED_VHICLE[i].id)
                            DISPATCHED_VHICLE.set(i, Vehicledespatched(DISPATCHED_VHICLE[i].id, DISPATCHED_VHICLE[i].vehicle_model, DISPATCHED_VHICLE[i].model_varient,
                                    DISPATCHED_VHICLE[i].color, DISPATCHED_VHICLE[i].chassis_no,/* DISPATCHED_VHICLE[i].spares_serial_no,*/ DISPATCHED_VHICLE[i].qty,
                                    DISPATCHED_VHICLE[i].indent_invoice_date, DISPATCHED_VHICLE[i].manufacture_date, DISPATCHED_VHICLE[i].status,
                                    DISPATCHED_VHICLE[i].acceptance, DISPATCHED_VHICLE[i].user_id, DISPATCHED_VHICLE[i].delivered_by, DISPATCHED_VHICLE[i].created_at,
                                    DISPATCHED_VHICLE[i].updated_at, true))

                        }

                        adapterItemStockVehicle!!.notifyDataSetChanged()


                    } else {
                        //Stock Pending with Check BOX
                        for (position: Int in (0..DISPATCHED_SPARE.size - 1)) {
                            listOfSelectionId.add(DISPATCHED_SPARE[position].id)
                            DISPATCHED_SPARE.set(position, Sparedespatched(DISPATCHED_SPARE[position].id, DISPATCHED_SPARE[position].spare_name, DISPATCHED_SPARE[position].serial_no,
                                    DISPATCHED_SPARE[position].qty, DISPATCHED_SPARE[position].indent_invoice_date, DISPATCHED_SPARE[position].manufacture_date, DISPATCHED_SPARE[position].status,
                                    DISPATCHED_SPARE[position].acceptance, DISPATCHED_SPARE[position].user_id, DISPATCHED_SPARE[position].delivered_by,
                                    DISPATCHED_SPARE[position].created_at, DISPATCHED_SPARE[position].updated_at, true))
                        }
                        itemStockSparePending?.notifyDataSetChanged()
                    }
                } else {
                    if (MyUtils.VIEW_ITEM.equals(getString(R.string.vehicle))) {
                        //Stock Pending for Vehicle
                        for (i: Int in (0..DISPATCHED_VHICLE.size - 1)) {
                            DISPATCHED_VHICLE.set(i, Vehicledespatched(DISPATCHED_VHICLE[i].id, DISPATCHED_VHICLE[i].vehicle_model, DISPATCHED_VHICLE[i].model_varient,
                                    DISPATCHED_VHICLE[i].color, DISPATCHED_VHICLE[i].chassis_no,/* DISPATCHED_VHICLE[i].spares_serial_no,*/ DISPATCHED_VHICLE[i].qty,
                                    DISPATCHED_VHICLE[i].indent_invoice_date, DISPATCHED_VHICLE[i].manufacture_date, DISPATCHED_VHICLE[i].status,
                                    DISPATCHED_VHICLE[i].acceptance, DISPATCHED_VHICLE[i].user_id, DISPATCHED_VHICLE[i].delivered_by, DISPATCHED_VHICLE[i].created_at,
                                    DISPATCHED_VHICLE[i].updated_at, false))
                        }
                        adapterItemStockVehicle!!.notifyDataSetChanged()

                    } else {
                        //Stock Pending with Check BOX
                        for (position: Int in (0..DISPATCHED_SPARE.size - 1)) {
                            listOfSelectionId.remove(DISPATCHED_SPARE[position].id)
                            DISPATCHED_SPARE.set(position, Sparedespatched(DISPATCHED_SPARE[position].id, DISPATCHED_SPARE[position].spare_name, DISPATCHED_SPARE[position].serial_no,
                                    DISPATCHED_SPARE[position].qty, DISPATCHED_SPARE[position].indent_invoice_date, DISPATCHED_SPARE[position].manufacture_date, DISPATCHED_SPARE[position].status,
                                    DISPATCHED_SPARE[position].acceptance, DISPATCHED_SPARE[position].user_id, DISPATCHED_SPARE[position].delivered_by,
                                    DISPATCHED_SPARE[position].created_at, DISPATCHED_SPARE[position].updated_at, false))
                        }

                        itemStockSparePending?.notifyDataSetChanged()
                    }
                }

                checkSelectionLayout()
            } catch (nullPointer: NullPointerException) {
                nullPointer.printStackTrace()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        (sheetBehavior as BottomSheetBehavior<LinearLayout?>?)?.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        //btnBottomSheet.setText("Close Sheet")
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        //btnBottomSheet.setText("Expand Sheet")
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })


        mAcceptDone?.setOnClickListener {

            if (idSelection != 0) {
                acceptStockDetails(listOfSelectionId)
            } else {
                MessageUtils.showSnackBar(activity, accpt_stock_not_found, "Select alteast one item")
            }
        }

        mAcceptCancel?.setOnClickListener {
            activity!!.onBackPressed()
        }

        mAcceptSelection?.setOnClickListener {

            fragment!!.show(activity!!.supportFragmentManager, "TAG")

        }
    }

    private fun acceptStockDetails(listOfSelectionId: ArrayList<Int>) {

        var map = HashMap<String, Any>()
        map.put("inventory_id", listOfSelectionId)
        map.put("stock_type", VIEW_ITEM)
        map.put("username", LocalSession.getUserInfo(activity!!, KEY_ID))
        var acceptModels: Call<AcceptedStockModel> = MyUtils.getRetroService().onAcceptBulkStocks(BEARER+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
        var dialog: Dialog = MessageUtils.showDialog(activity)

        dialog.setOnKeyListener { dialogInterface, i, keyEvent ->
            MessageUtils.dismissDialog(dialog);
            acceptModels.cancel()
            false
        }

        acceptModels.enqueue(object : Callback<AcceptedStockModel> {

            override fun onResponse(call: Call<AcceptedStockModel>?, response: Response<AcceptedStockModel>?) {
                MessageUtils.dismissDialog(dialog);
                if (response != null) {
                    if (response.isSuccessful) {
                        var acceptStockModel = response?.body()
                        if (acceptStockModel!!.success) {
                            MessageUtils.showSnackBar(activity!!, accpt_stock_not_found, acceptStockModel!!.message)
                            if (VIEW_ITEM.equals(getString(R.string.vehicle))) {
                                try {
                                    for (itemVechil: Int in 0..(DISPATCHED_VHICLE.size - 1)) {
                                        for (itemPoistion in listOfSelectionId) {
                                            if (itemPoistion.equals(DISPATCHED_VHICLE[itemVechil].id)) {
                                                DISPATCHED_VHICLE.removeAt(itemVechil)
                                            }
                                        }
                                    }
                                } catch (Ind: IndexOutOfBoundsException) {
                                    Ind.printStackTrace()
                                }
                                adapterItemStockVehicle?.notifyDataSetChanged()
                            } else {

                                try {
                                    var count: Int = (DISPATCHED_SPARE.size - 1)
                                    for (itemVechil: Int in 0..count) {
                                        for (itemPoistion in listOfSelectionId) {
                                            if (itemPoistion.equals(DISPATCHED_SPARE[itemVechil].id)) {
                                                DISPATCHED_SPARE.removeAt(itemVechil)
                                            }
                                        }

                                    }
                                } catch (Ind: IndexOutOfBoundsException) {
                                    Ind.printStackTrace()
                                }
                                itemStockSparePending?.notifyDataSetChanged()

                            }
                            mSelectLay!!.visibility = View.GONE
                        } else {
                            MessageUtils.showSnackBar(activity!!, accpt_stock_not_found, acceptStockModel!!.message)
                        }
                    } else {
                        var msg = MessageUtils.showErrorCodeToast(response.code())
                        MessageUtils.showSnackBar(activity, accpt_stock_not_found, msg)

                    }
                } else {
                    MessageUtils.showSnackBar(activity!!.applicationContext, accpt_stock_not_found, getString(R.string.api_response_not_found))
                }
            }


            override fun onFailure(call: Call<AcceptedStockModel>?, t: Throwable?) {
                MessageUtils.dismissDialog(dialog);
                var msg = MessageUtils.showFailureToast(t!!.message)
                MessageUtils.showSnackBar(activity!!.applicationContext, accpt_stock_not_found, msg)
            }

        })

    }

    fun checkSelectionLayout() {
        if (listOfSelectionId.size >= 1) {
            mSelectLay!!.visibility = View.VISIBLE
        } else {
            mSelectLay!!.visibility = View.GONE
        }
        mAcceptSelection?.text = "" + activity!!.getString(R.string.select) + " (" + listOfSelectionId.size + ")"
    }

    fun showBootmSheet() {
        if (sheetBehavior?.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior?.setState(BottomSheetBehavior.STATE_EXPANDED);
            layoutBottomSheet!!.visibility = View.GONE

        } else {
            sheetBehavior?.setState(BottomSheetBehavior.STATE_COLLAPSED);
            layoutBottomSheet!!.visibility = View.VISIBLE

        }
    }

    class ItemStockeInVehicleDispatched(var activity: FragmentActivity, var stockOfDispatched: ArrayList<Vehicledespatched>, val accpt_stock_not_found: View) : RecyclerView.Adapter<ItemStocksInSpare.StocksInSpareHolder>() {


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemStocksInSpare.StocksInSpareHolder {

            return ItemStocksInSpare.StocksInSpareHolder(View.inflate(activity, R.layout.item_stocks_spare, null))
        }

        override fun getItemCount(): Int {
            return stockOfDispatched.size
        }

        @SuppressLint("SetTextI18n")
        override fun onBindViewHolder(holder: ItemStocksInSpare.StocksInSpareHolder, position: Int) {

            holder.stock_name.text = stockOfDispatched[position].vehicle_model

            holder.stock_qty.text = stockOfDispatched[position].qty
            holder.stock_serial_number.text = stockOfDispatched[position].chassis_no
            holder.stock_invoice_date.text = DateUtils.convertDates(stockOfDispatched[position].indent_invoice_date, DateUtils.YYYY_MM_DD, DateUtils.DD_MM)


            if (stockOfDispatched[position].isCheck) {
                holder.stock_check.isChecked = true
            } else {
                holder.stock_check.isChecked = false
            }

            holder.inventory_lay.setOnClickListener {
                if (listOfSelectionId.size == 0) {
                    var fragment = ViewViehicleDetailed()
                    var bundle = Bundle()
                    bundle.putString("id", "" + stockOfDispatched[position].id)
                    bundle.putString("id_values", "Vehicle_despatched")
                    fragment.arguments = bundle
                    MyUtils.passFragmentBackStack(activity!!, fragment)
                } else {
                    MessageUtils.showSnackBar(activity!!, accpt_stock_not_found, "Please remove selected item")
                }
            }
            holder.stock_check.setOnClickListener {


                if (stockOfDispatched[position].isCheck) {
                    idSelection = 0
                    listOfSelectionId.remove(stockOfDispatched[position].id)
                    stockOfDispatched.set(position, Vehicledespatched(stockOfDispatched[position].id, stockOfDispatched[position].vehicle_model, stockOfDispatched[position].model_varient,
                            stockOfDispatched[position].color, stockOfDispatched[position].chassis_no,/* stockOfDispatched[position].spares_serial_no,*/ stockOfDispatched[position].qty,
                            stockOfDispatched[position].indent_invoice_date, stockOfDispatched[position].manufacture_date, stockOfDispatched[position].status,
                            stockOfDispatched[position].acceptance, stockOfDispatched[position].user_id, stockOfDispatched[position].delivered_by, stockOfDispatched[position].created_at,
                            stockOfDispatched[position].updated_at, false))

                } else {
                    idSelection = stockOfDispatched[position].id
                    listOfSelectionId.add(stockOfDispatched[position].id)
                    stockOfDispatched.set(position, Vehicledespatched(stockOfDispatched[position].id, stockOfDispatched[position].vehicle_model, stockOfDispatched[position].model_varient,
                            stockOfDispatched[position].color, stockOfDispatched[position].chassis_no, /*stockOfDispatched[position].spares_serial_no,*/ stockOfDispatched[position].qty,
                            stockOfDispatched[position].indent_invoice_date, stockOfDispatched[position].manufacture_date, stockOfDispatched[position].status,
                            stockOfDispatched[position].acceptance, stockOfDispatched[position].user_id, stockOfDispatched[position].delivered_by, stockOfDispatched[position].created_at,
                            stockOfDispatched[position].updated_at, true))
                }


                mAcceptSelection?.text = "" + activity.getString(R.string.select) + " (" + listOfSelectionId.size + ")"


                if (listOfSelectionId.size >= 1) {
                    mSelectLay!!.visibility = View.VISIBLE
                } else {
                    mSelectLay!!.visibility = View.GONE
                }
            }


        }
    }

    private fun setListView(view: View) {

        listOfSelectionId.clear()
        view.invent_list_view.visibility = View.GONE
        view.invent_no_data.visibility = View.GONE
        mSelectLay = view.findViewById(R.id.select_lay)
        mAcceptDone = view.findViewById(R.id.accept)
        mAcceptCancel = view.findViewById(R.id.accept_cancel)
        mAcceptSelection = view.findViewById(R.id.accept_select)
        layoutBottomSheet = view.findViewById(R.id.bs_select_all_lay)
        layoutBottomSheet?.visibility = View.GONE
        mBS_DeSelectAll = view.findViewById(R.id.bs_de_select_all)
        mBS_SelectAll = view.findViewById(R.id.bs_select_all)

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        view.invent_list_view.layoutManager = GridLayoutManager(activity, 1)
        fragment = BottomSheetSelectionFragment()
        if (MyUtils.VIEW_ITEM.equals(getString(R.string.vehicle))) {
            //Pending (waiting for accept)
            if (MyUtils.DISPATCHED_VHICLE.size == 0) {

                view.invent_no_data.setText("No Stocks Found")
                view.invent_no_data.visibility = View.VISIBLE
                view.invent_list_view.visibility = View.GONE

            } else {
                view.invent_no_data.visibility = View.GONE
                view.invent_list_view.visibility = View.VISIBLE
                adapterItemStockVehicle = ItemStockeInVehicleDispatched(activity!!, DISPATCHED_VHICLE, accpt_stock_not_found)

                view.invent_list_view.adapter = adapterItemStockVehicle

            }

        } else {

            if (MyUtils.DISPATCHED_SPARE.size == 0) {
                view.invent_no_data.setText("No Spare Stocks  Found")
                view.invent_no_data.visibility = View.VISIBLE
                view.invent_list_view.visibility = View.GONE

            } else {
                view.invent_no_data.visibility = View.GONE
                view.invent_list_view.visibility = View.VISIBLE
                itemStockSparePending = ItemStockPendingSpares(activity!!, DISPATCHED_SPARE, accpt_stock_not_found)
                view.invent_list_view.adapter = itemStockSparePending

            }
        }

    }


    class ItemStockPendingSpares(var activity: FragmentActivity, var stockPandingSpare: ArrayList<Sparedespatched>, val accpt_stock_not_found: View) : RecyclerView.Adapter<ItemStockPendingSpares.ItemStocksInPendingSpare>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemStocksInPendingSpare {
            return ItemStocksInPendingSpare(View.inflate(activity, R.layout.item_stocks_spare, null))

        }

        override fun getItemCount(): Int {
            return stockPandingSpare.size
        }

        override fun onBindViewHolder(holder: ItemStocksInPendingSpare, position: Int) {
            holder.stock_check.visibility = View.VISIBLE
            holder.stock_name.text = stockPandingSpare[position].spare_name
            holder.stock_serial_number.text = stockPandingSpare[position].serial_no
            holder.stock_invoice_date.text = DateUtils.convertDates(stockPandingSpare[position].indent_invoice_date, DateUtils.YYYY_MM_DD, DD_MM)
            holder.stock_qty.text = stockPandingSpare[position].qty

            if (stockPandingSpare[position].isCheck) {
                holder.stock_check.isChecked = true
            } else {
                holder.stock_check.isChecked = false
            }

/*
            holder.stock_check.setOnCheckedChangeListener { compoundButton, b ->
                if (isCheck) {
                    isCheck = false
                    holder.stock_check.isChecked = false
                } else {
                    isCheck = true
                    holder.stock_check.isChecked = true
                }
            }*/

            holder.stock_check.setOnClickListener {


                if (stockPandingSpare[position].isCheck) {

                    idSelection = 0
                    listOfSelectionId.remove(stockPandingSpare[position].id)
                    stockPandingSpare.set(position, Sparedespatched(stockPandingSpare[position].id, stockPandingSpare[position].spare_name, stockPandingSpare[position].serial_no,
                            stockPandingSpare[position].qty, stockPandingSpare[position].indent_invoice_date, stockPandingSpare[position].manufacture_date, stockPandingSpare[position].status,
                            stockPandingSpare[position].acceptance, stockPandingSpare[position].user_id, stockPandingSpare[position].delivered_by,
                            stockPandingSpare[position].created_at, stockPandingSpare[position].updated_at, false))

                } else {

                    idSelection = stockPandingSpare[position].id
                    listOfSelectionId.add(stockPandingSpare[position].id)


                    stockPandingSpare.set(position, Sparedespatched(stockPandingSpare[position].id, stockPandingSpare[position].spare_name, stockPandingSpare[position].serial_no,
                            stockPandingSpare[position].qty, stockPandingSpare[position].indent_invoice_date, stockPandingSpare[position].manufacture_date, stockPandingSpare[position].status,
                            stockPandingSpare[position].acceptance, stockPandingSpare[position].user_id, stockPandingSpare[position].delivered_by,
                            stockPandingSpare[position].created_at, stockPandingSpare[position].updated_at, true))

                }

                mAcceptSelection?.text = "" + activity.getString(R.string.select) + " (" + listOfSelectionId.size + ")"

                if (listOfSelectionId.size >= 1) {
                    mSelectLay!!.visibility = View.VISIBLE
                } else {
                    mSelectLay!!.visibility = View.GONE
                }

            }

            holder.inventory_lay.setOnClickListener {
                if (listOfSelectionId.size == 0) {
                    var fragment = ViewSpareDetailed()
                    var bundle = Bundle()
                    bundle.putString("id", "" + stockPandingSpare[position].id)
                    bundle.putString("id_values", "Spare_despatched")
                    bundle.putSerializable("item_values", stockPandingSpare[position] as Serializable)

                    fragment.arguments = bundle
                    MyUtils.passFragmentBackStack(activity, fragment)
                } else {
                    MessageUtils.showSnackBar(activity!!, accpt_stock_not_found, "Please remove selected item")
                }
            }
        }

        class ItemStocksInPendingSpare(view: View) : RecyclerView.ViewHolder(view) {
            val stock_name = view.stock_name
            val stock_check = view.stock_check
            val stock_serial_number = view.stock_serial_number
            val stock_invoice_date = view.stock_invoice_date
            val stock_qty = view.stock_qty
            val inventory_lay = view.inventory_lay
        }
    }
}