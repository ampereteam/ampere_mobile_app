package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class
TaskEnquiryModel {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("enquiry_id")
    @Expose
    public String enquiryId;
    @SerializedName("enquiry_no")
    @Expose
    public String enquiryNo;
    @SerializedName("customer_name")
    @Expose
    public String customerName;
    @SerializedName("mobile_no")
    @Expose
    public String mobileNo;
    @SerializedName("mtype")
    @Expose
    public String mtype;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("address1")
    @Expose
    public String address1;
    @SerializedName("address2")
    @Expose
    public String address2;
    @SerializedName("locality")
    @Expose
    public String locality;
    @SerializedName("pincode")
    @Expose
    public String pincode;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("enquiry_date")
    @Expose
    public String enquiryDate;
    @SerializedName("enquiry_time")
    @Expose
    public String enquiryTime;
    @SerializedName("leadsource")
    @Expose
    public String leadsource;
    @SerializedName("enquiry_for")
    @Expose
    public String enquiryFor;
    @SerializedName("enquiry_type")
    @Expose
    public String enquiryType;
    @SerializedName("vehicle_model")
    @Expose
    public String vehicleModel;
    @SerializedName("vehicle_color")
    @Expose
    public String vehicleColor;
    @SerializedName("vehicle_qty")
    @Expose
    public String vehicleQty;
    @SerializedName("chassis_no")
    @Expose
    public String chassisNo;
    @SerializedName("serial_no")
    @Expose
    public String serialNo;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("assigned_to")
    @Expose
    public String assignedTo;
    @SerializedName("assigned_person")
    @Expose
    public Integer assignedPerson;
    @SerializedName("assigned_to_sub")
    @Expose
    public String assignedToSub;
    @SerializedName("assigned_date")
    @Expose
    public String assignedDate;
    @SerializedName("task")
    @Expose
    public String task;
    @SerializedName("task_status")
    @Expose
    public String taskStatus;
    @SerializedName("service_type")
    @Expose
    public String serviceType;
    @SerializedName("warranty_component")
    @Expose
    public String warrantyComponent;
    @SerializedName("place_of_service")
    @Expose
    public String placeOfService;
    @SerializedName("assigned_to_self")
    @Expose
    public String assignedToSelf;
    @SerializedName("created_by")
    @Expose
    public String createdBy;
    @SerializedName("spares_id")
    @Expose
    public String sparesId;
    @SerializedName("spares_name")
    @Expose
    public String sparesName;
    @SerializedName("vehcile_image")
    @Expose
    public String vehcileImage;
    @SerializedName("invoice_value")
    @Expose
    public String invoiceValue;
    @SerializedName("spares_value")
    @Expose
    public String sparesValue;
    @SerializedName("service_issues")
    @Expose
    public String serviceIssues;
    @SerializedName("root_cause")
    @Expose
    public String rootCause;
    @SerializedName("complaint_status")
    @Expose
    public String complaintStatus;
    @SerializedName("created_time")
    @Expose
    public String createdTime;
    @SerializedName("customer_type")
    @Expose
    public String customerType;
    @SerializedName("closing_date")
    @Expose
    public String closingDate;
    @SerializedName("modified_date")
    @Expose
    public String modifiedDate;
    @SerializedName("modified_by")
    @Expose
    public String modifiedBy;
    @SerializedName("read_status")
    @Expose
    public Integer readStatus;
    @SerializedName("created_at")
    @Expose
    public String createdAt;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;

    @SerializedName("assignedperson")
    public AssignedPerson assignedDetails;

}