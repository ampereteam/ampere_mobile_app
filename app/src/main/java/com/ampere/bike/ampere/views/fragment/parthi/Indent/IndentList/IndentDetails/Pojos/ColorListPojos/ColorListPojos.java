package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.ColorListPojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ColorListPojos {
    @SerializedName("color")
    @Expose
    private String color;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
