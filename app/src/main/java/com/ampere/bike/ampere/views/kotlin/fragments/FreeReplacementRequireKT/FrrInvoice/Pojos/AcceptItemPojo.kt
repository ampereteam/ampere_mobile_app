package com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrInvoice.Pojos

import com.google.gson.annotations.SerializedName


class AcceptItemPojo(
        @field:SerializedName("invoice_id")
        var invoiceID :String?=null
)
