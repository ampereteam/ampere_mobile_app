package com.ampere.bike.ampere.views.fragment.parthi.Indent.VehileDispatch;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.api.Retrofit.NetworkManager;
import com.ampere.bike.ampere.api.Retrofit.ResponsebackToClass;
import com.ampere.bike.ampere.api.api_new.UtilsRetrofitSerivce;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentHome;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.VehileDispatch.Adapter.VehicleDispatchAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.VehileDispatch.Pojos.VehicleDispatchPojo;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ampere.bike.ampere.CustomNavigationDuo.snackVIew;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;


public class VehileDispatch extends Fragment implements ResponsebackToClass, ConfigClickEvent {
    Common common;
    //SessionManager sessionManager;
    RecyclerView recyclerView;
    CheckBox checkBox;
    ArrayList<VehicleDispatchPojo> vehicleDispatchPojoArrayList = new ArrayList<>();
    ArrayList<VehicleDispatchPojo> vehiclServerList = new ArrayList<>();
    VehicleDispatchAdapter vehicleDispatchAdapter;
    Button button;
    TextView textView;
    LinearLayout linearLayout;
    Dialog dialog;
    LinearLayout linspareListtitle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_indent_vehile_dispatch, container, false);
        common = new Common(getActivity(), this);
        dialog = new Dialog(getActivity());
        //sessionManager = new SessionManager(getActivity());
        recyclerView = view.findViewById(R.id.vehile_dispatch_List);
        checkBox = view.findViewById(R.id.checkbox);
        button = view.findViewById(R.id.acpt);
        textView = view.findViewById(R.id.emptyList);
        linspareListtitle = view.findViewById(R.id.main);
        linearLayout = view.findViewById(R.id.accept);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        CallApi();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (vehicleDispatchPojoArrayList.size() > 0) {
                    if (isChecked) {
                        int i;
                        for (i = 0; vehicleDispatchPojoArrayList.size() > i; i++) {
                            vehicleDispatchPojoArrayList.get(i).setCheck(true);
                        }
                        vehicleDispatchAdapter.setList(vehicleDispatchPojoArrayList);
                        vehicleDispatchAdapter.notifyDataSetChanged();
                        linearLayout.setVisibility(View.VISIBLE);
                    } else {
                        for (int i = 0; vehicleDispatchPojoArrayList.size() > i; i++) {
                            vehicleDispatchPojoArrayList.get(i).setCheck(false);
                        }
                        vehicleDispatchAdapter.setList(vehicleDispatchPojoArrayList);
                        vehicleDispatchAdapter.notifyDataSetChanged();
                        linearLayout.setVisibility(View.GONE);
                    }
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendToserver();
            }
        });
        /*view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(getActivity(), CustomNavigationDuo.class);
                    i.putExtra("SELECTEDVALUE", 0);
//                    // i.putExtra("Comingback", cameback);
                    startActivity(i);
                    return true;
                } else {
                    return false;
                }
            }
        });*/
        return view;

    }

    public void CallApi() {
     try{
         if (common.isNetworkConnected()) {
             dialog =MessageUtils.showDialog(getActivity());
             HashMap<String, String> map = new HashMap<>();
             map.put("userid", LocalSession.getUserInfo(getActivity(), KEY_ID));
             common.callApiRequest("vehicledelivered", "POST", map, 0);
         } else {
//            common.hideLoad();
             Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();
         }
     }catch (Exception sx){
         sx.printStackTrace();
     }
    }

    @Override
    public void apiCallBackOverRideMethod(Response<ResponseBody> response, JSONArray objResponse, String method, int position, String message, boolean sucs) throws JSONException, IOException {
                try{
                    MessageUtils.dismissDialog(dialog);
                    if (objResponse != null) {
                        vehicleDispatchPojoArrayList = new Gson().fromJson(objResponse.toString(), new TypeToken<ArrayList<VehicleDispatchPojo>>() {
                        }.getType());
/*         VehicleDispatchPojo vehicleDispatchPojo = new VehicleDispatchPojo();
         vehicleDispatchPojo.setChassisNo("987564564");
         vehicleDispatchPojo.setColor("BluREd");
         vehicleDispatchPojo.setDriverName("mureugsn");
         vehicleDispatchPojo.setAcceptance("wtrew");
         vehicleDispatchPojo.setCheck(false);
         vehicleDispatchPojo.setDriverName("mureugsn");
         vehicleDispatchPojoArrayList.add(vehicleDispatchPojo);
         vehicleDispatchPojoArrayList.addAll(vehicleDispatchPojoArrayList);
         vehicleDispatchPojoArrayList.addAll(vehicleDispatchPojoArrayList);
         vehicleDispatchPojoArrayList.addAll(vehicleDispatchPojoArrayList);*/
                        if (vehicleDispatchPojoArrayList.size() > 0) {
                            vehicleDispatchAdapter = new VehicleDispatchAdapter(getActivity(), vehicleDispatchPojoArrayList, this);
                            recyclerView.setAdapter(vehicleDispatchAdapter);
                        } else {
                            textView.setVisibility(View.VISIBLE);
                            linearLayout.setVisibility(View.GONE);
                            linspareListtitle.setVisibility(View.GONE);
                            textView.setText(message);
                        }

                    } else {
                        if (method!= null && method.equals("acceptvehicledelivered")) {
//                startActivity(new Intent(getActivity(), IndentHome.class));
                            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        }else{
                            textView.setVisibility(View.VISIBLE);
                            linearLayout.setVisibility(View.GONE);
                            linspareListtitle.setVisibility(View.GONE);
                            textView.setText(message);
                        }
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }
    }

    @Override
    public void connectposition(int position, int listSize) {
        try{
            if (listSize == 0) {
                linearLayout.setVisibility(View.GONE);
            } else {
                linearLayout.setVisibility(View.VISIBLE);
                button.setText("Accept(" + listSize + ")");
            }
            if(vehicleDispatchPojoArrayList.get(position).isCheck()){
                Log.d("VehiceleModel",""+vehicleDispatchPojoArrayList.get(position).getColor());
            }
            Log.d("Position",""+listSize+"\t"+position);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void sendToserver() {
        try{
                        HashMap<String, Object> objectHashMap = new HashMap<>();
                        HashMap<String, Object> object = new HashMap<>();
                        vehiclServerList.clear();
                        for (int i = 0; i < vehicleDispatchPojoArrayList.size(); i++) {
                            if (vehicleDispatchPojoArrayList.get(i).isCheck()) {
                                vehiclServerList.add(vehicleDispatchPojoArrayList.get(i));
                            }
                        }
                        object.put("vehicle", vehiclServerList);

                        objectHashMap.put("vehicles", object);
                        if (common.isNetworkConnected()) {
                                dialog  =MessageUtils.showDialog(getActivity());
                            NetworkManager utilsRetrofitSerivce = common.connectRetro("POST","acceptvehicledelivered");
                            Call<ResponseBody> objCallApi = utilsRetrofitSerivce.call_post("acceptvehicledelivered","Bearer "+LocalSession.getUserInfo(getActivity(),KEY_TOKEN),objectHashMap);
//            common.callApiReques("acceptvehicledelivered", "POST", objectHashMap, 0);
                            objCallApi.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                            MessageUtils.dismissDialog(dialog);
                                           if(response.isSuccessful()){
                                               try{
                                                   JSONObject jsonObject   = new JSONObject(response.body().string());
                                                   String  message = jsonObject.getString("message");
                                                   Boolean success = jsonObject.getBoolean("success");
                                                   if(success){
                                                       MessageUtils.showToastMessage(getActivity(),message);
                                                       MyUtils.passFragmentBackStack(getActivity(),new IndentHome());
                                                   }else {
                                                       MessageUtils.showSnackBar(getActivity(),snackVIew,message);

                                                   }
                                               }catch (Exception ex){
                                                   ex.printStackTrace();
                                               }

                                           }else{
                                               MessageUtils.showSnackBar(getActivity(),snackVIew,response.message());
                                           }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    MessageUtils.dismissDialog(dialog);
                                    MessageUtils.showSnackBar(getActivity(),snackVIew,t.getLocalizedMessage());
                                }
                            });
                        } else {
                            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
    }
}