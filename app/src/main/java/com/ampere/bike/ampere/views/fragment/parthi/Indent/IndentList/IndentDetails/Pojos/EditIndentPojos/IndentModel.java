package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.Pojos.EditIndentPojos;

import com.google.gson.annotations.SerializedName;

public class IndentModel {
    public IndentModel(String indentType,
                       String vehicleModel,
                       String spare,
                       String qty,
                       String modelVarient,
                       String color,
                       String remarks,
                       int indent_id
    ) {
        this.indentType = indentType;
        this.color = color;
        this.qty = qty;
        this.modelVarient = modelVarient;
        this.vehicleModel = vehicleModel;
        this.spare = spare;
        this.remarks = remarks;
        this.indent_id = indent_id;
    }

    @SerializedName("indent_type")
    private String indentType;

    @SerializedName("color")
    private String color;

    @SerializedName("assignName")
    private String assignName;

    @SerializedName("qty")
    private String qty;

    @SerializedName("model_varient")
    private String modelVarient;

    @SerializedName("vehicle_model")
    private String vehicleModel;

    @SerializedName("spares")
    private String spare;

    @SerializedName("request_to")
    private String requestTo;

    @SerializedName("assignTo")
    private String assignTo;

    @SerializedName("remarks")
    private String remarks;


    @SerializedName("indent_id")
    private int indent_id;

    public int getIndent_id() {
        return indent_id;
    }

    public void setIndent_id(int  indent_id) {
        this.indent_id = indent_id;
    }

    public void setIndentType(String indentType){
        this.indentType = indentType;
    }

    public String getIndentType(){
        return indentType;
    }

    public void setColor(String color){
        this.color = color;
    }

    public String getColor(){
        return color;
    }

    public void setAssignName(String assignName){
        this.assignName = assignName;
    }

    public String getAssignName(){
        return assignName;
    }

    public void setQty(String qty){
        this.qty = qty;
    }

    public String getQty(){
        return qty;
    }

    public void setModelVarient(String modelVarient){
        this.modelVarient = modelVarient;
    }

    public String getModelVarient(){
        return modelVarient;
    }

    public void setVehicleModel(String vehicleModel){
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleModel(){
        return vehicleModel;
    }

    public void setSpare(String spare){
        this.spare = spare;
    }

    public String getSpare(){
        return spare;
    }

    public void setRequestTo(String requestTo){
        this.requestTo = requestTo;
    }

    public String getRequestTo(){
        return requestTo;
    }

    public void setAssignTo(String assignTo){
        this.assignTo = assignTo;
    }

    public String getAssignTo(){
        return assignTo;
    }

    public void setRemarks(String remarks){
        this.remarks = remarks;
    }

    public String getRemarks(){
        return remarks;
    }

    @Override
    public String toString(){
        return
                "IndentPojos{" +
                        "indent_type = '" + indentType + '\'' +
                        ",serviceType = '" + color + '\'' +
                        ",assignName = '" + assignName + '\'' +
                        ",qty = '" + qty + '\'' +
                        ",model_varient = '" + modelVarient + '\'' +
                        ",vehicle_model = '" + vehicleModel + '\'' +
                        ",spare = '" + spare + '\'' +
                        ",request_to = '" + requestTo + '\'' +
                        ",assignTo = '" + assignTo + '\'' +
                        ",remarks = '" + remarks + '\'' +
                        ",indent_id = '" + indent_id + '\'' +
                        "}";
    }
}
