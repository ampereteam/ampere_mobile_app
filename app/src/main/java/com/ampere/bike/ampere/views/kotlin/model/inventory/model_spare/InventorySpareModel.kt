package com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare

data class InventorySpareModel(
        val success: Boolean,
        val data: Data,
        val message: String
)