package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.SpareServcieReplacementAdapter

import android.content.Context
import android.support.design.widget.TextInputEditText
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.SpareServiceRepalcementServerPojos.SpareServiceserverPojo
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.Spareservicereplaement
import com.ampere.vehicles.R
import customviews.CustomTextEditView

class SpareServiceReplacementAdapter(val context: Context,val spareServiceArrayList: ArrayList<Spareservicereplaement?>?,val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<SpareServiceReplacementAdapter.ViewHOlder>(){
    var spareServiceReplacemenSTRList : ArrayList<String> = ArrayList()
    var spareServiceReplacementSTRAdapter :ArrayAdapter<String> ?= null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpareServiceReplacementAdapter.ViewHOlder {
        val view = LayoutInflater.from(this.context).inflate(R.layout.adpter_spare_service_replacement,parent,false)
        return ViewHOlder(view)
    }

    override fun getItemCount(): Int {return this.spareServiceArrayList?.size!! }

    override fun onBindViewHolder(holder: SpareServiceReplacementAdapter.ViewHOlder, position: Int) {
        try{
            val currentposition =position
            for(spareServce  in spareServiceArrayList!![position]?.sparesstock!!){
                spareServiceReplacemenSTRList.add(spareServce?.serialNo!!)
            }
            spareServiceReplacemenSTRList.add(0,"Select The Serial No")
            holder.componentName.setText(spareServiceArrayList[position]?.component)
            holder.oldeserial.setText(spareServiceArrayList[position]?.oldserial)
            spareServiceReplacementSTRAdapter = ArrayAdapter(context,R.layout.spinnertext,R.id.text1,spareServiceReplacemenSTRList)
            holder.spNewSerial.adapter =spareServiceReplacementSTRAdapter
            holder.spNewSerial.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    try{
                        Log.d("NothingSelected",""+parent?.selectedItem)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    try{
                        val selecedItem = holder.spNewSerial.selectedItem.toString()
                        holder.edtNewSerial.setText(selecedItem)
                        if(selecedItem.equals("Select The Serial No")){
                            Log.d("SelectedItem",""+selecedItem)
                        }else{
                            val spareServiceserverPojo = SpareServiceserverPojo(holder.componentName.text?.toString(),holder.oldeserial.text.toString(),
                                    holder.spNewSerial.selectedItem.toString(),spareServiceArrayList[currentposition]?.sparesservicecomplaintId)
                            CRMDetailsActivity.spareServiceServerList[currentposition] = spareServiceserverPojo
                            Log.d("SPareService",""+CRMDetailsActivity.spareServiceServerList[currentposition].component)
                            Log.d("SPareService",""+CRMDetailsActivity.spareServiceServerList[currentposition].oldserial)
                            Log.d("SPareService",""+CRMDetailsActivity.spareServiceServerList[currentposition].newserial)
                            Log.d("SPareService",""+CRMDetailsActivity.spareServiceServerList[currentposition].id)
                        }
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }
            }
            for(spareServcieComponent  in spareServiceArrayList){
                val spareServiceserverPojo = SpareServiceserverPojo(spareServcieComponent?.component,spareServcieComponent?.oldserial,
                       " ", spareServcieComponent?.sparesservicecomplaintId)
                Log.d("sparesServiceServer",""+spareServcieComponent?.component)
                Log.d("sparesServiceServer",""+spareServcieComponent?.oldserial)
                Log.d("sparesServiceServer",""+spareServcieComponent?.sparesstock!![0]?.serialNo)
                CRMDetailsActivity.spareServiceServerList.add(spareServiceserverPojo)
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class ViewHOlder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val componentName: TextInputEditText= itemView?.findViewById(R.id.edt_component_name_spare_service)!!
        val oldeserial: TextInputEditText= itemView?.findViewById(R.id.bs_edt_old_serial_number_spare_service)!!
        val edtNewSerial : CustomTextEditView =itemView?.findViewById(R.id.bs_edt_new_serial_number_spare_service)!!
        val spNewSerial : Spinner =itemView?.findViewById(R.id.bs_sp_new_serial_number_spare_service)!!

    }
}