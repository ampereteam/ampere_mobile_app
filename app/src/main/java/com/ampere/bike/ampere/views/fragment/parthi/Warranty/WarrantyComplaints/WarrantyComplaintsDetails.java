package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.api.Retrofit.ResponsebackToClass;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints.Adapter.WarrantyComplaintsAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints.Pojos.Warrantycomplaint;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyHome;
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.Image;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyHome.snackView;


@SuppressLint("Registered")
public class WarrantyComplaintsDetails extends AppCompatActivity implements ResponsebackToClass,ConfigClickEvent {
    RecyclerView warrantyComplaintsList;
    WarrantyComplaintsAdapter warrantyComplaintsAdapter;
    Spinner spinner;
    String complntId;
    TextView textView;

    ArrayList<Warrantycomplaint> warrantycomplaintsArrayList ;
    AlertDialog dialog;
    ArrayList<String> sendthrough = new ArrayList<>();
    LinearLayout  linearLayout ;
    ArrayAdapter<String> arrayAdapter;
    Common common;
    EditText docket,vide,driverName,drMobileNo;
    String complaintid ;
    ImageView backSymbol;
    View view;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            getSupportActionBar().hide();
        }catch (Exception e){
            e.printStackTrace();
        }
        setContentView(R.layout.activity_warranty_complaint_details);
        try{
            complaintid = getIntent().getStringExtra("complaintid");
        }catch (Exception e){
            e.printStackTrace();
        }
            common = new Common(this, this);
            sendthrough.clear();
            sendthrough.add("Send Through");
            sendthrough.add("Courier");
            sendthrough.add("Direct");
            sendthrough.add("Transport");
            warrantyComplaintsList = findViewById(R.id.warranty_complaint_list);
            linearLayout = findViewById(R.id.main);
            textView = findViewById(R.id.emptyList);
            backSymbol = findViewById(R.id.ic_back_wrntydetails);
            backSymbol.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
          getResponseSVR();
            warrantyComplaintsList.setLayoutManager(new LinearLayoutManager(this));

        /* view.setFocusableInTouchMode(true);
       view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    this.getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(this, CustomNavigationDuo.class);
//                    // i.putExtra("Comingback", cameback);
                    i.putExtra("SELECTEDVALUE", 0);
                    startActivity(i);
                    return true;
                } else {
                    return false;
                }
            }
        });*/

    }

    private void getResponseSVR() {
    try{
        HashMap<String, String> map = new HashMap<>();
//        map.put("userid",sharedPreferences.getString("id",""));sharedPreferences.getvalue(ID);
        if (LocalSession.getUserInfo(this, KEY_ID) != null) {
            map.put("userid", LocalSession.getUserInfo(this, KEY_ID));
            map.put("complaintid", complaintid);
        } else {
            Log.v("ngsliv4er hg4   Id", LocalSession.getUserInfo(this, KEY_ID));
        }
        if (common.isNetworkConnected()) {
            common.showLoad(true);
            common.callApiRequest("warrantycomplaints", "POST", map, 0);
        } else {
            common.hideLoad();
            MessageUtils.showSnackBarAction(Objects.requireNonNull(this), snackView, getString(R.string.check_internet));
//            Toast.makeText(this, R.string.data, Toast.LENGTH_SHORT).show();
        }
    }catch (Exception ex){
        ex.printStackTrace();
    }
    }


    public  void  popmenu(int configposition){
        try {
            LayoutInflater inflater = (LayoutInflater) Objects.requireNonNull(this).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
             view = Objects.requireNonNull(inflater).inflate(R.layout.dialog_wanty_cmplt_send, null);
            spinner = view.findViewById(R.id.sendthrough);
            final LinearLayout linearLayout = view.findViewById(R.id.courier);
            docket = view.findViewById(R.id.docket);
            vide = view.findViewById(R.id.vide);
            driverName = view.findViewById(R.id.dr_name);
            drMobileNo = view.findViewById(R.id.mob_no);
            final Button sendToPlant = view.findViewById(R.id.snd_plnt);
            final Button close = view.findViewById(R.id.close);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setView(view);
            alertDialogBuilder.setCancelable(true);
            arrayAdapter = new ArrayAdapter<String>(this, R.layout.spinnertext,R.id.text1, sendthrough);
            dialog = alertDialogBuilder.create();
            spinner.setAdapter(arrayAdapter);
            dialog.show();

            sendToPlant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        /*if(dialog.isShowing()){
                            dialog.dismiss();
                        }else{
                            dialog.dismiss();

                        }*/
                    sendTOServer();
                }
            });
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();

                    } else {
                        dialog.dismiss();

                    }
                }
            });
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    final String selectstring = spinner.getSelectedItem().toString();
                    switch (selectstring) {
                        case "Courier":
                            docket.setVisibility(View.VISIBLE);
                            vide.setVisibility(View.VISIBLE);
                            driverName.setVisibility(View.GONE);
                            drMobileNo.setVisibility(View.GONE);
                            break;
                        default:
                            docket.setVisibility(View.GONE);
                            vide.setVisibility(View.GONE);
                            driverName.setVisibility(View.GONE);
                            drMobileNo.setVisibility(View.GONE);
                            break;
                        case "Transport":
                            docket.setVisibility(View.GONE);
                            vide.setVisibility(View.GONE);
                            driverName.setVisibility(View.VISIBLE);
                            drMobileNo.setVisibility(View.VISIBLE);
                            break;
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void sendTOServer()
    {
        try{

            HashMap<String,String> map = new HashMap<>();
            map.put("userid",LocalSession.getUserInfo(this,KEY_ID));
            map.put("complaintid",complntId);
            map.put("send_through",spinner.getSelectedItem().toString());
            if(spinner.getSelectedItem().equals("Direct")){
                Log.v("spinnerSelectedItem",""+spinner.getSelectedItem());
                if(common.isNetworkConnected()){
                    common.showLoad(true);
                    System.out.println("Params" + map);
                    common.callApiRequest("sendforservice","POST",map,0);
                }else{
                    common.hideLoad();
                    MessageUtils.showSnackBarAction(this,view,getString(R.string.check_internet));
//            Toast.makeText(this, R.string.data, Toast.LENGTH_SHORT).show();
                }
            }else if(spinner.getSelectedItem().equals("Send Through")){
                MessageUtils.showSnackBar(this,view,"Select Send Through");
            }else if(spinner.getSelectedItem().equals("Transport")){
                if (drMobileNo.getText().toString().matches("") ||
                        drMobileNo.getText() == null  || drMobileNo.getText().length() !=10) {
                    drMobileNo.setError("Required");
                    MessageUtils.showSnackBar(this,view,getString(R.string.mobile_number_not_valid));
                } else {
                    if (driverName.getText().toString().matches("") || driverName.getText() == null) {
                        driverName.setError("Required");
                        MessageUtils.showSnackBar(this,view,getString(R.string.drive_name_cantempty));
                    } else {
                        map.put("Driver_phone_no", drMobileNo.getText().toString());
                        map.put("driver_name", driverName.getText().toString());
                        if(common.isNetworkConnected()){
                            common.showLoad(true);
                            System.out.println("Params" + map);
                            common.callApiRequest("sendforservice","POST",map,0);
                        }else{
                            common.hideLoad();
                            MessageUtils.showSnackBarAction(this,view,getString(R.string.check_internet));
//            Toast.makeText(this, R.string.data, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }else{
                if(docket.getText() == null ||  docket.getText().toString().matches("")) {
                    MessageUtils.showSnackBar(this,view,getString(R.string.docket_not_valid));
                }else{
                    if (vide.getText().toString().matches("") || vide.getText() == null) {
                        vide.setError("Required");
                        MessageUtils.showSnackBar(this,view,getString(R.string.vide_not_valid));
                    } else {
                        map.put("docket", docket.getText().toString());
                        map.put("vide", vide.getText().toString());
                        if(common.isNetworkConnected()){
                            common.showLoad(true);
                            System.out.println("Params" + map);
                            common.callApiRequest("sendforservice","POST",map,0);
                        }else{
                            common.hideLoad();
                            MessageUtils.showSnackBarAction(this,view,getString(R.string.check_internet));
//            Toast.makeText(this, R.string.data, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void apiCallBackOverRideMethod(Response<ResponseBody> response, JSONArray objResponse, String method, int position, String message, boolean successs) throws JSONException, IOException {
        common.hideLoad();
        try {
            if (objResponse != null) {
                if (method.equals("warrantycomplaints")) {
                    warrantycomplaintsArrayList = new Gson().fromJson(objResponse.toString(), new TypeToken<ArrayList<Warrantycomplaint>>() {
                    }.getType());
                    if (warrantycomplaintsArrayList.size() > 0) {
                        linearLayout.setVisibility(View.VISIBLE);
                        textView.setVisibility(View.GONE);
                        warrantyComplaintsAdapter = new WarrantyComplaintsAdapter(this, warrantycomplaintsArrayList, this);
                        warrantyComplaintsList.setAdapter(warrantyComplaintsAdapter);
                    } else {
                        textView.setText(message);
                        textView.setVisibility(View.VISIBLE);
                    }


                }

            } else if (method != null && method.equals("sendforservice")) {
                if (successs) {
                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();

                    }
                    getResponseSVR();
                    //startActivity(new Intent(this, WarrantyHome.class));

                  /* try{
                       MyUtils.passFragmentBackStack(this, new WarrantyHome());
                   }catch (Exception ex){
                        ex.printStackTrace();
                   }*/
                } else {
                    Log.v("info", "" + successs);
                }

            } else {
                linearLayout.setVisibility(View.GONE);
            }
        }catch (Exception d){
            d.printStackTrace();
        }
    }
    @Override
    public void connectposition(int position,int listSize) {
        try{
            String status=warrantycomplaintsArrayList.get(position).getStatus();
            String cmpletests=warrantycomplaintsArrayList.get(position).getComplaintstatus();
            String replacement_type=warrantycomplaintsArrayList.get(position).getReplacementType();
            complntId=warrantycomplaintsArrayList.get(position).getId().toString();
            System.out.println("Status:"+status);
            System.out.println("Status complainryt:"+cmpletests);
            System.out.println("Status complainryt replacement_type:"+replacement_type);
//            popmenu(position);//Testing Purpose
            if(replacement_type.equals("Service Replacement") && status.equals("Approved")&& cmpletests.equals("Send to Plant")){
                popmenu(position);
                warrantyComplaintsAdapter.notifyDataSetChanged();
            }else if(replacement_type.equals("")){
                //  popmenu(position);
                System.out.println("Status:"+status);
                System.out.println("Status complainryt:"+cmpletests);
                System.out.println("Status complainryt replacement_type:"+replacement_type);
            }else if(cmpletests.equals("View")){
                    Intent  intent = new  Intent(this, CRMDetailsActivity.class);
                    intent.putExtra("id", "" +warrantycomplaintsArrayList.get(position).getEnquiryId());
                    intent.putExtra("user_name", "" + warrantycomplaintsArrayList.get(position).getComplaint());
                    intent.putExtra("task_name", "" + getString(R.string.service));
                    startActivity(intent);

              /* val intent = Intent(this, CRMDetailsActivity::class.java)
               intent.putExtra("id", "" +complaintDtilsRes?.data?.freecomplaints?.get(position)?.enquiryId)
               intent.putExtra("user_name", "" + complaintDtilsRes?.data?.freecomplaints?.get(position)?.complaint)
               intent.putExtra("task_name", "" + complaintDtilsRes?.data?.freecomplaints?.get(position)?.complaintstatus)
               startActivity(intent)*/

            }else{/*
                Intent  intent = new  Intent(this, CRMDetailsActivity.class);
                intent.putExtra("id", "" +warrantycomplaintsArrayList.get(position).getEnquiryId());
                intent.putExtra("user_name", "" + warrantycomplaintsArrayList.get(position).getComplaint());
                intent.putExtra("task_name", "" + warrantycomplaintsArrayList.get(position).getComplaintstatus());
                startActivity(intent);*/
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void onBackPressed() {
        if (Objects.requireNonNull(getFragmentManager()).getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else
           finish();
    }
}
