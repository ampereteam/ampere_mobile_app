package com.ampere.bike.ampere.utils;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.EditText;

import com.ampere.bike.ampere.api.RetrofitCall;
import com.ampere.bike.ampere.api.RetrofitService;
import com.ampere.bike.ampere.model.task.EnquiryModel;
import com.ampere.bike.ampere.nav.NavMenuModel;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare.Sparedelivered;
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_spare.Sparedespatched;
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_vehicle.Vehicledelivered;
import com.ampere.bike.ampere.views.kotlin.model.inventory.model_vehicle.Vehicledespatched;
import com.ampere.bike.ampere.views.kotlin.model.unassinged.UnAssignEnquiryModel;
import com.ampere.vehicles.R;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import static com.ampere.bike.ampere.shared.LocalSession.T_CRM;
import static com.ampere.bike.ampere.shared.LocalSession.T_DEALER;
import static com.ampere.bike.ampere.shared.LocalSession.T_SPECIAL_OFFICER;
import static com.itextpdf.text.factories.GreekAlphabetFactory.getString;

public class MyUtils {


    public static boolean isSelect = false;
    public static String CAL_MONTH = "";
    public static String CAL_YEAR = "";

    public static String LEAVE_CAL_DATE = "";
    public static String LEAVE_CAL_MONTH = "";
    public static String LEAVE_CAL_YEAR = "";

    public static ArrayList<EnquiryModel> SALES = new ArrayList<EnquiryModel>();
    public static ArrayList<EnquiryModel> SERVICE = new ArrayList<EnquiryModel>();
    public static ArrayList<EnquiryModel> SPARES = new ArrayList<EnquiryModel>();
    public static ArrayList<EnquiryModel> SPARESSERVICE = new ArrayList<EnquiryModel>();


    public static ArrayList<UnAssignEnquiryModel> SALES_UN_ASSIGN = new ArrayList<>();
    public static ArrayList<UnAssignEnquiryModel> SERIVCE_UN_ASSIGN = new ArrayList<>();
    public static ArrayList<UnAssignEnquiryModel> SPARES_UN_ASSIGN = new ArrayList<>();

    public static String VIEW_ITEM = "";

    //Pending Vehicle
    public static ArrayList<Vehicledespatched> DISPATCHED_VHICLE = new ArrayList<>();
    //Accepted Vehicle
    public static ArrayList<Vehicledelivered> DELIVER_VEHICLE = new ArrayList<>();

    //Pending Spare
    public static ArrayList<Sparedespatched> DISPATCHED_SPARE = new ArrayList<>();
    //Accepted Spare
    public static ArrayList<Sparedelivered> DELIVER_SPARE = new ArrayList<>();
    @NotNull
    public static final String SELECT_ADDRESS_PROOF = "Select Address Proof";
    public static final String SELECT_ID_PROOF = "Select Id Proof";
    public static final String SELECT_VEHICLE_MODEL = "Select Vehicle Model";
    public static final String SELECT_STATE_NAME = "Select State Name";
    public static final String SELECT_VARIANT_TYPE = "Select Variant Type";
    public static final String SELECT_COLOR = "Select Color";
    public static final String SELECT_CHASSIS_NUMBER = "Select Chassis Number";

    /**
     * Set Navigation ListView Values
     *
     * @param activity
     * @return
     */

    public static NavMenuModel[] setNavigationView(FragmentActivity activity) {
        NavMenuModel[] nav_3_modalClasses = null;
        if (LocalSession.getUserInfo(activity, LocalSession.KEY_USER_TYPE).equals(T_CRM)) {

            /*nav_3_modalClasses = new NavMenuModel[]{new NavMenuModel(R.drawable.feed, "HOME", ""),
                    new NavMenuModel(R.drawable.chat, "ENQUIRY", "37"), new NavMenuModel(R.drawable.logout, "LOGOUT", "")};*/


            nav_3_modalClasses = new NavMenuModel[]{new NavMenuModel(R.drawable.feed, "HOME", ""), new NavMenuModel(R.drawable.chat, "ENQUIRY", ""),
                    new NavMenuModel(R.drawable.ic_indent, "INDENT DETAILS", ""), new NavMenuModel(R.drawable.ic_sales_customer, "SALES DETAILS", ""),
                   /* new NavMenuModel(R.drawable.ic_stock_invent, "STOCK DETAILS", "true"),*/ new NavMenuModel(R.drawable.ic_stock_invent, "UNASSIGNED TASK", ""),
                    new NavMenuModel(R.drawable.logout, "LOGOUT", "")};


        } else if (LocalSession.getUserInfo(activity, LocalSession.KEY_USER_TYPE).equals(T_SPECIAL_OFFICER)) {
            nav_3_modalClasses = new NavMenuModel[]{new NavMenuModel(R.drawable.feed, "HOME", ""), new NavMenuModel(R.drawable.chat, "ENQUIRY", ""),
                    new NavMenuModel(R.drawable.ic_enquiry_, "MY ENQUIRY", ""), /*new NavMenuModel(R.drawable.setting, "INDENT DETAILS", ""),*/
                    new NavMenuModel(R.drawable.logout, "LOGOUT", "")};

            /*nav_3_modalClasses = new NavMenuModel[]{new NavMenuModel(R.drawable.feed, "HOME", ""), new NavMenuModel(R.drawable.chat, "ENQUIRY", "37"),
                    new NavMenuModel(R.drawable.ic_indent, "INDENT DETAILS", ""), new NavMenuModel(R.drawable.ic_sales_customer, "SALES DETAILS", ""),
                    new NavMenuModel(R.drawable.ic_stock_invent, "STOCK DETAILS", ""), new NavMenuModel(R.drawable.logout, "LOGOUT", "")};*/

        } else if (LocalSession.getUserInfo(activity, LocalSession.KEY_USER_TYPE).equals(T_DEALER)) {

            nav_3_modalClasses = new NavMenuModel[]{new NavMenuModel(R.drawable.feed, "HOME", ""),
                    new NavMenuModel(R.drawable.chat, "ENQUIRY", ""),
                    new NavMenuModel(R.drawable.ic_indent, "INDENT DETAILS", ""),
                    new NavMenuModel(R.drawable.ic_sales_customer, "SALES DETAILS", "true")
                    /*,new NavMenuModel(R.drawable.ic_stock_invent, "STOCK DETAILS", "true")*/
                    , new NavMenuModel(R.drawable.inventory_logo, "INVENTORY", "")
//                    , new NavMenuModel(R.drawable.ic_spare, "SPARE DETAILS", "")
                    , new NavMenuModel(R.drawable.warranty_logo, "WARRANTY", "")
                    , new NavMenuModel(R.drawable.free_replacement_spare, "Free Replacement".toUpperCase(), "")
                    , new NavMenuModel(R.drawable.spare_serivce_logo, "Spare Service".toUpperCase(), "")
                    , new NavMenuModel(R.drawable.user_management, "user Management".toUpperCase(), "")
                    , new NavMenuModel(R.drawable.unassignment, "Assign Enquiries".toUpperCase(), "")
                    , new NavMenuModel(R.drawable.logout, "LOGOUT", "")

            };

        } else {

            /*nav_3_modalClasses = new NavMenuModel[]{new NavMenuModel(R.drawable.feed, "HOME", ""), new NavMenuModel(R.drawable.chat, "ENQUIRY", "37"),
                    new NavMenuModel(R.drawable.ic_indent, "INDENT DETAILS", ""), new NavMenuModel(R.drawable.logout, "LOGOUT", "")};*/

            nav_3_modalClasses = new NavMenuModel[]{new NavMenuModel(R.drawable.feed, "HOME", ""), new NavMenuModel(R.drawable.chat, "ENQUIRY", ""),
                    new NavMenuModel(R.drawable.ic_indent, "INDENT DETAILS", ""), new NavMenuModel(R.drawable.ic_sales_customer, "SALES DETAILS", ""),
                 /*   new NavMenuModel(R.drawable.ic_stock_invent, "STOCK DETAILS", "true"), */new NavMenuModel(R.drawable.logout, "LOGOUT", "")};
        }

        return nav_3_modalClasses;
    }

    /**
     * Pass fragment without back stack
     *
     * @param fragmentActivity
     * @param fragment
     */
    public static void passFragmentWithoutBackStack(FragmentActivity fragmentActivity, Fragment fragment) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

    }


    public static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    /**
     * Pass Fragment with Back Stack Mode
     *
     * @param fragmentActivity
     * @param fragment
     */
    public static void passFragmentBackStack(FragmentActivity fragmentActivity, Fragment fragment) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();

    }

    /**
     * Check Build Version
     *
     * @return
     */
    public static int checkVersion() {
        int currentapiVersion = Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            //view.setBackgroundResource(R.drawable.ripple_center);
            return 1;
        } else {
            return 0;
        }
    }


    /**
     * Init Retrofit
     *
     * @return
     */
    public static RetrofitService getRetroService() {
        return RetrofitCall.getClient().create(RetrofitService.class);
    }

    /**
     * Check Two Digit number
     *
     * @param inputDigits
     * @return
     */
    public static String checkTwoDigitNumber(int inputDigits) {
        if (inputDigits <= 9) {
            return "0" + inputDigits;
        } else {
            return String.valueOf(inputDigits);
        }
    }

    /**
     * Sorting Decending
     *
     * @param sortingValues
     * @return
     */

    public static ArrayList<HashMap<String, String>> sortingZ_A(ArrayList<HashMap<String, String>> sortingValues) {
        Collections.sort(sortingValues, new Comparator<HashMap<String, String>>() {
            @Override
            public int compare(HashMap<String, String> o1, HashMap<String, String> o2) {
                return o2.get("dates").compareTo(o1.get("dates"));
            }
        });

        return sortingValues;
    }


    public static List<EnquiryModel> sortingZA(List<EnquiryModel> enquiryModels) {
        Collections.sort(enquiryModels, new Comparator<EnquiryModel>() {
            @Override
            public int compare(EnquiryModel enquiryModel, EnquiryModel enquiryModel2) {
                return enquiryModel2.getAssignedDate().compareTo(enquiryModel.getAssignedDate());
            }
        });
        return enquiryModels;
    }


    public static void darkenStatusBar(Activity activity, int color) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            activity.getWindow().setStatusBarColor(darkenColor(ContextCompat.getColor(activity, color)));
        }

    }


    // Code to darken the serviceType supplied (mostly serviceType of toolbar)
    private static int darkenColor(int color) {
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        hsv[2] *= 0.8f;
        return Color.HSVToColor(hsv);
    }


    /**
     * Check Empty Values
     */

    public static boolean isEmpty(FragmentActivity appCompatActivity, String input, String errorMessage) {
        if (input.isEmpty()) {
            MessageUtils.showToastMessage(appCompatActivity, errorMessage);
            return true;
        } else {
            return false;
        }
    }


    /**
     * Check Empty Values
     */

    public static boolean isEmptySnackView(FragmentActivity appCompatActivity, View view, String input, String errorMessage) {
        if (input.isEmpty()) {
            MessageUtils.showSnackBar(appCompatActivity, view, errorMessage);
            return true;
        } else {
            return false;
        }
    }

    public static RetrofitService getInstance() {
        return RetrofitCall.getClient().create(RetrofitService.class);
    }


    public static Typeface getTypeface(FragmentActivity activity, String font) {
        return Typeface.createFromAsset(activity.getAssets(), font);
    }


    /**
     * ConvertInputStream to String
     */

    public static String convertStreamToString(InputStream is) throws IOException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        return sb.toString();
    }


    @SuppressLint("ResourceAsColor")
    public static void focusable(EditText editText, boolean status) {
        editText.setFocusableInTouchMode(status);
        editText.setLongClickable(status);
        editText.setFocusable(status);
        if (!status) {
            editText.setBackgroundColor(android.R.color.transparent);
        } else {
            editText.setBackgroundColor(android.R.color.black);
        }
    }

    public static String nullPointerValidation(@Nullable String inputStr) {
        try {

            //Log.d("Whatss", "" + inputStr);



            /*if (inputStr != null) {
                Log.d("Whatss", "0000sss");
            } else if (inputStr == null) {
                Log.d("Whatss", "0011ssss");
            }

            if (inputStr.equals(null)) {
                Log.d("Whatss", "1111");
            } else if (!inputStr.equals(null)) {
                Log.d("Whatss", "1100");
            }
*/

            if (inputStr == null) {
                return "-";
            } else if (inputStr.isEmpty()) {
                return "-";
            } else {
                return inputStr;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return inputStr;
        }

    }


    public static Typeface setType(FragmentActivity activity, String fontType) {
        return Typeface.createFromAsset(activity.getAssets(), String.valueOf(fontType));
    }
}
