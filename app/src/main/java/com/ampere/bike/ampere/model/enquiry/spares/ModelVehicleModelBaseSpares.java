package com.ampere.bike.ampere.model.enquiry.spares;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelVehicleModelBaseSpares {

    @SerializedName("success")
    public boolean success;

    @SerializedName("message")
    public String message;

/*
    @SerializedName("varient")
    public List<Varient> varients;
*/

    @SerializedName("data")
    public SpareList data;

    public class Varient {
        @SerializedName("varient")
        public String varient;
    }

    public class SpareList {
        @SerializedName("spareslist")
      public   List<SparesList> sparesLists;
    }

    public class SparesList {
        @SerializedName("id")
        public int id;
        @SerializedName("spare_code")
        public String spare_code;
        @SerializedName("spare_id")
        public String spare_id;
        @SerializedName("vehicle_model_id")
        public int vehicle_model_id;
        @SerializedName("model_varient_id")
        public String model_varient_id;
        @SerializedName("spares")
        public String spares;
    }
}