package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.ampere.bike.ampere.model.enquiry.edit.ModelServiceComponets;
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.serviceReplacementPojos.ServicereplacementPojos;
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.dynamicPaidcomponentPojos.DynamicPaidComponentPojos;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Data {

    @SerializedName("enquirydetails")
    public List<EnquiryDetails> enquirydetails;

    @SerializedName("service_rootcause")
    public List<ServiceRootCause> serviceRootCause;

    @SerializedName("enquiry")
    public TaskEnquiryModel enquiry;

    @SerializedName("tasks")
    public List<TasksItem> tasks;
    @SerializedName("enquirystatuslist")
    public List<EnquiryStatusList> enquirystatuslist;

    @SerializedName("enquirystatus")
    public List<EnquiryStatus> enquiryStatusList;

    @SerializedName("spares")
    public List<Spares> sparesList;

    @SerializedName("sparehistory")
    public ArrayList<SpareHistory> spareHistory;

    @SerializedName("warrantycomponents")
    public List<ModelServiceComponets> modelServiceComponets;


    /*** Only Shown in Service Enquiry Four Type Of Service in here Like New Replacement is array Name replacement
     *  Warranty Service Complaint     is servicereplacement  , Paid Service Array Name is  paid Components and  Free Replacement  arrayName  is Free Replacement
     */
    @SerializedName("replacement")
    @Expose
    private ArrayList<Replacement> replacementList;
    @SerializedName("paidcomponents")
    @Expose
    public ArrayList<DynamicPaidComponentPojos> paidcomponents = null;
    @SerializedName("servicereplacement")
    @Expose
    private ArrayList<ServicereplacementPojos> servicereplacement;
    @SerializedName("freereplacement")
    @Expose
    private ArrayList<Freereplacement> freereplacementArrayList;
    /**
     * Spare Service Replacement is used to SpareServiceReplacement
     */
    @SerializedName("spareservicereplacement")
    @Expose
    private ArrayList<Spareservicereplaement> spareservicereplaementArrayList;

    public ArrayList<Freereplacement> getFreereplacementArrayList() {
        return freereplacementArrayList;
    }

    public void setFreereplacementArrayList(ArrayList<Freereplacement> freereplacementArrayList) {
        this.freereplacementArrayList = freereplacementArrayList;
    }

    public ArrayList<Spareservicereplaement> getSpareservicereplaementArrayList() {
        return spareservicereplaementArrayList;
    }

    public void setSpareservicereplaementArrayList(ArrayList<Spareservicereplaement> spareservicereplaementArrayList) {
        this.spareservicereplaementArrayList = spareservicereplaementArrayList;
    }

    public ArrayList<Replacement> getReplacementList() {
        return replacementList;
    }

    public void setReplacementList(ArrayList<Replacement> replacementList) {
        this.replacementList = replacementList;
    }

    public ArrayList<ServicereplacementPojos> getServicereplacement() {
        return servicereplacement;
    }

    public void setServicereplacement(ArrayList<ServicereplacementPojos> servicereplacement) {
        this.servicereplacement = servicereplacement;
    }

    public ArrayList<DynamicPaidComponentPojos> getPaidcomponents() {
        return paidcomponents;
    }

    public void setPaidcomponents(ArrayList<DynamicPaidComponentPojos> paidcomponents) {
        this.paidcomponents = paidcomponents;
    }


    public List<EnquiryDetails> getEnquirydetails() {
        return enquirydetails;
    }

    public void setEnquirydetails(List<EnquiryDetails> enquirydetails) {
        this.enquirydetails = enquirydetails;
    }

    public class EnquiryStatus {
        @SerializedName("id")
        public String id;
        @SerializedName("enquiry_id")
        public String enquiry_id;
        @SerializedName("status")
        public String status;
        @SerializedName("completed_at")
        public String completed_at;
        @SerializedName("name")
        public String completed_by;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
    }

    public class Spares {
        @SerializedName("id")
        public String id;

        @SerializedName("enquiry_id")
        public String enquiry_id;

        @SerializedName("spares_id")
        public String spares_id;

        @SerializedName("spare_name")
        public String spare_name;


        @SerializedName("qty")
        public String qty;
    }

    public class SpareHistory {
        @SerializedName("id")
        public String id;
        @SerializedName("enquiry_id")
        public String enquiry_id;
        @SerializedName("chassis_no")
        public String chassis_no;
        @SerializedName("component")
        public String component;
        @SerializedName("old_serial_no")
        public String old_serial_no;
        @SerializedName("new_serial_no")
        public String new_serial_no;
        @SerializedName("warranty")
        public String warranty;
        @SerializedName("replaced_by")
        public String replaced_by;
        @SerializedName("changed_date")
        public String changed_date;

        @SerializedName("sent_for_replacement")
        public String sent_for_replacement;
        @SerializedName("created_at")
        public String created_at;
        @SerializedName("updated_at")
        public String updated_at;
    }
}