package com.ampere.bike.ampere.views.kotlin.fragments.SpareService.TapAdapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.ArrayList

class SpareServiceTapAdpter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {
    private val ffrFragmentList = ArrayList<Fragment>()
    private val ffrFragmentTitleList = ArrayList<String>()


    fun addFragment(fragment: Fragment, title: String) {
        ffrFragmentList.add(fragment)
        ffrFragmentTitleList.add(title)
    }

    override fun getItem(position: Int): Fragment {
        return ffrFragmentList[position]
    }


    override fun getCount(): Int {
        return ffrFragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ffrFragmentTitleList[position]
    }

}