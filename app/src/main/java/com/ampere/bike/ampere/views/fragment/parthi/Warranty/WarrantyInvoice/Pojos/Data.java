package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyInvoice.Pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Data{

	@SerializedName("warrantyinvoice")
	private ArrayList<WarrantyinvoiceItem> warrantyinvoice;

	public void setWarrantyinvoice(ArrayList<WarrantyinvoiceItem> warrantyinvoice){
		this.warrantyinvoice = warrantyinvoice;
	}

	public ArrayList<WarrantyinvoiceItem> getWarrantyinvoice(){
		return warrantyinvoice;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"warrantyinvoice = '" + warrantyinvoice + '\'' + 
			"}";
		}
}