package com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyDefects.Pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DefectsPojo {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("enquiry_id")
    @Expose
    private Integer enquiryId;
    @SerializedName("chassis_no")
    @Expose
    private String chassisNo;
    @SerializedName("component")
    @Expose
    private String component;
    @SerializedName("old_serial_no")
    @Expose
    private String oldSerialNo;
    @SerializedName("new_serial_no")
    @Expose
    private String newSerialNo;
    @SerializedName("warranty")
    @Expose
    private String warranty;
    @SerializedName("replaced_by")
    @Expose
    private Integer replacedBy;
    @SerializedName("changed_date")
    @Expose
    private String changedDate;
    @SerializedName("sent_for_replacement")
    @Expose
    private String sentForReplacement;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEnquiryId() {
        return enquiryId;
    }

    public void setEnquiryId(Integer enquiryId) {
        this.enquiryId = enquiryId;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getOldSerialNo() {
        return oldSerialNo;
    }

    public void setOldSerialNo(String oldSerialNo) {
        this.oldSerialNo = oldSerialNo;
    }

    public String getNewSerialNo() {
        return newSerialNo;
    }

    public void setNewSerialNo(String newSerialNo) {
        this.newSerialNo = newSerialNo;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public Integer getReplacedBy() {
        return replacedBy;
    }

    public void setReplacedBy(Integer replacedBy) {
        this.replacedBy = replacedBy;
    }

    public String getChangedDate() {
        return changedDate;
    }

    public void setChangedDate(String changedDate) {
        this.changedDate = changedDate;
    }

    public String getSentForReplacement() {
        return sentForReplacement;
    }

    public void setSentForReplacement(String sentForReplacement) {
        this.sentForReplacement = sentForReplacement;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    boolean check =false;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
