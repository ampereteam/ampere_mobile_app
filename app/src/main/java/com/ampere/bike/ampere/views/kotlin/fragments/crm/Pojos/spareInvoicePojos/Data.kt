package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoicePojos

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("spareinventory")
	val spareinventory: List<SpareinventoryItem?>? = null,
	@field:SerializedName("sparelists")
	val sparelists: List<SparelistsPojos?>? = null,

	@field:SerializedName("statelists")
	val statelists: List<StatelistsItem?>? = null,

	@field:SerializedName("proofs")
	val proofs: List<ProofsItem?>? = null,

	@field:SerializedName("gst")
	val gst: Gst? = null,

	@field:SerializedName("enquiry")
	val enquiry: Enquiry? = null,

	@field:SerializedName("colors")
	val colors: List<ColorsItem?>? = null,

	@field:SerializedName("vehiclemodels")
	val vehiclemodels: List<VehiclemodelsItem?>? = null
)