package com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrInvoice


import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.CustomNavigationDuo.snackVIew
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_ID
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils.*
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FreeReplacementHome.Companion.msnakview
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrInvoice.Adapter.FrrInVoiceAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrInvoice.Pojos.FrrInvoiceRes
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrInvoice.Pojos.AcceptItemPojo
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FrrInvoice.Pojos.FreereplacementinvoiceItem

import com.ampere.vehicles.R
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FreeReplacementInvoice : Fragment(),ConfigClickEvent {


    /**
     * Global Variable  Declaration
     */
    var recyInvocieList :RecyclerView ? =null
    var emptyList :TextView ? =null
    var aceplinear :LinearLayout ? =null
    var acepbtn :Button ? =null
    var callApi :UtilsCallApi?=null
    var dialog : Dialog?= null
    var  frrInvoiceRes :FrrInvoiceRes?=null
    var  acceptItemPojoList :ArrayList<AcceptItemPojo> =ArrayList()
    var frrInVoiceAdapter :FrrInVoiceAdapter ? =null
    var frrCheckBox :CheckBox ? =null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val  view:View =inflater.inflate(R.layout.fragment_free_replacement_invoice, container, false)
        recyInvocieList = view.findViewById(R.id.frrin_invocie_list)
        aceplinear = view.findViewById(R.id.ffrin_accept)
        acepbtn = view.findViewById(R.id.ffrin_acpt)
        emptyList = view.findViewById(R.id.frrc_emptyList)
        frrCheckBox = view.findViewById(R.id.frrin_checkbox)
        callApi = UtilsCallApi(activity)
        dialog = Dialog(activity)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            try{
                recyInvocieList?.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL,false)
                getDataFrmSrvr()
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        try{
            frrCheckBox?.setOnCheckedChangeListener { buttonView, isChecked ->
                if(isChecked){
                    if(frrInvoiceRes?.data?.freereplacementinvoice?.size!!>0){
                        for(i:Int in 0..frrInvoiceRes?.data?.freereplacementinvoice?.size!!-1){
                            frrInvoiceRes?.data?.freereplacementinvoice?.get(i)?.check =true
                        }
                        frrInVoiceAdapter?.notifyDataSetChanged()
                        aceplinear?.visibility =View.VISIBLE
                        acepbtn?.setText("Accept("+frrInvoiceRes?.data?.freereplacementinvoice?.size+")")
                    }

                }else{
                    if(frrInvoiceRes?.data?.freereplacementinvoice?.size!!>0){
                        for(i:Int in 0..frrInvoiceRes?.data?.freereplacementinvoice?.size!!-1){
                            frrInvoiceRes?.data?.freereplacementinvoice?.get(i)?.check =false
                        }
                        frrInVoiceAdapter?.notifyDataSetChanged()
                        aceplinear?.visibility =View.GONE
                        acepbtn?.setText("Accept(0)")
                    }
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
        try{
            acepbtn?.setOnClickListener{
                if(callApi?.isNetworkConnected!!){
                    sendTOSrvr()
                }else{
                    showSnackBarAction(activity,msnakview,getString(R.string.check_internet))
                }
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun sendTOSrvr() {
        try {
            Log.d("CalledFunction","123")
            for(i:Int in 0 .. frrInvoiceRes?.data?.freereplacementinvoice?.size!!-1){
                if(frrInvoiceRes?.data?.freereplacementinvoice?.get(i)?.check!!) {
                    Log.d("SpareName",""+frrInvoiceRes?.data?.freereplacementinvoice?.get(i)?.id.toString())
                    val acceptItemPojo = AcceptItemPojo(frrInvoiceRes?.data?.freereplacementinvoice?.get(i)?.id.toString())
                    acceptItemPojoList.add(acceptItemPojo)
                }
            }
            val hashMap:HashMap<String,Any> =HashMap()
            val map:HashMap<String,Any> = HashMap()
            hashMap.put("invoice", acceptItemPojoList)
            map.put("invoices",hashMap)
            map.put("userid",LocalSession.getUserInfo(activity, KEY_ID))
            val calinterface  = callApi?.connectRetro("POST","acceptfreereplacementinvoice")
            dialog = showDialog(activity)
            val callback = calinterface?.call_post("acceptfreereplacementinvoice","Bearer "+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
            callback?.enqueue(object :Callback<ResponseBody>{
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    dismissDialog(dialog)
                    showSnackBar(activity,snackVIew,getString(R.string.server_not_found))
                    Log.d("Failure",""+t.message)
                    Log.d("Failure",""+t.localizedMessage)
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    dismissDialog(dialog)
                    if(response.isSuccessful){
                        try{
                            frrInvoiceRes = Gson().fromJson(response.body()?.string(),FrrInvoiceRes::class.java)
                            if(frrInvoiceRes?.success!!){
                                aceplinear?.visibility =View.GONE
                                showToastMessageLong(activity,frrInvoiceRes?.message)
                                getDataFrmSrvr()
                            }else{
                                aceplinear?.visibility =View.GONE
                                showToastMessage(activity,frrInvoiceRes?.message)
                            }
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }

                    }else{
                        showSnackBar(activity,snackVIew,getString(R.string.server_not_found))
                        Log.d("ResponseFailure",""+response.body()?.string())
                    }
                }

            })

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun getDataFrmSrvr() {
        try{
            if(callApi?.isNetworkConnected!!){
                val map :HashMap<String,Any> = HashMap()
                map["userid"] =LocalSession.getUserInfo(activity, KEY_ID)
                val calinterface = callApi?.connectRetro("POST","freereplacementinvoice")
                        dialog = showDialog(activity)
                val callback  =calinterface?.call_post("freereplacementinvoice","Bearer "+LocalSession.getUserInfo(activity,KEY_TOKEN), map)
               callback?.enqueue(object :Callback<ResponseBody>{
                   override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                       dismissDialog(dialog)
                       showSnackBar(activity,snackVIew,getString(R.string.server_not_found))
                       Log.d("FailureResponse",""+t.localizedMessage)
                       Log.d("FailureResponse",""+t.message)
                   }

                   override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                       dismissDialog(dialog)
                       if(response.isSuccessful){
                           try{
                               frrInvoiceRes =Gson().fromJson(response.body()?.string(),FrrInvoiceRes::class.java)
                              /* val item = FreereplacementinvoiceItem(2,"REd","Audi",54,"6654564",
                                       "2431","true","2","20-53-2018",2,"12","Paid Service",4,"Wheel"
                               ,"654343544","20145-56-21","3",false)
                               frrInvoiceRes?.data?.freereplacementinvoice?.add(item)*/

                           }catch (ex:Exception){
                            ex.printStackTrace()
                           }
                            if(frrInvoiceRes?.data?.freereplacementinvoice?.size!! <=0){
                                emptyList?.text=frrInvoiceRes?.message
                                Log.d("FreeReplacement",""+frrInvoiceRes?.message)
                                emptyList?.visibility =View.VISIBLE
                            }else{
                                emptyList?.visibility =View.GONE
                                setToAdpter()
                            }

                       }else{
                           showSnackBar(activity,snackVIew,getString(R.string.server_not_found))
                           Log.d("Response ",""+response.body()?.string())
                       }

                   }

               })

            }else{
                showSnackBarAction(activity, msnakview,getString(R.string.check_internet))
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun setToAdpter() {
        try{
            frrInVoiceAdapter =FrrInVoiceAdapter(activity!!,frrInvoiceRes?.data?.freereplacementinvoice,this)
            recyInvocieList?.adapter = frrInVoiceAdapter
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
    override fun connectposition(position: Int, listSize: Int) {
        try{
            Log.d("CalledFunction",""+position+listSize)
            if(listSize <=0){
                aceplinear?.visibility =View.GONE
                acepbtn?.setText("Accept(0)")
            }else{
                aceplinear?.visibility =View.VISIBLE
                acepbtn?.setText("Accept("+listSize+")")
            }
            if(frrInvoiceRes?.data?.freereplacementinvoice?.get(position)?.check!!){
                Log.d("SpareName",""+frrInvoiceRes?.data?.freereplacementinvoice?.get(position)?.spareName)
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }
}
