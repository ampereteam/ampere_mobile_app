package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.dynamicAddPojo;

import com.google.gson.annotations.SerializedName;

public class ServiceReplecementViewPojos {


    public ServiceReplecementViewPojos(String oldsrlNo, String componentName, String newSerialNumber, String warranty_check, String replacement) {
        this.oldsrlNo = oldsrlNo;
        this.componentName = componentName;
        this.newSerialNumber = newSerialNumber;
        this.warranty_check = warranty_check;
        this.replacement = replacement;
    }

    @SerializedName("old_serial_number")
    String oldsrlNo;

    @SerializedName("component_name")
    String  componentName;

    @SerializedName("new_serial_number")
    String newSerialNumber;

    @SerializedName("warranty_check")
    String warranty_check;
    @SerializedName("replacement")
    String replacement;

    public String getReplacement() {
        return replacement;
    }

    public void setReplacement(String replacement) {
        this.replacement = replacement;
    }

    public String getWarranty_check() {
        return warranty_check;
    }


    public void setWarranty_check(String warranty_check) {
        this.warranty_check = warranty_check;
    }

    public String getOldsrlNo() {
        return oldsrlNo;
    }

    public void setOldsrlNo(String oldsrlNo) {
        this.oldsrlNo = oldsrlNo;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getNewSerialNumber() {
        return newSerialNumber;
    }

    public void setNewSerialNumber(String newSerialNumber) {
        this.newSerialNumber = newSerialNumber;
    }
}
