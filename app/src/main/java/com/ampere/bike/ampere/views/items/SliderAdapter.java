package com.ampere.bike.ampere.views.items;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ampere.bike.ampere.views.fragment.Home;
import  com.ampere.vehicles.R;

import java.util.List;
import java.util.Timer;

public class SliderAdapter extends PagerAdapter {

    private Context context;
    private List<Integer> color;
    private List<String> colorName;
    private Timer timer;
    private Home.SliderTimer sliderTimer;
    private boolean isLogClick;

    public SliderAdapter(Context context, List<Integer> color, List<String> colorName, Timer timer, Home.SliderTimer sliderTimer) {
        this.context = context;
        this.color = color;
        this.colorName = colorName;
        this.timer = timer;
        this.sliderTimer = sliderTimer;
    }

    @Override
    public int getCount() {
        return color.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_slider, null);

        TextView textView = (TextView) view.findViewById(R.id.textView);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);

        textView.setText(colorName.get(position));
        //textView.setBackgroundColor(serviceType.get(position));

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);
/*
        linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.d("clickEvent", "Long click " + sliderTimer);
                isLogClick = true;
                stopTimer(timer, sliderTimer);

                return false;
            }
        });
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("clickEvent", "click " + sliderTimer);
                if (isLogClick) {
                    timer = new Timer();

                    startTime(timer, sliderTimer);
                } else {
                    Toast.makeText(context, " Click ", Toast.LENGTH_SHORT).show();
                }

            }
        });*/

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
/*
    @Override
    public void startTime(Timer timer, Home.SliderTimer sliderTimer) {

        timer.schedule(sliderTimer, 200, 400);
    }

    @Override
    public void stopTimer(Timer timer, Home.SliderTimer sliderTimer) {
        Log.d("sliderTimer", "" + sliderTimer + "  " + timer);
        sliderTimer.cancel();
        timer.cancel();

    }*/
}