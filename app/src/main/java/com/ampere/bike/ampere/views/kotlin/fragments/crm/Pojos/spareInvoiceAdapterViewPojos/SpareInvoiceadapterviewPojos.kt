package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoiceAdapterViewPojos

import com.google.gson.annotations.SerializedName

import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
class SpareInvoiceadapterviewPojos(
        @SerializedName("sparelist")
        var spareNam: String = "",

        @SerializedName("spare_qty")
        var qty: String = "",

        @SerializedName("price_per_unit")
        var pricePerUnit: String = "",

        @SerializedName("spare_amount")
        var total: String = ""
        )

