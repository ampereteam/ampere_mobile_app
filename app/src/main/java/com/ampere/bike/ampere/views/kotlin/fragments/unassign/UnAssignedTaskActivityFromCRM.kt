package com.ampere.bike.ampere.views.kotlin.fragments.unassign

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.BEARER
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.utils.MyUtils.*
import com.ampere.bike.ampere.views.kotlin.fragments.unassign.sales.UnAssignSales
import com.ampere.bike.ampere.views.kotlin.fragments.unassign.service.UnAssignService
import com.ampere.bike.ampere.views.kotlin.fragments.unassign.spares.UnAssignSpares
import com.ampere.bike.ampere.views.kotlin.model.unassinged.UnAssginedTaskModel
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.activity_crm.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UnAssignedTaskActivityFromCRM : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_crm)

        supportActionBar?.hide()
        supportActionBar?.setTitle("Un Assigned Task")
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tool_bar_title.text = getString(R.string.un_assigned_task)
        searchView2.visibility = View.GONE

        txt_no_data.visibility = View.GONE
        container_child.visibility = View.GONE
        onTabCreatetion()
        onLoadUnAssignedTask()

        ic_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun onLoadUnAssignedTask() {

        var map = HashMap<String, String>()
        map.put("user_name", LocalSession.getUserInfo(this, LocalSession.KEY_USER_NAME))
        var unAssignedTaskModel: Call<UnAssginedTaskModel> = MyUtils.getRetroService().onUnAssignedTask(BEARER+LocalSession.getUserInfo(this, KEY_TOKEN),map)

        var dialog = MessageUtils.showDialog(this)
        dialog.setOnKeyListener { dialogInterface, i, keyEvent ->
            MessageUtils.dismissDialog(dialog);
            unAssignedTaskModel.cancel()
            false
        }

        unAssignedTaskModel.enqueue(object : Callback<UnAssginedTaskModel> {

            override fun onResponse(call: Call<UnAssginedTaskModel>?, response: Response<UnAssginedTaskModel>?) {
                MessageUtils.dismissDialog(dialog);
                if (response?.isSuccessful!!) {
                    var models = response.body()
                    if (models!!.data != null) {
                        SALES_UN_ASSIGN.clear()
                        SERIVCE_UN_ASSIGN.clear()
                        SPARES_UN_ASSIGN.clear()
                        Log.d("filterValue_before", "" + models!!.data.enquiry.size);
                        for (enquiryDetails in models!!.data.enquiry) {
                            var filterValue = enquiryDetails.enquiry_for
                            if (filterValue.equals(getString(R.string.sales))) {
                                SALES_UN_ASSIGN.add(enquiryDetails)
                            } else if (filterValue.equals(getString(R.string.service))) {
                                SERIVCE_UN_ASSIGN.add(enquiryDetails)
                            } else {
                                SPARES_UN_ASSIGN.add(enquiryDetails)
                            }
                        }

                        Log.d("filterValue", "SALES_UN_ASSIGN " + SALES_UN_ASSIGN.size + " SERIVCE_UN_ASSIGN " + SERIVCE_UN_ASSIGN.size + " SPARES_UN_ASSIGN " + SPARES_UN_ASSIGN.size)
                        onTabCreatetion()

                    } else {
                        MessageUtils.showSnackBar(applicationContext, main_content, models.message)

                    }

                } else {
                    var msg = MessageUtils.showErrorCodeToast(response.code())
                    MessageUtils.showSnackBar(applicationContext, main_content, msg)
                }
            }

            override fun onFailure(call: Call<UnAssginedTaskModel>?, t: Throwable?) {
                MessageUtils.dismissDialog(dialog);

                var msg = MessageUtils.showFailureToast(t?.message)
                MessageUtils.showSnackBar(applicationContext, main_content, msg)
            }

        })

    }

    private fun onTabCreatetion() {

        var tabValues = arrayListOf<String>("Sales", "Service", "Spares");
        viewpager.adapter = TabsItem(supportFragmentManager, tabValues)

        tabs.setupWithViewPager(viewpager)

        for (i: Int in 0..tabs.getTabCount() - 1) {
            //noinspection Constant Conditions
            var tv = TextView(this)
            tv.setTypeface(MyUtils.getTypeface(this, "fonts/Lato-Bold.ttf"));
            if (i == 0) {
                tv.setText("" + tabValues[i].toUpperCase())
            } else if (i == 1) {
                tv.setText("" + tabValues[i].toUpperCase())
            } else {
                tv.setText("" + tabValues[i].toUpperCase())
            }

            tv.setTextColor(this.resources.getColor(R.color.white))
            tv.gravity = Gravity.CENTER
            tabs.getTabAt(i)!!.setCustomView(tv);

        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        val ff = supportFragmentManager.findFragmentById(R.id.container_child)
        Log.d("when_back_press_child", "" + ff)
        if (ff is UnAssignSales || ff is UnAssignService || ff is UnAssignSpares) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    class TabsItem(fm: FragmentManager?, val values: ArrayList<String>) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    UnAssignSales()
                }
                1 -> {
                    UnAssignService()
                }
                2 -> {
                    UnAssignSpares()
                }
                else -> {
                    return UnAssignService()
                }
            }
        }

        override fun getCount(): Int {
            return values.size
        }

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> values[position]
                1 -> values[position]
                2 -> values[position]
                else -> {
                    return values[0]
                }
            }
        }
    }
}