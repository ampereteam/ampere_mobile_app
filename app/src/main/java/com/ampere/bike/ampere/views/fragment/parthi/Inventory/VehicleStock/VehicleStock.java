package com.ampere.bike.ampere.views.fragment.parthi.Inventory.VehicleStock;



import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ResponsebackToClass;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.views.fragment.parthi.Inventory.VehicleStock.Adapter.VehicleStockAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Inventory.VehicleStock.Pojos.VehicleStockPojo;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import okhttp3.ResponseBody;
import retrofit2.Response;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.views.fragment.parthi.Inventory.InventoryHome.msnakview;


/**
 * A simple {@link Fragment} subclass.
 */
public class VehicleStock extends Fragment implements ResponsebackToClass {
    RecyclerView vehicleStockList;
    Common common;
    TextView textView;
    ArrayList<VehicleStockPojo> vehicleStockPojosArrayList  =new ArrayList<>();
    VehicleStockAdapter vehicleStockAdapter;
    LinearLayout linearLayout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inventory_vehicle_stock,container,false);
            common = new Common(getActivity(),this);
        /*view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(getActivity(), CustomNavigationDuo.class);
//                    // i.putExtra("Comingback", cameback);
                    startActivity(i);


                    return true;
                } else {
                    return false;
                }
            }
        });*/
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        vehicleStockList = view.findViewById(R.id.vehicle_Stock_List);
        vehicleStockList.setLayoutManager(new LinearLayoutManager(getActivity()));
        textView = view.findViewById(R.id.emptyList);
        linearLayout = view.findViewById(R.id.main);
        callApi();
//    callApi();
    }

    public void callApi(){
        common.showLoad(true);
        if(common.isNetworkConnected()){
            HashMap<String,String> map = new HashMap<>();
            map.put("userid", LocalSession.getUserInfo(getActivity(),KEY_ID));
            common.callApiRequest("vehiclestock","POST",map,0);
        }else{
            common.hideLoad();
            MessageUtils.showSnackBarAction(Objects.requireNonNull(getActivity()),msnakview,getString(R.string.check_internet));
//            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void apiCallBackOverRideMethod(Response<ResponseBody> response, JSONArray objResponse, String method, int position, String message , boolean success)   {
        common.hideLoad();
        if(objResponse!=null){
            if(method.equals("vehiclestock")){
                try{

                    vehicleStockPojosArrayList= new Gson().fromJson(objResponse.toString(),new TypeToken<ArrayList<VehicleStockPojo>>(){}.getType());

                }catch (Exception e){
                    e.printStackTrace();
                }
                if(vehicleStockPojosArrayList.size()>0){
                    linearLayout.setVisibility(View.VISIBLE);
                    vehicleStockAdapter =new VehicleStockAdapter(vehicleStockPojosArrayList,getActivity());
                    vehicleStockList.setAdapter(vehicleStockAdapter);
                }else{
                    textView.setVisibility(View.VISIBLE);
                    textView.setText(message);
                }

            }
        }else{
            if(method ==null){
                linearLayout.setVisibility(View.GONE);
            }else{
//                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
//                Objects.requireNonNull(getActivity()).finish();
                MessageUtils.showSnackBar(Objects.requireNonNull(getActivity()),msnakview,"Server Not Found");
            }

        }

    }
}
