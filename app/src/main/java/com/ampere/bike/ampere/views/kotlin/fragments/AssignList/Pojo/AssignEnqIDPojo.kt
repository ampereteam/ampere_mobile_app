package com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo

import com.google.gson.annotations.SerializedName

class AssignEnqIDPojo(
        @field:SerializedName("id")
        var id:String?=null
)