package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.ServiceEnquiryAdapter

import android.content.Context
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.ServiceEnquiryServerPojo.FreeReplacementServerPojos
import com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate.Freereplacement
import com.ampere.vehicles.R

class  FreeServiceReplacementAdapter (val context: Context, val freeReplacementArrayList: ArrayList<Freereplacement?>? ,val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<FreeServiceReplacementAdapter.ViewHolder>(){
    var newSerialSTRList  :ArrayList<String> = ArrayList()
    var newSerialSTRAdapter:ArrayAdapter<String> ?=null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FreeServiceReplacementAdapter.ViewHolder {
        val view = LayoutInflater.from(this.context).inflate(R.layout.adapter_service_enquiry_free_replacement,parent,false)
        return  ViewHolder(view)
    }

    override fun getItemCount(): Int { return this.freeReplacementArrayList?.size!!}

    override fun onBindViewHolder(holder: FreeServiceReplacementAdapter.ViewHolder, position: Int) {
        try{
            var currentPosition = position
            holder.componet.setText(this.freeReplacementArrayList!![position]?.component)
            holder.oldserialNo.setText(this.freeReplacementArrayList[position]?.oldserial)
            for(newSerialno in freeReplacementArrayList[position]?.sparesstock!!){
                newSerialSTRList.add(newSerialno.serialNo)
            }
            newSerialSTRList.add(0,"Select The New Serial No")
            newSerialSTRAdapter = ArrayAdapter(context,R.layout.spinnertext,R.id.text1,newSerialSTRList)
            holder.spNewSerialNO.adapter = newSerialSTRAdapter
            holder.spNewSerialNO.onItemSelectedListener = object  :AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    try{
                        Log.d("NOthingSelected",""+parent?.selectedItem)
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    try{
                        val selectedItem = holder.spNewSerialNO.selectedItem.toString()
                        holder.edtNewSErialNo.setText(selectedItem)
                        if(selectedItem.equals("Select The New Serial No")){
                           Log.d("",""+selectedItem)
                        }else{
                            if(holder.oldserialNo.text.toString().isEmpty()){
                               Toast.makeText(context,"Old Serial Number Can't be Empty",Toast.LENGTH_SHORT).show()
                                holder.spNewSerialNO.setSelection(0)
                            }else{
                                val freeReplacementServerPojos = FreeReplacementServerPojos(holder.componet.text.toString(),holder.oldserialNo.text.toString(),
                                        selectedItem,freeReplacementArrayList[currentPosition]?.freecomplaintId)
                                CRMDetailsActivity.freeReplacementServerLISt.set(currentPosition,freeReplacementServerPojos)
                                Log.d("CRMACtivity_Free",""+  CRMDetailsActivity.freeReplacementServerLISt[currentPosition].componentName)
                                Log.d("CRMACtivity_Free",""+  CRMDetailsActivity.freeReplacementServerLISt[currentPosition].oldSerialNo)
                                Log.d("CRMACtivity_Free",""+  CRMDetailsActivity.freeReplacementServerLISt[currentPosition].newSerialNo)
                                Log.d("CRMACtivity_Free",""+  CRMDetailsActivity.freeReplacementServerLISt[currentPosition].id)
                            }

                        }
                    }catch (ex:Exception){
                        ex.printStackTrace()
                    }
                }

            }
            for(freeRepalcement  in this.freeReplacementArrayList){
                val freeReplacementServerPojos = FreeReplacementServerPojos(freeRepalcement?.component,freeRepalcement?.oldserial,
                        freeRepalcement?.sparesstock!![0].toString(),freeRepalcement.freecomplaintId)
                CRMDetailsActivity.freeReplacementServerLISt.add(freeReplacementServerPojos)
                Log.d("CRMACtivity_Free",""+ freeRepalcement.component)
                Log.d("CRMACtivity_Free",""+ freeRepalcement.oldserial)
                Log.d("CRMACtivity_Free",""+ freeRepalcement.sparesstock[0])
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val componet: AppCompatEditText = itemView?.findViewById(R.id.bs_edt_component_name_addmore_free)!!
        val oldserialNo: AppCompatEditText = itemView?.findViewById(R.id.bs_edt_old_serial_number_addmore_free)!!
        val edtNewSErialNo: AppCompatEditText = itemView?.findViewById(R.id.bs_edt_new_serial_number_addmore_free)!!
        val spNewSerialNO: Spinner = itemView?.findViewById(R.id.bs_sp_new_serial_number_addmore_free)!!
    }
}