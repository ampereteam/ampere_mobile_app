package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.Pojos;

import java.util.ArrayList;

public class Data{
	private ArrayList<IndentsItemPojos> indents;

	public void setIndents(ArrayList<IndentsItemPojos> indents){
		this.indents = indents;
	}

	public ArrayList<IndentsItemPojos> getIndents(){
		return indents;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"indents = '" + indents + '\'' + 
			"}";
		}
}