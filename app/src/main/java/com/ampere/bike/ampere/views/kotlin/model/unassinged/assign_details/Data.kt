package com.ampere.bike.ampere.views.kotlin.model.unassinged.assign_details

data class Data(
        val users: List<User>,
        val tasks: List<Task>
)