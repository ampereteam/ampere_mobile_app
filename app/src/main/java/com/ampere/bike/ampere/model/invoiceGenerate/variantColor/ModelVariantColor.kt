package com.ampere.bike.ampere.model.invoiceGenerate.variantColor

data class ModelVariantColor(
        val success: Boolean,
        val data: Data,
        val message: String
)