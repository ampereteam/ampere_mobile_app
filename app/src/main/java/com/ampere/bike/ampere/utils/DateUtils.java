package com.ampere.bike.ampere.utils;

import android.app.DatePickerDialog;
import android.support.v4.app.FragmentActivity;
import android.widget.DatePicker;

import com.ampere.vehicles.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by AND I5 on 19-03-2018.
 */

public class DateUtils {


    public static String DD_MM_YYYY = "dd-MM-yyyy";
    public static String DD_MM_YY = "dd-MM-yy";
    public static String YYYY_MM_DD = "yyyy-MM-dd";
    public static String DATE_DD_MM_YYYY_HH_MM_AP = "dd-MM-yyyy hh:mm a";
    public static String DATE_DDMMYYYYHHMMAP = "ddMMyyyyhhmma";
    public static String DATE_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static String MMM_DD_YY = "MMM dd,yyyy";
    public static String CURRENT_FORMAT = "yyyy-MM-dd";
    public static String DD_MM = "dd/MM";

    /**
     * Get Current date to any format
     *
     * @param dateFormat
     * @return
     */

    public static String getCurrentDate(String dateFormat) {
        String datStr = "";
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);
            Calendar cal = Calendar.getInstance();
            datStr = simpleDateFormat.format(cal.getTime());
            return datStr;
        } catch (NullPointerException e) {
            return datStr;
        }

    }

    /**
     * Convert date format from user given date
     *
     * @param input_date    input values
     * @param currentFormat date format on input values
     * @param convertFormat converted format.
     * @return
     * @throws ParseException
     */
    public static String convertDates(String input_date, String currentFormat, String convertFormat) throws ParseException {
        String dateStr = "";
        try {
            if (input_date != null && !input_date.isEmpty()) {
                SimpleDateFormat curFormater = new SimpleDateFormat(currentFormat);
                Date dateObj = curFormater.parse(input_date);
                SimpleDateFormat postFormater = new SimpleDateFormat(convertFormat);
                dateStr = postFormater.format(dateObj);
            }
            return dateStr;
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateStr;
    }

    public static String getFormattedDateFromTimestamp(long timeStampInMilliSeconds, String date_format) {
        String formattedDate = "";
        try {
            Date date = new Date();
            date.setTime(timeStampInMilliSeconds);
            formattedDate = new SimpleDateFormat(date_format).format(date);
            return formattedDate;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return formattedDate;

    }

    /**
     * @param changeDates input values
     * @param dateFormat  output format
     * @return
     */
    public static long getTimestampToday(String changeDates, String dateFormat) {

        long timeInMilliseconds = 0;

        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
        try {
            Date mDate = sdf.parse(changeDates);
            timeInMilliseconds = mDate.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    public static int saturdaysundaycount(Date d1, Date d2) {

        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(d2);
        int sundays = 0;
        // int saturday = 0;
        while (!c1.after(c2)) {
           /* if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                saturday++;
            }*/
            if (c1.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) {
                sundays++;
            }

            c1.add(Calendar.DATE, 1);
        }

        //  System.out.println("Saturday Count = " + saturday);
        System.out.println("Sunday Count = " + sundays);
        return sundays;
    }

    public static int getMonthOf(int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        return calendar.getActualMaximum(dayOfMonth);

    }

    public static String addPrefix(int year, int month, int day) {
        String days = "" + day, months = "" + (month + 1);
        days = days.length() == 1 ? (0 + "" + days) : "" + days;
        months = months.length() == 1 ? (0 + "" + months) : "" + months;
        return "" + days + "-" + months + "-" + year;
    }


    public static String[] showDateDailog( FragmentActivity activity) {
        final int[] year = {0};
        final int[] month = {0};
        final int[] day = {0};
        final String[] date = new String[1];
        final DatePickerDialog datePickerDialog = new DatePickerDialog(activity, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDate) {

                year[0] = selectedYear;
                month[0] = selectedMonth;
                day[0] = selectedDate;
                SimpleDateFormat dateFormatter = new SimpleDateFormat(DateUtils.DD_MM_YYYY, Locale.US);
                Calendar newDate = Calendar.getInstance();
                newDate.set(year[0], month[0], day[0]);
                date[0] = dateFormatter.format(newDate.getTime());
                /*if (datePic == 1) {
                    mDate.setText(dateFormatter.format(newDate.getTime()));
                } else if (datePic == 2) {
                    mDateTwo.setText(dateFormatter.format(newDate.getTime()));
                } else {
                    mEdtSelfDate.setText(dateFormatter.format(newDate.getTime()));
                }
                Calendar calendar = Calendar.getInstance();
                String time = mTime.getText().toString();
                if (time == "") {
                    TimePickerDialog d = new TimePickerDialog(getActivity(), getTimeFromPicker(Enquiry.this, mTime), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
                    d.getWindow().setWindowAnimations(R.style.grow);
                    d.show();
                }
*/
            }
        }, year[0], month[0], day[0]);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.getWindow().setWindowAnimations(R.style.grow);
        datePickerDialog.show();
        return date;
    }

}
