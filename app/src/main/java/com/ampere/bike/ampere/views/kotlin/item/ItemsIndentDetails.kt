package com.ampere.bike.ampere.views.kotlin.item

import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.`interface`.SampleInterface
import com.ampere.bike.ampere.model.indent.history.IndentdetailsItem
import com.ampere.bike.ampere.model.indent.history.IndentsItem
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_USER_TYPE
import com.ampere.bike.ampere.shared.LocalSession.T_CRM
import com.ampere.bike.ampere.utils.DateUtils
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.kotlin.fragments.indent.IndentDetailsClick
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.item_indent_info.view.*

class ItemsIndentDetails(val activity: FragmentActivity?, val indents: List<IndentsItem>, val indentsDeatilsItem: List<IndentdetailsItem>, val sampl: SampleInterface.Sample) : RecyclerView.Adapter<ItemsIndentDetails.ViewHolderIndent>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderIndent {
        return ViewHolderIndent(View.inflate(activity, R.layout.item_indent_info, null))
    }


    override fun getItemCount(): Int {
        return indents.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolderIndent, position: Int) {
        if (LocalSession.getUserInfo(activity, KEY_USER_TYPE).equals(T_CRM)) {
            holder.itemName.visibility = View.VISIBLE
        } else {
            holder.itemName.visibility = View.GONE
        }
        holder.itemName.gravity = Gravity.LEFT
        holder.itemName.text = indents[position].requestby.name
        holder.itemDate.text = DateUtils.convertDates(indents[position].requestDate, DateUtils.CURRENT_FORMAT, DateUtils.MMM_DD_YY)
        holder.itemStatus.text = indents[position].status
        holder.itemTask.text = indents[position].totalQty

        holder.snack_view.setOnClickListener {
            /*var obj = HashMap<String, String>()
            obj.put("name",indents[position].requestby.name.toString())
            sampl.toastMsg(obj*//*indents[position].requestby.name*//*)*/

            if (indents[position].totalQty.isEmpty()) {
                MessageUtils.showToastMessage(activity, "No Quantity Found")
            } else {
                var bundle = Bundle()
                var fragment = IndentDetailsClick()
                bundle.putString("id", "" + indents[position].id)
                bundle.putString("custom_name", DateUtils.convertDates(indents[position].requestDate,DateUtils.CURRENT_FORMAT, DateUtils.MMM_DD_YY))
                fragment.arguments = bundle
                MyUtils.passFragmentBackStack(activity, fragment)
            }
        }
    }

    class ViewHolderIndent(view: View) : RecyclerView.ViewHolder(view) {
        val itemName = view.indent_name
        val itemDate = view.indent_date
        val itemTask = view.indent_task
        val itemStatus = view.indent_status
        val snack_view = view.indent_header

    }
}
