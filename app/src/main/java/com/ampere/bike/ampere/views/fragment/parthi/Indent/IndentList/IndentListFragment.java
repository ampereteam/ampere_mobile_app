package com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.api.Retrofit.ResponsebackToClass;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.Adapter.IndentListAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.IndentDetails.IndentDetailsActivity;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentList.Pojos.IndentsItemPojos;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.ampere.bike.ampere.CustomNavigationDuo.snackVIew;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentHome.msnackview;


public class IndentListFragment extends Fragment implements ResponsebackToClass, ConfigClickEvent {
    IndentListAdapter indentListAdapter;
    Common common;

    ArrayList<IndentsItemPojos> indentsItemPojosArrayList;
    RecyclerView recyclerView;
    TextView textView;
    LinearLayout linearLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_indent_list, container, false);
        common = new Common(getActivity(), this);

        recyclerView = view.findViewById(R.id.indntList);
        textView = view.findViewById(R.id.emptyList);
        linearLayout = view.findViewById(R.id.main);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        // recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        CallApi();
        /*view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @SuppressLint("ResourceType")
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i(getTag(), "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
//                    getActivity().getActionBar().show();
                    Log.i(getTag(), "onKey Back listener is working!!!");
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }
                    // String cameback="CameBack";
                    Intent i = new Intent(getActivity(), CustomNavigationDuo.class);
                    i.putExtra("SELECTEDVALUE", 0);
//                    // i.putExtra("Comingback", cameback);
                    startActivity(i);


                    return true;
                } else {
                    return false;
                }
            }
        });*/
        return view;
    }

    public void CallApi() {
        common.showLoad(true);
        if (common.isNetworkConnected()) {
            HashMap<String, String> map = new HashMap<>();
            map.put("userid", LocalSession.getUserInfo(getActivity(), KEY_ID));
            common.callApiRequest("dealerindent", "POST", map, 0);
        } else {
            common.hideLoad();
            MessageUtils.showSnackBarAction(Objects.requireNonNull(getActivity()),msnackview,getString(R.string.check_internet));
//            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void apiCallBackOverRideMethod(Response<ResponseBody> response, JSONArray objResponse, String method, int position, String message, boolean sucs)   {
        common.hideLoad();
        if (objResponse != null) {
            if (method.equals("dealerindent")) {

                System.out.println("response LIst" + objResponse.toString());
                indentsItemPojosArrayList = new Gson().fromJson(objResponse.toString(), new TypeToken<ArrayList<IndentsItemPojos>>() {
                }.getType());
                if (indentsItemPojosArrayList.size() > 0) {
                    textView.setVisibility(View.GONE);
                    linearLayout.setVisibility(View.VISIBLE);
                    indentListAdapter = new IndentListAdapter(getActivity(), indentsItemPojosArrayList, this);
                    recyclerView.setAdapter(indentListAdapter);
                } else {
                    textView.setVisibility(View.VISIBLE);
                    textView.setText(message);
                }
            }
        } else {
            if (method != null && method.equals("dealerindent")) {

               // startActivity(new Intent(getActivity(), IndentHome.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                MessageUtils.showSnackBar(Objects.requireNonNull(getActivity()),snackVIew,"Server Not Found");
            }
            else{
                textView.setVisibility(View.VISIBLE);
                textView.setText(message);
                linearLayout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void connectposition(int position, int listSize) {
   try{
       IndentDetailsActivity indentDetailsActivity = new IndentDetailsActivity();
       Bundle bundle = new Bundle();
       bundle.putString("ID", String.valueOf(indentsItemPojosArrayList.get(position).getId()));
       bundle.putString("status", indentsItemPojosArrayList.get(position).getStatus());
       indentDetailsActivity.setArguments(bundle);
       MyUtils.passFragmentBackStack(Objects.requireNonNull(getActivity()), indentDetailsActivity);
   }catch (Exception ex){
       ex.printStackTrace();
   }
    }


}
