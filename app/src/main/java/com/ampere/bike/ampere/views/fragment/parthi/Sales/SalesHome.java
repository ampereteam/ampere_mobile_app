package com.ampere.bike.ampere.views.fragment.parthi.Sales;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.api.Retrofit.Common;
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent;
import com.ampere.bike.ampere.api.Retrofit.NetworkManager;
import com.ampere.bike.ampere.api.Retrofit.ResponsebackToClass;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.CompressFile;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesAdapter.SalesAdapter;
import com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.SalesPojos;
import com.ampere.bike.ampere.views.fragment.indent.Indent;
import com.ampere.vehicles.BuildConfig;
import com.ampere.vehicles.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_ID;
import static com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN;


public class SalesHome extends Fragment implements ResponsebackToClass, ConfigClickEvent, View.OnClickListener {
    private static final String TAG = "INFO";
    SalesAdapter indentListAdapter;
    Common common;
    ArrayList<SalesPojos> salesPojosArrayList;
    RecyclerView recyclerView;
    TextView textView, salesInvoicePdf, uploadedInvoiceForm, uploadedAdharCard, cancel, submit;
    String invoice_id, invoice_form, aadhar_card;
    AlertDialog.Builder alertDialogBuilder;
    AlertDialog dialog;
    Dialog dialogProgress;
    TextView selectTextView, selectAdharCard;
    WebView webview;
    CardView deleteDialog;
    String PACKAGENAME;
    FragmentActivity fragmentActivity;
    View snackview;
    LinearLayout linearLayout;
    HashMap<String, String> uriArrayList = new HashMap<>();
    private static final int INVOICEFORM = 1234;
    private static final int ADHAR = 12345;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((CustomNavigationDuo) getActivity()).disbleNavigationView("Sales Vehicle");
        View view = inflater.inflate(R.layout.activity_sales_home, container, false);

        snackview = view.findViewById(R.id.snack_View);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        common = new Common(getActivity(), this);
        dialogProgress =new Dialog(getActivity());
        recyclerView = view.findViewById(R.id.indntList);
        textView = view.findViewById(R.id.emptyList);
        linearLayout = view.findViewById(R.id.main);
        alertDialogBuilder = new AlertDialog.Builder(getActivity());
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        // recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        CallApi();
        fragmentActivity = getActivity();
        PACKAGENAME = view.getContext().getPackageName();
        /*Toast.makeText(getActivity(), "" + PACKAGENAME, Toast.LENGTH_SHORT).show();
        System.out.println(" PACKAGE NAME" + PACKAGENAME);*/

        alertDialogBuilder.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                dialog.dismiss();
                return true;
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public void CallApi() {


        if (common.isNetworkConnected()) {
            HashMap<String, String> map = new HashMap<>();
            map.put("userid", LocalSession.getUserInfo(getActivity(), KEY_ID));
            common.callApiRequest("dealersales", "POST", map, 0);
        } else {
            MessageUtils.showSnackBarAction(Objects.requireNonNull(getActivity()),snackview,getString(R.string.check_internet));
            Toast.makeText(getActivity(), R.string.data, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void apiCallBackOverRideMethod(Response<ResponseBody> response, JSONArray objResponse, String method, int position, String message, boolean sucs) throws JSONException, IOException {
        common.hideLoad();
        if (objResponse != null) {
            System.out.println("response LIst" + objResponse.toString());
            salesPojosArrayList = new Gson().fromJson(objResponse.toString(), new TypeToken<ArrayList<SalesPojos>>() {
            }.getType());
            if (salesPojosArrayList.size() > 0) {
                linearLayout.setVisibility(View.VISIBLE);
                indentListAdapter = new SalesAdapter(getActivity(), salesPojosArrayList, this);
                recyclerView.setAdapter(indentListAdapter);
            } else {
                textView.setVisibility(View.VISIBLE);
                textView.setText(message);
            }

        } else {
            if ( method != null && method.equals("dealerindent")) {
                startActivity(new Intent(getActivity(), Indent.class));
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }else{
                linearLayout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void connectposition(int position, int listSize) {
        invoice_id = String.valueOf(salesPojosArrayList.get(position).getId());
        if (salesPojosArrayList.get(position).getAadharCard() != null && salesPojosArrayList.get(position).getInvoiceForm() != null) {
            invoice_form = salesPojosArrayList.get(position).getInvoiceForm();
            aadhar_card = salesPojosArrayList.get(position).getAadharCard();
            System.out.println(" test  invoice _form" + invoice_form + aadhar_card);
        } else {
            invoice_form = null;
            aadhar_card = null;
            System.out.println(" test  invoice _form" + invoice_form + aadhar_card);
        }

        if (invoice_id != null) {
            try {
                checkPermission();
                /*final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dialog_internet);

                CustomTextView title = dialog.findViewById(R.id.cmn_title);
                CustomTextView msg = dialog.findViewById(R.id.cmn_msg);
                CustomTextView cancel = dialog.findViewById(R.id.cancel);
                CustomTextView done = dialog.findViewById(R.id.gotoSt);
                dialog.setCancelable(false);
                dialog.show();
                title.setText("Invoice Download");
                msg.setText("You already download invoice please Skip to download now or Download");
                done.setText("Download");
                cancel.setText("Skip");
                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        new PdfDownload("SLAS_INVS_0" + invoice_id + ".pdf", getResources().getString(R.string.view_pdf) + invoice_id).execute();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        checkPermission();
                    }
                });*/
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    private void popmenu() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_sales_file_upload, null, false);
        alertDialogBuilder.setView(view);
        alertDialogBuilder.setCancelable(false);
        dialog = alertDialogBuilder.create();
        dialog.show();
        LinearLayout linearLayout = view.findViewById(R.id.upload_popup);
//        webview  = view.findViewById(R.id.webview);
        LinearLayout uploaFiles = view.findViewById(R.id.upload_files);
        selectTextView = view.findViewById(R.id.select_invoice_form);
        selectAdharCard = view.findViewById(R.id.select_adhar_card);
        salesInvoicePdf = view.findViewById(R.id.sales_invoice_pdf);
        cancel = view.findViewById(R.id.cancel);
        submit = view.findViewById(R.id.upload);
        deleteDialog = view.findViewById(R.id.delete_dialog);
        uploadedInvoiceForm = view.findViewById(R.id.uploaded_invoice_form);
        uploadedAdharCard = view.findViewById(R.id.uploaded_aadhar_card);


        selectAdharCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showFileChooser(ADHAR);
            }
        });


        selectTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser(INVOICEFORM);
            }

        });


        deleteDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (dialog != null || dialog.isShowing()) {
                        dialog.dismiss();
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (dialog != null || dialog.isShowing()) {
                        dialog.dismiss();
                        uriArrayList.clear();
                    } else {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    sendToServer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        uploadedInvoiceForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    new PdfDownload("INVSFM_0" + invoice_form, getResources().getString(R.string.view_uploaded_file) + invoice_form).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                new PdfDownload("INVSFM_0"+invoice_form,getResources().getString(R.string.view_uploaded_file)+invoice_form).execute();

            }
        });
        uploadedAdharCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    new PdfDownload("ADRC_0" + aadhar_card, getResources().getString(R.string.view_uploaded_file) + aadhar_card).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                new PdfDownload("ADRC_0"+aadhar_card,getResources().getString(R.string.view_uploaded_file)+aadhar_card).execute();

            }
        });
        salesInvoicePdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    new PdfDownload("SLAS_INVS_0" + invoice_id + ".pdf", getResources().getString(R.string.view_pdf) + invoice_id).execute();

                } catch (Exception e) {
                    e.printStackTrace();
                }
//               new PdfDownload("SLAS_INVS_0"+invoice_id+".pdf",getResources().getString(R.string.view_pdf)+invoice_id).execute();

            }
        });

        if (invoice_form != null && aadhar_card != null) {
            linearLayout.setVisibility(View.GONE);
        } else {
            linearLayout.setVisibility(View.VISIBLE);
            uploaFiles.setVisibility(View.GONE);
        }
        uriArrayList.clear();


    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                !=
                PackageManager.PERMISSION_GRANTED
                ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                ActivityCompat.requestPermissions(getActivity(), new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                }, 1121);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 1121);
            }
        } else {
            Log.d("PemrissionChecking","Permission in sales chcecking Permission....");
            popmenu();
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        Log.d("Pemrission Checking","requestCode"+requestCode);
        switch (requestCode) {
            case 1121:
                if (grantResults.length > 0 && grantResults[0] + grantResults[1]
                        == PackageManager.PERMISSION_GRANTED) {
                    Log.d("Pemrission Checking","Permission in sales");
                    popmenu();
                } else {
                    Toast.makeText(getActivity(), " NO Permission granted", Toast.LENGTH_SHORT).show();
                    checkPermission();
                }
        }
    }

    private void sendToServer() {

        OkHttpClient objOkHttpClient = new OkHttpClient.Builder().readTimeout(1, TimeUnit.MINUTES).connectTimeout(1, TimeUnit.MINUTES).addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
        Retrofit objRetrofit = new Retrofit.Builder()
                .client(objOkHttpClient)
                .baseUrl(getResources().getString(R.string.appURL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        NetworkManager networkManager = objRetrofit.create(NetworkManager.class);

        File invoiceFile =CompressFile.getCompressedImageFile( new File(uriArrayList.get("form")),getActivity());
        final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), invoiceFile);
        MultipartBody.Part invoicebody = MultipartBody.Part.createFormData("file", invoiceFile.getName(), requestFile);



        File adharCardFile = CompressFile.getCompressedImageFile(new File(uriArrayList.get("adhar")),getActivity());
        RequestBody adharCardRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), adharCardFile);
        MultipartBody.Part adharcardPart = MultipartBody.Part.createFormData("aadhar_card", adharCardFile.getName(), adharCardRequestBody);



        RequestBody invoiceID = RequestBody.create(MediaType.parse("multipart/form-data"), invoice_id);
            dialogProgress =MessageUtils.showDialog(getActivity());
        Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response> responseBodyCall = networkManager.call_post("uploaddocument",
                "Bearer " + LocalSession.getUserInfo(getActivity(), KEY_TOKEN), invoiceID, invoicebody, adharcardPart);

        responseBodyCall.clone().enqueue(new Callback<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>() {
            @Override
            public void onResponse(Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response> call, Response<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response> response) {
               MessageUtils.dismissDialog(dialogProgress);
                if (response.isSuccessful()) {


                    if (response.body().isSuccess()) {
                                if(dialog!=null && dialog.isShowing()){
                                    dialog.dismiss();
                                }
                        Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        CallApi();
                    }

                } else {

                    Toast.makeText(getActivity(), "SEVER NOT FOUND", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response> call, Throwable t) {
                MessageUtils.dismissDialog(dialogProgress);
                Toast.makeText(getActivity(), "SEVER NOT FOUND", Toast.LENGTH_SHORT).show();
            }
        });

        //viewPdf(uriArrayList.get("form"));
    }

    private void showFileChooser(int code) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    code);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(getActivity(), "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case INVOICEFORM:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    uriArrayList.put("form", getPath(getActivity(), uri));
                    File invoiceFile = new File(getPath(getActivity(), uri));
                    selectTextView.setText(invoiceFile.getName());
                    System.out.println("get path" + getPath(getActivity(), uri));

                }
                break;
            case ADHAR:

                Uri uri = data.getData();
                uriArrayList.put("adhar", getPath(getActivity(), uri));
                File adharFile = new File(getPath(getActivity(), uri));
                selectAdharCard.setText(adharFile.getName());
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void viewPdf() {
        webview.getSettings().setJavaScriptEnabled(true);
        ViewGroup vg = (ViewGroup) (webview.getParent());
        vg.removeView(webview);
        webview.loadUrl(getResources().getString(R.string.view_uploaded_file) + aadhar_card);
        dialog.setContentView(getView());
    }

    public static String getPath(Context context, Uri uri) {

        // DocumentProvider
        if (DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    public void viewUploadFile(String filename) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        MimeTypeMap myMime = MimeTypeMap.getSingleton();
        Intent newIntent = new Intent(Intent.ACTION_VIEW);
//    String path ="/storage/emulated/0/My_pdf/invoice_009.pdf";
//    String path ="/WhatsApp/Media/WhatsAppDocuments/wireframe.pdf";
//    String mimeType = myMime.getMimeTypeFromExtension(fileExt(filename).substring(1));
        // System.out.println("TYPE "+mimeType);
        //newIntent.setDataAndType(Uri.fromFile(new File(pat)),mimeType);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        if (filename.contains(".pdf")) {
            newIntent.setDataAndType(FileProvider.getUriForFile(fragmentActivity, BuildConfig.APPLICATION_ID, new File(filename)), "application/pdf");
        } else {
            newIntent.setDataAndType(FileProvider.getUriForFile(fragmentActivity, BuildConfig.APPLICATION_ID, new File(filename)), "image/*");
        }
        newIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
//        startActivity(newIntent);
            if (newIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(Intent.createChooser(newIntent, "Choose  Any  One Must"));
            }

        } catch (ActivityNotFoundException e) {
            Toast.makeText(getActivity(), "No handler for this type of file.", Toast.LENGTH_LONG).show();
        }
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.upload:
                sendToServer();
                break;
            case R.id.select_invoice_form:
                showFileChooser(INVOICEFORM);
                break;
            case R.id.select_adhar_card:
                showFileChooser(ADHAR);
                break;
            case R.id.cancel:
                dialog.dismiss();
                uriArrayList.clear();
                break;
            case R.id.sales_invoice_pdf:
                new PdfDownload("SLAS_INVS_0" + invoice_id + ".pdf", getResources().getString(R.string.view_pdf) + invoice_id).execute();
                dialog.dismiss();
                break;
            case R.id.delete_dialog:
                dialog.dismiss();
                break;
            case R.id.uploaded_aadhar_card:
                new PdfDownload("ADRC_0" + aadhar_card, getResources().getString(R.string.view_uploaded_file) + aadhar_card).execute();
                break;
            case R.id.uploaded_invoice_form:
                new PdfDownload("INVSFM_0" + invoice_form, getResources().getString(R.string.view_uploaded_file) + invoice_form).execute();
                break;
            default:
                break;
        }
    }

      public class PdfDownload extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;
        String localFileName;
        String serverFileName;

        public PdfDownload(String localFileName) {
            this.localFileName = localFileName;
        }

        public PdfDownload(String localFileName, String serverFileName) {
            this.localFileName = localFileName;
            this.serverFileName = serverFileName;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            common.showLoad(true);
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                common.hideLoad();
                if (outputFile != null) {
                    Log.v("downloaded file path", "" + outputFile.toString());
                    viewUploadFile(outputFile.toString());
                    Toast.makeText(getActivity(), "Download Success " + apkStorage.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                    //checkPermission();
                } else {

                    Log.e(TAG, "Download Failed");

                }
            } catch (Exception e) {
                e.printStackTrace();

                //Change button text if exception occurs
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.v("info", "try again");
                    }
                }, 3000);
                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }


            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(serverFileName);//Create Download URl
                System.out.println("Server URL" + serverFileName);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are getting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode() + " " + c.getResponseMessage());

                }


                //Get File if SD card is present
                if (isSDCardPresent()) {

                    apkStorage = new File(Environment.getExternalStorageDirectory() + "/My_Pdf");
                } else
                    Toast.makeText(getActivity(), "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e(TAG, "Directory Created.");
                }

                outputFile = new File(apkStorage, localFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e(TAG, "File Created"+outputFile);
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file

                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());

            }

            return null;
        }
    }

    public boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(

                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }


}

