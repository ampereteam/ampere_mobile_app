package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.dynamicAddPojo.ServiceReplecementViewPojos
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.dynamicview_crm_service_closure.view.*

class  ServiceReplecementAdapter (val context: Context, val ServicepojoList:ArrayList<ServiceReplecementViewPojos>)
    : RecyclerView.Adapter<ServiceReplecementAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceReplecementAdapter.ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.dynamicview_crm_service_closure, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return  ServicepojoList.size
    }

    override fun onBindViewHolder(holder: ServiceReplecementAdapter.ViewHolder, position: Int) {
        holder.tvComponent.setText(ServicepojoList.get(position).componentName)
        holder.tvNewSrLNO.setText(ServicepojoList.get(position).newSerialNumber)
        holder.tvOldSrLNO.setText(ServicepojoList.get(position).oldsrlNo)
        holder.bnRemove.setOnClickListener {
            ServicepojoList.removeAt(position)
            notifyDataSetChanged()
        }

    }
    class ViewHolder(view : View): RecyclerView.ViewHolder(view) {
        val  tvOldSrLNO = view.dy_old_srl_no
        val  tvNewSrLNO = view.dy_new_srl_no
        val  tvComponent = view.dy_component
        val  bnRemove = view.dy_remove

    }

}