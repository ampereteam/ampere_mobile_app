package com.ampere.bike.ampere.views.kotlin.model.invoice

data class AddInvoice(
        val success: Boolean,
        val data: String,
        val message: String
)