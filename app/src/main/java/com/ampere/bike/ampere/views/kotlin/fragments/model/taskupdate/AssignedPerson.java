package com.ampere.bike.ampere.views.kotlin.fragments.model.taskupdate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssignedPerson {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("profile_photo")
    @Expose
    private String profilePhoto;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("statecode")
    @Expose
    private Integer statecode;
    @SerializedName("depatment_id")
    @Expose
    private Integer depatmentId;
    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("dealer_id")
    @Expose
    private Object dealerId;
    @SerializedName("gst_no")
    @Expose
    private String gstNo;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
}
