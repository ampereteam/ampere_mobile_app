
package com.ampere.bike.ampere.model.task;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class EnquiryModel {

    @SerializedName("address1")
    public String mAddress1;
    @SerializedName("address2")
    public String mAddress2;
    @SerializedName("assigned_date")
    public String mAssignedDate;
    @SerializedName("assigned_person")
    public String mAssignedPerson;
    @SerializedName("assigned_to")
    public String mAssignedTo;
    @SerializedName("assigned_to_self")
    public String mAssignedToSelf;
    @SerializedName("assigned_to_sub")
    public Object mAssignedToSub;
    @SerializedName("chassis_no")
    public String mChassisNo;
    @SerializedName("city")
    public String mCity;
    @SerializedName("closing_date")
    public String mClosingDate;
    @SerializedName("complaint_status")
    public Object mComplaintStatus;
    /*  @SerializedName("created_at")
      public String mCreatedAt;*/
    @SerializedName("created_by")
    public String mCreatedBy;
    @SerializedName("created_time")
    public String mCreatedTime;
    @SerializedName("customer_name")
    public String mCustomerName;
    @SerializedName("customer_type")
    public String mCustomerType;
    @SerializedName("email")
    public String mEmail;
    @SerializedName("enquiry_date")
    public String mEnquiryDate;
    @SerializedName("enquiry_for")
    public String mEnquiryFor;
    @SerializedName("enquiry_id")
    public String mEnquiryId;
    @SerializedName("enquiry_no")
    public String mEnquiryNo;
    @SerializedName("enquiry_time")
    public String mEnquiryTime;
    @SerializedName("enquiry_type")
    public Object mEnquiryType;
    @SerializedName("id")
    public Long mId;
    @SerializedName("invoice_value")
    public String mInvoiceValue;
    @SerializedName("leadsource")
    public String mLeadsource;
    @SerializedName("locality")
    public String mLocality;
    @SerializedName("message")
    public String mMessage;
    @SerializedName("mobile_no")
    public String mMobileNo;
    @SerializedName("modified_by")
    public String mModifiedBy;
    @SerializedName("modified_date")
    public String mModifiedDate;
    @SerializedName("mtype")
    public Object mMtype;
    @SerializedName("pincode")
    public String mPincode;
    @SerializedName("place_of_service")
    public Object mPlaceOfService;
    @SerializedName("read_status")
    public Long mReadStatus;
    @SerializedName("root_cause")
    public Object mRootCause;
    @SerializedName("service_issues")
    public String mServiceIssues;
    @SerializedName("service_type")
    public Object mServiceType;
    @SerializedName("spares_id")
    public Object mSparesId;
    @SerializedName("spares_name")
    public Object mSparesName;
    @SerializedName("state")
    public String mState;
    @SerializedName("task")
    public String mTask;
    @SerializedName("task_status")
    public String mTaskStatus;
    /*  @SerializedName("updated_at")
      public String mUpdatedAt;*/
    @SerializedName("vehcile_image")
    public Object mVehcileImage;
    @SerializedName("vehicle_color")
    public String mVehicleColor;
    @SerializedName("vehicle_model")
    public String mVehicleModel;
    @SerializedName("vehicle_qty")
    public String mVehicleQty;
    @SerializedName("warranty_component")
    public Object mWarrantyComponent;

    @SerializedName("stock_exist")
    public boolean stock_exist;

    @SerializedName("invoice_exist")
    public boolean invoice_exist;

    public boolean isChecked;

    public boolean isStock_exist() {
        return stock_exist;
    }

    public void setStock_exist(boolean stock_exist) {
        this.stock_exist = stock_exist;
    }

    public boolean isInvoice_exist() {
        return invoice_exist;
    }

    public void setInvoice_exist(boolean invoice_exist) {
        this.invoice_exist = invoice_exist;
    }

    public String getAddress1() {
        return mAddress1;
    }

    public void setAddress1(String address1) {
        mAddress1 = address1;
    }

    public String getAddress2() {
        return mAddress2;
    }

    public void setAddress2(String address2) {
        mAddress2 = address2;
    }

    public String getAssignedDate() {
        return mAssignedDate;
    }

    public void setAssignedDate(String assignedDate) {
        mAssignedDate = assignedDate;
    }

    public String getAssignedPerson() {
        return mAssignedPerson;
    }

    public void setAssignedPerson(String assignedPerson) {
        mAssignedPerson = assignedPerson;
    }

    public String getAssignedTo() {
        return mAssignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        mAssignedTo = assignedTo;
    }

    public String getAssignedToSelf() {
        return mAssignedToSelf;
    }

    public void setAssignedToSelf(String assignedToSelf) {
        mAssignedToSelf = assignedToSelf;
    }

    public Object getAssignedToSub() {
        return mAssignedToSub;
    }

    public void setAssignedToSub(Object assignedToSub) {
        mAssignedToSub = assignedToSub;
    }

    public String getChassisNo() {
        return mChassisNo;
    }

    public void setChassisNo(String chassisNo) {
        mChassisNo = chassisNo;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getClosingDate() {
        return mClosingDate;
    }

    public void setClosingDate(String closingDate) {
        mClosingDate = closingDate;
    }

    public Object getComplaintStatus() {
        return mComplaintStatus;
    }

    public void setComplaintStatus(Object complaintStatus) {
        mComplaintStatus = complaintStatus;
    }

/*    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }*/

    public String getCreatedBy() {
        return mCreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        mCreatedBy = createdBy;
    }

    public String getCreatedTime() {
        return mCreatedTime;
    }

    public void setCreatedTime(String createdTime) {
        mCreatedTime = createdTime;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String customerName) {
        mCustomerName = customerName;
    }

    public String getCustomerType() {
        return mCustomerType;
    }

    public void setCustomerType(String customerType) {
        mCustomerType = customerType;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getEnquiryDate() {
        return mEnquiryDate;
    }

    public void setEnquiryDate(String enquiryDate) {
        mEnquiryDate = enquiryDate;
    }

    public String getEnquiryFor() {
        return mEnquiryFor;
    }

    public void setEnquiryFor(String enquiryFor) {
        mEnquiryFor = enquiryFor;
    }

    public String getEnquiryId() {
        return mEnquiryId;
    }

    public void setEnquiryId(String enquiryId) {
        mEnquiryId = enquiryId;
    }

    public String getEnquiryNo() {
        return mEnquiryNo;
    }

    public void setEnquiryNo(String enquiryNo) {
        mEnquiryNo = enquiryNo;
    }

    public String getEnquiryTime() {
        return mEnquiryTime;
    }

    public void setEnquiryTime(String enquiryTime) {
        mEnquiryTime = enquiryTime;
    }

    public Object getEnquiryType() {
        return mEnquiryType;
    }

    public void setEnquiryType(Object enquiryType) {
        mEnquiryType = enquiryType;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInvoiceValue() {
        return mInvoiceValue;
    }

    public void setInvoiceValue(String invoiceValue) {
        mInvoiceValue = invoiceValue;
    }

    public String getLeadsource() {
        return mLeadsource;
    }

    public void setLeadsource(String leadsource) {
        mLeadsource = leadsource;
    }

    public String getLocality() {
        return mLocality;
    }

    public void setLocality(String locality) {
        mLocality = locality;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getModifiedBy() {
        return mModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        mModifiedBy = modifiedBy;
    }

    public String getModifiedDate() {
        return mModifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        mModifiedDate = modifiedDate;
    }

    public Object getMtype() {
        return mMtype;
    }

    public void setMtype(Object mtype) {
        mMtype = mtype;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String pincode) {
        mPincode = pincode;
    }

    public Object getPlaceOfService() {
        return mPlaceOfService;
    }

    public void setPlaceOfService(Object placeOfService) {
        mPlaceOfService = placeOfService;
    }

    public Long getReadStatus() {
        return mReadStatus;
    }

    public void setReadStatus(Long readStatus) {
        mReadStatus = readStatus;
    }

    public Object getRootCause() {
        return mRootCause;
    }

    public void setRootCause(Object rootCause) {
        mRootCause = rootCause;
    }

    public String getServiceIssues() {
        return mServiceIssues;
    }

    public void setServiceIssues(String serviceIssues) {
        mServiceIssues = serviceIssues;
    }

    public Object getServiceType() {
        return mServiceType;
    }

    public void setServiceType(Object serviceType) {
        mServiceType = serviceType;
    }

    public Object getSparesId() {
        return mSparesId;
    }

    public void setSparesId(Object sparesId) {
        mSparesId = sparesId;
    }

    public Object getSparesName() {
        return mSparesName;
    }

    public void setSparesName(Object sparesName) {
        mSparesName = sparesName;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getTask() {
        return mTask;
    }

    public void setTask(String task) {
        mTask = task;
    }

    public String getTaskStatus() {
        return mTaskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        mTaskStatus = taskStatus;
    }

 /*   public String getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        mUpdatedAt = updatedAt;
    }*/

    public Object getVehcileImage() {
        return mVehcileImage;
    }

    public void setVehcileImage(Object vehcileImage) {
        mVehcileImage = vehcileImage;
    }

    public String getVehicleColor() {
        return mVehicleColor;
    }

    public void setVehicleColor(String vehicleColor) {
        mVehicleColor = vehicleColor;
    }

    public String getVehicleModel() {
        return mVehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        mVehicleModel = vehicleModel;
    }

    public String getVehicleQty() {
        return mVehicleQty;
    }

    public void setVehicleQty(String vehicleQty) {
        mVehicleQty = vehicleQty;
    }

    public Object getWarrantyComponent() {
        return mWarrantyComponent;
    }

    public void setWarrantyComponent(Object warrantyComponent) {
        mWarrantyComponent = warrantyComponent;
    }

}
