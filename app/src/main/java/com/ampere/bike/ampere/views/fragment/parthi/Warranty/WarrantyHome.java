package com.ampere.bike.ampere.views.fragment.parthi.Warranty;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaintListFragmentKotlin.WarrantyComplaintsListFragment;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyComplaints.WarrantyComplaintsDetails;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyDefects.WarrantyDefectsFragment;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyInvoice.WarrantyInvoiceFragment;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyServiceReturn.WarrantyServiceReturnFragment;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WrrantyTabAdapter.WarrantyTabAdapter;
import com.ampere.vehicles.R;

import java.util.ArrayList;
import java.util.Objects;


public class WarrantyHome extends Fragment {
    private WarrantyTabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    @SuppressLint("StaticFieldLeak")
    public  static  View snackView ;
    ArrayList<String> fragmnetList ;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ((CustomNavigationDuo) getActivity()).disbleNavigationView(getString(R.string.warrent_details));
        fragmnetList = new ArrayList<>();

        fragmnetList.add("Complaints");
        fragmnetList.add("Service Return");
        fragmnetList.add("Defects");
        fragmnetList.add("Invoice");
        View view =  inflater.inflate(R.layout.activity_warranty_home, container, false);
        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tabLayout);
        snackView = view.findViewById(R.id.warranty_snackview);
        adapter = new WarrantyTabAdapter(getChildFragmentManager());
        adapter.addFragment(new WarrantyComplaintsListFragment(), fragmnetList.get(0));
        adapter.addFragment(new WarrantyServiceReturnFragment(), fragmnetList.get(1));
        adapter.addFragment(new WarrantyDefectsFragment(), fragmnetList.get(2));
        adapter.addFragment(new WarrantyInvoiceFragment(), fragmnetList.get(3));
       /* viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);*/
        initTabs();

        return  view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {



//            initTabs();
           /* tabLayout.s`etSelectedTabIndicatorColor(getActivity().getResources().getServiceType(R.serviceType.black));
            tabLayout.setSelectedTabIndicatorHeight((int) (3 * getResources().getDisplayMetrics().density));
            tabLayout.setTabTextColors(getResources().getServiceType(R.serviceType.black), getResources().getServiceType(R.serviceType.coloRed));*/

        }catch ( Exception e){
            e.printStackTrace();
        }
       /* tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.v("Selected Tab", "" + tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                Log.v("unSelected Tab", "" + tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                Log.v("RESelected Tab", "" + tab);
            }
        });*/
    }


    /*
    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.viewPager);
        System.out.println("  when click back press function"+fragment.toString());
//       startActivity(new Intent(this, CustomNavigationDuo.class).setAction(String.valueOf(Intent.FLAG_ACTIVITY_CLEAR_TASK)).setAction(String.valueOf(Intent.FLAG_ACTIVITY_CLEAR_TOP)).putExtra("SELECTEDVALUE", 0));
        finish();

    }*/
    @SuppressLint("ResourceType")
    private void initTabs() {
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
      /*View view = getLayoutInflater().inflate(R.layout.tab_layout_custom_font,null,false);
        TextView  textView  = view.findViewById(R.id.tab_text);
        textView.setTypeface(MyUtils.getTypeface(getActivity(), "fonts/Lato-Bold.ttf"));
        textView.setTextColor(ContextCompat.getColorStateList(getActivity(), R.drawable.tab_selector));
        textView.setGravity(Gravity.CENTER);
        textView.setText("hello");*/
//        tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).setCustomView(textView);
        //tabLayout it doesn't respond to style attribute 'tabTextAppearance' so we have to use customview
        for (int i = 0; i < adapter.getCount(); i++) {
            TextView  textView  = new TextView(getActivity());
            textView.setTypeface(MyUtils.getTypeface(Objects.requireNonNull(getActivity()), "fonts/Lato-Bold.ttf"));
            textView.setTextColor(ContextCompat.getColorStateList(getActivity(), R.drawable.tab_selector));
            textView.setGravity(Gravity.CENTER);
            textView.setText(fragmnetList.get(i));
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(textView);
                tab.setText(adapter.getPageTitle(i));
            }
        }
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
    }


}
