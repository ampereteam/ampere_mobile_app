package com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ampere.bike.ampere.api.api_new.ConfigPosition
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoiceAdapterViewPojos.SpareInvoiceadapterviewPojos
import com.ampere.vehicles.R
import kotlinx.android.synthetic.main.adapter_spareinvoice_addmore.view.*

class  SpareinvoiceviewAdapter (val context: Context, val SpareinvoicepojoList:ArrayList<SpareInvoiceadapterviewPojos>,val callpostion: ConfigPosition)
    : RecyclerView.Adapter<SpareinvoiceviewAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpareinvoiceviewAdapter.ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.adapter_spareinvoice_addmore, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return  SpareinvoicepojoList.size
    }

    override fun onBindViewHolder(holder: SpareinvoiceviewAdapter.ViewHolder, position: Int) {
        try{

                holder.sparename.setText(SpareinvoicepojoList.get(position).spareNam)
                holder.perunit.setText(SpareinvoicepojoList.get(position).pricePerUnit)
                holder.spareqty.setText(SpareinvoicepojoList.get(position).qty)
                holder.total.setText(SpareinvoicepojoList.get(position).total)
                holder.bnRemove.setOnClickListener {

                    callpostion.callinterfacePosition(position)
                    SpareinvoicepojoList.removeAt(position)
                    notifyDataSetChanged()
                }

        }catch (ex : Exception){
            ex.printStackTrace()
        }

    }
    class ViewHolder(view : View): RecyclerView.ViewHolder(view) {
        val  sparename = view.adp_spr_edt_spare
        val  perunit = view.adp_spr_edt_prunit
        val  spareqty = view.adp_spr_edt_qty
        val  total = view.adp_spr_edt_total
        val  bnRemove = view.adp_spr_imgbn_remove

    }

}