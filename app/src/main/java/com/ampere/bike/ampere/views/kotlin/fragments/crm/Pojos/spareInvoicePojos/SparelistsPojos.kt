package com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareInvoicePojos

import com.google.gson.annotations.SerializedName

data class SparelistsPojos (
            @field:SerializedName("id")
             val id: Int? = null,
            @field:SerializedName("enquiry_id")
            val enquiryId: Int? = null,
            @field:SerializedName("spares_id")
            val sparesId: Int? = null,
            @field:SerializedName("qty")
            val qty: Int? = null,
            @field:SerializedName("created_at")
            val createdAt: String? = null,
            @field:SerializedName("updated_at")
            val updatedAt: String? = null
)
