package com.ampere.bike.ampere;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ampere.bike.ampere.nav.NavMenuModel;
import com.ampere.bike.ampere.shared.LocalSession;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.Home;
import com.ampere.bike.ampere.views.fragment.Login;
import com.ampere.bike.ampere.views.fragment.Splash;
import com.ampere.bike.ampere.views.fragment.enquiry.Enquiry;
import com.ampere.bike.ampere.views.fragment.indent.Indent;
import com.ampere.bike.ampere.views.fragment.parthi.Indent.IndentHome;
import com.ampere.bike.ampere.views.fragment.parthi.Inventory.InventoryHome;
import com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesHome;
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyHome;
import com.ampere.bike.ampere.views.kotlin.fragments.FreeReplacementRequireKT.FreeReplacementHome;
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.SpareServiceHomeKT;
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome;
import com.ampere.bike.ampere.views.kotlin.fragments.UserManagement.UserManageList;
import com.ampere.bike.ampere.views.kotlin.fragments.crm.SpareDetailsFgmt;
import com.ampere.bike.ampere.views.kotlin.fragments.indent.IndentDetails;
import com.ampere.bike.ampere.views.kotlin.fragments.unassign.UnAssignedTaskFragment;
import com.ampere.vehicles.R;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import customviews.CustomTextView;
import nl.psdcompany.duonavigationdrawer.views.DuoDrawerLayout;
import nl.psdcompany.duonavigationdrawer.widgets.DuoDrawerToggle;

import static com.ampere.bike.ampere.shared.LocalSession.T_CRM;
import static com.ampere.bike.ampere.shared.LocalSession.T_DEALER;
import static com.ampere.bike.ampere.shared.LocalSession.T_SPECIAL_OFFICER;

public class CustomNavigationDuo extends AppCompatActivity {

    public static DuoDrawerLayout drawerLayout;
    public static FrameLayout snackVIew;
    private Toolbar toolbar;
    public static RecyclerView recycleview;
    public static NavigationAdapter navigationAdapter;
    private DuoDrawerToggle drawerToggle;
    private CustomTextView mTootlBarTitle;
    private ImageView mNavToggleImg;
    private LinearLayout mProfileLayout;
    boolean doubleBackToExitPressedOnce = false, isBackClick = false;
    public static CustomTextView mUserName, mUserEmail;
    static FragmentActivity fragmentActivity;
    public static String enquiryType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyUtils.darkenStatusBar(this, R.color.colorPrimaryDark);
        setContentView(R.layout.activity_main_custom_nav);            fragmentActivity = this;
        drawerLayout = findViewById(R.id.drawer_layout);
        drawerToggle = new DuoDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
        recycleview = findViewById(R.id.recycleview);
        snackVIew = findViewById(R.id.container);
        drawerLayout.closeDrawer();
        // Tool bar Title and Image
        mTootlBarTitle = findViewById(R.id.title);
        mNavToggleImg = findViewById(R.id.navigation_menu);
        mProfileLayout = findViewById(R.id.profile);
        mUserName = findViewById(R.id.header_user_name);
        mUserEmail = findViewById(R.id.header_user_email);

        //Set Header name (Login name) and set navigation view
/*
        try {
            readXmlFormat();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        setHeaderText(this);

        mNavToggleImg.setImageResource(R.drawable.menu);
        setToolbar();
        Intent intent = getIntent();

        try {
            String redirect = intent.getStringExtra("redirect");
            if (redirect != null) {
                if (redirect.equals("indent")) {
                    MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new Indent());
                }  else if (redirect.equals("SalesCustomerDetails")) {
                    MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new SalesHome());
                }else if (redirect.equals("spareDetails")) {
                    MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new SpareDetailsFgmt());
                } else {
                    MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new Splash());
                }
            } else {
                MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new Splash());
            }
        } catch (Exception e) {
            e.printStackTrace();
            MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new Splash());
        }
        mProfileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //MyUtils.passFragmentBackStack(CustomNavigationDuo.this, new Profile());
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            String redirect = intent.getStringExtra("redirect");
            if (redirect != null) {
                if (redirect.equals("indent")) {
                    MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new Indent());
                } else {
                    MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new Splash());
                }
            } else {
                MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new Splash());
            }
        } catch (Exception e) {
            e.printStackTrace();
            MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new Splash());
        }
    }

    private void readXmlFormat() throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();

        SAXParser saxParser = factory.newSAXParser();


        DefaultHandler handler = new DefaultHandler() {

            boolean name = false;

            boolean salary = false;


            public void startElement(String uri, String localName, String qName,
                                     Attributes attributes) throws SAXException {
                if (qName.equalsIgnoreCase("name")) {
                    name = true;
                }
                if (qName.equalsIgnoreCase("salary")) {
                    salary = true;
                }
            }//end of startElement method

            public void endElement(String uri, String localName,
                                   String qName) throws SAXException {
            }

            public void characters(char ch[], int start, int length) throws SAXException {
                if (name) {

                    Log.d("Namesss", "" + new String(ch, start, length));
                    // tv.setText(tv.getText() + "\n\n Name : " + new String(ch, start, length));
                    name = false;
                }
                if (salary) {
                    Log.d("Salary", "" + new String(ch, start, length));
                    //tv.setText(tv.getText() + "\n Salary : " + new String(ch, start, length));
                    salary = false;
                }
            }//end of characters


        };//end of DefaultHandler object
        InputStream is = getAssets().open("files.xml");
        saxParser.parse(is, handler);
    }


    public static void setHeaderText(FragmentActivity activity) {

        if (LocalSession.isLogin(activity)) {
            navigationAdapter = new NavigationAdapter(activity, MyUtils.setNavigationView(activity));
            recycleview.setLayoutManager(new LinearLayoutManager(activity));
            recycleview.setAdapter(navigationAdapter);
            mUserEmail.setText(LocalSession.getUserInfo(activity, LocalSession.KEY_EMAIL));
            mUserName.setText(LocalSession.getUserInfo(activity, LocalSession.KEY_NAME));
        }
    }

    private void setToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(false);
        assert actionBar != null;
        actionBar.setTitle("");

        toolbar.findViewById(R.id.navigation_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen()) {
                    drawerLayout.closeDrawer();
                } else {
                    drawerLayout.openDrawer();
                }
            }
        });
    }


    /*
   Disable hole toolbar
    */
    @SuppressLint("WrongConstant")
    public void disableToolbar() {

        getSupportActionBar().hide();
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /*
   Disable Navigation view
    */
    @SuppressLint("WrongConstant")
    public void disbleNavigationView(String title) {
        getSupportActionBar().show();
        //  getSupportActionBar().setTitle(title);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
      /*  getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
        mNavToggleImg.setImageResource(R.drawable.ic_arrow_back);
        getSupportActionBar().setTitle(null);
        mTootlBarTitle.setText(title);
        setToolbar();

        isBackClick = true;
        //menuClick();

        toolbar.setEnabled(false);
        mNavToggleImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    @SuppressLint("WrongConstant")
    public void disbleNavigationViewButShowToolbarCustom(String title) {
        getSupportActionBar().hide();
        //  getSupportActionBar().setTitle(title);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
      /*  getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/
        mNavToggleImg.setImageResource(R.drawable.ic_arrow_back);
        getSupportActionBar().setTitle(null);
        mTootlBarTitle.setText(title);

        isBackClick = true;
        //menuClick();

        toolbar.setEnabled(false);
        mNavToggleImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    /*
    Enable Navigation View
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void enableNavigationView(String titleStr, int position) {

        //  navigationView.getMenu().getItem(position).setChecked(true);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Objects.requireNonNull(getSupportActionBar()).show();
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawerToggle.setDrawerIndicatorEnabled(true);
        //getSupportActionBar().setTitle(titleStr);
        mNavToggleImg.setImageResource(R.drawable.menu);
        getSupportActionBar().setTitle(null);
        mTootlBarTitle.setText(titleStr);
        //menuClick();
        toolbar.setEnabled(true);
        mNavToggleImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (drawerLayout.isDrawerOpen()) {
                    drawerLayout.closeDrawer();
                } else {
                    drawerLayout.openDrawer();
                }
            }
        });
    }

    public void menuClick() {
        if (!isBackClick) {
            toolbar.findViewById(R.id.navigation_menu).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (drawerLayout.isDrawerOpen()) {
                        drawerLayout.closeDrawer();
                    } else {
                        drawerLayout.openDrawer();
                    }
                }
            });
        } else {

            drawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        Fragment ff = getSupportFragmentManager().findFragmentById(R.id.container);
        Log.d("when_back_press", "" + ff);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (ff instanceof Home) {
            if (drawerLayout.isDrawerOpen()) {
                drawerLayout.closeDrawer();
            } else {
                checkBackPress();
            }
        } else if (ff instanceof Login) {
            checkBackPress();
        } else if (ff instanceof IndentDetails) {

            MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new Home());
        }  else if (ff instanceof AssignListHome) {

            MyUtils.passFragmentWithoutBackStack(CustomNavigationDuo.this, new Home());
        } else {
            super.onBackPressed();
        }
    }


    static class  NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.MyViewHolder> {

        boolean isExpand = false;
        FragmentActivity mContext;
        NavMenuModel[] nav_3_modalClasses;

        NavigationAdapter(FragmentActivity mContext, NavMenuModel[] nav_3_modalClasses) {
            this.mContext = mContext;
            this.nav_3_modalClasses = nav_3_modalClasses;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            public CustomTextView txtname, txtcount, txt_stock_vehicle, txt_stock_sales;
            public ImageView img, close_btn;
            LinearLayout mLayout, expandable_;


            MyViewHolder(View itemView) {
                super(itemView);

                txtname = itemView.findViewById(R.id.txtname);
                txtcount = itemView.findViewById(R.id.txtcount);
                img = itemView.findViewById(R.id.img);
                mLayout = itemView.findViewById(R.id.hole_layout);
                expandable_ = itemView.findViewById(R.id.expandable_);
                txt_stock_vehicle = itemView.findViewById(R.id.txt_stock_vehicle);
                txt_stock_sales = itemView.findViewById(R.id.txt_stock_sales);
                close_btn = itemView.findViewById(R.id.close_btn);

            }
        }


        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nav_3, parent, false);
            return new MyViewHolder(itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            holder.txtname.setText(nav_3_modalClasses[position].getName());
            holder.txtcount.setText(nav_3_modalClasses[position].getCount());
            holder.img.setImageResource(nav_3_modalClasses[position].getImage());
            final String count = nav_3_modalClasses[position].getCount();
            if (Boolean.parseBoolean(count)) {
                holder.txtname.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
            } else {
                holder.expandable_.setVisibility(View.GONE);
            }

         /*   if (nav_3_modalClasses[position].getName().equals("LOGOUT")) {
                holder.close_btn.setVisibility(View.GONE);
            } else {
                holder.close_btn.setVisibility(View.GONE);
            }*/
            holder.close_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    collaps(holder);
                }
            });

            holder.mLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    collaps(holder);
                    switch (position) {
                        case 0:
                            collaps(holder);
                            MyUtils.passFragmentBackStack(mContext, new Home());

                            break;
                        case 1:
                            collaps(holder);
                            MyUtils.passFragmentBackStack(mContext, new Enquiry());

                            break;
                        case 2:

                          /*  if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_CRM)) {
                                // recycleview.setAdapter(new NavigationAdapter(CustomNavigationDuo.this, setNavigationView()));
                                logout(mContext);

                            } else if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_SPECIAL_OFFICER)) {
                                MyUtils.passFragmentBackStack(mContext, new MyEnquiry());
                            } else {*/
                            collaps(holder);
                            //MyUtils.passFragmentBackStack(mContext, new IndentDetails());
                            MyUtils.passFragmentBackStack(mContext, new IndentHome());
                          /*  Intent intent = new Intent(mContext, IndentHome.class);
                            mContext.startActivity(intent);
                            mContext.overridePendingTransition(0, 0);*/


                            // }

                            break;
                        /*case 3:

                            //recycleview.setAdapter(new NavigationAdapter(CustomNavigationDuo.this, setNavigationView()));
                            if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_SPECIAL_OFFICER)) {
                                //MyUtils.passFragmentBackStack(mContext, new IndentDetails());

                                logout(mContext, holder);


                            } *//*else if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_DEALER)) {*//* else {
                                collaps(holder);
                                checkPermissionstro(2);
//                                MyUtils.passFragmentBackStack(mContext, new SalesHome());

                            }
                            *//*} else {
                                logout(mContext);
                            }*//*

                            break;*/
                        case 3:
                          if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_DEALER)) {
                              //MyUtils.passFragmentBackStack(mContext, new InventoryOrStock());


                              if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_SPECIAL_OFFICER)) {

                                  logout(mContext, holder);
                              } else {
                                  if (!count.isEmpty()) {

                                      Log.d("Show_Detlsi", "Expandable " + count);
                                      if (Boolean.parseBoolean(count)) {

                                          if (!isExpand) {
                                              holder.txtname.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
                                              isExpand = true;
                                              holder.expandable_.setVisibility(View.VISIBLE);
                                          } else {
                                              holder.txtname.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up, 0);
                                              isExpand = false;
                                              holder.expandable_.setVisibility(View.GONE);
                                          }
                                      }
                                  } else {
                                    /*  mContext.startActivity(new Intent(mContext, InventoryOrStock.class));
                                      mContext.overridePendingTransition(0, 0);
                                      drawerLayout.closeDrawer();*/
                                  }
                              }
                          }
                            break;
                        case 4:

                            if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_CRM)) {
                                /*mContext.startActivity(new Intent(mContext, UnAssignedTaskActivityFromCRM.class));
                                mContext.overridePendingTransition(0, 0);*/
                                collaps(holder);
                                MyUtils.passFragmentBackStack(mContext, new InventoryHome());

                            } else {
                                MyUtils.passFragmentBackStack(mContext, new InventoryHome());
//                                logout(mContext, holder);

                            }
                            break;
             /*           case 5:

                            if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_CRM)) {
                                mContext.startActivity(new Intent(mContext, UnAssignedTaskActivityFromCRM.class));
                                mContext.overridePendingTransition(0, 0);
                                collaps(holder);
                                MyUtils.passFragmentBackStack(mContext, new UnAssignedTaskFragment());

                            } else {
                                collaps(holder);
//                                logout(mContext, holder);
                                //mContext.startActivity(new Intent(mContext, WarrantyHome.class));
//                                MyUtils.passFragmentBackStack(mContext, new SpareDetailsFgmt());
                                checkPermissionstro(1);
                            }
                            break;*/
                        case 5:

                            if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_CRM)) {
                                /*mContext.startActivity(new Intent(mContext, UnAssignedTaskActivityFromCRM.class));
                                mContext.overridePendingTransition(0, 0);*/
                                collaps(holder);
                                MyUtils.passFragmentBackStack(mContext, new UnAssignedTaskFragment());

                            } else {
                                collaps(holder);
//                                logout(mContext, holder);
                                //mContext.startActivity(new Intent(mContext, WarrantyHome.class));
                                MyUtils.passFragmentBackStack(mContext, new WarrantyHome());
                            }
                            break;
                        case 6:

                            if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_CRM)) {
                                /*mContext.startActivity(new Intent(mContext, UnAssignedTaskActivityFromCRM.class));
                                mContext.overridePendingTransition(0, 0);*/
                                collaps(holder);
                                MyUtils.passFragmentBackStack(mContext, new UnAssignedTaskFragment());

                            } else {
                                collaps(holder);
//                                logout(mContext, holder);
                                //mContext.startActivity(new Intent(mContext, WarrantyHome.class));
                                MyUtils.passFragmentBackStack(mContext, new FreeReplacementHome());
                            }
                            break;
                        case 7:

                            if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_CRM)) {
                                /*mContext.startActivity(new Intent(mContext, UnAssignedTaskActivityFromCRM.class));
                                mContext.overridePendingTransition(0, 0);*/
                                collaps(holder);
                                MyUtils.passFragmentBackStack(mContext, new UnAssignedTaskFragment());

                            } else {
                                collaps(holder);
//                                logout(mContext, holder);
                                //mContext.startActivity(new Intent(mContext, WarrantyHome.class));
                                MyUtils.passFragmentBackStack(mContext, new SpareServiceHomeKT());
                            }
                            break;
                        case 8:

                            if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_CRM)) {
                                /*mContext.startActivity(new Intent(mContext, UnAssignedTaskActivityFromCRM.class));
                                mContext.overridePendingTransition(0, 0);*/
                                collaps(holder);
//                                MyUtils.passFragmentBackStack(mContext, new UnAssignedTaskFragment());

                            } else {
                                collaps(holder);
                                MyUtils.passFragmentBackStack(mContext, new UserManageList());
//                                logout(mContext, holder);

                            }
                            break;
                            case 9:

                            if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_CRM)) {
                                /*mContext.startActivity(new Intent(mContext, UnAssignedTaskActivityFromCRM.class));
                                mContext.overridePendingTransition(0, 0);*/
                                collaps(holder);
//                                MyUtils.passFragmentBackStack(mContext, new UnAssignedTaskFragment());

                            } else {
                                collaps(holder);
                                MyUtils.passFragmentBackStack(mContext, new AssignListHome());

                            }
                            break;
                        case 10:

                            if (LocalSession.getUserInfo(mContext, LocalSession.KEY_USER_TYPE).equals(T_CRM)) {
                                /*mContext.startActivity(new Intent(mContext, UnAssignedTaskActivityFromCRM.class));
                                mContext.overridePendingTransition(0, 0);*/
                                collaps(holder);
//                                MyUtils.passFragmentBackStack(mContext, new UnAssignedTaskFragment());

                            } else {

                                logout(mContext, holder);

                            }
                            break;
                        case 11:
                            logout(mContext, holder);
                            break;

                        default:
                            collaps(holder);
                            MyUtils.passFragmentBackStack(mContext, new Home());

                            break;
                    }

                }
            });
            holder.txt_stock_sales.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    collaps(holder);
                    try{
                        checkPermissionstro(1);
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                   /* Fragment fragment = new InventoryOrStockTabs();
                    Bundle bundle = new Bundle();
                    bundle.putString("viewItemType", "Spares");
                    MyUtils.VIEW_ITEM = "Spares";
                    fragment.setArguments(bundle);
                    MyUtils.passFragmentBackStack(mContext, fragment);
                    Intent intent = new Intent(mContext, InventoryOrStock.class);
                    intent.putExtra("viewItemType", "Spares");
                    MyUtils.VIEW_ITEM = mContext.getString(R.string.spares);
                    mContext.startActivity(intent);
                    mContext.overridePendingTransition(0, 0);*/

                }
            });

            holder.txt_stock_vehicle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    collaps(holder);
                    try{
                        checkPermissionstro(2);
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                   /* Fragment fragment = new InventoryOrStockTabs();
                    Bundle bundle = new Bundle();
                    bundle.putString("viewItemType", "Vehicle");
                    MyUtils.VIEW_ITEM = "Vehicle";
                    fragment.setArguments(bundle);
                    MyUtils.passFragmentBackStack(mContext, fragment);
                    Intent intent = new Intent(mContext, InventoryOrStock.class);
                    intent.putExtra("viewItemType", "Vehicle");
                    MyUtils.VIEW_ITEM = mContext.getString(R.string.vehicle);
                    mContext.startActivity(intent);
                    mContext.overridePendingTransition(0, 0);*/


                }
            });

        }

        public void collaps(MyViewHolder holder) {

            notifyDataSetChanged();
            isExpand = false;
            holder.txtname.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down, 0);
            holder.expandable_.setVisibility(View.GONE);
            drawerLayout.closeDrawer();
        }

        @Override
        public int getItemCount() {
            return nav_3_modalClasses.length;
        }


    }


    public static void logout(final FragmentActivity activity, final NavigationAdapter.MyViewHolder holder) {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        //Yes button clicked
                        dialog.dismiss();
                        navigationAdapter.collaps(holder);
                        LocalSession.logout(activity);

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked

                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.sure_to_logout).setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            View view = getCurrentFocus();
            if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
                int scrcoords[] = new int[2];
                view.getLocationOnScreen(scrcoords);
                float x = ev.getRawX() + view.getLeft() - scrcoords[0];
                float y = ev.getRawY() + view.getTop() - scrcoords[1];
                if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom())
                    ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return super.dispatchTouchEvent(ev);
    }

    private void checkBackPress() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            return;
        }

        doubleBackToExitPressedOnce = true;

        Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 1000);
    }


  static   private void checkPermissionstro( int  requestcode) {
        if (ContextCompat.checkSelfPermission(fragmentActivity, Manifest.permission.READ_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(fragmentActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                !=
                PackageManager.PERMISSION_GRANTED
                ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(fragmentActivity, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(fragmentActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                ActivityCompat.requestPermissions(fragmentActivity, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                }, requestcode);
            } else {
                ActivityCompat.requestPermissions(fragmentActivity, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, requestcode);
            }
        } else {
            if(requestcode == 2){
                MyUtils.passFragmentBackStack(fragmentActivity, new SalesHome());
            }else {
                MyUtils.passFragmentBackStack(fragmentActivity, new SpareDetailsFgmt());
            }


        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] + grantResults[1]
                        == PackageManager.PERMISSION_GRANTED) {
                    MyUtils.passFragmentBackStack(fragmentActivity, new SpareDetailsFgmt());
                } else {
                    Toast.makeText(this, " NO Permission granted", Toast.LENGTH_SHORT).show();
                    Log.d("Permission Denied","Please Accept  the Permission ");
                }
                break;
            case 2:
                if (grantResults.length > 0 && grantResults[0] + grantResults[1]
                        == PackageManager.PERMISSION_GRANTED) {
                    MyUtils.passFragmentBackStack(fragmentActivity, new SalesHome());
                } else {
                    Toast.makeText(this, " NO Permission granted", Toast.LENGTH_SHORT).show();
                    Log.d("Permission Denied","Please Accept  the Permission ");
                }

                break;
        }
    }

}
