package com.ampere.bike.ampere.views.kotlin.fragments.crm

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.*
import android.support.design.widget.Snackbar
import android.support.design.widget.Snackbar.make
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.TextView
import android.widget.Toast
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.CustomNavigationDuo.snackVIew
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.api.api_new.UtilsUICallBack
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.utils.MyUtils
import com.ampere.bike.ampere.views.fragment.parthi.Warranty.WarrantyHome.snackView
import com.ampere.bike.ampere.views.kotlin.fragments.crm.CRMDetailsActivity.Companion.dialog
import com.ampere.bike.ampere.views.kotlin.fragments.crm.Pojos.spareDetailsPojos.SpareDetailsPojos
import com.ampere.bike.ampere.views.kotlin.fragments.crm.adapter.SpareDetailsAdapter
import com.ampere.vehicles.BuildConfig
import com.ampere.vehicles.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main_custom_nav.*
import kotlinx.android.synthetic.main.frag_crm.*
import kotlinx.android.synthetic.main.fragment_spare_details.*
import kotlinx.android.synthetic.main.fragment_spare_details.view.*
import nl.psdcompany.duonavigationdrawer.views.DuoDrawerLayout
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

@SuppressLint("ByteOrderMark")
class SpareDetailsFgmt : Fragment(), UtilsUICallBack, ConfigClickEvent {
    var callApi: UtilsCallApi? = null

    var spareDetailsResponse: SpareDetailsPojos? = null
    var spareDetailsAdapter: SpareDetailsAdapter? = null
    var spareinvoicepdfPATH = ""
    var views : View? = null
    var dialog :Dialog?=null
    lateinit var msnackview : View

    var RECORD_REQUEST_CODE =101

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        callApi = UtilsCallApi(activity, this)
        spareinvoicepdfPATH = getString(R.string.spare_invoice_pdf)

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity as CustomNavigationDuo).disbleNavigationView("Sales Spare")
        views = inflater.inflate(R.layout.fragment_spare_details, container, false)
        // Inflate the layout for this fragment
        msnackview = views?.findViewById(R.id.spareparentLayout)!!
        views?.spr_rcyle_spareList?.layoutManager = LinearLayoutManager(activity)

        checkPermission()
        return views
    }

    private fun getSpareDetails() {
        val map: HashMap<String, Any> = HashMap()
        map.put("userid", LocalSession.getUserInfo(activity, LocalSession.KEY_ID))
        if (callApi?.isNetworkConnected!!) {
try{
    dialog =MessageUtils.showDialog(activity)
            val apiCallInterfae = callApi?.connectRetro("POST", "sparesinvoice")
            val callbackList = apiCallInterfae?.call_post("sparesinvoice", "Bearer " + LocalSession.getUserInfo(activity, LocalSession.KEY_TOKEN), map)
            callbackList?.enqueue(object : retrofit2.Callback<ResponseBody> {

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.d("Response \n", "" + t.toString())
                }

                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    MessageUtils.dismissDialog(dialog)
                    if (response.isSuccessful) {
                        spareDetailsResponse = Gson().fromJson(response.body()?.string(), SpareDetailsPojos::class.java)
                        println("Response  " + spareDetailsResponse)
                        if (spareDetailsResponse?.success!!) {
                            spareDetailsAdapter = SpareDetailsAdapter(this@SpareDetailsFgmt, spareDetailsResponse?.data?.invoices, this@SpareDetailsFgmt)
                            views?.spr_rcyle_spareList?.adapter = spareDetailsAdapter

                        } else {
                            println("Response  " + spareDetailsResponse)
                        }
                    } else {
                        println("Response  " + spareDetailsResponse)
                    }
                }

            })

}catch ( e :Exception){
    e.printStackTrace()
}

        } else {
//            Log.d("View ",""+msnackview)
            showSnackBarActio()
        }
    }

    private fun showSnackBarActio(): Snackbar {


        val snackbar: Snackbar
        snackbar = make(snackVIew, getString(R.string.check_internet), Snackbar.LENGTH_LONG)
        val snackBarView = snackbar.view
        snackBarView.setBackgroundColor(activity?.getResources()?.getColor(R.color.red)!!)
        val textView = snackBarView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        //textView.setGravity(Gravity.CENTER);
        textView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        textView.textSize = 16f
        /* Typeface custom_font = Typeface.createFromAsset(context.getAssets(), "Typomoderno_bold.ttf");
        textView.setTypeface(custom_font);*/
        textView.setTextColor(Color.WHITE)
        snackbar.show()
        snackbar.setAction("Go") {
            val intent = Intent()
            intent.component = ComponentName("com.android.settings", "com.android.settings.Settings")
            activity!!.startActivity(intent)
        }
        return snackbar


    }

    override fun apiCallBackOverRideMethod(response: JSONObject?, position: Int, success: Boolean, message: String?, apiCallPath: String?) {

    }

    override fun connectposition(position: Int, listSize: Int) {
        Log.d("ArrayLIst Position", "" + spareDetailsResponse?.data?.invoices?.get(position))
        val enquiry_id = spareDetailsResponse?.data?.invoices?.get(position)?.id.toString()
        if(callApi!!.isNetworkConnected){
            dialog = MessageUtils.showDialog(activity)
            PdfDownload(enquiry_id).execute()
            MessageUtils.dismissDialog(dialog)
        }else{
            MessageUtils.showSnackBarAction(activity, snackVIew,getString(R.string.check_internet))
        }


    }

    @SuppressLint("StaticFieldLeak")
    inner class PdfDownload(internal var enquiryID: String) : AsyncTask<Void, Void, Void>() {

        internal var apkStorage: File? = null
        internal var outputFile: File? = null
        //        internal var serverFileName: String


        @SuppressLint("ResourceAsColor")
        override fun onPreExecute() {
            super.onPreExecute()

        }

        override fun onPostExecute(result: Void?) {
            MessageUtils.dismissDialog(dialog)
            println("Out put file " + outputFile.toString())
            try {

                if (outputFile != null) {
                    Log.v("downloaded file path", "" + outputFile!!.toString())
                    viewUploadFile(outputFile!!.toString())
                    Toast.makeText(activity, "Download Success " + apkStorage!!.absolutePath, Toast.LENGTH_SHORT).show()
                    //checkPermission();
                } else {

                    Log.e(TAG, "Download Failed")
                    MessageUtils.showToastMessage(activity, "File Not Found")
                }
            } catch (e: Exception) {
                e.printStackTrace()

                //Change button text if exception occurs
                Handler().postDelayed({ Log.v("info", "try again") }, 3000)
                Log.e(TAG, "Download Failed with Exception - " + e.localizedMessage)

            }


            super.onPostExecute(result)
        }

        override fun doInBackground(vararg arg0: Void): Void? {
            try {
                val url = URL(spareinvoicepdfPATH + enquiryID)//Create Download URl
//                val url = URL(spareinvoicepdfPATH+enquiryID)//Create Download URl
                println("Server URL$spareinvoicepdfPATH" + "4")
                val c = url.openConnection() as HttpURLConnection//Open Url Connection
                c.requestMethod = "GET"//Set Request Method to "GET" since we are getting data
                c.connect()//connect the URL Connection
                println("connection url" + c.connectTimeout.toString())
                //If Connection response is not OK then show Logs
                if (c.responseCode != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.responseCode + " " + c.responseMessage)

                }
                println("Server url response code" + c.responseCode.toString())
                //Get File if SD card is present
                if (isSDCardPresent()) {
                    apkStorage = File(Environment.getExternalStorageDirectory().toString() + "/My_Pdf/Spare")
                    println("apKstoarage" + apkStorage.toString())
//                    Toast.makeText(activity, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show()
                } else
                    Toast.makeText(activity, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show()
                //If File is not present create directory
                if (!apkStorage!!.exists()) {
                    apkStorage!!.mkdir()
                    Log.e(TAG, "Directory Created.")
                }
                println("apKstoarage" + apkStorage.toString())
                outputFile = File(apkStorage, "Sparein_00" + enquiryID + ".pdf")//Create Output file in Main File
//                outputFile!!.mkdir()
                //Create New File if n ot present
                println("outputFile" + outputFile.toString())
                if (!outputFile!!.exists()) {
                    outputFile!!.createNewFile()
                    Log.e(TAG, "File Created" + outputFile!!)
                }
                println("outputFile" + outputFile.toString())
                val fos = FileOutputStream(outputFile!!)//Get OutputStream for NewFile Location
                println("FOS " + fos.toString())
//                val is = c.inputStream//Get InputStream for connection
                val inputStream: InputStream = c.inputStream
                /*val buffer = ByteArray(1024)//Set buffer type
                println("BUFFER"+buffer.toString())
                val len1 = inputStream.read(buffer)
                //init length
//                while ((len1 = inputStream.read(buffer)) != -1) {
                while (len1 !=-1) {
                    println(" file Os"+len1+"\n"+fos.toString())
                    fos.write(buffer, 0, len1)//Write new file
                }*/
                inputStream.use { input ->
                    outputFile!!.outputStream().use { fos ->
                        input.copyTo(fos)
                    }
                }
                println("END FOS" + fos.toString())
                //Close all connection after doing task
                fos.flush()
                fos.close()
                inputStream.close()
            } catch (e: Exception) {
                //Read exception if something went wrong
                e.printStackTrace()
                outputFile = null
                Log.e(TAG, "Download Error Exception " + e.message)


            }

            return null
        }
    }

    fun viewUploadFile(filename: String) {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        val myMime = MimeTypeMap.getSingleton()
        val newIntent = Intent(Intent.ACTION_VIEW)
        newIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        if (filename.contains(".pdf")) {
            newIntent.setDataAndType(FileProvider.getUriForFile(this.activity!!, BuildConfig.APPLICATION_ID, File(filename)), "application/pdf")
        } else {
            newIntent.setDataAndType(FileProvider.getUriForFile(this.activity!!, BuildConfig.APPLICATION_ID, File(filename)), "image/*")
        }
        newIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        try {
            //        startActivity(newIntent);
            if (newIntent.resolveActivity(activity!!.packageManager) != null) {
                startActivity(Intent.createChooser(newIntent, "Choose  Any  One Must"))
            }

        } catch (e: ActivityNotFoundException) {
            Toast.makeText(activity, "No handler for this type of file.", Toast.LENGTH_LONG).show()
        }

    }

    fun isSDCardPresent(): Boolean {
        return if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
            true
        } else {
            false
        }
    }

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat.checkSelfPermission(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.READ_EXTERNAL_STORAGE) || ActivityCompat.shouldShowRequestPermissionRationale(activity!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), RECORD_REQUEST_CODE)
           Log.d("Log d","Checkpermission Function")
//                checkPermission()
            } else {
                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), RECORD_REQUEST_CODE)
                Log.d("Log d","Checkpermission Function Else")
                checkPermission()
            }
        } else {
            getSpareDetails()
            Log.d("Log d","Checkpermission Function if Else")
//            popmenu()
        }

    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        Log.d("Log.d " ,""+requestCode)
        when (requestCode) {
           RECORD_REQUEST_CODE-> if (grantResults.size == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Log.d("Log d","Checkpermission Function result if "+ requestCode)
                this.getSpareDetails()
            } else {
                Log.d("Log d","Checkpermission Function result else "+ requestCode)
                Toast.makeText(activity, " NO Permission granted", Toast.LENGTH_SHORT).show()
                checkPermission()
            }
        }
    }


}