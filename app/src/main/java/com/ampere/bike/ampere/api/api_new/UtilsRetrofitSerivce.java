package com.ampere.bike.ampere.api.api_new;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface UtilsRetrofitSerivce {

    @POST("{path}")
    Call<ResponseBody> call_post(@Path("path") String method);

    @POST("{path}")
    Call<ResponseBody> call_post(@Path("path") String method, @Header("Authorization") String token, @Body Map<String, Object> map);
    @Multipart
    @POST("{path}")
    Call<com.ampere.bike.ampere.views.fragment.parthi.Sales.SalesPojos.Response>
    call_post(@Path("path") String method,
              @Header("Authorization") String auth,
              @Part("chassis_no") RequestBody chassisno,
              @Part("defect_item") RequestBody defectItem,
              @Part("defect_qty") RequestBody defectQty,
              @Part("complaint") RequestBody complaints,
              @Part("enquiry_id") RequestBody enquiry_id,
              @Part("userid") RequestBody userid,
              @Part MultipartBody.Part complaintImage
    );
}

