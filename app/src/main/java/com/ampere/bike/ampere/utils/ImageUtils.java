package com.ampere.bike.ampere.utils;

import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;

/**
 * Created by AND I5 on 05-02-2018.
 */

public class ImageUtils {


    /**
     * Set imageview using url internal
     *
     * @param image
     * @param url
     * @param activity
     */
    public static void setImage(ImageView image, String url, FragmentActivity activity) {
       // Picasso.with(activity).load((Uri.fromFile(new File(url)))).placeholder(R.drawable.loader_gif).into(image);
        Glide.with(activity).load((Uri.fromFile(new File(url)))).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(image);

    }


    /**
     * Set imageview using url live
     *
     * @param image
     * @param url
     * @param activity
     */
    public static void setImageLive(ImageView image, String url, FragmentActivity activity) {
        //   Picasso.with(activity).load(url).placeholder(R.drawable.loader_gif).into(image);
        Glide.with(activity).load(url).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(image);
    }

    /**
     * Set image using drawable
     *
     * @param image
     * @param url
     * @param activity
     */
    public static void setImage(ImageView image, int url, FragmentActivity activity) {
        //Picasso.with(activity).load(url).placeholder(R.drawable.loader_gif).into(image);
        Glide.with(activity).load(url).diskCacheStrategy(DiskCacheStrategy.SOURCE).into(image);
    }
}
