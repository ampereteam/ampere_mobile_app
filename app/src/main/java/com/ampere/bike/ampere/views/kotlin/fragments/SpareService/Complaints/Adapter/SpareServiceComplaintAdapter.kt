package com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Complaints.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.kotlin.fragments.SpareService.Complaints.ComplaintListPojo.SparecomplaintsItem
import com.ampere.vehicles.R
import java.lang.Exception

class SpareServiceComplaintAdapter(val context: Context, val complaintList: ArrayList<SparecomplaintsItem?>?, val configClickEvent: ConfigClickEvent)
    :RecyclerView.Adapter<SpareServiceComplaintAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SpareServiceComplaintAdapter.ViewHolder {
        val view =LayoutInflater.from(this.context).inflate(R.layout.adpter_spare_service_complint_list,parent,false)
        return  ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return  this.complaintList?.size!!
    }

    override fun onBindViewHolder(holder: SpareServiceComplaintAdapter.ViewHolder, position: Int) {
        try{
                holder.complaintstatus.text=this.complaintList!![position]?.complaintStatus
                holder.sentBy.text =this.complaintList[position]?.sentBy
                holder.sno.text = (position+1).toString()
                holder.spcomplaint.text = this.complaintList[position]?.complaint
                holder.sentdate .text = this.complaintList[position]?.sentDate
                holder.spsclinearadp.setOnClickListener {
                    this.configClickEvent.connectposition(position,complaintList.size)
                }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
            val complaintstatus:TextView =itemView?.findViewById(R.id.spsc_complaint_status)!!
            val sno:TextView =itemView?.findViewById(R.id.spsc_s_no)!!
            val sentBy:TextView =itemView?.findViewById(R.id.spsc_sent_by)!!
            val sentdate:TextView =itemView?.findViewById(R.id.spsc_sentdate)!!
            val spcomplaint:TextView =itemView?.findViewById(R.id.spsc_complints)!!
            val spsclinearadp:LinearLayout =itemView?.findViewById(R.id.spsc_linear_adp)!!

    }
}