package com.ampere.bike.ampere.model.task;

import com.google.gson.annotations.SerializedName;

public class TaskUpdateModel {


    @SerializedName("success")
    public boolean isStatus;


    @SerializedName("message")
    public String message;

}
