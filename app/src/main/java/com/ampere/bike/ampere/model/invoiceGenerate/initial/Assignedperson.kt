package com.ampere.bike.ampere.model.invoiceGenerate.initial

data class Assignedperson(
        val id: Int,
        val name: String,
        val username: String,
        val email: String,
        val mobile: String,
        val profile_photo: String,
        val address: String,
        val address1: String,
        val state: String,
        val statecode: Int,
        val depatment_id: Int,
        val user_type: Int,
        val dealer_id: Any,
        val gst_no: String,
        val active: String,
        val created_at: String,
        val updated_at: String
)