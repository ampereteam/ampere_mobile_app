package com.ampere.bike.ampere.views.fragment.indent.VariantListPojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SparePojos {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("spare_code")
    @Expose
    private String spareCode;
    @SerializedName("spare_id")
    @Expose
    private String spareId;
    @SerializedName("vehicle_model_id")
    @Expose
    private Integer vehicleModelId;
    @SerializedName("model_varient_id")
    @Expose
    private String modelVarientId;
    @SerializedName("spares")
    @Expose
    private String spares;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSpareCode() {
        return spareCode;
    }

    public void setSpareCode(String spareCode) {
        this.spareCode = spareCode;
    }

    public String getSpareId() {
        return spareId;
    }

    public void setSpareId(String spareId) {
        this.spareId = spareId;
    }

    public Integer getVehicleModelId() {
        return vehicleModelId;
    }

    public void setVehicleModelId(Integer vehicleModelId) {
        this.vehicleModelId = vehicleModelId;
    }

    public String getModelVarientId() {
        return modelVarientId;
    }

    public void setModelVarientId(String modelVarientId) {
        this.modelVarientId = modelVarientId;
    }

    public String getSpares() {
        return spares;
    }

    public void setSpares(String spares) {
        this.spares = spares;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
