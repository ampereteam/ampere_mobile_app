package com.ampere.bike.ampere.model.invoiceGenerate.variantColor

data class Data(
        val colors: ArrayList<String>,
        val spares: ArrayList<Spare>
)