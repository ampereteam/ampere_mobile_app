package com.ampere.bike.ampere.views.kotlin.fragments.SpareService.DefectReturn.Pojos

import com.google.gson.annotations.SerializedName

class DefectItem(
        @field:SerializedName("id")
        var defectId :String?=null
)