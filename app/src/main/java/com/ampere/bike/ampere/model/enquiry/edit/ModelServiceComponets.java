
package com.ampere.bike.ampere.model.enquiry.edit;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ModelServiceComponets {

    @SerializedName("componentid")
    private Long mComponentid;
    @SerializedName("componentname")
    private String mComponentname;

    public Long getComponentid() {
        return mComponentid;
    }

    public void setComponentid(Long componentid) {
        mComponentid = componentid;
    }

    public String getComponentname() {
        return mComponentname;
    }

    public void setComponentname(String componentname) {
        mComponentname = componentname;
    }

}
