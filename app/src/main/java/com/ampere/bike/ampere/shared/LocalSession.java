package com.ampere.bike.ampere.shared;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;

import com.ampere.bike.ampere.CustomNavigationDuo;
import com.ampere.bike.ampere.utils.MessageUtils;
import com.ampere.bike.ampere.utils.MyUtils;
import com.ampere.bike.ampere.views.fragment.Home;

import static com.ampere.bike.ampere.CustomNavigationDuo.setHeaderText;
@SuppressLint("CommitPrefEdits")
public class LocalSession {

    /*
 1.Dealer
2.Corporate Sales
3.Sales
4.Service
5.Customers
6.CRM
7.Corporate Service*/
    public static String T_DEALER = "2";
    public static String T_CORPORATE_SALES = "2";
    public static String T_SALES = "3";
    public static String T_SERVICE = "4";
    public static String T_CUSTOMERS = "5";
    public static String T_CRM = "6";
    public static String T_CORPORATE_SERVICE = "7";
    public static String T_SPECIAL_OFFICER = "8";

    public static String KEY_NAME = "name";
    public static String KEY_MOBILE = "mobile";
    public static String KEY_LOGIN_STATUS = "false";
    public static String KEY_USER_TYPE = "user_type";
    public static String KEY_TOKEN = "token";
    public static String BEARER = "Bearer ";
    public static String KEY_PHOTO = "photo";
    public static String KEY_ADDRESS = "address";
    public static String KEY_EMAIL = "email";
    public static String KEY_USER_NAME = "user_name";
    public static String KEY_ID = "id";

    /**
     * Initialize SharedPreference
     *
     * @param activity
     * @return
     */
    public static SharedPreferences init(Context activity) {
        SharedPreferences pref = activity.getSharedPreferences("AmpereVehicles", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        return pref;
    }

    /**
     * 1.Dealer
     * 2.Corporate
     * 3.Sales
     * 4.Service
     * 5.Customers
     * 6.Crm
     */

    public LocalSession(FragmentActivity activity) {
        SharedPreferences sharedPreferences = init(activity);
      SharedPreferences.Editor editor = sharedPreferences.edit();
    }

    /**
     * Create Session
     *
     * @param activity
     * @param name
     * @param mobile
     */
    public static void createSeesion(FragmentActivity activity, String name, String user_name, String mobile, String user_type, String token, String photo, String address, String email, int id) {


        SharedPreferences sharedPreferences = init(activity);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_LOGIN_STATUS, true);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_MOBILE, mobile);

        editor.putString(KEY_USER_TYPE, user_type);
        editor.putString(KEY_TOKEN, token);

        editor.putString(KEY_PHOTO, photo);
        editor.putString(KEY_ADDRESS, address);

        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_ID, "" + id);
        editor.putString(KEY_USER_NAME, user_name);

        editor.apply();

        MyUtils.passFragmentWithoutBackStack(activity, new Home());


    }

    /**
     * Check Login Status
     *
     * @param activity
     * @return
     */
    public static boolean isLogin(FragmentActivity activity) {
        SharedPreferences sharedPreferences = init(activity);
        return sharedPreferences.getBoolean(KEY_LOGIN_STATUS, false);
    }

    /**
     * Get selected value (Multi Purpose)
     *
     * @param activity
     * @param name
     * @return
     */
    public static String getUserInfo(Context activity, String name) {
        SharedPreferences pref = init(activity);
        //Log.d("szsass", "" + pref.getAll());
        return pref.getString(name, "");
    }


    public static void logout(FragmentActivity activity) {

        SharedPreferences pref = init(activity);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.apply();
        setHeaderText(activity);
        //listSetter.setList(setNavigationView(activity));
        MessageUtils.showToastMessage(activity, "Logout Success");

        activity.startActivity(new Intent(activity, CustomNavigationDuo.class));
        activity.overridePendingTransition(0, 0);

        activity.finish();
        //MyUtils.passFragmentWithoutBackStack(activity, new Login());

    }
}
