package com.ampere.bike.ampere.views.fragment.enquiry.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.views.fragment.enquiry.servicesearchpojo.DetailsItem
import com.ampere.vehicles.R
import customviews.CustomTextView


class ServiceSearchAdapter (val context : Context, val serviceenqityList:ArrayList<DetailsItem?>?, val onclickEvnt: ConfigClickEvent)
    : RecyclerView.Adapter<ServiceSearchAdapter.ViewHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ServiceSearchAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.adapter_service_enq_search,p0,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int { return  serviceenqityList?.size!! }

    override fun onBindViewHolder(p0: ServiceSearchAdapter.ViewHolder, p1: Int) {
        try {

            Log.d("AdpterData",""+ serviceenqityList!![p1]?.chassisNo)
            Log.d("AdpterData",""+ serviceenqityList!![p1]?.vehicleModel)
            Log.d("AdpterData",""+ serviceenqityList!![p1]?.color)
            p0.chassnumber.text = serviceenqityList!![p1]?.chassisNo
            p0.vehclemodel.text = serviceenqityList[p1]?.vehicleModel
            p0.color.text = serviceenqityList[p1]?.color
            p0.linear.setOnClickListener {
                onclickEvnt.connectposition(p1,101)
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var  vehclemodel =itemView.findViewById<CustomTextView>(R.id.search_vehi_adap)
        var  chassnumber =itemView.findViewById<CustomTextView>(R.id.search_chass_adap)
        var  color =itemView.findViewById<CustomTextView>(R.id.search_color_adap)
        var  linear =itemView.findViewById<LinearLayout>(R.id.menus)
    }
}