

package com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSales


import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.ampere.bike.ampere.CustomNavigationDuo
import com.ampere.bike.ampere.CustomNavigationDuo.snackVIew
import com.ampere.bike.ampere.api.Retrofit.ConfigClickEvent
import com.ampere.bike.ampere.api.api_new.UtilsCallApi
import com.ampere.bike.ampere.shared.LocalSession
import com.ampere.bike.ampere.shared.LocalSession.KEY_ID
import com.ampere.bike.ampere.shared.LocalSession.KEY_TOKEN
import com.ampere.bike.ampere.utils.MessageUtils
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome.Companion.btnAssignList
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome.Companion.salesserverEnquiryList
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignListHome.Companion.spareserverEnquiryList
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.AssignSales.Adapter.AssignSalesAdapter
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo.AssignEnqIDPojo
import com.ampere.bike.ampere.views.kotlin.fragments.AssignList.Pojo.EnquiryListRes

import com.ampere.vehicles.R
import com.google.gson.Gson
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("SetTextI18n,StaticFieldLeak")
class AssignSales : Fragment(),ConfigClickEvent {


    var recyAssignSAles :RecyclerView?=null
     var dialog :Dialog  ?=null
     var utilsCallApi:UtilsCallApi?=null
     var cbxAssingSales:CheckBox ?= null
    var  emptyList :TextView ?=null
    var  assignSalesAdapter : AssignSalesAdapter?=null
    companion object {
        var btnAssignsales:TextView ?= null
        var salesEnqryRES: EnquiryListRes?= null
        var salescount =0
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        (activity as CustomNavigationDuo).disbleNavigationView(getString(R.string.assigned_enquiry))
        return inflater.inflate(R.layout.fragment_unassign_sales, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(activity)
        utilsCallApi = UtilsCallApi(activity)
        recyAssignSAles=view.findViewById(R.id.recyl_assign_sales)
        cbxAssingSales =view.findViewById(R.id.cbx_assgin_sales)
        btnAssignsales =view.findViewById(R.id.btn_assignsales)
        emptyList =view.findViewById(R.id.empty_list)
        cbxAssingSales?.isChecked =false

        btnAssignsales?.visibility =View.GONE
        if(utilsCallApi?.isNetworkConnected!!){
            getSalesEnquiryList()
        }else{
            MessageUtils.showSnackBarAction(activity,snackVIew,getString(R.string.check_internet))
        }

        cbxAssingSales?.setOnCheckedChangeListener { buttonView, isChecked ->

                if(salesEnqryRES?.data?.enquiries?.size!!<=0){
                    Log.d("LISTSIze",""+salesEnqryRES?.data?.enquiries?.size)
                    salescount=0
                }else{
                    if(isChecked){
                        for(i:Int in 0.. salesEnqryRES?.data?.enquiries?.size!!-1){
                            salesEnqryRES?.data?.enquiries?.get(i)?.check =true
                            salesserverEnquiryList[i]?.check =true
                        }
                        btnAssignsales?.setText(getString(R.string.assign)+"("+salesEnqryRES?.data?.enquiries?.size+")")
                        btnAssignList?.setText(getString(R.string.assign)+"("+salesEnqryRES?.data?.enquiries?.size+")")
                        btnAssignsales?.visibility =View.VISIBLE
                        btnAssignList?.visibility =View.VISIBLE
                        salescount= salesEnqryRES?.data?.enquiries?.size!!
                    }else{
                        for(i:Int in 0.. salesEnqryRES?.data?.enquiries?.size!!-1){
                            salesEnqryRES?.data?.enquiries?.get(i)?.check =false
                            salesserverEnquiryList[i]?.check =false
                        }
                        btnAssignsales?.visibility =View.GONE
                        btnAssignList?.visibility =View.GONE
                        salescount=0
                    }
                    assignSalesAdapter?.notifyDataSetChanged()
                }


        }
        btnAssignsales?.setOnClickListener {
            try{
                if(AssignListHome.spAssignPerson?.selectedItem !=null){
                    assignServiceEnquiries()
                }else{
                    MessageUtils.showSnackBar(activity, snackVIew,"Select Assign To and Person")
                }
            }catch (ex:Exception){
                ex.printStackTrace()
            }
        }
   /*     if(AssignListHome.btnAssignList!=null){
            AssignListHome.btnAssignList?.setOnClickListener {
                try{
                    if(AssignListHome.spAssignPerson?.selectedItem !=null){
                        assignServiceEnquiries()
                    }else{
                        MessageUtils.showSnackBar(activity, CustomNavigationDuo.snackVIew,"Select Assign To and Person")
                    }
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }
        }*/
    }

    private fun assignServiceEnquiries() {
        try{
            val map:HashMap<String,Any>  = HashMap()
            map["userid"] =LocalSession.getUserInfo(activity, KEY_ID)
            map["assign_to"] = AssignListHome.spAssignTo?.selectedItem.toString()
            var assignpersonID :String?=null
            for(user in AssignListHome.userListRes?.data?.users!!){
                if( user?.name.equals(AssignListHome.spAssignPerson?.selectedItem.toString())){
                    assignpersonID = user?.id.toString()
                }
            }
            if(assignpersonID!=null){
                map["assign_person"] =assignpersonID.toInt()
            }
            val hashMap:HashMap<String,Any>  =HashMap()
            val enquiryList :ArrayList<AssignEnqIDPojo> = ArrayList()
            for (i:Int in 0 .. salesserverEnquiryList.size-1){
                if(salesserverEnquiryList.get(i)?.check!!) {
                    val assignEnqIDPojo = AssignEnqIDPojo(salesserverEnquiryList.get(i)?.id.toString())
                    enquiryList.add(assignEnqIDPojo)
                }
                }
            hashMap["enquiry"] =enquiryList
            map["enquiries"] =hashMap
            Log.d("AssignEnquirySales",""+map.toString())
            if(utilsCallApi?.isNetworkConnected!!){
                dialog =MessageUtils.showDialog(activity)
                val calinterface = utilsCallApi?.connectRetro("POST","assignenquiry")
                val callback = calinterface?.call_post("assignenquiry","Bearer "+LocalSession.getUserInfo(activity, KEY_TOKEN),map)
                callback?.enqueue(object :Callback<ResponseBody>{
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        try{
                            MessageUtils.dismissDialog(dialog)
                            MessageUtils.showSnackBar(activity, snackVIew,t.localizedMessage)
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                    }

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        MessageUtils.dismissDialog(dialog)
                        try{
                            if(response.isSuccessful){
                                try{
                                    val jsonObject =JSONObject(response.body()?.string())
                                    val message = jsonObject.getString("message")
                                    val success =jsonObject.getBoolean("success")
                                    if(success){
                                        btnAssignsales?.visibility =View.GONE
                                        btnAssignList?.visibility =View.GONE
                                        Toast.makeText(activity,message,Toast.LENGTH_LONG).show()
                                        getSalesEnquiryList()
                                    }else{
                                        MessageUtils.showSnackBar(activity, snackVIew,message)
                                    }
                                }catch (ex:Exception){
                                    ex.printStackTrace()

                                }
                            }else{
                                MessageUtils.showSnackBar(activity, snackVIew,response.message())
                            }
                        }catch (ex:Exception){
                            ex.printStackTrace()
                        }
                    }

                })
            }else{
                MessageUtils.showSnackBarAction(activity, snackVIew,getString(R.string.check_internet))
            }

        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }

    private fun getSalesEnquiryList() {
     try{
         val hashMap :HashMap<String,Any> = HashMap()
            hashMap["userid"] =LocalSession.getUserInfo(activity, KEY_ID)
            hashMap["enquiry_type"] = getString(R.string.sales)
         dialog =MessageUtils.showDialog(activity)
         val callInterface  = utilsCallApi?.connectRetro("POST","EnquiryListRes")
         val callBack = callInterface?.call_post("unassignedenquiry","Bearer "+LocalSession.getUserInfo(activity,KEY_TOKEN),hashMap)
          callBack?.enqueue(object :Callback<ResponseBody>{
              override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                  try{
                      MessageUtils.dismissDialog(dialog)
                      MessageUtils.showSnackBar(activity, snackVIew,t.localizedMessage)

                  }catch (ex:Exception){
                      ex.printStackTrace()
                  }
              }

              override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                  MessageUtils.dismissDialog(dialog)
                  try{
                      if(response.isSuccessful){
                          try{
                              salesEnqryRES = Gson().fromJson(response.body()?.string(),EnquiryListRes::class.java)
                              if(salesEnqryRES?.success!!){
                                  if(salesEnqryRES?.data?.enquiries?.size!! <=0){
                                      emptyList?.setText(salesEnqryRES?.message)
                                      emptyList?.visibility = View.VISIBLE
                                      recyAssignSAles?.visibility =View.GONE
                                  }else{
                                      if(btnAssignsales?.visibility == View.VISIBLE  ){
                                          btnAssignsales?.visibility = View.GONE
                                      }
                                      if(cbxAssingSales?.isChecked!!){
                                          cbxAssingSales?.isChecked =false
                                      }
                                      emptyList?.visibility = View.GONE
                                      recyAssignSAles?.visibility =View.VISIBLE
                                      salesserverEnquiryList = salesEnqryRES?.data?.enquiries!!
                                      salescount=0
                                      setTOAdapter()
                                  }

                              }else{
                                  MessageUtils.showSnackBar(activity, snackVIew,salesEnqryRES?.message)
                              }
                          }catch (ex:Exception){
                              ex.printStackTrace()
                          }
                      }else{
                          MessageUtils.showSnackBar(activity, snackVIew,response.message())
                      }
                  }catch (ex:Exception){
                      ex.printStackTrace()
                  }
              }
          })
     }catch (ex:Exception){
         ex.printStackTrace()
     }
    }

    private fun setTOAdapter() {
        try{
            recyAssignSAles?.layoutManager = LinearLayoutManager(activity,LinearLayout.VERTICAL,false)
            assignSalesAdapter = AssignSalesAdapter(activity!!,salesserverEnquiryList,this)
            recyAssignSAles?.adapter = assignSalesAdapter
        }catch (eX:Exception){
            eX.printStackTrace()
        }
    }

    override fun connectposition(position: Int, listSize: Int) {
        try{
            Log.d("CallPostion",""+position+"\tlistsize"+listSize)
            if(listSize<=0){
                btnAssignsales?.visibility =View.GONE
                btnAssignList?.visibility =View.GONE
                salescount=0
            }else{
                btnAssignsales?.setText(getString(R.string.assign)+"("+listSize+")")
                btnAssignList?.setText(getString(R.string.assign)+"("+listSize+")")
                btnAssignsales?.visibility =View.VISIBLE
                btnAssignList?.visibility =View.VISIBLE
                salescount=listSize
            }
        }catch (ex:Exception){
            ex.printStackTrace()
        }
    }
}
