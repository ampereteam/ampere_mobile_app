package com.ampere.bike.ampere.views.kotlin.model.unassinged.assign_details

data class AssingFromCRM(
        val success: Boolean,
        val data: Data,
        val message: String
)